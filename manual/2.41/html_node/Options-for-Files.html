<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Options for Files (The GNU C Library)</title>

<meta name="description" content="Options for Files (The GNU C Library)">
<meta name="keywords" content="Options for Files (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="System-Configuration.html" rel="up" title="System Configuration">
<link href="File-Minimums.html" rel="next" title="File Minimums">
<link href="Limits-for-Files.html" rel="prev" title="Limits for Files">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Options-for-Files">
<div class="nav-panel">
<p>
Next: <a href="File-Minimums.html" accesskey="n" rel="next">Minimum Values for File System Limits</a>, Previous: <a href="Limits-for-Files.html" accesskey="p" rel="prev">Limits on File System Capacity</a>, Up: <a href="System-Configuration.html" accesskey="u" rel="up">System Configuration Parameters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Optional-Features-in-File-Support"><span>33.7 Optional Features in File Support<a class="copiable-link" href="#Optional-Features-in-File-Support"> &para;</a></span></h3>

<p>POSIX defines certain system-specific options in the system calls for
operating on files.  Some systems support these options and others do
not.  Since these options are provided in the kernel, not in the
library, simply using the GNU C Library does not guarantee that any of these
features is supported; it depends on the system you are using.  They can
also vary between file systems on a single machine.
</p>
<a class="index-entry-id" id="index-unistd_002eh-28"></a>
<p>This section describes the macros you can test to determine whether a
particular option is supported on your machine.  If a given macro is
defined in <samp class="file">unistd.h</samp>, then its value says whether the
corresponding feature is supported.  (A value of <code class="code">-1</code> indicates no;
any other value indicates yes.)  If the macro is undefined, it means
particular files may or may not support the feature.
</p>
<p>Since all the machines that support the GNU C Library also support NFS,
one can never make a general statement about whether all file systems
support the <code class="code">_POSIX_CHOWN_RESTRICTED</code> and <code class="code">_POSIX_NO_TRUNC</code>
features.  So these names are never defined as macros in the GNU C Library.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPOSIX_005fCHOWN_005fRESTRICTED"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">_POSIX_CHOWN_RESTRICTED</strong><a class="copiable-link" href="#index-_005fPOSIX_005fCHOWN_005fRESTRICTED"> &para;</a></span></dt>
<dd>
<p>If this option is in effect, the <code class="code">chown</code> function is restricted so
that the only changes permitted to nonprivileged processes is to change
the group owner of a file to either be the effective group ID of the
process, or one of its supplementary group IDs.  See <a class="xref" href="File-Owner.html">File Owner</a>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPOSIX_005fNO_005fTRUNC"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">_POSIX_NO_TRUNC</strong><a class="copiable-link" href="#index-_005fPOSIX_005fNO_005fTRUNC"> &para;</a></span></dt>
<dd>
<p>If this option is in effect, file name components longer than
<code class="code">NAME_MAX</code> generate an <code class="code">ENAMETOOLONG</code> error.  Otherwise, file
name components that are too long are silently truncated.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPOSIX_005fVDISABLE-1"><span class="category-def">Macro: </span><span><code class="def-type">unsigned char</code> <strong class="def-name">_POSIX_VDISABLE</strong><a class="copiable-link" href="#index-_005fPOSIX_005fVDISABLE-1"> &para;</a></span></dt>
<dd>
<p>This option is only meaningful for files that are terminal devices.
If it is enabled, then handling for special control characters can
be disabled individually.  See <a class="xref" href="Special-Characters.html">Special Characters</a>.
</p></dd></dl>

<a class="index-entry-id" id="index-unistd_002eh-29"></a>
<p>If one of these macros is undefined, that means that the option might be
in effect for some files and not for others.  To inquire about a
particular file, call <code class="code">pathconf</code> or <code class="code">fpathconf</code>.
See <a class="xref" href="Pathconf.html">Using <code class="code">pathconf</code></a>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="File-Minimums.html">Minimum Values for File System Limits</a>, Previous: <a href="Limits-for-Files.html">Limits on File System Capacity</a>, Up: <a href="System-Configuration.html">System Configuration Parameters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
