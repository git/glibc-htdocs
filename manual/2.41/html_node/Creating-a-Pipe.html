<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Creating a Pipe (The GNU C Library)</title>

<meta name="description" content="Creating a Pipe (The GNU C Library)">
<meta name="keywords" content="Creating a Pipe (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Pipes-and-FIFOs.html" rel="up" title="Pipes and FIFOs">
<link href="Pipe-to-a-Subprocess.html" rel="next" title="Pipe to a Subprocess">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Creating-a-Pipe">
<div class="nav-panel">
<p>
Next: <a href="Pipe-to-a-Subprocess.html" accesskey="n" rel="next">Pipe to a Subprocess</a>, Up: <a href="Pipes-and-FIFOs.html" accesskey="u" rel="up">Pipes and FIFOs</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Creating-a-Pipe-1"><span>15.1 Creating a Pipe<a class="copiable-link" href="#Creating-a-Pipe-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-creating-a-pipe"></a>
<a class="index-entry-id" id="index-opening-a-pipe"></a>
<a class="index-entry-id" id="index-interprocess-communication_002c-with-pipes"></a>

<p>The primitive for creating a pipe is the <code class="code">pipe</code> function.  This
creates both the reading and writing ends of the pipe.  It is not very
useful for a single process to use a pipe to talk to itself.  In typical
use, a process creates a pipe just before it forks one or more child
processes (see <a class="pxref" href="Creating-a-Process.html">Creating a Process</a>).  The pipe is then used for
communication either between the parent or child processes, or between
two sibling processes.
</p>
<p>The <code class="code">pipe</code> function is declared in the header file
<samp class="file">unistd.h</samp>.
<a class="index-entry-id" id="index-unistd_002eh-12"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pipe-1"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">pipe</strong> <code class="def-code-arguments">(int <var class="var">filedes</var><code class="t">[2]</code>)</code><a class="copiable-link" href="#index-pipe-1"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">pipe</code> function creates a pipe and puts the file descriptors
for the reading and writing ends of the pipe (respectively) into
<code class="code"><var class="var">filedes</var>[0]</code> and <code class="code"><var class="var">filedes</var>[1]</code>.
</p>
<p>An easy way to remember that the input end comes first is that file
descriptor <code class="code">0</code> is standard input, and file descriptor <code class="code">1</code> is
standard output.
</p>
<p>If successful, <code class="code">pipe</code> returns a value of <code class="code">0</code>.  On failure,
<code class="code">-1</code> is returned.  The following <code class="code">errno</code> error conditions are
defined for this function:
</p>
<dl class="table">
<dt><code class="code">EMFILE</code></dt>
<dd><p>The process has too many files open.
</p>
</dd>
<dt><code class="code">ENFILE</code></dt>
<dd><p>There are too many open files in the entire system.  See <a class="xref" href="Error-Codes.html">Error Codes</a>,
for more information about <code class="code">ENFILE</code>.  This error never occurs on
GNU/Hurd systems.
</p></dd>
</dl>
</dd></dl>

<p>Here is an example of a simple program that creates a pipe.  This program
uses the <code class="code">fork</code> function (see <a class="pxref" href="Creating-a-Process.html">Creating a Process</a>) to create
a child process.  The parent process writes data to the pipe, which is
read by the child process.
</p>
<div class="example smallexample">
<pre class="example-preformatted">

#include &lt;sys/types.h&gt;
#include &lt;unistd.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

/* <span class="r">Read characters from the pipe and echo them to <code class="code">stdout</code>.</span> */

void
read_from_pipe (int file)
{
  FILE *stream;
  int c;
  stream = fdopen (file, &quot;r&quot;);
  while ((c = fgetc (stream)) != EOF)
    putchar (c);
  fclose (stream);
}

/* <span class="r">Write some random text to the pipe.</span> */

void
write_to_pipe (int file)
{
  FILE *stream;
  stream = fdopen (file, &quot;w&quot;);
  fprintf (stream, &quot;hello, world!\n&quot;);
  fprintf (stream, &quot;goodbye, world!\n&quot;);
  fclose (stream);
}

int
main (void)
{
  pid_t pid;
  int mypipe[2];

</pre><div class="group"><pre class="example-preformatted">  /* <span class="r">Create the pipe.</span> */
  if (pipe (mypipe))
    {
      fprintf (stderr, &quot;Pipe failed.\n&quot;);
      return EXIT_FAILURE;
    }
</pre></div><pre class="example-preformatted">

  /* <span class="r">Create the child process.</span> */
  pid = fork ();
  if (pid == (pid_t) 0)
    {
      /* <span class="r">This is the child process.
         Close other end first.</span> */
      close (mypipe[1]);
      read_from_pipe (mypipe[0]);
      return EXIT_SUCCESS;
    }
  else if (pid &lt; (pid_t) 0)
    {
      /* <span class="r">The fork failed.</span> */
      fprintf (stderr, &quot;Fork failed.\n&quot;);
      return EXIT_FAILURE;
    }
  else
    {
      /* <span class="r">This is the parent process.
         Close other end first.</span> */
      close (mypipe[0]);
      write_to_pipe (mypipe[1]);
      return EXIT_SUCCESS;
    }
}
</pre></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Pipe-to-a-Subprocess.html">Pipe to a Subprocess</a>, Up: <a href="Pipes-and-FIFOs.html">Pipes and FIFOs</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
