<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Open File Description Locks (The GNU C Library)</title>

<meta name="description" content="Open File Description Locks (The GNU C Library)">
<meta name="keywords" content="Open File Description Locks (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Low_002dLevel-I_002fO.html" rel="up" title="Low-Level I/O">
<link href="Open-File-Description-Locks-Example.html" rel="next" title="Open File Description Locks Example">
<link href="File-Locks.html" rel="prev" title="File Locks">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Open-File-Description-Locks">
<div class="nav-panel">
<p>
Next: <a href="Open-File-Description-Locks-Example.html" accesskey="n" rel="next">Open File Description Locks Example</a>, Previous: <a href="File-Locks.html" accesskey="p" rel="prev">File Locks</a>, Up: <a href="Low_002dLevel-I_002fO.html" accesskey="u" rel="up">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Open-File-Description-Locks-1"><span>13.17 Open File Description Locks<a class="copiable-link" href="#Open-File-Description-Locks-1"> &para;</a></span></h3>

<p>In contrast to process-associated record locks (see <a class="pxref" href="File-Locks.html">File Locks</a>),
open file description record locks are associated with an open file
description rather than a process.
</p>
<p>Using <code class="code">fcntl</code> to apply an open file description lock on a region that
already has an existing open file description lock that was created via the
same file descriptor will never cause a lock conflict.
</p>
<p>Open file description locks are also inherited by child processes across
<code class="code">fork</code>, or <code class="code">clone</code> with <code class="code">CLONE_FILES</code> set
(see <a class="pxref" href="Creating-a-Process.html">Creating a Process</a>), along with the file descriptor.
</p>
<p>It is important to distinguish between the open file <em class="emph">description</em> (an
instance of an open file, usually created by a call to <code class="code">open</code>) and
an open file <em class="emph">descriptor</em>, which is a numeric value that refers to the
open file description.  The locks described here are associated with the
open file <em class="emph">description</em> and not the open file <em class="emph">descriptor</em>.
</p>
<p>Using <code class="code">dup</code> (see <a class="pxref" href="Duplicating-Descriptors.html">Duplicating Descriptors</a>) to copy a file
descriptor does not give you a new open file description, but rather copies a
reference to an existing open file description and assigns it to a new
file descriptor.  Thus, open file description locks set on a file
descriptor cloned by <code class="code">dup</code> will never conflict with open file
description locks set on the original descriptor since they refer to the
same open file description.  Depending on the range and type of lock
involved, the original lock may be modified by a <code class="code">F_OFD_SETLK</code> or
<code class="code">F_OFD_SETLKW</code> command in this situation however.
</p>
<p>Open file description locks always conflict with process-associated locks,
even if acquired by the same process or on the same open file
descriptor.
</p>
<p>Open file description locks use the same <code class="code">struct flock</code> as
process-associated locks as an argument (see <a class="pxref" href="File-Locks.html">File Locks</a>) and the
macros for the <code class="code">command</code> values are also declared in the header file
<samp class="file">fcntl.h</samp>. To use them, the macro <code class="code">_GNU_SOURCE</code> must be
defined prior to including any header file.
</p>
<p>In contrast to process-associated locks, any <code class="code">struct flock</code> used as
an argument to open file description lock commands must have the <code class="code">l_pid</code>
value set to <em class="math">0</em>.  Also, when returning information about an
open file description lock in a <code class="code">F_GETLK</code> or <code class="code">F_OFD_GETLK</code> request,
the <code class="code">l_pid</code> field in <code class="code">struct flock</code> will be set to <em class="math">-1</em>
to indicate that the lock is not associated with a process.
</p>
<p>When the same <code class="code">struct flock</code> is reused as an argument to a
<code class="code">F_OFD_SETLK</code> or <code class="code">F_OFD_SETLKW</code> request after being used for an
<code class="code">F_OFD_GETLK</code> request, it is necessary to inspect and reset the
<code class="code">l_pid</code> field to <em class="math">0</em>.
</p>
<a class="index-entry-id" id="index-fcntl_002eh_002e"></a>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-F_005fOFD_005fGETLK-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">F_OFD_GETLK</strong><a class="copiable-link" href="#index-F_005fOFD_005fGETLK-1"> &para;</a></span></dt>
<dd><p>This macro is used as the <var class="var">command</var> argument to <code class="code">fcntl</code>, to
specify that it should get information about a lock.  This command
requires a third argument of type <code class="code">struct&nbsp;flock&nbsp;*</code><!-- /@w --> to be passed
to <code class="code">fcntl</code>, so that the form of the call is:
</p>
<div class="example smallexample">
<pre class="example-preformatted">fcntl (<var class="var">filedes</var>, F_OFD_GETLK, <var class="var">lockp</var>)
</pre></div>

<p>If there is a lock already in place that would block the lock described
by the <var class="var">lockp</var> argument, information about that lock is written to
<code class="code">*<var class="var">lockp</var></code>.  Existing locks are not reported if they are
compatible with making a new lock as specified.  Thus, you should
specify a lock type of <code class="code">F_WRLCK</code> if you want to find out about both
read and write locks, or <code class="code">F_RDLCK</code> if you want to find out about
write locks only.
</p>
<p>There might be more than one lock affecting the region specified by the
<var class="var">lockp</var> argument, but <code class="code">fcntl</code> only returns information about
one of them. Which lock is returned in this situation is undefined.
</p>
<p>The <code class="code">l_whence</code> member of the <var class="var">lockp</var> structure are set to
<code class="code">SEEK_SET</code> and the <code class="code">l_start</code> and <code class="code">l_len</code> fields are set
to identify the locked region.
</p>
<p>If no conflicting lock exists, the only change to the <var class="var">lockp</var> structure
is to update the <code class="code">l_type</code> field to the value <code class="code">F_UNLCK</code>.
</p>
<p>The normal return value from <code class="code">fcntl</code> with this command is either <em class="math">0</em>
on success or <em class="math">-1</em>, which indicates an error. The following <code class="code">errno</code>
error conditions are defined for this command:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is invalid.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>Either the <var class="var">lockp</var> argument doesn&rsquo;t specify valid lock information,
the operating system kernel doesn&rsquo;t support open file description locks, or the file
associated with <var class="var">filedes</var> doesn&rsquo;t support locks.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-F_005fOFD_005fSETLK-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">F_OFD_SETLK</strong><a class="copiable-link" href="#index-F_005fOFD_005fSETLK-1"> &para;</a></span></dt>
<dd>
<p>This macro is used as the <var class="var">command</var> argument to <code class="code">fcntl</code>, to
specify that it should set or clear a lock.  This command requires a
third argument of type <code class="code">struct&nbsp;flock&nbsp;*</code><!-- /@w --> to be passed to
<code class="code">fcntl</code>, so that the form of the call is:
</p>
<div class="example smallexample">
<pre class="example-preformatted">fcntl (<var class="var">filedes</var>, F_OFD_SETLK, <var class="var">lockp</var>)
</pre></div>

<p>If the open file already has a lock on any part of the
region, the old lock on that part is replaced with the new lock.  You
can remove a lock by specifying a lock type of <code class="code">F_UNLCK</code>.
</p>
<p>If the lock cannot be set, <code class="code">fcntl</code> returns immediately with a value
of <em class="math">-1</em>.  This command does not wait for other tasks
to release locks.  If <code class="code">fcntl</code> succeeds, it returns <em class="math">0</em>.
</p>
<p>The following <code class="code">errno</code> error conditions are defined for this
command:
</p>
<dl class="table">
<dt><code class="code">EAGAIN</code></dt>
<dd><p>The lock cannot be set because it is blocked by an existing lock on the
file.
</p>
</dd>
<dt><code class="code">EBADF</code></dt>
<dd><p>Either: the <var class="var">filedes</var> argument is invalid; you requested a read lock
but the <var class="var">filedes</var> is not open for read access; or, you requested a
write lock but the <var class="var">filedes</var> is not open for write access.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>Either the <var class="var">lockp</var> argument doesn&rsquo;t specify valid lock information,
the operating system kernel doesn&rsquo;t support open file description locks, or the
file associated with <var class="var">filedes</var> doesn&rsquo;t support locks.
</p>
</dd>
<dt><code class="code">ENOLCK</code></dt>
<dd><p>The system has run out of file lock resources; there are already too
many file locks in place.
</p>
<p>Well-designed file systems never report this error, because they have no
limitation on the number of locks.  However, you must still take account
of the possibility of this error, as it could result from network access
to a file system on another machine.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-F_005fOFD_005fSETLKW-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">F_OFD_SETLKW</strong><a class="copiable-link" href="#index-F_005fOFD_005fSETLKW-1"> &para;</a></span></dt>
<dd>
<p>This macro is used as the <var class="var">command</var> argument to <code class="code">fcntl</code>, to
specify that it should set or clear a lock.  It is just like the
<code class="code">F_OFD_SETLK</code> command, but causes the process to wait until the request
can be completed.
</p>
<p>This command requires a third argument of type <code class="code">struct flock *</code>, as
for the <code class="code">F_OFD_SETLK</code> command.
</p>
<p>The <code class="code">fcntl</code> return values and errors are the same as for the
<code class="code">F_OFD_SETLK</code> command, but these additional <code class="code">errno</code> error conditions
are defined for this command:
</p>
<dl class="table">
<dt><code class="code">EINTR</code></dt>
<dd><p>The function was interrupted by a signal while it was waiting.
See <a class="xref" href="Interrupted-Primitives.html">Primitives Interrupted by Signals</a>.
</p>
</dd>
</dl>
</dd></dl>

<p>Open file description locks are useful in the same sorts of situations as
process-associated locks. They can also be used to synchronize file
access between threads within the same process by having each thread perform
its own <code class="code">open</code> of the file, to obtain its own open file description.
</p>
<p>Because open file description locks are automatically freed only upon
closing the last file descriptor that refers to the open file
description, this locking mechanism avoids the possibility that locks
are inadvertently released due to a library routine opening and closing
a file without the application being aware.
</p>
<p>As with process-associated locks, open file description locks are advisory.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Open-File-Description-Locks-Example.html">Open File Description Locks Example</a>, Previous: <a href="File-Locks.html">File Locks</a>, Up: <a href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
