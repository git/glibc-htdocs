<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Portable Positioning (The GNU C Library)</title>

<meta name="description" content="Portable Positioning (The GNU C Library)">
<meta name="keywords" content="Portable Positioning (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="I_002fO-on-Streams.html" rel="up" title="I/O on Streams">
<link href="Stream-Buffering.html" rel="next" title="Stream Buffering">
<link href="File-Positioning.html" rel="prev" title="File Positioning">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Portable-Positioning">
<div class="nav-panel">
<p>
Next: <a href="Stream-Buffering.html" accesskey="n" rel="next">Stream Buffering</a>, Previous: <a href="File-Positioning.html" accesskey="p" rel="prev">File Positioning</a>, Up: <a href="I_002fO-on-Streams.html" accesskey="u" rel="up">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Portable-File_002dPosition-Functions"><span>12.19 Portable File-Position Functions<a class="copiable-link" href="#Portable-File_002dPosition-Functions"> &para;</a></span></h3>

<p>On GNU systems, the file position is truly a character count.  You
can specify any character count value as an argument to <code class="code">fseek</code> or
<code class="code">fseeko</code> and get reliable results for any random access file.
However, some ISO&nbsp;C<!-- /@w --> systems do not represent file positions in this
way.
</p>
<p>On some systems where text streams truly differ from binary streams, it
is impossible to represent the file position of a text stream as a count
of characters from the beginning of the file.  For example, the file
position on some systems must encode both a record offset within the
file, and a character offset within the record.
</p>
<p>As a consequence, if you want your programs to be portable to these
systems, you must observe certain rules:
</p>
<ul class="itemize mark-bullet">
<li>The value returned from <code class="code">ftell</code> on a text stream has no predictable
relationship to the number of characters you have read so far.  The only
thing you can rely on is that you can use it subsequently as the
<var class="var">offset</var> argument to <code class="code">fseek</code> or <code class="code">fseeko</code> to move back to
the same file position.

</li><li>In a call to <code class="code">fseek</code> or <code class="code">fseeko</code> on a text stream, either the
<var class="var">offset</var> must be zero, or <var class="var">whence</var> must be <code class="code">SEEK_SET</code> and
the <var class="var">offset</var> must be the result of an earlier call to <code class="code">ftell</code>
on the same stream.

</li><li>The value of the file position indicator of a text stream is undefined
while there are characters that have been pushed back with <code class="code">ungetc</code>
that haven&rsquo;t been read or discarded.  See <a class="xref" href="Unreading.html">Unreading</a>.
</li></ul>

<p>But even if you observe these rules, you may still have trouble for long
files, because <code class="code">ftell</code> and <code class="code">fseek</code> use a <code class="code">long int</code> value
to represent the file position.  This type may not have room to encode
all the file positions in a large file.  Using the <code class="code">ftello</code> and
<code class="code">fseeko</code> functions might help here since the <code class="code">off_t</code> type is
expected to be able to hold all file position values but this still does
not help to handle additional information which must be associated with
a file position.
</p>
<p>So if you do want to support systems with peculiar encodings for the
file positions, it is better to use the functions <code class="code">fgetpos</code> and
<code class="code">fsetpos</code> instead.  These functions represent the file position
using the data type <code class="code">fpos_t</code>, whose internal representation varies
from system to system.
</p>
<p>These symbols are declared in the header file <samp class="file">stdio.h</samp>.
<a class="index-entry-id" id="index-stdio_002eh-10"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-fpos_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">fpos_t</strong><a class="copiable-link" href="#index-fpos_005ft"> &para;</a></span></dt>
<dd>
<p>This is the type of an object that can encode information about the
file position of a stream, for use by the functions <code class="code">fgetpos</code> and
<code class="code">fsetpos</code>.
</p>
<p>In the GNU C Library, <code class="code">fpos_t</code> is an opaque data structure that
contains internal data to represent file offset and conversion state
information.  In other systems, it might have a different internal
representation.
</p>
<p>When compiling with <code class="code">_FILE_OFFSET_BITS == 64</code> on a 32 bit machine
this type is in fact equivalent to <code class="code">fpos64_t</code> since the LFS
interface transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-fpos64_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">fpos64_t</strong><a class="copiable-link" href="#index-fpos64_005ft"> &para;</a></span></dt>
<dd>
<p>This is the type of an object that can encode information about the
file position of a stream, for use by the functions <code class="code">fgetpos64</code> and
<code class="code">fsetpos64</code>.
</p>
<p>In the GNU C Library, <code class="code">fpos64_t</code> is an opaque data structure that
contains internal data to represent file offset and conversion state
information.  In other systems, it might have a different internal
representation.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fgetpos"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fgetpos</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, fpos_t *<var class="var">position</var>)</code><a class="copiable-link" href="#index-fgetpos"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function stores the value of the file position indicator for the
stream <var class="var">stream</var> in the <code class="code">fpos_t</code> object pointed to by
<var class="var">position</var>.  If successful, <code class="code">fgetpos</code> returns zero; otherwise
it returns a nonzero value and stores an implementation-defined positive
value in <code class="code">errno</code>.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit system the function is in fact <code class="code">fgetpos64</code>.  I.e., the LFS
interface transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fgetpos64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fgetpos64</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, fpos64_t *<var class="var">position</var>)</code><a class="copiable-link" href="#index-fgetpos64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">fgetpos</code> but the file position is
returned in a variable of type <code class="code">fpos64_t</code> to which <var class="var">position</var>
points.
</p>
<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a 32
bits machine this function is available under the name <code class="code">fgetpos</code>
and so transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fsetpos"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fsetpos</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, const fpos_t *<var class="var">position</var>)</code><a class="copiable-link" href="#index-fsetpos"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function sets the file position indicator for the stream <var class="var">stream</var>
to the position <var class="var">position</var>, which must have been set by a previous
call to <code class="code">fgetpos</code> on the same stream.  If successful, <code class="code">fsetpos</code>
clears the end-of-file indicator on the stream, discards any characters
that were &ldquo;pushed back&rdquo; by the use of <code class="code">ungetc</code>, and returns a value
of zero.  Otherwise, <code class="code">fsetpos</code> returns a nonzero value and stores
an implementation-defined positive value in <code class="code">errno</code>.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit system the function is in fact <code class="code">fsetpos64</code>.  I.e., the LFS
interface transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fsetpos64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fsetpos64</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, const fpos64_t *<var class="var">position</var>)</code><a class="copiable-link" href="#index-fsetpos64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">fsetpos</code> but the file position used
for positioning is provided in a variable of type <code class="code">fpos64_t</code> to
which <var class="var">position</var> points.
</p>
<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a 32
bits machine this function is available under the name <code class="code">fsetpos</code>
and so transparently replaces the old interface.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Stream-Buffering.html">Stream Buffering</a>, Previous: <a href="File-Positioning.html">File Positioning</a>, Up: <a href="I_002fO-on-Streams.html">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
