<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Argp Parsing State (The GNU C Library)</title>

<meta name="description" content="Argp Parsing State (The GNU C Library)">
<meta name="keywords" content="Argp Parsing State (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Argp-Parser-Functions.html" rel="up" title="Argp Parser Functions">
<link href="Argp-Helper-Functions.html" rel="next" title="Argp Helper Functions">
<link href="Argp-Special-Keys.html" rel="prev" title="Argp Special Keys">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Argp-Parsing-State">
<div class="nav-panel">
<p>
Next: <a href="Argp-Helper-Functions.html" accesskey="n" rel="next">Functions For Use in Argp Parsers</a>, Previous: <a href="Argp-Special-Keys.html" accesskey="p" rel="prev">Special Keys for Argp Parser Functions</a>, Up: <a href="Argp-Parser-Functions.html" accesskey="u" rel="up">Argp Parser Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Argp-Parsing-State-1"><span>26.3.5.2 Argp Parsing State<a class="copiable-link" href="#Argp-Parsing-State-1"> &para;</a></span></h4>

<p>The third argument to argp parser functions (see <a class="pxref" href="Argp-Parser-Functions.html">Argp Parser Functions</a>) is a pointer to a <code class="code">struct argp_state</code>, which contains
information about the state of the option parsing.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-argp_005fstate"><span class="category-def">Data Type: </span><span><strong class="def-name">struct argp_state</strong><a class="copiable-link" href="#index-struct-argp_005fstate"> &para;</a></span></dt>
<dd>
<p>This structure has the following fields, which may be modified as noted:
</p>
<dl class="table">
<dt><code class="code">const struct argp *const root_argp</code></dt>
<dd><p>The top level argp parser being parsed.  Note that this is often
<em class="emph">not</em> the same <code class="code">struct argp</code> passed into <code class="code">argp_parse</code> by
the invoking program.  See <a class="xref" href="Argp.html">Parsing Program Options with Argp</a>.  It is an internal argp parser that
contains options implemented by <code class="code">argp_parse</code> itself, such as
&lsquo;<samp class="samp">--help</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">int argc</code></dt>
<dt><code class="code">char **argv</code></dt>
<dd><p>The argument vector being parsed.  This may be modified.
</p>
</dd>
<dt><code class="code">int next</code></dt>
<dd><p>The index in <code class="code">argv</code> of the next argument to be parsed.  This may be
modified.
</p>
<p>One way to consume all remaining arguments in the input is to set
<code class="code"><var class="var">state</var>-&gt;next = <var class="var">state</var>-&gt;argc</code>, perhaps after recording
the value of the <code class="code">next</code> field to find the consumed arguments.  The
current option can be re-parsed immediately by decrementing this field,
then modifying <code class="code"><var class="var">state</var>-&gt;argv[<var class="var">state</var>-&gt;next]</code> to reflect
the option that should be reexamined.
</p>
</dd>
<dt><code class="code">unsigned flags</code></dt>
<dd><p>The flags supplied to <code class="code">argp_parse</code>.  These may be modified, although
some flags may only take effect when <code class="code">argp_parse</code> is first
invoked.  See <a class="xref" href="Argp-Flags.html">Flags for <code class="code">argp_parse</code></a>.
</p>
</dd>
<dt><code class="code">unsigned arg_num</code></dt>
<dd><p>While calling a parsing function with the <var class="var">key</var> argument
<code class="code">ARGP_KEY_ARG</code>, this represents the number of the current arg,
starting at 0.  It is incremented after each <code class="code">ARGP_KEY_ARG</code> call
returns.  At all other times, this is the number of <code class="code">ARGP_KEY_ARG</code>
arguments that have been processed.
</p>
</dd>
<dt><code class="code">int quoted</code></dt>
<dd><p>If non-zero, the index in <code class="code">argv</code> of the first argument following a
special &lsquo;<samp class="samp">--</samp>&rsquo; argument.  This prevents anything that follows from
being interpreted as an option.  It is only set after argument parsing
has proceeded past this point.
</p>
</dd>
<dt><code class="code">void *input</code></dt>
<dd><p>An arbitrary pointer passed in from the caller of <code class="code">argp_parse</code>, in
the <var class="var">input</var> argument.
</p>
</dd>
<dt><code class="code">void **child_inputs</code></dt>
<dd><p>These are values that will be passed to child parsers.  This vector will
be the same length as the number of children in the current parser.  Each
child parser will be given the value of
<code class="code"><var class="var">state</var>-&gt;child_inputs[<var class="var">i</var>]</code> as <em class="emph">its</em>
<code class="code"><var class="var">state</var>-&gt;input</code> field, where <var class="var">i</var> is the index of the child
in the this parser&rsquo;s <code class="code">children</code> field.  See <a class="xref" href="Argp-Children.html">Combining Multiple Argp Parsers</a>.
</p>
</dd>
<dt><code class="code">void *hook</code></dt>
<dd><p>For the parser function&rsquo;s use.  Initialized to 0, but otherwise ignored
by argp.
</p>
</dd>
<dt><code class="code">char *name</code></dt>
<dd><p>The name used when printing messages.  This is initialized to
<code class="code">argv[0]</code>, or <code class="code">program_invocation_name</code> if <code class="code">argv[0]</code> is
unavailable.
</p>
</dd>
<dt><code class="code">FILE *err_stream</code></dt>
<dt><code class="code">FILE *out_stream</code></dt>
<dd><p>The stdio streams used when argp prints.  Error messages are printed to
<code class="code">err_stream</code>, all other output, such as &lsquo;<samp class="samp">--help</samp>&rsquo; output) to
<code class="code">out_stream</code>.  These are initialized to <code class="code">stderr</code> and
<code class="code">stdout</code> respectively.  See <a class="xref" href="Standard-Streams.html">Standard Streams</a>.
</p>
</dd>
<dt><code class="code">void *pstate</code></dt>
<dd><p>Private, for use by the argp implementation.
</p></dd>
</dl>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Argp-Helper-Functions.html">Functions For Use in Argp Parsers</a>, Previous: <a href="Argp-Special-Keys.html">Special Keys for Argp Parser Functions</a>, Up: <a href="Argp-Parser-Functions.html">Argp Parser Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
