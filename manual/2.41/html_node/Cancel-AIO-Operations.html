<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Cancel AIO Operations (The GNU C Library)</title>

<meta name="description" content="Cancel AIO Operations (The GNU C Library)">
<meta name="keywords" content="Cancel AIO Operations (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Asynchronous-I_002fO.html" rel="up" title="Asynchronous I/O">
<link href="Configuration-of-AIO.html" rel="next" title="Configuration of AIO">
<link href="Synchronizing-AIO-Operations.html" rel="prev" title="Synchronizing AIO Operations">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Cancel-AIO-Operations">
<div class="nav-panel">
<p>
Next: <a href="Configuration-of-AIO.html" accesskey="n" rel="next">How to optimize the AIO implementation</a>, Previous: <a href="Synchronizing-AIO-Operations.html" accesskey="p" rel="prev">Getting into a Consistent State</a>, Up: <a href="Asynchronous-I_002fO.html" accesskey="u" rel="up">Perform I/O Operations in Parallel</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Cancellation-of-AIO-Operations"><span>13.11.4 Cancellation of AIO Operations<a class="copiable-link" href="#Cancellation-of-AIO-Operations"> &para;</a></span></h4>

<p>When one or more requests are asynchronously processed, it might be
useful in some situations to cancel a selected operation, e.g., if it
becomes obvious that the written data is no longer accurate and would
have to be overwritten soon.  As an example, assume an application, which
writes data in files in a situation where new incoming data would have
to be written in a file which will be updated by an enqueued request.
The POSIX AIO implementation provides such a function, but this function
is not capable of forcing the cancellation of the request.  It is up to the
implementation to decide whether it is possible to cancel the operation
or not.  Therefore using this function is merely a hint.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-aio_005fcancel"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">aio_cancel</strong> <code class="def-code-arguments">(int <var class="var">fildes</var>, struct aiocb *<var class="var">aiocbp</var>)</code><a class="copiable-link" href="#index-aio_005fcancel"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock heap
| AC-Unsafe lock mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">aio_cancel</code> function can be used to cancel one or more
outstanding requests.  If the <var class="var">aiocbp</var> parameter is <code class="code">NULL</code>, the
function tries to cancel all of the outstanding requests which would process
the file descriptor <var class="var">fildes</var> (i.e., whose <code class="code">aio_fildes</code> member
is <var class="var">fildes</var>).  If <var class="var">aiocbp</var> is not <code class="code">NULL</code>, <code class="code">aio_cancel</code>
attempts to cancel the specific request pointed to by <var class="var">aiocbp</var>.
</p>
<p>For requests which were successfully canceled, the normal notification
about the termination of the request should take place.  I.e., depending
on the <code class="code">struct sigevent</code> object which controls this, nothing
happens, a signal is sent or a thread is started.  If the request cannot
be canceled, it terminates the usual way after performing the operation.
</p>
<p>After a request is successfully canceled, a call to <code class="code">aio_error</code> with
a reference to this request as the parameter will return
<code class="code">ECANCELED</code> and a call to <code class="code">aio_return</code> will return <em class="math">-1</em>.
If the request wasn&rsquo;t canceled and is still running the error status is
still <code class="code">EINPROGRESS</code>.
</p>
<p>The return value of the function is <code class="code">AIO_CANCELED</code> if there were
requests which haven&rsquo;t terminated and which were successfully canceled.
If there is one or more requests left which couldn&rsquo;t be canceled, the
return value is <code class="code">AIO_NOTCANCELED</code>.  In this case <code class="code">aio_error</code>
must be used to find out which of the, perhaps multiple, requests (if
<var class="var">aiocbp</var> is <code class="code">NULL</code>) weren&rsquo;t successfully canceled.  If all
requests already terminated at the time <code class="code">aio_cancel</code> is called the
return value is <code class="code">AIO_ALLDONE</code>.
</p>
<p>If an error occurred during the execution of <code class="code">aio_cancel</code> the
function returns <em class="math">-1</em> and sets <code class="code">errno</code> to one of the following
values.
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The file descriptor <var class="var">fildes</var> is not valid.
</p></dd>
<dt><code class="code">ENOSYS</code></dt>
<dd><p><code class="code">aio_cancel</code> is not implemented.
</p></dd>
</dl>

<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code>, this
function is in fact <code class="code">aio_cancel64</code> since the LFS interface
transparently replaces the normal implementation.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-aio_005fcancel64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">aio_cancel64</strong> <code class="def-code-arguments">(int <var class="var">fildes</var>, struct aiocb64 *<var class="var">aiocbp</var>)</code><a class="copiable-link" href="#index-aio_005fcancel64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock heap
| AC-Unsafe lock mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">aio_cancel</code> with the only difference
that the argument is a reference to a variable of type <code class="code">struct
aiocb64</code>.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code>, this
function is available under the name <code class="code">aio_cancel</code> and so
transparently replaces the interface for small files on 32 bit
machines.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Configuration-of-AIO.html">How to optimize the AIO implementation</a>, Previous: <a href="Synchronizing-AIO-Operations.html">Getting into a Consistent State</a>, Up: <a href="Asynchronous-I_002fO.html">Perform I/O Operations in Parallel</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
