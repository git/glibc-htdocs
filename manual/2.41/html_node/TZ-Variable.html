<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>TZ Variable (The GNU C Library)</title>

<meta name="description" content="TZ Variable (The GNU C Library)">
<meta name="keywords" content="TZ Variable (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Calendar-Time.html" rel="up" title="Calendar Time">
<link href="Time-Zone-State.html" rel="next" title="Time Zone State">
<link href="Parsing-Date-and-Time.html" rel="prev" title="Parsing Date and Time">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="TZ-Variable">
<div class="nav-panel">
<p>
Next: <a href="Time-Zone-State.html" accesskey="n" rel="next">State Variables for Time Zones</a>, Previous: <a href="Parsing-Date-and-Time.html" accesskey="p" rel="prev">Convert textual time and date information back</a>, Up: <a href="Calendar-Time.html" accesskey="u" rel="up">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Specifying-the-Time-Zone-with-TZ"><span>22.5.6 Specifying the Time Zone with <code class="env">TZ</code><a class="copiable-link" href="#Specifying-the-Time-Zone-with-TZ"> &para;</a></span></h4>

<p>In POSIX systems, a user can specify the time zone by means of the
<code class="env">TZ</code> environment variable.  For information about how to set
environment variables, see <a class="ref" href="Environment-Variables.html">Environment Variables</a>.  The functions
for accessing the time zone are declared in <samp class="file">time.h</samp>.
<a class="index-entry-id" id="index-time_002eh-3"></a>
<a class="index-entry-id" id="index-time-zone"></a>
</p>
<p>You should not normally need to set <code class="env">TZ</code>.  If the system is
configured properly, the default time zone will be correct.  You might
set <code class="env">TZ</code> if you are using a computer over a network from a
different time zone, and would like times reported to you in the time
zone local to you, rather than what is local to the computer.
</p>
<p>The value of <code class="env">TZ</code> can be in one of the following formats:
</p>
<ul class="itemize mark-bullet">
<li>The <em class="dfn">geographical format</em> specifies a location that stands for
the past and future time zones observed in that location.
See <a class="xref" href="Geographical-TZ.html">Geographical Format for <code class="env">TZ</code></a>.
Here are some examples:

<div class="example smallexample">
<pre class="example-preformatted">Asia/Tokyo
America/New_York
/usr/share/zoneinfo/America/Nuuk
</pre></div>

</li><li>The <em class="dfn">proleptic format</em> represents a time zone that has always
been and always will be the same offset from UTC,
optionally with a simple daylight saving scheme that has always been
(and always will be) used every year.
See <a class="xref" href="Proleptic-TZ.html">Proleptic Format for <code class="env">TZ</code></a>.
Here are some examples:

<div class="example smallexample">
<pre class="example-preformatted">JST-9
EST+5EDT,M3.2.0/2,M11.1.0/2
&lt;-02&gt;+2&lt;-01&gt;,M3.5.0/-1,M10.5.0/0
</pre></div>

</li><li>The <em class="dfn">colon format</em> begins with &lsquo;<samp class="samp">:</samp>&rsquo;.  Here is an example.

<div class="example smallexample">
<pre class="example-preformatted">:/etc/localtime
</pre></div>

<p>Each operating system can interpret this format differently;
in the GNU C Library, the &lsquo;<samp class="samp">:</samp>&rsquo; is ignored and <var class="var">characters</var>
are treated as if they specified the geographical or proleptic format.
</p>
</li><li>As an extension to POSIX, when the value of <code class="env">TZ</code> is the empty string,
the GNU C Library uses UTC.
</li></ul>

<a class="index-entry-id" id="index-_002fetc_002flocaltime"></a>
<a class="index-entry-id" id="index-localtime-1"></a>
<p>If the <code class="env">TZ</code> environment variable does not have a value, the
implementation chooses a time zone by default.  In the GNU C Library, the
default time zone is like the specification &lsquo;<samp class="samp">TZ=/etc/localtime</samp>&rsquo;
(or &lsquo;<samp class="samp">TZ=/usr/local/etc/localtime</samp>&rsquo;, depending on how the GNU C Library
was configured; see <a class="pxref" href="Installation.html">Installing the GNU C Library</a>).  Other C libraries use their own
rule for choosing the default time zone, so there is little we can say
about them.
</p>

<ul class="mini-toc">
<li><a href="Geographical-TZ.html" accesskey="1">Geographical Format for <code class="env">TZ</code></a></li>
<li><a href="Proleptic-TZ.html" accesskey="2">Proleptic Format for <code class="env">TZ</code></a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Time-Zone-State.html">State Variables for Time Zones</a>, Previous: <a href="Parsing-Date-and-Time.html">Convert textual time and date information back</a>, Up: <a href="Calendar-Time.html">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
