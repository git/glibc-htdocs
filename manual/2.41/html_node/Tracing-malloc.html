<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Tracing malloc (The GNU C Library)</title>

<meta name="description" content="Tracing malloc (The GNU C Library)">
<meta name="keywords" content="Tracing malloc (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Allocation-Debugging.html" rel="up" title="Allocation Debugging">
<link href="Using-the-Memory-Debugger.html" rel="next" title="Using the Memory Debugger">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Tracing-malloc">
<div class="nav-panel">
<p>
Next: <a href="Using-the-Memory-Debugger.html" accesskey="n" rel="next">Example program excerpts</a>, Up: <a href="Allocation-Debugging.html" accesskey="u" rel="up">Allocation Debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="How-to-install-the-tracing-functionality"><span>3.2.4.1 How to install the tracing functionality<a class="copiable-link" href="#How-to-install-the-tracing-functionality"> &para;</a></span></h4>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mtrace"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">mtrace</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-mtrace"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe env race:mtrace init
| AS-Unsafe init heap corrupt lock
| AC-Unsafe init corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">mtrace</code> function provides a way to trace memory allocation
events in the program that calls it.  It is disabled by default in the
library and can be enabled by preloading the debugging library
<samp class="file">libc_malloc_debug</samp> using the <code class="code">LD_PRELOAD</code> environment
variable.
</p>
<p>When the <code class="code">mtrace</code> function is called it looks for an environment
variable named <code class="code">MALLOC_TRACE</code>.  This variable is supposed to
contain a valid file name.  The user must have write access.  If the
file already exists it is truncated.  If the environment variable is not
set or it does not name a valid file which can be opened for writing
nothing is done.  The behavior of <code class="code">malloc</code> etc. is not changed.
For obvious reasons this also happens if the application is installed
with the SUID or SGID bit set.
</p>
<p>If the named file is successfully opened, <code class="code">mtrace</code> installs special
handlers for the functions <code class="code">malloc</code>, <code class="code">realloc</code>, and
<code class="code">free</code>.  From then on, all uses of these functions are traced and
protocolled into the file.  There is now of course a speed penalty for all
calls to the traced functions so tracing should not be enabled during normal
use.
</p>
<p>This function is a GNU extension and generally not available on other
systems.  The prototype can be found in <samp class="file">mcheck.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-muntrace"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">muntrace</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-muntrace"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:mtrace locale
| AS-Unsafe corrupt heap
| AC-Unsafe corrupt mem lock fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">muntrace</code> function can be called after <code class="code">mtrace</code> was used
to enable tracing the <code class="code">malloc</code> calls.  If no (successful) call of
<code class="code">mtrace</code> was made <code class="code">muntrace</code> does nothing.
</p>
<p>Otherwise it deinstalls the handlers for <code class="code">malloc</code>, <code class="code">realloc</code>,
and <code class="code">free</code> and then closes the protocol file.  No calls are
protocolled anymore and the program runs again at full speed.
</p>
<p>This function is a GNU extension and generally not available on other
systems.  The prototype can be found in <samp class="file">mcheck.h</samp>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Using-the-Memory-Debugger.html">Example program excerpts</a>, Up: <a href="Allocation-Debugging.html">Allocation Debugging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
