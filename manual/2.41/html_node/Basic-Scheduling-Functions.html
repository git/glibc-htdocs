<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Basic Scheduling Functions (The GNU C Library)</title>

<meta name="description" content="Basic Scheduling Functions (The GNU C Library)">
<meta name="keywords" content="Basic Scheduling Functions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Priority.html" rel="up" title="Priority">
<link href="Extensible-Scheduling.html" rel="next" title="Extensible Scheduling">
<link href="Realtime-Scheduling.html" rel="prev" title="Realtime Scheduling">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Basic-Scheduling-Functions">
<div class="nav-panel">
<p>
Next: <a href="Extensible-Scheduling.html" accesskey="n" rel="next">Extensible Scheduling</a>, Previous: <a href="Realtime-Scheduling.html" accesskey="p" rel="prev">Realtime Scheduling</a>, Up: <a href="Priority.html" accesskey="u" rel="up">Process CPU Priority And Scheduling</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Basic-Scheduling-Functions-1"><span>23.3.3 Basic Scheduling Functions<a class="copiable-link" href="#Basic-Scheduling-Functions-1"> &para;</a></span></h4>

<p>This section describes functions in the GNU C Library for setting the
absolute priority and scheduling policy of a process.
</p>
<p><strong class="strong">Portability Note:</strong>  On systems that have the functions in this
section, the macro _POSIX_PRIORITY_SCHEDULING is defined in
<samp class="file">&lt;unistd.h&gt;</samp>.
</p>
<p>For the case that the scheduling policy is traditional scheduling, more
functions to fine tune the scheduling are in <a class="ref" href="Traditional-Scheduling.html">Traditional Scheduling</a>.
</p>
<p>Don&rsquo;t try to make too much out of the naming and structure of these
functions.  They don&rsquo;t match the concepts described in this manual
because the functions are as defined by POSIX.1b, but the implementation
on systems that use the GNU C Library is the inverse of what the POSIX
structure contemplates.  The POSIX scheme assumes that the primary
scheduling parameter is the scheduling policy and that the priority
value, if any, is a parameter of the scheduling policy.  In the
implementation, though, the priority value is king and the scheduling
policy, if anything, only fine tunes the effect of that priority.
</p>
<p>The symbols in this section are declared by including file <samp class="file">sched.h</samp>.
</p>
<p><strong class="strong">Portability Note:</strong> In POSIX, the <code class="code">pid_t</code> arguments of the
functions below refer to process IDs.  On Linux, they are actually
thread IDs, and control how specific threads are scheduled with
regards to the entire system.  The resulting behavior does not conform
to POSIX.  This is why the following description refers to tasks and
tasks IDs, and not processes and process IDs.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-sched_005fparam"><span class="category-def">Data Type: </span><span><strong class="def-name">struct sched_param</strong><a class="copiable-link" href="#index-struct-sched_005fparam"> &para;</a></span></dt>
<dd>
<p>This structure describes an absolute priority.
</p><dl class="table">
<dt><code class="code">int sched_priority</code></dt>
<dd><p>absolute priority value
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sched_005fsetscheduler"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sched_setscheduler</strong> <code class="def-code-arguments">(pid_t <var class="var">pid</var>, int <var class="var">policy</var>, const struct sched_param *<var class="var">param</var>)</code><a class="copiable-link" href="#index-sched_005fsetscheduler"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function sets both the absolute priority and the scheduling policy
for a task.
</p>
<p>It assigns the absolute priority value given by <var class="var">param</var> and the
scheduling policy <var class="var">policy</var> to the task with ID <var class="var">pid</var>,
or the calling task if <var class="var">pid</var> is zero.  If <var class="var">policy</var> is
negative, <code class="code">sched_setscheduler</code> keeps the existing scheduling policy.
</p>
<p>The following macros represent the valid values for <var class="var">policy</var>:
</p>
<dl class="vtable">
<dt><a id="index-SCHED_005fOTHER"></a><span><code class="code">SCHED_OTHER</code><a class="copiable-link" href="#index-SCHED_005fOTHER"> &para;</a></span></dt>
<dd><p>Traditional Scheduling
</p></dd>
<dt><a id="index-SCHED_005fFIFO"></a><span><code class="code">SCHED_FIFO</code><a class="copiable-link" href="#index-SCHED_005fFIFO"> &para;</a></span></dt>
<dd><p>First In First Out
</p></dd>
<dt><a id="index-SCHED_005fRR"></a><span><code class="code">SCHED_RR</code><a class="copiable-link" href="#index-SCHED_005fRR"> &para;</a></span></dt>
<dd><p>Round Robin
</p></dd>
</dl>


<p>On success, the return value is <code class="code">0</code>.  Otherwise, it is <code class="code">-1</code>
and <code class="code">ERRNO</code> is set accordingly.  The <code class="code">errno</code> values specific
to this function are:
</p>
<dl class="table">
<dt><code class="code">EPERM</code></dt>
<dd><ul class="itemize mark-bullet">
<li>The calling task does not have <code class="code">CAP_SYS_NICE</code> permission and
<var class="var">policy</var> is not <code class="code">SCHED_OTHER</code> (or it&rsquo;s negative and the
existing policy is not <code class="code">SCHED_OTHER</code>.

</li><li>The calling task does not have <code class="code">CAP_SYS_NICE</code> permission and its
owner is not the target task&rsquo;s owner.  I.e., the effective uid of the
calling task is neither the effective nor the real uid of task
<var class="var">pid</var>.
</li></ul>

</dd>
<dt><code class="code">ESRCH</code></dt>
<dd><p>There is no task with pid <var class="var">pid</var> and <var class="var">pid</var> is not zero.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><ul class="itemize mark-bullet">
<li><var class="var">policy</var> does not identify an existing scheduling policy.

</li><li>The absolute priority value identified by *<var class="var">param</var> is outside the
valid range for the scheduling policy <var class="var">policy</var> (or the existing
scheduling policy if <var class="var">policy</var> is negative) or <var class="var">param</var> is
null.  <code class="code">sched_get_priority_max</code> and <code class="code">sched_get_priority_min</code>
tell you what the valid range is.

</li><li><var class="var">pid</var> is negative.
</li></ul>
</dd>
</dl>

</dd></dl>


<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sched_005fgetscheduler"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sched_getscheduler</strong> <code class="def-code-arguments">(pid_t <var class="var">pid</var>)</code><a class="copiable-link" href="#index-sched_005fgetscheduler"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function returns the scheduling policy assigned to the task with
ID <var class="var">pid</var>, or the calling task if <var class="var">pid</var> is zero.
</p>
<p>The return value is the scheduling policy.  See
<code class="code">sched_setscheduler</code> for the possible values.
</p>
<p>If the function fails, the return value is instead <code class="code">-1</code> and
<code class="code">errno</code> is set accordingly.
</p>
<p>The <code class="code">errno</code> values specific to this function are:
</p>
<dl class="table">
<dt><code class="code">ESRCH</code></dt>
<dd><p>There is no task with pid <var class="var">pid</var> and it is not zero.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p><var class="var">pid</var> is negative.
</p>
</dd>
</dl>

<p>Note that this function is not an exact mate to <code class="code">sched_setscheduler</code>
because while that function sets the scheduling policy and the absolute
priority, this function gets only the scheduling policy.  To get the
absolute priority, use <code class="code">sched_getparam</code>.
</p>
</dd></dl>


<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sched_005fsetparam"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sched_setparam</strong> <code class="def-code-arguments">(pid_t <var class="var">pid</var>, const struct sched_param *<var class="var">param</var>)</code><a class="copiable-link" href="#index-sched_005fsetparam"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function sets a task&rsquo;s absolute priority.
</p>
<p>It is functionally identical to <code class="code">sched_setscheduler</code> with
<var class="var">policy</var> = <code class="code">-1</code>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sched_005fgetparam"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sched_getparam</strong> <code class="def-code-arguments">(pid_t <var class="var">pid</var>, struct sched_param *<var class="var">param</var>)</code><a class="copiable-link" href="#index-sched_005fgetparam"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function returns a task&rsquo;s absolute priority.
</p>
<p><var class="var">pid</var> is the task ID of the task whose absolute priority you want
to know.
</p>
<p><var class="var">param</var> is a pointer to a structure in which the function stores the
absolute priority of the task.
</p>
<p>On success, the return value is <code class="code">0</code>.  Otherwise, it is <code class="code">-1</code>
and <code class="code">errno</code> is set accordingly.  The <code class="code">errno</code> values specific
to this function are:
</p>
<dl class="table">
<dt><code class="code">ESRCH</code></dt>
<dd><p>There is no task with ID <var class="var">pid</var> and it is not zero.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p><var class="var">pid</var> is negative.
</p>
</dd>
</dl>

</dd></dl>


<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sched_005fget_005fpriority_005fmin"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sched_get_priority_min</strong> <code class="def-code-arguments">(int <var class="var">policy</var>)</code><a class="copiable-link" href="#index-sched_005fget_005fpriority_005fmin"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function returns the lowest absolute priority value that is
allowable for a task with scheduling policy <var class="var">policy</var>.
</p>
<p>On Linux, it is 0 for SCHED_OTHER and 1 for everything else.
</p>
<p>On success, the return value is <code class="code">0</code>.  Otherwise, it is <code class="code">-1</code>
and <code class="code">ERRNO</code> is set accordingly.  The <code class="code">errno</code> values specific
to this function are:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p><var class="var">policy</var> does not identify an existing scheduling policy.
</p></dd>
</dl>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sched_005fget_005fpriority_005fmax"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sched_get_priority_max</strong> <code class="def-code-arguments">(int <var class="var">policy</var>)</code><a class="copiable-link" href="#index-sched_005fget_005fpriority_005fmax"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function returns the highest absolute priority value that is
allowable for a task that with scheduling policy <var class="var">policy</var>.
</p>
<p>On Linux, it is 0 for SCHED_OTHER and 99 for everything else.
</p>
<p>On success, the return value is <code class="code">0</code>.  Otherwise, it is <code class="code">-1</code>
and <code class="code">ERRNO</code> is set accordingly.  The <code class="code">errno</code> values specific
to this function are:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p><var class="var">policy</var> does not identify an existing scheduling policy.
</p></dd>
</dl>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sched_005frr_005fget_005finterval"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sched_rr_get_interval</strong> <code class="def-code-arguments">(pid_t <var class="var">pid</var>, struct timespec *<var class="var">interval</var>)</code><a class="copiable-link" href="#index-sched_005frr_005fget_005finterval"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function returns the length of the quantum (time slice) used with
the Round Robin scheduling policy, if it is used, for the task with
task ID <var class="var">pid</var>.
</p>
<p>It returns the length of time as <var class="var">interval</var>.
</p>
<p>With a Linux kernel, the round robin time slice is always 150
microseconds, and <var class="var">pid</var> need not even be a real pid.
</p>
<p>The return value is <code class="code">0</code> on success and in the pathological case
that it fails, the return value is <code class="code">-1</code> and <code class="code">errno</code> is set
accordingly.  There is nothing specific that can go wrong with this
function, so there are no specific <code class="code">errno</code> values.
</p>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sched_005fyield"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sched_yield</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-sched_005fyield"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function voluntarily gives up the task&rsquo;s claim on the CPU.
</p>
<p>Technically, <code class="code">sched_yield</code> causes the calling task to be made
immediately ready to run (as opposed to running, which is what it was
before).  This means that if it has absolute priority higher than 0, it
gets pushed onto the tail of the queue of tasks that share its
absolute priority and are ready to run, and it will run again when its
turn next arrives.  If its absolute priority is 0, it is more
complicated, but still has the effect of yielding the CPU to other
tasks.
</p>
<p>If there are no other tasks that share the calling task&rsquo;s absolute
priority, this function doesn&rsquo;t have any effect.
</p>
<p>To the extent that the containing program is oblivious to what other
processes in the system are doing and how fast it executes, this
function appears as a no-op.
</p>
<p>The return value is <code class="code">0</code> on success and in the pathological case
that it fails, the return value is <code class="code">-1</code> and <code class="code">errno</code> is set
accordingly.  There is nothing specific that can go wrong with this
function, so there are no specific <code class="code">errno</code> values.
</p>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Extensible-Scheduling.html">Extensible Scheduling</a>, Previous: <a href="Realtime-Scheduling.html">Realtime Scheduling</a>, Up: <a href="Priority.html">Process CPU Priority And Scheduling</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
