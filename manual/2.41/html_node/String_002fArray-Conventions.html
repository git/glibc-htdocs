<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>String/Array Conventions (The GNU C Library)</title>

<meta name="description" content="String/Array Conventions (The GNU C Library)">
<meta name="keywords" content="String/Array Conventions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="String-and-Array-Utilities.html" rel="up" title="String and Array Utilities">
<link href="String-Length.html" rel="next" title="String Length">
<link href="Representation-of-Strings.html" rel="prev" title="Representation of Strings">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="String_002fArray-Conventions">
<div class="nav-panel">
<p>
Next: <a href="String-Length.html" accesskey="n" rel="next">String Length</a>, Previous: <a href="Representation-of-Strings.html" accesskey="p" rel="prev">Representation of Strings</a>, Up: <a href="String-and-Array-Utilities.html" accesskey="u" rel="up">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="String-and-Array-Conventions"><span>5.2 String and Array Conventions<a class="copiable-link" href="#String-and-Array-Conventions"> &para;</a></span></h3>

<p>This chapter describes both functions that work on arbitrary arrays or
blocks of memory, and functions that are specific to strings and wide
strings.
</p>
<p>Functions that operate on arbitrary blocks of memory have names
beginning with &lsquo;<samp class="samp">mem</samp>&rsquo; and &lsquo;<samp class="samp">wmem</samp>&rsquo; (such as <code class="code">memcpy</code> and
<code class="code">wmemcpy</code>) and invariably take an argument which specifies the size
(in bytes and wide characters respectively) of the block of memory to
operate on.  The array arguments and return values for these functions
have type <code class="code">void *</code> or <code class="code">wchar_t *</code>.  As a matter of style, the
elements of the arrays used with the &lsquo;<samp class="samp">mem</samp>&rsquo; functions are referred
to as &ldquo;bytes&rdquo;.  You can pass any kind of pointer to these functions,
and the <code class="code">sizeof</code> operator is useful in computing the value for the
size argument.  Parameters to the &lsquo;<samp class="samp">wmem</samp>&rsquo; functions must be of type
<code class="code">wchar_t *</code>.  These functions are not really usable with anything
but arrays of this type.
</p>
<p>In contrast, functions that operate specifically on strings and wide
strings have names beginning with &lsquo;<samp class="samp">str</samp>&rsquo; and &lsquo;<samp class="samp">wcs</samp>&rsquo;
respectively (such as <code class="code">strcpy</code> and <code class="code">wcscpy</code>) and look for a
terminating null byte or null wide character instead of requiring an explicit
size argument to be passed.  (Some of these functions accept a specified
maximum length, but they also check for premature termination.)
The array arguments and return values for these
functions have type <code class="code">char *</code> and <code class="code">wchar_t *</code> respectively, and
the array elements are referred to as &ldquo;bytes&rdquo; and &ldquo;wide
characters&rdquo;.
</p>
<p>In many cases, there are both &lsquo;<samp class="samp">mem</samp>&rsquo; and &lsquo;<samp class="samp">str</samp>&rsquo;/&lsquo;<samp class="samp">wcs</samp>&rsquo;
versions of a function.  The one that is more appropriate to use depends
on the exact situation.  When your program is manipulating arbitrary
arrays or blocks of storage, then you should always use the &lsquo;<samp class="samp">mem</samp>&rsquo;
functions.  On the other hand, when you are manipulating
strings it is usually more convenient to use the &lsquo;<samp class="samp">str</samp>&rsquo;/&lsquo;<samp class="samp">wcs</samp>&rsquo;
functions, unless you already know the length of the string in advance.
The &lsquo;<samp class="samp">wmem</samp>&rsquo; functions should be used for wide character arrays with
known size.
</p>
<a class="index-entry-id" id="index-wint_005ft"></a>
<a class="index-entry-id" id="index-parameter-promotion"></a>
<p>Some of the memory and string functions take single characters as
arguments.  Since a value of type <code class="code">char</code> is automatically promoted
into a value of type <code class="code">int</code> when used as a parameter, the functions
are declared with <code class="code">int</code> as the type of the parameter in question.
In case of the wide character functions the situation is similar: the
parameter type for a single wide character is <code class="code">wint_t</code> and not
<code class="code">wchar_t</code>.  This would for many implementations not be necessary
since <code class="code">wchar_t</code> is large enough to not be automatically
promoted, but since the ISO&nbsp;C<!-- /@w --> standard does not require such a
choice of types the <code class="code">wint_t</code> type is used.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="String-Length.html">String Length</a>, Previous: <a href="Representation-of-Strings.html">Representation of Strings</a>, Up: <a href="String-and-Array-Utilities.html">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
