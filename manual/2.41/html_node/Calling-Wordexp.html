<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Calling Wordexp (The GNU C Library)</title>

<meta name="description" content="Calling Wordexp (The GNU C Library)">
<meta name="keywords" content="Calling Wordexp (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Word-Expansion.html" rel="up" title="Word Expansion">
<link href="Flags-for-Wordexp.html" rel="next" title="Flags for Wordexp">
<link href="Expansion-Stages.html" rel="prev" title="Expansion Stages">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Calling-Wordexp">
<div class="nav-panel">
<p>
Next: <a href="Flags-for-Wordexp.html" accesskey="n" rel="next">Flags for Word Expansion</a>, Previous: <a href="Expansion-Stages.html" accesskey="p" rel="prev">The Stages of Word Expansion</a>, Up: <a href="Word-Expansion.html" accesskey="u" rel="up">Shell-Style Word Expansion</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Calling-wordexp"><span>10.4.2 Calling <code class="code">wordexp</code><a class="copiable-link" href="#Calling-wordexp"> &para;</a></span></h4>

<p>All the functions, constants and data types for word expansion are
declared in the header file <samp class="file">wordexp.h</samp>.
</p>
<p>Word expansion produces a vector of words (strings).  To return this
vector, <code class="code">wordexp</code> uses a special data type, <code class="code">wordexp_t</code>, which
is a structure.  You pass <code class="code">wordexp</code> the address of the structure,
and it fills in the structure&rsquo;s fields to tell you about the results.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-wordexp_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">wordexp_t</strong><a class="copiable-link" href="#index-wordexp_005ft"> &para;</a></span></dt>
<dd>
<p>This data type holds a pointer to a word vector.  More precisely, it
records both the address of the word vector and its size.
</p>
<dl class="table">
<dt><code class="code">we_wordc</code></dt>
<dd><p>The number of elements in the vector.
</p>
</dd>
<dt><code class="code">we_wordv</code></dt>
<dd><p>The address of the vector.  This field has type <code class="code">char&nbsp;**</code><!-- /@w -->.
</p>
</dd>
<dt><code class="code">we_offs</code></dt>
<dd><p>The offset of the first real element of the vector, from its nominal
address in the <code class="code">we_wordv</code> field.  Unlike the other fields, this
is always an input to <code class="code">wordexp</code>, rather than an output from it.
</p>
<p>If you use a nonzero offset, then that many elements at the beginning of
the vector are left empty.  (The <code class="code">wordexp</code> function fills them with
null pointers.)
</p>
<p>The <code class="code">we_offs</code> field is meaningful only if you use the
<code class="code">WRDE_DOOFFS</code> flag.  Otherwise, the offset is always zero
regardless of what is in this field, and the first real element comes at
the beginning of the vector.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wordexp"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">wordexp</strong> <code class="def-code-arguments">(const char *<var class="var">words</var>, wordexp_t *<var class="var">word-vector-ptr</var>, int <var class="var">flags</var>)</code><a class="copiable-link" href="#index-wordexp"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:utent const:env env sig:ALRM timer locale
| AS-Unsafe dlopen plugin i18n heap corrupt lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Perform word expansion on the string <var class="var">words</var>, putting the result in
a newly allocated vector, and store the size and address of this vector
into <code class="code">*<var class="var">word-vector-ptr</var></code>.  The argument <var class="var">flags</var> is a
combination of bit flags; see <a class="ref" href="Flags-for-Wordexp.html">Flags for Word Expansion</a>, for details of
the flags.
</p>
<p>You shouldn&rsquo;t use any of the characters &lsquo;<samp class="samp">|&amp;;&lt;&gt;</samp>&rsquo; in the string
<var class="var">words</var> unless they are quoted; likewise for newline.  If you use
these characters unquoted, you will get the <code class="code">WRDE_BADCHAR</code> error
code.  Don&rsquo;t use parentheses or braces unless they are quoted or part of
a word expansion construct.  If you use quotation characters &lsquo;<samp class="samp">'&quot;`</samp>&rsquo;,
they should come in pairs that balance.
</p>
<p>The results of word expansion are a sequence of words.  The function
<code class="code">wordexp</code> allocates a string for each resulting word, then
allocates a vector of type <code class="code">char **</code> to store the addresses of
these strings.  The last element of the vector is a null pointer.
This vector is called the <em class="dfn">word vector</em>.
</p>
<p>To return this vector, <code class="code">wordexp</code> stores both its address and its
length (number of elements, not counting the terminating null pointer)
into <code class="code">*<var class="var">word-vector-ptr</var></code>.
</p>
<p>If <code class="code">wordexp</code> succeeds, it returns 0.  Otherwise, it returns one
of these error codes:
</p>
<dl class="vtable">
<dt><a id="index-WRDE_005fBADCHAR"></a><span><code class="code">WRDE_BADCHAR</code><a class="copiable-link" href="#index-WRDE_005fBADCHAR"> &para;</a></span></dt>
<dd>
<p>The input string <var class="var">words</var> contains an unquoted invalid character such
as &lsquo;<samp class="samp">|</samp>&rsquo;.
</p>
</dd>
<dt><a id="index-WRDE_005fBADVAL"></a><span><code class="code">WRDE_BADVAL</code><a class="copiable-link" href="#index-WRDE_005fBADVAL"> &para;</a></span></dt>
<dd>
<p>The input string refers to an undefined shell variable, and you used the flag
<code class="code">WRDE_UNDEF</code> to forbid such references.
</p>
</dd>
<dt><a id="index-WRDE_005fCMDSUB"></a><span><code class="code">WRDE_CMDSUB</code><a class="copiable-link" href="#index-WRDE_005fCMDSUB"> &para;</a></span></dt>
<dd>
<p>The input string uses command substitution, and you used the flag
<code class="code">WRDE_NOCMD</code> to forbid command substitution.
</p>
</dd>
<dt><a id="index-WRDE_005fNOSPACE"></a><span><code class="code">WRDE_NOSPACE</code><a class="copiable-link" href="#index-WRDE_005fNOSPACE"> &para;</a></span></dt>
<dd>
<p>It was impossible to allocate memory to hold the result.  In this case,
<code class="code">wordexp</code> can store part of the results&mdash;as much as it could
allocate room for.
</p>
</dd>
<dt><a id="index-WRDE_005fSYNTAX"></a><span><code class="code">WRDE_SYNTAX</code><a class="copiable-link" href="#index-WRDE_005fSYNTAX"> &para;</a></span></dt>
<dd>
<p>There was a syntax error in the input string.  For example, an unmatched
quoting character is a syntax error.  This error code is also used to
signal division by zero and overflow in arithmetic expansion.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wordfree"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">wordfree</strong> <code class="def-code-arguments">(wordexp_t *<var class="var">word-vector-ptr</var>)</code><a class="copiable-link" href="#index-wordfree"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Free the storage used for the word-strings and vector that
<code class="code">*<var class="var">word-vector-ptr</var></code> points to.  This does not free the
structure <code class="code">*<var class="var">word-vector-ptr</var></code> itself&mdash;only the other
data it points to.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Flags-for-Wordexp.html">Flags for Word Expansion</a>, Previous: <a href="Expansion-Stages.html">The Stages of Word Expansion</a>, Up: <a href="Word-Expansion.html">Shell-Style Word Expansion</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
