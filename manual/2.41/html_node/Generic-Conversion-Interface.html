<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Generic Conversion Interface (The GNU C Library)</title>

<meta name="description" content="Generic Conversion Interface (The GNU C Library)">
<meta name="keywords" content="Generic Conversion Interface (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Generic-Charset-Conversion.html" rel="up" title="Generic Charset Conversion">
<link href="iconv-Examples.html" rel="next" title="iconv Examples">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Generic-Conversion-Interface">
<div class="nav-panel">
<p>
Next: <a href="iconv-Examples.html" accesskey="n" rel="next">A complete <code class="code">iconv</code> example</a>, Up: <a href="Generic-Charset-Conversion.html" accesskey="u" rel="up">Generic Charset Conversion</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Generic-Character-Set-Conversion-Interface"><span>6.5.1 Generic Character Set Conversion Interface<a class="copiable-link" href="#Generic-Character-Set-Conversion-Interface"> &para;</a></span></h4>

<p>This set of functions follows the traditional cycle of using a resource:
open&ndash;use&ndash;close.  The interface consists of three functions, each of
which implements one step.
</p>
<p>Before the interfaces are described it is necessary to introduce a
data type.  Just like other open&ndash;use&ndash;close interfaces the functions
introduced here work using handles and the <samp class="file">iconv.h</samp> header
defines a special type for the handles used.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-iconv_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">iconv_t</strong><a class="copiable-link" href="#index-iconv_005ft"> &para;</a></span></dt>
<dd>
<p>This data type is an abstract type defined in <samp class="file">iconv.h</samp>.  The user
must not assume anything about the definition of this type; it must be
completely opaque.
</p>
<p>Objects of this type can be assigned handles for the conversions using
the <code class="code">iconv</code> functions.  The objects themselves need not be freed, but
the conversions for which the handles stand for have to.
</p></dd></dl>

<p>The first step is the function to create a handle.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-iconv_005fopen"><span class="category-def">Function: </span><span><code class="def-type">iconv_t</code> <strong class="def-name">iconv_open</strong> <code class="def-code-arguments">(const char *<var class="var">tocode</var>, const char *<var class="var">fromcode</var>)</code><a class="copiable-link" href="#index-iconv_005fopen"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">iconv_open</code> function has to be used before starting a
conversion.  The two parameters this function takes determine the
source and destination character set for the conversion, and if the
implementation has the possibility to perform such a conversion, the
function returns a handle.
</p>
<p>If the wanted conversion is not available, the <code class="code">iconv_open</code> function
returns <code class="code">(iconv_t) -1</code>.  In this case the global variable
<code class="code">errno</code> can have the following values:
</p>
<dl class="table">
<dt><code class="code">EMFILE</code></dt>
<dd><p>The process already has <code class="code">OPEN_MAX</code> file descriptors open.
</p></dd>
<dt><code class="code">ENFILE</code></dt>
<dd><p>The system limit of open files is reached.
</p></dd>
<dt><code class="code">ENOMEM</code></dt>
<dd><p>Not enough memory to carry out the operation.
</p></dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>The conversion from <var class="var">fromcode</var> to <var class="var">tocode</var> is not supported.
</p></dd>
</dl>

<p>It is not possible to use the same descriptor in different threads to
perform independent conversions.  The data structures associated
with the descriptor include information about the conversion state.
This must not be messed up by using it in different conversions.
</p>
<p>An <code class="code">iconv</code> descriptor is like a file descriptor as for every use a
new descriptor must be created.  The descriptor does not stand for all
of the conversions from <var class="var">fromset</var> to <var class="var">toset</var>.
</p>
<p>The GNU C Library implementation of <code class="code">iconv_open</code> has one
significant extension to other implementations.  To ease the extension
of the set of available conversions, the implementation allows storing
the necessary files with data and code in an arbitrary number of
directories.  How this extension must be written will be explained below
(see <a class="pxref" href="glibc-iconv-Implementation.html">The <code class="code">iconv</code> Implementation in the GNU C Library</a>).  Here it is only important to say
that all directories mentioned in the <code class="code">GCONV_PATH</code> environment
variable are considered only if they contain a file <samp class="file">gconv-modules</samp>.
These directories need not necessarily be created by the system
administrator.  In fact, this extension is introduced to help users
writing and using their own, new conversions.  Of course, this does not
work for security reasons in SUID binaries; in this case only the system
directory is considered and this normally is
<samp class="file"><var class="var">prefix</var>/lib/gconv</samp>.  The <code class="code">GCONV_PATH</code> environment
variable is examined exactly once at the first call of the
<code class="code">iconv_open</code> function.  Later modifications of the variable have no
effect.
</p>
<a class="index-entry-id" id="index-iconv_002eh"></a>
<p>The <code class="code">iconv_open</code> function was introduced early in the X/Open
Portability Guide, version&nbsp;2<!-- /@w -->.  It is supported by all commercial
Unices as it is required for the Unix branding.  However, the quality and
completeness of the implementation varies widely.  The <code class="code">iconv_open</code>
function is declared in <samp class="file">iconv.h</samp>.
</p></dd></dl>

<p>The <code class="code">iconv</code> implementation can associate large data structure with
the handle returned by <code class="code">iconv_open</code>.  Therefore, it is crucial to
free all the resources once all conversions are carried out and the
conversion is not needed anymore.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-iconv_005fclose"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">iconv_close</strong> <code class="def-code-arguments">(iconv_t <var class="var">cd</var>)</code><a class="copiable-link" href="#index-iconv_005fclose"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">iconv_close</code> function frees all resources associated with the
handle <var class="var">cd</var>, which must have been returned by a successful call to
the <code class="code">iconv_open</code> function.
</p>
<p>If the function call was successful the return value is <em class="math">0</em>.
Otherwise it is <em class="math">-1</em> and <code class="code">errno</code> is set appropriately.
Defined errors are:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The conversion descriptor is invalid.
</p></dd>
</dl>

<a class="index-entry-id" id="index-iconv_002eh-1"></a>
<p>The <code class="code">iconv_close</code> function was introduced together with the rest
of the <code class="code">iconv</code> functions in XPG2 and is declared in <samp class="file">iconv.h</samp>.
</p></dd></dl>

<p>The standard defines only one actual conversion function.  This has,
therefore, the most general interface: it allows conversion from one
buffer to another.  Conversion from a file to a buffer, vice versa, or
even file to file can be implemented on top of it.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-iconv-1"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">iconv</strong> <code class="def-code-arguments">(iconv_t <var class="var">cd</var>, char **<var class="var">inbuf</var>, size_t *<var class="var">inbytesleft</var>, char **<var class="var">outbuf</var>, size_t *<var class="var">outbytesleft</var>)</code><a class="copiable-link" href="#index-iconv-1"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:cd
| AS-Safe 
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<a class="index-entry-id" id="index-stateful-3"></a>
<p>The <code class="code">iconv</code> function converts the text in the input buffer
according to the rules associated with the descriptor <var class="var">cd</var> and
stores the result in the output buffer.  It is possible to call the
function for the same text several times in a row since for stateful
character sets the necessary state information is kept in the data
structures associated with the descriptor.
</p>
<p>The input buffer is specified by <code class="code">*<var class="var">inbuf</var></code> and it contains
<code class="code">*<var class="var">inbytesleft</var></code> bytes.  The extra indirection is necessary for
communicating the used input back to the caller (see below).  It is
important to note that the buffer pointer is of type <code class="code">char</code> and the
length is measured in bytes even if the input text is encoded in wide
characters.
</p>
<p>The output buffer is specified in a similar way.  <code class="code">*<var class="var">outbuf</var></code>
points to the beginning of the buffer with at least
<code class="code">*<var class="var">outbytesleft</var></code> bytes room for the result.  The buffer
pointer again is of type <code class="code">char</code> and the length is measured in
bytes.  If <var class="var">outbuf</var> or <code class="code">*<var class="var">outbuf</var></code> is a null pointer, the
conversion is performed but no output is available.
</p>
<p>If <var class="var">inbuf</var> is a null pointer, the <code class="code">iconv</code> function performs the
necessary action to put the state of the conversion into the initial
state.  This is obviously a no-op for non-stateful encodings, but if the
encoding has a state, such a function call might put some byte sequences
in the output buffer, which perform the necessary state changes.  The
next call with <var class="var">inbuf</var> not being a null pointer then simply goes on
from the initial state.  It is important that the programmer never makes
any assumption as to whether the conversion has to deal with states.
Even if the input and output character sets are not stateful, the
implementation might still have to keep states.  This is due to the
implementation chosen for the GNU C Library as it is described below.
Therefore an <code class="code">iconv</code> call to reset the state should always be
performed if some protocol requires this for the output text.
</p>
<p>The conversion stops for one of three reasons.  The first is that all
characters from the input buffer are converted.  This actually can mean
two things: either all bytes from the input buffer are consumed or
there are some bytes at the end of the buffer that possibly can form a
complete character but the input is incomplete.  The second reason for a
stop is that the output buffer is full.  And the third reason is that
the input contains invalid characters.
</p>
<p>In all of these cases the buffer pointers after the last successful
conversion, for the input and output buffers, are stored in <var class="var">inbuf</var> and
<var class="var">outbuf</var>, and the available room in each buffer is stored in
<var class="var">inbytesleft</var> and <var class="var">outbytesleft</var>.
</p>
<p>Since the character sets selected in the <code class="code">iconv_open</code> call can be
almost arbitrary, there can be situations where the input buffer contains
valid characters, which have no identical representation in the output
character set.  The behavior in this situation is undefined.  The
<em class="emph">current</em> behavior of the GNU C Library in this situation is to
return with an error immediately.  This certainly is not the most
desirable solution; therefore, future versions will provide better ones,
but they are not yet finished.
</p>
<p>If all input from the input buffer is successfully converted and stored
in the output buffer, the function returns the number of non-reversible
conversions performed.  In all other cases the return value is
<code class="code">(size_t) -1</code> and <code class="code">errno</code> is set appropriately.  In such cases
the value pointed to by <var class="var">inbytesleft</var> is nonzero.
</p>
<dl class="table">
<dt><code class="code">EILSEQ</code></dt>
<dd><p>The conversion stopped because of an invalid byte sequence in the input.
After the call, <code class="code">*<var class="var">inbuf</var></code> points at the first byte of the
invalid byte sequence.
</p>
</dd>
<dt><code class="code">E2BIG</code></dt>
<dd><p>The conversion stopped because it ran out of space in the output buffer.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>The conversion stopped because of an incomplete byte sequence at the end
of the input buffer.
</p>
</dd>
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">cd</var> argument is invalid.
</p></dd>
</dl>

<a class="index-entry-id" id="index-iconv_002eh-2"></a>
<p>The <code class="code">iconv</code> function was introduced in the XPG2 standard and is
declared in the <samp class="file">iconv.h</samp> header.
</p></dd></dl>

<p>The definition of the <code class="code">iconv</code> function is quite good overall.  It
provides quite flexible functionality.  The only problems lie in the
boundary cases, which are incomplete byte sequences at the end of the
input buffer and invalid input.  A third problem, which is not really
a design problem, is the way conversions are selected.  The standard
does not say anything about the legitimate names, a minimal set of
available conversions.  We will see how this negatively impacts other
implementations, as demonstrated below.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="iconv-Examples.html">A complete <code class="code">iconv</code> example</a>, Up: <a href="Generic-Charset-Conversion.html">Generic Charset Conversion</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
