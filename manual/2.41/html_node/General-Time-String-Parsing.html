<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>General Time String Parsing (The GNU C Library)</title>

<meta name="description" content="General Time String Parsing (The GNU C Library)">
<meta name="keywords" content="General Time String Parsing (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Parsing-Date-and-Time.html" rel="up" title="Parsing Date and Time">
<link href="Low_002dLevel-Time-String-Parsing.html" rel="prev" title="Low-Level Time String Parsing">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="General-Time-String-Parsing">
<div class="nav-panel">
<p>
Previous: <a href="Low_002dLevel-Time-String-Parsing.html" accesskey="p" rel="prev">Interpret string according to given format</a>, Up: <a href="Parsing-Date-and-Time.html" accesskey="u" rel="up">Convert textual time and date information back</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="A-More-User_002dfriendly-Way-to-Parse-Times-and-Dates"><span>22.5.5.2 A More User-friendly Way to Parse Times and Dates<a class="copiable-link" href="#A-More-User_002dfriendly-Way-to-Parse-Times-and-Dates"> &para;</a></span></h4>

<p>The Unix standard defines another function for parsing date strings.
The interface is weird, but if the function happens to suit your
application it is just fine.  It is problematic to use this function
in multi-threaded programs or libraries, since it returns a pointer to
a static variable, and uses a global variable and global state based
on an environment variable.
</p>
<dl class="first-defvr first-defvar-alias-first-defvr">
<dt class="defvr defvar-alias-defvr" id="index-getdate_005ferr"><span class="category-def">Variable: </span><span><strong class="def-name">getdate_err</strong><a class="copiable-link" href="#index-getdate_005ferr"> &para;</a></span></dt>
<dd>
<p>This variable of type <code class="code">int</code> contains the error code of the last
unsuccessful call to <code class="code">getdate</code>.  Defined values are:
</p>
<dl class="table">
<dt><em class="math">1</em></dt>
<dd><p>The environment variable <code class="env">DATEMSK</code> is not defined or null.
</p></dd>
<dt><em class="math">2</em></dt>
<dd><p>The template file denoted by the <code class="env">DATEMSK</code> environment variable
cannot be opened.
</p></dd>
<dt><em class="math">3</em></dt>
<dd><p>Information about the template file cannot retrieved.
</p></dd>
<dt><em class="math">4</em></dt>
<dd><p>The template file is not a regular file.
</p></dd>
<dt><em class="math">5</em></dt>
<dd><p>An I/O error occurred while reading the template file.
</p></dd>
<dt><em class="math">6</em></dt>
<dd><p>Not enough memory available to execute the function.
</p></dd>
<dt><em class="math">7</em></dt>
<dd><p>The template file contains no matching template.
</p></dd>
<dt><em class="math">8</em></dt>
<dd><p>The input date is invalid, but would match a template otherwise.  This
includes dates like February 31st, and dates which cannot be represented
in a <code class="code">time_t</code> variable.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getdate"><span class="category-def">Function: </span><span><code class="def-type">struct tm *</code> <strong class="def-name">getdate</strong> <code class="def-code-arguments">(const char *<var class="var">string</var>)</code><a class="copiable-link" href="#index-getdate"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:getdate env locale
| AS-Unsafe heap lock
| AC-Unsafe lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The interface to <code class="code">getdate</code> is the simplest possible for a function
to parse a string and return the value.  <var class="var">string</var> is the input
string and the result is returned in a statically-allocated variable.
</p>
<p>The details about how the string is processed are hidden from the user.
In fact, they can be outside the control of the program.  Which formats
are recognized is controlled by the file named by the environment
variable <code class="env">DATEMSK</code>.  This file should contain
lines of valid format strings which could be passed to <code class="code">strptime</code>.
</p>
<p>The <code class="code">getdate</code> function reads these format strings one after the
other and tries to match the input string.  The first line which
completely matches the input string is used.
</p>
<p>Elements not initialized through the format string retain the values
present at the time of the <code class="code">getdate</code> function call.
</p>
<p>The formats recognized by <code class="code">getdate</code> are the same as for
<code class="code">strptime</code>.  See above for an explanation.  There are only a few
extensions to the <code class="code">strptime</code> behavior:
</p>
<ul class="itemize mark-bullet">
<li>If the <code class="code">%Z</code> format is given the broken-down time is based on the
current time of the time zone matched, not of the current time zone of the
runtime environment.

<p><em class="emph">Note</em>: This is not implemented (currently).  The problem is that
time zone abbreviations are not unique.  If a fixed time zone is assumed for a
given string (say <code class="code">EST</code> meaning US East Coast time), then uses for
countries other than the USA will fail.  So far we have found no good
solution to this.
</p>
</li><li>If only the weekday is specified the selected day depends on the current
date.  If the current weekday is greater than or equal to the <code class="code">tm_wday</code>
value the current week&rsquo;s day is chosen, otherwise the day next week is chosen.

</li><li>A similar heuristic is used when only the month is given and not the
year.  If the month is greater than or equal to the current month, then
the current year is used.  Otherwise it wraps to next year.  The first
day of the month is assumed if one is not explicitly specified.

</li><li>The current hour, minute, and second are used if the appropriate value is
not set through the format.

</li><li>If no date is given tomorrow&rsquo;s date is used if the time is
smaller than the current time.  Otherwise today&rsquo;s date is taken.
</li></ul>

<p>It should be noted that the format in the template file need not only
contain format elements.  The following is a list of possible format
strings (taken from the Unix standard):
</p>
<div class="example smallexample">
<pre class="example-preformatted">%m
%A %B %d, %Y %H:%M:%S
%A
%B
%m/%d/%y %I %p
%d,%m,%Y %H:%M
at %A the %dst of %B in %Y
run job at %I %p,%B %dnd
%A den %d. %B %Y %H.%M Uhr
</pre></div>

<p>As you can see, the template list can contain very specific strings like
<code class="code">run job at %I %p,%B %dnd</code>.  Using the above list of templates and
assuming the current time is Mon Sep 22 12:19:47 EDT 1986, we can obtain the
following results for the given input.
</p>
<table class="multitable">
<tbody><tr><td>Input</td><td>Match</td><td>Result</td></tr>
<tr><td>Mon</td><td>%a</td><td>Mon Sep 22 12:19:47 EDT 1986</td></tr>
<tr><td>Sun</td><td>%a</td><td>Sun Sep 28 12:19:47 EDT 1986</td></tr>
<tr><td>Fri</td><td>%a</td><td>Fri Sep 26 12:19:47 EDT 1986</td></tr>
<tr><td>September</td><td>%B</td><td>Mon Sep 1 12:19:47 EDT 1986</td></tr>
<tr><td>January</td><td>%B</td><td>Thu Jan 1 12:19:47 EST 1987</td></tr>
<tr><td>December</td><td>%B</td><td>Mon Dec 1 12:19:47 EST 1986</td></tr>
<tr><td>Sep Mon</td><td>%b %a</td><td>Mon Sep 1 12:19:47 EDT 1986</td></tr>
<tr><td>Jan Fri</td><td>%b %a</td><td>Fri Jan 2 12:19:47 EST 1987</td></tr>
<tr><td>Dec Mon</td><td>%b %a</td><td>Mon Dec 1 12:19:47 EST 1986</td></tr>
<tr><td>Jan Wed 1989</td><td>%b %a %Y</td><td>Wed Jan 4 12:19:47 EST 1989</td></tr>
<tr><td>Fri 9</td><td>%a %H</td><td>Fri Sep 26 09:00:00 EDT 1986</td></tr>
<tr><td>Feb 10:30</td><td>%b %H:%S</td><td>Sun Feb 1 10:00:30 EST 1987</td></tr>
<tr><td>10:30</td><td>%H:%M</td><td>Tue Sep 23 10:30:00 EDT 1986</td></tr>
<tr><td>13:30</td><td>%H:%M</td><td>Mon Sep 22 13:30:00 EDT 1986</td></tr>
</tbody>
</table>

<p>The return value of the function is a pointer to a static variable of
type <code class="code">struct&nbsp;tm</code><!-- /@w -->, or a null pointer if an error occurred.  The
result is only valid until the next <code class="code">getdate</code> call, making this
function unusable in multi-threaded applications.
</p>
<p>The <code class="code">errno</code> variable is <em class="emph">not</em> changed.  Error conditions are
stored in the global variable <code class="code">getdate_err</code>.  See the
description above for a list of the possible error values.
</p>
<p><em class="emph">Warning:</em> The <code class="code">getdate</code> function should <em class="emph">never</em> be
used in SUID-programs.  The reason is obvious: using the
<code class="env">DATEMSK</code> environment variable you can get the function to open
any arbitrary file and chances are high that with some bogus input
(such as a binary file) the program will crash.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getdate_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getdate_r</strong> <code class="def-code-arguments">(const char *<var class="var">string</var>, struct tm *<var class="var">tp</var>)</code><a class="copiable-link" href="#index-getdate_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env locale
| AS-Unsafe heap lock
| AC-Unsafe lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getdate_r</code> function is the reentrant counterpart of
<code class="code">getdate</code>.  It does not use the global variable <code class="code">getdate_err</code>
to signal an error, but instead returns an error code.  The same error
codes as described in the <code class="code">getdate_err</code> documentation above are
used, with 0 meaning success.
</p>
<p>Moreover, <code class="code">getdate_r</code> stores the broken-down time in the variable
of type <code class="code">struct tm</code> pointed to by the second argument, rather than
in a static variable.
</p>
<p>This function is not defined in the Unix standard.  Nevertheless it is
available on some other Unix systems as well.
</p>
<p>The warning against using <code class="code">getdate</code> in SUID-programs applies to
<code class="code">getdate_r</code> as well.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Low_002dLevel-Time-String-Parsing.html">Interpret string according to given format</a>, Up: <a href="Parsing-Date-and-Time.html">Convert textual time and date information back</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
