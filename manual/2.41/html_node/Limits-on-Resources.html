<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Limits on Resources (The GNU C Library)</title>

<meta name="description" content="Limits on Resources (The GNU C Library)">
<meta name="keywords" content="Limits on Resources (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Resource-Usage-And-Limitation.html" rel="up" title="Resource Usage And Limitation">
<link href="Priority.html" rel="next" title="Priority">
<link href="Resource-Usage.html" rel="prev" title="Resource Usage">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Limits-on-Resources">
<div class="nav-panel">
<p>
Next: <a href="Priority.html" accesskey="n" rel="next">Process CPU Priority And Scheduling</a>, Previous: <a href="Resource-Usage.html" accesskey="p" rel="prev">Resource Usage</a>, Up: <a href="Resource-Usage-And-Limitation.html" accesskey="u" rel="up">Resource Usage And Limitation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Limiting-Resource-Usage"><span>23.2 Limiting Resource Usage<a class="copiable-link" href="#Limiting-Resource-Usage"> &para;</a></span></h3>
<a class="index-entry-id" id="index-resource-limits"></a>
<a class="index-entry-id" id="index-limits-on-resource-usage"></a>
<a class="index-entry-id" id="index-usage-limits"></a>

<p>You can specify limits for the resource usage of a process.  When the
process tries to exceed a limit, it may get a signal, or the system call
by which it tried to do so may fail, depending on the resource.  Each
process initially inherits its limit values from its parent, but it can
subsequently change them.
</p>
<p>There are two per-process limits associated with a resource:
<a class="index-entry-id" id="index-limit"></a>
</p>
<dl class="table">
<dt><em class="dfn">current limit</em></dt>
<dd><p>The current limit is the value the system will not allow usage to
exceed.  It is also called the &ldquo;soft limit&rdquo; because the process being
limited can generally raise the current limit at will.
<a class="index-entry-id" id="index-current-limit"></a>
<a class="index-entry-id" id="index-soft-limit"></a>
</p>
</dd>
<dt><em class="dfn">maximum limit</em></dt>
<dd><p>The maximum limit is the maximum value to which a process is allowed to
set its current limit.  It is also called the &ldquo;hard limit&rdquo; because
there is no way for a process to get around it.  A process may lower
its own maximum limit, but only the superuser may increase a maximum
limit.
<a class="index-entry-id" id="index-maximum-limit"></a>
<a class="index-entry-id" id="index-hard-limit"></a>
</p></dd>
</dl>

<a class="index-entry-id" id="index-sys_002fresource_002eh-1"></a>
<p>The symbols for use with <code class="code">getrlimit</code>, <code class="code">setrlimit</code>,
<code class="code">getrlimit64</code>, and <code class="code">setrlimit64</code> are defined in
<samp class="file">sys/resource.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getrlimit"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getrlimit</strong> <code class="def-code-arguments">(int <var class="var">resource</var>, struct rlimit *<var class="var">rlp</var>)</code><a class="copiable-link" href="#index-getrlimit"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Read the current and maximum limits for the resource <var class="var">resource</var>
and store them in <code class="code">*<var class="var">rlp</var></code>.
</p>
<p>The return value is <code class="code">0</code> on success and <code class="code">-1</code> on failure.  The
only possible <code class="code">errno</code> error condition is <code class="code">EFAULT</code>.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32-bit system this function is in fact <code class="code">getrlimit64</code>.  Thus, the
LFS interface transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getrlimit64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getrlimit64</strong> <code class="def-code-arguments">(int <var class="var">resource</var>, struct rlimit64 *<var class="var">rlp</var>)</code><a class="copiable-link" href="#index-getrlimit64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">getrlimit</code> but its second parameter is
a pointer to a variable of type <code class="code">struct rlimit64</code>, which allows it
to read values which wouldn&rsquo;t fit in the member of a <code class="code">struct
rlimit</code>.
</p>
<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32-bit machine, this function is available under the name
<code class="code">getrlimit</code> and so transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setrlimit"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">setrlimit</strong> <code class="def-code-arguments">(int <var class="var">resource</var>, const struct rlimit *<var class="var">rlp</var>)</code><a class="copiable-link" href="#index-setrlimit"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Change the current and maximum limits of the process for the resource
<var class="var">resource</var> to the values provided in <code class="code">*<var class="var">rlp</var></code>.
</p>
<p>The return value is <code class="code">0</code> on success and <code class="code">-1</code> on failure.  The
following <code class="code">errno</code> error condition is possible:
</p>
<dl class="table">
<dt><code class="code">EPERM</code></dt>
<dd><ul class="itemize mark-bullet">
<li>The process tried to raise a current limit beyond the maximum limit.

</li><li>The process tried to raise a maximum limit, but is not superuser.
</li></ul>
</dd>
</dl>

<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32-bit system this function is in fact <code class="code">setrlimit64</code>.  Thus, the
LFS interface transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setrlimit64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">setrlimit64</strong> <code class="def-code-arguments">(int <var class="var">resource</var>, const struct rlimit64 *<var class="var">rlp</var>)</code><a class="copiable-link" href="#index-setrlimit64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">setrlimit</code> but its second parameter is
a pointer to a variable of type <code class="code">struct rlimit64</code> which allows it
to set values which wouldn&rsquo;t fit in the member of a <code class="code">struct
rlimit</code>.
</p>
<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32-bit machine this function is available under the name
<code class="code">setrlimit</code> and so transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-struct-rlimit"><span class="category-def">Data Type: </span><span><strong class="def-name">struct rlimit</strong><a class="copiable-link" href="#index-struct-rlimit"> &para;</a></span></dt>
<dd>
<p>This structure is used with <code class="code">getrlimit</code> to receive limit values,
and with <code class="code">setrlimit</code> to specify limit values for a particular process
and resource.  It has two fields:
</p>
<dl class="table">
<dt><code class="code">rlim_t rlim_cur</code></dt>
<dd><p>The current limit
</p>
</dd>
<dt><code class="code">rlim_t rlim_max</code></dt>
<dd><p>The maximum limit.
</p></dd>
</dl>

<p>For <code class="code">getrlimit</code>, the structure is an output; it receives the current
values.  For <code class="code">setrlimit</code>, it specifies the new values.
</p></dd></dl>

<p>For the LFS functions a similar type is defined in <samp class="file">sys/resource.h</samp>.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-rlimit64"><span class="category-def">Data Type: </span><span><strong class="def-name">struct rlimit64</strong><a class="copiable-link" href="#index-struct-rlimit64"> &para;</a></span></dt>
<dd>
<p>This structure is analogous to the <code class="code">rlimit</code> structure above, but
its components have wider ranges.  It has two fields:
</p>
<dl class="table">
<dt><code class="code">rlim64_t rlim_cur</code></dt>
<dd><p>This is analogous to <code class="code">rlimit.rlim_cur</code>, but with a different type.
</p>
</dd>
<dt><code class="code">rlim64_t rlim_max</code></dt>
<dd><p>This is analogous to <code class="code">rlimit.rlim_max</code>, but with a different type.
</p></dd>
</dl>

</dd></dl>

<p>Here is a list of resources for which you can specify a limit.  Memory
and file sizes are measured in bytes.
</p>
<dl class="vtable">
<dt><a id="index-RLIMIT_005fCPU"></a><span><code class="code">RLIMIT_CPU</code><a class="copiable-link" href="#index-RLIMIT_005fCPU"> &para;</a></span></dt>
<dd>
<p>The maximum amount of CPU time the process can use.  If it runs for
longer than this, it gets a signal: <code class="code">SIGXCPU</code>.  The value is
measured in seconds.  See <a class="xref" href="Operation-Error-Signals.html">Operation Error Signals</a>.
</p>
</dd>
<dt><a id="index-RLIMIT_005fFSIZE"></a><span><code class="code">RLIMIT_FSIZE</code><a class="copiable-link" href="#index-RLIMIT_005fFSIZE"> &para;</a></span></dt>
<dd>
<p>The maximum size of file the process can create.  Trying to write a
larger file causes a signal: <code class="code">SIGXFSZ</code>.  See <a class="xref" href="Operation-Error-Signals.html">Operation Error Signals</a>.
</p>
</dd>
<dt><a id="index-RLIMIT_005fDATA"></a><span><code class="code">RLIMIT_DATA</code><a class="copiable-link" href="#index-RLIMIT_005fDATA"> &para;</a></span></dt>
<dd>
<p>The maximum size of data memory for the process.  If the process tries
to allocate data memory beyond this amount, the allocation function
fails.
</p>
</dd>
<dt><a id="index-RLIMIT_005fSTACK"></a><span><code class="code">RLIMIT_STACK</code><a class="copiable-link" href="#index-RLIMIT_005fSTACK"> &para;</a></span></dt>
<dd>
<p>The maximum stack size for the process.  If the process tries to extend
its stack past this size, it gets a <code class="code">SIGSEGV</code> signal.
See <a class="xref" href="Program-Error-Signals.html">Program Error Signals</a>.
</p>
</dd>
<dt><a id="index-RLIMIT_005fCORE"></a><span><code class="code">RLIMIT_CORE</code><a class="copiable-link" href="#index-RLIMIT_005fCORE"> &para;</a></span></dt>
<dd>
<p>The maximum size core file that this process can create.  If the process
terminates and would dump a core file larger than this, then no core
file is created.  So setting this limit to zero prevents core files from
ever being created.
</p>
</dd>
<dt><a id="index-RLIMIT_005fRSS"></a><span><code class="code">RLIMIT_RSS</code><a class="copiable-link" href="#index-RLIMIT_005fRSS"> &para;</a></span></dt>
<dd>
<p>The maximum amount of physical memory that this process should get.
This parameter is a guide for the system&rsquo;s scheduler and memory
allocator; the system may give the process more memory when there is a
surplus.
</p>
</dd>
<dt><a id="index-RLIMIT_005fMEMLOCK"></a><span><code class="code">RLIMIT_MEMLOCK</code><a class="copiable-link" href="#index-RLIMIT_005fMEMLOCK"> &para;</a></span></dt>
<dd>
<p>The maximum amount of memory that can be locked into physical memory (so
it will never be paged out).
</p>
</dd>
<dt><a id="index-RLIMIT_005fNPROC"></a><span><code class="code">RLIMIT_NPROC</code><a class="copiable-link" href="#index-RLIMIT_005fNPROC"> &para;</a></span></dt>
<dd>
<p>The maximum number of processes that can be created with the same user ID.
If you have reached the limit for your user ID, <code class="code">fork</code> will fail
with <code class="code">EAGAIN</code>.  See <a class="xref" href="Creating-a-Process.html">Creating a Process</a>.
</p>
</dd>
<dt><a id="index-RLIMIT_005fNOFILE"></a><span><code class="code">RLIMIT_NOFILE</code><a class="copiable-link" href="#index-RLIMIT_005fNOFILE"> &para;</a></span></dt>
<dt><a id="index-RLIMIT_005fOFILE"></a><span><code class="code">RLIMIT_OFILE</code><a class="copiable-link" href="#index-RLIMIT_005fOFILE"> &para;</a></span></dt>
<dd>
<p>The maximum number of files that the process can open.  If it tries to
open more files than this, its open attempt fails with <code class="code">errno</code>
<code class="code">EMFILE</code>.  See <a class="xref" href="Error-Codes.html">Error Codes</a>.  Not all systems support this limit;
GNU does, and 4.4 BSD does.
</p>
</dd>
<dt><a id="index-RLIMIT_005fAS"></a><span><code class="code">RLIMIT_AS</code><a class="copiable-link" href="#index-RLIMIT_005fAS"> &para;</a></span></dt>
<dd>
<p>The maximum size of total memory that this process should get.  If the
process tries to allocate more memory beyond this amount with, for
example, <code class="code">brk</code>, <code class="code">malloc</code>, <code class="code">mmap</code> or <code class="code">sbrk</code>, the
allocation function fails.
</p>
</dd>
<dt><a id="index-RLIM_005fNLIMITS"></a><span><code class="code">RLIM_NLIMITS</code><a class="copiable-link" href="#index-RLIM_005fNLIMITS"> &para;</a></span></dt>
<dd>
<p>The number of different resource limits.  Any valid <var class="var">resource</var>
operand must be less than <code class="code">RLIM_NLIMITS</code>.
</p></dd>
</dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-RLIM_005fINFINITY"><span class="category-def">Constant: </span><span><code class="def-type">rlim_t</code> <strong class="def-name">RLIM_INFINITY</strong><a class="copiable-link" href="#index-RLIM_005fINFINITY"> &para;</a></span></dt>
<dd>
<p>This constant stands for a value of &ldquo;infinity&rdquo; when supplied as
the limit value in <code class="code">setrlimit</code>.
</p></dd></dl>


<p>The following are historical functions to do some of what the functions
above do.  The functions above are better choices.
</p>
<p><code class="code">ulimit</code> and the command symbols are declared in <samp class="file">ulimit.h</samp>.
<a class="index-entry-id" id="index-ulimit_002eh"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ulimit"><span class="category-def">Function: </span><span><code class="def-type">long int</code> <strong class="def-name">ulimit</strong> <code class="def-code-arguments">(int <var class="var">cmd</var>, &hellip;)</code><a class="copiable-link" href="#index-ulimit"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p><code class="code">ulimit</code> gets the current limit or sets the current and maximum
limit for a particular resource for the calling process according to the
command <var class="var">cmd</var>.
</p>
<p>If you are getting a limit, the command argument is the only argument.
If you are setting a limit, there is a second argument:
<code class="code">long int</code> <var class="var">limit</var> which is the value to which you are setting
the limit.
</p>
<p>The <var class="var">cmd</var> values and the operations they specify are:
</p><dl class="vtable">
<dt><a id="index-GETFSIZE"></a><span><code class="code">GETFSIZE</code><a class="copiable-link" href="#index-GETFSIZE"> &para;</a></span></dt>
<dd><p>Get the current limit on the size of a file, in units of 512 bytes.
</p>
</dd>
<dt><a id="index-SETFSIZE"></a><span><code class="code">SETFSIZE</code><a class="copiable-link" href="#index-SETFSIZE"> &para;</a></span></dt>
<dd><p>Set the current and maximum limit on the size of a file to <var class="var">limit</var> *
512 bytes.
</p>
</dd>
</dl>

<p>There are also some other <var class="var">cmd</var> values that may do things on some
systems, but they are not supported.
</p>
<p>Only the superuser may increase a maximum limit.
</p>
<p>When you successfully get a limit, the return value of <code class="code">ulimit</code> is
that limit, which is never negative.  When you successfully set a limit,
the return value is zero.  When the function fails, the return value is
<code class="code">-1</code> and <code class="code">errno</code> is set according to the reason:
</p>
<dl class="table">
<dt><code class="code">EPERM</code></dt>
<dd><p>A process tried to increase a maximum limit, but is not superuser.
</p></dd>
</dl>


</dd></dl>

<p><code class="code">vlimit</code> and its resource symbols are declared in <samp class="file">sys/vlimit.h</samp>.
<a class="index-entry-id" id="index-sys_002fvlimit_002eh"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vlimit"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vlimit</strong> <code class="def-code-arguments">(int <var class="var">resource</var>, int <var class="var">limit</var>)</code><a class="copiable-link" href="#index-vlimit"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:setrlimit
| AS-Unsafe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p><code class="code">vlimit</code> sets the current limit for a resource for a process.
</p>
<p><var class="var">resource</var> identifies the resource:
</p>
<dl class="vtable">
<dt><a id="index-LIM_005fCPU"></a><span><code class="code">LIM_CPU</code><a class="copiable-link" href="#index-LIM_005fCPU"> &para;</a></span></dt>
<dd><p>Maximum CPU time.  Same as <code class="code">RLIMIT_CPU</code> for <code class="code">setrlimit</code>.
</p></dd>
<dt><a id="index-LIM_005fFSIZE"></a><span><code class="code">LIM_FSIZE</code><a class="copiable-link" href="#index-LIM_005fFSIZE"> &para;</a></span></dt>
<dd><p>Maximum file size.  Same as <code class="code">RLIMIT_FSIZE</code> for <code class="code">setrlimit</code>.
</p></dd>
<dt><a id="index-LIM_005fDATA"></a><span><code class="code">LIM_DATA</code><a class="copiable-link" href="#index-LIM_005fDATA"> &para;</a></span></dt>
<dd><p>Maximum data memory.  Same as <code class="code">RLIMIT_DATA</code> for <code class="code">setrlimit</code>.
</p></dd>
<dt><a id="index-LIM_005fSTACK"></a><span><code class="code">LIM_STACK</code><a class="copiable-link" href="#index-LIM_005fSTACK"> &para;</a></span></dt>
<dd><p>Maximum stack size.  Same as <code class="code">RLIMIT_STACK</code> for <code class="code">setrlimit</code>.
</p></dd>
<dt><a id="index-LIM_005fCORE"></a><span><code class="code">LIM_CORE</code><a class="copiable-link" href="#index-LIM_005fCORE"> &para;</a></span></dt>
<dd><p>Maximum core file size.  Same as <code class="code">RLIMIT_COR</code> for <code class="code">setrlimit</code>.
</p></dd>
<dt><a id="index-LIM_005fMAXRSS"></a><span><code class="code">LIM_MAXRSS</code><a class="copiable-link" href="#index-LIM_005fMAXRSS"> &para;</a></span></dt>
<dd><p>Maximum physical memory.  Same as <code class="code">RLIMIT_RSS</code> for <code class="code">setrlimit</code>.
</p></dd>
</dl>

<p>The return value is zero for success, and <code class="code">-1</code> with <code class="code">errno</code> set
accordingly for failure:
</p>
<dl class="table">
<dt><code class="code">EPERM</code></dt>
<dd><p>The process tried to set its current limit beyond its maximum limit.
</p></dd>
</dl>

</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Priority.html">Process CPU Priority And Scheduling</a>, Previous: <a href="Resource-Usage.html">Resource Usage</a>, Up: <a href="Resource-Usage-And-Limitation.html">Resource Usage And Limitation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
