<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>fstab (The GNU C Library)</title>

<meta name="description" content="fstab (The GNU C Library)">
<meta name="keywords" content="fstab (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Mount-Information.html" rel="up" title="Mount Information">
<link href="mtab.html" rel="next" title="mtab">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="fstab">
<div class="nav-panel">
<p>
Next: <a href="mtab.html" accesskey="n" rel="next">The <samp class="file">mtab</samp> file</a>, Up: <a href="Mount-Information.html" accesskey="u" rel="up">Mount Information</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="The-fstab-file"><span>32.3.1.1 The <samp class="file">fstab</samp> file<a class="copiable-link" href="#The-fstab-file"> &para;</a></span></h4>

<p>The internal representation for entries of the file is <code class="code">struct&nbsp;fstab</code><!-- /@w -->, defined in <samp class="file">fstab.h</samp>.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-fstab"><span class="category-def">Data Type: </span><span><strong class="def-name">struct fstab</strong><a class="copiable-link" href="#index-struct-fstab"> &para;</a></span></dt>
<dd>
<p>This structure is used with the <code class="code">getfsent</code>, <code class="code">getfsspec</code>, and
<code class="code">getfsfile</code> functions.
</p>
<dl class="table">
<dt><code class="code">char *fs_spec</code></dt>
<dd><p>This element describes the device from which the filesystem is mounted.
Normally this is the name of a special device, such as a hard disk
partition, but it could also be a more or less generic string.  For
<em class="dfn">NFS</em> it would be a hostname and directory name combination.
</p>
<p>Even though the element is not declared <code class="code">const</code> it shouldn&rsquo;t be
modified.  The missing <code class="code">const</code> has historic reasons, since this
function predates ISO&nbsp;C<!-- /@w -->.  The same is true for the other string
elements of this structure.
</p>
</dd>
<dt><code class="code">char *fs_file</code></dt>
<dd><p>This describes the mount point on the local system.  I.e., accessing any
file in this filesystem has implicitly or explicitly this string as a
prefix.
</p>
</dd>
<dt><code class="code">char *fs_vfstype</code></dt>
<dd><p>This is the type of the filesystem.  Depending on what the underlying
kernel understands it can be any string.
</p>
</dd>
<dt><code class="code">char *fs_mntops</code></dt>
<dd><p>This is a string containing options passed to the kernel with the
<code class="code">mount</code> call.  Again, this can be almost anything.  There can be
more than one option, separated from the others by a comma.  Each option
consists of a name and an optional value part, introduced by an <code class="code">=</code>
character.
</p>
<p>If the value of this element must be processed it should ideally be done
using the <code class="code">getsubopt</code> function; see <a class="ref" href="Suboptions.html">Parsing of Suboptions</a>.
</p>
</dd>
<dt><code class="code">const char *fs_type</code></dt>
<dd><p>This name is poorly chosen.  This element points to a string (possibly
in the <code class="code">fs_mntops</code> string) which describes the modes with which the
filesystem is mounted.  <samp class="file">fstab</samp> defines five macros to describe the
possible values:
</p>
<dl class="vtable">
<dt><a id="index-FSTAB_005fRW"></a><span><code class="code">FSTAB_RW</code><a class="copiable-link" href="#index-FSTAB_005fRW"> &para;</a></span></dt>
<dd><p>The filesystem gets mounted with read and write enabled.
</p></dd>
<dt><a id="index-FSTAB_005fRQ"></a><span><code class="code">FSTAB_RQ</code><a class="copiable-link" href="#index-FSTAB_005fRQ"> &para;</a></span></dt>
<dd><p>The filesystem gets mounted with read and write enabled.  Write access
is restricted by quotas.
</p></dd>
<dt><a id="index-FSTAB_005fRO"></a><span><code class="code">FSTAB_RO</code><a class="copiable-link" href="#index-FSTAB_005fRO"> &para;</a></span></dt>
<dd><p>The filesystem gets mounted read-only.
</p></dd>
<dt><a id="index-FSTAB_005fSW"></a><span><code class="code">FSTAB_SW</code><a class="copiable-link" href="#index-FSTAB_005fSW"> &para;</a></span></dt>
<dd><p>This is not a real filesystem, it is a swap device.
</p></dd>
<dt><a id="index-FSTAB_005fXX"></a><span><code class="code">FSTAB_XX</code><a class="copiable-link" href="#index-FSTAB_005fXX"> &para;</a></span></dt>
<dd><p>This entry from the <samp class="file">fstab</samp> file is totally ignored.
</p></dd>
</dl>

<p>Testing for equality with these values must happen using <code class="code">strcmp</code>
since these are all strings.  Comparing the pointer will probably always
fail.
</p>
</dd>
<dt><code class="code">int fs_freq</code></dt>
<dd><p>This element describes the dump frequency in days.
</p>
</dd>
<dt><code class="code">int fs_passno</code></dt>
<dd><p>This element describes the pass number on parallel dumps.  It is closely
related to the <code class="code">dump</code> utility used on Unix systems.
</p></dd>
</dl>
</dd></dl>


<p>To read the entire content of the of the <samp class="file">fstab</samp> file the GNU C Library
contains a set of three functions which are designed in the usual way.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setfsent"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">setfsent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-setfsent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:fsent
| AS-Unsafe heap corrupt lock
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function makes sure that the internal read pointer for the
<samp class="file">fstab</samp> file is at the beginning of the file.  This is done by
either opening the file or resetting the read pointer.
</p>
<p>Since the file handle is internal to the libc this function is not
thread-safe.
</p>
<p>This function returns a non-zero value if the operation was successful
and the <code class="code">getfs*</code> functions can be used to read the entries of the
file.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-endfsent"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">endfsent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-endfsent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:fsent
| AS-Unsafe heap corrupt lock
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function makes sure that all resources acquired by a prior call to
<code class="code">setfsent</code> (explicitly or implicitly by calling <code class="code">getfsent</code>) are
freed.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getfsent"><span class="category-def">Function: </span><span><code class="def-type">struct fstab *</code> <strong class="def-name">getfsent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-getfsent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:fsent locale
| AS-Unsafe corrupt heap lock
| AC-Unsafe corrupt lock mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the next entry of the <samp class="file">fstab</samp> file.  If this
is the first call to any of the functions handling <samp class="file">fstab</samp> since
program start or the last call of <code class="code">endfsent</code>, the file will be
opened.
</p>
<p>The function returns a pointer to a variable of type <code class="code">struct
fstab</code>.  This variable is shared by all threads and therefore this
function is not thread-safe.  If an error occurred <code class="code">getfsent</code>
returns a <code class="code">NULL</code> pointer.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getfsspec"><span class="category-def">Function: </span><span><code class="def-type">struct fstab *</code> <strong class="def-name">getfsspec</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>)</code><a class="copiable-link" href="#index-getfsspec"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:fsent locale
| AS-Unsafe corrupt heap lock
| AC-Unsafe corrupt lock mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the next entry of the <samp class="file">fstab</samp> file which has
a string equal to <var class="var">name</var> pointed to by the <code class="code">fs_spec</code> element.
Since there is normally exactly one entry for each special device it
makes no sense to call this function more than once for the same
argument.  If this is the first call to any of the functions handling
<samp class="file">fstab</samp> since program start or the last call of <code class="code">endfsent</code>,
the file will be opened.
</p>
<p>The function returns a pointer to a variable of type <code class="code">struct
fstab</code>.  This variable is shared by all threads and therefore this
function is not thread-safe.  If an error occurred <code class="code">getfsent</code>
returns a <code class="code">NULL</code> pointer.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getfsfile"><span class="category-def">Function: </span><span><code class="def-type">struct fstab *</code> <strong class="def-name">getfsfile</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>)</code><a class="copiable-link" href="#index-getfsfile"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:fsent locale
| AS-Unsafe corrupt heap lock
| AC-Unsafe corrupt lock mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the next entry of the <samp class="file">fstab</samp> file which has
a string equal to <var class="var">name</var> pointed to by the <code class="code">fs_file</code> element.
Since there is normally exactly one entry for each mount point it
makes no sense to call this function more than once for the same
argument.  If this is the first call to any of the functions handling
<samp class="file">fstab</samp> since program start or the last call of <code class="code">endfsent</code>,
the file will be opened.
</p>
<p>The function returns a pointer to a variable of type <code class="code">struct
fstab</code>.  This variable is shared by all threads and therefore this
function is not thread-safe.  If an error occurred <code class="code">getfsent</code>
returns a <code class="code">NULL</code> pointer.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="mtab.html">The <samp class="file">mtab</samp> file</a>, Up: <a href="Mount-Information.html">Mount Information</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
