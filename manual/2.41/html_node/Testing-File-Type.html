<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Testing File Type (The GNU C Library)</title>

<meta name="description" content="Testing File Type (The GNU C Library)">
<meta name="keywords" content="Testing File Type (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-Attributes.html" rel="up" title="File Attributes">
<link href="File-Owner.html" rel="next" title="File Owner">
<link href="Reading-Attributes.html" rel="prev" title="Reading Attributes">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Testing-File-Type">
<div class="nav-panel">
<p>
Next: <a href="File-Owner.html" accesskey="n" rel="next">File Owner</a>, Previous: <a href="Reading-Attributes.html" accesskey="p" rel="prev">Reading the Attributes of a File</a>, Up: <a href="File-Attributes.html" accesskey="u" rel="up">File Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Testing-the-Type-of-a-File"><span>14.10.3 Testing the Type of a File<a class="copiable-link" href="#Testing-the-Type-of-a-File"> &para;</a></span></h4>

<p>The <em class="dfn">file mode</em>, stored in the <code class="code">st_mode</code> field of the file
attributes, contains two kinds of information: the file type code, and
the access permission bits.  This section discusses only the type code,
which you can use to tell whether the file is a directory, socket,
symbolic link, and so on.  For details about access permissions see
<a class="ref" href="Permission-Bits.html">The Mode Bits for Access Permission</a>.
</p>
<p>There are two ways you can access the file type information in a file
mode.  Firstly, for each file type there is a <em class="dfn">predicate macro</em>
which examines a given file mode and returns whether it is of that type
or not.  Secondly, you can mask out the rest of the file mode to leave
just the file type code, and compare this against constants for each of
the supported file types.
</p>
<p>All of the symbols listed in this section are defined in the header file
<samp class="file">sys/stat.h</samp>.
<a class="index-entry-id" id="index-sys_002fstat_002eh-3"></a>
</p>
<p>The following predicate macros test the type of a file, given the value
<var class="var">m</var> which is the <code class="code">st_mode</code> field returned by <code class="code">stat</code> on
that file:
</p>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-S_005fISDIR"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_ISDIR</strong> <code class="def-code-arguments">(mode_t <var class="var">m</var>)</code><a class="copiable-link" href="#index-S_005fISDIR"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns non-zero if the file is a directory.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-S_005fISCHR"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_ISCHR</strong> <code class="def-code-arguments">(mode_t <var class="var">m</var>)</code><a class="copiable-link" href="#index-S_005fISCHR"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns non-zero if the file is a character special file (a
device like a terminal).
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-S_005fISBLK"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_ISBLK</strong> <code class="def-code-arguments">(mode_t <var class="var">m</var>)</code><a class="copiable-link" href="#index-S_005fISBLK"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns non-zero if the file is a block special file (a device
like a disk).
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-S_005fISREG"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_ISREG</strong> <code class="def-code-arguments">(mode_t <var class="var">m</var>)</code><a class="copiable-link" href="#index-S_005fISREG"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns non-zero if the file is a regular file.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-S_005fISFIFO"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_ISFIFO</strong> <code class="def-code-arguments">(mode_t <var class="var">m</var>)</code><a class="copiable-link" href="#index-S_005fISFIFO"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns non-zero if the file is a FIFO special file, or a
pipe.  See <a class="xref" href="Pipes-and-FIFOs.html">Pipes and FIFOs</a>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-S_005fISLNK"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_ISLNK</strong> <code class="def-code-arguments">(mode_t <var class="var">m</var>)</code><a class="copiable-link" href="#index-S_005fISLNK"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns non-zero if the file is a symbolic link.
See <a class="xref" href="Symbolic-Links.html">Symbolic Links</a>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-S_005fISSOCK"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_ISSOCK</strong> <code class="def-code-arguments">(mode_t <var class="var">m</var>)</code><a class="copiable-link" href="#index-S_005fISSOCK"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns non-zero if the file is a socket.  See <a class="xref" href="Sockets.html">Sockets</a>.
</p></dd></dl>

<p>An alternate non-POSIX method of testing the file type is supported for
compatibility with BSD.  The mode can be bitwise AND-ed with
<code class="code">S_IFMT</code> to extract the file type code, and compared to the
appropriate constant.  For example,
</p>
<div class="example smallexample">
<pre class="example-preformatted">S_ISCHR (<var class="var">mode</var>)
</pre></div>

<p>is equivalent to:
</p>
<div class="example smallexample">
<pre class="example-preformatted">((<var class="var">mode</var> &amp; S_IFMT) == S_IFCHR)
</pre></div>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-S_005fIFMT"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_IFMT</strong><a class="copiable-link" href="#index-S_005fIFMT"> &para;</a></span></dt>
<dd>
<p>This is a bit mask used to extract the file type code from a mode value.
</p></dd></dl>

<p>These are the symbolic names for the different file type codes:
</p>
<dl class="vtable">
<dt><a id="index-S_005fIFDIR"></a><span><code class="code">S_IFDIR</code><a class="copiable-link" href="#index-S_005fIFDIR"> &para;</a></span></dt>
<dd>
<p>This is the file type constant of a directory file.
</p>
</dd>
<dt><a id="index-S_005fIFCHR"></a><span><code class="code">S_IFCHR</code><a class="copiable-link" href="#index-S_005fIFCHR"> &para;</a></span></dt>
<dd>
<p>This is the file type constant of a character-oriented device file.
</p>
</dd>
<dt><a id="index-S_005fIFBLK"></a><span><code class="code">S_IFBLK</code><a class="copiable-link" href="#index-S_005fIFBLK"> &para;</a></span></dt>
<dd>
<p>This is the file type constant of a block-oriented device file.
</p>
</dd>
<dt><a id="index-S_005fIFREG"></a><span><code class="code">S_IFREG</code><a class="copiable-link" href="#index-S_005fIFREG"> &para;</a></span></dt>
<dd>
<p>This is the file type constant of a regular file.
</p>
</dd>
<dt><a id="index-S_005fIFLNK"></a><span><code class="code">S_IFLNK</code><a class="copiable-link" href="#index-S_005fIFLNK"> &para;</a></span></dt>
<dd>
<p>This is the file type constant of a symbolic link.
</p>
</dd>
<dt><a id="index-S_005fIFSOCK"></a><span><code class="code">S_IFSOCK</code><a class="copiable-link" href="#index-S_005fIFSOCK"> &para;</a></span></dt>
<dd>
<p>This is the file type constant of a socket.
</p>
</dd>
<dt><a id="index-S_005fIFIFO"></a><span><code class="code">S_IFIFO</code><a class="copiable-link" href="#index-S_005fIFIFO"> &para;</a></span></dt>
<dd>
<p>This is the file type constant of a FIFO or pipe.
</p></dd>
</dl>

<p>The POSIX.1b standard introduced a few more objects which possibly can
be implemented as objects in the filesystem.  These are message queues,
semaphores, and shared memory objects.  To allow differentiating these
objects from other files the POSIX standard introduced three new test
macros.  But unlike the other macros they do not take the value of the
<code class="code">st_mode</code> field as the parameter.  Instead they expect a pointer to
the whole <code class="code">struct stat</code> structure.
</p>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-S_005fTYPEISMQ"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_TYPEISMQ</strong> <code class="def-code-arguments">(struct stat *<var class="var">s</var>)</code><a class="copiable-link" href="#index-S_005fTYPEISMQ"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>If the system implements POSIX message queues as distinct objects and the
file is a message queue object, this macro returns a non-zero value.
In all other cases the result is zero.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-S_005fTYPEISSEM"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_TYPEISSEM</strong> <code class="def-code-arguments">(struct stat *<var class="var">s</var>)</code><a class="copiable-link" href="#index-S_005fTYPEISSEM"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>If the system implements POSIX semaphores as distinct objects and the
file is a semaphore object, this macro returns a non-zero value.
In all other cases the result is zero.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-S_005fTYPEISSHM"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">S_TYPEISSHM</strong> <code class="def-code-arguments">(struct stat *<var class="var">s</var>)</code><a class="copiable-link" href="#index-S_005fTYPEISSHM"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>If the system implements POSIX shared memory objects as distinct objects
and the file is a shared memory object, this macro returns a non-zero
value.  In all other cases the result is zero.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="File-Owner.html">File Owner</a>, Previous: <a href="Reading-Attributes.html">Reading the Attributes of a File</a>, Up: <a href="File-Attributes.html">File Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
