<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Noncanonical Input (The GNU C Library)</title>

<meta name="description" content="Noncanonical Input (The GNU C Library)">
<meta name="keywords" content="Noncanonical Input (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Terminal-Modes.html" rel="up" title="Terminal Modes">
<link href="Special-Characters.html" rel="prev" title="Special Characters">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Noncanonical-Input">
<div class="nav-panel">
<p>
Previous: <a href="Special-Characters.html" accesskey="p" rel="prev">Special Characters</a>, Up: <a href="Terminal-Modes.html" accesskey="u" rel="up">Terminal Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Noncanonical-Input-1"><span>17.4.10 Noncanonical Input<a class="copiable-link" href="#Noncanonical-Input-1"> &para;</a></span></h4>

<p>In noncanonical input mode, the special editing characters such as
ERASE and KILL are ignored.  The system facilities for the user to edit
input are disabled in noncanonical mode, so that all input characters
(unless they are special for signal or flow-control purposes) are passed
to the application program exactly as typed.  It is up to the
application program to give the user ways to edit the input, if
appropriate.
</p>
<p>Noncanonical mode offers special parameters called MIN and TIME for
controlling whether and how long to wait for input to be available.  You
can even use them to avoid ever waiting&mdash;to return immediately with
whatever input is available, or with no input.
</p>
<p>The MIN and TIME are stored in elements of the <code class="code">c_cc</code> array, which
is a member of the <code class="code">struct&nbsp;termios</code><!-- /@w --> structure.  Each element of
this array has a particular role, and each element has a symbolic
constant that stands for the index of that element.  <code class="code">VMIN</code> and
<code class="code">VTIME</code> are the names for the indices in the array of the MIN and
TIME slots.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-VMIN"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">VMIN</strong><a class="copiable-link" href="#index-VMIN"> &para;</a></span></dt>
<dd>
<a class="index-entry-id" id="index-MIN-termios-slot"></a>
<p>This is the subscript for the MIN slot in the <code class="code">c_cc</code> array.  Thus,
<code class="code"><var class="var">termios</var>.c_cc[VMIN]</code> is the value itself.
</p>
<p>The MIN slot is only meaningful in noncanonical input mode; it
specifies the minimum number of bytes that must be available in the
input queue in order for <code class="code">read</code> to return.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-VTIME"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">VTIME</strong><a class="copiable-link" href="#index-VTIME"> &para;</a></span></dt>
<dd>
<a class="index-entry-id" id="index-TIME-termios-slot"></a>
<p>This is the subscript for the TIME slot in the <code class="code">c_cc</code> array.  Thus,
<code class="code"><var class="var">termios</var>.c_cc[VTIME]</code> is the value itself.
</p>
<p>The TIME slot is only meaningful in noncanonical input mode; it
specifies how long to wait for input before returning, in units of 0.1
seconds.
</p></dd></dl>

<p>The MIN and TIME values interact to determine the criterion for when
<code class="code">read</code> should return; their precise meanings depend on which of
them are nonzero.  There are four possible cases:
</p>
<ul class="itemize mark-bullet">
<li>Both TIME and MIN are nonzero.

<p>In this case, TIME specifies how long to wait after each input character
to see if more input arrives.  After the first character received,
<code class="code">read</code> keeps waiting until either MIN bytes have arrived in all, or
TIME elapses with no further input.
</p>
<p><code class="code">read</code> always blocks until the first character arrives, even if
TIME elapses first.  <code class="code">read</code> can return more than MIN characters if
more than MIN happen to be in the queue.
</p>
</li><li>Both MIN and TIME are zero.

<p>In this case, <code class="code">read</code> always returns immediately with as many
characters as are available in the queue, up to the number requested.
If no input is immediately available, <code class="code">read</code> returns a value of
zero.
</p>
</li><li>MIN is zero but TIME has a nonzero value.

<p>In this case, <code class="code">read</code> waits for time TIME for input to become
available; the availability of a single byte is enough to satisfy the
read request and cause <code class="code">read</code> to return.  When it returns, it
returns as many characters as are available, up to the number requested.
If no input is available before the timer expires, <code class="code">read</code> returns a
value of zero.
</p>
</li><li>TIME is zero but MIN has a nonzero value.

<p>In this case, <code class="code">read</code> waits until at least MIN bytes are available
in the queue.  At that time, <code class="code">read</code> returns as many characters as
are available, up to the number requested.  <code class="code">read</code> can return more
than MIN characters if more than MIN happen to be in the queue.
</p></li></ul>

<p>What happens if MIN is 50 and you ask to read just 10 bytes?
Normally, <code class="code">read</code> waits until there are 50 bytes in the buffer (or,
more generally, the wait condition described above is satisfied), and
then reads 10 of them, leaving the other 40 buffered in the operating
system for a subsequent call to <code class="code">read</code>.
</p>
<p><strong class="strong">Portability note:</strong> On some systems, the MIN and TIME slots are
actually the same as the EOF and EOL slots.  This causes no serious
problem because the MIN and TIME slots are used only in noncanonical
input and the EOF and EOL slots are used only in canonical input, but it
isn&rsquo;t very clean.  The GNU C Library allocates separate slots for these
uses.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-cfmakeraw"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">cfmakeraw</strong> <code class="def-code-arguments">(struct termios *<var class="var">termios-p</var>)</code><a class="copiable-link" href="#index-cfmakeraw"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function provides an easy way to set up <code class="code">*<var class="var">termios-p</var></code> for
what has traditionally been called &ldquo;raw mode&rdquo; in BSD.  This uses
noncanonical input, and turns off most processing to give an unmodified
channel to the terminal.
</p>
<p>It does exactly this:
</p><div class="example smallexample">
<pre class="example-preformatted">  <var class="var">termios-p</var>-&gt;c_iflag &amp;= ~(IGNBRK|BRKINT|PARMRK|ISTRIP
                                |INLCR|IGNCR|ICRNL|IXON);
  <var class="var">termios-p</var>-&gt;c_oflag &amp;= ~OPOST;
  <var class="var">termios-p</var>-&gt;c_lflag &amp;= ~(ECHO|ECHONL|ICANON|ISIG|IEXTEN);
  <var class="var">termios-p</var>-&gt;c_cflag &amp;= ~(CSIZE|PARENB);
  <var class="var">termios-p</var>-&gt;c_cflag |= CS8;
</pre></div>
</dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Special-Characters.html">Special Characters</a>, Up: <a href="Terminal-Modes.html">Terminal Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
