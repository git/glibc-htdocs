<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Pathconf (The GNU C Library)</title>

<meta name="description" content="Pathconf (The GNU C Library)">
<meta name="keywords" content="Pathconf (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="System-Configuration.html" rel="up" title="System Configuration">
<link href="Utility-Limits.html" rel="next" title="Utility Limits">
<link href="File-Minimums.html" rel="prev" title="File Minimums">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Pathconf">
<div class="nav-panel">
<p>
Next: <a href="Utility-Limits.html" accesskey="n" rel="next">Utility Program Capacity Limits</a>, Previous: <a href="File-Minimums.html" accesskey="p" rel="prev">Minimum Values for File System Limits</a>, Up: <a href="System-Configuration.html" accesskey="u" rel="up">System Configuration Parameters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Using-pathconf"><span>33.9 Using <code class="code">pathconf</code><a class="copiable-link" href="#Using-pathconf"> &para;</a></span></h3>

<p>When your machine allows different files to have different values for a
file system parameter, you can use the functions in this section to find
out the value that applies to any particular file.
</p>
<p>These functions and the associated constants for the <var class="var">parameter</var>
argument are declared in the header file <samp class="file">unistd.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pathconf"><span class="category-def">Function: </span><span><code class="def-type">long int</code> <strong class="def-name">pathconf</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, int <var class="var">parameter</var>)</code><a class="copiable-link" href="#index-pathconf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock heap
| AC-Unsafe lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is used to inquire about the limits that apply to
the file named <var class="var">filename</var>.
</p>
<p>The <var class="var">parameter</var> argument should be one of the &lsquo;<samp class="samp">_PC_</samp>&rsquo; constants
listed below.
</p>
<p>The normal return value from <code class="code">pathconf</code> is the value you requested.
A value of <code class="code">-1</code> is returned both if the implementation does not
impose a limit, and in case of an error.  In the former case,
<code class="code">errno</code> is not set, while in the latter case, <code class="code">errno</code> is set
to indicate the cause of the problem.  So the only way to use this
function robustly is to store <code class="code">0</code> into <code class="code">errno</code> just before
calling it.
</p>
<p>Besides the usual file name errors (see <a class="pxref" href="File-Name-Errors.html">File Name Errors</a>),
the following error condition is defined for this function:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The value of <var class="var">parameter</var> is invalid, or the implementation doesn&rsquo;t
support the <var class="var">parameter</var> for the specific file.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fpathconf"><span class="category-def">Function: </span><span><code class="def-type">long int</code> <strong class="def-name">fpathconf</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, int <var class="var">parameter</var>)</code><a class="copiable-link" href="#index-fpathconf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock heap
| AC-Unsafe lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is just like <code class="code">pathconf</code> except that an open file descriptor
is used to specify the file for which information is requested, instead
of a file name.
</p>
<p>The following <code class="code">errno</code> error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is not a valid file descriptor.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>The value of <var class="var">parameter</var> is invalid, or the implementation doesn&rsquo;t
support the <var class="var">parameter</var> for the specific file.
</p></dd>
</dl>
</dd></dl>

<p>Here are the symbolic constants that you can use as the <var class="var">parameter</var>
argument to <code class="code">pathconf</code> and <code class="code">fpathconf</code>.  The values are all
integer constants.
</p>
<dl class="vtable">
<dt><a id="index-_005fPC_005fLINK_005fMAX"></a><span><code class="code">_PC_LINK_MAX</code><a class="copiable-link" href="#index-_005fPC_005fLINK_005fMAX"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">LINK_MAX</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fMAX_005fCANON"></a><span><code class="code">_PC_MAX_CANON</code><a class="copiable-link" href="#index-_005fPC_005fMAX_005fCANON"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">MAX_CANON</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fMAX_005fINPUT"></a><span><code class="code">_PC_MAX_INPUT</code><a class="copiable-link" href="#index-_005fPC_005fMAX_005fINPUT"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">MAX_INPUT</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fNAME_005fMAX"></a><span><code class="code">_PC_NAME_MAX</code><a class="copiable-link" href="#index-_005fPC_005fNAME_005fMAX"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">NAME_MAX</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fPATH_005fMAX"></a><span><code class="code">_PC_PATH_MAX</code><a class="copiable-link" href="#index-_005fPC_005fPATH_005fMAX"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">PATH_MAX</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fPIPE_005fBUF"></a><span><code class="code">_PC_PIPE_BUF</code><a class="copiable-link" href="#index-_005fPC_005fPIPE_005fBUF"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">PIPE_BUF</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fCHOWN_005fRESTRICTED"></a><span><code class="code">_PC_CHOWN_RESTRICTED</code><a class="copiable-link" href="#index-_005fPC_005fCHOWN_005fRESTRICTED"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">_POSIX_CHOWN_RESTRICTED</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fNO_005fTRUNC"></a><span><code class="code">_PC_NO_TRUNC</code><a class="copiable-link" href="#index-_005fPC_005fNO_005fTRUNC"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">_POSIX_NO_TRUNC</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fVDISABLE"></a><span><code class="code">_PC_VDISABLE</code><a class="copiable-link" href="#index-_005fPC_005fVDISABLE"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">_POSIX_VDISABLE</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fSYNC_005fIO"></a><span><code class="code">_PC_SYNC_IO</code><a class="copiable-link" href="#index-_005fPC_005fSYNC_005fIO"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">_POSIX_SYNC_IO</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fASYNC_005fIO"></a><span><code class="code">_PC_ASYNC_IO</code><a class="copiable-link" href="#index-_005fPC_005fASYNC_005fIO"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">_POSIX_ASYNC_IO</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fPRIO_005fIO"></a><span><code class="code">_PC_PRIO_IO</code><a class="copiable-link" href="#index-_005fPC_005fPRIO_005fIO"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">_POSIX_PRIO_IO</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fFILESIZEBITS"></a><span><code class="code">_PC_FILESIZEBITS</code><a class="copiable-link" href="#index-_005fPC_005fFILESIZEBITS"> &para;</a></span></dt>
<dd>
<p>Inquire about the availability of large files on the filesystem.
</p>
</dd>
<dt><a id="index-_005fPC_005fREC_005fINCR_005fXFER_005fSIZE"></a><span><code class="code">_PC_REC_INCR_XFER_SIZE</code><a class="copiable-link" href="#index-_005fPC_005fREC_005fINCR_005fXFER_005fSIZE"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">POSIX_REC_INCR_XFER_SIZE</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fREC_005fMAX_005fXFER_005fSIZE"></a><span><code class="code">_PC_REC_MAX_XFER_SIZE</code><a class="copiable-link" href="#index-_005fPC_005fREC_005fMAX_005fXFER_005fSIZE"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">POSIX_REC_MAX_XFER_SIZE</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fREC_005fMIN_005fXFER_005fSIZE"></a><span><code class="code">_PC_REC_MIN_XFER_SIZE</code><a class="copiable-link" href="#index-_005fPC_005fREC_005fMIN_005fXFER_005fSIZE"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">POSIX_REC_MIN_XFER_SIZE</code>.
</p>
</dd>
<dt><a id="index-_005fPC_005fREC_005fXFER_005fALIGN"></a><span><code class="code">_PC_REC_XFER_ALIGN</code><a class="copiable-link" href="#index-_005fPC_005fREC_005fXFER_005fALIGN"> &para;</a></span></dt>
<dd>
<p>Inquire about the value of <code class="code">POSIX_REC_XFER_ALIGN</code>.
</p></dd>
</dl>

<p><strong class="strong">Portability Note:</strong> On some systems, the GNU C Library does not
enforce <code class="code">_PC_NAME_MAX</code> or <code class="code">_PC_PATH_MAX</code> limits.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Utility-Limits.html">Utility Program Capacity Limits</a>, Previous: <a href="File-Minimums.html">Minimum Values for File System Limits</a>, Up: <a href="System-Configuration.html">System Configuration Parameters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
