<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Reading Persona (The GNU C Library)</title>

<meta name="description" content="Reading Persona (The GNU C Library)">
<meta name="keywords" content="Reading Persona (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Users-and-Groups.html" rel="up" title="Users and Groups">
<link href="Setting-User-ID.html" rel="next" title="Setting User ID">
<link href="How-Change-Persona.html" rel="prev" title="How Change Persona">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Reading-Persona">
<div class="nav-panel">
<p>
Next: <a href="Setting-User-ID.html" accesskey="n" rel="next">Setting the User ID</a>, Previous: <a href="How-Change-Persona.html" accesskey="p" rel="prev">How an Application Can Change Persona</a>, Up: <a href="Users-and-Groups.html" accesskey="u" rel="up">Users and Groups</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Reading-the-Persona-of-a-Process"><span>31.5 Reading the Persona of a Process<a class="copiable-link" href="#Reading-the-Persona-of-a-Process"> &para;</a></span></h3>

<p>Here are detailed descriptions of the functions for reading the user and
group IDs of a process, both real and effective.  To use these
facilities, you must include the header files <samp class="file">sys/types.h</samp> and
<samp class="file">unistd.h</samp>.
<a class="index-entry-id" id="index-unistd_002eh-22"></a>
<a class="index-entry-id" id="index-sys_002ftypes_002eh-4"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-uid_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">uid_t</strong><a class="copiable-link" href="#index-uid_005ft"> &para;</a></span></dt>
<dd>
<p>This is an integer data type used to represent user IDs.  In
the GNU C Library, this is an alias for <code class="code">unsigned int</code>.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-gid_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">gid_t</strong><a class="copiable-link" href="#index-gid_005ft"> &para;</a></span></dt>
<dd>
<p>This is an integer data type used to represent group IDs.  In
the GNU C Library, this is an alias for <code class="code">unsigned int</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getuid"><span class="category-def">Function: </span><span><code class="def-type">uid_t</code> <strong class="def-name">getuid</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-getuid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getuid</code> function returns the real user ID of the process.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getgid"><span class="category-def">Function: </span><span><code class="def-type">gid_t</code> <strong class="def-name">getgid</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-getgid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getgid</code> function returns the real group ID of the process.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-geteuid"><span class="category-def">Function: </span><span><code class="def-type">uid_t</code> <strong class="def-name">geteuid</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-geteuid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">geteuid</code> function returns the effective user ID of the process.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getegid"><span class="category-def">Function: </span><span><code class="def-type">gid_t</code> <strong class="def-name">getegid</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-getegid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getegid</code> function returns the effective group ID of the process.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getgroups"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getgroups</strong> <code class="def-code-arguments">(int <var class="var">count</var>, gid_t *<var class="var">groups</var>)</code><a class="copiable-link" href="#index-getgroups"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getgroups</code> function is used to inquire about the supplementary
group IDs of the process.  Up to <var class="var">count</var> of these group IDs are
stored in the array <var class="var">groups</var>; the return value from the function is
the number of group IDs actually stored.  If <var class="var">count</var> is smaller than
the total number of supplementary group IDs, then <code class="code">getgroups</code>
returns a value of <code class="code">-1</code> and <code class="code">errno</code> is set to <code class="code">EINVAL</code>.
</p>
<p>If <var class="var">count</var> is zero, then <code class="code">getgroups</code> just returns the total
number of supplementary group IDs.  On systems that do not support
supplementary groups, this will always be zero.
</p>
<p>Here&rsquo;s how to use <code class="code">getgroups</code> to read all the supplementary group
IDs:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">gid_t *
read_all_groups (void)
{
  int ngroups = getgroups (0, NULL);
  gid_t *groups
    = (gid_t *) xmalloc (ngroups * sizeof (gid_t));
  int val = getgroups (ngroups, groups);
  if (val &lt; 0)
    {
      free (groups);
      return NULL;
    }
  return groups;
}
</pre></div></div>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Setting-User-ID.html">Setting the User ID</a>, Previous: <a href="How-Change-Persona.html">How an Application Can Change Persona</a>, Up: <a href="Users-and-Groups.html">Users and Groups</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
