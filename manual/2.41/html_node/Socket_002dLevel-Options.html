<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Socket-Level Options (The GNU C Library)</title>

<meta name="description" content="Socket-Level Options (The GNU C Library)">
<meta name="keywords" content="Socket-Level Options (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Socket-Options.html" rel="up" title="Socket Options">
<link href="Socket-Option-Functions.html" rel="prev" title="Socket Option Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Socket_002dLevel-Options">
<div class="nav-panel">
<p>
Previous: <a href="Socket-Option-Functions.html" accesskey="p" rel="prev">Socket Option Functions</a>, Up: <a href="Socket-Options.html" accesskey="u" rel="up">Socket Options</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Socket_002dLevel-Options-1"><span>16.12.2 Socket-Level Options<a class="copiable-link" href="#Socket_002dLevel-Options-1"> &para;</a></span></h4>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SOL_005fSOCKET"><span class="category-def">Constant: </span><span><code class="def-type">int</code> <strong class="def-name">SOL_SOCKET</strong><a class="copiable-link" href="#index-SOL_005fSOCKET"> &para;</a></span></dt>
<dd>
<p>Use this constant as the <var class="var">level</var> argument to <code class="code">getsockopt</code> or
<code class="code">setsockopt</code> to manipulate the socket-level options described in
this section.
</p></dd></dl>

<a class="index-entry-id" id="index-sys_002fsocket_002eh-14"></a>
<p>Here is a table of socket-level option names; all are defined in the
header file <samp class="file">sys/socket.h</samp>.
</p>
<dl class="vtable">
<dt><a id="index-SO_005fDEBUG"></a><span><code class="code">SO_DEBUG</code><a class="copiable-link" href="#index-SO_005fDEBUG"> &para;</a></span></dt>
<dd>

<p>This option toggles recording of debugging information in the underlying
protocol modules.  The value has type <code class="code">int</code>; a nonzero value means
&ldquo;yes&rdquo;.
</p>
</dd>
<dt><a id="index-SO_005fREUSEADDR"></a><span><code class="code">SO_REUSEADDR</code><a class="copiable-link" href="#index-SO_005fREUSEADDR"> &para;</a></span></dt>
<dd>
<p>This option controls whether <code class="code">bind</code> (see <a class="pxref" href="Setting-Address.html">Setting the Address of a Socket</a>)
should permit reuse of local addresses for this socket.  If you enable
this option, you can actually have two sockets with the same Internet
port number; but the system won&rsquo;t allow you to use the two
identically-named sockets in a way that would confuse the Internet.  The
reason for this option is that some higher-level Internet protocols,
including FTP, require you to keep reusing the same port number.
</p>
<p>The value has type <code class="code">int</code>; a nonzero value means &ldquo;yes&rdquo;.
</p>
</dd>
<dt><a id="index-SO_005fKEEPALIVE"></a><span><code class="code">SO_KEEPALIVE</code><a class="copiable-link" href="#index-SO_005fKEEPALIVE"> &para;</a></span></dt>
<dd>
<p>This option controls whether the underlying protocol should
periodically transmit messages on a connected socket.  If the peer
fails to respond to these messages, the connection is considered
broken.  The value has type <code class="code">int</code>; a nonzero value means
&ldquo;yes&rdquo;.
</p>
</dd>
<dt><a id="index-SO_005fDONTROUTE"></a><span><code class="code">SO_DONTROUTE</code><a class="copiable-link" href="#index-SO_005fDONTROUTE"> &para;</a></span></dt>
<dd>
<p>This option controls whether outgoing messages bypass the normal
message routing facilities.  If set, messages are sent directly to the
network interface instead.  The value has type <code class="code">int</code>; a nonzero
value means &ldquo;yes&rdquo;.
</p>
</dd>
<dt><a id="index-SO_005fLINGER"></a><span><code class="code">SO_LINGER</code><a class="copiable-link" href="#index-SO_005fLINGER"> &para;</a></span></dt>
<dd>
<p>This option specifies what should happen when the socket of a type
that promises reliable delivery still has untransmitted messages when
it is closed; see <a class="ref" href="Closing-a-Socket.html">Closing a Socket</a>.  The value has type
<code class="code">struct linger</code>.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-linger"><span class="category-def">Data Type: </span><span><strong class="def-name">struct linger</strong><a class="copiable-link" href="#index-struct-linger"> &para;</a></span></dt>
<dd>
<p>This structure type has the following members:
</p>
<dl class="table">
<dt><code class="code">int l_onoff</code></dt>
<dd><p>This field is interpreted as a boolean.  If nonzero, <code class="code">close</code>
blocks until the data are transmitted or the timeout period has expired.
</p>
</dd>
<dt><code class="code">int l_linger</code></dt>
<dd><p>This specifies the timeout period, in seconds.
</p></dd>
</dl>
</dd></dl>

</dd>
<dt><a id="index-SO_005fBROADCAST"></a><span><code class="code">SO_BROADCAST</code><a class="copiable-link" href="#index-SO_005fBROADCAST"> &para;</a></span></dt>
<dd>
<p>This option controls whether datagrams may be broadcast from the socket.
The value has type <code class="code">int</code>; a nonzero value means &ldquo;yes&rdquo;.
</p>
</dd>
<dt><a id="index-SO_005fOOBINLINE"></a><span><code class="code">SO_OOBINLINE</code><a class="copiable-link" href="#index-SO_005fOOBINLINE"> &para;</a></span></dt>
<dd>
<p>If this option is set, out-of-band data received on the socket is
placed in the normal input queue.  This permits it to be read using
<code class="code">read</code> or <code class="code">recv</code> without specifying the <code class="code">MSG_OOB</code>
flag.  See <a class="xref" href="Out_002dof_002dBand-Data.html">Out-of-Band Data</a>.  The value has type <code class="code">int</code>; a
nonzero value means &ldquo;yes&rdquo;.
</p>
</dd>
<dt><a id="index-SO_005fSNDBUF"></a><span><code class="code">SO_SNDBUF</code><a class="copiable-link" href="#index-SO_005fSNDBUF"> &para;</a></span></dt>
<dd>
<p>This option gets or sets the size of the output buffer.  The value is a
<code class="code">size_t</code>, which is the size in bytes.
</p>
</dd>
<dt><a id="index-SO_005fRCVBUF"></a><span><code class="code">SO_RCVBUF</code><a class="copiable-link" href="#index-SO_005fRCVBUF"> &para;</a></span></dt>
<dd>
<p>This option gets or sets the size of the input buffer.  The value is a
<code class="code">size_t</code>, which is the size in bytes.
</p>
</dd>
<dt><a id="index-SO_005fSTYLE"></a><span><code class="code">SO_STYLE</code><a class="copiable-link" href="#index-SO_005fSTYLE"> &para;</a></span></dt>
<dt><a id="index-SO_005fTYPE"></a><span><code class="code">SO_TYPE</code><a class="copiable-link" href="#index-SO_005fTYPE"> &para;</a></span></dt>
<dd>

<p>This option can be used with <code class="code">getsockopt</code> only.  It is used to
get the socket&rsquo;s communication style.  <code class="code">SO_TYPE</code> is the
historical name, and <code class="code">SO_STYLE</code> is the preferred name in GNU.
The value has type <code class="code">int</code> and its value designates a communication
style; see <a class="ref" href="Communication-Styles.html">Communication Styles</a>.
</p>
</dd>
<dt><a id="index-SO_005fERROR"></a><span><code class="code">SO_ERROR</code><a class="copiable-link" href="#index-SO_005fERROR"> &para;</a></span></dt>
<dd>

<p>This option can be used with <code class="code">getsockopt</code> only.  It is used to reset
the error status of the socket.  The value is an <code class="code">int</code>, which represents
the previous error status.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Socket-Option-Functions.html">Socket Option Functions</a>, Up: <a href="Socket-Options.html">Socket Options</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
