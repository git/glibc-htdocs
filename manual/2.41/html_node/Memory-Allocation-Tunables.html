<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Memory Allocation Tunables (The GNU C Library)</title>

<meta name="description" content="Memory Allocation Tunables (The GNU C Library)">
<meta name="keywords" content="Memory Allocation Tunables (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Tunables.html" rel="up" title="Tunables">
<link href="Dynamic-Linking-Tunables.html" rel="next" title="Dynamic Linking Tunables">
<link href="Tunable-names.html" rel="prev" title="Tunable names">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Memory-Allocation-Tunables">
<div class="nav-panel">
<p>
Next: <a href="Dynamic-Linking-Tunables.html" accesskey="n" rel="next">Dynamic Linking Tunables</a>, Previous: <a href="Tunable-names.html" accesskey="p" rel="prev">Tunable names</a>, Up: <a href="Tunables.html" accesskey="u" rel="up">Tunables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Memory-Allocation-Tunables-1"><span>39.2 Memory Allocation Tunables<a class="copiable-link" href="#Memory-Allocation-Tunables-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-memory-allocation-tunables"></a>
<a class="index-entry-id" id="index-malloc-tunables"></a>
<a class="index-entry-id" id="index-tunables_002c-malloc"></a>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc"><span class="category-def">Tunable namespace: </span><span><strong class="def-name">glibc.malloc</strong><a class="copiable-link" href="#index-glibc_002emalloc"> &para;</a></span></dt>
<dd><p>Memory allocation behavior can be modified by setting any of the
following tunables in the <code class="code">malloc</code> namespace:
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002echeck"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.check</strong><a class="copiable-link" href="#index-glibc_002emalloc_002echeck"> &para;</a></span></dt>
<dd><p>This tunable supersedes the <code class="env">MALLOC_CHECK_</code> environment variable and is
identical in features. This tunable has no effect by default and needs the
debug library <samp class="file">libc_malloc_debug</samp> to be preloaded using the
<code class="code">LD_PRELOAD</code> environment variable.
</p>
<p>Setting this tunable to a non-zero value less than 4 enables a special (less
efficient) memory allocator for the <code class="code">malloc</code> family of functions that is
designed to be tolerant against simple errors such as double calls of
free with the same argument, or overruns of a single byte (off-by-one
bugs). Not all such errors can be protected against, however, and memory
leaks can result.  Any detected heap corruption results in immediate
termination of the process.
</p>
<p>Like <code class="env">MALLOC_CHECK_</code>, <code class="code">glibc.malloc.check</code> has a problem in that it
diverges from normal program behavior by writing to <code class="code">stderr</code>, which could
by exploited in SUID and SGID binaries.  Therefore, <code class="code">glibc.malloc.check</code>
is disabled by default for SUID and SGID binaries.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002etop_005fpad"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.top_pad</strong><a class="copiable-link" href="#index-glibc_002emalloc_002etop_005fpad"> &para;</a></span></dt>
<dd><p>This tunable supersedes the <code class="env">MALLOC_TOP_PAD_</code> environment variable and is
identical in features.
</p>
<p>This tunable determines the amount of extra memory in bytes to obtain from the
system when any of the arenas need to be extended.  It also specifies the
number of bytes to retain when shrinking any of the arenas.  This provides the
necessary hysteresis in heap size such that excessive amounts of system calls
can be avoided.
</p>
<p>The default value of this tunable is &lsquo;<samp class="samp">131072</samp>&rsquo; (128 KB).
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002eperturb"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.perturb</strong><a class="copiable-link" href="#index-glibc_002emalloc_002eperturb"> &para;</a></span></dt>
<dd><p>This tunable supersedes the <code class="env">MALLOC_PERTURB_</code> environment variable and is
identical in features.
</p>
<p>If set to a non-zero value, memory blocks are initialized with values depending
on some low order bits of this tunable when they are allocated (except when
allocated by <code class="code">calloc</code>) and freed.  This can be used to debug the use of
uninitialized or freed heap memory. Note that this option does not guarantee
that the freed block will have any specific values. It only guarantees that the
content the block had before it was freed will be overwritten.
</p>
<p>The default value of this tunable is &lsquo;<samp class="samp">0</samp>&rsquo;.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002emmap_005fthreshold"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.mmap_threshold</strong><a class="copiable-link" href="#index-glibc_002emalloc_002emmap_005fthreshold"> &para;</a></span></dt>
<dd><p>This tunable supersedes the <code class="env">MALLOC_MMAP_THRESHOLD_</code> environment variable
and is identical in features.
</p>
<p>When this tunable is set, all chunks larger than this value in bytes are
allocated outside the normal heap, using the <code class="code">mmap</code> system call. This way
it is guaranteed that the memory for these chunks can be returned to the system
on <code class="code">free</code>. Note that requests smaller than this threshold might still be
allocated via <code class="code">mmap</code>.
</p>
<p>If this tunable is not set, the default value is set to &lsquo;<samp class="samp">131072</samp>&rsquo; bytes and
the threshold is adjusted dynamically to suit the allocation patterns of the
program.  If the tunable is set, the dynamic adjustment is disabled and the
value is set as static.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002etrim_005fthreshold"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.trim_threshold</strong><a class="copiable-link" href="#index-glibc_002emalloc_002etrim_005fthreshold"> &para;</a></span></dt>
<dd><p>This tunable supersedes the <code class="env">MALLOC_TRIM_THRESHOLD_</code> environment variable
and is identical in features.
</p>
<p>The value of this tunable is the minimum size (in bytes) of the top-most,
releasable chunk in an arena that will trigger a system call in order to return
memory to the system from that arena.
</p>
<p>If this tunable is not set, the default value is set as 128 KB and the
threshold is adjusted dynamically to suit the allocation patterns of the
program.  If the tunable is set, the dynamic adjustment is disabled and the
value is set as static.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002emmap_005fmax"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.mmap_max</strong><a class="copiable-link" href="#index-glibc_002emalloc_002emmap_005fmax"> &para;</a></span></dt>
<dd><p>This tunable supersedes the <code class="env">MALLOC_MMAP_MAX_</code> environment variable and is
identical in features.
</p>
<p>The value of this tunable is maximum number of chunks to allocate with
<code class="code">mmap</code>.  Setting this to zero disables all use of <code class="code">mmap</code>.
</p>
<p>The default value of this tunable is &lsquo;<samp class="samp">65536</samp>&rsquo;.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002earena_005ftest"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.arena_test</strong><a class="copiable-link" href="#index-glibc_002emalloc_002earena_005ftest"> &para;</a></span></dt>
<dd><p>This tunable supersedes the <code class="env">MALLOC_ARENA_TEST</code> environment variable and is
identical in features.
</p>
<p>The <code class="code">glibc.malloc.arena_test</code> tunable specifies the number of arenas that
can be created before the test on the limit to the number of arenas is
conducted.  The value is ignored if <code class="code">glibc.malloc.arena_max</code> is set.
</p>
<p>The default value of this tunable is 2 for 32-bit systems and 8 for 64-bit
systems.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002earena_005fmax"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.arena_max</strong><a class="copiable-link" href="#index-glibc_002emalloc_002earena_005fmax"> &para;</a></span></dt>
<dd><p>This tunable supersedes the <code class="env">MALLOC_ARENA_MAX</code> environment variable and is
identical in features.
</p>
<p>This tunable sets the number of arenas to use in a process regardless of the
number of cores in the system.
</p>
<p>The default value of this tunable is <code class="code">0</code>, meaning that the limit on the
number of arenas is determined by the number of CPU cores online.  For 32-bit
systems the limit is twice the number of cores online and on 64-bit systems, it
is 8 times the number of cores online.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002etcache_005fmax"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.tcache_max</strong><a class="copiable-link" href="#index-glibc_002emalloc_002etcache_005fmax"> &para;</a></span></dt>
<dd><p>The maximum size of a request (in bytes) which may be met via the
per-thread cache.  The default (and maximum) value is 1032 bytes on
64-bit systems and 516 bytes on 32-bit systems.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002etcache_005fcount"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.tcache_count</strong><a class="copiable-link" href="#index-glibc_002emalloc_002etcache_005fcount"> &para;</a></span></dt>
<dd><p>The maximum number of chunks of each size to cache. The default is 7.
The upper limit is 65535.  If set to zero, the per-thread cache is effectively
disabled.
</p>
<p>The approximate maximum overhead of the per-thread cache is thus equal
to the number of bins times the chunk count in each bin times the size
of each chunk.  With defaults, the approximate maximum overhead of the
per-thread cache is approximately 236 KB on 64-bit systems and 118 KB
on 32-bit systems.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002etcache_005funsorted_005flimit"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.tcache_unsorted_limit</strong><a class="copiable-link" href="#index-glibc_002emalloc_002etcache_005funsorted_005flimit"> &para;</a></span></dt>
<dd><p>When the user requests memory and the request cannot be met via the
per-thread cache, the arenas are used to meet the request.  At this
time, additional chunks will be moved from existing arena lists to
pre-fill the corresponding cache.  While copies from the fastbins,
smallbins, and regular bins are bounded and predictable due to the bin
sizes, copies from the unsorted bin are not bounded, and incur
additional time penalties as they need to be sorted as they&rsquo;re
scanned.  To make scanning the unsorted list more predictable and
bounded, the user may set this tunable to limit the number of chunks
that are scanned from the unsorted list while searching for chunks to
pre-fill the per-thread cache with.  The default, or when set to zero,
is no limit.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002emxfast"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.mxfast</strong><a class="copiable-link" href="#index-glibc_002emalloc_002emxfast"> &para;</a></span></dt>
<dd><p>One of the optimizations <code class="code">malloc</code> uses is to maintain a series of &ldquo;fast
bins&rdquo; that hold chunks up to a specific size.  The default and
maximum size which may be held this way is 80 bytes on 32-bit systems
or 160 bytes on 64-bit systems.  Applications which value size over
speed may choose to reduce the size of requests which are serviced
from fast bins with this tunable.  Note that the value specified
includes <code class="code">malloc</code>&rsquo;s internal overhead, which is normally the size of one
pointer, so add 4 on 32-bit systems or 8 on 64-bit systems to the size
passed to <code class="code">malloc</code> for the largest bin size to enable.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002emalloc_002ehugetlb"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.malloc.hugetlb</strong><a class="copiable-link" href="#index-glibc_002emalloc_002ehugetlb"> &para;</a></span></dt>
<dd><p>This tunable controls the usage of Huge Pages on <code class="code">malloc</code> calls.  The
default value is <code class="code">0</code>, which disables any additional support on
<code class="code">malloc</code>.
</p>
<p>Setting its value to <code class="code">1</code> enables the use of <code class="code">madvise</code> with
<code class="code">MADV_HUGEPAGE</code> after memory allocation with <code class="code">mmap</code>.  It is enabled
only if the system supports Transparent Huge Page (currently only on Linux).
</p>
<p>Setting its value to <code class="code">2</code> enables the use of Huge Page directly with
<code class="code">mmap</code> with the use of <code class="code">MAP_HUGETLB</code> flag.  The huge page size
to use will be the default one provided by the system.  A value larger than
<code class="code">2</code> specifies huge page size, which will be matched against the system
supported ones.  If provided value is invalid, <code class="code">MAP_HUGETLB</code> will not
be used.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Dynamic-Linking-Tunables.html">Dynamic Linking Tunables</a>, Previous: <a href="Tunable-names.html">Tunable names</a>, Up: <a href="Tunables.html">Tunables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
