<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Reading/Closing Directory (The GNU C Library)</title>

<meta name="description" content="Reading/Closing Directory (The GNU C Library)">
<meta name="keywords" content="Reading/Closing Directory (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Accessing-Directories.html" rel="up" title="Accessing Directories">
<link href="Simple-Directory-Lister.html" rel="next" title="Simple Directory Lister">
<link href="Opening-a-Directory.html" rel="prev" title="Opening a Directory">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Reading_002fClosing-Directory">
<div class="nav-panel">
<p>
Next: <a href="Simple-Directory-Lister.html" accesskey="n" rel="next">Simple Program to List a Directory</a>, Previous: <a href="Opening-a-Directory.html" accesskey="p" rel="prev">Opening a Directory Stream</a>, Up: <a href="Accessing-Directories.html" accesskey="u" rel="up">Accessing Directories</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Reading-and-Closing-a-Directory-Stream"><span>14.3.3 Reading and Closing a Directory Stream<a class="copiable-link" href="#Reading-and-Closing-a-Directory-Stream"> &para;</a></span></h4>

<a class="index-entry-id" id="index-dirent_002eh-3"></a>
<p>This section describes how to read directory entries from a directory
stream, and how to close the stream when you are done with it.  All the
symbols are declared in the header file <samp class="file">dirent.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-readdir"><span class="category-def">Function: </span><span><code class="def-type">struct dirent *</code> <strong class="def-name">readdir</strong> <code class="def-code-arguments">(DIR *<var class="var">dirstream</var>)</code><a class="copiable-link" href="#index-readdir"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function reads the next entry from the directory.  It normally
returns a pointer to a structure containing information about the
file.  This structure is associated with the <var class="var">dirstream</var> handle
and can be rewritten by a subsequent call.
</p>
<p><strong class="strong">Portability Note:</strong> On some systems <code class="code">readdir</code> may not
return entries for <samp class="file">.</samp> and <samp class="file">..</samp>, even though these are always
valid file names in any directory.  See <a class="xref" href="File-Name-Resolution.html">File Name Resolution</a>.
</p>
<p>If there are no more entries in the directory or an error is detected,
<code class="code">readdir</code> returns a null pointer.  The following <code class="code">errno</code> error
conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">dirstream</var> argument is not valid.
</p></dd>
</dl>

<p>To distinguish between an end-of-directory condition or an error, you
must set <code class="code">errno</code> to zero before calling <code class="code">readdir</code>.  To avoid
entering an infinite loop, you should stop reading from the directory
after the first error.
</p>
<p><strong class="strong">Caution:</strong> The pointer returned by <code class="code">readdir</code> points to
a buffer within the <code class="code">DIR</code> object.  The data in that buffer will
be overwritten by the next call to <code class="code">readdir</code>.  You must take care,
for instance, to copy the <code class="code">d_name</code> string if you need it later.
</p>
<p>Because of this, it is not safe to share a <code class="code">DIR</code> object among
multiple threads, unless you use your own locking to ensure that
no thread calls <code class="code">readdir</code> while another thread is still using the
data from the previous call.  In the GNU C Library, it is safe to call
<code class="code">readdir</code> from multiple threads as long as each thread uses
its own <code class="code">DIR</code> object.  POSIX.1-2008 does not require this to
be safe, but we are not aware of any operating systems where it
does not work.
</p>
<p><code class="code">readdir_r</code> allows you to provide your own buffer for the
<code class="code">struct dirent</code>, but it is less portable than <code class="code">readdir</code>, and
has problems with very long filenames (see below).  We recommend
you use <code class="code">readdir</code>, but do not share <code class="code">DIR</code> objects.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-readdir_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">readdir_r</strong> <code class="def-code-arguments">(DIR *<var class="var">dirstream</var>, struct dirent *<var class="var">entry</var>, struct dirent **<var class="var">result</var>)</code><a class="copiable-link" href="#index-readdir_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is a version of <code class="code">readdir</code> which performs internal
locking.  Like <code class="code">readdir</code> it returns the next entry from the
directory.  To prevent conflicts between simultaneously running
threads the result is stored inside the <var class="var">entry</var> object.
</p>
<p><strong class="strong">Portability Note:</strong> <code class="code">readdir_r</code> is deprecated.  It is
recommended to use <code class="code">readdir</code> instead of <code class="code">readdir_r</code> for the
following reasons:
</p>
<ul class="itemize mark-bullet">
<li>On systems which do not define <code class="code">NAME_MAX</code>, it may not be possible
to use <code class="code">readdir_r</code> safely because the caller does not specify the
length of the buffer for the directory entry.

</li><li>On some systems, <code class="code">readdir_r</code> cannot read directory entries with
very long names.  If such a name is encountered, the GNU C Library
implementation of <code class="code">readdir_r</code> returns with an error code of
<code class="code">ENAMETOOLONG</code> after the final directory entry has been read.  On
other systems, <code class="code">readdir_r</code> may return successfully, but the
<code class="code">d_name</code> member may not be NUL-terminated or may be truncated.

</li><li>POSIX-1.2008 does not guarantee that <code class="code">readdir</code> is thread-safe,
even when access to the same <var class="var">dirstream</var> is serialized.  But in
current implementations (including the GNU C Library), it is safe to call
<code class="code">readdir</code> concurrently on different <var class="var">dirstream</var>s, so there is
no need to use <code class="code">readdir_r</code> in most multi-threaded programs.  In
the rare case that multiple threads need to read from the same
<var class="var">dirstream</var>, it is still better to use <code class="code">readdir</code> and external
synchronization.

</li><li>It is expected that future versions of POSIX will obsolete
<code class="code">readdir_r</code> and mandate the level of thread safety for
<code class="code">readdir</code> which is provided by the GNU C Library and other
implementations today.
</li></ul>

<p>Normally <code class="code">readdir_r</code> returns zero and sets <code class="code">*<var class="var">result</var></code>
to <var class="var">entry</var>.  If there are no more entries in the directory or an
error is detected, <code class="code">readdir_r</code> sets <code class="code">*<var class="var">result</var></code> to a
null pointer and returns a nonzero error code, also stored in
<code class="code">errno</code>, as described for <code class="code">readdir</code>.
</p>
<p>It is also important to look at the definition of the <code class="code">struct
dirent</code> type.  Simply passing a pointer to an object of this type for
the second parameter of <code class="code">readdir_r</code> might not be enough.  Some
systems don&rsquo;t define the <code class="code">d_name</code> element sufficiently long.  In
this case the user has to provide additional space.  There must be room
for at least <code class="code">NAME_MAX + 1</code> characters in the <code class="code">d_name</code> array.
Code to call <code class="code">readdir_r</code> could look like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  union
  {
    struct dirent d;
    char b[offsetof (struct dirent, d_name) + NAME_MAX + 1];
  } u;

  if (readdir_r (dir, &amp;u.d, &amp;res) == 0)
    ...
</pre></div>
</dd></dl>

<p>To support large filesystems on 32-bit machines there are LFS variants
of the last two functions.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-readdir64"><span class="category-def">Function: </span><span><code class="def-type">struct dirent64 *</code> <strong class="def-name">readdir64</strong> <code class="def-code-arguments">(DIR *<var class="var">dirstream</var>)</code><a class="copiable-link" href="#index-readdir64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">readdir64</code> function is just like the <code class="code">readdir</code> function
except that it returns a pointer to a record of type <code class="code">struct
dirent64</code>.  Some of the members of this data type (notably <code class="code">d_ino</code>)
might have a different size to allow large filesystems.
</p>
<p>In all other aspects this function is equivalent to <code class="code">readdir</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-readdir64_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">readdir64_r</strong> <code class="def-code-arguments">(DIR *<var class="var">dirstream</var>, struct dirent64 *<var class="var">entry</var>, struct dirent64 **<var class="var">result</var>)</code><a class="copiable-link" href="#index-readdir64_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The deprecated <code class="code">readdir64_r</code> function is equivalent to the
<code class="code">readdir_r</code> function except that it takes parameters of base type
<code class="code">struct dirent64</code> instead of <code class="code">struct dirent</code> in the second and
third position.  The same precautions mentioned in the documentation of
<code class="code">readdir_r</code> also apply here.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-closedir"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">closedir</strong> <code class="def-code-arguments">(DIR *<var class="var">dirstream</var>)</code><a class="copiable-link" href="#index-closedir"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap lock/hurd
| AC-Unsafe mem fd lock/hurd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function closes the directory stream <var class="var">dirstream</var>.  It returns
<code class="code">0</code> on success and <code class="code">-1</code> on failure.
</p>
<p>The following <code class="code">errno</code> error conditions are defined for this
function:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">dirstream</var> argument is not valid.
</p></dd>
</dl>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Simple-Directory-Lister.html">Simple Program to List a Directory</a>, Previous: <a href="Opening-a-Directory.html">Opening a Directory Stream</a>, Up: <a href="Accessing-Directories.html">Accessing Directories</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
