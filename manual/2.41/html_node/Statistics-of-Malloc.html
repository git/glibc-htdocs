<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Statistics of Malloc (The GNU C Library)</title>

<meta name="description" content="Statistics of Malloc (The GNU C Library)">
<meta name="keywords" content="Statistics of Malloc (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Unconstrained-Allocation.html" rel="up" title="Unconstrained Allocation">
<link href="Summary-of-Malloc.html" rel="next" title="Summary of Malloc">
<link href="Heap-Consistency-Checking.html" rel="prev" title="Heap Consistency Checking">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Statistics-of-Malloc">
<div class="nav-panel">
<p>
Next: <a href="Summary-of-Malloc.html" accesskey="n" rel="next">Summary of <code class="code">malloc</code>-Related Functions</a>, Previous: <a href="Heap-Consistency-Checking.html" accesskey="p" rel="prev">Heap Consistency Checking</a>, Up: <a href="Unconstrained-Allocation.html" accesskey="u" rel="up">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Statistics-for-Memory-Allocation-with-malloc"><span>3.2.3.9 Statistics for Memory Allocation with <code class="code">malloc</code><a class="copiable-link" href="#Statistics-for-Memory-Allocation-with-malloc"> &para;</a></span></h4>

<a class="index-entry-id" id="index-allocation-statistics"></a>
<p>You can get information about dynamic memory allocation by calling the
<code class="code">mallinfo2</code> function.  This function and its associated data type
are declared in <samp class="file">malloc.h</samp>; they are an extension of the standard
SVID/XPG version.
<a class="index-entry-id" id="index-malloc_002eh-1"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-mallinfo2"><span class="category-def">Data Type: </span><span><strong class="def-name">struct mallinfo2</strong><a class="copiable-link" href="#index-struct-mallinfo2"> &para;</a></span></dt>
<dd>
<p>This structure type is used to return information about the dynamic
memory allocator.  It contains the following members:
</p>
<dl class="table">
<dt><code class="code">size_t arena</code></dt>
<dd><p>This is the total size of memory allocated with <code class="code">sbrk</code> by
<code class="code">malloc</code>, in bytes.
</p>
</dd>
<dt><code class="code">size_t ordblks</code></dt>
<dd><p>This is the number of chunks not in use.  (The memory allocator
internally gets chunks of memory from the operating system, and then
carves them up to satisfy individual <code class="code">malloc</code> requests;
see <a class="pxref" href="The-GNU-Allocator.html">The GNU Allocator</a>.)
</p>
</dd>
<dt><code class="code">size_t smblks</code></dt>
<dd><p>This field is unused.
</p>
</dd>
<dt><code class="code">size_t hblks</code></dt>
<dd><p>This is the total number of chunks allocated with <code class="code">mmap</code>.
</p>
</dd>
<dt><code class="code">size_t hblkhd</code></dt>
<dd><p>This is the total size of memory allocated with <code class="code">mmap</code>, in bytes.
</p>
</dd>
<dt><code class="code">size_t usmblks</code></dt>
<dd><p>This field is unused and always 0.
</p>
</dd>
<dt><code class="code">size_t fsmblks</code></dt>
<dd><p>This field is unused.
</p>
</dd>
<dt><code class="code">size_t uordblks</code></dt>
<dd><p>This is the total size of memory occupied by chunks handed out by
<code class="code">malloc</code>.
</p>
</dd>
<dt><code class="code">size_t fordblks</code></dt>
<dd><p>This is the total size of memory occupied by free (not in use) chunks.
</p>
</dd>
<dt><code class="code">size_t keepcost</code></dt>
<dd><p>This is the size of the top-most releasable chunk that normally
borders the end of the heap (i.e., the high end of the virtual address
space&rsquo;s data segment).
</p>
</dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mallinfo2"><span class="category-def">Function: </span><span><code class="def-type">struct mallinfo2</code> <strong class="def-name">mallinfo2</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-mallinfo2"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe init const:mallopt
| AS-Unsafe init lock
| AC-Unsafe init lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>



<p>This function returns information about the current dynamic memory usage
in a structure of type <code class="code">struct mallinfo2</code>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Summary-of-Malloc.html">Summary of <code class="code">malloc</code>-Related Functions</a>, Previous: <a href="Heap-Consistency-Checking.html">Heap Consistency Checking</a>, Up: <a href="Unconstrained-Allocation.html">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
