<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>FP Exceptions (The GNU C Library)</title>

<meta name="description" content="FP Exceptions (The GNU C Library)">
<meta name="keywords" content="FP Exceptions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Floating-Point-Errors.html" rel="up" title="Floating Point Errors">
<link href="Infinity-and-NaN.html" rel="next" title="Infinity and NaN">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="FP-Exceptions">
<div class="nav-panel">
<p>
Next: <a href="Infinity-and-NaN.html" accesskey="n" rel="next">Infinity and NaN</a>, Up: <a href="Floating-Point-Errors.html" accesskey="u" rel="up">Errors in Floating-Point Calculations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="FP-Exceptions-1"><span>20.5.1 FP Exceptions<a class="copiable-link" href="#FP-Exceptions-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-exception"></a>
<a class="index-entry-id" id="index-signal"></a>
<a class="index-entry-id" id="index-zero-divide"></a>
<a class="index-entry-id" id="index-division-by-zero"></a>
<a class="index-entry-id" id="index-inexact-exception"></a>
<a class="index-entry-id" id="index-invalid-exception"></a>
<a class="index-entry-id" id="index-overflow-exception"></a>
<a class="index-entry-id" id="index-underflow-exception"></a>

<p>The IEEE&nbsp;754<!-- /@w --> standard defines five <em class="dfn">exceptions</em> that can occur
during a calculation.  Each corresponds to a particular sort of error,
such as overflow.
</p>
<p>When exceptions occur (when exceptions are <em class="dfn">raised</em>, in the language
of the standard), one of two things can happen.  By default the
exception is simply noted in the floating-point <em class="dfn">status word</em>, and
the program continues as if nothing had happened.  The operation
produces a default value, which depends on the exception (see the table
below).  Your program can check the status word to find out which
exceptions happened.
</p>
<p>Alternatively, you can enable <em class="dfn">traps</em> for exceptions.  In that case,
when an exception is raised, your program will receive the <code class="code">SIGFPE</code>
signal.  The default action for this signal is to terminate the
program.  See <a class="xref" href="Signal-Handling.html">Signal Handling</a>, for how you can change the effect of
the signal.
</p>
<p>The exceptions defined in IEEE&nbsp;754<!-- /@w --> are:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">Invalid Operation</samp>&rsquo;</dt>
<dd><p>This exception is raised if the given operands are invalid for the
operation to be performed.  Examples are
(see IEEE&nbsp;754<!-- /@w -->, section&nbsp;7<!-- /@w -->):
</p><ol class="enumerate">
<li> Addition or subtraction: <em class="math">&#x221E; - &#x221E;</em>.  (But
<em class="math">&#x221E; + &#x221E; = &#x221E;</em>).
</li><li> Multiplication: <em class="math">0 &#x00B7; &#x221E;</em>.
</li><li> Division: <em class="math">0/0</em> or <em class="math">&#x221E;/&#x221E;</em>.
</li><li> Remainder: <em class="math">x</em> REM <em class="math">y</em>, where <em class="math">y</em> is zero or <em class="math">x</em> is
infinite.
</li><li> Square root if the operand is less than zero.  More generally, any
mathematical function evaluated outside its domain produces this
exception.
</li><li> Conversion of a floating-point number to an integer or decimal
string, when the number cannot be represented in the target format (due
to overflow, infinity, or NaN).
</li><li> Conversion of an unrecognizable input string.
</li><li> Comparison via predicates involving <em class="math">&lt;</em> or <em class="math">&gt;</em>, when one or
other of the operands is NaN.  You can prevent this exception by using
the unordered comparison functions instead; see <a class="ref" href="FP-Comparison-Functions.html">Floating-Point Comparison Functions</a>.
</li></ol>

<p>If the exception does not trap, the result of the operation is NaN.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Division by Zero</samp>&rsquo;</dt>
<dd><p>This exception is raised when a finite nonzero number is divided
by zero.  If no trap occurs the result is either <em class="math">+&#x221E;</em> or
<em class="math">-&#x221E;</em>, depending on the signs of the operands.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Overflow</samp>&rsquo;</dt>
<dd><p>This exception is raised whenever the result cannot be represented
as a finite value in the precision format of the destination.  If no trap
occurs the result depends on the sign of the intermediate result and the
current rounding mode (IEEE&nbsp;754<!-- /@w -->, section&nbsp;7.3<!-- /@w -->):
</p><ol class="enumerate">
<li> Round to nearest carries all overflows to <em class="math">&#x221E;</em>
with the sign of the intermediate result.
</li><li> Round toward <em class="math">0</em> carries all overflows to the largest representable
finite number with the sign of the intermediate result.
</li><li> Round toward <em class="math">-&#x221E;</em> carries positive overflows to the
largest representable finite number and negative overflows to
<em class="math">-&#x221E;</em>.

</li><li> Round toward <em class="math">&#x221E;</em> carries negative overflows to the
most negative representable finite number and positive overflows
to <em class="math">&#x221E;</em>.
</li></ol>

<p>Whenever the overflow exception is raised, the inexact exception is also
raised.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Underflow</samp>&rsquo;</dt>
<dd><p>The underflow exception is raised when an intermediate result is too
small to be calculated accurately, or if the operation&rsquo;s result rounded
to the destination precision is too small to be normalized.
</p>
<p>When no trap is installed for the underflow exception, underflow is
signaled (via the underflow flag) only when both tininess and loss of
accuracy have been detected.  If no trap handler is installed the
operation continues with an imprecise small value, or zero if the
destination precision cannot hold the small exact result.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Inexact</samp>&rsquo;</dt>
<dd><p>This exception is signalled if a rounded result is not exact (such as
when calculating the square root of two) or a result overflows without
an overflow trap.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Infinity-and-NaN.html">Infinity and NaN</a>, Up: <a href="Floating-Point-Errors.html">Errors in Floating-Point Calculations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
