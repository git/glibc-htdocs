<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Printing of Floats (The GNU C Library)</title>

<meta name="description" content="Printing of Floats (The GNU C Library)">
<meta name="keywords" content="Printing of Floats (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Arithmetic.html" rel="up" title="Arithmetic">
<link href="System-V-Number-Conversion.html" rel="next" title="System V Number Conversion">
<link href="Parsing-of-Numbers.html" rel="prev" title="Parsing of Numbers">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Printing-of-Floats">
<div class="nav-panel">
<p>
Next: <a href="System-V-Number-Conversion.html" accesskey="n" rel="next">Old-fashioned System V number-to-string functions</a>, Previous: <a href="Parsing-of-Numbers.html" accesskey="p" rel="prev">Parsing of Numbers</a>, Up: <a href="Arithmetic.html" accesskey="u" rel="up">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Printing-of-Floats-1"><span>20.12 Printing of Floats<a class="copiable-link" href="#Printing-of-Floats-1"> &para;</a></span></h3>

<a class="index-entry-id" id="index-stdlib_002eh-20"></a>
<p>The &lsquo;<samp class="samp">strfrom</samp>&rsquo; functions are declared in <samp class="file">stdlib.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strfromd"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">strfromd</strong> <code class="def-code-arguments">(char *restrict <var class="var">string</var>, size_t <var class="var">size</var>, const char *restrict <var class="var">format</var>, double <var class="var">value</var>)</code><a class="copiable-link" href="#index-strfromd"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-strfromf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">strfromf</strong> <code class="def-code-arguments">(char *restrict <var class="var">string</var>, size_t <var class="var">size</var>, const char *restrict <var class="var">format</var>, float <var class="var">value</var>)</code><a class="copiable-link" href="#index-strfromf"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-strfroml"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">strfroml</strong> <code class="def-code-arguments">(char *restrict <var class="var">string</var>, size_t <var class="var">size</var>, const char *restrict <var class="var">format</var>, long double <var class="var">value</var>)</code><a class="copiable-link" href="#index-strfroml"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The functions <code class="code">strfromd</code> (&ldquo;string-from-double&rdquo;), <code class="code">strfromf</code>
(&ldquo;string-from-float&rdquo;), and <code class="code">strfroml</code> (&ldquo;string-from-long-double&rdquo;)
convert the floating-point number <var class="var">value</var> to a string of characters and
stores them into the area pointed to by <var class="var">string</var>.  The conversion
writes at most <var class="var">size</var> characters and respects the format specified by
<var class="var">format</var>.
</p>
<p>The format string must start with the character &lsquo;<samp class="samp">%</samp>&rsquo;.  An optional
precision follows, which starts with a period, &lsquo;<samp class="samp">.</samp>&rsquo;, and may be
followed by a decimal integer, representing the precision.  If a decimal
integer is not specified after the period, the precision is taken to be
zero.  The character &lsquo;<samp class="samp">*</samp>&rsquo; is not allowed.  Finally, the format string
ends with one of the following conversion specifiers: &lsquo;<samp class="samp">a</samp>&rsquo;, &lsquo;<samp class="samp">A</samp>&rsquo;,
&lsquo;<samp class="samp">e</samp>&rsquo;, &lsquo;<samp class="samp">E</samp>&rsquo;, &lsquo;<samp class="samp">f</samp>&rsquo;, &lsquo;<samp class="samp">F</samp>&rsquo;, &lsquo;<samp class="samp">g</samp>&rsquo; or &lsquo;<samp class="samp">G</samp>&rsquo; (see <a class="pxref" href="Table-of-Output-Conversions.html">Table of Output Conversions</a>).  Invalid format strings result in undefined
behavior.
</p>
<p>These functions return the number of characters that would have been
written to <var class="var">string</var> had <var class="var">size</var> been sufficiently large, not
counting the terminating null character.  Thus, the null-terminated output
has been completely written if and only if the returned value is less than
<var class="var">size</var>.
</p>
<p>These functions were introduced by ISO/IEC TS 18661-1.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strfromfN"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">strfromfN</strong> <code class="def-code-arguments">(char *restrict <var class="var">string</var>, size_t <var class="var">size</var>, const char *restrict <var class="var">format</var>, _Float<var class="var">N</var> <var class="var">value</var>)</code><a class="copiable-link" href="#index-strfromfN"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-strfromfNx"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">strfromfNx</strong> <code class="def-code-arguments">(char *restrict <var class="var">string</var>, size_t <var class="var">size</var>, const char *restrict <var class="var">format</var>, _Float<var class="var">N</var>x <var class="var">value</var>)</code><a class="copiable-link" href="#index-strfromfNx"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions are like <code class="code">strfromd</code>, except for the type of
<code class="code">value</code>.
</p>
<p>They were introduced in ISO/IEC&nbsp;TS&nbsp;18661-3<!-- /@w --> and are available on machines
that support the related types; see <a class="pxref" href="Mathematics.html">Mathematics</a>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="System-V-Number-Conversion.html">Old-fashioned System V number-to-string functions</a>, Previous: <a href="Parsing-of-Numbers.html">Parsing of Numbers</a>, Up: <a href="Arithmetic.html">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
