<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Configuring and compiling (The GNU C Library)</title>

<meta name="description" content="Configuring and compiling (The GNU C Library)">
<meta name="keywords" content="Configuring and compiling (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Installation.html" rel="up" title="Installation">
<link href="Running-make-install.html" rel="next" title="Running make install">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="appendixsec-level-extent" id="Configuring-and-compiling">
<div class="nav-panel">
<p>
Next: <a href="Running-make-install.html" accesskey="n" rel="next">Installing the C Library</a>, Up: <a href="Installation.html" accesskey="u" rel="up">Installing the GNU C Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="appendixsec" id="Configuring-and-compiling-the-GNU-C-Library"><span>C.1 Configuring and compiling the GNU C Library<a class="copiable-link" href="#Configuring-and-compiling-the-GNU-C-Library"> &para;</a></span></h3>
<a class="index-entry-id" id="index-configuring"></a>
<a class="index-entry-id" id="index-compiling"></a>

<p>The GNU C Library cannot be compiled in the source directory.  You must build
it in a separate build directory.  For example, if you have unpacked
the GNU C Library sources in <samp class="file">/src/gnu/glibc-<var class="var">version</var></samp>,
create a directory
<samp class="file">/src/gnu/glibc-build</samp> to put the object files in.  This allows
removing the whole build directory in case an error occurs, which is
the safest way to get a fresh start and should always be done.
</p>
<p>From your object directory, run the shell script <samp class="file">configure</samp> located
at the top level of the source tree.  In the scenario above, you&rsquo;d type
</p>
<div class="example smallexample">
<pre class="example-preformatted">$ ../glibc-<var class="var">version</var>/configure <var class="var">args...</var>
</pre></div>

<p>Please note that even though you&rsquo;re building in a separate build
directory, the compilation may need to create or modify files and
directories in the source directory.
</p>
<p><code class="code">configure</code> takes many options, but the only one that is usually
mandatory is &lsquo;<samp class="samp">--prefix</samp>&rsquo;.  This option tells <code class="code">configure</code>
where you want the GNU C Library installed.  This defaults to <samp class="file">/usr/local</samp>,
but the normal setting to install as the standard system library is
&lsquo;<samp class="samp">--prefix=/usr</samp>&rsquo; for GNU/Linux systems and &lsquo;<samp class="samp">--prefix=</samp>&rsquo; (an
empty prefix) for GNU/Hurd systems.
</p>
<p>It may also be useful to pass &lsquo;<samp class="samp">CC=<var class="var">compiler</var></samp>&rsquo; and
<code class="code">CFLAGS=<var class="var">flags</var></code> arguments to <code class="code">configure</code>.  <code class="code">CC</code>
selects the C compiler that will be used, and <code class="code">CFLAGS</code> sets
optimization options for the compiler.  Any compiler options required
for all compilations, such as options selecting an ABI or a processor
for which to generate code, should be included in <code class="code">CC</code>.  Options
that may be overridden by the GNU C Library build system for particular
files, such as for optimization and debugging, should go in
<code class="code">CFLAGS</code>.  The default value of <code class="code">CFLAGS</code> is &lsquo;<samp class="samp">-g -O2</samp>&rsquo;,
and the GNU C Library cannot be compiled without optimization, so if
<code class="code">CFLAGS</code> is specified it must enable optimization.  For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">$ ../glibc-<var class="var">version</var>/configure CC=&quot;gcc -m32&quot; CFLAGS=&quot;-O3&quot;
</pre></div>

<p>To test the GNU C Library with a different set of C and C++ compilers,
&lsquo;<samp class="samp">TEST_CC=<var class="var">compiler</var></samp>&rsquo; and &lsquo;<samp class="samp">TEST_CXX=<var class="var">compiler</var></samp>&rsquo;
arguments can be passed to <code class="code">configure</code>.  For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">$ ../glibc-<var class="var">version</var>/configure TEST_CC=&quot;gcc-6.4.1&quot; TEST_CXX=&quot;g++-6.4.1&quot;
</pre></div>

<p>The following list describes all of the available options for
 <code class="code">configure</code>:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">--prefix=<var class="var">directory</var></samp>&rsquo;</dt>
<dd><p>Install machine-independent data files in subdirectories of
<samp class="file"><var class="var">directory</var></samp>.  The default is to install in <samp class="file">/usr/local</samp>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--exec-prefix=<var class="var">directory</var></samp>&rsquo;</dt>
<dd><p>Install the library and other machine-dependent files in subdirectories
of <samp class="file"><var class="var">directory</var></samp>.  The default is to the &lsquo;<samp class="samp">--prefix</samp>&rsquo;
directory if that option is specified, or <samp class="file">/usr/local</samp> otherwise.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--with-headers=<var class="var">directory</var></samp>&rsquo;</dt>
<dd><p>Look for kernel header files in <var class="var">directory</var>, not
<samp class="file">/usr/include</samp>.  The GNU C Library needs information from the kernel&rsquo;s header
files describing the interface to the kernel.  The GNU C Library will normally
look in <samp class="file">/usr/include</samp> for them,
but if you specify this option, it will look in <var class="var">DIRECTORY</var> instead.
</p>
<p>This option is primarily of use on a system where the headers in
<samp class="file">/usr/include</samp> come from an older version of the GNU C Library.  Conflicts can
occasionally happen in this case.  You can also use this option if you want to
compile the GNU C Library with a newer set of kernel headers than the ones found in
<samp class="file">/usr/include</samp>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--enable-kernel=<var class="var">version</var></samp>&rsquo;</dt>
<dd><p>This option is currently only useful on GNU/Linux systems.  The
<var class="var">version</var> parameter should have the form X.Y.Z and describes the
smallest version of the Linux kernel the generated library is expected
to support.  The higher the <var class="var">version</var> number is, the less
compatibility code is added, and the faster the code gets.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--with-binutils=<var class="var">directory</var></samp>&rsquo;</dt>
<dd><p>Use the binutils (assembler and linker) in <samp class="file"><var class="var">directory</var></samp>, not
the ones the C compiler would default to.  You can use this option if
the default binutils on your system cannot deal with all the constructs
in the GNU C Library.  In that case, <code class="code">configure</code> will detect the
problem and suppress these constructs, so that the library will still be
usable, but functionality may be lost&mdash;for example, you can&rsquo;t build a
shared libc with old binutils.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--with-nonshared-cflags=<var class="var">cflags</var></samp>&rsquo;</dt>
<dd><p>Use additional compiler flags <var class="var">cflags</var> to build the parts of the
library which are always statically linked into applications and
libraries even with shared linking (that is, the object files contained
in <samp class="file">lib*_nonshared.a</samp> libraries).  The build process will
automatically use the appropriate flags, but this option can be used to
set additional flags required for building applications and libraries,
to match local policy.  For example, if such a policy requires that all
code linked into applications must be built with source fortification,
&lsquo;<samp class="samp">--with-nonshared-cflags=-Wp,-D_FORTIFY_SOURCE=2</samp>&rsquo; will make sure
that the objects in <samp class="file">libc_nonshared.a</samp> are compiled with this flag
(although this will not affect the generated code in this particular
case and potentially change debugging information and metadata only).
</p>
</dd>
<dt>&lsquo;<samp class="samp">--with-rtld-early-cflags=<var class="var">cflags</var></samp>&rsquo;</dt>
<dd><p>Use additional compiler flags <var class="var">cflags</var> to build the early startup
code of the dynamic linker.  These flags can be used to enable early
dynamic linker diagnostics to run on CPUs which are not compatible with
the rest of the GNU C Library, for example, due to compiler flags which target
a later instruction set architecture (ISA).
</p>
</dd>
<dt>&lsquo;<samp class="samp">--with-timeoutfactor=<var class="var">NUM</var></samp>&rsquo;</dt>
<dd><p>Specify an integer <var class="var">NUM</var> to scale the timeout of test programs.
This factor can be changed at run time using <code class="env">TIMEOUTFACTOR</code>
environment variable.
</p>

</dd>
<dt>&lsquo;<samp class="samp">--disable-shared</samp>&rsquo;</dt>
<dd><p>Don&rsquo;t build shared libraries even if it is possible.  Not all systems
support shared libraries; you need ELF support and (currently) the GNU
linker.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--disable-default-pie</samp>&rsquo;</dt>
<dd><p>Don&rsquo;t build glibc programs and the testsuite as position independent
executables (PIE).  By default, glibc programs and tests are created as
position independent executables on targets that support it.  If the toolchain
and architecture support it, static executables are built as static PIE and the
resulting glibc can be used with the GCC option, -static-pie, which is
available with GCC 8 or above, to create static PIE.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--enable-cet</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">--enable-cet=permissive</samp>&rsquo;</dt>
<dd><p>Enable Intel Control-flow Enforcement Technology (CET) support.  When
the GNU C Library is built with <samp class="option">--enable-cet</samp> or
<samp class="option">--enable-cet=permissive</samp>, the resulting library
is protected with indirect branch tracking (IBT) and shadow stack
(SHSTK).  When CET is enabled, the GNU C Library is compatible with all
existing executables and shared libraries.  This feature is currently
supported on x86_64 and x32 with GCC 8 and binutils 2.29 or later.
With <samp class="option">--enable-cet</samp>, it is an error to dlopen a non CET
enabled shared library in CET enabled application.  With
<samp class="option">--enable-cet=permissive</samp>, CET is disabled when dlopening a
non CET enabled shared library in CET enabled application.
</p>
<p>NOTE: <samp class="option">--enable-cet</samp> is only supported on x86_64 and x32.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--enable-memory-tagging</samp>&rsquo;</dt>
<dd><p>Enable memory tagging support if the architecture supports it.  When
the GNU C Library is built with this option then the resulting library will
be able to control the use of tagged memory when hardware support is
present by use of the tunable &lsquo;<samp class="samp">glibc.mem.tagging</samp>&rsquo;.  This includes
the generation of tagged memory when using the <code class="code">malloc</code> APIs.
</p>
<p>At present only AArch64 platforms with MTE provide this functionality,
although the library will still operate (without memory tagging) on
older versions of the architecture.
</p>
<p>The default is to disable support for memory tagging.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--disable-profile</samp>&rsquo;</dt>
<dd><p>Don&rsquo;t build libraries with profiling information.  You may want to use
this option if you don&rsquo;t plan to do profiling.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--enable-static-nss</samp>&rsquo;</dt>
<dd><p>Compile static versions of the NSS (Name Service Switch) libraries.
This is not recommended because it defeats the purpose of NSS; a program
linked statically with the NSS libraries cannot be dynamically
reconfigured to use a different name database.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--enable-hardcoded-path-in-tests</samp>&rsquo;</dt>
<dd><p>By default, dynamic tests are linked to run with the installed C library.
This option hardcodes the newly built C library path in dynamic tests
so that they can be invoked directly.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--disable-timezone-tools</samp>&rsquo;</dt>
<dd><p>By default, time zone related utilities (<code class="command">zic</code>, <code class="command">zdump</code>,
and <code class="command">tzselect</code>) are installed with the GNU C Library.  If you are building
these independently (e.g. by using the &lsquo;<samp class="samp">tzcode</samp>&rsquo; package), then this
option will allow disabling the install of these.
</p>
<p>Note that you need to make sure the external tools are kept in sync with
the versions that the GNU C Library expects as the data formats may change over
time.  Consult the <samp class="file">timezone</samp> subdirectory for more details.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--enable-stack-protector</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">--enable-stack-protector=strong</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">--enable-stack-protector=all</samp>&rsquo;</dt>
<dd><p>Compile the C library and all other parts of the glibc package
(including the threading and math libraries, NSS modules, and
transliteration modules) using the GCC <samp class="option">-fstack-protector</samp>,
<samp class="option">-fstack-protector-strong</samp> or <samp class="option">-fstack-protector-all</samp>
options to detect stack overruns.  Only the dynamic linker and a small
number of routines called directly from assembler are excluded from this
protection.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--enable-bind-now</samp>&rsquo;</dt>
<dd><p>Disable lazy binding for installed shared objects and programs.  This
provides additional security hardening because it enables full RELRO
and a read-only global offset table (GOT), at the cost of slightly
increased program load times.
</p>
</dd>
<dt><a class="index-entry-id" id="index-grantpt-1"></a>
<a id="index-pt_005fchown"></a><span>&lsquo;<samp class="samp">--enable-pt_chown</samp>&rsquo;<a class="copiable-link" href="#index-pt_005fchown"> &para;</a></span></dt>
<dd><p>The file <samp class="file">pt_chown</samp> is a helper binary for <code class="code">grantpt</code>
(see <a class="pxref" href="Allocation.html">Pseudo-Terminals</a>) that is installed setuid root to
fix up pseudo-terminal ownership on GNU/Hurd.  It is not required on
GNU/Linux, and the GNU C Library will not use the installed <samp class="file">pt_chown</samp>
program when configured with <samp class="option">--enable-pt_chown</samp>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--disable-werror</samp>&rsquo;</dt>
<dd><p>By default, the GNU C Library is built with <samp class="option">-Werror</samp>.  If you wish
to build without this option (for example, if building with a newer
version of GCC than this version of the GNU C Library was tested with, so
new warnings cause the build with <samp class="option">-Werror</samp> to fail), you can
configure with <samp class="option">--disable-werror</samp>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--disable-mathvec</samp>&rsquo;</dt>
<dd><p>By default for x86_64, the GNU C Library is built with the vector math library.
Use this option to disable the vector math library.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--disable-static-c++-tests</samp>&rsquo;</dt>
<dd><p>By default, if the C++ toolchain lacks support for static linking,
configure fails to find the C++ header files and the glibc build fails.
<samp class="option">--disable-static-c++-link-check</samp> allows the glibc build to finish,
but static C++ tests will fail if the C++ toolchain doesn&rsquo;t have the
necessary static C++ libraries.  Use this option to skip the static C++
tests.  This option implies <samp class="option">--disable-static-c++-link-check</samp>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--disable-static-c++-link-check</samp>&rsquo;</dt>
<dd><p>By default, if the C++ toolchain lacks support for static linking,
configure fails to find the C++ header files and the glibc build fails.
Use this option to disable the static C++ link check so that the C++
header files can be located.  The newly built libc.a can be used to
create static C++ tests if the C++ toolchain has the necessary static
C++ libraries.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--disable-scv</samp>&rsquo;</dt>
<dd><p>Disable using <code class="code">scv</code> instruction for syscalls. All syscalls will use
<code class="code">sc</code> instead, even if the kernel supports <code class="code">scv</code>. PowerPC only.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--build=<var class="var">build-system</var></samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">--host=<var class="var">host-system</var></samp>&rsquo;</dt>
<dd><p>These options are for cross-compiling.  If you specify both options and
<var class="var">build-system</var> is different from <var class="var">host-system</var>, <code class="code">configure</code>
will prepare to cross-compile the GNU C Library from <var class="var">build-system</var> to be used
on <var class="var">host-system</var>.  You&rsquo;ll probably need the &lsquo;<samp class="samp">--with-headers</samp>&rsquo;
option too, and you may have to override <var class="var">configure</var>&rsquo;s selection of
the compiler and/or binutils.
</p>
<p>If you only specify &lsquo;<samp class="samp">--host</samp>&rsquo;, <code class="code">configure</code> will prepare for a
native compile but use what you specify instead of guessing what your
system is.  This is most useful to change the CPU submodel.  For example,
if <code class="code">configure</code> guesses your machine as <code class="code">i686-pc-linux-gnu</code> but
you want to compile a library for 586es, give
&lsquo;<samp class="samp">--host=i586-pc-linux-gnu</samp>&rsquo; or just &lsquo;<samp class="samp">--host=i586-linux</samp>&rsquo; and add
the appropriate compiler flags (&lsquo;<samp class="samp">-mcpu=i586</samp>&rsquo; will do the trick) to
<code class="code">CC</code>.
</p>
<p>If you specify just &lsquo;<samp class="samp">--build</samp>&rsquo;, <code class="code">configure</code> will get confused.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--with-pkgversion=<var class="var">version</var></samp>&rsquo;</dt>
<dd><p>Specify a description, possibly including a build number or build
date, of the binaries being built, to be included in
<samp class="option">--version</samp> output from programs installed with the GNU C Library.
For example, <samp class="option">--with-pkgversion='FooBar GNU/Linux glibc build
123'</samp>.  The default value is &lsquo;<samp class="samp">GNU libc</samp>&rsquo;.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--with-bugurl=<var class="var">url</var></samp>&rsquo;</dt>
<dd><p>Specify the URL that users should visit if they wish to report a bug,
to be included in <samp class="option">--help</samp> output from programs installed with
the GNU C Library.  The default value refers to the main bug-reporting
information for the GNU C Library.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--enable-fortify-source</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">--enable-fortify-source=<var class="var">LEVEL</var></samp>&rsquo;</dt>
<dd><p>Use -D_FORTIFY_SOURCE=<samp class="option">LEVEL</samp> to control hardening in the GNU C Library.
If not provided, <samp class="option">LEVEL</samp> defaults to highest possible value supported by
the build compiler.
</p>
<p>Default is to disable fortification.
</p></dd>
</dl>

<p>To build the library and related programs, type <code class="code">make</code>.  This will
produce a lot of output, some of which may look like errors from
<code class="code">make</code> but aren&rsquo;t.  Look for error messages from <code class="code">make</code>
containing &lsquo;<samp class="samp">***</samp>&rsquo;.  Those indicate that something is seriously wrong.
</p>
<p>The compilation process can take a long time, depending on the
configuration and the speed of your machine.  Some complex modules may
take a very long time to compile, as much as several minutes on slower
machines.  Do not panic if the compiler appears to hang.
</p>
<p>If you want to run a parallel make, simply pass the &lsquo;<samp class="samp">-j</samp>&rsquo; option
with an appropriate numeric parameter to <code class="code">make</code>.  You need a recent
GNU <code class="code">make</code> version, though.
</p>
<p>To build and run test programs which exercise some of the library
facilities, type <code class="code">make check</code>.  If it does not complete
successfully, do not use the built library, and report a bug after
verifying that the problem is not already known.  See <a class="xref" href="Reporting-Bugs.html">Reporting Bugs</a>,
for instructions on reporting bugs.  Note that some of the tests assume
they are not being run by <code class="code">root</code>.  We recommend you compile and
test the GNU C Library as an unprivileged user.
</p>
<p>Before reporting bugs make sure there is no problem with your system.
The tests (and later installation) use some pre-existing files of the
system such as <samp class="file">/etc/passwd</samp>, <samp class="file">/etc/nsswitch.conf</samp> and others.
These files must all contain correct and sensible content.
</p>
<p>Normally, <code class="code">make check</code> will run all the tests before reporting
all problems found and exiting with error status if any problems
occurred.  You can specify &lsquo;<samp class="samp">stop-on-test-failure=y</samp>&rsquo; when running
<code class="code">make check</code> to make the test run stop and exit with an error
status immediately when a failure occurs.
</p>
<p>To format the <cite class="cite">GNU C Library Reference Manual</cite> for printing, type
<code class="code">make&nbsp;dvi</code><!-- /@w -->.  You need a working TeX installation to do
this.  The distribution builds the on-line formatted version of the
manual, as Info files, as part of the build process.  You can build
them manually with <code class="code">make&nbsp;info</code><!-- /@w -->.
</p>
<p>The library has a number of special-purpose configuration parameters
which you can find in <samp class="file">Makeconfig</samp>.  These can be overwritten with
the file <samp class="file">configparms</samp>.  To change them, create a
<samp class="file">configparms</samp> in your build directory and add values as appropriate
for your system.  The file is included and parsed by <code class="code">make</code> and has
to follow the conventions for makefiles.
</p>
<p>It is easy to configure the GNU C Library for cross-compilation by
setting a few variables in <samp class="file">configparms</samp>.  Set <code class="code">CC</code> to the
cross-compiler for the target you configured the library for; it is
important to use this same <code class="code">CC</code> value when running
<code class="code">configure</code>, like this: &lsquo;<samp class="samp">configure <var class="var">target</var>
CC=<var class="var">target</var>-gcc</samp>&rsquo;.  Set <code class="code">BUILD_CC</code> to the compiler to use for programs
run on the build system as part of compiling the library.  You may need to
set <code class="code">AR</code> to cross-compiling versions of <code class="code">ar</code>
if the native tools are not configured to work with
object files for the target you configured for.  When cross-compiling
the GNU C Library, it may be tested using &lsquo;<samp class="samp">make check
test-wrapper=&quot;<var class="var">srcdir</var>/scripts/cross-test-ssh.sh <var class="var">hostname</var>&quot;</samp>&rsquo;,
where <var class="var">srcdir</var> is the absolute directory name for the main source
directory and <var class="var">hostname</var> is the host name of a system that can run
the newly built binaries of the GNU C Library.  The source and build
directories must be visible at the same locations on both the build
system and <var class="var">hostname</var>.
The &lsquo;<samp class="samp">cross-test-ssh.sh</samp>&rsquo; script requires &lsquo;<samp class="samp">flock</samp>&rsquo; from
&lsquo;<samp class="samp">util-linux</samp>&rsquo; to work when <var class="var">glibc_test_allow_time_setting</var>
environment variable is set.
</p>
<p>It is also possible to execute tests, which require setting the date on
the target machine.  Following use cases are supported:
</p><ul class="itemize mark-bullet">
<li><code class="code">GLIBC_TEST_ALLOW_TIME_SETTING</code> is set in the environment in
which eligible tests are executed and have the privilege to run
<code class="code">clock_settime</code>.  In this case, nothing prevents those tests from
running in parallel, so the caller shall assure that those tests
are serialized or provide a proper wrapper script for them.

</li><li>The <code class="code">cross-test-ssh.sh</code> script is used and one passes the
<samp class="option">--allow-time-setting</samp> flag.  In this case, both sets
<code class="code">GLIBC_TEST_ALLOW_TIME_SETTING</code> and serialization of test
execution are assured automatically.
</li></ul>

<p>In general, when testing the GNU C Library, &lsquo;<samp class="samp">test-wrapper</samp>&rsquo; may be set
to the name and arguments of any program to run newly built binaries.
This program must preserve the arguments to the binary being run, its
working directory and the standard input, output and error file
descriptors.  If &lsquo;<samp class="samp"><var class="var">test-wrapper</var> env</samp>&rsquo; will not work to run a
program with environment variables set, then &lsquo;<samp class="samp">test-wrapper-env</samp>&rsquo;
must be set to a program that runs a newly built program with
environment variable assignments in effect, those assignments being
specified as &lsquo;<samp class="samp"><var class="var">var</var>=<var class="var">value</var></samp>&rsquo; before the name of the
program to be run.  If multiple assignments to the same variable are
specified, the last assignment specified must take precedence.
Similarly, if &lsquo;<samp class="samp"><var class="var">test-wrapper</var> env -i</samp>&rsquo; will not work to run a
program with an environment completely empty of variables except those
directly assigned, then &lsquo;<samp class="samp">test-wrapper-env-only</samp>&rsquo; must be set; its
use has the same syntax as &lsquo;<samp class="samp">test-wrapper-env</samp>&rsquo;, the only
difference in its semantics being starting with an empty set of
environment variables rather than the ambient set.
</p>
<p>For AArch64 with SVE, when testing the GNU C Library, &lsquo;<samp class="samp">test-wrapper</samp>&rsquo;
may be set to &quot;<var class="var">srcdir</var>/sysdeps/unix/sysv/linux/aarch64/vltest.py
<var class="var">vector-length</var>&quot; to change Vector Length.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Running-make-install.html">Installing the C Library</a>, Up: <a href="Installation.html">Installing the GNU C Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
