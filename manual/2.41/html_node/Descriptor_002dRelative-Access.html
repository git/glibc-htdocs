<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Descriptor-Relative Access (The GNU C Library)</title>

<meta name="description" content="Descriptor-Relative Access (The GNU C Library)">
<meta name="keywords" content="Descriptor-Relative Access (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-System-Interface.html" rel="up" title="File System Interface">
<link href="Accessing-Directories.html" rel="next" title="Accessing Directories">
<link href="Working-Directory.html" rel="prev" title="Working Directory">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Descriptor_002dRelative-Access">
<div class="nav-panel">
<p>
Next: <a href="Accessing-Directories.html" accesskey="n" rel="next">Accessing Directories</a>, Previous: <a href="Working-Directory.html" accesskey="p" rel="prev">Working Directory</a>, Up: <a href="File-System-Interface.html" accesskey="u" rel="up">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Descriptor_002dRelative-Access-1"><span>14.2 Descriptor-Relative Access<a class="copiable-link" href="#Descriptor_002dRelative-Access-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-file-name-resolution-based-on-descriptors"></a>
<a class="index-entry-id" id="index-descriptor_002dbased-file-name-resolution"></a>
<a class="index-entry-id" id="index-_2026at-functions"></a>

<p>Many functions that accept file names have <code class="code">&hellip;at</code> variants
which accept a file descriptor and a file name argument instead of just
a file name argument.  For example, <code class="code">fstatat</code> is the
descriptor-based variant of the <code class="code">fstat</code> function.  Most such
functions also accept an additional flags argument which changes the
behavior of the file name lookup based on the passed <code class="code">AT_&hellip;</code>
flags.
</p>
<p>There are several reasons to use descriptor-relative access:
</p>
<ul class="itemize mark-bullet">
<li>The working directory is a process-wide resource, so individual threads
cannot change it without affecting other threads in the process.
Explicitly specifying the directory against which relative paths are
resolved can be a thread-safe alternative to changing the working
directory.

</li><li>If a program wishes to access a directory tree which is being modified
concurrently, perhaps even by a different user on the system, the
program must avoid looking up file names with multiple components, in
order to detect symbolic links, using the <code class="code">O_NOFOLLOW</code> flag
(see <a class="pxref" href="Open_002dtime-Flags.html">Open-time Flags</a>) or the <code class="code">AT_SYMLINK_FOLLOW</code> flag
(described below).  Without directory-relative access, it is necessary
to use the <code class="code">fchdir</code> function to change the working directory
(see <a class="pxref" href="Working-Directory.html">Working Directory</a>), which is not thread-safe.

</li><li>Listing directory contents using the <code class="code">readdir</code> or <code class="code">readdir64</code>
functions (see <a class="pxref" href="Reading_002fClosing-Directory.html">Reading and Closing a Directory Stream</a>) does not provide full file
name paths.  Using <code class="code">&hellip;at</code> functions, it is possible to use the
file names directly, without having to construct such full paths.

</li><li>Additional flags available with some of the <code class="code">&hellip;at</code> functions
provide access to functionality which is not available otherwise.
</li></ul>

<p>The file descriptor used by these <code class="code">&hellip;at</code> functions has the
following uses:
</p>
<ul class="itemize mark-bullet">
<li>It can be a file descriptor referring to a directory.  Such a descriptor
can be created explicitly using the <code class="code">open</code> function and the
<code class="code">O_RDONLY</code> file access mode, with or without the <code class="code">O_DIRECTORY</code>
flag.  See <a class="xref" href="Opening-and-Closing-Files.html">Opening and Closing Files</a>.  Or it can be created
implicitly by <code class="code">opendir</code> and retrieved using the <code class="code">dirfd</code>
function.  See <a class="xref" href="Opening-a-Directory.html">Opening a Directory Stream</a>.

<p>If a directory descriptor is used with one of the <code class="code">&hellip;at</code>
functions, a relative file name argument is resolved relative to
directory referred to by the file descriptor, just as if that directory
were the current working directory.  Absolute file name arguments
(starting with &lsquo;<samp class="samp">/</samp>&rsquo;) are resolved against the file system root, and
the descriptor argument is effectively ignored.
</p>
<p>This means that file name lookup is not constrained to the directory of
the descriptor.  For example, it is possible to access a file
<samp class="file">example</samp> in the descriptor&rsquo;s parent directory using a file name
argument <code class="code">&quot;../example&quot;</code>, or in the root directory using
<code class="code">&quot;/example&quot;</code>.
</p>
<p>If the file descriptor refers to a directory, the empty string <code class="code">&quot;&quot;</code>
is not a valid file name argument.  It is possible to use <code class="code">&quot;.&quot;</code> to
refer to the directory itself.  Also see <code class="code">AT_EMPTY_PATH</code> below.
</p>
</li><li><a class="index-entry-id" id="index-AT_005fFDCWD"></a>
The special value <code class="code">AT_FDCWD</code>.  This means that the current working
directory is used for the lookup if the file name is a relative.  For
<code class="code">&hellip;at</code> functions with an <code class="code">AT_&hellip;</code> flags argument,
this provides a shortcut to use those flags with regular (not
descriptor-based) file name lookups.

<p>If <code class="code">AT_FDCWD</code> is used, the empty string <code class="code">&quot;&quot;</code> is not a valid
file name argument.
</p>
</li><li>An arbitrary file descriptor, along with an empty string <code class="code">&quot;&quot;</code> as
the file name argument, and the <code class="code">AT_EMPTY_PATH</code> flag.  In this
case, the operation uses the file descriptor directly, without further
file name resolution.  On Linux, this allows operations on descriptors
opened with the <code class="code">O_PATH</code> flag.  For regular descriptors (opened
without <code class="code">O_PATH</code>), the same functionality is also available through
the plain descriptor-based functions (for example, <code class="code">fstat</code> instead
of <code class="code">fstatat</code>).

<p>This is a GNU extension.
</p></li></ul>

<a class="index-entry-id" id="index-file-name-resolution-flags"></a>
<a class="index-entry-id" id="index-AT_005f_002a-file-name-resolution-flags"></a>
<p>The flags argument in <code class="code">&hellip;at</code> functions can be a combination of
the following flags, defined in <samp class="file">fcntl.h</samp>.  Not all such functions
support all flags, and some (such as <code class="code">openat</code>) do not accept a
flags argument at all.
</p>
<p>In the flag descriptions below, the <em class="dfn">effective final path component</em>
refers to the final component (basename) of the full path constructed
from the descriptor and file name arguments, using file name lookup, as
described above.
</p>
<dl class="vtable">
<dt><a id="index-AT_005fEMPTY_005fPATH"></a><span><code class="code">AT_EMPTY_PATH</code><a class="copiable-link" href="#index-AT_005fEMPTY_005fPATH"> &para;</a></span></dt>
<dd><p>This flag is used with an empty file name <code class="code">&quot;&quot;</code> and a descriptor
which does not necessarily refer to a directory.  It is most useful with
<code class="code">O_PATH</code> descriptors, as described above.  This flag is a GNU
extension.
</p>
</dd>
<dt><a id="index-AT_005fNO_005fAUTOMOUNT"></a><span><code class="code">AT_NO_AUTOMOUNT</code><a class="copiable-link" href="#index-AT_005fNO_005fAUTOMOUNT"> &para;</a></span></dt>
<dd><p>If the effective final path component refers to a potential file system
mount point controlled by an auto-mounting service, the operation does
not trigger auto-mounting and refers to the unmounted mount point
instead.  See <a class="xref" href="Mount_002dUnmount_002dRemount.html">Mount, Unmount, Remount</a>.  If a file system has already
been mounted at the effective final path component, the operation
applies to the file or directory in the mounted file system, not the
underlying file system that was mounted over.  This flag is a GNU
extension.
</p>
</dd>
<dt><a id="index-AT_005fSYMLINK_005fFOLLOW"></a><span><code class="code">AT_SYMLINK_FOLLOW</code><a class="copiable-link" href="#index-AT_005fSYMLINK_005fFOLLOW"> &para;</a></span></dt>
<dd><p>If the effective final path component is a symbolic link, the
operation follows the symbolic link and operates on its target.  (For
most functions, this is the default behavior.)
</p>
</dd>
<dt><a id="index-AT_005fSYMLINK_005fNOFOLLOW"></a><span><code class="code">AT_SYMLINK_NOFOLLOW</code><a class="copiable-link" href="#index-AT_005fSYMLINK_005fNOFOLLOW"> &para;</a></span></dt>
<dd><p>If the effective final path component is a symbolic link, the
operation operates on the symbolic link, without following it.  The
difference in behavior enabled by this flag is similar to the difference
between the <code class="code">lstat</code> and <code class="code">stat</code> functions, or the behavior
activated by the <code class="code">O_NOFOLLOW</code> argument to the <code class="code">open</code> function.
Even with the <code class="code">AT_SYMLINK_NOFOLLOW</code> flag present, symbolic links in
a non-final component of the file name are still followed.
</p></dd>
</dl>

<p><strong class="strong">Note:</strong> There is no relationship between these flags and the type
argument to the <code class="code">getauxval</code> function (with <code class="code">AT_&hellip;</code>
constants defined in <samp class="file">elf.h</samp>).  See <a class="xref" href="Auxiliary-Vector.html">Auxiliary Vector</a>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Accessing-Directories.html">Accessing Directories</a>, Previous: <a href="Working-Directory.html">Working Directory</a>, Up: <a href="File-System-Interface.html">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
