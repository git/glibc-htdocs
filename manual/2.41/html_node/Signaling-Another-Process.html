<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Signaling Another Process (The GNU C Library)</title>

<meta name="description" content="Signaling Another Process (The GNU C Library)">
<meta name="keywords" content="Signaling Another Process (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Generating-Signals.html" rel="up" title="Generating Signals">
<link href="Permission-for-kill.html" rel="next" title="Permission for kill">
<link href="Signaling-Yourself.html" rel="prev" title="Signaling Yourself">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Signaling-Another-Process">
<div class="nav-panel">
<p>
Next: <a href="Permission-for-kill.html" accesskey="n" rel="next">Permission for using <code class="code">kill</code></a>, Previous: <a href="Signaling-Yourself.html" accesskey="p" rel="prev">Signaling Yourself</a>, Up: <a href="Generating-Signals.html" accesskey="u" rel="up">Generating Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Signaling-Another-Process-1"><span>25.6.2 Signaling Another Process<a class="copiable-link" href="#Signaling-Another-Process-1"> &para;</a></span></h4>

<a class="index-entry-id" id="index-killing-a-process"></a>
<p>The <code class="code">kill</code> function can be used to send a signal to another process.
In spite of its name, it can be used for a lot of things other than
causing a process to terminate.  Some examples of situations where you
might want to send signals between processes are:
</p>
<ul class="itemize mark-bullet">
<li>A parent process starts a child to perform a task&mdash;perhaps having the
child running an infinite loop&mdash;and then terminates the child when the
task is no longer needed.

</li><li>A process executes as part of a group, and needs to terminate or notify
the other processes in the group when an error or other event occurs.

</li><li>Two processes need to synchronize while working together.
</li></ul>

<p>This section assumes that you know a little bit about how processes
work.  For more information on this subject, see <a class="ref" href="Processes.html">Processes</a>.
</p>
<p>The <code class="code">kill</code> function is declared in <samp class="file">signal.h</samp>.
<a class="index-entry-id" id="index-signal_002eh-6"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-kill"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">kill</strong> <code class="def-code-arguments">(pid_t <var class="var">pid</var>, int <var class="var">signum</var>)</code><a class="copiable-link" href="#index-kill"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">kill</code> function sends the signal <var class="var">signum</var> to the process
or process group specified by <var class="var">pid</var>.  Besides the signals listed in
<a class="ref" href="Standard-Signals.html">Standard Signals</a>, <var class="var">signum</var> can also have a value of zero to
check the validity of the <var class="var">pid</var>.
</p>
<p>The <var class="var">pid</var> specifies the process or process group to receive the
signal:
</p>
<dl class="table">
<dt><code class="code"><var class="var">pid</var> &gt; 0</code></dt>
<dd><p>The process whose identifier is <var class="var">pid</var>.  (On Linux, the signal is
sent to the entire process even if <var class="var">pid</var> is a thread ID distinct
from the process ID.)
</p>
</dd>
<dt><code class="code"><var class="var">pid</var> == 0</code></dt>
<dd><p>All processes in the same process group as the sender.
</p>
</dd>
<dt><code class="code"><var class="var">pid</var> &lt; -1</code></dt>
<dd><p>The process group whose identifier is &minus;<var class="var">pid</var>.
</p>
</dd>
<dt><code class="code"><var class="var">pid</var> == -1</code></dt>
<dd><p>If the process is privileged, send the signal to all processes except
for some special system processes.  Otherwise, send the signal to all
processes with the same effective user ID.
</p></dd>
</dl>

<p>A process can send a signal to itself with a call like <code class="code">kill&nbsp;(getpid(),&nbsp;<var class="var">signum</var>)</code><!-- /@w -->.  If <code class="code">kill</code> is used by a process to send
a signal to itself, and the signal is not blocked, then <code class="code">kill</code>
delivers at least one signal (which might be some other pending
unblocked signal instead of the signal <var class="var">signum</var>) to that process
before it returns.
</p>
<p>The return value from <code class="code">kill</code> is zero if the signal can be sent
successfully.  Otherwise, no signal is sent, and a value of <code class="code">-1</code> is
returned.  If <var class="var">pid</var> specifies sending a signal to several processes,
<code class="code">kill</code> succeeds if it can send the signal to at least one of them.
There&rsquo;s no way you can tell which of the processes got the signal
or whether all of them did.
</p>
<p>The following <code class="code">errno</code> error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The <var class="var">signum</var> argument is an invalid or unsupported number.
</p>
</dd>
<dt><code class="code">EPERM</code></dt>
<dd><p>You do not have the privilege to send a signal to the process or any of
the processes in the process group named by <var class="var">pid</var>.
</p>
</dd>
<dt><code class="code">ESRCH</code></dt>
<dd><p>The <var class="var">pid</var> argument does not refer to an existing process or group.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tgkill"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">tgkill</strong> <code class="def-code-arguments">(pid_t <var class="var">pid</var>, pid_t <var class="var">tid</var>, int <var class="var">signum</var>)</code><a class="copiable-link" href="#index-tgkill"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">tgkill</code> function sends the signal <var class="var">signum</var> to the thread
or process with ID <var class="var">tid</var>, like the <code class="code">kill</code> function, but only
if the process ID of the thread <var class="var">tid</var> is equal to <var class="var">pid</var>.  If
the target thread belongs to another process, the function fails with
<code class="code">ESRCH</code>.
</p>
<p>The <code class="code">tgkill</code> function can be used to avoid sending a signal to a
thread in the wrong process if the caller ensures that the passed
<var class="var">pid</var> value is not reused by the kernel (for example, if it is the
process ID of the current process, as returned by <code class="code">getpid</code>).
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-killpg"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">killpg</strong> <code class="def-code-arguments">(int <var class="var">pgid</var>, int <var class="var">signum</var>)</code><a class="copiable-link" href="#index-killpg"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is similar to <code class="code">kill</code>, but sends signal <var class="var">signum</var> to the
process group <var class="var">pgid</var>.  This function is provided for compatibility
with BSD; using <code class="code">kill</code> to do this is more portable.
</p></dd></dl>

<p>As a simple example of <code class="code">kill</code>, the call <code class="code">kill&nbsp;(getpid&nbsp;(),&nbsp;<var class="var">sig</var>)</code><!-- /@w --> has the same effect as <code class="code">raise&nbsp;(<var class="var">sig</var>)</code><!-- /@w -->.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Permission-for-kill.html">Permission for using <code class="code">kill</code></a>, Previous: <a href="Signaling-Yourself.html">Signaling Yourself</a>, Up: <a href="Generating-Signals.html">Generating Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
