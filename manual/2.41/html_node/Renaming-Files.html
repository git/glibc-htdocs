<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Renaming Files (The GNU C Library)</title>

<meta name="description" content="Renaming Files (The GNU C Library)">
<meta name="keywords" content="Renaming Files (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-System-Interface.html" rel="up" title="File System Interface">
<link href="Creating-Directories.html" rel="next" title="Creating Directories">
<link href="Deleting-Files.html" rel="prev" title="Deleting Files">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Renaming-Files">
<div class="nav-panel">
<p>
Next: <a href="Creating-Directories.html" accesskey="n" rel="next">Creating Directories</a>, Previous: <a href="Deleting-Files.html" accesskey="p" rel="prev">Deleting Files</a>, Up: <a href="File-System-Interface.html" accesskey="u" rel="up">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Renaming-Files-1"><span>14.8 Renaming Files<a class="copiable-link" href="#Renaming-Files-1"> &para;</a></span></h3>

<p>The <code class="code">rename</code> function is used to change a file&rsquo;s name.
</p>
<a class="index-entry-id" id="index-renaming-a-file"></a>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-rename"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">rename</strong> <code class="def-code-arguments">(const char *<var class="var">oldname</var>, const char *<var class="var">newname</var>)</code><a class="copiable-link" href="#index-rename"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">rename</code> function renames the file <var class="var">oldname</var> to
<var class="var">newname</var>.  The file formerly accessible under the name
<var class="var">oldname</var> is afterwards accessible as <var class="var">newname</var> instead.  (If
the file had any other names aside from <var class="var">oldname</var>, it continues to
have those names.)
</p>
<p>The directory containing the name <var class="var">newname</var> must be on the same file
system as the directory containing the name <var class="var">oldname</var>.
</p>
<p>One special case for <code class="code">rename</code> is when <var class="var">oldname</var> and
<var class="var">newname</var> are two names for the same file.  The consistent way to
handle this case is to delete <var class="var">oldname</var>.  However, in this case
POSIX requires that <code class="code">rename</code> do nothing and report success&mdash;which
is inconsistent.  We don&rsquo;t know what your operating system will do.
</p>
<p>If <var class="var">oldname</var> is not a directory, then any existing file named
<var class="var">newname</var> is removed during the renaming operation.  However, if
<var class="var">newname</var> is the name of a directory, <code class="code">rename</code> fails in this
case.
</p>
<p>If <var class="var">oldname</var> is a directory, then either <var class="var">newname</var> must not
exist or it must name a directory that is empty.  In the latter case,
the existing directory named <var class="var">newname</var> is deleted first.  The name
<var class="var">newname</var> must not specify a subdirectory of the directory
<code class="code">oldname</code> which is being renamed.
</p>
<p>One useful feature of <code class="code">rename</code> is that the meaning of <var class="var">newname</var>
changes &ldquo;atomically&rdquo; from any previously existing file by that name to
its new meaning (i.e., the file that was called <var class="var">oldname</var>).  There is
no instant at which <var class="var">newname</var> is non-existent &ldquo;in between&rdquo; the old
meaning and the new meaning.  If there is a system crash during the
operation, it is possible for both names to still exist; but
<var class="var">newname</var> will always be intact if it exists at all.
</p>
<p>If <code class="code">rename</code> fails, it returns <code class="code">-1</code>.  In addition to the usual
file name errors (see <a class="pxref" href="File-Name-Errors.html">File Name Errors</a>), the following
<code class="code">errno</code> error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EACCES</code></dt>
<dd><p>One of the directories containing <var class="var">newname</var> or <var class="var">oldname</var>
refuses write permission; or <var class="var">newname</var> and <var class="var">oldname</var> are
directories and write permission is refused for one of them.
</p>
</dd>
<dt><code class="code">EBUSY</code></dt>
<dd><p>A directory named by <var class="var">oldname</var> or <var class="var">newname</var> is being used by
the system in a way that prevents the renaming from working.  This includes
directories that are mount points for filesystems, and directories
that are the current working directories of processes.
</p>
</dd>
<dt><code class="code">ENOTEMPTY</code></dt>
<dt><code class="code">EEXIST</code></dt>
<dd><p>The directory <var class="var">newname</var> isn&rsquo;t empty.  GNU/Linux and GNU/Hurd systems always return
<code class="code">ENOTEMPTY</code> for this, but some other systems return <code class="code">EEXIST</code>.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p><var class="var">oldname</var> is a directory that contains <var class="var">newname</var>.
</p>
</dd>
<dt><code class="code">EISDIR</code></dt>
<dd><p><var class="var">newname</var> is a directory but the <var class="var">oldname</var> isn&rsquo;t.
</p>
</dd>
<dt><code class="code">EMLINK</code></dt>
<dd><p>The parent directory of <var class="var">newname</var> would have too many links
(entries).
</p>
</dd>
<dt><code class="code">ENOENT</code></dt>
<dd><p>The file <var class="var">oldname</var> doesn&rsquo;t exist.
</p>
</dd>
<dt><code class="code">ENOSPC</code></dt>
<dd><p>The directory that would contain <var class="var">newname</var> has no room for another
entry, and there is no space left in the file system to expand it.
</p>
</dd>
<dt><code class="code">EROFS</code></dt>
<dd><p>The operation would involve writing to a directory on a read-only file
system.
</p>
</dd>
<dt><code class="code">EXDEV</code></dt>
<dd><p>The two file names <var class="var">newname</var> and <var class="var">oldname</var> are on different
file systems.
</p></dd>
</dl>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Creating-Directories.html">Creating Directories</a>, Previous: <a href="Deleting-Files.html">Deleting Files</a>, Up: <a href="File-System-Interface.html">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
