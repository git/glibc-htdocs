<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Operating Modes (The GNU C Library)</title>

<meta name="description" content="Operating Modes (The GNU C Library)">
<meta name="keywords" content="Operating Modes (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-Status-Flags.html" rel="up" title="File Status Flags">
<link href="Getting-File-Status-Flags.html" rel="next" title="Getting File Status Flags">
<link href="Open_002dtime-Flags.html" rel="prev" title="Open-time Flags">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Operating-Modes">
<div class="nav-panel">
<p>
Next: <a href="Getting-File-Status-Flags.html" accesskey="n" rel="next">Getting and Setting File Status Flags</a>, Previous: <a href="Open_002dtime-Flags.html" accesskey="p" rel="prev">Open-time Flags</a>, Up: <a href="File-Status-Flags.html" accesskey="u" rel="up">File Status Flags</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="I_002fO-Operating-Modes"><span>13.15.3 I/O Operating Modes<a class="copiable-link" href="#I_002fO-Operating-Modes"> &para;</a></span></h4>

<p>The operating modes affect how input and output operations using a file
descriptor work.  These flags are set by <code class="code">open</code> and can be fetched
and changed with <code class="code">fcntl</code>.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fAPPEND"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_APPEND</strong><a class="copiable-link" href="#index-O_005fAPPEND"> &para;</a></span></dt>
<dd>
<p>The bit that enables append mode for the file.  If set, then all
<code class="code">write</code> operations write the data at the end of the file, extending
it, regardless of the current file position.  This is the only reliable
way to append to a file.  In append mode, you are guaranteed that the
data you write will always go to the current end of the file, regardless
of other processes writing to the file.  Conversely, if you simply set
the file position to the end of file and write, then another process can
extend the file after you set the file position but before you write,
resulting in your data appearing someplace before the real end of file.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fNONBLOCK-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_NONBLOCK</strong><a class="copiable-link" href="#index-O_005fNONBLOCK-1"> &para;</a></span></dt>
<dd>
<p>The bit that enables nonblocking mode for the file.  If this bit is set,
<code class="code">read</code> requests on the file can return immediately with a failure
status if there is no input immediately available, instead of blocking.
Likewise, <code class="code">write</code> requests can also return immediately with a
failure status if the output can&rsquo;t be written immediately.
</p>
<p>Note that the <code class="code">O_NONBLOCK</code> flag is overloaded as both an I/O
operating mode and a file name translation flag; see <a class="pxref" href="Open_002dtime-Flags.html">Open-time Flags</a>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fNDELAY"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_NDELAY</strong><a class="copiable-link" href="#index-O_005fNDELAY"> &para;</a></span></dt>
<dd>
<p>This is an obsolete name for <code class="code">O_NONBLOCK</code>, provided for
compatibility with BSD.  It is not defined by the POSIX.1 standard.
</p></dd></dl>

<p>The remaining operating modes are BSD and GNU extensions.  They exist only
on some systems.  On other systems, these macros are not defined.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fASYNC"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_ASYNC</strong><a class="copiable-link" href="#index-O_005fASYNC"> &para;</a></span></dt>
<dd>
<p>The bit that enables asynchronous input mode.  If set, then <code class="code">SIGIO</code>
signals will be generated when input is available.  See <a class="xref" href="Interrupt-Input.html">Interrupt-Driven Input</a>.
</p>
<p>Asynchronous input mode is a BSD feature.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fFSYNC"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_FSYNC</strong><a class="copiable-link" href="#index-O_005fFSYNC"> &para;</a></span></dt>
<dd>
<p>The bit that enables synchronous writing for the file.  If set, each
<code class="code">write</code> call will make sure the data is reliably stored on disk before
returning. </p>
<p>Synchronous writing is a BSD feature.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fSYNC"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_SYNC</strong><a class="copiable-link" href="#index-O_005fSYNC"> &para;</a></span></dt>
<dd>
<p>This is another name for <code class="code">O_FSYNC</code>.  They have the same value.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fNOATIME"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_NOATIME</strong><a class="copiable-link" href="#index-O_005fNOATIME"> &para;</a></span></dt>
<dd>
<p>If this bit is set, <code class="code">read</code> will not update the access time of the
file.  See <a class="xref" href="File-Times.html">File Times</a>.  This is used by programs that do backups, so
that backing a file up does not count as reading it.
Only the owner of the file or the superuser may use this bit.
</p>
<p>This is a GNU extension.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Getting-File-Status-Flags.html">Getting and Setting File Status Flags</a>, Previous: <a href="Open_002dtime-Flags.html">Open-time Flags</a>, Up: <a href="File-Status-Flags.html">File Status Flags</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
