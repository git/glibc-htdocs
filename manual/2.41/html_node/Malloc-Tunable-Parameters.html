<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Malloc Tunable Parameters (The GNU C Library)</title>

<meta name="description" content="Malloc Tunable Parameters (The GNU C Library)">
<meta name="keywords" content="Malloc Tunable Parameters (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Unconstrained-Allocation.html" rel="up" title="Unconstrained Allocation">
<link href="Heap-Consistency-Checking.html" rel="next" title="Heap Consistency Checking">
<link href="Aligned-Memory-Blocks.html" rel="prev" title="Aligned Memory Blocks">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Malloc-Tunable-Parameters">
<div class="nav-panel">
<p>
Next: <a href="Heap-Consistency-Checking.html" accesskey="n" rel="next">Heap Consistency Checking</a>, Previous: <a href="Aligned-Memory-Blocks.html" accesskey="p" rel="prev">Allocating Aligned Memory Blocks</a>, Up: <a href="Unconstrained-Allocation.html" accesskey="u" rel="up">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Malloc-Tunable-Parameters-1"><span>3.2.3.7 Malloc Tunable Parameters<a class="copiable-link" href="#Malloc-Tunable-Parameters-1"> &para;</a></span></h4>

<p>You can adjust some parameters for dynamic memory allocation with the
<code class="code">mallopt</code> function.  This function is the general SVID/XPG
interface, defined in <samp class="file">malloc.h</samp>.
<a class="index-entry-id" id="index-malloc_002eh"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mallopt"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">mallopt</strong> <code class="def-code-arguments">(int <var class="var">param</var>, int <var class="var">value</var>)</code><a class="copiable-link" href="#index-mallopt"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Unsafe init const:mallopt
| AS-Unsafe init lock
| AC-Unsafe init lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>When calling <code class="code">mallopt</code>, the <var class="var">param</var> argument specifies the
parameter to be set, and <var class="var">value</var> the new value to be set.  Possible
choices for <var class="var">param</var>, as defined in <samp class="file">malloc.h</samp>, are:
</p>
<dl class="vtable">
<dt><a id="index-M_005fMMAP_005fMAX"></a><span><code class="code">M_MMAP_MAX</code><a class="copiable-link" href="#index-M_005fMMAP_005fMAX"> &para;</a></span></dt>
<dd><p>The maximum number of chunks to allocate with <code class="code">mmap</code>.  Setting this
to zero disables all use of <code class="code">mmap</code>.
</p>
<p>The default value of this parameter is <code class="code">65536</code>.
</p>
<p>This parameter can also be set for the process at startup by setting the
environment variable <code class="env">MALLOC_MMAP_MAX_</code> to the desired value.
</p>
</dd>
<dt><a id="index-M_005fMMAP_005fTHRESHOLD"></a><span><code class="code">M_MMAP_THRESHOLD</code><a class="copiable-link" href="#index-M_005fMMAP_005fTHRESHOLD"> &para;</a></span></dt>
<dd><p>All chunks larger than this value are allocated outside the normal
heap, using the <code class="code">mmap</code> system call.  This way it is guaranteed
that the memory for these chunks can be returned to the system on
<code class="code">free</code>.  Note that requests smaller than this threshold might still
be allocated via <code class="code">mmap</code>.
</p>
<p>If this parameter is not set, the default value is set as 128 KiB and the
threshold is adjusted dynamically to suit the allocation patterns of the
program. If the parameter is set, the dynamic adjustment is disabled and the
value is set statically to the input value.
</p>
<p>This parameter can also be set for the process at startup by setting the
environment variable <code class="env">MALLOC_MMAP_THRESHOLD_</code> to the desired value.
</p>
</dd>
<dt><a id="index-M_005fPERTURB"></a><span><code class="code">M_PERTURB</code><a class="copiable-link" href="#index-M_005fPERTURB"> &para;</a></span></dt>
<dd><p>If non-zero, memory blocks are filled with values depending on some
low order bits of this parameter when they are allocated (except when
allocated by <code class="code">calloc</code>) and freed.  This can be used to debug the
use of uninitialized or freed heap memory.  Note that this option does not
guarantee that the freed block will have any specific values.  It only
guarantees that the content the block had before it was freed will be
overwritten.
</p>
<p>The default value of this parameter is <code class="code">0</code>.
</p>
<p>This parameter can also be set for the process at startup by setting the
environment variable <code class="env">MALLOC_PERTURB_</code> to the desired value.
</p>
</dd>
<dt><a id="index-M_005fTOP_005fPAD"></a><span><code class="code">M_TOP_PAD</code><a class="copiable-link" href="#index-M_005fTOP_005fPAD"> &para;</a></span></dt>
<dd><p>This parameter determines the amount of extra memory to obtain from the system
when an arena needs to be extended.  It also specifies the number of bytes to
retain when shrinking an arena.  This provides the necessary hysteresis in heap
size such that excessive amounts of system calls can be avoided.
</p>
<p>The default value of this parameter is <code class="code">0</code>.
</p>
<p>This parameter can also be set for the process at startup by setting the
environment variable <code class="env">MALLOC_TOP_PAD_</code> to the desired value.
</p>
</dd>
<dt><a id="index-M_005fTRIM_005fTHRESHOLD"></a><span><code class="code">M_TRIM_THRESHOLD</code><a class="copiable-link" href="#index-M_005fTRIM_005fTHRESHOLD"> &para;</a></span></dt>
<dd><p>This is the minimum size (in bytes) of the top-most, releasable chunk
that will trigger a system call in order to return memory to the system.
</p>
<p>If this parameter is not set, the default value is set as 128 KiB and the
threshold is adjusted dynamically to suit the allocation patterns of the
program. If the parameter is set, the dynamic adjustment is disabled and the
value is set statically to the provided input.
</p>
<p>This parameter can also be set for the process at startup by setting the
environment variable <code class="env">MALLOC_TRIM_THRESHOLD_</code> to the desired value.
</p>
</dd>
<dt><a id="index-M_005fARENA_005fTEST"></a><span><code class="code">M_ARENA_TEST</code><a class="copiable-link" href="#index-M_005fARENA_005fTEST"> &para;</a></span></dt>
<dd><p>This parameter specifies the number of arenas that can be created before the
test on the limit to the number of arenas is conducted. The value is ignored if
<code class="code">M_ARENA_MAX</code> is set.
</p>
<p>The default value of this parameter is 2 on 32-bit systems and 8 on 64-bit
systems.
</p>
<p>This parameter can also be set for the process at startup by setting the
environment variable <code class="env">MALLOC_ARENA_TEST</code> to the desired value.
</p>
</dd>
<dt><a id="index-M_005fARENA_005fMAX"></a><span><code class="code">M_ARENA_MAX</code><a class="copiable-link" href="#index-M_005fARENA_005fMAX"> &para;</a></span></dt>
<dd><p>This parameter sets the number of arenas to use regardless of the number of
cores in the system.
</p>
<p>The default value of this tunable is <code class="code">0</code>, meaning that the limit on the
number of arenas is determined by the number of CPU cores online. For 32-bit
systems the limit is twice the number of cores online and on 64-bit systems, it
is eight times the number of cores online.  Note that the default value is not
derived from the default value of M_ARENA_TEST and is computed independently.
</p>
<p>This parameter can also be set for the process at startup by setting the
environment variable <code class="env">MALLOC_ARENA_MAX</code> to the desired value.
</p></dd>
</dl>

</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Heap-Consistency-Checking.html">Heap Consistency Checking</a>, Previous: <a href="Aligned-Memory-Blocks.html">Allocating Aligned Memory Blocks</a>, Up: <a href="Unconstrained-Allocation.html">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
