<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Scanning All Groups (The GNU C Library)</title>

<meta name="description" content="Scanning All Groups (The GNU C Library)">
<meta name="keywords" content="Scanning All Groups (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Group-Database.html" rel="up" title="Group Database">
<link href="Lookup-Group.html" rel="prev" title="Lookup Group">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Scanning-All-Groups">
<div class="nav-panel">
<p>
Previous: <a href="Lookup-Group.html" accesskey="p" rel="prev">Looking Up One Group</a>, Up: <a href="Group-Database.html" accesskey="u" rel="up">Group Database</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Scanning-the-List-of-All-Groups"><span>31.14.3 Scanning the List of All Groups<a class="copiable-link" href="#Scanning-the-List-of-All-Groups"> &para;</a></span></h4>
<a class="index-entry-id" id="index-scanning-the-group-list"></a>

<p>This section explains how a program can read the list of all groups in
the system, one group at a time.  The functions described here are
declared in <samp class="file">grp.h</samp>.
</p>
<p>You can use the <code class="code">fgetgrent</code> function to read group entries from a
particular file.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fgetgrent"><span class="category-def">Function: </span><span><code class="def-type">struct group *</code> <strong class="def-name">fgetgrent</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-fgetgrent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:fgrent
| AS-Unsafe corrupt lock
| AC-Unsafe corrupt lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fgetgrent</code> function reads the next entry from <var class="var">stream</var>.
It returns a pointer to the entry.  The structure is statically
allocated and is overwritten on subsequent calls to <code class="code">fgetgrent</code>.  You
must copy the contents of the structure if you wish to save the
information.
</p>
<p>The stream must correspond to a file in the same format as the standard
group database file.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fgetgrent_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fgetgrent_r</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, struct group *<var class="var">result_buf</var>, char *<var class="var">buffer</var>, size_t <var class="var">buflen</var>, struct group **<var class="var">result</var>)</code><a class="copiable-link" href="#index-fgetgrent_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe corrupt lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">fgetgrent</code> in that it reads the next
user entry from <var class="var">stream</var>.  But the result is returned in the
structure pointed to by <var class="var">result_buf</var>.  The first <var class="var">buflen</var> bytes
of the additional buffer pointed to by <var class="var">buffer</var> are used to contain
additional information, normally strings which are pointed to by the
elements of the result structure.
</p>
<p>This stream must correspond to a file in the same format as the standard
group database file.
</p>
<p>If the function returns zero <var class="var">result</var> points to the structure with
the wanted data (normally this is in <var class="var">result_buf</var>).  If errors
occurred the return value is non-zero and <var class="var">result</var> contains a null
pointer.
</p></dd></dl>

<p>The way to scan all the entries in the group database is with
<code class="code">setgrent</code>, <code class="code">getgrent</code>, and <code class="code">endgrent</code>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setgrent"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">setgrent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-setgrent"> &para;</a></span></dt>
<dd>

<p>Preliminary:
| MT-Unsafe race:grent locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function initializes a stream for reading from the group data base.
You use this stream by calling <code class="code">getgrent</code> or <code class="code">getgrent_r</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getgrent"><span class="category-def">Function: </span><span><code class="def-type">struct group *</code> <strong class="def-name">getgrent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-getgrent"> &para;</a></span></dt>
<dd>

<p>Preliminary:
| MT-Unsafe race:grent race:grentbuf locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getgrent</code> function reads the next entry from the stream
initialized by <code class="code">setgrent</code>.  It returns a pointer to the entry.  The
structure is statically allocated and is overwritten on subsequent calls
to <code class="code">getgrent</code>.  You must copy the contents of the structure if you
wish to save the information.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getgrent_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getgrent_r</strong> <code class="def-code-arguments">(struct group *<var class="var">result_buf</var>, char *<var class="var">buffer</var>, size_t <var class="var">buflen</var>, struct group **<var class="var">result</var>)</code><a class="copiable-link" href="#index-getgrent_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:grent locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">getgrent</code> in that it returns the next
entry from the stream initialized by <code class="code">setgrent</code>.  Like
<code class="code">fgetgrent_r</code>, it places the result in user-supplied buffers
pointed to by <var class="var">result_buf</var> and <var class="var">buffer</var>.
</p>
<p>If the function returns zero <var class="var">result</var> contains a pointer to the data
(normally equal to <var class="var">result_buf</var>).  If errors occurred the return
value is non-zero and <var class="var">result</var> contains a null pointer.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-endgrent"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">endgrent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-endgrent"> &para;</a></span></dt>
<dd>

<p>Preliminary:
| MT-Unsafe race:grent locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function closes the internal stream used by <code class="code">getgrent</code> or
<code class="code">getgrent_r</code>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Lookup-Group.html">Looking Up One Group</a>, Up: <a href="Group-Database.html">Group Database</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
