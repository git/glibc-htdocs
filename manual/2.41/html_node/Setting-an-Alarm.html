<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Setting an Alarm (The GNU C Library)</title>

<meta name="description" content="Setting an Alarm (The GNU C Library)">
<meta name="keywords" content="Setting an Alarm (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Date-and-Time.html" rel="up" title="Date and Time">
<link href="Sleeping.html" rel="next" title="Sleeping">
<link href="Calendar-Time.html" rel="prev" title="Calendar Time">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Setting-an-Alarm">
<div class="nav-panel">
<p>
Next: <a href="Sleeping.html" accesskey="n" rel="next">Sleeping</a>, Previous: <a href="Calendar-Time.html" accesskey="p" rel="prev">Calendar Time</a>, Up: <a href="Date-and-Time.html" accesskey="u" rel="up">Date and Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Setting-an-Alarm-1"><span>22.6 Setting an Alarm<a class="copiable-link" href="#Setting-an-Alarm-1"> &para;</a></span></h3>

<p>The <code class="code">alarm</code> and <code class="code">setitimer</code> functions provide a mechanism for a
process to interrupt itself in the future.  They do this by setting a
timer; when the timer expires, the process receives a signal.
</p>
<a class="index-entry-id" id="index-setting-an-alarm"></a>
<a class="index-entry-id" id="index-interval-timer_002c-setting"></a>
<a class="index-entry-id" id="index-alarms_002c-setting"></a>
<a class="index-entry-id" id="index-timers_002c-setting"></a>
<p>Each process has three independent interval timers available:
</p>
<ul class="itemize mark-bullet">
<li>A real-time timer that counts elapsed time.  This timer sends a
<code class="code">SIGALRM</code> signal to the process when it expires.
<a class="index-entry-id" id="index-real_002dtime-timer"></a>
<a class="index-entry-id" id="index-timer_002c-real_002dtime"></a>

</li><li>A virtual timer that counts processor time used by the process.  This timer
sends a <code class="code">SIGVTALRM</code> signal to the process when it expires.
<a class="index-entry-id" id="index-virtual-timer"></a>
<a class="index-entry-id" id="index-timer_002c-virtual"></a>

</li><li>A profiling timer that counts both processor time used by the process,
and processor time spent in system calls on behalf of the process.  This
timer sends a <code class="code">SIGPROF</code> signal to the process when it expires.
<a class="index-entry-id" id="index-profiling-timer"></a>
<a class="index-entry-id" id="index-timer_002c-profiling"></a>

<p>This timer is useful for profiling in interpreters.  The interval timer
mechanism does not have the fine granularity necessary for profiling
native code.
</p></li></ul>

<p>You can only have one timer of each kind set at any given time.  If you
set a timer that has not yet expired, that timer is simply reset to the
new value.
</p>
<p>You should establish a handler for the appropriate alarm signal using
<code class="code">signal</code> or <code class="code">sigaction</code> before issuing a call to
<code class="code">setitimer</code> or <code class="code">alarm</code>.  Otherwise, an unusual chain of events
could cause the timer to expire before your program establishes the
handler.  In this case it would be terminated, since termination is the
default action for the alarm signals.  See <a class="xref" href="Signal-Handling.html">Signal Handling</a>.
</p>
<p>To be able to use the alarm function to interrupt a system call which
might block otherwise indefinitely it is important to <em class="emph">not</em> set the
<code class="code">SA_RESTART</code> flag when registering the signal handler using
<code class="code">sigaction</code>.  When not using <code class="code">sigaction</code> things get even
uglier: the <code class="code">signal</code> function has fixed semantics with respect
to restarts.  The BSD semantics for this function is to set the flag.
Therefore, if <code class="code">sigaction</code> for whatever reason cannot be used, it is
necessary to use <code class="code">sysv_signal</code> and not <code class="code">signal</code>.
</p>
<p>The <code class="code">setitimer</code> function is the primary means for setting an alarm.
This facility is declared in the header file <samp class="file">sys/time.h</samp>.  The
<code class="code">alarm</code> function, declared in <samp class="file">unistd.h</samp>, provides a somewhat
simpler interface for setting the real-time timer.
<a class="index-entry-id" id="index-unistd_002eh-14"></a>
<a class="index-entry-id" id="index-sys_002ftime_002eh-1"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-itimerval"><span class="category-def">Data Type: </span><span><strong class="def-name">struct itimerval</strong><a class="copiable-link" href="#index-struct-itimerval"> &para;</a></span></dt>
<dd>
<p>This structure is used to specify when a timer should expire.  It contains
the following members:
</p><dl class="table">
<dt><code class="code">struct timeval it_interval</code></dt>
<dd><p>This is the period between successive timer interrupts.  If zero, the
alarm will only be sent once.
</p>
</dd>
<dt><code class="code">struct timeval it_value</code></dt>
<dd><p>This is the period between now and the first timer interrupt.  If zero,
the alarm is disabled.
</p></dd>
</dl>

<p>The <code class="code">struct timeval</code> data type is described in <a class="ref" href="Time-Types.html">Time Types</a>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setitimer"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">setitimer</strong> <code class="def-code-arguments">(int <var class="var">which</var>, const struct itimerval *<var class="var">new</var>, struct itimerval *<var class="var">old</var>)</code><a class="copiable-link" href="#index-setitimer"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe timer
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">setitimer</code> function sets the timer specified by <var class="var">which</var>
according to <var class="var">new</var>.  The <var class="var">which</var> argument can have a value of
<code class="code">ITIMER_REAL</code>, <code class="code">ITIMER_VIRTUAL</code>, or <code class="code">ITIMER_PROF</code>.
</p>
<p>If <var class="var">old</var> is not a null pointer, <code class="code">setitimer</code> returns information
about any previous unexpired timer of the same kind in the structure it
points to.
</p>
<p>The return value is <code class="code">0</code> on success and <code class="code">-1</code> on failure.  The
following <code class="code">errno</code> error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The timer period is too large.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getitimer"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getitimer</strong> <code class="def-code-arguments">(int <var class="var">which</var>, struct itimerval *<var class="var">old</var>)</code><a class="copiable-link" href="#index-getitimer"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getitimer</code> function stores information about the timer specified
by <var class="var">which</var> in the structure pointed at by <var class="var">old</var>.
</p>
<p>The return value and error conditions are the same as for <code class="code">setitimer</code>.
</p></dd></dl>

<dl class="vtable">
<dt><a id="index-ITIMER_005fREAL"></a><span><code class="code">ITIMER_REAL</code><a class="copiable-link" href="#index-ITIMER_005fREAL"> &para;</a></span></dt>
<dd>
<p>This constant can be used as the <var class="var">which</var> argument to the
<code class="code">setitimer</code> and <code class="code">getitimer</code> functions to specify the real-time
timer.
</p>
</dd>
<dt><a id="index-ITIMER_005fVIRTUAL"></a><span><code class="code">ITIMER_VIRTUAL</code><a class="copiable-link" href="#index-ITIMER_005fVIRTUAL"> &para;</a></span></dt>
<dd>
<p>This constant can be used as the <var class="var">which</var> argument to the
<code class="code">setitimer</code> and <code class="code">getitimer</code> functions to specify the virtual
timer.
</p>
</dd>
<dt><a id="index-ITIMER_005fPROF"></a><span><code class="code">ITIMER_PROF</code><a class="copiable-link" href="#index-ITIMER_005fPROF"> &para;</a></span></dt>
<dd>
<p>This constant can be used as the <var class="var">which</var> argument to the
<code class="code">setitimer</code> and <code class="code">getitimer</code> functions to specify the profiling
timer.
</p></dd>
</dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-alarm"><span class="category-def">Function: </span><span><code class="def-type">unsigned int</code> <strong class="def-name">alarm</strong> <code class="def-code-arguments">(unsigned int <var class="var">seconds</var>)</code><a class="copiable-link" href="#index-alarm"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe timer
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">alarm</code> function sets the real-time timer to expire in
<var class="var">seconds</var> seconds.  If you want to cancel any existing alarm, you
can do this by calling <code class="code">alarm</code> with a <var class="var">seconds</var> argument of
zero.
</p>
<p>The return value indicates how many seconds remain before the previous
alarm would have been sent.  If there was no previous alarm, <code class="code">alarm</code>
returns zero.
</p></dd></dl>

<p>The <code class="code">alarm</code> function could be defined in terms of <code class="code">setitimer</code>
like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">unsigned int
alarm (unsigned int seconds)
{
  struct itimerval old, new;
  new.it_interval.tv_usec = 0;
  new.it_interval.tv_sec = 0;
  new.it_value.tv_usec = 0;
  new.it_value.tv_sec = (long int) seconds;
  if (setitimer (ITIMER_REAL, &amp;new, &amp;old) &lt; 0)
    return 0;
  else
    return old.it_value.tv_sec;
}
</pre></div>

<p>There is an example showing the use of the <code class="code">alarm</code> function in
<a class="ref" href="Handler-Returns.html">Signal Handlers that Return</a>.
</p>
<p>If you simply want your process to wait for a given number of seconds,
you should use the <code class="code">sleep</code> function.  See <a class="xref" href="Sleeping.html">Sleeping</a>.
</p>
<p>You shouldn&rsquo;t count on the signal arriving precisely when the timer
expires.  In a multiprocessing environment there is typically some
amount of delay involved.
</p>
<p><strong class="strong">Portability Note:</strong> The <code class="code">setitimer</code> and <code class="code">getitimer</code>
functions are derived from BSD Unix, while the <code class="code">alarm</code> function is
specified by POSIX.  <code class="code">setitimer</code> is more powerful than
<code class="code">alarm</code>, but <code class="code">alarm</code> is more widely used.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Sleeping.html">Sleeping</a>, Previous: <a href="Calendar-Time.html">Calendar Time</a>, Up: <a href="Date-and-Time.html">Date and Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
