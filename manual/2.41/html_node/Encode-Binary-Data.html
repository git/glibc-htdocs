<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Encode Binary Data (The GNU C Library)</title>

<meta name="description" content="Encode Binary Data (The GNU C Library)">
<meta name="keywords" content="Encode Binary Data (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="String-and-Array-Utilities.html" rel="up" title="String and Array Utilities">
<link href="Argz-and-Envz-Vectors.html" rel="next" title="Argz and Envz Vectors">
<link href="Obfuscating-Data.html" rel="prev" title="Obfuscating Data">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Encode-Binary-Data">
<div class="nav-panel">
<p>
Next: <a href="Argz-and-Envz-Vectors.html" accesskey="n" rel="next">Argz and Envz Vectors</a>, Previous: <a href="Obfuscating-Data.html" accesskey="p" rel="prev">Obfuscating Data</a>, Up: <a href="String-and-Array-Utilities.html" accesskey="u" rel="up">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Encode-Binary-Data-1"><span>5.14 Encode Binary Data<a class="copiable-link" href="#Encode-Binary-Data-1"> &para;</a></span></h3>

<p>To store or transfer binary data in environments which only support text
one has to encode the binary data by mapping the input bytes to
bytes in the range allowed for storing or transferring.  SVID
systems (and nowadays XPG compliant systems) provide minimal support for
this task.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-l64a"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">l64a</strong> <code class="def-code-arguments">(long int <var class="var">n</var>)</code><a class="copiable-link" href="#index-l64a"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:l64a
| AS-Unsafe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function encodes a 32-bit input value using bytes from the
basic character set.  It returns a pointer to a 7 byte buffer which
contains an encoded version of <var class="var">n</var>.  To encode a series of bytes the
user must copy the returned string to a destination buffer.  It returns
the empty string if <var class="var">n</var> is zero, which is somewhat bizarre but
mandated by the standard.<br>
<strong class="strong">Warning:</strong> Since a static buffer is used this function should not
be used in multi-threaded programs.  There is no thread-safe alternative
to this function in the C library.<br>
<strong class="strong">Compatibility Note:</strong> The XPG standard states that the return
value of <code class="code">l64a</code> is undefined if <var class="var">n</var> is negative.  In the GNU
implementation, <code class="code">l64a</code> treats its argument as unsigned, so it will
return a sensible encoding for any nonzero <var class="var">n</var>; however, portable
programs should not rely on this.
</p>
<p>To encode a large buffer <code class="code">l64a</code> must be called in a loop, once for
each 32-bit word of the buffer.  For example, one could do something
like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">char *
encode (const void *buf, size_t len)
{
  /* <span class="r">We know in advance how long the buffer has to be.</span> */
  unsigned char *in = (unsigned char *) buf;
  char *out = malloc (6 + ((len + 3) / 4) * 6 + 1);
  char *cp = out, *p;

  /* <span class="r">Encode the length.</span> */
  /* <span class="r">Using &lsquo;htonl&rsquo; is necessary so that the data can be</span>
     <span class="r">decoded even on machines with different byte order.</span>
     <span class="r">&lsquo;l64a&rsquo; can return a string shorter than 6 bytes, so </span>
     <span class="r">we pad it with encoding of 0 (</span>'.'<span class="r">) at the end by </span>
     <span class="r">hand.</span> */

  p = stpcpy (cp, l64a (htonl (len)));
  cp = mempcpy (p, &quot;......&quot;, 6 - (p - cp));

  while (len &gt; 3)
    {
      unsigned long int n = *in++;
      n = (n &lt;&lt; 8) | *in++;
      n = (n &lt;&lt; 8) | *in++;
      n = (n &lt;&lt; 8) | *in++;
      len -= 4;
      p = stpcpy (cp, l64a (htonl (n)));
      cp = mempcpy (p, &quot;......&quot;, 6 - (p - cp));
    }
  if (len &gt; 0)
    {
      unsigned long int n = *in++;
      if (--len &gt; 0)
        {
          n = (n &lt;&lt; 8) | *in++;
          if (--len &gt; 0)
            n = (n &lt;&lt; 8) | *in;
        }
      cp = stpcpy (cp, l64a (htonl (n)));
    }
  *cp = '\0';
  return out;
}
</pre></div>

<p>It is strange that the library does not provide the complete
functionality needed but so be it.
</p>
</dd></dl>

<p>To decode data produced with <code class="code">l64a</code> the following function should be
used.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-a64l"><span class="category-def">Function: </span><span><code class="def-type">long int</code> <strong class="def-name">a64l</strong> <code class="def-code-arguments">(const char *<var class="var">string</var>)</code><a class="copiable-link" href="#index-a64l"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The parameter <var class="var">string</var> should contain a string which was produced by
a call to <code class="code">l64a</code>.  The function processes at least 6 bytes of
this string, and decodes the bytes it finds according to the table
below.  It stops decoding when it finds a byte not in the table,
rather like <code class="code">atoi</code>; if you have a buffer which has been broken into
lines, you must be careful to skip over the end-of-line bytes.
</p>
<p>The decoded number is returned as a <code class="code">long int</code> value.
</p></dd></dl>

<p>The <code class="code">l64a</code> and <code class="code">a64l</code> functions use a base 64 encoding, in
which each byte of an encoded string represents six bits of an
input word.  These symbols are used for the base 64 digits:
</p>
<table class="multitable">
<tbody><tr><td></td><td>0</td><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td></tr>
<tr><td>0</td><td><code class="code">.</code></td><td><code class="code">/</code></td><td><code class="code">0</code></td><td><code class="code">1</code></td><td><code class="code">2</code></td><td><code class="code">3</code></td><td><code class="code">4</code></td><td><code class="code">5</code></td></tr>
<tr><td>8</td><td><code class="code">6</code></td><td><code class="code">7</code></td><td><code class="code">8</code></td><td><code class="code">9</code></td><td><code class="code">A</code></td><td><code class="code">B</code></td><td><code class="code">C</code></td><td><code class="code">D</code></td></tr>
<tr><td>16</td><td><code class="code">E</code></td><td><code class="code">F</code></td><td><code class="code">G</code></td><td><code class="code">H</code></td><td><code class="code">I</code></td><td><code class="code">J</code></td><td><code class="code">K</code></td><td><code class="code">L</code></td></tr>
<tr><td>24</td><td><code class="code">M</code></td><td><code class="code">N</code></td><td><code class="code">O</code></td><td><code class="code">P</code></td><td><code class="code">Q</code></td><td><code class="code">R</code></td><td><code class="code">S</code></td><td><code class="code">T</code></td></tr>
<tr><td>32</td><td><code class="code">U</code></td><td><code class="code">V</code></td><td><code class="code">W</code></td><td><code class="code">X</code></td><td><code class="code">Y</code></td><td><code class="code">Z</code></td><td><code class="code">a</code></td><td><code class="code">b</code></td></tr>
<tr><td>40</td><td><code class="code">c</code></td><td><code class="code">d</code></td><td><code class="code">e</code></td><td><code class="code">f</code></td><td><code class="code">g</code></td><td><code class="code">h</code></td><td><code class="code">i</code></td><td><code class="code">j</code></td></tr>
<tr><td>48</td><td><code class="code">k</code></td><td><code class="code">l</code></td><td><code class="code">m</code></td><td><code class="code">n</code></td><td><code class="code">o</code></td><td><code class="code">p</code></td><td><code class="code">q</code></td><td><code class="code">r</code></td></tr>
<tr><td>56</td><td><code class="code">s</code></td><td><code class="code">t</code></td><td><code class="code">u</code></td><td><code class="code">v</code></td><td><code class="code">w</code></td><td><code class="code">x</code></td><td><code class="code">y</code></td><td><code class="code">z</code></td></tr>
</tbody>
</table>

<p>This encoding scheme is not standard.  There are some other encoding
methods which are much more widely used (UU encoding, MIME encoding).
Generally, it is better to use one of these encodings.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Argz-and-Envz-Vectors.html">Argz and Envz Vectors</a>, Previous: <a href="Obfuscating-Data.html">Obfuscating Data</a>, Up: <a href="String-and-Array-Utilities.html">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
