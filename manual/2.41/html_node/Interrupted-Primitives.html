<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Interrupted Primitives (The GNU C Library)</title>

<meta name="description" content="Interrupted Primitives (The GNU C Library)">
<meta name="keywords" content="Interrupted Primitives (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Signal-Handling.html" rel="up" title="Signal Handling">
<link href="Generating-Signals.html" rel="next" title="Generating Signals">
<link href="Defining-Handlers.html" rel="prev" title="Defining Handlers">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Interrupted-Primitives">
<div class="nav-panel">
<p>
Next: <a href="Generating-Signals.html" accesskey="n" rel="next">Generating Signals</a>, Previous: <a href="Defining-Handlers.html" accesskey="p" rel="prev">Defining Signal Handlers</a>, Up: <a href="Signal-Handling.html" accesskey="u" rel="up">Signal Handling</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Primitives-Interrupted-by-Signals"><span>25.5 Primitives Interrupted by Signals<a class="copiable-link" href="#Primitives-Interrupted-by-Signals"> &para;</a></span></h3>

<p>A signal can arrive and be handled while an I/O primitive such as
<code class="code">open</code> or <code class="code">read</code> is waiting for an I/O device.  If the signal
handler returns, the system faces the question: what should happen next?
</p>
<p>POSIX specifies one approach: make the primitive fail right away.  The
error code for this kind of failure is <code class="code">EINTR</code>.  This is flexible,
but usually inconvenient.  Typically, POSIX applications that use signal
handlers must check for <code class="code">EINTR</code> after each library function that
can return it, in order to try the call again.  Often programmers forget
to check, which is a common source of error.
</p>
<p>The GNU C Library provides a convenient way to retry a call after a
temporary failure, with the macro <code class="code">TEMP_FAILURE_RETRY</code>:
</p>
<dl class="first-deffn first-defmac-alias-first-deffn">
<dt class="deffn defmac-alias-deffn" id="index-TEMP_005fFAILURE_005fRETRY"><span class="category-def">Macro: </span><span><strong class="def-name">TEMP_FAILURE_RETRY</strong> <var class="def-var-arguments">(<var class="var">expression</var>)</var><a class="copiable-link" href="#index-TEMP_005fFAILURE_005fRETRY"> &para;</a></span></dt>
<dd>
<p>This macro evaluates <var class="var">expression</var> once, and examines its value as
type <code class="code">long int</code>.  If the value equals <code class="code">-1</code>, that indicates a
failure and <code class="code">errno</code> should be set to show what kind of failure.
If it fails and reports error code <code class="code">EINTR</code>,
<code class="code">TEMP_FAILURE_RETRY</code> evaluates it again, and over and over until
the result is not a temporary failure.
</p>
<p>The value returned by <code class="code">TEMP_FAILURE_RETRY</code> is whatever value
<var class="var">expression</var> produced.
</p></dd></dl>

<p>BSD avoids <code class="code">EINTR</code> entirely and provides a more convenient
approach: to restart the interrupted primitive, instead of making it
fail.  If you choose this approach, you need not be concerned with
<code class="code">EINTR</code>.
</p>
<p>You can choose either approach with the GNU C Library.  If you use
<code class="code">sigaction</code> to establish a signal handler, you can specify how that
handler should behave.  If you specify the <code class="code">SA_RESTART</code> flag,
return from that handler will resume a primitive; otherwise, return from
that handler will cause <code class="code">EINTR</code>.  See <a class="xref" href="Flags-for-Sigaction.html">Flags for <code class="code">sigaction</code></a>.
</p>
<p>Another way to specify the choice is with the <code class="code">siginterrupt</code>
function.  See <a class="xref" href="BSD-Signal-Handling.html">BSD Signal Handling</a>.
</p>
<p>When you don&rsquo;t specify with <code class="code">sigaction</code> or <code class="code">siginterrupt</code> what
a particular handler should do, it uses a default choice.  The default
choice in the GNU C Library is to make primitives fail with <code class="code">EINTR</code>.
<a class="index-entry-id" id="index-EINTR_002c-and-restarting-interrupted-primitives"></a>
<a class="index-entry-id" id="index-restarting-interrupted-primitives"></a>
<a class="index-entry-id" id="index-interrupting-primitives"></a>
<a class="index-entry-id" id="index-primitives_002c-interrupting"></a>
</p>
<p>The description of each primitive affected by this issue
lists <code class="code">EINTR</code> among the error codes it can return.
</p>
<p>There is one situation where resumption never happens no matter which
choice you make: when a data-transfer function such as <code class="code">read</code> or
<code class="code">write</code> is interrupted by a signal after transferring part of the
data.  In this case, the function returns the number of bytes already
transferred, indicating partial success.
</p>
<p>This might at first appear to cause unreliable behavior on
record-oriented devices (including datagram sockets; see <a class="pxref" href="Datagrams.html">Datagram Socket Operations</a>),
where splitting one <code class="code">read</code> or <code class="code">write</code> into two would read or
write two records.  Actually, there is no problem, because interruption
after a partial transfer cannot happen on such devices; they always
transfer an entire record in one burst, with no waiting once data
transfer has started.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Generating-Signals.html">Generating Signals</a>, Previous: <a href="Defining-Handlers.html">Defining Signal Handlers</a>, Up: <a href="Signal-Handling.html">Signal Handling</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
