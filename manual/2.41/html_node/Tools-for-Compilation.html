<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Tools for Compilation (The GNU C Library)</title>

<meta name="description" content="Tools for Compilation (The GNU C Library)">
<meta name="keywords" content="Tools for Compilation (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Installation.html" rel="up" title="Installation">
<link href="Linux.html" rel="next" title="Linux">
<link href="Running-make-install.html" rel="prev" title="Running make install">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="appendixsec-level-extent" id="Tools-for-Compilation">
<div class="nav-panel">
<p>
Next: <a href="Linux.html" accesskey="n" rel="next">Specific advice for GNU/Linux systems</a>, Previous: <a href="Running-make-install.html" accesskey="p" rel="prev">Installing the C Library</a>, Up: <a href="Installation.html" accesskey="u" rel="up">Installing the GNU C Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="appendixsec" id="Recommended-Tools-for-Compilation"><span>C.3 Recommended Tools for Compilation<a class="copiable-link" href="#Recommended-Tools-for-Compilation"> &para;</a></span></h3>
<a class="index-entry-id" id="index-installation-tools"></a>
<a class="index-entry-id" id="index-tools_002c-for-installing-library"></a>

<p>We recommend installing the following GNU tools before attempting to
build the GNU C Library:
</p>
<ul class="itemize mark-bullet">
<li>GNU <code class="code">make</code> 4.0 or newer

<p>As of release time, GNU <code class="code">make</code> 4.4.1 is the newest verified to work
to build the GNU C Library.
</p>
</li><li>GCC 6.2 or newer

<p>GCC 6.2 or higher is required.  In general it is recommended to use
the newest version of the compiler that is known to work for building
the GNU C Library, as newer compilers usually produce better code.  As of
release time, GCC 14.2.1 is the newest compiler verified to work to build
the GNU C Library.
</p>
<p>For PowerPC 64-bits little-endian (powerpc64le), a GCC version with support
for <samp class="option">-mno-gnu-attribute</samp>, <samp class="option">-mabi=ieeelongdouble</samp>, and
<samp class="option">-mabi=ibmlongdouble</samp> is required.  Likewise, the compiler must also
support passing <samp class="option">-mlong-double-128</samp> with the preceding options.  As
of release, this implies GCC 7.4 and newer (excepting GCC 7.5.0, see GCC
PR94200).  These additional features are required for building the GNU C
Library with support for IEEE long double.
</p>

<p>For ARC architecture builds, GCC 8.3 or higher is needed.
</p>
<p>For s390x architecture builds, GCC 7.1 or higher is needed (See gcc Bug 98269).
</p>
<p>For AArch64 architecture builds with mathvec enabled, GCC 10 or higher is needed
due to dependency on arm_sve.h.
</p>
<p>For multi-arch support it is recommended to use a GCC which has been built with
support for GNU indirect functions.  This ensures that correct debugging
information is generated for functions selected by IFUNC resolvers.  This
support can either be enabled by configuring GCC with
&lsquo;<samp class="samp">--enable-gnu-indirect-function</samp>&rsquo;, or by enabling it by default by setting
&lsquo;<samp class="samp">default_gnu_indirect_function</samp>&rsquo; variable for a particular architecture in
the GCC source file <samp class="file">gcc/config.gcc</samp>.
</p>
<p>You can use whatever compiler you like to compile programs that use
the GNU C Library.
</p>
<p>Check the FAQ for any special compiler issues on particular platforms.
</p>
</li><li>GNU <code class="code">binutils</code> 2.25 or later

<p>You must use GNU <code class="code">binutils</code> (as and ld) to build the GNU C Library.
No other assembler or linker has the necessary functionality at the
moment. As of release time, GNU <code class="code">binutils</code> 2.43.1 is the newest
verified to work to build the GNU C Library.
</p>
<p>For PowerPC 64-bits little-endian (powerpc64le), <code class="command">objcopy</code> is required
to support <samp class="option">--update-section</samp>.  This option requires binutils 2.26 or
newer.
</p>
<p>ARC architecture needs <code class="code">binutils</code> 2.32 or higher for TLS related fixes.
</p>
</li><li>GNU <code class="code">texinfo</code> 4.7 or later

<p>To correctly translate and install the Texinfo documentation you need
this version of the <code class="code">texinfo</code> package.  Earlier versions do not
understand all the tags used in the document, and the installation
mechanism for the info files is not present or works differently.
As of release time, <code class="code">texinfo</code> 7.2 is the newest verified to work
to build the GNU C Library.
</p>
</li><li>GNU <code class="code">awk</code> 3.1.2, or higher

<p><code class="code">awk</code> is used in several places to generate files.
Some <code class="code">gawk</code> extensions are used, including the <code class="code">asorti</code>
function, which was introduced in version 3.1.2 of <code class="code">gawk</code>.
As of release time, <code class="code">gawk</code> version 5.3.1 is the newest verified
to work to build the GNU C Library.
</p>
<p>Testing the GNU C Library requires <code class="code">gawk</code> to be compiled with
support for high precision arithmetic via the <code class="code">MPFR</code>
multiple-precision floating-point computation library.
</p>
</li><li>GNU <code class="code">bison</code> 2.7 or later

<p><code class="code">bison</code> is used to generate the <code class="code">yacc</code> parser code in the <samp class="file">intl</samp>
subdirectory.  As of release time, <code class="code">bison</code> version 3.8.2 is the newest
verified to work to build the GNU C Library.
</p>
</li><li>Perl 5

<p>Perl is not required, but if present it is used in some tests and the
<code class="code">mtrace</code> program, to build the GNU C Library manual.  As of release
time <code class="code">perl</code> version 5.40.0 is the newest verified to work to
build the GNU C Library.
</p>
</li><li>GNU <code class="code">sed</code> 3.02 or newer

<p><code class="code">Sed</code> is used in several places to generate files.  Most scripts work
with any version of <code class="code">sed</code>.  As of release time, <code class="code">sed</code> version
4.9 is the newest verified to work to build the GNU C Library.
</p>
</li><li>Python 3.4 or later

<p>Python is required to build the GNU C Library.  As of release time, Python
3.12.8 is the newest verified to work for building and testing
the GNU C Library.
</p>
</li><li>PExpect 4.0

<p>The pretty printer tests drive GDB through test programs and compare
its output to the printers&rsquo;.  PExpect is used to capture the output of
GDB, and should be compatible with the Python version in your system.
As of release time PExpect 4.9.0 is the newest verified to work to test
the pretty printers.
</p>
</li><li>The Python <code class="code">abnf</code> module.

<p>This module is optional and used to verify some ABNF grammars in the
manual.  Version 2.2.0 has been confirmed to work as expected.  A
missing <code class="code">abnf</code> module does not reduce the test coverage of the
library itself.
</p>
</li><li>GDB 7.8 or later with support for Python 2.7/3.4 or later

<p>GDB itself needs to be configured with Python support in order to use
the pretty printers.  Notice that your system having Python available
doesn&rsquo;t imply that GDB supports it, nor that your system&rsquo;s Python and
GDB&rsquo;s have the same version.  As of release time GNU <code class="code">debugger</code>
14.2 is the newest verified to work to test the pretty printers.
</p>
<p>Unless Python, PExpect and GDB with Python support are present, the
printer tests will report themselves as <code class="code">UNSUPPORTED</code>.  Notice
that some of the printer tests require the GNU C Library to be compiled with
debugging symbols.
</p></li></ul>

<p>If you change any of the <samp class="file">configure.ac</samp> files you will also need
</p>
<ul class="itemize mark-bullet">
<li>GNU <code class="code">autoconf</code> 2.72 (exactly)
</li></ul>

<p>and if you change any of the message translation files you will need
</p>
<ul class="itemize mark-bullet">
<li>GNU <code class="code">gettext</code> 0.10.36 or later

<p>As of release time, GNU <code class="code">gettext</code> version 0.23 is the newest
version verified to work to build the GNU C Library.
</p></li></ul>


<p>You may also need these packages if you upgrade your source tree using
patches, although we try to avoid this.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Linux.html">Specific advice for GNU/Linux systems</a>, Previous: <a href="Running-make-install.html">Installing the C Library</a>, Up: <a href="Installation.html">Installing the GNU C Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
