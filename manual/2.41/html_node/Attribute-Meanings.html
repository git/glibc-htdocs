<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Attribute Meanings (The GNU C Library)</title>

<meta name="description" content="Attribute Meanings (The GNU C Library)">
<meta name="keywords" content="Attribute Meanings (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-Attributes.html" rel="up" title="File Attributes">
<link href="Reading-Attributes.html" rel="next" title="Reading Attributes">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Attribute-Meanings">
<div class="nav-panel">
<p>
Next: <a href="Reading-Attributes.html" accesskey="n" rel="next">Reading the Attributes of a File</a>, Up: <a href="File-Attributes.html" accesskey="u" rel="up">File Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="The-meaning-of-the-File-Attributes"><span>14.10.1 The meaning of the File Attributes<a class="copiable-link" href="#The-meaning-of-the-File-Attributes"> &para;</a></span></h4>
<a class="index-entry-id" id="index-status-of-a-file"></a>
<a class="index-entry-id" id="index-attributes-of-a-file"></a>
<a class="index-entry-id" id="index-file-attributes"></a>

<p>When you read the attributes of a file, they come back in a structure
called <code class="code">struct stat</code>.  This section describes the names of the
attributes, their data types, and what they mean.  For the functions
to read the attributes of a file, see <a class="ref" href="Reading-Attributes.html">Reading the Attributes of a File</a>.
</p>
<p>The header file <samp class="file">sys/stat.h</samp> declares all the symbols defined
in this section.
<a class="index-entry-id" id="index-sys_002fstat_002eh-2"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-stat"><span class="category-def">Data Type: </span><span><strong class="def-name">struct stat</strong><a class="copiable-link" href="#index-struct-stat"> &para;</a></span></dt>
<dd>
<p>The <code class="code">stat</code> structure type is used to return information about the
attributes of a file.  It contains at least the following members:
</p>
<dl class="table">
<dt><code class="code">mode_t st_mode</code></dt>
<dd><p>Specifies the mode of the file.  This includes file type information
(see <a class="pxref" href="Testing-File-Type.html">Testing the Type of a File</a>) and the file permission bits
(see <a class="pxref" href="Permission-Bits.html">The Mode Bits for Access Permission</a>).
</p>
</dd>
<dt><code class="code">ino_t st_ino</code></dt>
<dd><p>The file serial number, which distinguishes this file from all other
files on the same device.
</p>
</dd>
<dt><code class="code">dev_t st_dev</code></dt>
<dd><p>Identifies the device containing the file.  The <code class="code">st_ino</code> and
<code class="code">st_dev</code>, taken together, uniquely identify the file.  The
<code class="code">st_dev</code> value is not necessarily consistent across reboots or
system crashes, however.
</p>
</dd>
<dt><code class="code">nlink_t st_nlink</code></dt>
<dd><p>The number of hard links to the file.  This count keeps track of how
many directories have entries for this file.  If the count is ever
decremented to zero, then the file itself is discarded as soon as no
process still holds it open.  Symbolic links are not counted in the
total.
</p>
</dd>
<dt><code class="code">uid_t st_uid</code></dt>
<dd><p>The user ID of the file&rsquo;s owner.  See <a class="xref" href="File-Owner.html">File Owner</a>.
</p>
</dd>
<dt><code class="code">gid_t st_gid</code></dt>
<dd><p>The group ID of the file.  See <a class="xref" href="File-Owner.html">File Owner</a>.
</p>
</dd>
<dt><code class="code">off_t st_size</code></dt>
<dd><p>This specifies the size of a regular file in bytes.  For files that are
really devices this field isn&rsquo;t usually meaningful.  For symbolic links
this specifies the length of the file name the link refers to.
</p>
</dd>
<dt><code class="code">time_t st_atime</code></dt>
<dd><p>This is the last access time for the file.  See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">unsigned long int st_atime_usec</code></dt>
<dd><p>This is the fractional part of the last access time for the file.
See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">time_t st_mtime</code></dt>
<dd><p>This is the time of the last modification to the contents of the file.
See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">unsigned long int st_mtime_usec</code></dt>
<dd><p>This is the fractional part of the time of the last modification to the
contents of the file.  See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">time_t st_ctime</code></dt>
<dd><p>This is the time of the last modification to the attributes of the file.
See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">unsigned long int st_ctime_usec</code></dt>
<dd><p>This is the fractional part of the time of the last modification to the
attributes of the file.  See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">blkcnt_t st_blocks</code></dt>
<dd><p>This is the amount of disk space that the file occupies, measured in
units of 512-byte blocks.
</p>
<p>The number of disk blocks is not strictly proportional to the size of
the file, for two reasons: the file system may use some blocks for
internal record keeping; and the file may be sparse&mdash;it may have
&ldquo;holes&rdquo; which contain zeros but do not actually take up space on the
disk.
</p>
<p>You can tell (approximately) whether a file is sparse by comparing this
value with <code class="code">st_size</code>, like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">(st.st_blocks * 512 &lt; st.st_size)
</pre></div>

<p>This test is not perfect because a file that is just slightly sparse
might not be detected as sparse at all.  For practical applications,
this is not a problem.
</p>
</dd>
<dt><code class="code">unsigned int st_blksize</code></dt>
<dd><p>The optimal block size for reading or writing this file, in bytes.  You
might use this size for allocating the buffer space for reading or
writing the file.  (This is unrelated to <code class="code">st_blocks</code>.)
</p></dd>
</dl>
</dd></dl>

<p>The extensions for the Large File Support (LFS) require, even on 32-bit
machines, types which can handle file sizes up to 2^63.
Therefore a new definition of <code class="code">struct stat</code> is necessary.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-stat64"><span class="category-def">Data Type: </span><span><strong class="def-name">struct stat64</strong><a class="copiable-link" href="#index-struct-stat64"> &para;</a></span></dt>
<dd>
<p>The members of this type are the same and have the same names as those
in <code class="code">struct stat</code>.  The only difference is that the members
<code class="code">st_ino</code>, <code class="code">st_size</code>, and <code class="code">st_blocks</code> have a different
type to support larger values.
</p>
<dl class="table">
<dt><code class="code">mode_t st_mode</code></dt>
<dd><p>Specifies the mode of the file.  This includes file type information
(see <a class="pxref" href="Testing-File-Type.html">Testing the Type of a File</a>) and the file permission bits
(see <a class="pxref" href="Permission-Bits.html">The Mode Bits for Access Permission</a>).
</p>
</dd>
<dt><code class="code">ino64_t st_ino</code></dt>
<dd><p>The file serial number, which distinguishes this file from all other
files on the same device.
</p>
</dd>
<dt><code class="code">dev_t st_dev</code></dt>
<dd><p>Identifies the device containing the file.  The <code class="code">st_ino</code> and
<code class="code">st_dev</code>, taken together, uniquely identify the file.  The
<code class="code">st_dev</code> value is not necessarily consistent across reboots or
system crashes, however.
</p>
</dd>
<dt><code class="code">nlink_t st_nlink</code></dt>
<dd><p>The number of hard links to the file.  This count keeps track of how
many directories have entries for this file.  If the count is ever
decremented to zero, then the file itself is discarded as soon as no
process still holds it open.  Symbolic links are not counted in the
total.
</p>
</dd>
<dt><code class="code">uid_t st_uid</code></dt>
<dd><p>The user ID of the file&rsquo;s owner.  See <a class="xref" href="File-Owner.html">File Owner</a>.
</p>
</dd>
<dt><code class="code">gid_t st_gid</code></dt>
<dd><p>The group ID of the file.  See <a class="xref" href="File-Owner.html">File Owner</a>.
</p>
</dd>
<dt><code class="code">off64_t st_size</code></dt>
<dd><p>This specifies the size of a regular file in bytes.  For files that are
really devices this field isn&rsquo;t usually meaningful.  For symbolic links
this specifies the length of the file name the link refers to.
</p>
</dd>
<dt><code class="code">time_t st_atime</code></dt>
<dd><p>This is the last access time for the file.  See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">unsigned long int st_atime_usec</code></dt>
<dd><p>This is the fractional part of the last access time for the file.
See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">time_t st_mtime</code></dt>
<dd><p>This is the time of the last modification to the contents of the file.
See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">unsigned long int st_mtime_usec</code></dt>
<dd><p>This is the fractional part of the time of the last modification to the
contents of the file.  See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">time_t st_ctime</code></dt>
<dd><p>This is the time of the last modification to the attributes of the file.
See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">unsigned long int st_ctime_usec</code></dt>
<dd><p>This is the fractional part of the time of the last modification to the
attributes of the file.  See <a class="xref" href="File-Times.html">File Times</a>.
</p>
</dd>
<dt><code class="code">blkcnt64_t st_blocks</code></dt>
<dd><p>This is the amount of disk space that the file occupies, measured in
units of 512-byte blocks.
</p>
</dd>
<dt><code class="code">unsigned int st_blksize</code></dt>
<dd><p>The optimal block size for reading of writing this file, in bytes.  You
might use this size for allocating the buffer space for reading of
writing the file.  (This is unrelated to <code class="code">st_blocks</code>.)
</p></dd>
</dl>
</dd></dl>

<p>Some of the file attributes have special data type names which exist
specifically for those attributes.  (They are all aliases for well-known
integer types that you know and love.)  These typedef names are defined
in the header file <samp class="file">sys/types.h</samp> as well as in <samp class="file">sys/stat.h</samp>.
Here is a list of them.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-mode_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">mode_t</strong><a class="copiable-link" href="#index-mode_005ft"> &para;</a></span></dt>
<dd>
<p>This is an integer data type used to represent file modes.  In
the GNU C Library, this is an unsigned type no narrower than <code class="code">unsigned
int</code>.
</p></dd></dl>

<a class="index-entry-id" id="index-inode-number"></a>
<dl class="first-deftp">
<dt class="deftp" id="index-ino_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">ino_t</strong><a class="copiable-link" href="#index-ino_005ft"> &para;</a></span></dt>
<dd>
<p>This is an unsigned integer type used to represent file serial numbers.
(In Unix jargon, these are sometimes called <em class="dfn">inode numbers</em>.)
In the GNU C Library, this type is no narrower than <code class="code">unsigned int</code>.
</p>
<p>If the source is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this type
is transparently replaced by <code class="code">ino64_t</code>.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-ino64_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">ino64_t</strong><a class="copiable-link" href="#index-ino64_005ft"> &para;</a></span></dt>
<dd>
<p>This is an unsigned integer type used to represent file serial numbers
for the use in LFS.  In the GNU C Library, this type is no narrower than
<code class="code">unsigned int</code>.
</p>
<p>When compiling with <code class="code">_FILE_OFFSET_BITS == 64</code> this type is
available under the name <code class="code">ino_t</code>.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-dev_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">dev_t</strong><a class="copiable-link" href="#index-dev_005ft"> &para;</a></span></dt>
<dd>
<p>This is an arithmetic data type used to represent file device numbers.
In the GNU C Library, this is an integer type no narrower than <code class="code">int</code>.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-nlink_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">nlink_t</strong><a class="copiable-link" href="#index-nlink_005ft"> &para;</a></span></dt>
<dd>
<p>This is an integer type used to represent file link counts.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-blkcnt_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">blkcnt_t</strong><a class="copiable-link" href="#index-blkcnt_005ft"> &para;</a></span></dt>
<dd>
<p>This is a signed integer type used to represent block counts.
In the GNU C Library, this type is no narrower than <code class="code">int</code>.
</p>
<p>If the source is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this type
is transparently replaced by <code class="code">blkcnt64_t</code>.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-blkcnt64_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">blkcnt64_t</strong><a class="copiable-link" href="#index-blkcnt64_005ft"> &para;</a></span></dt>
<dd>
<p>This is a signed integer type used to represent block counts for the
use in LFS.  In the GNU C Library, this type is no narrower than <code class="code">int</code>.
</p>
<p>When compiling with <code class="code">_FILE_OFFSET_BITS == 64</code> this type is
available under the name <code class="code">blkcnt_t</code>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Reading-Attributes.html">Reading the Attributes of a File</a>, Up: <a href="File-Attributes.html">File Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
