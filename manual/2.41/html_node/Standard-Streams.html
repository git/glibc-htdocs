<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Standard Streams (The GNU C Library)</title>

<meta name="description" content="Standard Streams (The GNU C Library)">
<meta name="keywords" content="Standard Streams (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="I_002fO-on-Streams.html" rel="up" title="I/O on Streams">
<link href="Opening-Streams.html" rel="next" title="Opening Streams">
<link href="Streams.html" rel="prev" title="Streams">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Standard-Streams">
<div class="nav-panel">
<p>
Next: <a href="Opening-Streams.html" accesskey="n" rel="next">Opening Streams</a>, Previous: <a href="Streams.html" accesskey="p" rel="prev">Streams</a>, Up: <a href="I_002fO-on-Streams.html" accesskey="u" rel="up">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Standard-Streams-1"><span>12.2 Standard Streams<a class="copiable-link" href="#Standard-Streams-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-standard-streams"></a>
<a class="index-entry-id" id="index-streams_002c-standard"></a>

<p>When the <code class="code">main</code> function of your program is invoked, it already has
three predefined streams open and available for use.  These represent
the &ldquo;standard&rdquo; input and output channels that have been established
for the process.
</p>
<p>These streams are declared in the header file <samp class="file">stdio.h</samp>.
<a class="index-entry-id" id="index-stdio_002eh-1"></a>
</p>
<dl class="first-deftypevr first-deftypevar-alias-first-deftypevr">
<dt class="deftypevr deftypevar-alias-deftypevr" id="index-stdin"><span class="category-def">Variable: </span><span><code class="def-type">FILE *</code> <strong class="def-name">stdin</strong><a class="copiable-link" href="#index-stdin"> &para;</a></span></dt>
<dd>
<p>The <em class="dfn">standard input</em> stream, which is the normal source of input for the
program.
</p></dd></dl>
<a class="index-entry-id" id="index-standard-input-stream"></a>

<dl class="first-deftypevr first-deftypevar-alias-first-deftypevr">
<dt class="deftypevr deftypevar-alias-deftypevr" id="index-stdout"><span class="category-def">Variable: </span><span><code class="def-type">FILE *</code> <strong class="def-name">stdout</strong><a class="copiable-link" href="#index-stdout"> &para;</a></span></dt>
<dd>
<p>The <em class="dfn">standard output</em> stream, which is used for normal output from
the program.
</p></dd></dl>
<a class="index-entry-id" id="index-standard-output-stream"></a>

<dl class="first-deftypevr first-deftypevar-alias-first-deftypevr">
<dt class="deftypevr deftypevar-alias-deftypevr" id="index-stderr"><span class="category-def">Variable: </span><span><code class="def-type">FILE *</code> <strong class="def-name">stderr</strong><a class="copiable-link" href="#index-stderr"> &para;</a></span></dt>
<dd>
<p>The <em class="dfn">standard error</em> stream, which is used for error messages and
diagnostics issued by the program.
</p></dd></dl>
<a class="index-entry-id" id="index-standard-error-stream"></a>

<p>On GNU systems, you can specify what files or processes correspond to
these streams using the pipe and redirection facilities provided by the
shell.  (The primitives shells use to implement these facilities are
described in <a class="ref" href="File-System-Interface.html">File System Interface</a>.)  Most other operating systems
provide similar mechanisms, but the details of how to use them can vary.
</p>
<p>In the GNU C Library, <code class="code">stdin</code>, <code class="code">stdout</code>, and <code class="code">stderr</code> are
normal variables which you can set just like any others.  For example,
to redirect the standard output to a file, you could do:
</p>
<div class="example smallexample">
<pre class="example-preformatted">fclose (stdout);
stdout = fopen (&quot;standard-output-file&quot;, &quot;w&quot;);
</pre></div>

<p>Note however, that in other systems <code class="code">stdin</code>, <code class="code">stdout</code>, and
<code class="code">stderr</code> are macros that you cannot assign to in the normal way.
But you can use <code class="code">freopen</code> to get the effect of closing one and
reopening it.  See <a class="xref" href="Opening-Streams.html">Opening Streams</a>.
</p>
<p>The three streams <code class="code">stdin</code>, <code class="code">stdout</code>, and <code class="code">stderr</code> are not
unoriented at program start (see <a class="pxref" href="Streams-and-I18N.html">Streams in Internationalized Applications</a>).
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Opening-Streams.html">Opening Streams</a>, Previous: <a href="Streams.html">Streams</a>, Up: <a href="I_002fO-on-Streams.html">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
