<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Feature Test Macros (The GNU C Library)</title>

<meta name="description" content="Feature Test Macros (The GNU C Library)">
<meta name="keywords" content="Feature Test Macros (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Using-the-Library.html" rel="up" title="Using the Library">
<link href="Reserved-Names.html" rel="prev" title="Reserved Names">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Feature-Test-Macros">
<div class="nav-panel">
<p>
Previous: <a href="Reserved-Names.html" accesskey="p" rel="prev">Reserved Names</a>, Up: <a href="Using-the-Library.html" accesskey="u" rel="up">Using the Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Feature-Test-Macros-1"><span>1.3.4 Feature Test Macros<a class="copiable-link" href="#Feature-Test-Macros-1"> &para;</a></span></h4>

<a class="index-entry-id" id="index-feature-test-macros"></a>
<p>The exact set of features available when you compile a source file
is controlled by which <em class="dfn">feature test macros</em> you define.
</p>
<p>If you compile your programs using &lsquo;<samp class="samp">gcc -ansi</samp>&rsquo;, you get only the
ISO&nbsp;C<!-- /@w --> library features, unless you explicitly request additional
features by defining one or more of the feature macros.
See <a data-manual="gcc" href="https://gcc.gnu.org/onlinedocs/gcc/Invoking-GCC.html#Invoking-GCC">GNU CC Command Options</a> in <cite class="cite">The GNU CC Manual</cite>,
for more information about GCC options.
</p>
<p>You should define these macros by using &lsquo;<samp class="samp">#define</samp>&rsquo; preprocessor
directives at the top of your source code files.  These directives
<em class="emph">must</em> come before any <code class="code">#include</code> of a system header file.  It
is best to make them the very first thing in the file, preceded only by
comments.  You could also use the &lsquo;<samp class="samp">-D</samp>&rsquo; option to GCC, but it&rsquo;s
better if you make the source files indicate their own meaning in a
self-contained way.
</p>
<p>This system exists to allow the library to conform to multiple standards.
Although the different standards are often described as supersets of each
other, they are usually incompatible because larger standards require
functions with names that smaller ones reserve to the user program.  This
is not mere pedantry &mdash; it has been a problem in practice.  For instance,
some non-GNU programs define functions named <code class="code">getline</code> that have
nothing to do with this library&rsquo;s <code class="code">getline</code>.  They would not be
compilable if all features were enabled indiscriminately.
</p>
<p>This should not be used to verify that a program conforms to a limited
standard.  It is insufficient for this purpose, as it will not protect you
from including header files outside the standard, or relying on semantics
undefined within the standard.
</p>
<dl class="first-defvr">
<dt class="defvr" id="index-_005fPOSIX_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_POSIX_SOURCE</strong><a class="copiable-link" href="#index-_005fPOSIX_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If you define this macro, then the functionality from the POSIX.1
standard (IEEE Standard 1003.1) is available, as well as all of the
ISO&nbsp;C<!-- /@w --> facilities.
</p>
<p>The state of <code class="code">_POSIX_SOURCE</code> is irrelevant if you define the
macro <code class="code">_POSIX_C_SOURCE</code> to a positive integer.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fPOSIX_005fC_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_POSIX_C_SOURCE</strong><a class="copiable-link" href="#index-_005fPOSIX_005fC_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>Define this macro to a positive integer to control which POSIX
functionality is made available.  The greater the value of this macro,
the more functionality is made available.
</p>
<p>If you define this macro to a value greater than or equal to <code class="code">1</code>,
then the functionality from the 1990 edition of the POSIX.1 standard
(IEEE Standard 1003.1-1990) is made available.
</p>
<p>If you define this macro to a value greater than or equal to <code class="code">2</code>,
then the functionality from the 1992 edition of the POSIX.2 standard
(IEEE Standard 1003.2-1992) is made available.
</p>
<p>If you define this macro to a value greater than or equal to <code class="code">199309L</code>,
then the functionality from the 1993 edition of the POSIX.1b standard
(IEEE Standard 1003.1b-1993) is made available.
</p>
<p>If you define this macro to a value greater than or equal to
<code class="code">199506L</code>, then the functionality from the 1995 edition of the
POSIX.1c standard (IEEE Standard 1003.1c-1995) is made available.
</p>
<p>If you define this macro to a value greater than or equal to
<code class="code">200112L</code>, then the functionality from the 2001 edition of the
POSIX standard (IEEE Standard 1003.1-2001) is made available.
</p>
<p>If you define this macro to a value greater than or equal to
<code class="code">200809L</code>, then the functionality from the 2008 edition of the
POSIX standard (IEEE Standard 1003.1-2008) is made available.
</p>
<p>Greater values for <code class="code">_POSIX_C_SOURCE</code> will enable future extensions.
The POSIX standards process will define these values as necessary, and
the GNU C Library should support them some time after they become standardized.
The 1996 edition of POSIX.1 (ISO/IEC 9945-1: 1996) states that
if you define <code class="code">_POSIX_C_SOURCE</code> to a value greater than
or equal to <code class="code">199506L</code>, then the functionality from the 1996
edition is made available.  In general, in the GNU C Library, bugfixes to
the standards are included when specifying the base version; e.g.,
POSIX.1-2004 will always be included with a value of <code class="code">200112L</code>.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fXOPEN_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_XOPEN_SOURCE</strong><a class="copiable-link" href="#index-_005fXOPEN_005fSOURCE"> &para;</a></span></dt>
<dt class="defvrx def-cmd-defvr" id="index-_005fXOPEN_005fSOURCE_005fEXTENDED"><span class="category-def">Macro: </span><span><strong class="def-name">_XOPEN_SOURCE_EXTENDED</strong><a class="copiable-link" href="#index-_005fXOPEN_005fSOURCE_005fEXTENDED"> &para;</a></span></dt>
<dd>
<p>If you define this macro, functionality described in the X/Open
Portability Guide is included.  This is a superset of the POSIX.1 and
POSIX.2 functionality and in fact <code class="code">_POSIX_SOURCE</code> and
<code class="code">_POSIX_C_SOURCE</code> are automatically defined.
</p>
<p>As the unification of all Unices, functionality only available in
BSD and SVID is also included.
</p>
<p>If the macro <code class="code">_XOPEN_SOURCE_EXTENDED</code> is also defined, even more
functionality is available.  The extra functions will make all functions
available which are necessary for the X/Open Unix brand.
</p>
<p>If the macro <code class="code">_XOPEN_SOURCE</code> has the value <em class="math">500</em> this includes
all functionality described so far plus some new definitions from the
Single Unix Specification, version&nbsp;2<!-- /@w -->.  The value <em class="math">600</em>
(corresponding to the sixth revision) includes definitions from SUSv3,
and using <em class="math">700</em> (the seventh revision) includes definitions from
SUSv4.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fLARGEFILE_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_LARGEFILE_SOURCE</strong><a class="copiable-link" href="#index-_005fLARGEFILE_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If this macro is defined some extra functions are available which
rectify a few shortcomings in all previous standards.  Specifically,
the functions <code class="code">fseeko</code> and <code class="code">ftello</code> are available.  Without
these functions the difference between the ISO&nbsp;C<!-- /@w --> interface
(<code class="code">fseek</code>, <code class="code">ftell</code>) and the low-level POSIX interface
(<code class="code">lseek</code>) would lead to problems.
</p>
<p>This macro was introduced as part of the Large File Support extension (LFS).
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fLARGEFILE64_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_LARGEFILE64_SOURCE</strong><a class="copiable-link" href="#index-_005fLARGEFILE64_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If you define this macro an additional set of functions is made available
which enables 32&nbsp;bit<!-- /@w --> systems to use files of sizes beyond
the usual limit of 2GB.  This interface is not available if the system
does not support files that large.  On systems where the natural file
size limit is greater than 2GB (i.e., on 64&nbsp;bit<!-- /@w --> systems) the new
functions are identical to the replaced functions.
</p>
<p>The new functionality is made available by a new set of types and
functions which replace the existing ones.  The names of these new objects
contain <code class="code">64</code> to indicate the intention, e.g., <code class="code">off_t</code>
vs. <code class="code">off64_t</code> and <code class="code">fseeko</code> vs. <code class="code">fseeko64</code>.
</p>
<p>This macro was introduced as part of the Large File Support extension
(LFS).  It is a transition interface for the period when 64&nbsp;bit<!-- /@w -->
offsets are not generally used (see <code class="code">_FILE_OFFSET_BITS</code>).
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fFILE_005fOFFSET_005fBITS"><span class="category-def">Macro: </span><span><strong class="def-name">_FILE_OFFSET_BITS</strong><a class="copiable-link" href="#index-_005fFILE_005fOFFSET_005fBITS"> &para;</a></span></dt>
<dd>
<p>This macro determines which file system interface shall be used, one
replacing the other.  Whereas <code class="code">_LARGEFILE64_SOURCE</code> makes the 64&nbsp;bit<!-- /@w --> interface available as an additional interface,
<code class="code">_FILE_OFFSET_BITS</code> allows the 64&nbsp;bit<!-- /@w --> interface to
replace the old interface.
</p>
<p>If <code class="code">_FILE_OFFSET_BITS</code> is defined to the
value <code class="code">32</code>, the 32&nbsp;bit<!-- /@w --> interface is used and
types like <code class="code">off_t</code> have a size of 32&nbsp;bits<!-- /@w --> on 32&nbsp;bit<!-- /@w -->
systems.
</p>
<p>If the macro is defined to the value <code class="code">64</code>, the large file interface
replaces the old interface.  I.e., the functions are not made available
under different names (as they are with <code class="code">_LARGEFILE64_SOURCE</code>).
Instead the old function names now reference the new functions, e.g., a
call to <code class="code">fseeko</code> now indeed calls <code class="code">fseeko64</code>.
</p>
<p>If the macro is not defined it currently defaults to <code class="code">32</code>, but
this default is planned to change due to a need to update
<code class="code">time_t</code> for Y2038 safety, and applications should not rely on
the default.
</p>
<p>This macro should only be selected if the system provides mechanisms for
handling large files.  On 64&nbsp;bit<!-- /@w --> systems this macro has no effect
since the <code class="code">*64</code> functions are identical to the normal functions.
</p>
<p>This macro was introduced as part of the Large File Support extension
(LFS).
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fTIME_005fBITS"><span class="category-def">Macro: </span><span><strong class="def-name">_TIME_BITS</strong><a class="copiable-link" href="#index-_005fTIME_005fBITS"> &para;</a></span></dt>
<dd><p>Define this macro to control the bit size of <code class="code">time_t</code>, and therefore
the bit size of all <code class="code">time_t</code>-derived types and the prototypes of all
related functions.
</p>
<ol class="enumerate">
<li> If <code class="code">_TIME_BITS</code> is undefined, the bit size of <code class="code">time_t</code> is
architecture dependent.  Currently it defaults to 64 bits on most
architectures.  Although it defaults to 32 bits on some traditional
architectures (i686, ARM), this is planned to change and applications
should not rely on this.

</li><li> If <code class="code">_TIME_BITS</code> is defined to be 64, <code class="code">time_t</code> is defined
to be a 64-bit integer.  On platforms where <code class="code">time_t</code> was
traditionally 32 bits, calls to proper syscalls depend on the
Linux kernel version on which the system is running. For Linux kernel
version above <b class="b">5.1</b> syscalls supporting 64-bit time are used. Otherwise,
a fallback code is used with legacy (i.e. 32-bit) syscalls.

<p>On such platforms, the GNU C Library will also define <code class="code">__USE_TIME64_REDIRECTS</code>
to indicate whether the declarations are expanded to different ones
(either by redefiniding the symbol name or using symbol aliais).
For instance, if the symbol <code class="code">clock_gettime</code> expands to
<code class="code">__glock_gettime64</code>.
</p>
</li><li> If <code class="code">_TIME_BITS</code> is defined to be 32, <code class="code">time_t</code> is defined to
be a 32-bit integer where that is supported.  This is not recommended,
as 32-bit <code class="code">time_t</code> stops working in the year 2038.

</li><li> For any other use case a compile-time error is emitted.
</li></ol>

<p><code class="code">_TIME_BITS=64</code> can be defined only when
<code class="code">_FILE_OFFSET_BITS=64</code> is also defined.
</p>
<p>By using this macro certain ports gain support for 64-bit time and as
a result become immune to the Y2038 problem.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fISOC99_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_ISOC99_SOURCE</strong><a class="copiable-link" href="#index-_005fISOC99_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If this macro is defined, features from ISO C99 are included.  Since
these features are included by default, this macro is mostly relevant
when the compiler uses an earlier language version.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fISOC11_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_ISOC11_SOURCE</strong><a class="copiable-link" href="#index-_005fISOC11_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If this macro is defined, ISO C11 extensions to ISO C99 are included.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fISOC23_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_ISOC23_SOURCE</strong><a class="copiable-link" href="#index-_005fISOC23_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If this macro is defined, ISO C23 extensions to ISO C11 are included.
Only some features from this draft standard are supported by
the GNU C Library.  The older name <code class="code">_ISOC2X_SOURCE</code> is also supported.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fISOC2Y_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_ISOC2Y_SOURCE</strong><a class="copiable-link" href="#index-_005fISOC2Y_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If this macro is defined, ISO C2Y extensions to ISO C23 are included.
Only some features from this draft standard are supported by
the GNU C Library.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005f_005fSTDC_005fWANT_005fLIB_005fEXT2_005f_005f"><span class="category-def">Macro: </span><span><strong class="def-name">__STDC_WANT_LIB_EXT2__</strong><a class="copiable-link" href="#index-_005f_005fSTDC_005fWANT_005fLIB_005fEXT2_005f_005f"> &para;</a></span></dt>
<dd>
<p>If you define this macro to the value <code class="code">1</code>, features from ISO/IEC
TR 24731-2:2010 (Dynamic Allocation Functions) are enabled.  Only some
of the features from this TR are supported by the GNU C Library.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005f_005fSTDC_005fWANT_005fIEC_005f60559_005fBFP_005fEXT_005f_005f"><span class="category-def">Macro: </span><span><strong class="def-name">__STDC_WANT_IEC_60559_BFP_EXT__</strong><a class="copiable-link" href="#index-_005f_005fSTDC_005fWANT_005fIEC_005f60559_005fBFP_005fEXT_005f_005f"> &para;</a></span></dt>
<dd>
<p>If you define this macro, features from ISO/IEC TS 18661-1:2014
(Floating-point extensions for C: Binary floating-point arithmetic)
are enabled.  Only some of the features from this TS are supported by
the GNU C Library.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005f_005fSTDC_005fWANT_005fIEC_005f60559_005fFUNCS_005fEXT_005f_005f"><span class="category-def">Macro: </span><span><strong class="def-name">__STDC_WANT_IEC_60559_FUNCS_EXT__</strong><a class="copiable-link" href="#index-_005f_005fSTDC_005fWANT_005fIEC_005f60559_005fFUNCS_005fEXT_005f_005f"> &para;</a></span></dt>
<dd>
<p>If you define this macro, features from ISO/IEC TS 18661-4:2015
(Floating-point extensions for C: Supplementary functions) are
enabled.  Only some of the features from this TS are supported by
the GNU C Library.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005f_005fSTDC_005fWANT_005fIEC_005f60559_005fTYPES_005fEXT_005f_005f"><span class="category-def">Macro: </span><span><strong class="def-name">__STDC_WANT_IEC_60559_TYPES_EXT__</strong><a class="copiable-link" href="#index-_005f_005fSTDC_005fWANT_005fIEC_005f60559_005fTYPES_005fEXT_005f_005f"> &para;</a></span></dt>
<dd>
<p>If you define this macro, features from ISO/IEC TS 18661-3:2015
(Floating-point extensions for C: Interchange and extended types) are
enabled.  Only some of the features from this TS are supported by
the GNU C Library.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005f_005fSTDC_005fWANT_005fIEC_005f60559_005fEXT_005f_005f"><span class="category-def">Macro: </span><span><strong class="def-name">__STDC_WANT_IEC_60559_EXT__</strong><a class="copiable-link" href="#index-_005f_005fSTDC_005fWANT_005fIEC_005f60559_005fEXT_005f_005f"> &para;</a></span></dt>
<dd>
<p>If you define this macro, ISO C23 features defined in Annex F of that
standard are enabled.  This affects declarations of the
<code class="code">totalorder</code> functions and functions related to NaN payloads.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fGNU_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_GNU_SOURCE</strong><a class="copiable-link" href="#index-_005fGNU_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If you define this macro, everything is included: ISO&nbsp;C89<!-- /@w -->, ISO&nbsp;C99<!-- /@w -->, POSIX.1, POSIX.2, BSD, SVID, X/Open, LFS, and GNU extensions.  In
the cases where POSIX.1 conflicts with BSD, the POSIX definitions take
precedence.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fDEFAULT_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_DEFAULT_SOURCE</strong><a class="copiable-link" href="#index-_005fDEFAULT_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If you define this macro, most features are included apart from
X/Open, LFS and GNU extensions: the effect is to enable features from
the 2008 edition of POSIX, as well as certain BSD and SVID features
without a separate feature test macro to control them.
</p>
<p>Be aware that compiler options also affect included features:
</p>
<ul class="itemize mark-bullet">
<li>If you use a strict conformance option, features beyond those from the
compiler&rsquo;s language version will be disabled, though feature test
macros may be used to enable them.

</li><li>Features enabled by compiler options are not overridden by feature
test macros.
</li></ul>
</dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fATFILE_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_ATFILE_SOURCE</strong><a class="copiable-link" href="#index-_005fATFILE_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If this macro is defined, additional <code class="code">*at</code> interfaces are
included.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fFORTIFY_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_FORTIFY_SOURCE</strong><a class="copiable-link" href="#index-_005fFORTIFY_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If this macro is defined to <em class="math">1</em>, security hardening is added to
various library functions.  If defined to <em class="math">2</em>, even stricter
checks are applied. If defined to <em class="math">3</em>, the GNU C Library may also use
checks that may have an additional performance overhead.
See <a class="xref" href="Source-Fortification.html">Fortification of function calls</a>.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fDYNAMIC_005fSTACK_005fSIZE_005fSOURCE"><span class="category-def">Macro: </span><span><strong class="def-name">_DYNAMIC_STACK_SIZE_SOURCE</strong><a class="copiable-link" href="#index-_005fDYNAMIC_005fSTACK_005fSIZE_005fSOURCE"> &para;</a></span></dt>
<dd>
<p>If this macro is defined, correct (but non compile-time constant)
MINSIGSTKSZ, SIGSTKSZ and PTHREAD_STACK_MIN are defined.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-_005fREENTRANT"><span class="category-def">Macro: </span><span><strong class="def-name">_REENTRANT</strong><a class="copiable-link" href="#index-_005fREENTRANT"> &para;</a></span></dt>
<dt class="defvrx def-cmd-defvr" id="index-_005fTHREAD_005fSAFE"><span class="category-def">Macro: </span><span><strong class="def-name">_THREAD_SAFE</strong><a class="copiable-link" href="#index-_005fTHREAD_005fSAFE"> &para;</a></span></dt>
<dd>
<p>These macros are obsolete.  They have the same effect as defining
<code class="code">_POSIX_C_SOURCE</code> with the value <code class="code">199506L</code>.
</p>
<p>Some very old C libraries required one of these macros to be defined
for basic functionality (e.g. <code class="code">getchar</code>) to be thread-safe.
</p></dd></dl>

<p>We recommend you use <code class="code">_GNU_SOURCE</code> in new programs.  If you don&rsquo;t
specify the &lsquo;<samp class="samp">-ansi</samp>&rsquo; option to GCC, or other conformance options
such as <samp class="option">-std=c99</samp>, and don&rsquo;t define any of these macros
explicitly, the effect is the same as defining <code class="code">_DEFAULT_SOURCE</code>
to 1.
</p>
<p>When you define a feature test macro to request a larger class of features,
it is harmless to define in addition a feature test macro for a subset of
those features.  For example, if you define <code class="code">_POSIX_C_SOURCE</code>, then
defining <code class="code">_POSIX_SOURCE</code> as well has no effect.  Likewise, if you
define <code class="code">_GNU_SOURCE</code>, then defining either <code class="code">_POSIX_SOURCE</code> or
<code class="code">_POSIX_C_SOURCE</code> as well has no effect.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Reserved-Names.html">Reserved Names</a>, Up: <a href="Using-the-Library.html">Using the Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
