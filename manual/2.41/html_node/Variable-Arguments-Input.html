<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Variable Arguments Input (The GNU C Library)</title>

<meta name="description" content="Variable Arguments Input (The GNU C Library)">
<meta name="keywords" content="Variable Arguments Input (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Input.html" rel="up" title="Formatted Input">
<link href="Formatted-Input-Functions.html" rel="prev" title="Formatted Input Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Variable-Arguments-Input">
<div class="nav-panel">
<p>
Previous: <a href="Formatted-Input-Functions.html" accesskey="p" rel="prev">Formatted Input Functions</a>, Up: <a href="Formatted-Input.html" accesskey="u" rel="up">Formatted Input</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Variable-Arguments-Input-Functions"><span>12.14.9 Variable Arguments Input Functions<a class="copiable-link" href="#Variable-Arguments-Input-Functions"> &para;</a></span></h4>

<p>The functions <code class="code">vscanf</code> and friends are provided so that you can
define your own variadic <code class="code">scanf</code>-like functions that make use of
the same internals as the built-in formatted output functions.
These functions are analogous to the <code class="code">vprintf</code> series of output
functions.  See <a class="xref" href="Variable-Arguments-Output.html">Variable Arguments Output Functions</a>, for important
information on how to use them.
</p>
<p><strong class="strong">Portability Note:</strong> The functions listed in this section were
introduced in ISO&nbsp;C99<!-- /@w --> and were before available as GNU extensions.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vscanf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vscanf</strong> <code class="def-code-arguments">(const char *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vscanf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">scanf</code>, but instead of taking
a variable number of arguments directly, it takes an argument list
pointer <var class="var">ap</var> of type <code class="code">va_list</code> (see <a class="pxref" href="Variadic-Functions.html">Variadic Functions</a>).
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vwscanf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vwscanf</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vwscanf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">wscanf</code>, but instead of taking
a variable number of arguments directly, it takes an argument list
pointer <var class="var">ap</var> of type <code class="code">va_list</code> (see <a class="pxref" href="Variadic-Functions.html">Variadic Functions</a>).
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vfscanf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vfscanf</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, const char *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vfscanf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the equivalent of <code class="code">fscanf</code> with the variable argument list
specified directly as for <code class="code">vscanf</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vfwscanf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vfwscanf</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, const wchar_t *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vfwscanf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the equivalent of <code class="code">fwscanf</code> with the variable argument list
specified directly as for <code class="code">vwscanf</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vsscanf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vsscanf</strong> <code class="def-code-arguments">(const char *<var class="var">s</var>, const char *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vsscanf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the equivalent of <code class="code">sscanf</code> with the variable argument list
specified directly as for <code class="code">vscanf</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vswscanf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vswscanf</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">s</var>, const wchar_t *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vswscanf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the equivalent of <code class="code">swscanf</code> with the variable argument list
specified directly as for <code class="code">vwscanf</code>.
</p></dd></dl>

<p>In GNU C, there is a special construct you can use to let the compiler
know that a function uses a <code class="code">scanf</code>-style format string.  Then it
can check the number and types of arguments in each call to the
function, and warn you when they do not match the format string.
For details, see <a data-manual="gcc" href="https://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html#Function-Attributes">Declaring Attributes of Functions</a> in <cite class="cite">Using GNU CC</cite>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Formatted-Input-Functions.html">Formatted Input Functions</a>, Up: <a href="Formatted-Input.html">Formatted Input</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
