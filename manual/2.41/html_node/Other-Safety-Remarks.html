<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Other Safety Remarks (The GNU C Library)</title>

<meta name="description" content="Other Safety Remarks (The GNU C Library)">
<meta name="keywords" content="Other Safety Remarks (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="POSIX.html" rel="up" title="POSIX">
<link href="Conditionally-Safe-Features.html" rel="prev" title="Conditionally Safe Features">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Other-Safety-Remarks">
<div class="nav-panel">
<p>
Previous: <a href="Conditionally-Safe-Features.html" accesskey="p" rel="prev">Conditionally Safe Features</a>, Up: <a href="POSIX.html" accesskey="u" rel="up">POSIX (The Portable Operating System Interface)</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Other-Safety-Remarks-1"><span>1.2.2.4 Other Safety Remarks<a class="copiable-link" href="#Other-Safety-Remarks-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-Other-Safety-Remarks"></a>

<p>Additional keywords may be attached to functions, indicating features
that do not make a function unsafe to call, but that may need to be
taken into account in certain classes of programs:
</p>
<ul class="itemize mark-bullet">
<li><code class="code">locale</code>
<a class="index-entry-id" id="index-locale"></a>

<p>Functions annotated with <code class="code">locale</code> as an MT-Safety issue read from
the locale object without any form of synchronization.  Functions
annotated with <code class="code">locale</code> called concurrently with locale changes may
behave in ways that do not correspond to any of the locales active
during their execution, but an unpredictable mix thereof.
</p>
<p>We do not mark these functions as MT- or AS-Unsafe, however, because
functions that modify the locale object are marked with
<code class="code">const:locale</code> and regarded as unsafe.  Being unsafe, the latter
are not to be called when multiple threads are running or asynchronous
signals are enabled, and so the locale can be considered effectively
constant in these contexts, which makes the former safe.
</p>


</li><li><code class="code">env</code>
<a class="index-entry-id" id="index-env"></a>

<p>Functions marked with <code class="code">env</code> as an MT-Safety issue access the
environment with <code class="code">getenv</code> or similar, without any guards to ensure
safety in the presence of concurrent modifications.
</p>
<p>We do not mark these functions as MT- or AS-Unsafe, however, because
functions that modify the environment are all marked with
<code class="code">const:env</code> and regarded as unsafe.  Being unsafe, the latter are
not to be called when multiple threads are running or asynchronous
signals are enabled, and so the environment can be considered
effectively constant in these contexts, which makes the former safe.
</p>

</li><li><code class="code">hostid</code>
<a class="index-entry-id" id="index-hostid"></a>

<p>The function marked with <code class="code">hostid</code> as an MT-Safety issue reads from
the system-wide data structures that hold the &ldquo;host ID&rdquo; of the
machine.  These data structures cannot generally be modified atomically.
Since it is expected that the &ldquo;host ID&rdquo; will not normally change, the
function that reads from it (<code class="code">gethostid</code>) is regarded as safe,
whereas the function that modifies it (<code class="code">sethostid</code>) is marked with
<code class="code">const:hostid</code>, indicating it may require special
care if it is to be called.  In this specific case, the special care
amounts to system-wide (not merely intra-process) coordination.
</p>

</li><li><code class="code">sigintr</code>
<a class="index-entry-id" id="index-sigintr"></a>

<p>Functions marked with <code class="code">sigintr</code> as an MT-Safety issue access the
<code class="code">_sigintr</code> internal data structure without any guards to ensure
safety in the presence of concurrent modifications.
</p>
<p>We do not mark these functions as MT- or AS-Unsafe, however, because
functions that modify the this data structure are all marked with
<code class="code">const:sigintr</code> and regarded as unsafe.  Being unsafe, the latter
are not to be called when multiple threads are running or asynchronous
signals are enabled, and so the data structure can be considered
effectively constant in these contexts, which makes the former safe.
</p>

</li><li><code class="code">fd</code>
<a class="index-entry-id" id="index-fd"></a>

<p>Functions annotated with <code class="code">fd</code> as an AC-Safety issue may leak file
descriptors if asynchronous thread cancellation interrupts their
execution.
</p>
<p>Functions that allocate or deallocate file descriptors will generally be
marked as such.  Even if they attempted to protect the file descriptor
allocation and deallocation with cleanup regions, allocating a new
descriptor and storing its number where the cleanup region could release
it cannot be performed as a single atomic operation.  Similarly,
releasing the descriptor and taking it out of the data structure
normally responsible for releasing it cannot be performed atomically.
There will always be a window in which the descriptor cannot be released
because it was not stored in the cleanup handler argument yet, or it was
already taken out before releasing it.  It cannot be taken out after
release: an open descriptor could mean either that the descriptor still
has to be closed, or that it already did so but the descriptor was
reallocated by another thread or signal handler.
</p>
<p>Such leaks could be internally avoided, with some performance penalty,
by temporarily disabling asynchronous thread cancellation.  However,
since callers of allocation or deallocation functions would have to do
this themselves, to avoid the same sort of leak in their own layer, it
makes more sense for the library to assume they are taking care of it
than to impose a performance penalty that is redundant when the problem
is solved in upper layers, and insufficient when it is not.
</p>
<p>This remark by itself does not cause a function to be regarded as
AC-Unsafe.  However, cumulative effects of such leaks may pose a
problem for some programs.  If this is the case, suspending asynchronous
cancellation for the duration of calls to such functions is recommended.
</p>

</li><li><code class="code">mem</code>
<a class="index-entry-id" id="index-mem"></a>

<p>Functions annotated with <code class="code">mem</code> as an AC-Safety issue may leak
memory if asynchronous thread cancellation interrupts their execution.
</p>
<p>The problem is similar to that of file descriptors: there is no atomic
interface to allocate memory and store its address in the argument to a
cleanup handler, or to release it and remove its address from that
argument, without at least temporarily disabling asynchronous
cancellation, which these functions do not do.
</p>
<p>This remark does not by itself cause a function to be regarded as
generally AC-Unsafe.  However, cumulative effects of such leaks may be
severe enough for some programs that disabling asynchronous cancellation
for the duration of calls to such functions may be required.
</p>

</li><li><code class="code">cwd</code>
<a class="index-entry-id" id="index-cwd"></a>

<p>Functions marked with <code class="code">cwd</code> as an MT-Safety issue may temporarily
change the current working directory during their execution, which may
cause relative pathnames to be resolved in unexpected ways in other
threads or within asynchronous signal or cancellation handlers.
</p>
<p>This is not enough of a reason to mark so-marked functions as MT- or
AS-Unsafe, but when this behavior is optional (e.g., <code class="code">nftw</code> with
<code class="code">FTW_CHDIR</code>), avoiding the option may be a good alternative to
using full pathnames or file descriptor-relative (e.g. <code class="code">openat</code>)
system calls.
</p>

</li><li><code class="code">!posix</code>
<a class="index-entry-id" id="index-_0021posix"></a>

<p>This remark, as an MT-, AS- or AC-Safety note to a function, indicates
the safety status of the function is known to differ from the specified
status in the POSIX standard.  For example, POSIX does not require a
function to be Safe, but our implementation is, or vice-versa.
</p>
<p>For the time being, the absence of this remark does not imply the safety
properties we documented are identical to those mandated by POSIX for
the corresponding functions.
</p>

</li><li><code class="code">:identifier</code>
<a class="index-entry-id" id="index-_003aidentifier"></a>

<p>Annotations may sometimes be followed by identifiers, intended to group
several functions that e.g. access the data structures in an unsafe way,
as in <code class="code">race</code> and <code class="code">const</code>, or to provide more specific
information, such as naming a signal in a function marked with
<code class="code">sig</code>.  It is envisioned that it may be applied to <code class="code">lock</code> and
<code class="code">corrupt</code> as well in the future.
</p>
<p>In most cases, the identifier will name a set of functions, but it may
name global objects or function arguments, or identifiable properties or
logical components associated with them, with a notation such as
e.g. <code class="code">:buf(arg)</code> to denote a buffer associated with the argument
<var class="var">arg</var>, or <code class="code">:tcattr(fd)</code> to denote the terminal attributes of a
file descriptor <var class="var">fd</var>.
</p>
<p>The most common use for identifiers is to provide logical groups of
functions and arguments that need to be protected by the same
synchronization primitive in order to ensure safe operation in a given
context.
</p>

</li><li><code class="code">/condition</code>
<a class="index-entry-id" id="index-_002fcondition"></a>

<p>Some safety annotations may be conditional, in that they only apply if a
boolean expression involving arguments, global variables or even the
underlying kernel evaluates to true.  Such conditions as
<code class="code">/hurd</code> or <code class="code">/!linux!bsd</code> indicate the preceding marker only
applies when the underlying kernel is the HURD, or when it is neither
Linux nor a BSD kernel, respectively.  <code class="code">/!ps</code> and
<code class="code">/one_per_line</code> indicate the preceding marker only applies when
argument <var class="var">ps</var> is NULL, or global variable <var class="var">one_per_line</var> is
nonzero.
</p>
<p>When all marks that render a function unsafe are adorned with such
conditions, and none of the named conditions hold, then the function can
be regarded as safe.
</p>

</li></ul>


</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Conditionally-Safe-Features.html">Conditionally Safe Features</a>, Up: <a href="POSIX.html">POSIX (The Portable Operating System Interface)</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
