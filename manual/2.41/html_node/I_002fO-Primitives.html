<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>I/O Primitives (The GNU C Library)</title>

<meta name="description" content="I/O Primitives (The GNU C Library)">
<meta name="keywords" content="I/O Primitives (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Low_002dLevel-I_002fO.html" rel="up" title="Low-Level I/O">
<link href="File-Position-Primitive.html" rel="next" title="File Position Primitive">
<link href="Opening-and-Closing-Files.html" rel="prev" title="Opening and Closing Files">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="I_002fO-Primitives">
<div class="nav-panel">
<p>
Next: <a href="File-Position-Primitive.html" accesskey="n" rel="next">Setting the File Position of a Descriptor</a>, Previous: <a href="Opening-and-Closing-Files.html" accesskey="p" rel="prev">Opening and Closing Files</a>, Up: <a href="Low_002dLevel-I_002fO.html" accesskey="u" rel="up">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Input-and-Output-Primitives"><span>13.2 Input and Output Primitives<a class="copiable-link" href="#Input-and-Output-Primitives"> &para;</a></span></h3>

<p>This section describes the functions for performing primitive input and
output operations on file descriptors: <code class="code">read</code>, <code class="code">write</code>, and
<code class="code">lseek</code>.  These functions are declared in the header file
<samp class="file">unistd.h</samp>.
<a class="index-entry-id" id="index-unistd_002eh-1"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-ssize_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">ssize_t</strong><a class="copiable-link" href="#index-ssize_005ft"> &para;</a></span></dt>
<dd>
<p>This data type is used to represent the sizes of blocks that can be
read or written in a single operation.  It is similar to <code class="code">size_t</code>,
but must be a signed type.
</p></dd></dl>

<a class="index-entry-id" id="index-reading-from-a-file-descriptor"></a>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-read"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">read</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, void *<var class="var">buffer</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-read"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">read</code> function reads up to <var class="var">size</var> bytes from the file
with descriptor <var class="var">filedes</var>, storing the results in the <var class="var">buffer</var>.
(This is not necessarily a character string, and no terminating null
character is added.)
</p>
<a class="index-entry-id" id="index-end_002dof_002dfile_002c-on-a-file-descriptor"></a>
<p>The return value is the number of bytes actually read.  This might be
less than <var class="var">size</var>; for example, if there aren&rsquo;t that many bytes left
in the file or if there aren&rsquo;t that many bytes immediately available.
The exact behavior depends on what kind of file it is.  Note that
reading less than <var class="var">size</var> bytes is not an error.
</p>
<p>A value of zero indicates end-of-file (except if the value of the
<var class="var">size</var> argument is also zero).  This is not considered an error.
If you keep calling <code class="code">read</code> while at end-of-file, it will keep
returning zero and doing nothing else.
</p>
<p>If <code class="code">read</code> returns at least one character, there is no way you can
tell whether end-of-file was reached.  But if you did reach the end, the
next read will return zero.
</p>
<p>In case of an error, <code class="code">read</code> returns <em class="math">-1</em>.  The following
<code class="code">errno</code> error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EAGAIN</code></dt>
<dd><p>Normally, when no input is immediately available, <code class="code">read</code> waits for
some input.  But if the <code class="code">O_NONBLOCK</code> flag is set for the file
(see <a class="pxref" href="File-Status-Flags.html">File Status Flags</a>), <code class="code">read</code> returns immediately without
reading any data, and reports this error.
</p>
<p><strong class="strong">Compatibility Note:</strong> Most versions of BSD Unix use a different
error code for this: <code class="code">EWOULDBLOCK</code>.  In the GNU C Library,
<code class="code">EWOULDBLOCK</code> is an alias for <code class="code">EAGAIN</code>, so it doesn&rsquo;t matter
which name you use.
</p>
<p>On some systems, reading a large amount of data from a character special
file can also fail with <code class="code">EAGAIN</code> if the kernel cannot find enough
physical memory to lock down the user&rsquo;s pages.  This is limited to
devices that transfer with direct memory access into the user&rsquo;s memory,
which means it does not include terminals, since they always use
separate buffers inside the kernel.  This problem never happens on
GNU/Hurd systems.
</p>
<p>Any condition that could result in <code class="code">EAGAIN</code> can instead result in a
successful <code class="code">read</code> which returns fewer bytes than requested.
Calling <code class="code">read</code> again immediately would result in <code class="code">EAGAIN</code>.
</p>
</dd>
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is not a valid file descriptor,
or is not open for reading.
</p>
</dd>
<dt><code class="code">EINTR</code></dt>
<dd><p><code class="code">read</code> was interrupted by a signal while it was waiting for input.
See <a class="xref" href="Interrupted-Primitives.html">Primitives Interrupted by Signals</a>.  A signal will not necessarily cause
<code class="code">read</code> to return <code class="code">EINTR</code>; it may instead result in a
successful <code class="code">read</code> which returns fewer bytes than requested.
</p>
</dd>
<dt><code class="code">EIO</code></dt>
<dd><p>For many devices, and for disk files, this error code indicates
a hardware error.
</p>
<p><code class="code">EIO</code> also occurs when a background process tries to read from the
controlling terminal, and the normal action of stopping the process by
sending it a <code class="code">SIGTTIN</code> signal isn&rsquo;t working.  This might happen if
the signal is being blocked or ignored, or because the process group is
orphaned.  See <a class="xref" href="Job-Control.html">Job Control</a>, for more information about job control,
and <a class="ref" href="Signal-Handling.html">Signal Handling</a>, for information about signals.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>In some systems, when reading from a character or block device, position
and size offsets must be aligned to a particular block size.  This error
indicates that the offsets were not properly aligned.
</p></dd>
</dl>

<p>Please note that there is no function named <code class="code">read64</code>.  This is not
necessary since this function does not directly modify or handle the
possibly wide file offset.  Since the kernel handles this state
internally, the <code class="code">read</code> function can be used for all cases.
</p>
<p>This function is a cancellation point in multi-threaded programs.  This
is a problem if the thread allocates some resources (like memory, file
descriptors, semaphores or whatever) at the time <code class="code">read</code> is
called.  If the thread gets canceled these resources stay allocated
until the program ends.  To avoid this, calls to <code class="code">read</code> should be
protected using cancellation handlers.
</p>
<p>The <code class="code">read</code> function is the underlying primitive for all of the
functions that read from streams, such as <code class="code">fgetc</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pread"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">pread</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, void *<var class="var">buffer</var>, size_t <var class="var">size</var>, off_t <var class="var">offset</var>)</code><a class="copiable-link" href="#index-pread"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">pread</code> function is similar to the <code class="code">read</code> function.  The
first three arguments are identical, and the return values and error
codes also correspond.
</p>
<p>The difference is the fourth argument and its handling.  The data block
is not read from the current position of the file descriptor
<code class="code">filedes</code>.  Instead the data is read from the file starting at
position <var class="var">offset</var>.  The position of the file descriptor itself is
not affected by the operation.  The value is the same as before the call.
</p>
<p>When the source file is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> the
<code class="code">pread</code> function is in fact <code class="code">pread64</code> and the type
<code class="code">off_t</code> has 64 bits, which makes it possible to handle files up to
2^63 bytes in length.
</p>
<p>The return value of <code class="code">pread</code> describes the number of bytes read.
In the error case it returns <em class="math">-1</em> like <code class="code">read</code> does and the
error codes are also the same, with these additions:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The value given for <var class="var">offset</var> is negative and therefore illegal.
</p>
</dd>
<dt><code class="code">ESPIPE</code></dt>
<dd><p>The file descriptor <var class="var">filedes</var> is associated with a pipe or a FIFO and
this device does not allow positioning of the file pointer.
</p></dd>
</dl>

<p>The function is an extension defined in the Unix Single Specification
version 2.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pread64"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">pread64</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, void *<var class="var">buffer</var>, size_t <var class="var">size</var>, off64_t <var class="var">offset</var>)</code><a class="copiable-link" href="#index-pread64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to the <code class="code">pread</code> function.  The difference
is that the <var class="var">offset</var> parameter is of type <code class="code">off64_t</code> instead of
<code class="code">off_t</code> which makes it possible on 32 bit machines to address
files larger than 2^31 bytes and up to 2^63 bytes.  The
file descriptor <code class="code">filedes</code> must be opened using <code class="code">open64</code> since
otherwise the large offsets possible with <code class="code">off64_t</code> will lead to
errors with a descriptor in small file mode.
</p>
<p>When the source file is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit machine this function is actually available under the name
<code class="code">pread</code> and so transparently replaces the 32 bit interface.
</p></dd></dl>

<a class="index-entry-id" id="index-writing-to-a-file-descriptor"></a>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-write"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">write</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, const void *<var class="var">buffer</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-write"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">write</code> function writes up to <var class="var">size</var> bytes from
<var class="var">buffer</var> to the file with descriptor <var class="var">filedes</var>.  The data in
<var class="var">buffer</var> is not necessarily a character string and a null character is
output like any other character.
</p>
<p>The return value is the number of bytes actually written.  This may be
<var class="var">size</var>, but can always be smaller.  Your program should always call
<code class="code">write</code> in a loop, iterating until all the data is written.
</p>
<p>Once <code class="code">write</code> returns, the data is enqueued to be written and can be
read back right away, but it is not necessarily written out to permanent
storage immediately.  You can use <code class="code">fsync</code> when you need to be sure
your data has been permanently stored before continuing.  (It is more
efficient for the system to batch up consecutive writes and do them all
at once when convenient.  Normally they will always be written to disk
within a minute or less.)  Modern systems provide another function
<code class="code">fdatasync</code> which guarantees integrity only for the file data and
is therefore faster.
You can use the <code class="code">O_FSYNC</code> open mode to make <code class="code">write</code> always
store the data to disk before returning; see <a class="pxref" href="Operating-Modes.html">I/O Operating Modes</a>.
</p>
<p>In the case of an error, <code class="code">write</code> returns <em class="math">-1</em>.  The following
<code class="code">errno</code> error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EAGAIN</code></dt>
<dd><p>Normally, <code class="code">write</code> blocks until the write operation is complete.
But if the <code class="code">O_NONBLOCK</code> flag is set for the file (see <a class="pxref" href="Control-Operations.html">Control Operations on Files</a>), it returns immediately without writing any data and
reports this error.  An example of a situation that might cause the
process to block on output is writing to a terminal device that supports
flow control, where output has been suspended by receipt of a STOP
character.
</p>
<p><strong class="strong">Compatibility Note:</strong> Most versions of BSD Unix use a different
error code for this: <code class="code">EWOULDBLOCK</code>.  In the GNU C Library,
<code class="code">EWOULDBLOCK</code> is an alias for <code class="code">EAGAIN</code>, so it doesn&rsquo;t matter
which name you use.
</p>
<p>On some systems, writing a large amount of data from a character special
file can also fail with <code class="code">EAGAIN</code> if the kernel cannot find enough
physical memory to lock down the user&rsquo;s pages.  This is limited to
devices that transfer with direct memory access into the user&rsquo;s memory,
which means it does not include terminals, since they always use
separate buffers inside the kernel.  This problem does not arise on
GNU/Hurd systems.
</p>
</dd>
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is not a valid file descriptor,
or is not open for writing.
</p>
</dd>
<dt><code class="code">EFBIG</code></dt>
<dd><p>The size of the file would become larger than the implementation can support.
</p>
</dd>
<dt><code class="code">EINTR</code></dt>
<dd><p>The <code class="code">write</code> operation was interrupted by a signal while it was
blocked waiting for completion.  A signal will not necessarily cause
<code class="code">write</code> to return <code class="code">EINTR</code>; it may instead result in a
successful <code class="code">write</code> which writes fewer bytes than requested.
See <a class="xref" href="Interrupted-Primitives.html">Primitives Interrupted by Signals</a>.
</p>
</dd>
<dt><code class="code">EIO</code></dt>
<dd><p>For many devices, and for disk files, this error code indicates
a hardware error.
</p>
</dd>
<dt><code class="code">ENOSPC</code></dt>
<dd><p>The device containing the file is full.
</p>
</dd>
<dt><code class="code">EPIPE</code></dt>
<dd><p>This error is returned when you try to write to a pipe or FIFO that
isn&rsquo;t open for reading by any process.  When this happens, a <code class="code">SIGPIPE</code>
signal is also sent to the process; see <a class="ref" href="Signal-Handling.html">Signal Handling</a>.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>In some systems, when writing to a character or block device, position
and size offsets must be aligned to a particular block size.  This error
indicates that the offsets were not properly aligned.
</p></dd>
</dl>

<p>Unless you have arranged to prevent <code class="code">EINTR</code> failures, you should
check <code class="code">errno</code> after each failing call to <code class="code">write</code>, and if the
error was <code class="code">EINTR</code>, you should simply repeat the call.
See <a class="xref" href="Interrupted-Primitives.html">Primitives Interrupted by Signals</a>.  The easy way to do this is with the
macro <code class="code">TEMP_FAILURE_RETRY</code>, as follows:
</p>
<div class="example smallexample">
<pre class="example-preformatted">nbytes = TEMP_FAILURE_RETRY (write (desc, buffer, count));
</pre></div>

<p>Please note that there is no function named <code class="code">write64</code>.  This is not
necessary since this function does not directly modify or handle the
possibly wide file offset.  Since the kernel handles this state
internally the <code class="code">write</code> function can be used for all cases.
</p>
<p>This function is a cancellation point in multi-threaded programs.  This
is a problem if the thread allocates some resources (like memory, file
descriptors, semaphores or whatever) at the time <code class="code">write</code> is
called.  If the thread gets canceled these resources stay allocated
until the program ends.  To avoid this, calls to <code class="code">write</code> should be
protected using cancellation handlers.
</p>
<p>The <code class="code">write</code> function is the underlying primitive for all of the
functions that write to streams, such as <code class="code">fputc</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pwrite"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">pwrite</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, const void *<var class="var">buffer</var>, size_t <var class="var">size</var>, off_t <var class="var">offset</var>)</code><a class="copiable-link" href="#index-pwrite"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">pwrite</code> function is similar to the <code class="code">write</code> function.  The
first three arguments are identical, and the return values and error codes
also correspond.
</p>
<p>The difference is the fourth argument and its handling.  The data block
is not written to the current position of the file descriptor
<code class="code">filedes</code>.  Instead the data is written to the file starting at
position <var class="var">offset</var>.  The position of the file descriptor itself is
not affected by the operation.  The value is the same as before the call.
</p>
<p>However, on Linux, if a file is opened with <code class="code">O_APPEND</code>,  <code class="code">pwrite</code>
appends data to the end of the file, regardless of the value of
<code class="code">offset</code>.
</p>
<p>When the source file is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> the
<code class="code">pwrite</code> function is in fact <code class="code">pwrite64</code> and the type
<code class="code">off_t</code> has 64 bits, which makes it possible to handle files up to
2^63 bytes in length.
</p>
<p>The return value of <code class="code">pwrite</code> describes the number of written bytes.
In the error case it returns <em class="math">-1</em> like <code class="code">write</code> does and the
error codes are also the same, with these additions:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The value given for <var class="var">offset</var> is negative and therefore illegal.
</p>
</dd>
<dt><code class="code">ESPIPE</code></dt>
<dd><p>The file descriptor <var class="var">filedes</var> is associated with a pipe or a FIFO and
this device does not allow positioning of the file pointer.
</p></dd>
</dl>

<p>The function is an extension defined in the Unix Single Specification
version 2.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pwrite64"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">pwrite64</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, const void *<var class="var">buffer</var>, size_t <var class="var">size</var>, off64_t <var class="var">offset</var>)</code><a class="copiable-link" href="#index-pwrite64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to the <code class="code">pwrite</code> function.  The difference
is that the <var class="var">offset</var> parameter is of type <code class="code">off64_t</code> instead of
<code class="code">off_t</code> which makes it possible on 32 bit machines to address
files larger than 2^31 bytes and up to 2^63 bytes.  The
file descriptor <code class="code">filedes</code> must be opened using <code class="code">open64</code> since
otherwise the large offsets possible with <code class="code">off64_t</code> will lead to
errors with a descriptor in small file mode.
</p>
<p>When the source file is compiled using <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit machine this function is actually available under the name
<code class="code">pwrite</code> and so transparently replaces the 32 bit interface.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="File-Position-Primitive.html">Setting the File Position of a Descriptor</a>, Previous: <a href="Opening-and-Closing-Files.html">Opening and Closing Files</a>, Up: <a href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
