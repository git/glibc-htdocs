<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>EOF and Errors (The GNU C Library)</title>

<meta name="description" content="EOF and Errors (The GNU C Library)">
<meta name="keywords" content="EOF and Errors (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="I_002fO-on-Streams.html" rel="up" title="I/O on Streams">
<link href="Error-Recovery.html" rel="next" title="Error Recovery">
<link href="Formatted-Input.html" rel="prev" title="Formatted Input">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="EOF-and-Errors">
<div class="nav-panel">
<p>
Next: <a href="Error-Recovery.html" accesskey="n" rel="next">Recovering from errors</a>, Previous: <a href="Formatted-Input.html" accesskey="p" rel="prev">Formatted Input</a>, Up: <a href="I_002fO-on-Streams.html" accesskey="u" rel="up">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="End_002dOf_002dFile-and-Errors"><span>12.15 End-Of-File and Errors<a class="copiable-link" href="#End_002dOf_002dFile-and-Errors"> &para;</a></span></h3>

<a class="index-entry-id" id="index-end-of-file_002c-on-a-stream"></a>
<p>Many of the functions described in this chapter return the value of the
macro <code class="code">EOF</code> to indicate unsuccessful completion of the operation.
Since <code class="code">EOF</code> is used to report both end of file and random errors,
it&rsquo;s often better to use the <code class="code">feof</code> function to check explicitly
for end of file and <code class="code">ferror</code> to check for errors.  These functions
check indicators that are part of the internal state of the stream
object, indicators set if the appropriate condition was detected by a
previous I/O operation on that stream.
</p>
<p>The end of file and error conditions are mutually exclusive.  For a
narrow oriented stream, end of file is not considered an error.  For
wide oriented streams, reaching the end of the underlying file can
result an error if the underlying file ends with an incomplete multibyte
sequence.  This is reported as an error by <code class="code">ferror</code>, and not as an
end of file by <code class="code">feof</code>.  End of file on wide oriented streams that
does not fall into the middle of a multibyte sequence is reported via
<code class="code">feof</code>.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-EOF"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">EOF</strong><a class="copiable-link" href="#index-EOF"> &para;</a></span></dt>
<dd>
<p>This macro is an integer value that is returned by a number of narrow
stream functions to indicate an end-of-file condition, or some other
error situation.  With the GNU C Library, <code class="code">EOF</code> is <code class="code">-1</code>.  In
other libraries, its value may be some other negative number.
</p>
<p>This symbol is declared in <samp class="file">stdio.h</samp>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-WEOF-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">WEOF</strong><a class="copiable-link" href="#index-WEOF-1"> &para;</a></span></dt>
<dd>
<p>This macro is an integer value that is returned by a number of wide
stream functions to indicate an end-of-file condition, or some other
error situation.  With the GNU C Library, <code class="code">WEOF</code> is <code class="code">-1</code>.  In
other libraries, its value may be some other negative number.
</p>
<p>This symbol is declared in <samp class="file">wchar.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-feof"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">feof</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-feof"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">feof</code> function returns nonzero if and only if the end-of-file
indicator for the stream <var class="var">stream</var> is set.
</p>
<p>This symbol is declared in <samp class="file">stdio.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-feof_005funlocked"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">feof_unlocked</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-feof_005funlocked"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">feof_unlocked</code> function is equivalent to the <code class="code">feof</code>
function except that it does not implicitly lock the stream.
</p>
<p>This function is a GNU extension.
</p>
<p>This symbol is declared in <samp class="file">stdio.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ferror"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">ferror</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-ferror"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">ferror</code> function returns nonzero if and only if the error
indicator for the stream <var class="var">stream</var> is set, indicating that an error
has occurred on a previous operation on the stream.
</p>
<p>This symbol is declared in <samp class="file">stdio.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ferror_005funlocked"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">ferror_unlocked</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-ferror_005funlocked"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">ferror_unlocked</code> function is equivalent to the <code class="code">ferror</code>
function except that it does not implicitly lock the stream.
</p>
<p>This function is a GNU extension.
</p>
<p>This symbol is declared in <samp class="file">stdio.h</samp>.
</p></dd></dl>

<p>In addition to setting the error indicator associated with the stream,
the functions that operate on streams also set <code class="code">errno</code> in the same
way as the corresponding low-level functions that operate on file
descriptors.  For example, all of the functions that perform output to a
stream&mdash;such as <code class="code">fputc</code>, <code class="code">printf</code>, and <code class="code">fflush</code>&mdash;are
implemented in terms of <code class="code">write</code>, and all of the <code class="code">errno</code> error
conditions defined for <code class="code">write</code> are meaningful for these functions.
For more information about the descriptor-level I/O functions, see
<a class="ref" href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Error-Recovery.html">Recovering from errors</a>, Previous: <a href="Formatted-Input.html">Formatted Input</a>, Up: <a href="I_002fO-on-Streams.html">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
