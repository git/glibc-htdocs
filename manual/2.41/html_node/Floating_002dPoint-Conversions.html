<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Floating-Point Conversions (The GNU C Library)</title>

<meta name="description" content="Floating-Point Conversions (The GNU C Library)">
<meta name="keywords" content="Floating-Point Conversions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Output.html" rel="up" title="Formatted Output">
<link href="Other-Output-Conversions.html" rel="next" title="Other Output Conversions">
<link href="Integer-Conversions.html" rel="prev" title="Integer Conversions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Floating_002dPoint-Conversions">
<div class="nav-panel">
<p>
Next: <a href="Other-Output-Conversions.html" accesskey="n" rel="next">Other Output Conversions</a>, Previous: <a href="Integer-Conversions.html" accesskey="p" rel="prev">Integer Conversions</a>, Up: <a href="Formatted-Output.html" accesskey="u" rel="up">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Floating_002dPoint-Conversions-1"><span>12.12.5 Floating-Point Conversions<a class="copiable-link" href="#Floating_002dPoint-Conversions-1"> &para;</a></span></h4>

<p>This section discusses the conversion specifications for floating-point
numbers: the &lsquo;<samp class="samp">%f</samp>&rsquo;, &lsquo;<samp class="samp">%F</samp>&rsquo;, &lsquo;<samp class="samp">%e</samp>&rsquo;, &lsquo;<samp class="samp">%E</samp>&rsquo;, &lsquo;<samp class="samp">%g</samp>&rsquo;, and
&lsquo;<samp class="samp">%G</samp>&rsquo; conversions.
</p>
<p>The &lsquo;<samp class="samp">%f</samp>&rsquo; and &lsquo;<samp class="samp">%F</samp>&rsquo; conversions print their argument in fixed-point
notation, producing output of the form
[<code class="code">-</code>]<var class="var">ddd</var><code class="code">.</code><var class="var">ddd</var><!-- /@w -->,
where the number of digits following the decimal point is controlled
by the precision you specify.
</p>
<p>The &lsquo;<samp class="samp">%e</samp>&rsquo; conversion prints its argument in exponential notation,
producing output of the form
[<code class="code">-</code>]<var class="var">d</var><code class="code">.</code><var class="var">ddd</var><code class="code">e</code>[<code class="code">+</code>|<code class="code">-</code>]<var class="var">dd</var><!-- /@w -->.
Again, the number of digits following the decimal point is controlled by
the precision.  The exponent always contains at least two digits.  The
&lsquo;<samp class="samp">%E</samp>&rsquo; conversion is similar but the exponent is marked with the letter
&lsquo;<samp class="samp">E</samp>&rsquo; instead of &lsquo;<samp class="samp">e</samp>&rsquo;.
</p>
<p>The &lsquo;<samp class="samp">%g</samp>&rsquo; and &lsquo;<samp class="samp">%G</samp>&rsquo; conversions print the argument in the style
of &lsquo;<samp class="samp">%e</samp>&rsquo; or &lsquo;<samp class="samp">%E</samp>&rsquo; (respectively) if the exponent would be less
than -4 or greater than or equal to the precision; otherwise they use
the &lsquo;<samp class="samp">%f</samp>&rsquo; or &lsquo;<samp class="samp">%F</samp>&rsquo; style.  A precision of <code class="code">0</code>, is taken as 1.
Trailing zeros are removed from the fractional portion of the result and
a decimal-point character appears only if it is followed by a digit.
</p>
<p>The &lsquo;<samp class="samp">%a</samp>&rsquo; and &lsquo;<samp class="samp">%A</samp>&rsquo; conversions are meant for representing
floating-point numbers exactly in textual form so that they can be
exchanged as texts between different programs and/or machines.  The
numbers are represented in the form
[<code class="code">-</code>]<code class="code">0x</code><var class="var">h</var><code class="code">.</code><var class="var">hhh</var><code class="code">p</code>[<code class="code">+</code>|<code class="code">-</code>]<var class="var">dd</var><!-- /@w -->.
At the left of the decimal-point character exactly one digit is print.
This character is only <code class="code">0</code> if the number is denormalized.
Otherwise the value is unspecified; it is implementation dependent how many
bits are used.  The number of hexadecimal digits on the right side of
the decimal-point character is equal to the precision.  If the precision
is zero it is determined to be large enough to provide an exact
representation of the number (or it is large enough to distinguish two
adjacent values if the <code class="code">FLT_RADIX</code> is not a power of 2,
see <a class="pxref" href="Floating-Point-Parameters.html">Floating Point Parameters</a>).  For the &lsquo;<samp class="samp">%a</samp>&rsquo; conversion
lower-case characters are used to represent the hexadecimal number and
the prefix and exponent sign are printed as <code class="code">0x</code> and <code class="code">p</code>
respectively.  Otherwise upper-case characters are used and <code class="code">0X</code>
and <code class="code">P</code> are used for the representation of prefix and exponent
string.  The exponent to the base of two is printed as a decimal number
using at least one digit but at most as many digits as necessary to
represent the value exactly.
</p>
<p>If the value to be printed represents infinity or a NaN, the output is
[<code class="code">-</code>]<code class="code">inf</code><!-- /@w --> or <code class="code">nan</code> respectively if the conversion
specifier is &lsquo;<samp class="samp">%a</samp>&rsquo;, &lsquo;<samp class="samp">%e</samp>&rsquo;, &lsquo;<samp class="samp">%f</samp>&rsquo;, or &lsquo;<samp class="samp">%g</samp>&rsquo; and it is
[<code class="code">-</code>]<code class="code">INF</code><!-- /@w --> or <code class="code">NAN</code> respectively if the conversion is
&lsquo;<samp class="samp">%A</samp>&rsquo;, &lsquo;<samp class="samp">%E</samp>&rsquo;, &lsquo;<samp class="samp">%F</samp>&rsquo; or &lsquo;<samp class="samp">%G</samp>&rsquo;.  On some implementations, a NaN
may result in longer output with information about the payload of the
NaN; ISO C23 defines a macro <code class="code">_PRINTF_NAN_LEN_MAX</code> giving the
maximum length of such output.
</p>
<p>The following flags can be used to modify the behavior:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">-</samp>&rsquo;</dt>
<dd><p>Left-justify the result in the field.  Normally the result is
right-justified.
</p>
</dd>
<dt>&lsquo;<samp class="samp">+</samp>&rsquo;</dt>
<dd><p>Always include a plus or minus sign in the result.
</p>
</dd>
<dt>&lsquo;<samp class="samp"> </samp>&rsquo;</dt>
<dd><p>If the result doesn&rsquo;t start with a plus or minus sign, prefix it with a
space instead.  Since the &lsquo;<samp class="samp">+</samp>&rsquo; flag ensures that the result includes
a sign, this flag is ignored if you supply both of them.
</p>
</dd>
<dt>&lsquo;<samp class="samp">#</samp>&rsquo;</dt>
<dd><p>Specifies that the result should always include a decimal point, even
if no digits follow it.  For the &lsquo;<samp class="samp">%g</samp>&rsquo; and &lsquo;<samp class="samp">%G</samp>&rsquo; conversions,
this also forces trailing zeros after the decimal point to be left
in place where they would otherwise be removed.
</p>
</dd>
<dt>&lsquo;<samp class="samp">'</samp>&rsquo;</dt>
<dd><p>Separate the digits of the integer part of the result into groups as
specified by the locale specified for the <code class="code">LC_NUMERIC</code> category;
see <a class="pxref" href="General-Numeric.html">Generic Numeric Formatting Parameters</a>.  This flag is a GNU extension.
</p>
</dd>
<dt>&lsquo;<samp class="samp">0</samp>&rsquo;</dt>
<dd><p>Pad the field with zeros instead of spaces; the zeros are placed
after any sign.  This flag is ignored if the &lsquo;<samp class="samp">-</samp>&rsquo; flag is also
specified.
</p></dd>
</dl>

<p>The precision specifies how many digits follow the decimal-point
character for the &lsquo;<samp class="samp">%f</samp>&rsquo;, &lsquo;<samp class="samp">%F</samp>&rsquo;, &lsquo;<samp class="samp">%e</samp>&rsquo;, and &lsquo;<samp class="samp">%E</samp>&rsquo; conversions.
For these conversions, the default precision is <code class="code">6</code>.  If the precision
is explicitly <code class="code">0</code>, this suppresses the decimal point character
entirely.  For the &lsquo;<samp class="samp">%g</samp>&rsquo; and &lsquo;<samp class="samp">%G</samp>&rsquo; conversions, the precision
specifies how many significant digits to print.  Significant digits are
the first digit before the decimal point, and all the digits after it.
If the precision is <code class="code">0</code> or not specified for &lsquo;<samp class="samp">%g</samp>&rsquo; or &lsquo;<samp class="samp">%G</samp>&rsquo;,
it is treated like a value of <code class="code">1</code>.  If the value being printed
cannot be expressed accurately in the specified number of digits, the
value is rounded to the nearest number that fits.
</p>
<p>Without a type modifier, the floating-point conversions use an argument
of type <code class="code">double</code>.  (By the default argument promotions, any
<code class="code">float</code> arguments are automatically converted to <code class="code">double</code>.)
The following type modifier is supported:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">L</samp>&rsquo;</dt>
<dd><p>An uppercase &lsquo;<samp class="samp">L</samp>&rsquo; specifies that the argument is a <code class="code">long
double</code>.
</p></dd>
</dl>

<p>Here are some examples showing how numbers print using the various
floating-point conversions.  All of the numbers were printed using
this template string:
</p>
<div class="example smallexample">
<pre class="example-preformatted">&quot;|%13.4a|%13.4f|%13.4e|%13.4g|\n&quot;
</pre></div>

<p>Here is the output:
</p>
<div class="example smallexample">
<pre class="example-preformatted">|  0x0.0000p+0|       0.0000|   0.0000e+00|            0|
|  0x1.0000p-1|       0.5000|   5.0000e-01|          0.5|
|  0x1.0000p+0|       1.0000|   1.0000e+00|            1|
| -0x1.0000p+0|      -1.0000|  -1.0000e+00|           -1|
|  0x1.9000p+6|     100.0000|   1.0000e+02|          100|
|  0x1.f400p+9|    1000.0000|   1.0000e+03|         1000|
| 0x1.3880p+13|   10000.0000|   1.0000e+04|        1e+04|
| 0x1.81c8p+13|   12345.0000|   1.2345e+04|    1.234e+04|
| 0x1.86a0p+16|  100000.0000|   1.0000e+05|        1e+05|
| 0x1.e240p+16|  123456.0000|   1.2346e+05|    1.235e+05|
</pre></div>

<p>Notice how the &lsquo;<samp class="samp">%g</samp>&rsquo; conversion drops trailing zeros.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Other-Output-Conversions.html">Other Output Conversions</a>, Previous: <a href="Integer-Conversions.html">Integer Conversions</a>, Up: <a href="Formatted-Output.html">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
