<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Scanning All Users (The GNU C Library)</title>

<meta name="description" content="Scanning All Users (The GNU C Library)">
<meta name="keywords" content="Scanning All Users (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="User-Database.html" rel="up" title="User Database">
<link href="Writing-a-User-Entry.html" rel="next" title="Writing a User Entry">
<link href="Lookup-User.html" rel="prev" title="Lookup User">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Scanning-All-Users">
<div class="nav-panel">
<p>
Next: <a href="Writing-a-User-Entry.html" accesskey="n" rel="next">Writing a User Entry</a>, Previous: <a href="Lookup-User.html" accesskey="p" rel="prev">Looking Up One User</a>, Up: <a href="User-Database.html" accesskey="u" rel="up">User Database</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Scanning-the-List-of-All-Users"><span>31.13.3 Scanning the List of All Users<a class="copiable-link" href="#Scanning-the-List-of-All-Users"> &para;</a></span></h4>
<a class="index-entry-id" id="index-scanning-the-user-list"></a>

<p>This section explains how a program can read the list of all users in
the system, one user at a time.  The functions described here are
declared in <samp class="file">pwd.h</samp>.
</p>
<p>You can use the <code class="code">fgetpwent</code> function to read user entries from a
particular file.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fgetpwent"><span class="category-def">Function: </span><span><code class="def-type">struct passwd *</code> <strong class="def-name">fgetpwent</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-fgetpwent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:fpwent
| AS-Unsafe corrupt lock
| AC-Unsafe corrupt lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function reads the next user entry from <var class="var">stream</var> and returns a
pointer to the entry.  The structure is statically allocated and is
rewritten on subsequent calls to <code class="code">fgetpwent</code>.  You must copy the
contents of the structure if you wish to save the information.
</p>
<p>The stream must correspond to a file in the same format as the standard
user database file.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fgetpwent_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fgetpwent_r</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, struct passwd *<var class="var">result_buf</var>, char *<var class="var">buffer</var>, size_t <var class="var">buflen</var>, struct passwd **<var class="var">result</var>)</code><a class="copiable-link" href="#index-fgetpwent_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe corrupt lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">fgetpwent</code> in that it reads the next
user entry from <var class="var">stream</var>.  But the result is returned in the
structure pointed to by <var class="var">result_buf</var>.  The
first <var class="var">buflen</var> bytes of the additional buffer pointed to by
<var class="var">buffer</var> are used to contain additional information, normally
strings which are pointed to by the elements of the result structure.
</p>
<p>The stream must correspond to a file in the same format as the standard
user database file.
</p>
<p>If the function returns zero <var class="var">result</var> points to the structure with
the wanted data (normally this is in <var class="var">result_buf</var>).  If errors
occurred the return value is nonzero and <var class="var">result</var> contains a null
pointer.
</p></dd></dl>

<p>The way to scan all the entries in the user database is with
<code class="code">setpwent</code>, <code class="code">getpwent</code>, and <code class="code">endpwent</code>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setpwent"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">setpwent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-setpwent"> &para;</a></span></dt>
<dd>

<p>Preliminary:
| MT-Unsafe race:pwent locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function initializes a stream which <code class="code">getpwent</code> and
<code class="code">getpwent_r</code> use to read the user database.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getpwent"><span class="category-def">Function: </span><span><code class="def-type">struct passwd *</code> <strong class="def-name">getpwent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-getpwent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:pwent race:pwentbuf locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getpwent</code> function reads the next entry from the stream
initialized by <code class="code">setpwent</code>.  It returns a pointer to the entry.  The
structure is statically allocated and is rewritten on subsequent calls
to <code class="code">getpwent</code>.  You must copy the contents of the structure if you
wish to save the information.
</p>
<p>A null pointer is returned when no more entries are available.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getpwent_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getpwent_r</strong> <code class="def-code-arguments">(struct passwd *<var class="var">result_buf</var>, char *<var class="var">buffer</var>, size_t <var class="var">buflen</var>, struct passwd **<var class="var">result</var>)</code><a class="copiable-link" href="#index-getpwent_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:pwent locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">getpwent</code> in that it returns the next
entry from the stream initialized by <code class="code">setpwent</code>.  Like
<code class="code">fgetpwent_r</code>, it uses the user-supplied buffers in
<var class="var">result_buf</var> and <var class="var">buffer</var> to return the information requested.
</p>
<p>The return values are the same as for <code class="code">fgetpwent_r</code>.
</p>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-endpwent"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">endpwent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-endpwent"> &para;</a></span></dt>
<dd>

<p>Preliminary:
| MT-Unsafe race:pwent locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function closes the internal stream used by <code class="code">getpwent</code> or
<code class="code">getpwent_r</code>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Writing-a-User-Entry.html">Writing a User Entry</a>, Previous: <a href="Lookup-User.html">Looking Up One User</a>, Up: <a href="User-Database.html">User Database</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
