<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Signaling Yourself (The GNU C Library)</title>

<meta name="description" content="Signaling Yourself (The GNU C Library)">
<meta name="keywords" content="Signaling Yourself (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Generating-Signals.html" rel="up" title="Generating Signals">
<link href="Signaling-Another-Process.html" rel="next" title="Signaling Another Process">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
kbd.kbd {font-style: oblique}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Signaling-Yourself">
<div class="nav-panel">
<p>
Next: <a href="Signaling-Another-Process.html" accesskey="n" rel="next">Signaling Another Process</a>, Up: <a href="Generating-Signals.html" accesskey="u" rel="up">Generating Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Signaling-Yourself-1"><span>25.6.1 Signaling Yourself<a class="copiable-link" href="#Signaling-Yourself-1"> &para;</a></span></h4>

<p>A process can send itself a signal with the <code class="code">raise</code> function.  This
function is declared in <samp class="file">signal.h</samp>.
<a class="index-entry-id" id="index-signal_002eh-5"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-raise"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">raise</strong> <code class="def-code-arguments">(int <var class="var">signum</var>)</code><a class="copiable-link" href="#index-raise"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">raise</code> function sends the signal <var class="var">signum</var> to the calling
process.  It returns zero if successful and a nonzero value if it fails.
About the only reason for failure would be if the value of <var class="var">signum</var>
is invalid.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gsignal"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">gsignal</strong> <code class="def-code-arguments">(int <var class="var">signum</var>)</code><a class="copiable-link" href="#index-gsignal"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">gsignal</code> function does the same thing as <code class="code">raise</code>; it is
provided only for compatibility with SVID.
</p></dd></dl>

<p>One convenient use for <code class="code">raise</code> is to reproduce the default behavior
of a signal that you have trapped.  For instance, suppose a user of your
program types the SUSP character (usually <kbd class="kbd">C-z</kbd>; see <a class="pxref" href="Special-Characters.html">Special Characters</a>) to send it an interactive stop signal
(<code class="code">SIGTSTP</code>), and you want to clean up some internal data buffers
before stopping.  You might set this up like this:
</p>

<div class="example smallexample">
<pre class="example-preformatted">#include &lt;signal.h&gt;

/* <span class="r">When a stop signal arrives, set the action back to the default
   and then resend the signal after doing cleanup actions.</span> */

void
tstp_handler (int sig)
{
  signal (SIGTSTP, SIG_DFL);
  /* <span class="r">Do cleanup actions here.</span> */
  ...
  raise (SIGTSTP);
}

/* <span class="r">When the process is continued again, restore the signal handler.</span> */

void
cont_handler (int sig)
{
  signal (SIGCONT, cont_handler);
  signal (SIGTSTP, tstp_handler);
}

</pre><div class="group"><pre class="example-preformatted">/* <span class="r">Enable both handlers during program initialization.</span> */

int
main (void)
{
  signal (SIGCONT, cont_handler);
  signal (SIGTSTP, tstp_handler);
  ...
}
</pre></div></div>

<p><strong class="strong">Portability note:</strong> <code class="code">raise</code> was invented by the ISO&nbsp;C<!-- /@w -->
committee.  Older systems may not support it, so using <code class="code">kill</code> may
be more portable.  See <a class="xref" href="Signaling-Another-Process.html">Signaling Another Process</a>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Signaling-Another-Process.html">Signaling Another Process</a>, Up: <a href="Generating-Signals.html">Generating Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
