<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>mtab (The GNU C Library)</title>

<meta name="description" content="mtab (The GNU C Library)">
<meta name="keywords" content="mtab (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Mount-Information.html" rel="up" title="Mount Information">
<link href="Other-Mount-Information.html" rel="next" title="Other Mount Information">
<link href="fstab.html" rel="prev" title="fstab">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="mtab">
<div class="nav-panel">
<p>
Next: <a href="Other-Mount-Information.html" accesskey="n" rel="next">Other (Non-libc) Sources of Mount Information</a>, Previous: <a href="fstab.html" accesskey="p" rel="prev">The <samp class="file">fstab</samp> file</a>, Up: <a href="Mount-Information.html" accesskey="u" rel="up">Mount Information</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="The-mtab-file"><span>32.3.1.2 The <samp class="file">mtab</samp> file<a class="copiable-link" href="#The-mtab-file"> &para;</a></span></h4>
<p>The following functions and data structure access the <samp class="file">mtab</samp> file.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-mntent"><span class="category-def">Data Type: </span><span><strong class="def-name">struct mntent</strong><a class="copiable-link" href="#index-struct-mntent"> &para;</a></span></dt>
<dd>
<p>This structure is used with the <code class="code">getmntent</code>, <code class="code">getmntent_r</code>,
<code class="code">addmntent</code>, and <code class="code">hasmntopt</code> functions.
</p>
<dl class="table">
<dt><code class="code">char *mnt_fsname</code></dt>
<dd><p>This element contains a pointer to a string describing the name of the
special device from which the filesystem is mounted.  It corresponds to
the <code class="code">fs_spec</code> element in <code class="code">struct fstab</code>.
</p>
</dd>
<dt><code class="code">char *mnt_dir</code></dt>
<dd><p>This element points to a string describing the mount point of the
filesystem.  It corresponds to the <code class="code">fs_file</code> element in
<code class="code">struct fstab</code>.
</p>
</dd>
<dt><code class="code">char *mnt_type</code></dt>
<dd><p><code class="code">mnt_type</code> describes the filesystem type and is therefore
equivalent to <code class="code">fs_vfstype</code> in <code class="code">struct fstab</code>.  <samp class="file">mntent.h</samp>
defines a few symbolic names for some of the values this string can have.
But since the kernel can support arbitrary filesystems it does not
make much sense to give them symbolic names.  If one knows the symbol
name one also knows the filesystem name.  Nevertheless here follows the
list of the symbols provided in <samp class="file">mntent.h</samp>.
</p>
<dl class="vtable">
<dt><a id="index-MNTTYPE_005fIGNORE"></a><span><code class="code">MNTTYPE_IGNORE</code><a class="copiable-link" href="#index-MNTTYPE_005fIGNORE"> &para;</a></span></dt>
<dd><p>This symbol expands to <code class="code">&quot;ignore&quot;</code>.  The value is sometimes used in
<samp class="file">fstab</samp> files to make sure entries are not used without removing them.
</p></dd>
<dt><a id="index-MNTTYPE_005fNFS"></a><span><code class="code">MNTTYPE_NFS</code><a class="copiable-link" href="#index-MNTTYPE_005fNFS"> &para;</a></span></dt>
<dd><p>Expands to <code class="code">&quot;nfs&quot;</code>.  Using this macro sometimes could make sense
since it names the default NFS implementation, in case both version 2
and 3 are supported.
</p></dd>
<dt><a id="index-MNTTYPE_005fSWAP"></a><span><code class="code">MNTTYPE_SWAP</code><a class="copiable-link" href="#index-MNTTYPE_005fSWAP"> &para;</a></span></dt>
<dd><p>This symbol expands to <code class="code">&quot;swap&quot;</code>.  It names the special <samp class="file">fstab</samp>
entry which names one of the possibly multiple swap partitions.
</p></dd>
</dl>

</dd>
<dt><code class="code">char *mnt_opts</code></dt>
<dd><p>The element contains a string describing the options used while mounting
the filesystem.  As for the equivalent element <code class="code">fs_mntops</code> of
<code class="code">struct fstab</code> it is best to use the function <code class="code">getsubopt</code>
(see <a class="pxref" href="Suboptions.html">Parsing of Suboptions</a>) to access the parts of this string.
</p>
<p>The <samp class="file">mntent.h</samp> file defines a number of macros with string values
which correspond to some of the options understood by the kernel.  There
might be many more options which are possible so it doesn&rsquo;t make much sense
to rely on these macros but to be consistent here is the list:
</p>
<dl class="vtable">
<dt><a id="index-MNTOPT_005fDEFAULTS"></a><span><code class="code">MNTOPT_DEFAULTS</code><a class="copiable-link" href="#index-MNTOPT_005fDEFAULTS"> &para;</a></span></dt>
<dd><p>Expands to <code class="code">&quot;defaults&quot;</code>.  This option should be used alone since it
indicates all values for the customizable values are chosen to be the
default.
</p></dd>
<dt><a id="index-MNTOPT_005fRO"></a><span><code class="code">MNTOPT_RO</code><a class="copiable-link" href="#index-MNTOPT_005fRO"> &para;</a></span></dt>
<dd><p>Expands to <code class="code">&quot;ro&quot;</code>.  See the <code class="code">FSTAB_RO</code> value, it means the
filesystem is mounted read-only.
</p></dd>
<dt><a id="index-MNTOPT_005fRW"></a><span><code class="code">MNTOPT_RW</code><a class="copiable-link" href="#index-MNTOPT_005fRW"> &para;</a></span></dt>
<dd><p>Expands to <code class="code">&quot;rw&quot;</code>.  See the <code class="code">FSTAB_RW</code> value, it means the
filesystem is mounted with read and write permissions.
</p></dd>
<dt><a id="index-MNTOPT_005fSUID"></a><span><code class="code">MNTOPT_SUID</code><a class="copiable-link" href="#index-MNTOPT_005fSUID"> &para;</a></span></dt>
<dd><p>Expands to <code class="code">&quot;suid&quot;</code>.  This means that the SUID bit (see <a class="pxref" href="How-Change-Persona.html">How an Application Can Change Persona</a>) is respected when a program from the filesystem is
started.
</p></dd>
<dt><a id="index-MNTOPT_005fNOSUID"></a><span><code class="code">MNTOPT_NOSUID</code><a class="copiable-link" href="#index-MNTOPT_005fNOSUID"> &para;</a></span></dt>
<dd><p>Expands to <code class="code">&quot;nosuid&quot;</code>.  This is the opposite of <code class="code">MNTOPT_SUID</code>,
the SUID bit for all files from the filesystem is ignored.
</p></dd>
<dt><a id="index-MNTOPT_005fNOAUTO"></a><span><code class="code">MNTOPT_NOAUTO</code><a class="copiable-link" href="#index-MNTOPT_005fNOAUTO"> &para;</a></span></dt>
<dd><p>Expands to <code class="code">&quot;noauto&quot;</code>.  At startup time the <code class="code">mount</code> program
will ignore this entry if it is started with the <code class="code">-a</code> option to
mount all filesystems mentioned in the <samp class="file">fstab</samp> file.
</p></dd>
</dl>

<p>As for the <code class="code">FSTAB_*</code> entries introduced above it is important to
use <code class="code">strcmp</code> to check for equality.
</p>
</dd>
<dt><code class="code">mnt_freq</code></dt>
<dd><p>This elements corresponds to <code class="code">fs_freq</code> and also specifies the
frequency in days in which dumps are made.
</p>
</dd>
<dt><code class="code">mnt_passno</code></dt>
<dd><p>This element is equivalent to <code class="code">fs_passno</code> with the same meaning
which is uninteresting for all programs beside <code class="code">dump</code>.
</p></dd>
</dl>
</dd></dl>

<p>For accessing the <samp class="file">mtab</samp> file there is again a set of three
functions to access all entries in a row.  Unlike the functions to
handle <samp class="file">fstab</samp> these functions do not access a fixed file and there
is even a thread safe variant of the get function.  Besides this the GNU C Library
contains functions to alter the file and test for specific options.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setmntent"><span class="category-def">Function: </span><span><code class="def-type">FILE *</code> <strong class="def-name">setmntent</strong> <code class="def-code-arguments">(const char *<var class="var">file</var>, const char *<var class="var">mode</var>)</code><a class="copiable-link" href="#index-setmntent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap lock
| AC-Unsafe mem fd lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">setmntent</code> function prepares the file named <var class="var">FILE</var> which
must be in the format of a <samp class="file">fstab</samp> and <samp class="file">mtab</samp> file for the
upcoming processing through the other functions of the family.  The
<var class="var">mode</var> parameter can be chosen in the way the <var class="var">opentype</var>
parameter for <code class="code">fopen</code> (see <a class="pxref" href="Opening-Streams.html">Opening Streams</a>) can be chosen.  If
the file is opened for writing the file is also allowed to be empty.
</p>
<p>If the file was successfully opened <code class="code">setmntent</code> returns a file
handle for future use.  Otherwise the return value is <code class="code">NULL</code>
and <code class="code">errno</code> is set accordingly.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-endmntent"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">endmntent</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-endmntent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap lock
| AC-Unsafe lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function takes for the <var class="var">stream</var> parameter a file handle which
previously was returned from the <code class="code">setmntent</code> call.
<code class="code">endmntent</code> closes the stream and frees all resources.
</p>
<p>The return value is <em class="math">1</em> unless an error occurred in which case it
is <em class="math">0</em>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getmntent"><span class="category-def">Function: </span><span><code class="def-type">struct mntent *</code> <strong class="def-name">getmntent</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-getmntent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:mntentbuf locale
| AS-Unsafe corrupt heap init
| AC-Unsafe init corrupt lock mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getmntent</code> function takes as the parameter a file handle
previously returned by a successful call to <code class="code">setmntent</code>.  It returns
a pointer to a static variable of type <code class="code">struct mntent</code> which is
filled with the information from the next entry from the file currently
read.
</p>
<p>The file format used prescribes the use of spaces or tab characters to
separate the fields.  This makes it harder to use names containing one
of these characters (e.g., mount points using spaces).  Therefore
these characters are encoded in the files and the <code class="code">getmntent</code>
function takes care of the decoding while reading the entries back in.
<code class="code">'\040'</code> is used to encode a space character, <code class="code">'\011'</code> to
encode a tab character, <code class="code">'\012'</code> to encode a newline character,
and <code class="code">'\\'</code> to encode a backslash.
</p>
<p>If there was an error or the end of the file is reached the return value
is <code class="code">NULL</code>.
</p>
<p>This function is not thread-safe since all calls to this function return
a pointer to the same static variable.  <code class="code">getmntent_r</code> should be
used in situations where multiple threads access the file.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getmntent_005fr"><span class="category-def">Function: </span><span><code class="def-type">struct mntent *</code> <strong class="def-name">getmntent_r</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, struct mntent *<var class="var">result</var>, char *<var class="var">buffer</var>, int <var class="var">bufsize</var>)</code><a class="copiable-link" href="#index-getmntent_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe corrupt lock mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getmntent_r</code> function is the reentrant variant of
<code class="code">getmntent</code>.  It also returns the next entry from the file and
returns a pointer.  The actual variable the values are stored in is not
static, though.  Instead the function stores the values in the variable
pointed to by the <var class="var">result</var> parameter.  Additional information (e.g.,
the strings pointed to by the elements of the result) are kept in the
buffer of size <var class="var">bufsize</var> pointed to by <var class="var">buffer</var>.
</p>
<p>Escaped characters (space, tab, backslash) are converted back in the
same way as it happens for <code class="code">getmentent</code>.
</p>
<p>The function returns a <code class="code">NULL</code> pointer in error cases.  Errors could be:
</p><ul class="itemize mark-bullet">
<li>error while reading the file,
</li><li>end of file reached,
</li><li><var class="var">bufsize</var> is too small for reading a complete new entry.
</li></ul>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-addmntent"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">addmntent</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, const struct mntent *<var class="var">mnt</var>)</code><a class="copiable-link" href="#index-addmntent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:stream locale
| AS-Unsafe corrupt
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">addmntent</code> function allows adding a new entry to the file
previously opened with <code class="code">setmntent</code>.  The new entries are always
appended.  I.e., even if the position of the file descriptor is not at
the end of the file this function does not overwrite an existing entry
following the current position.
</p>
<p>The implication of this is that to remove an entry from a file one has
to create a new file while leaving out the entry to be removed and after
closing the file remove the old one and rename the new file to the
chosen name.
</p>
<p>This function takes care of spaces and tab characters in the names to be
written to the file.  It converts them and the backslash character into
the format described in the <code class="code">getmntent</code> description above.
</p>
<p>This function returns <em class="math">0</em> in case the operation was successful.
Otherwise the return value is <em class="math">1</em> and <code class="code">errno</code> is set
appropriately.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-hasmntopt"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">hasmntopt</strong> <code class="def-code-arguments">(const struct mntent *<var class="var">mnt</var>, const char *<var class="var">opt</var>)</code><a class="copiable-link" href="#index-hasmntopt"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function can be used to check whether the string pointed to by the
<code class="code">mnt_opts</code> element of the variable pointed to by <var class="var">mnt</var> contains
the option <var class="var">opt</var>.  If this is true a pointer to the beginning of the
option in the <code class="code">mnt_opts</code> element is returned.  If no such option
exists the function returns <code class="code">NULL</code>.
</p>
<p>This function is useful to test whether a specific option is present but
when all options have to be processed one is better off with using the
<code class="code">getsubopt</code> function to iterate over all options in the string.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Other-Mount-Information.html">Other (Non-libc) Sources of Mount Information</a>, Previous: <a href="fstab.html">The <samp class="file">fstab</samp> file</a>, Up: <a href="Mount-Information.html">Mount Information</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
