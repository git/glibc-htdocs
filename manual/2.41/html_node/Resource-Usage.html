<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Resource Usage (The GNU C Library)</title>

<meta name="description" content="Resource Usage (The GNU C Library)">
<meta name="keywords" content="Resource Usage (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Resource-Usage-And-Limitation.html" rel="up" title="Resource Usage And Limitation">
<link href="Limits-on-Resources.html" rel="next" title="Limits on Resources">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Resource-Usage">
<div class="nav-panel">
<p>
Next: <a href="Limits-on-Resources.html" accesskey="n" rel="next">Limiting Resource Usage</a>, Up: <a href="Resource-Usage-And-Limitation.html" accesskey="u" rel="up">Resource Usage And Limitation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Resource-Usage-1"><span>23.1 Resource Usage<a class="copiable-link" href="#Resource-Usage-1"> &para;</a></span></h3>

<a class="index-entry-id" id="index-sys_002fresource_002eh"></a>
<p>The function <code class="code">getrusage</code> and the data type <code class="code">struct rusage</code>
are used to examine the resource usage of a process.  They are declared
in <samp class="file">sys/resource.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getrusage"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getrusage</strong> <code class="def-code-arguments">(int <var class="var">processes</var>, struct rusage *<var class="var">rusage</var>)</code><a class="copiable-link" href="#index-getrusage"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function reports resource usage totals for processes specified by
<var class="var">processes</var>, storing the information in <code class="code">*<var class="var">rusage</var></code>.
</p>
<p>In most systems, <var class="var">processes</var> has only two valid values:
</p>
<dl class="vtable">
<dt><a id="index-RUSAGE_005fSELF"></a><span><code class="code">RUSAGE_SELF</code><a class="copiable-link" href="#index-RUSAGE_005fSELF"> &para;</a></span></dt>
<dd>
<p>Just the current process.
</p>
</dd>
<dt><a id="index-RUSAGE_005fCHILDREN"></a><span><code class="code">RUSAGE_CHILDREN</code><a class="copiable-link" href="#index-RUSAGE_005fCHILDREN"> &para;</a></span></dt>
<dd>
<p>All child processes (direct and indirect) that have already terminated.
</p></dd>
</dl>

<p>The return value of <code class="code">getrusage</code> is zero for success, and <code class="code">-1</code>
for failure.
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The argument <var class="var">processes</var> is not valid.
</p></dd>
</dl>
</dd></dl>

<p>One way of getting resource usage for a particular child process is with
the function <code class="code">wait4</code>, which returns totals for a child when it
terminates.  See <a class="xref" href="BSD-Wait-Functions.html">BSD Process Wait Function</a>.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-rusage"><span class="category-def">Data Type: </span><span><strong class="def-name">struct rusage</strong><a class="copiable-link" href="#index-struct-rusage"> &para;</a></span></dt>
<dd>
<p>This data type stores various resource usage statistics.  It has the
following members, and possibly others:
</p>
<dl class="table">
<dt><code class="code">struct timeval ru_utime</code></dt>
<dd><p>Time spent executing user instructions.
</p>
</dd>
<dt><code class="code">struct timeval ru_stime</code></dt>
<dd><p>Time spent in operating system code on behalf of <var class="var">processes</var>.
</p>
</dd>
<dt><code class="code">long int ru_maxrss</code></dt>
<dd><p>The maximum resident set size used, in kilobytes.  That is, the maximum
number of kilobytes of physical memory that <var class="var">processes</var> used
simultaneously.
</p>
</dd>
<dt><code class="code">long int ru_ixrss</code></dt>
<dd><p>An integral value expressed in kilobytes times ticks of execution, which
indicates the amount of memory used by text that was shared with other
processes.
</p>
</dd>
<dt><code class="code">long int ru_idrss</code></dt>
<dd><p>An integral value expressed the same way, which is the amount of
unshared memory used for data.
</p>
</dd>
<dt><code class="code">long int ru_isrss</code></dt>
<dd><p>An integral value expressed the same way, which is the amount of
unshared memory used for stack space.
</p>
</dd>
<dt><code class="code">long int ru_minflt</code></dt>
<dd><p>The number of page faults which were serviced without requiring any I/O.
</p>
</dd>
<dt><code class="code">long int ru_majflt</code></dt>
<dd><p>The number of page faults which were serviced by doing I/O.
</p>
</dd>
<dt><code class="code">long int ru_nswap</code></dt>
<dd><p>The number of times <var class="var">processes</var> was swapped entirely out of main memory.
</p>
</dd>
<dt><code class="code">long int ru_inblock</code></dt>
<dd><p>The number of times the file system had to read from the disk on behalf
of <var class="var">processes</var>.
</p>
</dd>
<dt><code class="code">long int ru_oublock</code></dt>
<dd><p>The number of times the file system had to write to the disk on behalf
of <var class="var">processes</var>.
</p>
</dd>
<dt><code class="code">long int ru_msgsnd</code></dt>
<dd><p>Number of IPC messages sent.
</p>
</dd>
<dt><code class="code">long int ru_msgrcv</code></dt>
<dd><p>Number of IPC messages received.
</p>
</dd>
<dt><code class="code">long int ru_nsignals</code></dt>
<dd><p>Number of signals received.
</p>
</dd>
<dt><code class="code">long int ru_nvcsw</code></dt>
<dd><p>The number of times <var class="var">processes</var> voluntarily invoked a context switch
(usually to wait for some service).
</p>
</dd>
<dt><code class="code">long int ru_nivcsw</code></dt>
<dd><p>The number of times an involuntary context switch took place (because
a time slice expired, or another process of higher priority was
scheduled).
</p></dd>
</dl>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Limits-on-Resources.html">Limiting Resource Usage</a>, Up: <a href="Resource-Usage-And-Limitation.html">Resource Usage And Limitation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
