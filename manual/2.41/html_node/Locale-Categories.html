<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Locale Categories (The GNU C Library)</title>

<meta name="description" content="Locale Categories (The GNU C Library)">
<meta name="keywords" content="Locale Categories (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Locales.html" rel="up" title="Locales">
<link href="Setting-the-Locale.html" rel="next" title="Setting the Locale">
<link href="Choosing-Locale.html" rel="prev" title="Choosing Locale">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Locale-Categories">
<div class="nav-panel">
<p>
Next: <a href="Setting-the-Locale.html" accesskey="n" rel="next">How Programs Set the Locale</a>, Previous: <a href="Choosing-Locale.html" accesskey="p" rel="prev">Choosing a Locale</a>, Up: <a href="Locales.html" accesskey="u" rel="up">Locales and Internationalization</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Locale-Categories-1"><span>7.3 Locale Categories<a class="copiable-link" href="#Locale-Categories-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-categories-for-locales"></a>
<a class="index-entry-id" id="index-locale-categories"></a>

<p>The purposes that locales serve are grouped into <em class="dfn">categories</em>, so
that a user or a program can choose the locale for each category
independently.  Here is a table of categories; each name is both an
environment variable that a user can set, and a macro name that you can
use as the first argument to <code class="code">setlocale</code>.
</p>
<p>The contents of the environment variable (or the string in the second
argument to <code class="code">setlocale</code>) has to be a valid locale name.
See <a class="xref" href="Locale-Names.html">Locale Names</a>.
</p>
<dl class="vtable">
<dt><a id="index-LC_005fCOLLATE"></a><span><code class="code">LC_COLLATE</code><a class="copiable-link" href="#index-LC_005fCOLLATE"> &para;</a></span></dt>
<dd>
<p>This category applies to collation of strings (functions <code class="code">strcoll</code>
and <code class="code">strxfrm</code>); see <a class="ref" href="Collation-Functions.html">Collation Functions</a>.
</p>
</dd>
<dt><a id="index-LC_005fCTYPE"></a><span><code class="code">LC_CTYPE</code><a class="copiable-link" href="#index-LC_005fCTYPE"> &para;</a></span></dt>
<dd>
<p>This category applies to classification and conversion of characters,
and to multibyte and wide characters;
see <a class="ref" href="Character-Handling.html">Character Handling</a>, and <a class="ref" href="Character-Set-Handling.html">Character Set Handling</a>.
</p>
</dd>
<dt><a id="index-LC_005fMONETARY"></a><span><code class="code">LC_MONETARY</code><a class="copiable-link" href="#index-LC_005fMONETARY"> &para;</a></span></dt>
<dd>
<p>This category applies to formatting monetary values; see <a class="ref" href="General-Numeric.html">Generic Numeric Formatting Parameters</a>.
</p>
</dd>
<dt><a id="index-LC_005fNUMERIC"></a><span><code class="code">LC_NUMERIC</code><a class="copiable-link" href="#index-LC_005fNUMERIC"> &para;</a></span></dt>
<dd>
<p>This category applies to formatting numeric values that are not
monetary; see <a class="ref" href="General-Numeric.html">Generic Numeric Formatting Parameters</a>.
</p>
</dd>
<dt><a id="index-LC_005fTIME"></a><span><code class="code">LC_TIME</code><a class="copiable-link" href="#index-LC_005fTIME"> &para;</a></span></dt>
<dd>
<p>This category applies to formatting date and time values; see
<a class="ref" href="Formatting-Calendar-Time.html">Formatting Calendar Time</a>.
</p>
</dd>
<dt><a id="index-LC_005fMESSAGES"></a><span><code class="code">LC_MESSAGES</code><a class="copiable-link" href="#index-LC_005fMESSAGES"> &para;</a></span></dt>
<dd>
<p>This category applies to selecting the language used in the user
interface for message translation (see <a class="pxref" href="The-Uniforum-approach.html">The Uniforum approach to Message Translation</a>;
see <a class="pxref" href="Message-catalogs-a-la-X_002fOpen.html">X/Open Message Catalog Handling</a>)  and contains regular expressions
for affirmative and negative responses.
</p>
</dd>
<dt><a id="index-LC_005fALL"></a><span><code class="code">LC_ALL</code><a class="copiable-link" href="#index-LC_005fALL"> &para;</a></span></dt>
<dd>
<p>This is not a category; it is only a macro that you can use
with <code class="code">setlocale</code> to set a single locale for all purposes.  Setting
this environment variable overwrites all selections by the other
<code class="code">LC_*</code> variables or <code class="code">LANG</code>.
</p>
</dd>
<dt><a id="index-LANG"></a><span><code class="code">LANG</code><a class="copiable-link" href="#index-LANG"> &para;</a></span></dt>
<dd>
<p>If this environment variable is defined, its value specifies the locale
to use for all purposes except as overridden by the variables above.
</p></dd>
</dl>

<a class="index-entry-id" id="index-LANGUAGE"></a>
<p>When developing the message translation functions it was felt that the
functionality provided by the variables above is not sufficient.  For
example, it should be possible to specify more than one locale name.
Take a Swedish user who better speaks German than English, and a program
whose messages are output in English by default.  It should be possible
to specify that the first choice of language is Swedish, the second
German, and if this also fails to use English.  This is
possible with the variable <code class="code">LANGUAGE</code>.  For further description of
this GNU extension see <a class="ref" href="Using-gettextized-software.html">User influence on <code class="code">gettext</code></a>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Setting-the-Locale.html">How Programs Set the Locale</a>, Previous: <a href="Choosing-Locale.html">Choosing a Locale</a>, Up: <a href="Locales.html">Locales and Internationalization</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
