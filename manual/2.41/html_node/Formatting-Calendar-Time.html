<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Formatting Calendar Time (The GNU C Library)</title>

<meta name="description" content="Formatting Calendar Time (The GNU C Library)">
<meta name="keywords" content="Formatting Calendar Time (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Calendar-Time.html" rel="up" title="Calendar Time">
<link href="Parsing-Date-and-Time.html" rel="next" title="Parsing Date and Time">
<link href="Broken_002ddown-Time.html" rel="prev" title="Broken-down Time">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Formatting-Calendar-Time">
<div class="nav-panel">
<p>
Next: <a href="Parsing-Date-and-Time.html" accesskey="n" rel="next">Convert textual time and date information back</a>, Previous: <a href="Broken_002ddown-Time.html" accesskey="p" rel="prev">Broken-down Time</a>, Up: <a href="Calendar-Time.html" accesskey="u" rel="up">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Formatting-Calendar-Time-1"><span>22.5.4 Formatting Calendar Time<a class="copiable-link" href="#Formatting-Calendar-Time-1"> &para;</a></span></h4>

<p>The functions described in this section format calendar time values as
strings.  These functions are declared in the header file <samp class="file">time.h</samp>.
<a class="index-entry-id" id="index-time_002eh-2"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strftime"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">strftime</strong> <code class="def-code-arguments">(char *<var class="var">s</var>, size_t <var class="var">size</var>, const char *<var class="var">template</var>, const struct tm *<var class="var">brokentime</var>)</code><a class="copiable-link" href="#index-strftime"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env locale
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to the <code class="code">sprintf</code> function (see <a class="pxref" href="Formatted-Input.html">Formatted Input</a>), but the conversion specifications that can appear in the format
template <var class="var">template</var> are specialized for printing components of
<var class="var">brokentime</var> according to the locale currently specified for
time conversion (see <a class="pxref" href="Locales.html">Locales and Internationalization</a>) and the current time zone
(see <a class="pxref" href="Time-Zone-State.html">State Variables for Time Zones</a>).
</p>
<p>Ordinary characters appearing in the <var class="var">template</var> are copied to the
output string <var class="var">s</var>; this can include multibyte character sequences.
Conversion specifiers are introduced by a &lsquo;<samp class="samp">%</samp>&rsquo; character, followed
by an optional flag which can be one of the following.  These flags
are all GNU extensions.  The first three affect only the output of
numbers:
</p>
<dl class="table">
<dt><code class="code">_</code></dt>
<dd><p>The number is padded with spaces.
</p>
</dd>
<dt><code class="code">-</code></dt>
<dd><p>The number is not padded at all.
</p>
</dd>
<dt><code class="code">0</code></dt>
<dd><p>The number is padded with zeros even if the format specifies padding
with spaces.
</p>
</dd>
<dt><code class="code">^</code></dt>
<dd><p>The output uses uppercase characters, but only if this is possible
(see <a class="pxref" href="Case-Conversion.html">Case Conversion</a>).
</p></dd>
</dl>

<p>The default action is to pad the number with zeros to keep it a constant
width.  Numbers that do not have a range indicated below are never
padded, since there is no natural width for them.
</p>
<p>Following the flag an optional specification of the width is possible.
This is specified in decimal notation.  If the natural size of the
output of the field has less than the specified number of characters,
the result is written right adjusted and space padded to the given
size.
</p>
<p>An optional modifier can follow the optional flag and width
specification.  The modifiers are:
</p>
<dl class="table">
<dt><code class="code">E</code></dt>
<dd><p>Use the locale&rsquo;s alternative representation for date and time.  This
modifier applies to the <code class="code">%c</code>, <code class="code">%C</code>, <code class="code">%x</code>, <code class="code">%X</code>,
<code class="code">%y</code> and <code class="code">%Y</code> format specifiers.  In a Japanese locale, for
example, <code class="code">%Ex</code> might yield a date format based on the Japanese
Emperors&rsquo; reigns.
</p>
</dd>
<dt><code class="code">O</code></dt>
<dd><p>With all format specifiers that produce numbers: use the locale&rsquo;s
alternative numeric symbols.
</p>
<p>With <code class="code">%B</code>, <code class="code">%b</code>, and <code class="code">%h</code>: use the grammatical form for
month names that is appropriate when the month is named by itself,
rather than the form that is appropriate when the month is used as
part of a complete date.  The <code class="code">%OB</code> and <code class="code">%Ob</code> formats are a
C23 feature, specified in C23 to use the locale&rsquo;s &lsquo;alternative&rsquo; month
name; the GNU C Library extends this specification to say that the form used
in a complete date is the default and the form naming the month by
itself is the alternative.
</p></dd>
</dl>

<p>If the format supports the modifier but no alternative representation
is available, it is ignored.
</p>
<p>The conversion specifier ends with a format specifier taken from the
following list.  The whole &lsquo;<samp class="samp">%</samp>&rsquo; sequence is replaced in the output
string as follows:
</p>
<dl class="table">
<dt><code class="code">%a</code></dt>
<dd><p>The abbreviated weekday name according to the current locale.
</p>
</dd>
<dt><code class="code">%A</code></dt>
<dd><p>The full weekday name according to the current locale.
</p>
</dd>
<dt><code class="code">%b</code></dt>
<dd><p>The abbreviated month name according to the current locale, in the
grammatical form used when the month is part of a complete date.
As a C23 feature (with a more detailed specification in the GNU C Library),
the <code class="code">O</code> modifier can be used (<code class="code">%Ob</code>) to get the grammatical
form used when the month is named by itself.
</p>
</dd>
<dt><code class="code">%B</code></dt>
<dd><p>The full month name according to the current locale, in the
grammatical form used when the month is part of a complete date.
As a C23 feature (with a more detailed specification in the GNU C Library),
the <code class="code">O</code> modifier can be used (<code class="code">%OB</code>) to get the grammatical
form used when the month is named by itself.
</p>
<p>Note that not all languages need two different forms of the month
names, so the text produced by <code class="code">%B</code> and <code class="code">%OB</code>, and by
<code class="code">%b</code> and <code class="code">%Ob</code>, may or may not be the same, depending on
the locale.
</p>
</dd>
<dt><code class="code">%c</code></dt>
<dd><p>The preferred calendar time representation for the current locale.
</p>
</dd>
<dt><code class="code">%C</code></dt>
<dd><p>The century of the year.  This is equivalent to the greatest integer not
greater than the year divided by 100.
</p>
<p>If the <code class="code">E</code> modifier is specified (<code class="code">%EC</code>), instead produces
the name of the period for the year (e.g. an era name) in the
locale&rsquo;s alternative calendar.
</p>
</dd>
<dt><code class="code">%d</code></dt>
<dd><p>The day of the month as a decimal number (range <code class="code">01</code> through <code class="code">31</code>).
</p>
</dd>
<dt><code class="code">%D</code></dt>
<dd><p>The date using the format <code class="code">%m/%d/%y</code>.
</p>
</dd>
<dt><code class="code">%e</code></dt>
<dd><p>The day of the month like with <code class="code">%d</code>, but padded with spaces (range
<code class="code"> 1</code> through <code class="code">31</code>).
</p>
</dd>
<dt><code class="code">%F</code></dt>
<dd><p>The date using the format <code class="code">%Y-%m-%d</code>.  This is the form specified
in the ISO&nbsp;8601<!-- /@w --> standard and is the preferred form for all uses.
</p>
</dd>
<dt><code class="code">%g</code></dt>
<dd><p>The year corresponding to the ISO week number, but without the century
(range <code class="code">00</code> through <code class="code">99</code>).  This has the same format and value
as <code class="code">%y</code>, except that if the ISO week number (see <code class="code">%V</code>) belongs
to the previous or next year, that year is used instead.
</p>
</dd>
<dt><code class="code">%G</code></dt>
<dd><p>The year corresponding to the ISO week number.  This has the same format
and value as <code class="code">%Y</code>, except that if the ISO week number (see
<code class="code">%V</code>) belongs to the previous or next year, that year is used
instead.
</p>
</dd>
<dt><code class="code">%h</code></dt>
<dd><p>The abbreviated month name according to the current locale.  The action
is the same as for <code class="code">%b</code>.
</p>
</dd>
<dt><code class="code">%H</code></dt>
<dd><p>The hour as a decimal number, using a 24-hour clock (range <code class="code">00</code> through
<code class="code">23</code>).
</p>
</dd>
<dt><code class="code">%I</code></dt>
<dd><p>The hour as a decimal number, using a 12-hour clock (range <code class="code">01</code> through
<code class="code">12</code>).
</p>
</dd>
<dt><code class="code">%j</code></dt>
<dd><p>The day of the year as a decimal number (range <code class="code">001</code> through <code class="code">366</code>).
</p>
</dd>
<dt><code class="code">%k</code></dt>
<dd><p>The hour as a decimal number, using a 24-hour clock like <code class="code">%H</code>, but
padded with spaces (range <code class="code"> 0</code> through <code class="code">23</code>).
</p>
<p>This format is a GNU extension.
</p>
</dd>
<dt><code class="code">%l</code></dt>
<dd><p>The hour as a decimal number, using a 12-hour clock like <code class="code">%I</code>, but
padded with spaces (range <code class="code"> 1</code> through <code class="code">12</code>).
</p>
<p>This format is a GNU extension.
</p>
</dd>
<dt><code class="code">%m</code></dt>
<dd><p>The month as a decimal number (range <code class="code">01</code> through <code class="code">12</code>).
</p>
</dd>
<dt><code class="code">%M</code></dt>
<dd><p>The minute as a decimal number (range <code class="code">00</code> through <code class="code">59</code>).
</p>
</dd>
<dt><code class="code">%n</code></dt>
<dd><p>A single &lsquo;<samp class="samp">\n</samp>&rsquo; (newline) character.
</p>
</dd>
<dt><code class="code">%p</code></dt>
<dd><p>Either &lsquo;<samp class="samp">AM</samp>&rsquo; or &lsquo;<samp class="samp">PM</samp>&rsquo;, according to the given time value; or the
corresponding strings for the current locale.  Noon is treated as
&lsquo;<samp class="samp">PM</samp>&rsquo; and midnight as &lsquo;<samp class="samp">AM</samp>&rsquo;.  In most locales
&lsquo;<samp class="samp">AM</samp>&rsquo;/&lsquo;<samp class="samp">PM</samp>&rsquo; format is not supported, in such cases <code class="t">&quot;%p&quot;</code>
yields an empty string.
</p>
</dd>
<dt><code class="code">%P</code></dt>
<dd><p>Either &lsquo;<samp class="samp">am</samp>&rsquo; or &lsquo;<samp class="samp">pm</samp>&rsquo;, according to the given time value; or the
corresponding strings for the current locale, printed in lowercase
characters.  Noon is treated as &lsquo;<samp class="samp">pm</samp>&rsquo; and midnight as &lsquo;<samp class="samp">am</samp>&rsquo;.  In
most locales &lsquo;<samp class="samp">AM</samp>&rsquo;/&lsquo;<samp class="samp">PM</samp>&rsquo; format is not supported, in such cases
<code class="t">&quot;%P&quot;</code> yields an empty string.
</p>
<p>This format is a GNU extension.
</p>
</dd>
<dt><code class="code">%r</code></dt>
<dd><p>The complete calendar time using the AM/PM format of the current locale.
</p>
<p>In the POSIX locale, this format is equivalent to <code class="code">%I:%M:%S %p</code>.
</p>
</dd>
<dt><code class="code">%R</code></dt>
<dd><p>The hour and minute in decimal numbers using the format <code class="code">%H:%M</code>.
</p>
</dd>
<dt><code class="code">%s</code></dt>
<dd><p>The number of seconds since the POSIX Epoch,
i.e., since 1970-01-01 00:00:00 UTC.
Leap seconds are not counted unless leap second support is available.
</p>
<p>This format is a GNU extension.
</p>
</dd>
<dt><code class="code">%S</code></dt>
<dd><p>The seconds as a decimal number (range <code class="code">00</code> through <code class="code">60</code>).
</p>
</dd>
<dt><code class="code">%t</code></dt>
<dd><p>A single &lsquo;<samp class="samp">\t</samp>&rsquo; (tabulator) character.
</p>
</dd>
<dt><code class="code">%T</code></dt>
<dd><p>The time of day using decimal numbers using the format <code class="code">%H:%M:%S</code>.
</p>
</dd>
<dt><code class="code">%u</code></dt>
<dd><p>The day of the week as a decimal number (range <code class="code">1</code> through
<code class="code">7</code>), Monday being <code class="code">1</code>.
</p>
</dd>
<dt><code class="code">%U</code></dt>
<dd><p>The week number of the current year as a decimal number (range <code class="code">00</code>
through <code class="code">53</code>), starting with the first Sunday as the first day of
the first week.  Days preceding the first Sunday in the year are
considered to be in week <code class="code">00</code>.
</p>
</dd>
<dt><code class="code">%V</code></dt>
<dd><p>The ISO&nbsp;8601<!-- /@w --> week number as a decimal number (range <code class="code">01</code>
through <code class="code">53</code>).  ISO weeks start with Monday and end with Sunday.
Week <code class="code">01</code> of a year is the first week which has the majority of its
days in that year; this is equivalent to the week containing the year&rsquo;s
first Thursday, and it is also equivalent to the week containing January
4.  Week <code class="code">01</code> of a year can contain days from the previous year.
The week before week <code class="code">01</code> of a year is the last week (<code class="code">52</code> or
<code class="code">53</code>) of the previous year even if it contains days from the new
year.
</p>
</dd>
<dt><code class="code">%w</code></dt>
<dd><p>The day of the week as a decimal number (range <code class="code">0</code> through
<code class="code">6</code>), Sunday being <code class="code">0</code>.
</p>
</dd>
<dt><code class="code">%W</code></dt>
<dd><p>The week number of the current year as a decimal number (range <code class="code">00</code>
through <code class="code">53</code>), starting with the first Monday as the first day of
the first week.  All days preceding the first Monday in the year are
considered to be in week <code class="code">00</code>.
</p>
</dd>
<dt><code class="code">%x</code></dt>
<dd><p>The preferred date representation for the current locale.
</p>
</dd>
<dt><code class="code">%X</code></dt>
<dd><p>The preferred time of day representation for the current locale.
</p>
</dd>
<dt><code class="code">%y</code></dt>
<dd><p>The year without a century as a decimal number (range <code class="code">00</code> through
<code class="code">99</code>).  This is equivalent to the year modulo 100.
</p>
<p>If the <code class="code">E</code> modifier is specified (<code class="code">%Ey</code>), instead produces
the year number according to a locale-specific alternative calendar.
Unlike <code class="code">%y</code>, the number is <em class="emph">not</em> reduced modulo 100.
However, by default it is zero-padded to a minimum of two digits (this
can be overridden by an explicit field width or by the <code class="code">_</code> and
<code class="code">-</code> flags).
</p>
</dd>
<dt><code class="code">%Y</code></dt>
<dd><p>The year as a decimal number, using the Gregorian calendar.  Years
before the year <code class="code">1</code> are numbered <code class="code">0</code>, <code class="code">-1</code>, and so on.
</p>
<p>If the <code class="code">E</code> modifier is specified (<code class="code">%EY</code>), instead produces a
complete representation of the year according to the locale&rsquo;s
alternative calendar.  Generally this will be some combination of the
information produced by <code class="code">%EC</code> and <code class="code">%Ey</code>.  As a GNU
extension, the formatting flags <code class="code">_</code> or <code class="code">-</code> may be used with
this conversion specifier; they affect how the year number is printed.
</p>
</dd>
<dt><code class="code">%z</code></dt>
<dd><p>RFC&nbsp;5322<!-- /@w -->/ISO&nbsp;8601<!-- /@w --> style numeric time zone (e.g.,
<code class="code">-0600</code> or <code class="code">+0100</code>), or nothing if no time zone is
determinable.
</p>
<p>In the POSIX locale, a full RFC&nbsp;5322<!-- /@w --> timestamp is generated by the format
<code class="t">&quot;%a,&nbsp;%d&nbsp;%b&nbsp;%Y&nbsp;%H:%M:%S&nbsp;%z&quot;</code><!-- /@w --> (or the equivalent
<code class="t">&quot;%a,&nbsp;%d&nbsp;%b&nbsp;%Y&nbsp;%T&nbsp;%z&quot;</code><!-- /@w -->).
</p>
</dd>
<dt><code class="code">%Z</code></dt>
<dd><p>The time zone abbreviation (empty if the time zone can&rsquo;t be determined).
</p>
</dd>
<dt><code class="code">%%</code></dt>
<dd><p>A literal &lsquo;<samp class="samp">%</samp>&rsquo; character.
</p></dd>
</dl>

<p>The <var class="var">size</var> parameter can be used to specify the maximum number of
characters to be stored in the array <var class="var">s</var>, including the terminating
null character.  If the formatted time requires more than <var class="var">size</var>
characters, <code class="code">strftime</code> returns zero and the contents of the array
<var class="var">s</var> are undefined.  Otherwise the return value indicates the
number of characters placed in the array <var class="var">s</var>, not including the
terminating null character.
</p>
<p><em class="emph">Warning:</em> This convention for the return value which is prescribed
in ISO&nbsp;C<!-- /@w --> can lead to problems in some situations.  For certain
format strings and certain locales the output really can be the empty
string and this cannot be discovered by testing the return value only.
E.g., in most locales the AM/PM time format is not supported (most of
the world uses the 24 hour time representation).  In such locales
<code class="t">&quot;%p&quot;</code> will return the empty string, i.e., the return value is
zero.  To detect situations like this something similar to the following
code should be used:
</p>
<div class="example smallexample">
<pre class="example-preformatted">buf[0] = '\1';
len = strftime (buf, bufsize, format, tp);
if (len == 0 &amp;&amp; buf[0] != '\0')
  {
    /* Something went wrong in the strftime call.  */
    ...
  }
</pre></div>

<p>If <var class="var">s</var> is a null pointer, <code class="code">strftime</code> does not actually write
anything, but instead returns the number of characters it would have written.
</p>
<p>Calling <code class="code">strftime</code> also sets the time zone state as if
<code class="code">tzset</code> were called.  See <a class="xref" href="Time-Zone-State.html">State Variables for Time Zones</a>.
</p>
<p>For an example of <code class="code">strftime</code>, see <a class="ref" href="Time-Functions-Example.html">Time Functions Example</a>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strftime_005fl"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">strftime_l</strong> <code class="def-code-arguments">(char *restrict <var class="var">s</var>, size_t <var class="var">size</var>, const char *restrict <var class="var">template</var>, const struct tm *<var class="var">brokentime</var>, locale_t <var class="var">locale</var>)</code><a class="copiable-link" href="#index-strftime_005fl"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env locale
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">strftime_l</code> function is equivalent to the <code class="code">strftime</code>
function except that it operates in <var class="var">locale</var> rather than in
the current locale.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcsftime"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">wcsftime</strong> <code class="def-code-arguments">(wchar_t *<var class="var">s</var>, size_t <var class="var">size</var>, const wchar_t *<var class="var">template</var>, const struct tm *<var class="var">brokentime</var>)</code><a class="copiable-link" href="#index-wcsftime"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env locale
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wcsftime</code> function is equivalent to the <code class="code">strftime</code>
function with the difference that it operates on wide character
strings.  The buffer where the result is stored, pointed to by <var class="var">s</var>,
must be an array of wide characters.  The parameter <var class="var">size</var> which
specifies the size of the output buffer gives the number of wide
characters, not the number of bytes.
</p>
<p>Also the format string <var class="var">template</var> is a wide character string.  Since
all characters needed to specify the format string are in the basic
character set it is portably possible to write format strings in the C
source code using the <code class="code">L&quot;&hellip;&quot;</code> notation.  The parameter
<var class="var">brokentime</var> has the same meaning as in the <code class="code">strftime</code> call.
</p>
<p>The <code class="code">wcsftime</code> function supports the same flags, modifiers, and
format specifiers as the <code class="code">strftime</code> function.
</p>
<p>The return value of <code class="code">wcsftime</code> is the number of wide characters
stored in <code class="code">s</code>.  When more characters would have to be written than
can be placed in the buffer <var class="var">s</var> the return value is zero, with the
same problems indicated in the <code class="code">strftime</code> documentation.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-asctime"><span class="category-def">Deprecated function: </span><span><code class="def-type">char *</code> <strong class="def-name">asctime</strong> <code class="def-code-arguments">(const struct tm *<var class="var">brokentime</var>)</code><a class="copiable-link" href="#index-asctime"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:asctime locale
| AS-Unsafe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">asctime</code> function converts the broken-down time value that
<var class="var">brokentime</var> points to into a string in a standard format:
</p>
<div class="example smallexample">
<pre class="example-preformatted">&quot;Tue May 21 13:46:22 1991\n&quot;
</pre></div>

<p>The abbreviations for the days of week are: &lsquo;<samp class="samp">Sun</samp>&rsquo;, &lsquo;<samp class="samp">Mon</samp>&rsquo;,
&lsquo;<samp class="samp">Tue</samp>&rsquo;, &lsquo;<samp class="samp">Wed</samp>&rsquo;, &lsquo;<samp class="samp">Thu</samp>&rsquo;, &lsquo;<samp class="samp">Fri</samp>&rsquo;, and &lsquo;<samp class="samp">Sat</samp>&rsquo;.
</p>
<p>The abbreviations for the months are: &lsquo;<samp class="samp">Jan</samp>&rsquo;, &lsquo;<samp class="samp">Feb</samp>&rsquo;,
&lsquo;<samp class="samp">Mar</samp>&rsquo;, &lsquo;<samp class="samp">Apr</samp>&rsquo;, &lsquo;<samp class="samp">May</samp>&rsquo;, &lsquo;<samp class="samp">Jun</samp>&rsquo;, &lsquo;<samp class="samp">Jul</samp>&rsquo;, &lsquo;<samp class="samp">Aug</samp>&rsquo;,
&lsquo;<samp class="samp">Sep</samp>&rsquo;, &lsquo;<samp class="samp">Oct</samp>&rsquo;, &lsquo;<samp class="samp">Nov</samp>&rsquo;, and &lsquo;<samp class="samp">Dec</samp>&rsquo;.
</p>
<p>Behavior is undefined if the calculated year would be less than 1000
or greater than 9999.
</p>
<p>The return value points to a statically allocated string, which might be
overwritten by subsequent calls to <code class="code">asctime</code> or <code class="code">ctime</code>.
(No other library function overwrites the contents of this
string.)
</p>
<p><strong class="strong">Portability note:</strong>
This obsolescent function is deprecated in C23.
Programs should instead use <code class="code">strftime</code> or even <code class="code">sprintf</code>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-asctime_005fr"><span class="category-def">Deprecated function: </span><span><code class="def-type">char *</code> <strong class="def-name">asctime_r</strong> <code class="def-code-arguments">(const struct tm *<var class="var">brokentime</var>, char *<var class="var">buffer</var>)</code><a class="copiable-link" href="#index-asctime_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">asctime</code> but instead of placing the
result in a static buffer it writes the string in the buffer pointed to
by the parameter <var class="var">buffer</var>.  This buffer should have room
for at least 26 bytes, including the terminating null.
Behavior is undefined if the calculated year would be less than 1000
or greater than 9999.
</p>
<p>If no error occurred the function returns a pointer to the string the
result was written into, i.e., it returns <var class="var">buffer</var>.  Otherwise
it returns <code class="code">NULL</code>.
</p>
<p><strong class="strong">Portability Note:</strong>
POSIX.1-2024 removed this obsolescent function.
Programs should instead use <code class="code">strftime</code> or even <code class="code">sprintf</code>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-ctime"><span class="category-def">Deprecated function: </span><span><code class="def-type">char *</code> <strong class="def-name">ctime</strong> <code class="def-code-arguments">(const time_t *<var class="var">time</var>)</code><a class="copiable-link" href="#index-ctime"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:tmbuf race:asctime env locale
| AS-Unsafe heap lock
| AC-Unsafe lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">ctime</code> function is similar to <code class="code">asctime</code>, except that you
specify the calendar time argument as a <code class="code">time_t</code> simple time value
rather than in broken-down local time format.  It is equivalent to
</p>
<div class="example smallexample">
<pre class="example-preformatted">asctime (localtime (<var class="var">time</var>))
</pre></div>

<p>Behavior is undefined if the calculated year would be less than 1000
or greater than 9999.
</p>
<p>Calling <code class="code">ctime</code> also sets the time zone state as if
<code class="code">tzset</code> were called.  See <a class="xref" href="Time-Zone-State.html">State Variables for Time Zones</a>.
</p>
<p><strong class="strong">Portability note:</strong>
This obsolescent function is deprecated in C23.
Programs should instead use <code class="code">strftime</code> or even <code class="code">sprintf</code>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-ctime_005fr"><span class="category-def">Deprecated function: </span><span><code class="def-type">char *</code> <strong class="def-name">ctime_r</strong> <code class="def-code-arguments">(const time_t *<var class="var">time</var>, char *<var class="var">buffer</var>)</code><a class="copiable-link" href="#index-ctime_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env locale
| AS-Unsafe heap lock
| AC-Unsafe lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">ctime</code>, but places the result in the
string pointed to by <var class="var">buffer</var>, and the time zone state is not
necessarily set as if <code class="code">tzset</code> were called.  It is equivalent to:
</p>
<div class="example smallexample">
<pre class="example-preformatted">asctime_r (localtime_r (<var class="var">time</var>, &amp;(struct tm) {0}), <var class="var">buffer</var>)
</pre></div>

<p>Behavior is undefined if the calculated year would be less than 1000
or greater than 9999.
</p>
<p>If no error occurred the function returns a pointer to the string the
result was written into, i.e., it returns <var class="var">buffer</var>.  Otherwise
it returns <code class="code">NULL</code>.
</p>
<p><strong class="strong">Portability Note:</strong>
POSIX.1-2024 removed this obsolescent function.
Programs should instead use <code class="code">strftime</code> or even <code class="code">sprintf</code>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Parsing-Date-and-Time.html">Convert textual time and date information back</a>, Previous: <a href="Broken_002ddown-Time.html">Broken-down Time</a>, Up: <a href="Calendar-Time.html">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
