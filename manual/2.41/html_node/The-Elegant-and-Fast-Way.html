<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>The Elegant and Fast Way (The GNU C Library)</title>

<meta name="description" content="The Elegant and Fast Way (The GNU C Library)">
<meta name="keywords" content="The Elegant and Fast Way (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Locale-Information.html" rel="up" title="Locale Information">
<link href="The-Lame-Way-to-Locale-Data.html" rel="prev" title="The Lame Way to Locale Data">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="The-Elegant-and-Fast-Way">
<div class="nav-panel">
<p>
Previous: <a href="The-Lame-Way-to-Locale-Data.html" accesskey="p" rel="prev"><code class="code">localeconv</code>: It is portable but &hellip;</a>, Up: <a href="Locale-Information.html" accesskey="u" rel="up">Accessing Locale Information</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Pinpoint-Access-to-Locale-Data"><span>7.7.2 Pinpoint Access to Locale Data<a class="copiable-link" href="#Pinpoint-Access-to-Locale-Data"> &para;</a></span></h4>

<p>When writing the X/Open Portability Guide the authors realized that the
<code class="code">localeconv</code> function is not enough to provide reasonable access to
locale information.  The information which was meant to be available
in the locale (as later specified in the POSIX.1 standard) requires more
ways to access it.  Therefore the <code class="code">nl_langinfo</code> function
was introduced.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-nl_005flanginfo"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">nl_langinfo</strong> <code class="def-code-arguments">(nl_item <var class="var">item</var>)</code><a class="copiable-link" href="#index-nl_005flanginfo"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">nl_langinfo</code> function can be used to access individual
elements of the locale categories.  Unlike the <code class="code">localeconv</code>
function, which returns all the information, <code class="code">nl_langinfo</code>
lets the caller select what information it requires.  This is very
fast and it is not a problem to call this function multiple times.
</p>
<p>A second advantage is that in addition to the numeric and monetary
formatting information, information from the
<code class="code">LC_TIME</code> and <code class="code">LC_MESSAGES</code> categories is available.
</p>
<a class="index-entry-id" id="index-langinfo_002eh"></a>
<p>The type <code class="code">nl_item</code> is defined in <samp class="file">nl_types.h</samp>.  The argument
<var class="var">item</var> is a numeric value defined in the header <samp class="file">langinfo.h</samp>.
The X/Open standard defines the following values:
</p>
<dl class="vtable">
<dt><a id="index-CODESET"></a><span><code class="code">CODESET</code><a class="copiable-link" href="#index-CODESET"> &para;</a></span></dt>
<dd><p><code class="code">nl_langinfo</code> returns a string with the name of the coded character
set used in the selected locale.
</p>
</dd>
<dt><a id="index-ABDAY_005f1"></a><span><code class="code">ABDAY_1</code><a class="copiable-link" href="#index-ABDAY_005f1"> &para;</a></span></dt>
<dt><a id="index-ABDAY_005f2"></a><span><code class="code">ABDAY_2</code><a class="copiable-link" href="#index-ABDAY_005f2"> &para;</a></span></dt>
<dt><a id="index-ABDAY_005f3"></a><span><code class="code">ABDAY_3</code><a class="copiable-link" href="#index-ABDAY_005f3"> &para;</a></span></dt>
<dt><a id="index-ABDAY_005f4"></a><span><code class="code">ABDAY_4</code><a class="copiable-link" href="#index-ABDAY_005f4"> &para;</a></span></dt>
<dt><a id="index-ABDAY_005f5"></a><span><code class="code">ABDAY_5</code><a class="copiable-link" href="#index-ABDAY_005f5"> &para;</a></span></dt>
<dt><a id="index-ABDAY_005f6"></a><span><code class="code">ABDAY_6</code><a class="copiable-link" href="#index-ABDAY_005f6"> &para;</a></span></dt>
<dt><a id="index-ABDAY_005f7"></a><span><code class="code">ABDAY_7</code><a class="copiable-link" href="#index-ABDAY_005f7"> &para;</a></span></dt>
<dd><p><code class="code">nl_langinfo</code> returns the abbreviated weekday name.  <code class="code">ABDAY_1</code>
corresponds to Sunday.
</p></dd>
<dt><a id="index-DAY_005f1"></a><span><code class="code">DAY_1</code><a class="copiable-link" href="#index-DAY_005f1"> &para;</a></span></dt>
<dt><a id="index-DAY_005f2"></a><span><code class="code">DAY_2</code><a class="copiable-link" href="#index-DAY_005f2"> &para;</a></span></dt>
<dt><a id="index-DAY_005f3"></a><span><code class="code">DAY_3</code><a class="copiable-link" href="#index-DAY_005f3"> &para;</a></span></dt>
<dt><a id="index-DAY_005f4"></a><span><code class="code">DAY_4</code><a class="copiable-link" href="#index-DAY_005f4"> &para;</a></span></dt>
<dt><a id="index-DAY_005f5"></a><span><code class="code">DAY_5</code><a class="copiable-link" href="#index-DAY_005f5"> &para;</a></span></dt>
<dt><a id="index-DAY_005f6"></a><span><code class="code">DAY_6</code><a class="copiable-link" href="#index-DAY_005f6"> &para;</a></span></dt>
<dt><a id="index-DAY_005f7"></a><span><code class="code">DAY_7</code><a class="copiable-link" href="#index-DAY_005f7"> &para;</a></span></dt>
<dd><p>Similar to <code class="code">ABDAY_1</code>, etc., but here the return value is the
unabbreviated weekday name.
</p></dd>
<dt><a id="index-ABMON_005f1"></a><span><code class="code">ABMON_1</code><a class="copiable-link" href="#index-ABMON_005f1"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f2"></a><span><code class="code">ABMON_2</code><a class="copiable-link" href="#index-ABMON_005f2"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f3"></a><span><code class="code">ABMON_3</code><a class="copiable-link" href="#index-ABMON_005f3"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f4"></a><span><code class="code">ABMON_4</code><a class="copiable-link" href="#index-ABMON_005f4"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f5"></a><span><code class="code">ABMON_5</code><a class="copiable-link" href="#index-ABMON_005f5"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f6"></a><span><code class="code">ABMON_6</code><a class="copiable-link" href="#index-ABMON_005f6"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f7"></a><span><code class="code">ABMON_7</code><a class="copiable-link" href="#index-ABMON_005f7"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f8"></a><span><code class="code">ABMON_8</code><a class="copiable-link" href="#index-ABMON_005f8"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f9"></a><span><code class="code">ABMON_9</code><a class="copiable-link" href="#index-ABMON_005f9"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f10"></a><span><code class="code">ABMON_10</code><a class="copiable-link" href="#index-ABMON_005f10"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f11"></a><span><code class="code">ABMON_11</code><a class="copiable-link" href="#index-ABMON_005f11"> &para;</a></span></dt>
<dt><a id="index-ABMON_005f12"></a><span><code class="code">ABMON_12</code><a class="copiable-link" href="#index-ABMON_005f12"> &para;</a></span></dt>
<dd><p>The return value is the abbreviated name of the month, in the
grammatical form used when the month forms part of a complete date.
<code class="code">ABMON_1</code> corresponds to January.
</p></dd>
<dt><a id="index-MON_005f1"></a><span><code class="code">MON_1</code><a class="copiable-link" href="#index-MON_005f1"> &para;</a></span></dt>
<dt><a id="index-MON_005f2"></a><span><code class="code">MON_2</code><a class="copiable-link" href="#index-MON_005f2"> &para;</a></span></dt>
<dt><a id="index-MON_005f3"></a><span><code class="code">MON_3</code><a class="copiable-link" href="#index-MON_005f3"> &para;</a></span></dt>
<dt><a id="index-MON_005f4"></a><span><code class="code">MON_4</code><a class="copiable-link" href="#index-MON_005f4"> &para;</a></span></dt>
<dt><a id="index-MON_005f5"></a><span><code class="code">MON_5</code><a class="copiable-link" href="#index-MON_005f5"> &para;</a></span></dt>
<dt><a id="index-MON_005f6"></a><span><code class="code">MON_6</code><a class="copiable-link" href="#index-MON_005f6"> &para;</a></span></dt>
<dt><a id="index-MON_005f7"></a><span><code class="code">MON_7</code><a class="copiable-link" href="#index-MON_005f7"> &para;</a></span></dt>
<dt><a id="index-MON_005f8"></a><span><code class="code">MON_8</code><a class="copiable-link" href="#index-MON_005f8"> &para;</a></span></dt>
<dt><a id="index-MON_005f9"></a><span><code class="code">MON_9</code><a class="copiable-link" href="#index-MON_005f9"> &para;</a></span></dt>
<dt><a id="index-MON_005f10"></a><span><code class="code">MON_10</code><a class="copiable-link" href="#index-MON_005f10"> &para;</a></span></dt>
<dt><a id="index-MON_005f11"></a><span><code class="code">MON_11</code><a class="copiable-link" href="#index-MON_005f11"> &para;</a></span></dt>
<dt><a id="index-MON_005f12"></a><span><code class="code">MON_12</code><a class="copiable-link" href="#index-MON_005f12"> &para;</a></span></dt>
<dd><p>Similar to <code class="code">ABMON_1</code>, etc., but here the month names are not
abbreviated.  Here the first value <code class="code">MON_1</code> also corresponds to
January.
</p></dd>
<dt><a id="index-ALTMON_005f1"></a><span><code class="code">ALTMON_1</code><a class="copiable-link" href="#index-ALTMON_005f1"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f2"></a><span><code class="code">ALTMON_2</code><a class="copiable-link" href="#index-ALTMON_005f2"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f3"></a><span><code class="code">ALTMON_3</code><a class="copiable-link" href="#index-ALTMON_005f3"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f4"></a><span><code class="code">ALTMON_4</code><a class="copiable-link" href="#index-ALTMON_005f4"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f5"></a><span><code class="code">ALTMON_5</code><a class="copiable-link" href="#index-ALTMON_005f5"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f6"></a><span><code class="code">ALTMON_6</code><a class="copiable-link" href="#index-ALTMON_005f6"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f7"></a><span><code class="code">ALTMON_7</code><a class="copiable-link" href="#index-ALTMON_005f7"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f8"></a><span><code class="code">ALTMON_8</code><a class="copiable-link" href="#index-ALTMON_005f8"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f9"></a><span><code class="code">ALTMON_9</code><a class="copiable-link" href="#index-ALTMON_005f9"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f10"></a><span><code class="code">ALTMON_10</code><a class="copiable-link" href="#index-ALTMON_005f10"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f11"></a><span><code class="code">ALTMON_11</code><a class="copiable-link" href="#index-ALTMON_005f11"> &para;</a></span></dt>
<dt><a id="index-ALTMON_005f12"></a><span><code class="code">ALTMON_12</code><a class="copiable-link" href="#index-ALTMON_005f12"> &para;</a></span></dt>
<dd><p>Similar to <code class="code">MON_1</code>, etc., but here the month names are in the
grammatical form used when the month is named by itself.  The
<code class="code">strftime</code> functions use these month names for the conversion
specifier <code class="code">%OB</code> (see <a class="pxref" href="Formatting-Calendar-Time.html">Formatting Calendar Time</a>).
</p>
<p>Note that not all languages need two different forms of the month names,
so the strings returned for <code class="code">MON_&hellip;</code> and <code class="code">ALTMON_&hellip;</code>
may or may not be the same, depending on the locale.
</p>
<p><strong class="strong">NB:</strong> <code class="code">ABALTMON_&hellip;</code> constants corresponding to the
<code class="code">%Ob</code> conversion specifier are not currently provided, but are
expected to be in a future release.  In the meantime, it is possible
to use <code class="code">_NL_ABALTMON_&hellip;</code>.
</p></dd>
<dt><a id="index-AM_005fSTR"></a><span><code class="code">AM_STR</code><a class="copiable-link" href="#index-AM_005fSTR"> &para;</a></span></dt>
<dt><a id="index-PM_005fSTR"></a><span><code class="code">PM_STR</code><a class="copiable-link" href="#index-PM_005fSTR"> &para;</a></span></dt>
<dd><p>The return values are strings which can be used in the representation of time
as an hour from 1 to 12 plus an am/pm specifier.
</p>
<p>Note that in locales which do not use this time representation
these strings might be empty, in which case the am/pm format
cannot be used at all.
</p></dd>
<dt><a id="index-D_005fT_005fFMT"></a><span><code class="code">D_T_FMT</code><a class="copiable-link" href="#index-D_005fT_005fFMT"> &para;</a></span></dt>
<dd><p>The return value can be used as a format string for <code class="code">strftime</code> to
represent time and date in a locale-specific way.
</p></dd>
<dt><a id="index-D_005fFMT"></a><span><code class="code">D_FMT</code><a class="copiable-link" href="#index-D_005fFMT"> &para;</a></span></dt>
<dd><p>The return value can be used as a format string for <code class="code">strftime</code> to
represent a date in a locale-specific way.
</p></dd>
<dt><a id="index-T_005fFMT"></a><span><code class="code">T_FMT</code><a class="copiable-link" href="#index-T_005fFMT"> &para;</a></span></dt>
<dd><p>The return value can be used as a format string for <code class="code">strftime</code> to
represent time in a locale-specific way.
</p></dd>
<dt><a id="index-T_005fFMT_005fAMPM"></a><span><code class="code">T_FMT_AMPM</code><a class="copiable-link" href="#index-T_005fFMT_005fAMPM"> &para;</a></span></dt>
<dd><p>The return value can be used as a format string for <code class="code">strftime</code> to
represent time in the am/pm format.
</p>
<p>Note that if the am/pm format does not make any sense for the
selected locale, the return value might be the same as the one for
<code class="code">T_FMT</code>.
</p></dd>
<dt><a id="index-ERA"></a><span><code class="code">ERA</code><a class="copiable-link" href="#index-ERA"> &para;</a></span></dt>
<dd><p>The return value represents the era used in the current locale.
</p>
<p>Most locales do not define this value.  An example of a locale which
does define this value is the Japanese one.  In Japan, the traditional
representation of dates includes the name of the era corresponding to
the then-emperor&rsquo;s reign.
</p>
<p>Normally it should not be necessary to use this value directly.
Specifying the <code class="code">E</code> modifier in their format strings causes the
<code class="code">strftime</code> functions to use this information.  The format of the
returned string is not specified, and therefore you should not assume
knowledge of it on different systems.
</p></dd>
<dt><a id="index-ERA_005fYEAR"></a><span><code class="code">ERA_YEAR</code><a class="copiable-link" href="#index-ERA_005fYEAR"> &para;</a></span></dt>
<dd><p>The return value gives the year in the relevant era of the locale.
As for <code class="code">ERA</code> it should not be necessary to use this value directly.
</p></dd>
<dt><a id="index-ERA_005fD_005fT_005fFMT"></a><span><code class="code">ERA_D_T_FMT</code><a class="copiable-link" href="#index-ERA_005fD_005fT_005fFMT"> &para;</a></span></dt>
<dd><p>This return value can be used as a format string for <code class="code">strftime</code> to
represent dates and times in a locale-specific era-based way.
</p></dd>
<dt><a id="index-ERA_005fD_005fFMT"></a><span><code class="code">ERA_D_FMT</code><a class="copiable-link" href="#index-ERA_005fD_005fFMT"> &para;</a></span></dt>
<dd><p>This return value can be used as a format string for <code class="code">strftime</code> to
represent a date in a locale-specific era-based way.
</p></dd>
<dt><a id="index-ERA_005fT_005fFMT"></a><span><code class="code">ERA_T_FMT</code><a class="copiable-link" href="#index-ERA_005fT_005fFMT"> &para;</a></span></dt>
<dd><p>This return value can be used as a format string for <code class="code">strftime</code> to
represent time in a locale-specific era-based way.
</p></dd>
<dt><a id="index-ALT_005fDIGITS"></a><span><code class="code">ALT_DIGITS</code><a class="copiable-link" href="#index-ALT_005fDIGITS"> &para;</a></span></dt>
<dd><p>The return value is a representation of up to <em class="math">100</em> values used to
represent the values <em class="math">0</em> to <em class="math">99</em>.  As for <code class="code">ERA</code> this
value is not intended to be used directly, but instead indirectly
through the <code class="code">strftime</code> function.  When the modifier <code class="code">O</code> is
used in a format which would otherwise use numerals to represent hours,
minutes, seconds, weekdays, months, or weeks, the appropriate value for
the locale is used instead.
</p></dd>
<dt><a id="index-INT_005fCURR_005fSYMBOL"></a><span><code class="code">INT_CURR_SYMBOL</code><a class="copiable-link" href="#index-INT_005fCURR_005fSYMBOL"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">int_curr_symbol</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-CURRENCY_005fSYMBOL"></a><span><code class="code">CURRENCY_SYMBOL</code><a class="copiable-link" href="#index-CURRENCY_005fSYMBOL"> &para;</a></span></dt>
<dt><a id="index-CRNCYSTR"></a><span><code class="code">CRNCYSTR</code><a class="copiable-link" href="#index-CRNCYSTR"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">currency_symbol</code> element of the <code class="code">struct lconv</code>.
</p>
<p><code class="code">CRNCYSTR</code> is a deprecated alias still required by Unix98.
</p></dd>
<dt><a id="index-MON_005fDECIMAL_005fPOINT"></a><span><code class="code">MON_DECIMAL_POINT</code><a class="copiable-link" href="#index-MON_005fDECIMAL_005fPOINT"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">mon_decimal_point</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-MON_005fTHOUSANDS_005fSEP"></a><span><code class="code">MON_THOUSANDS_SEP</code><a class="copiable-link" href="#index-MON_005fTHOUSANDS_005fSEP"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">mon_thousands_sep</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-MON_005fGROUPING"></a><span><code class="code">MON_GROUPING</code><a class="copiable-link" href="#index-MON_005fGROUPING"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">mon_grouping</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-POSITIVE_005fSIGN"></a><span><code class="code">POSITIVE_SIGN</code><a class="copiable-link" href="#index-POSITIVE_005fSIGN"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">positive_sign</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-NEGATIVE_005fSIGN"></a><span><code class="code">NEGATIVE_SIGN</code><a class="copiable-link" href="#index-NEGATIVE_005fSIGN"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">negative_sign</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-INT_005fFRAC_005fDIGITS"></a><span><code class="code">INT_FRAC_DIGITS</code><a class="copiable-link" href="#index-INT_005fFRAC_005fDIGITS"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">int_frac_digits</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-FRAC_005fDIGITS"></a><span><code class="code">FRAC_DIGITS</code><a class="copiable-link" href="#index-FRAC_005fDIGITS"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">frac_digits</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-P_005fCS_005fPRECEDES"></a><span><code class="code">P_CS_PRECEDES</code><a class="copiable-link" href="#index-P_005fCS_005fPRECEDES"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">p_cs_precedes</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-P_005fSEP_005fBY_005fSPACE"></a><span><code class="code">P_SEP_BY_SPACE</code><a class="copiable-link" href="#index-P_005fSEP_005fBY_005fSPACE"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">p_sep_by_space</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-N_005fCS_005fPRECEDES"></a><span><code class="code">N_CS_PRECEDES</code><a class="copiable-link" href="#index-N_005fCS_005fPRECEDES"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">n_cs_precedes</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-N_005fSEP_005fBY_005fSPACE"></a><span><code class="code">N_SEP_BY_SPACE</code><a class="copiable-link" href="#index-N_005fSEP_005fBY_005fSPACE"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">n_sep_by_space</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-P_005fSIGN_005fPOSN"></a><span><code class="code">P_SIGN_POSN</code><a class="copiable-link" href="#index-P_005fSIGN_005fPOSN"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">p_sign_posn</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-N_005fSIGN_005fPOSN"></a><span><code class="code">N_SIGN_POSN</code><a class="copiable-link" href="#index-N_005fSIGN_005fPOSN"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">n_sign_posn</code> element of the <code class="code">struct lconv</code>.
</p>
</dd>
<dt><a id="index-INT_005fP_005fCS_005fPRECEDES"></a><span><code class="code">INT_P_CS_PRECEDES</code><a class="copiable-link" href="#index-INT_005fP_005fCS_005fPRECEDES"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">int_p_cs_precedes</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-INT_005fP_005fSEP_005fBY_005fSPACE"></a><span><code class="code">INT_P_SEP_BY_SPACE</code><a class="copiable-link" href="#index-INT_005fP_005fSEP_005fBY_005fSPACE"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">int_p_sep_by_space</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-INT_005fN_005fCS_005fPRECEDES"></a><span><code class="code">INT_N_CS_PRECEDES</code><a class="copiable-link" href="#index-INT_005fN_005fCS_005fPRECEDES"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">int_n_cs_precedes</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-INT_005fN_005fSEP_005fBY_005fSPACE"></a><span><code class="code">INT_N_SEP_BY_SPACE</code><a class="copiable-link" href="#index-INT_005fN_005fSEP_005fBY_005fSPACE"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">int_n_sep_by_space</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-INT_005fP_005fSIGN_005fPOSN"></a><span><code class="code">INT_P_SIGN_POSN</code><a class="copiable-link" href="#index-INT_005fP_005fSIGN_005fPOSN"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">int_p_sign_posn</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-INT_005fN_005fSIGN_005fPOSN"></a><span><code class="code">INT_N_SIGN_POSN</code><a class="copiable-link" href="#index-INT_005fN_005fSIGN_005fPOSN"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">int_n_sign_posn</code> element of the <code class="code">struct lconv</code>.
</p>
</dd>
<dt><a id="index-DECIMAL_005fPOINT"></a><span><code class="code">DECIMAL_POINT</code><a class="copiable-link" href="#index-DECIMAL_005fPOINT"> &para;</a></span></dt>
<dt><a id="index-RADIXCHAR"></a><span><code class="code">RADIXCHAR</code><a class="copiable-link" href="#index-RADIXCHAR"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">decimal_point</code> element of the <code class="code">struct lconv</code>.
</p>
<p>The name <code class="code">RADIXCHAR</code> is a deprecated alias still used in Unix98.
</p></dd>
<dt><a id="index-THOUSANDS_005fSEP"></a><span><code class="code">THOUSANDS_SEP</code><a class="copiable-link" href="#index-THOUSANDS_005fSEP"> &para;</a></span></dt>
<dt><a id="index-THOUSEP"></a><span><code class="code">THOUSEP</code><a class="copiable-link" href="#index-THOUSEP"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">thousands_sep</code> element of the <code class="code">struct lconv</code>.
</p>
<p>The name <code class="code">THOUSEP</code> is a deprecated alias still used in Unix98.
</p></dd>
<dt><a id="index-GROUPING"></a><span><code class="code">GROUPING</code><a class="copiable-link" href="#index-GROUPING"> &para;</a></span></dt>
<dd><p>The same as the value returned by <code class="code">localeconv</code> in the
<code class="code">grouping</code> element of the <code class="code">struct lconv</code>.
</p></dd>
<dt><a id="index-YESEXPR"></a><span><code class="code">YESEXPR</code><a class="copiable-link" href="#index-YESEXPR"> &para;</a></span></dt>
<dd><p>The return value is a regular expression which can be used with the
<code class="code">regex</code> function to recognize a positive response to a yes/no
question.  The GNU C Library provides the <code class="code">rpmatch</code> function for
easier handling in applications.
</p></dd>
<dt><a id="index-NOEXPR"></a><span><code class="code">NOEXPR</code><a class="copiable-link" href="#index-NOEXPR"> &para;</a></span></dt>
<dd><p>The return value is a regular expression which can be used with the
<code class="code">regex</code> function to recognize a negative response to a yes/no
question.
</p></dd>
<dt><a id="index-YESSTR"></a><span><code class="code">YESSTR</code><a class="copiable-link" href="#index-YESSTR"> &para;</a></span></dt>
<dd><p>The return value is a locale-specific translation of the positive response
to a yes/no question.
</p>
<p>Using this value is deprecated since it is a very special case of
message translation, and is better handled by the message
translation functions (see <a class="pxref" href="Message-Translation.html">Message Translation</a>).
</p>
<p>The use of this symbol is deprecated.  Instead message translation
should be used.
</p></dd>
<dt><a id="index-NOSTR"></a><span><code class="code">NOSTR</code><a class="copiable-link" href="#index-NOSTR"> &para;</a></span></dt>
<dd><p>The return value is a locale-specific translation of the negative response
to a yes/no question.  What is said for <code class="code">YESSTR</code> is also true here.
</p>
<p>The use of this symbol is deprecated.  Instead message translation
should be used.
</p></dd>
</dl>

<p>The file <samp class="file">langinfo.h</samp> defines a lot more symbols but none of them
are official.  Using them is not portable, and the format of the
return values might change.  Therefore we recommended you not use
them.
</p>
<p>Note that the return value for any valid argument can be used
in all situations (with the possible exception of the am/pm time formatting
codes).  If the user has not selected any locale for the
appropriate category, <code class="code">nl_langinfo</code> returns the information from the
<code class="code">&quot;C&quot;</code> locale.  It is therefore possible to use this function as
shown in the example below.
</p>
<p>If the argument <var class="var">item</var> is not valid, a pointer to an empty string is
returned.
</p></dd></dl>

<p>An example of <code class="code">nl_langinfo</code> usage is a function which has to
print a given date and time in a locale-specific way.  At first one
might think that, since <code class="code">strftime</code> internally uses the locale
information, writing something like the following is enough:
</p>
<div class="example smallexample">
<pre class="example-preformatted">size_t
i18n_time_n_data (char *s, size_t len, const struct tm *tp)
{
  return strftime (s, len, &quot;%X %D&quot;, tp);
}
</pre></div>

<p>The format contains no weekday or month names and therefore is
internationally usable.  Wrong!  The output produced is something like
<code class="code">&quot;hh:mm:ss MM/DD/YY&quot;</code>.  This format is only recognizable in the
USA.  Other countries use different formats.  Therefore the function
should be rewritten like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">size_t
i18n_time_n_data (char *s, size_t len, const struct tm *tp)
{
  return strftime (s, len, nl_langinfo (D_T_FMT), tp);
}
</pre></div>

<p>Now it uses the date and time format of the locale
selected when the program runs.  If the user selects the locale
correctly there should never be a misunderstanding over the time and
date format.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="The-Lame-Way-to-Locale-Data.html"><code class="code">localeconv</code>: It is portable but &hellip;</a>, Up: <a href="Locale-Information.html">Accessing Locale Information</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
