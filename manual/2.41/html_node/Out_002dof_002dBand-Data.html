<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Out-of-Band Data (The GNU C Library)</title>

<meta name="description" content="Out-of-Band Data (The GNU C Library)">
<meta name="keywords" content="Out-of-Band Data (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Connections.html" rel="up" title="Connections">
<link href="Server-Example.html" rel="prev" title="Server Example">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Out_002dof_002dBand-Data">
<div class="nav-panel">
<p>
Previous: <a href="Server-Example.html" accesskey="p" rel="prev">Byte Stream Connection Server Example</a>, Up: <a href="Connections.html" accesskey="u" rel="up">Using Sockets with Connections</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Out_002dof_002dBand-Data-1"><span>16.9.8 Out-of-Band Data<a class="copiable-link" href="#Out_002dof_002dBand-Data-1"> &para;</a></span></h4>

<a class="index-entry-id" id="index-out_002dof_002dband-data"></a>
<a class="index-entry-id" id="index-high_002dpriority-data"></a>
<p>Streams with connections permit <em class="dfn">out-of-band</em> data that is
delivered with higher priority than ordinary data.  Typically the
reason for sending out-of-band data is to send notice of an
exceptional condition.  To send out-of-band data use
<code class="code">send</code>, specifying the flag <code class="code">MSG_OOB</code> (see <a class="pxref" href="Sending-Data.html">Sending Data</a>).
</p>
<p>Out-of-band data are received with higher priority because the
receiving process need not read it in sequence; to read the next
available out-of-band data, use <code class="code">recv</code> with the <code class="code">MSG_OOB</code>
flag (see <a class="pxref" href="Receiving-Data.html">Receiving Data</a>).  Ordinary read operations do not read
out-of-band data; they read only ordinary data.
</p>
<a class="index-entry-id" id="index-urgent-socket-condition"></a>
<p>When a socket finds that out-of-band data are on their way, it sends a
<code class="code">SIGURG</code> signal to the owner process or process group of the
socket.  You can specify the owner using the <code class="code">F_SETOWN</code> command
to the <code class="code">fcntl</code> function; see <a class="ref" href="Interrupt-Input.html">Interrupt-Driven Input</a>.  You must
also establish a handler for this signal, as described in <a class="ref" href="Signal-Handling.html">Signal Handling</a>, in order to take appropriate action such as reading the
out-of-band data.
</p>
<p>Alternatively, you can test for pending out-of-band data, or wait
until there is out-of-band data, using the <code class="code">select</code> function; it
can wait for an exceptional condition on the socket.  See <a class="xref" href="Waiting-for-I_002fO.html">Waiting for Input or Output</a>, for more information about <code class="code">select</code>.
</p>
<p>Notification of out-of-band data (whether with <code class="code">SIGURG</code> or with
<code class="code">select</code>) indicates that out-of-band data are on the way; the data
may not actually arrive until later.  If you try to read the
out-of-band data before it arrives, <code class="code">recv</code> fails with an
<code class="code">EWOULDBLOCK</code> error.
</p>
<p>Sending out-of-band data automatically places a &ldquo;mark&rdquo; in the stream
of ordinary data, showing where in the sequence the out-of-band data
&ldquo;would have been&rdquo;.  This is useful when the meaning of out-of-band
data is &ldquo;cancel everything sent so far&rdquo;.  Here is how you can test,
in the receiving process, whether any ordinary data was sent before
the mark:
</p>
<div class="example smallexample">
<pre class="example-preformatted">success = ioctl (socket, SIOCATMARK, &amp;atmark);
</pre></div>

<p>The <code class="code">integer</code> variable <var class="var">atmark</var> is set to a nonzero value if
the socket&rsquo;s read pointer has reached the &ldquo;mark&rdquo;.
</p>

<p>Here&rsquo;s a function to discard any ordinary data preceding the
out-of-band mark:
</p>
<div class="example smallexample">
<pre class="example-preformatted">int
discard_until_mark (int socket)
{
  while (1)
    {
      /* <span class="r">This is not an arbitrary limit; any size will do.</span>  */
      char buffer[1024];
      int atmark, success;

      /* <span class="r">If we have reached the mark, return.</span>  */
      success = ioctl (socket, SIOCATMARK, &amp;atmark);
      if (success &lt; 0)
        perror (&quot;ioctl&quot;);
      if (result)
        return;

      /* <span class="r">Otherwise, read a bunch of ordinary data and discard it.</span>
         <span class="r">This is guaranteed not to read past the mark</span>
         <span class="r">if it starts before the mark.</span>  */
      success = read (socket, buffer, sizeof buffer);
      if (success &lt; 0)
        perror (&quot;read&quot;);
    }
}
</pre></div>

<p>If you don&rsquo;t want to discard the ordinary data preceding the mark, you
may need to read some of it anyway, to make room in internal system
buffers for the out-of-band data.  If you try to read out-of-band data
and get an <code class="code">EWOULDBLOCK</code> error, try reading some ordinary data
(saving it so that you can use it when you want it) and see if that
makes room.  Here is an example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">struct buffer
{
  char *buf;
  int size;
  struct buffer *next;
};

/* <span class="r">Read the out-of-band data from SOCKET and return it</span>
   <span class="r">as a &lsquo;struct buffer&rsquo;, which records the address of the data</span>
   <span class="r">and its size.</span>

   <span class="r">It may be necessary to read some ordinary data</span>
   <span class="r">in order to make room for the out-of-band data.</span>
   <span class="r">If so, the ordinary data are saved as a chain of buffers</span>
   <span class="r">found in the &lsquo;next&rsquo; field of the value.</span>  */

struct buffer *
read_oob (int socket)
{
  struct buffer *tail = 0;
  struct buffer *list = 0;

  while (1)
    {
      /* <span class="r">This is an arbitrary limit.</span>
         <span class="r">Does anyone know how to do this without a limit?</span>  */
#define BUF_SZ 1024
      char *buf = (char *) xmalloc (BUF_SZ);
      int success;
      int atmark;

      /* <span class="r">Try again to read the out-of-band data.</span>  */
      success = recv (socket, buf, BUF_SZ, MSG_OOB);
      if (success &gt;= 0)
        {
          /* <span class="r">We got it, so return it.</span>  */
          struct buffer *link
            = (struct buffer *) xmalloc (sizeof (struct buffer));
          link-&gt;buf = buf;
          link-&gt;size = success;
          link-&gt;next = list;
          return link;
        }

      /* <span class="r">If we fail, see if we are at the mark.</span>  */
      success = ioctl (socket, SIOCATMARK, &amp;atmark);
      if (success &lt; 0)
        perror (&quot;ioctl&quot;);
      if (atmark)
        {
          /* <span class="r">At the mark; skipping past more ordinary data cannot help.</span>
             <span class="r">So just wait a while.</span>  */
          sleep (1);
          continue;
        }

      /* <span class="r">Otherwise, read a bunch of ordinary data and save it.</span>
         <span class="r">This is guaranteed not to read past the mark</span>
         <span class="r">if it starts before the mark.</span>  */
      success = read (socket, buf, BUF_SZ);
      if (success &lt; 0)
        perror (&quot;read&quot;);

      /* <span class="r">Save this data in the buffer list.</span>  */
      {
        struct buffer *link
          = (struct buffer *) xmalloc (sizeof (struct buffer));
        link-&gt;buf = buf;
        link-&gt;size = success;

        /* <span class="r">Add the new link to the end of the list.</span>  */
        if (tail)
          tail-&gt;next = link;
        else
          list = link;
        tail = link;
      }
    }
}
</pre></div>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Server-Example.html">Byte Stream Connection Server Example</a>, Up: <a href="Connections.html">Using Sockets with Connections</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
