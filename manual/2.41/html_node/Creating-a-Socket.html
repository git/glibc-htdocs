<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Creating a Socket (The GNU C Library)</title>

<meta name="description" content="Creating a Socket (The GNU C Library)">
<meta name="keywords" content="Creating a Socket (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Open_002fClose-Sockets.html" rel="up" title="Open/Close Sockets">
<link href="Closing-a-Socket.html" rel="next" title="Closing a Socket">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Creating-a-Socket">
<div class="nav-panel">
<p>
Next: <a href="Closing-a-Socket.html" accesskey="n" rel="next">Closing a Socket</a>, Up: <a href="Open_002fClose-Sockets.html" accesskey="u" rel="up">Opening and Closing Sockets</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Creating-a-Socket-1"><span>16.8.1 Creating a Socket<a class="copiable-link" href="#Creating-a-Socket-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-creating-a-socket"></a>
<a class="index-entry-id" id="index-socket_002c-creating"></a>
<a class="index-entry-id" id="index-opening-a-socket"></a>

<p>The primitive for creating a socket is the <code class="code">socket</code> function,
declared in <samp class="file">sys/socket.h</samp>.
<a class="index-entry-id" id="index-sys_002fsocket_002eh-6"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-socket-1"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">socket</strong> <code class="def-code-arguments">(int <var class="var">namespace</var>, int <var class="var">style</var>, int <var class="var">protocol</var>)</code><a class="copiable-link" href="#index-socket-1"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function creates a socket and specifies communication style
<var class="var">style</var>, which should be one of the socket styles listed in
<a class="ref" href="Communication-Styles.html">Communication Styles</a>.  The <var class="var">namespace</var> argument specifies
the namespace; it must be <code class="code">PF_LOCAL</code> (see <a class="pxref" href="Local-Namespace.html">The Local Namespace</a>) or
<code class="code">PF_INET</code> (see <a class="pxref" href="Internet-Namespace.html">The Internet Namespace</a>).  <var class="var">protocol</var>
designates the specific protocol (see <a class="pxref" href="Socket-Concepts.html">Socket Concepts</a>); zero is
usually right for <var class="var">protocol</var>.
</p>
<p>The return value from <code class="code">socket</code> is the file descriptor for the new
socket, or <code class="code">-1</code> in case of error.  The following <code class="code">errno</code> error
conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EPROTONOSUPPORT</code></dt>
<dd><p>The <var class="var">protocol</var> or <var class="var">style</var> is not supported by the
<var class="var">namespace</var> specified.
</p>
</dd>
<dt><code class="code">EMFILE</code></dt>
<dd><p>The process already has too many file descriptors open.
</p>
</dd>
<dt><code class="code">ENFILE</code></dt>
<dd><p>The system already has too many file descriptors open.
</p>
</dd>
<dt><code class="code">EACCES</code></dt>
<dd><p>The process does not have the privilege to create a socket of the specified
<var class="var">style</var> or <var class="var">protocol</var>.
</p>
</dd>
<dt><code class="code">ENOBUFS</code></dt>
<dd><p>The system ran out of internal buffer space.
</p></dd>
</dl>

<p>The file descriptor returned by the <code class="code">socket</code> function supports both
read and write operations.  However, like pipes, sockets do not support file
positioning operations.
</p></dd></dl>

<p>For examples of how to call the <code class="code">socket</code> function,
see <a class="ref" href="Local-Socket-Example.html">Example of Local-Namespace Sockets</a>, or <a class="ref" href="Inet-Example.html">Internet Socket Example</a>.
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Closing-a-Socket.html">Closing a Socket</a>, Up: <a href="Open_002fClose-Sockets.html">Opening and Closing Sockets</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
