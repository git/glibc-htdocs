<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>String Length (The GNU C Library)</title>

<meta name="description" content="String Length (The GNU C Library)">
<meta name="keywords" content="String Length (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="String-and-Array-Utilities.html" rel="up" title="String and Array Utilities">
<link href="Copying-Strings-and-Arrays.html" rel="next" title="Copying Strings and Arrays">
<link href="String_002fArray-Conventions.html" rel="prev" title="String/Array Conventions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="String-Length">
<div class="nav-panel">
<p>
Next: <a href="Copying-Strings-and-Arrays.html" accesskey="n" rel="next">Copying Strings and Arrays</a>, Previous: <a href="String_002fArray-Conventions.html" accesskey="p" rel="prev">String and Array Conventions</a>, Up: <a href="String-and-Array-Utilities.html" accesskey="u" rel="up">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="String-Length-1"><span>5.3 String Length<a class="copiable-link" href="#String-Length-1"> &para;</a></span></h3>

<p>You can get the length of a string using the <code class="code">strlen</code> function.
This function is declared in the header file <samp class="file">string.h</samp>.
<a class="index-entry-id" id="index-string_002eh-2"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strlen"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">strlen</strong> <code class="def-code-arguments">(const char *<var class="var">s</var>)</code><a class="copiable-link" href="#index-strlen"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">strlen</code> function returns the length of the
string <var class="var">s</var> in bytes.  (In other words, it returns the offset of the
terminating null byte within the array.)
</p>
<p>For example,
</p><div class="example smallexample">
<pre class="example-preformatted">strlen (&quot;hello, world&quot;)
    &rArr; 12
</pre></div>

<p>When applied to an array, the <code class="code">strlen</code> function returns
the length of the string stored there, not its allocated size.  You can
get the allocated size of the array that holds a string using
the <code class="code">sizeof</code> operator:
</p>
<div class="example smallexample">
<pre class="example-preformatted">char string[32] = &quot;hello, world&quot;;
sizeof (string)
    &rArr; 32
strlen (string)
    &rArr; 12
</pre></div>

<p>But beware, this will not work unless <var class="var">string</var> is the
array itself, not a pointer to it.  For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">char string[32] = &quot;hello, world&quot;;
char *ptr = string;
sizeof (string)
    &rArr; 32
sizeof (ptr)
    &rArr; 4  /* <span class="r">(on a machine with 4 byte pointers)</span> */
</pre></div>

<p>This is an easy mistake to make when you are working with functions that
take string arguments; those arguments are always pointers, not arrays.
</p>
<p>It must also be noted that for multibyte encoded strings the return
value does not have to correspond to the number of characters in the
string.  To get this value the string can be converted to wide
characters and <code class="code">wcslen</code> can be used or something like the following
code can be used:
</p>
<div class="example smallexample">
<pre class="example-preformatted">/* <span class="r">The input is in <code class="code">string</code>.</span>
   <span class="r">The length is expected in <code class="code">n</code>.</span>  */
{
  mbstate_t t;
  char *scopy = string;
  /* In initial state.  */
  memset (&amp;t, '\0', sizeof (t));
  /* Determine number of characters.  */
  n = mbsrtowcs (NULL, &amp;scopy, strlen (scopy), &amp;t);
}
</pre></div>

<p>This is cumbersome to do so if the number of characters (as opposed to
bytes) is needed often it is better to work with wide characters.
</p></dd></dl>

<p>The wide character equivalent is declared in <samp class="file">wchar.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcslen"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">wcslen</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">ws</var>)</code><a class="copiable-link" href="#index-wcslen"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wcslen</code> function is the wide character equivalent to
<code class="code">strlen</code>.  The return value is the number of wide characters in the
wide string pointed to by <var class="var">ws</var> (this is also the offset of
the terminating null wide character of <var class="var">ws</var>).
</p>
<p>Since there are no multi wide character sequences making up one wide
character the return value is not only the offset in the array, it is
also the number of wide characters.
</p>
<p>This function was introduced in Amendment&nbsp;1<!-- /@w --> to ISO&nbsp;C90<!-- /@w -->.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strnlen"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">strnlen</strong> <code class="def-code-arguments">(const char *<var class="var">s</var>, size_t <var class="var">maxlen</var>)</code><a class="copiable-link" href="#index-strnlen"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This returns the offset of the first null byte in the array <var class="var">s</var>,
except that it returns <var class="var">maxlen</var> if the first <var class="var">maxlen</var> bytes
are all non-null.
Therefore this function is equivalent to
<code class="code">(strlen (<var class="var">s</var>) &lt; <var class="var">maxlen</var> ? strlen (<var class="var">s</var>) : <var class="var">maxlen</var>)</code>
but it
is more efficient and works even if <var class="var">s</var> is not null-terminated so
long as <var class="var">maxlen</var> does not exceed the size of <var class="var">s</var>&rsquo;s array.
</p>
<div class="example smallexample">
<pre class="example-preformatted">char string[32] = &quot;hello, world&quot;;
strnlen (string, 32)
    &rArr; 12
strnlen (string, 5)
    &rArr; 5
</pre></div>

<p>This function is part of POSIX.1-2008 and later editions, but was
available in the GNU C Library and other systems as an extension long before
it was standardized.  It is declared in <samp class="file">string.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcsnlen"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">wcsnlen</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">ws</var>, size_t <var class="var">maxlen</var>)</code><a class="copiable-link" href="#index-wcsnlen"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">wcsnlen</code> is the wide character equivalent to <code class="code">strnlen</code>.  The
<var class="var">maxlen</var> parameter specifies the maximum number of wide characters.
</p>
<p>This function is part of POSIX.1-2008 and later editions, and is
declared in <samp class="file">wchar.h</samp>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Copying-Strings-and-Arrays.html">Copying Strings and Arrays</a>, Previous: <a href="String_002fArray-Conventions.html">String and Array Conventions</a>, Up: <a href="String-and-Array-Utilities.html">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
