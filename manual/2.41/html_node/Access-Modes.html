<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Access Modes (The GNU C Library)</title>

<meta name="description" content="Access Modes (The GNU C Library)">
<meta name="keywords" content="Access Modes (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-Status-Flags.html" rel="up" title="File Status Flags">
<link href="Open_002dtime-Flags.html" rel="next" title="Open-time Flags">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Access-Modes">
<div class="nav-panel">
<p>
Next: <a href="Open_002dtime-Flags.html" accesskey="n" rel="next">Open-time Flags</a>, Up: <a href="File-Status-Flags.html" accesskey="u" rel="up">File Status Flags</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="File-Access-Modes"><span>13.15.1 File Access Modes<a class="copiable-link" href="#File-Access-Modes"> &para;</a></span></h4>

<p>The file access mode allows a file descriptor to be used for reading,
writing, both, or neither.  The access mode is determined when the file
is opened, and never change.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fRDONLY"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_RDONLY</strong><a class="copiable-link" href="#index-O_005fRDONLY"> &para;</a></span></dt>
<dd>
<p>Open the file for read access.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fWRONLY"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_WRONLY</strong><a class="copiable-link" href="#index-O_005fWRONLY"> &para;</a></span></dt>
<dd>
<p>Open the file for write access.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fRDWR"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_RDWR</strong><a class="copiable-link" href="#index-O_005fRDWR"> &para;</a></span></dt>
<dd>
<p>Open the file for both reading and writing.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fPATH"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_PATH</strong><a class="copiable-link" href="#index-O_005fPATH"> &para;</a></span></dt>
<dd>
<p>Obtain a file descriptor for the file, but do not open the file for
reading or writing.  Permission checks for the file itself are skipped
when the file is opened (but permission to access the directory that
contains it is still needed), and permissions are checked when the
descriptor is used later on.
</p>
<p>For example, such descriptors can be used with the <code class="code">fexecve</code>
function (see <a class="pxref" href="Executing-a-File.html">Executing a File</a>).  Other applications involve the
&lsquo;<samp class="samp">*at</samp>&rsquo; function variants, along with the <code class="code">AT_EMPTY_PATH</code> flag.
See <a class="xref" href="Descriptor_002dRelative-Access.html">Descriptor-Relative Access</a>.
</p>
<p>This access mode is specific to Linux.  On GNU/Hurd systems, it is
possible to use <code class="code">O_EXEC</code> explicitly, or specify no access modes
at all (see below).
</p></dd></dl>

<p>The portable file access modes <code class="code">O_RDONLY</code>, <code class="code">O_WRONLY</code>, and
<code class="code">O_RDWR</code> may not correspond to individual bits.  To determine the
file access mode with <code class="code">fcntl</code>, you must extract the access mode
bits from the retrieved file status flags, using the <code class="code">O_ACCMODE</code>
mask.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fACCMODE"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_ACCMODE</strong><a class="copiable-link" href="#index-O_005fACCMODE"> &para;</a></span></dt>
<dd>

<p>This macro is a mask that can be bitwise-ANDed with the file status flag
value to recover the file access mode, assuming that a standard file
access mode is in use.
</p></dd></dl>

<p>If a non-standard file access mode is used (such as <code class="code">O_PATH</code> or
<code class="code">O_EXEC</code>), masking with <code class="code">O_ACCMODE</code> may give incorrect
results.  These non-standard access modes are identified by individual
bits and have to be checked directly (without masking with
<code class="code">O_ACCMODE</code> first).
</p>
<p>On GNU/Hurd systems (but not on other systems), <code class="code">O_RDONLY</code> and
<code class="code">O_WRONLY</code> are independent bits that can be bitwise-ORed together,
and it is valid for either bit to be set or clear.  This means that
<code class="code">O_RDWR</code> is the same as <code class="code">O_RDONLY|O_WRONLY</code>.  A file access
mode of zero is permissible; it allows no operations that do input or
output to the file, but does allow other operations such as
<code class="code">fchmod</code>.  On GNU/Hurd systems, since &ldquo;read-only&rdquo; or &ldquo;write-only&rdquo;
is a misnomer, <samp class="file">fcntl.h</samp> defines additional names for the file
access modes.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fREAD"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_READ</strong><a class="copiable-link" href="#index-O_005fREAD"> &para;</a></span></dt>
<dd>
<p>Open the file for reading.  Same as <code class="code">O_RDONLY</code>; only defined on GNU/Hurd.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fWRITE"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_WRITE</strong><a class="copiable-link" href="#index-O_005fWRITE"> &para;</a></span></dt>
<dd>
<p>Open the file for writing.  Same as <code class="code">O_WRONLY</code>; only defined on GNU/Hurd.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fEXEC"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_EXEC</strong><a class="copiable-link" href="#index-O_005fEXEC"> &para;</a></span></dt>
<dd>
<p>Open the file for executing.  Only defined on GNU/Hurd.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Open_002dtime-Flags.html">Open-time Flags</a>, Up: <a href="File-Status-Flags.html">File Status Flags</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
