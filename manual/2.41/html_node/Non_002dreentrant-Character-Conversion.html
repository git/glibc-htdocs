<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Non-reentrant Character Conversion (The GNU C Library)</title>

<meta name="description" content="Non-reentrant Character Conversion (The GNU C Library)">
<meta name="keywords" content="Non-reentrant Character Conversion (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Non_002dreentrant-Conversion.html" rel="up" title="Non-reentrant Conversion">
<link href="Non_002dreentrant-String-Conversion.html" rel="next" title="Non-reentrant String Conversion">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Non_002dreentrant-Character-Conversion">
<div class="nav-panel">
<p>
Next: <a href="Non_002dreentrant-String-Conversion.html" accesskey="n" rel="next">Non-reentrant Conversion of Strings</a>, Up: <a href="Non_002dreentrant-Conversion.html" accesskey="u" rel="up">Non-reentrant Conversion Function</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Non_002dreentrant-Conversion-of-Single-Characters"><span>6.4.1 Non-reentrant Conversion of Single Characters<a class="copiable-link" href="#Non_002dreentrant-Conversion-of-Single-Characters"> &para;</a></span></h4>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mbtowc"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">mbtowc</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">result</var>, const char *restrict <var class="var">string</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-mbtowc"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">mbtowc</code> (&ldquo;multibyte to wide character&rdquo;) function when called
with non-null <var class="var">string</var> converts the first multibyte character
beginning at <var class="var">string</var> to its corresponding wide character code.  It
stores the result in <code class="code">*<var class="var">result</var></code>.
</p>
<p><code class="code">mbtowc</code> never examines more than <var class="var">size</var> bytes.  (The idea is
to supply for <var class="var">size</var> the number of bytes of data you have in hand.)
</p>
<p><code class="code">mbtowc</code> with non-null <var class="var">string</var> distinguishes three
possibilities: the first <var class="var">size</var> bytes at <var class="var">string</var> start with
valid multibyte characters, they start with an invalid byte sequence or
just part of a character, or <var class="var">string</var> points to an empty string (a
null character).
</p>
<p>For a valid multibyte character, <code class="code">mbtowc</code> converts it to a wide
character and stores that in <code class="code">*<var class="var">result</var></code>, and returns the
number of bytes in that character (always at least <em class="math">1</em> and never
more than <var class="var">size</var>).
</p>
<p>For an invalid byte sequence, <code class="code">mbtowc</code> returns <em class="math">-1</em>.  For an
empty string, it returns <em class="math">0</em>, also storing <code class="code">'\0'</code> in
<code class="code">*<var class="var">result</var></code>.
</p>
<p>If the multibyte character code uses shift characters, then
<code class="code">mbtowc</code> maintains and updates a shift state as it scans.  If you
call <code class="code">mbtowc</code> with a null pointer for <var class="var">string</var>, that
initializes the shift state to its standard initial value.  It also
returns nonzero if the multibyte character code in use actually has a
shift state.  See <a class="xref" href="Shift-State.html">States in Non-reentrant Functions</a>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wctomb"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">wctomb</strong> <code class="def-code-arguments">(char *<var class="var">string</var>, wchar_t <var class="var">wchar</var>)</code><a class="copiable-link" href="#index-wctomb"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wctomb</code> (&ldquo;wide character to multibyte&rdquo;) function converts
the wide character code <var class="var">wchar</var> to its corresponding multibyte
character sequence, and stores the result in bytes starting at
<var class="var">string</var>.  At most <code class="code">MB_CUR_MAX</code> characters are stored.
</p>
<p><code class="code">wctomb</code> with non-null <var class="var">string</var> distinguishes three
possibilities for <var class="var">wchar</var>: a valid wide character code (one that can
be translated to a multibyte character), an invalid code, and
<code class="code">L'\0'</code>.
</p>
<p>Given a valid code, <code class="code">wctomb</code> converts it to a multibyte character,
storing the bytes starting at <var class="var">string</var>.  Then it returns the number
of bytes in that character (always at least <em class="math">1</em> and never more
than <code class="code">MB_CUR_MAX</code>).
</p>
<p>If <var class="var">wchar</var> is an invalid wide character code, <code class="code">wctomb</code> returns
<em class="math">-1</em>.  If <var class="var">wchar</var> is <code class="code">L'\0'</code>, it returns <code class="code">0</code>, also
storing <code class="code">'\0'</code> in <code class="code">*<var class="var">string</var></code>.
</p>
<p>If the multibyte character code uses shift characters, then
<code class="code">wctomb</code> maintains and updates a shift state as it scans.  If you
call <code class="code">wctomb</code> with a null pointer for <var class="var">string</var>, that
initializes the shift state to its standard initial value.  It also
returns nonzero if the multibyte character code in use actually has a
shift state.  See <a class="xref" href="Shift-State.html">States in Non-reentrant Functions</a>.
</p>
<p>Calling this function with a <var class="var">wchar</var> argument of zero when
<var class="var">string</var> is not null has the side-effect of reinitializing the
stored shift state <em class="emph">as well as</em> storing the multibyte character
<code class="code">'\0'</code> and returning <em class="math">0</em>.
</p></dd></dl>

<p>Similar to <code class="code">mbrlen</code> there is also a non-reentrant function that
computes the length of a multibyte character.  It can be defined in
terms of <code class="code">mbtowc</code>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mblen"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">mblen</strong> <code class="def-code-arguments">(const char *<var class="var">string</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-mblen"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">mblen</code> function with a non-null <var class="var">string</var> argument returns
the number of bytes that make up the multibyte character beginning at
<var class="var">string</var>, never examining more than <var class="var">size</var> bytes.  (The idea is
to supply for <var class="var">size</var> the number of bytes of data you have in hand.)
</p>
<p>The return value of <code class="code">mblen</code> distinguishes three possibilities: the
first <var class="var">size</var> bytes at <var class="var">string</var> start with valid multibyte
characters, they start with an invalid byte sequence or just part of a
character, or <var class="var">string</var> points to an empty string (a null character).
</p>
<p>For a valid multibyte character, <code class="code">mblen</code> returns the number of
bytes in that character (always at least <code class="code">1</code> and never more than
<var class="var">size</var>).  For an invalid byte sequence, <code class="code">mblen</code> returns
<em class="math">-1</em>.  For an empty string, it returns <em class="math">0</em>.
</p>
<p>If the multibyte character code uses shift characters, then <code class="code">mblen</code>
maintains and updates a shift state as it scans.  If you call
<code class="code">mblen</code> with a null pointer for <var class="var">string</var>, that initializes the
shift state to its standard initial value.  It also returns a nonzero
value if the multibyte character code in use actually has a shift state.
See <a class="xref" href="Shift-State.html">States in Non-reentrant Functions</a>.
</p>
<a class="index-entry-id" id="index-stdlib_002eh-7"></a>
<p>The function <code class="code">mblen</code> is declared in <samp class="file">stdlib.h</samp>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Non_002dreentrant-String-Conversion.html">Non-reentrant Conversion of Strings</a>, Up: <a href="Non_002dreentrant-Conversion.html">Non-reentrant Conversion Function</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
