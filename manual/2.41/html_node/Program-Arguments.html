<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Program Arguments (The GNU C Library)</title>

<meta name="description" content="Program Arguments (The GNU C Library)">
<meta name="keywords" content="Program Arguments (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Program-Basics.html" rel="up" title="Program Basics">
<link href="Environment-Variables.html" rel="next" title="Environment Variables">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Program-Arguments">
<div class="nav-panel">
<p>
Next: <a href="Environment-Variables.html" accesskey="n" rel="next">Environment Variables</a>, Up: <a href="Program-Basics.html" accesskey="u" rel="up">The Basic Program/System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Program-Arguments-1"><span>26.1 Program Arguments<a class="copiable-link" href="#Program-Arguments-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-program-arguments"></a>
<a class="index-entry-id" id="index-command-line-arguments"></a>
<a class="index-entry-id" id="index-arguments_002c-to-program"></a>

<a class="index-entry-id" id="index-program-startup"></a>
<a class="index-entry-id" id="index-startup-of-program"></a>
<a class="index-entry-id" id="index-invocation-of-program"></a>
<a class="index-entry-id" id="index-main-function"></a>
<a class="index-entry-id" id="index-main"></a>
<p>The system starts a C program by calling the function <code class="code">main</code>.  It
is up to you to write a function named <code class="code">main</code>&mdash;otherwise, you
won&rsquo;t even be able to link your program without errors.
</p>
<p>In ISO&nbsp;C<!-- /@w --> you can define <code class="code">main</code> either to take no arguments, or to
take two arguments that represent the command line arguments to the
program, like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">int main (int <var class="var">argc</var>, char *<var class="var">argv</var>[])
</pre></div>

<a class="index-entry-id" id="index-argc-_0028program-argument-count_0029"></a>
<a class="index-entry-id" id="index-argv-_0028program-argument-vector_0029"></a>
<p>The command line arguments are the whitespace-separated tokens given in
the shell command used to invoke the program; thus, in &lsquo;<samp class="samp">cat foo
bar</samp>&rsquo;, the arguments are &lsquo;<samp class="samp">foo</samp>&rsquo; and &lsquo;<samp class="samp">bar</samp>&rsquo;.  The only way a
program can look at its command line arguments is via the arguments of
<code class="code">main</code>.  If <code class="code">main</code> doesn&rsquo;t take arguments, then you cannot get
at the command line.
</p>
<p>The value of the <var class="var">argc</var> argument is the number of command line
arguments.  The <var class="var">argv</var> argument is a vector of C strings; its
elements are the individual command line argument strings.  The file
name of the program being run is also included in the vector as the
first element; the value of <var class="var">argc</var> counts this element.  A null
pointer always follows the last element: <code class="code"><var class="var">argv</var>[<var class="var">argc</var>]</code>
is this null pointer.
</p>
<p>For the command &lsquo;<samp class="samp">cat foo bar</samp>&rsquo;, <var class="var">argc</var> is 3 and <var class="var">argv</var> has
three elements, <code class="code">&quot;cat&quot;</code>, <code class="code">&quot;foo&quot;</code> and <code class="code">&quot;bar&quot;</code>.
</p>
<p>In Unix systems you can define <code class="code">main</code> a third way, using three arguments:
</p>
<div class="example smallexample">
<pre class="example-preformatted">int main (int <var class="var">argc</var>, char *<var class="var">argv</var>[], char *<var class="var">envp</var>[])
</pre></div>

<p>The first two arguments are just the same.  The third argument
<var class="var">envp</var> gives the program&rsquo;s environment; it is the same as the value
of <code class="code">environ</code>.  See <a class="xref" href="Environment-Variables.html">Environment Variables</a>.  POSIX.1 does not
allow this three-argument form, so to be portable it is best to write
<code class="code">main</code> to take two arguments, and use the value of <code class="code">environ</code>.
</p>

<ul class="mini-toc">
<li><a href="Argument-Syntax.html" accesskey="1">Program Argument Syntax Conventions</a></li>
<li><a href="Parsing-Program-Arguments.html" accesskey="2">Parsing Program Arguments</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Environment-Variables.html">Environment Variables</a>, Up: <a href="Program-Basics.html">The Basic Program/System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
