<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Mode Functions (The GNU C Library)</title>

<meta name="description" content="Mode Functions (The GNU C Library)">
<meta name="keywords" content="Mode Functions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Terminal-Modes.html" rel="up" title="Terminal Modes">
<link href="Setting-Modes.html" rel="next" title="Setting Modes">
<link href="Mode-Data-Types.html" rel="prev" title="Mode Data Types">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Mode-Functions">
<div class="nav-panel">
<p>
Next: <a href="Setting-Modes.html" accesskey="n" rel="next">Setting Terminal Modes Properly</a>, Previous: <a href="Mode-Data-Types.html" accesskey="p" rel="prev">Terminal Mode Data Types</a>, Up: <a href="Terminal-Modes.html" accesskey="u" rel="up">Terminal Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Terminal-Mode-Functions"><span>17.4.2 Terminal Mode Functions<a class="copiable-link" href="#Terminal-Mode-Functions"> &para;</a></span></h4>
<a class="index-entry-id" id="index-terminal-mode-functions"></a>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tcgetattr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">tcgetattr</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, struct termios *<var class="var">termios-p</var>)</code><a class="copiable-link" href="#index-tcgetattr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is used to examine the attributes of the terminal
device with file descriptor <var class="var">filedes</var>.  The attributes are returned
in the structure that <var class="var">termios-p</var> points to.
</p>
<p>If successful, <code class="code">tcgetattr</code> returns <em class="math">0</em>.  A return value of <em class="math">-1</em>
indicates an error.  The following <code class="code">errno</code> error conditions are
defined for this function:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is not a valid file descriptor.
</p>
</dd>
<dt><code class="code">ENOTTY</code></dt>
<dd><p>The <var class="var">filedes</var> is not associated with a terminal.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tcsetattr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">tcsetattr</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, int <var class="var">when</var>, const struct termios *<var class="var">termios-p</var>)</code><a class="copiable-link" href="#index-tcsetattr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function sets the attributes of the terminal device with file
descriptor <var class="var">filedes</var>.  The new attributes are taken from the
structure that <var class="var">termios-p</var> points to.
</p>
<p>The <var class="var">when</var> argument specifies how to deal with input and output
already queued.  It can be one of the following values:
</p>
<dl class="vtable">
<dt><a id="index-TCSANOW"></a><span><code class="code">TCSANOW</code><a class="copiable-link" href="#index-TCSANOW"> &para;</a></span></dt>
<dd>
<p>Make the change immediately.
</p>
</dd>
<dt><a id="index-TCSADRAIN"></a><span><code class="code">TCSADRAIN</code><a class="copiable-link" href="#index-TCSADRAIN"> &para;</a></span></dt>
<dd>
<p>Make the change after waiting until all queued output has been written.
You should usually use this option when changing parameters that affect
output.
</p>
</dd>
<dt><a id="index-TCSAFLUSH"></a><span><code class="code">TCSAFLUSH</code><a class="copiable-link" href="#index-TCSAFLUSH"> &para;</a></span></dt>
<dd>
<p>This is like <code class="code">TCSADRAIN</code>, but also discards any queued input.
</p>
</dd>
<dt><a id="index-TCSASOFT"></a><span><code class="code">TCSASOFT</code><a class="copiable-link" href="#index-TCSASOFT"> &para;</a></span></dt>
<dd>
<p>This is a flag bit that you can add to any of the above alternatives.
Its meaning is to inhibit alteration of the state of the terminal
hardware.  It is a BSD extension; it is only supported on BSD systems
and GNU/Hurd systems.
</p>
<p>Using <code class="code">TCSASOFT</code> is exactly the same as setting the <code class="code">CIGNORE</code>
bit in the <code class="code">c_cflag</code> member of the structure <var class="var">termios-p</var> points
to.  See <a class="xref" href="Control-Modes.html">Control Modes</a>, for a description of <code class="code">CIGNORE</code>.
</p></dd>
</dl>

<p>If this function is called from a background process on its controlling
terminal, normally all processes in the process group are sent a
<code class="code">SIGTTOU</code> signal, in the same way as if the process were trying to
write to the terminal.  The exception is if the calling process itself
is ignoring or blocking <code class="code">SIGTTOU</code> signals, in which case the
operation is performed and no signal is sent.  See <a class="xref" href="Job-Control.html">Job Control</a>.
</p>
<p>If successful, <code class="code">tcsetattr</code> returns <em class="math">0</em>.  A return value of
<em class="math">-1</em> indicates an error.  The following <code class="code">errno</code> error
conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is not a valid file descriptor.
</p>
</dd>
<dt><code class="code">ENOTTY</code></dt>
<dd><p>The <var class="var">filedes</var> is not associated with a terminal.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>Either the value of the <code class="code">when</code> argument is not valid, or there is
something wrong with the data in the <var class="var">termios-p</var> argument.
</p></dd>
</dl>
</dd></dl>

<p>Although <code class="code">tcgetattr</code> and <code class="code">tcsetattr</code> specify the terminal
device with a file descriptor, the attributes are those of the terminal
device itself and not of the file descriptor.  This means that the
effects of changing terminal attributes are persistent; if another
process opens the terminal file later on, it will see the changed
attributes even though it doesn&rsquo;t have anything to do with the open file
descriptor you originally specified in changing the attributes.
</p>
<p>Similarly, if a single process has multiple or duplicated file
descriptors for the same terminal device, changing the terminal
attributes affects input and output to all of these file
descriptors.  This means, for example, that you can&rsquo;t open one file
descriptor or stream to read from a terminal in the normal
line-buffered, echoed mode; and simultaneously have another file
descriptor for the same terminal that you use to read from it in
single-character, non-echoed mode.  Instead, you have to explicitly
switch the terminal back and forth between the two modes.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Setting-Modes.html">Setting Terminal Modes Properly</a>, Previous: <a href="Mode-Data-Types.html">Terminal Mode Data Types</a>, Up: <a href="Terminal-Modes.html">Terminal Modes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
