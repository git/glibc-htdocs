<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.41.

Copyright © 1993-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Example of Getopt (The GNU C Library)</title>

<meta name="description" content="Example of Getopt (The GNU C Library)">
<meta name="keywords" content="Example of Getopt (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Getopt.html" rel="up" title="Getopt">
<link href="Getopt-Long-Options.html" rel="next" title="Getopt Long Options">
<link href="Using-Getopt.html" rel="prev" title="Using Getopt">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Example-of-Getopt">
<div class="nav-panel">
<p>
Next: <a href="Getopt-Long-Options.html" accesskey="n" rel="next">Parsing Long Options with <code class="code">getopt_long</code></a>, Previous: <a href="Using-Getopt.html" accesskey="p" rel="prev">Using the <code class="code">getopt</code> function</a>, Up: <a href="Getopt.html" accesskey="u" rel="up">Parsing program options using <code class="code">getopt</code></a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Example-of-Parsing-Arguments-with-getopt"><span>26.2.2 Example of Parsing Arguments with <code class="code">getopt</code><a class="copiable-link" href="#Example-of-Parsing-Arguments-with-getopt"> &para;</a></span></h4>

<p>Here is an example showing how <code class="code">getopt</code> is typically used.  The
key points to notice are:
</p>
<ul class="itemize mark-bullet">
<li>Normally, <code class="code">getopt</code> is called in a loop.  When <code class="code">getopt</code> returns
<code class="code">-1</code>, indicating no more options are present, the loop terminates.

</li><li>A <code class="code">switch</code> statement is used to dispatch on the return value from
<code class="code">getopt</code>.  In typical use, each case just sets a variable that
is used later in the program.

</li><li>A second loop is used to process the remaining non-option arguments.
</li></ul>

<div class="example smallexample">
<pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">#include &lt;ctype.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;
#include &lt;unistd.h&gt;

int
main (int argc, char **argv)
{
  int aflag = 0;
  int bflag = 0;
  char *cvalue = NULL;
  int index;
  int c;

  opterr = 0;
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">  while ((c = getopt (argc, argv, &quot;abc:&quot;)) != -1)
    switch (c)
      {
      case 'a':
        aflag = 1;
        break;
      case 'b':
        bflag = 1;
        break;
      case 'c':
        cvalue = optarg;
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, &quot;Option -%c requires an argument.\n&quot;, optopt);
        else if (isprint (optopt))
          fprintf (stderr, &quot;Unknown option `-%c'.\n&quot;, optopt);
        else
          fprintf (stderr,
                   &quot;Unknown option character `\\x%x'.\n&quot;,
                   optopt);
        return 1;
      default:
        abort ();
      }
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">  printf (&quot;aflag = %d, bflag = %d, cvalue = %s\n&quot;,
          aflag, bflag, cvalue);

  for (index = optind; index &lt; argc; index++)
    printf (&quot;Non-option argument %s\n&quot;, argv[index]);
  return 0;
}
</pre></div></div>

<p>Here are some examples showing what this program prints with different
combinations of arguments:
</p>
<div class="example smallexample">
<pre class="example-preformatted">% testopt
aflag = 0, bflag = 0, cvalue = (null)

% testopt -a -b
aflag = 1, bflag = 1, cvalue = (null)

% testopt -ab
aflag = 1, bflag = 1, cvalue = (null)

% testopt -c foo
aflag = 0, bflag = 0, cvalue = foo

% testopt -cfoo
aflag = 0, bflag = 0, cvalue = foo

% testopt arg1
aflag = 0, bflag = 0, cvalue = (null)
Non-option argument arg1

% testopt -a arg1
aflag = 1, bflag = 0, cvalue = (null)
Non-option argument arg1

% testopt -c foo arg1
aflag = 0, bflag = 0, cvalue = foo
Non-option argument arg1

% testopt -a -- -b
aflag = 1, bflag = 0, cvalue = (null)
Non-option argument -b

% testopt -a -
aflag = 1, bflag = 0, cvalue = (null)
Non-option argument -
</pre></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Getopt-Long-Options.html">Parsing Long Options with <code class="code">getopt_long</code></a>, Previous: <a href="Using-Getopt.html">Using the <code class="code">getopt</code> function</a>, Up: <a href="Getopt.html">Parsing program options using <code class="code">getopt</code></a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
