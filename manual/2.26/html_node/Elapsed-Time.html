<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU C Library.

This is
The GNU C Library Reference Manual, for version
2.26.

Copyright (C) 1993-2017 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Elapsed Time (The GNU C Library)</title>

<meta name="description" content="Elapsed Time (The GNU C Library)">
<meta name="keywords" content="Elapsed Time (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Date-and-Time.html" rel="up" title="Date and Time">
<link href="Processor-And-CPU-Time.html" rel="next" title="Processor And CPU Time">
<link href="Time-Basics.html" rel="prev" title="Time Basics">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="section" id="Elapsed-Time">
<div class="header">
<p>
Next: <a href="Processor-And-CPU-Time.html" accesskey="n" rel="next">Processor And CPU Time</a>, Previous: <a href="Time-Basics.html" accesskey="p" rel="prev">Time Basics</a>, Up: <a href="Date-and-Time.html" accesskey="u" rel="up">Date and Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Elapsed-Time-1"></span><h3 class="section">21.2 Elapsed Time</h3>
<span id="index-elapsed-time-1"></span>

<p>One way to represent an elapsed time is with a simple arithmetic data
type, as with the following function to compute the elapsed time between
two calendar times.  This function is declared in <samp>time.h</samp>.
</p>
<dl class="def">
<dt id="index-difftime"><span class="category">Function: </span><span><em>double</em> <strong>difftime</strong> <em>(time_t <var>time1</var>, time_t <var>time0</var>)</em><a href='#index-difftime' class='copiable-anchor'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code>difftime</code> function returns the number of seconds of elapsed
time between calendar time <var>time1</var> and calendar time <var>time0</var>, as
a value of type <code>double</code>.  The difference ignores leap seconds
unless leap second support is enabled.
</p>
<p>In the GNU C Library, you can simply subtract <code>time_t</code> values.  But on
other systems, the <code>time_t</code> data type might use some other encoding
where subtraction doesn&rsquo;t work directly.
</p></dd></dl>

<p>The GNU C Library provides two data types specifically for representing
an elapsed time.  They are used by various GNU C Library functions, and
you can use them for your own purposes too.  They&rsquo;re exactly the same
except that one has a resolution in microseconds, and the other, newer
one, is in nanoseconds.
</p>
<dl class="def">
<dt id="index-struct-timeval"><span class="category">Data Type: </span><span><strong>struct timeval</strong><a href='#index-struct-timeval' class='copiable-anchor'> &para;</a></span></dt>
<dd>
<span id="index-timeval"></span>
<p>The <code>struct timeval</code> structure represents an elapsed time.  It is
declared in <samp>sys/time.h</samp> and has the following members:
</p>
<dl compact="compact">
<dt><span><code>time_t tv_sec</code></span></dt>
<dd><p>This represents the number of whole seconds of elapsed time.
</p>
</dd>
<dt><span><code>long int tv_usec</code></span></dt>
<dd><p>This is the rest of the elapsed time (a fraction of a second),
represented as the number of microseconds.  It is always less than one
million.
</p>
</dd>
</dl>
</dd></dl>

<dl class="def">
<dt id="index-struct-timespec"><span class="category">Data Type: </span><span><strong>struct timespec</strong><a href='#index-struct-timespec' class='copiable-anchor'> &para;</a></span></dt>
<dd>
<span id="index-timespec"></span>
<p>The <code>struct timespec</code> structure represents an elapsed time.  It is
declared in <samp>time.h</samp> and has the following members:
</p>
<dl compact="compact">
<dt><span><code>time_t tv_sec</code></span></dt>
<dd><p>This represents the number of whole seconds of elapsed time.
</p>
</dd>
<dt><span><code>long int tv_nsec</code></span></dt>
<dd><p>This is the rest of the elapsed time (a fraction of a second),
represented as the number of nanoseconds.  It is always less than one
billion.
</p>
</dd>
</dl>
</dd></dl>

<p>It is often necessary to subtract two values of type <code>struct&nbsp;timeval</code><!-- /@w --> or <code>struct&nbsp;timespec</code><!-- /@w -->.  Here is the best way to do
this.  It works even on some peculiar operating systems where the
<code>tv_sec</code> member has an unsigned type.
</p>
<div class="example">
<pre class="example">

/* <span class="roman">Subtract the &lsquo;struct timeval&rsquo; values X and Y,
   storing the result in RESULT.
   Return 1 if the difference is negative, otherwise 0.</span> */

int
timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y)
{
  /* <span class="roman">Perform the carry for the later subtraction by updating <var>y</var>.</span> */
  if (x-&gt;tv_usec &lt; y-&gt;tv_usec) {
    int nsec = (y-&gt;tv_usec - x-&gt;tv_usec) / 1000000 + 1;
    y-&gt;tv_usec -= 1000000 * nsec;
    y-&gt;tv_sec += nsec;
  }
  if (x-&gt;tv_usec - y-&gt;tv_usec &gt; 1000000) {
    int nsec = (x-&gt;tv_usec - y-&gt;tv_usec) / 1000000;
    y-&gt;tv_usec += 1000000 * nsec;
    y-&gt;tv_sec -= nsec;
  }

  /* <span class="roman">Compute the time remaining to wait.
     <code>tv_usec</code> is certainly positive.</span> */
  result-&gt;tv_sec = x-&gt;tv_sec - y-&gt;tv_sec;
  result-&gt;tv_usec = x-&gt;tv_usec - y-&gt;tv_usec;

  /* <span class="roman">Return 1 if result is negative.</span> */
  return x-&gt;tv_sec &lt; y-&gt;tv_sec;
}
</pre></div>

<p>Common functions that use <code>struct timeval</code> are <code>gettimeofday</code>
and <code>settimeofday</code>.
</p>

<p>There are no GNU C Library functions specifically oriented toward
dealing with elapsed times, but the calendar time, processor time, and
alarm and sleeping functions have a lot to do with them.
</p>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Processor-And-CPU-Time.html">Processor And CPU Time</a>, Previous: <a href="Time-Basics.html">Time Basics</a>, Up: <a href="Date-and-Time.html">Date and Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
