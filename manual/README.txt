After releasing glibc 2.36 we switched to putting the manual in versioned
directory names. This provides a way for commit messages to reference
specific versions of the manual. This also allows users to reference the
version of the manual that applies to their system.

For example the glibc 2.36 version of the manual is under:
	2.36/

The current directories under the top-level manual/ directory are:
	dvi/
	html_mono/
	html_node/
	info/
	pdf/
	texi/
	text/

These directories are copies of the latest released manual.

When updating the website:

 * Run ./update-glibc-manual.sh X.Y

 * Update the "latest" manual: rm latest; ln -s X.Y latest;

Where X.Y was the most recent version uploaded.

After each script is run you follow the commit instructions at the end of
the script.

The last thing to do is update index.html itself with the following steps:

 * Update the first <p> to use the new uploaded version.

 * In the <ul> with the list of manuals add the latest to the list and
   call it "X.Y (latest)", and remove "(latest)" from the version that is
   no longer the latests.  

 * Update the last updated date.

Commit the changes to index.html  and that completes the manual update.
