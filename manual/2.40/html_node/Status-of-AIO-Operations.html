<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Status of AIO Operations (The GNU C Library)</title>

<meta name="description" content="Status of AIO Operations (The GNU C Library)">
<meta name="keywords" content="Status of AIO Operations (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Asynchronous-I_002fO.html" rel="up" title="Asynchronous I/O">
<link href="Synchronizing-AIO-Operations.html" rel="next" title="Synchronizing AIO Operations">
<link href="Asynchronous-Reads_002fWrites.html" rel="prev" title="Asynchronous Reads/Writes">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Status-of-AIO-Operations">
<div class="nav-panel">
<p>
Next: <a href="Synchronizing-AIO-Operations.html" accesskey="n" rel="next">Getting into a Consistent State</a>, Previous: <a href="Asynchronous-Reads_002fWrites.html" accesskey="p" rel="prev">Asynchronous Read and Write Operations</a>, Up: <a href="Asynchronous-I_002fO.html" accesskey="u" rel="up">Perform I/O Operations in Parallel</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Getting-the-Status-of-AIO-Operations"><span>13.11.2 Getting the Status of AIO Operations<a class="copiable-link" href="#Getting-the-Status-of-AIO-Operations"> &para;</a></span></h4>

<p>As already described in the documentation of the functions in the last
section, it must be possible to get information about the status of an I/O
request.  When the operation is performed truly asynchronously (as with
<code class="code">aio_read</code> and <code class="code">aio_write</code> and with <code class="code">lio_listio</code> when the
mode is <code class="code">LIO_NOWAIT</code>), one sometimes needs to know whether a
specific request already terminated and if so, what the result was.
The following two functions allow you to get this kind of information.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-aio_005ferror"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">aio_error</strong> <code class="def-code-arguments">(const struct aiocb *<var class="var">aiocbp</var>)</code><a class="copiable-link" href="#index-aio_005ferror"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function determines the error state of the request described by the
<code class="code">struct aiocb</code> variable pointed to by <var class="var">aiocbp</var>.  If the
request has not yet terminated the value returned is always
<code class="code">EINPROGRESS</code>.  Once the request has terminated the value
<code class="code">aio_error</code> returns is either <em class="math">0</em> if the request completed
successfully or it returns the value which would be stored in the
<code class="code">errno</code> variable if the request would have been done using
<code class="code">read</code>, <code class="code">write</code>, or <code class="code">fsync</code>.
</p>
<p>The function can return <code class="code">ENOSYS</code> if it is not implemented.  It
could also return <code class="code">EINVAL</code> if the <var class="var">aiocbp</var> parameter does not
refer to an asynchronous operation whose return status is not yet known.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
function is in fact <code class="code">aio_error64</code> since the LFS interface
transparently replaces the normal implementation.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-aio_005ferror64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">aio_error64</strong> <code class="def-code-arguments">(const struct aiocb64 *<var class="var">aiocbp</var>)</code><a class="copiable-link" href="#index-aio_005ferror64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">aio_error</code> with the only difference
that the argument is a reference to a variable of type <code class="code">struct
aiocb64</code>.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
function is available under the name <code class="code">aio_error</code> and so
transparently replaces the interface for small files on 32 bit
machines.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-aio_005freturn"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">aio_return</strong> <code class="def-code-arguments">(struct aiocb *<var class="var">aiocbp</var>)</code><a class="copiable-link" href="#index-aio_005freturn"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function can be used to retrieve the return status of the operation
carried out by the request described in the variable pointed to by
<var class="var">aiocbp</var>.  As long as the error status of this request as returned
by <code class="code">aio_error</code> is <code class="code">EINPROGRESS</code> the return value of this function is
undefined.
</p>
<p>Once the request is finished this function can be used exactly once to
retrieve the return value.  Following calls might lead to undefined
behavior.  The return value itself is the value which would have been
returned by the <code class="code">read</code>, <code class="code">write</code>, or <code class="code">fsync</code> call.
</p>
<p>The function can return <code class="code">ENOSYS</code> if it is not implemented.  It
could also return <code class="code">EINVAL</code> if the <var class="var">aiocbp</var> parameter does not
refer to an asynchronous operation whose return status is not yet known.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
function is in fact <code class="code">aio_return64</code> since the LFS interface
transparently replaces the normal implementation.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-aio_005freturn64"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">aio_return64</strong> <code class="def-code-arguments">(struct aiocb64 *<var class="var">aiocbp</var>)</code><a class="copiable-link" href="#index-aio_005freturn64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">aio_return</code> with the only difference
that the argument is a reference to a variable of type <code class="code">struct
aiocb64</code>.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
function is available under the name <code class="code">aio_return</code> and so
transparently replaces the interface for small files on 32 bit
machines.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Synchronizing-AIO-Operations.html">Getting into a Consistent State</a>, Previous: <a href="Asynchronous-Reads_002fWrites.html">Asynchronous Read and Write Operations</a>, Up: <a href="Asynchronous-I_002fO.html">Perform I/O Operations in Parallel</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
