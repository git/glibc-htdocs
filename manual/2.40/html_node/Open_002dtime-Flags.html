<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Open-time Flags (The GNU C Library)</title>

<meta name="description" content="Open-time Flags (The GNU C Library)">
<meta name="keywords" content="Open-time Flags (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-Status-Flags.html" rel="up" title="File Status Flags">
<link href="Operating-Modes.html" rel="next" title="Operating Modes">
<link href="Access-Modes.html" rel="prev" title="Access Modes">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Open_002dtime-Flags">
<div class="nav-panel">
<p>
Next: <a href="Operating-Modes.html" accesskey="n" rel="next">I/O Operating Modes</a>, Previous: <a href="Access-Modes.html" accesskey="p" rel="prev">File Access Modes</a>, Up: <a href="File-Status-Flags.html" accesskey="u" rel="up">File Status Flags</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Open_002dtime-Flags-1"><span>13.15.2 Open-time Flags<a class="copiable-link" href="#Open_002dtime-Flags-1"> &para;</a></span></h4>

<p>The open-time flags specify options affecting how <code class="code">open</code> will behave.
These options are not preserved once the file is open.  The exception to
this is <code class="code">O_NONBLOCK</code>, which is also an I/O operating mode and so it
<em class="emph">is</em> saved.  See <a class="xref" href="Opening-and-Closing-Files.html">Opening and Closing Files</a>, for how to call
<code class="code">open</code>.
</p>
<p>There are two sorts of options specified by open-time flags.
</p>
<ul class="itemize mark-bullet">
<li><em class="dfn">File name translation flags</em> affect how <code class="code">open</code> looks up the
file name to locate the file, and whether the file can be created.
<a class="index-entry-id" id="index-file-name-translation-flags"></a>
<a class="index-entry-id" id="index-flags_002c-file-name-translation"></a>

</li><li><em class="dfn">Open-time action flags</em> specify extra operations that <code class="code">open</code> will
perform on the file once it is open.
<a class="index-entry-id" id="index-open_002dtime-action-flags"></a>
<a class="index-entry-id" id="index-flags_002c-open_002dtime-action"></a>
</li></ul>

<p>Here are the file name translation flags.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fCREAT"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_CREAT</strong><a class="copiable-link" href="#index-O_005fCREAT"> &para;</a></span></dt>
<dd>
<p>If set, the file will be created if it doesn&rsquo;t already exist.
<a class="index-entry-id" id="index-create-on-open-_0028file-status-flag_0029"></a>
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fEXCL"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_EXCL</strong><a class="copiable-link" href="#index-O_005fEXCL"> &para;</a></span></dt>
<dd>
<p>If both <code class="code">O_CREAT</code> and <code class="code">O_EXCL</code> are set, then <code class="code">open</code> fails
if the specified file already exists.  This is guaranteed to never
clobber an existing file.
</p>
<p>The <code class="code">O_EXCL</code> flag has a special meaning in combination with
<code class="code">O_TMPFILE</code>; see below.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fDIRECTORY"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_DIRECTORY</strong><a class="copiable-link" href="#index-O_005fDIRECTORY"> &para;</a></span></dt>
<dd>
<p>If set, the open operation fails if the given name is not the name of
a directory.  The <code class="code">errno</code> variable is set to <code class="code">ENOTDIR</code> for
this error condition.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fNOFOLLOW"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_NOFOLLOW</strong><a class="copiable-link" href="#index-O_005fNOFOLLOW"> &para;</a></span></dt>
<dd>
<p>If set, the open operation fails if the final component of the file name
refers to a symbolic link.  The <code class="code">errno</code> variable is set to
<code class="code">ELOOP</code> for this error condition.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fTMPFILE"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_TMPFILE</strong><a class="copiable-link" href="#index-O_005fTMPFILE"> &para;</a></span></dt>
<dd>
<p>If this flag is specified, functions in the <code class="code">open</code> family create an
unnamed temporary file.  In this case, the pathname argument to the
<code class="code">open</code> family of functions (see <a class="pxref" href="Opening-and-Closing-Files.html">Opening and Closing Files</a>) is
interpreted as the directory in which the temporary file is created
(thus determining the file system which provides the storage for the
file).  The <code class="code">O_TMPFILE</code> flag must be combined with <code class="code">O_WRONLY</code>
or <code class="code">O_RDWR</code>, and the <var class="var">mode</var> argument is required.
</p>
<p>The temporary file can later be given a name using <code class="code">linkat</code>,
turning it into a regular file.  This allows the atomic creation of a
file with the specific file attributes (mode and extended attributes)
and file contents.  If, for security reasons, it is not desirable that a
name can be given to the file, the <code class="code">O_EXCL</code> flag can be specified
along with <code class="code">O_TMPFILE</code>.
</p>
<p>Not all kernels support this open flag.  If this flag is unsupported, an
attempt to create an unnamed temporary file fails with an error of
<code class="code">EINVAL</code>.  If the underlying file system does not support the
<code class="code">O_TMPFILE</code> flag, an <code class="code">EOPNOTSUPP</code> error is the result.
</p>
<p>The <code class="code">O_TMPFILE</code> flag is a GNU extension.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fNONBLOCK"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_NONBLOCK</strong><a class="copiable-link" href="#index-O_005fNONBLOCK"> &para;</a></span></dt>
<dd>
<a class="index-entry-id" id="index-non_002dblocking-open"></a>
<p>This prevents <code class="code">open</code> from blocking for a &ldquo;long time&rdquo; to open the
file.  This is only meaningful for some kinds of files, usually devices
such as serial ports; when it is not meaningful, it is harmless and
ignored.  Often, opening a port to a modem blocks until the modem reports
carrier detection; if <code class="code">O_NONBLOCK</code> is specified, <code class="code">open</code> will
return immediately without a carrier.
</p>
<p>Note that the <code class="code">O_NONBLOCK</code> flag is overloaded as both an I/O operating
mode and a file name translation flag.  This means that specifying
<code class="code">O_NONBLOCK</code> in <code class="code">open</code> also sets nonblocking I/O mode;
see <a class="pxref" href="Operating-Modes.html">I/O Operating Modes</a>.  To open the file without blocking but do normal
I/O that blocks, you must call <code class="code">open</code> with <code class="code">O_NONBLOCK</code> set and
then call <code class="code">fcntl</code> to turn the bit off.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fNOCTTY"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_NOCTTY</strong><a class="copiable-link" href="#index-O_005fNOCTTY"> &para;</a></span></dt>
<dd>
<p>If the named file is a terminal device, don&rsquo;t make it the controlling
terminal for the process.  See <a class="xref" href="Job-Control.html">Job Control</a>, for information about
what it means to be the controlling terminal.
</p>
<p>On GNU/Hurd systems and 4.4 BSD, opening a file never makes it the
controlling terminal and <code class="code">O_NOCTTY</code> is zero.  However, GNU/Linux systems
and some other systems use a nonzero value for <code class="code">O_NOCTTY</code> and set the
controlling terminal when you open a file that is a terminal device; so
to be portable, use <code class="code">O_NOCTTY</code> when it is important to avoid this.
<a class="index-entry-id" id="index-controlling-terminal_002c-setting"></a>
</p></dd></dl>

<p>The following three file name translation flags exist only on
GNU/Hurd systems.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fIGNORE_005fCTTY"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_IGNORE_CTTY</strong><a class="copiable-link" href="#index-O_005fIGNORE_005fCTTY"> &para;</a></span></dt>
<dd>
<p>Do not recognize the named file as the controlling terminal, even if it
refers to the process&rsquo;s existing controlling terminal device.  Operations
on the new file descriptor will never induce job control signals.
See <a class="xref" href="Job-Control.html">Job Control</a>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fNOLINK"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_NOLINK</strong><a class="copiable-link" href="#index-O_005fNOLINK"> &para;</a></span></dt>
<dd>
<p>If the named file is a symbolic link, open the link itself instead of
the file it refers to.  (<code class="code">fstat</code> on the new file descriptor will
return the information returned by <code class="code">lstat</code> on the link&rsquo;s name.)
<a class="index-entry-id" id="index-symbolic-link_002c-opening"></a>
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fNOTRANS"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_NOTRANS</strong><a class="copiable-link" href="#index-O_005fNOTRANS"> &para;</a></span></dt>
<dd>
<p>If the named file is specially translated, do not invoke the translator.
Open the bare file the translator itself sees.
</p></dd></dl>


<p>The open-time action flags tell <code class="code">open</code> to do additional operations
which are not really related to opening the file.  The reason to do them
as part of <code class="code">open</code> instead of in separate calls is that <code class="code">open</code>
can do them <i class="i">atomically</i>.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fTRUNC"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_TRUNC</strong><a class="copiable-link" href="#index-O_005fTRUNC"> &para;</a></span></dt>
<dd>
<p>Truncate the file to zero length.  This option is only useful for
regular files, not special files such as directories or FIFOs.  POSIX.1
requires that you open the file for writing to use <code class="code">O_TRUNC</code>.  In
BSD and GNU you must have permission to write the file to truncate it,
but you need not open for write access.
</p>
<p>This is the only open-time action flag specified by POSIX.1.  There is
no good reason for truncation to be done by <code class="code">open</code>, instead of by
calling <code class="code">ftruncate</code> afterwards.  The <code class="code">O_TRUNC</code> flag existed in
Unix before <code class="code">ftruncate</code> was invented, and is retained for backward
compatibility.
</p></dd></dl>

<p>The remaining operating modes are BSD extensions.  They exist only
on some systems.  On other systems, these macros are not defined.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fSHLOCK"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_SHLOCK</strong><a class="copiable-link" href="#index-O_005fSHLOCK"> &para;</a></span></dt>
<dd>
<p>Acquire a shared lock on the file, as with <code class="code">flock</code>.
See <a class="xref" href="File-Locks.html">File Locks</a>.
</p>
<p>If <code class="code">O_CREAT</code> is specified, the locking is done atomically when
creating the file.  You are guaranteed that no other process will get
the lock on the new file first.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-O_005fEXLOCK"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">O_EXLOCK</strong><a class="copiable-link" href="#index-O_005fEXLOCK"> &para;</a></span></dt>
<dd>
<p>Acquire an exclusive lock on the file, as with <code class="code">flock</code>.
See <a class="xref" href="File-Locks.html">File Locks</a>.  This is atomic like <code class="code">O_SHLOCK</code>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Operating-Modes.html">I/O Operating Modes</a>, Previous: <a href="Access-Modes.html">File Access Modes</a>, Up: <a href="File-Status-Flags.html">File Status Flags</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
