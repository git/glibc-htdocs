<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Mount-Unmount-Remount (The GNU C Library)</title>

<meta name="description" content="Mount-Unmount-Remount (The GNU C Library)">
<meta name="keywords" content="Mount-Unmount-Remount (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Filesystem-Handling.html" rel="up" title="Filesystem Handling">
<link href="Mount-Information.html" rel="prev" title="Mount Information">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
kbd.kbd {font-style: oblique}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Mount_002dUnmount_002dRemount">
<div class="nav-panel">
<p>
Previous: <a href="Mount-Information.html" accesskey="p" rel="prev">Mount Information</a>, Up: <a href="Filesystem-Handling.html" accesskey="u" rel="up">Controlling and Querying Mounts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Mount_002c-Unmount_002c-Remount"><span>32.3.2 Mount, Unmount, Remount<a class="copiable-link" href="#Mount_002c-Unmount_002c-Remount"> &para;</a></span></h4>

<p>This section describes the functions for mounting, unmounting, and
remounting filesystems.
</p>
<p>Only the superuser can mount, unmount, or remount a filesystem.
</p>
<p>These functions do not access the <samp class="file">fstab</samp> and <samp class="file">mtab</samp> files.  You
should maintain and use these separately.  See <a class="xref" href="Mount-Information.html">Mount Information</a>.
</p>
<p>The symbols in this section are declared in <samp class="file">sys/mount.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mount"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">mount</strong> <code class="def-code-arguments">(const char *<var class="var">special_file</var>, const char *<var class="var">dir</var>, const char *<var class="var">fstype</var>, unsigned long int <var class="var">options</var>, const void *<var class="var">data</var>)</code><a class="copiable-link" href="#index-mount"> &para;</a></span></dt>
<dd>

<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p><code class="code">mount</code> mounts or remounts a filesystem.  The two operations are
quite different and are merged rather unnaturally into this one function.
The <code class="code">MS_REMOUNT</code> option, explained below, determines whether
<code class="code">mount</code> mounts or remounts.
</p>
<p>For a mount, the filesystem on the block device represented by the
device special file named <var class="var">special_file</var> gets mounted over the mount
point <var class="var">dir</var>.  This means that the directory <var class="var">dir</var> (along with any
files in it) is no longer visible; in its place (and still with the name
<var class="var">dir</var>) is the root directory of the filesystem on the device.
</p>
<p>As an exception, if the filesystem type (see below) is one which is not
based on a device (e.g. &ldquo;proc&rdquo;), <code class="code">mount</code> instantiates a
filesystem and mounts it over <var class="var">dir</var> and ignores <var class="var">special_file</var>.
</p>
<p>For a remount, <var class="var">dir</var> specifies the mount point where the filesystem
to be remounted is (and remains) mounted and <var class="var">special_file</var> is
ignored.  Remounting a filesystem means changing the options that control
operations on the filesystem while it is mounted.  It does not mean
unmounting and mounting again.
</p>
<p>For a mount, you must identify the type of the filesystem with
<var class="var">fstype</var>.  This type tells the kernel how to access the filesystem
and can be thought of as the name of a filesystem driver.  The
acceptable values are system dependent.  On a system with a Linux kernel
and the <code class="code">proc</code> filesystem, the list of possible values is in the
file <samp class="file">filesystems</samp> in the <code class="code">proc</code> filesystem (e.g. type
<kbd class="kbd">cat /proc/filesystems</kbd> to see the list).  With a Linux kernel, the
types of filesystems that <code class="code">mount</code> can mount, and their type names,
depends on what filesystem drivers are configured into the kernel or
loaded as loadable kernel modules.  An example of a common value for
<var class="var">fstype</var> is <code class="code">ext2</code>.
</p>
<p>For a remount, <code class="code">mount</code> ignores <var class="var">fstype</var>.
</p>
<p><var class="var">options</var> specifies a variety of options that apply until the
filesystem is unmounted or remounted.  The precise meaning of an option
depends on the filesystem and with some filesystems, an option may have
no effect at all.  Furthermore, for some filesystems, some of these
options (but never <code class="code">MS_RDONLY</code>) can be overridden for individual
file accesses via <code class="code">ioctl</code>.
</p>
<p><var class="var">options</var> is a bit string with bit fields defined using the
following mask and masked value macros:
</p>
<dl class="vtable">
<dt><a id="index-MS_005fMGC_005fMASK"></a><span><code class="code">MS_MGC_MASK</code><a class="copiable-link" href="#index-MS_005fMGC_005fMASK"> &para;</a></span></dt>
<dd><p>This multibit field contains a magic number.  If it does not have the value
<code class="code">MS_MGC_VAL</code>, <code class="code">mount</code> assumes all the following bits are zero and
the <var class="var">data</var> argument is a null string, regardless of their actual values.
</p>
</dd>
<dt><a id="index-MS_005fREMOUNT"></a><span><code class="code">MS_REMOUNT</code><a class="copiable-link" href="#index-MS_005fREMOUNT"> &para;</a></span></dt>
<dd><p>This bit on means to remount the filesystem.  Off means to mount it.
</p>
</dd>
<dt><a id="index-MS_005fRDONLY"></a><span><code class="code">MS_RDONLY</code><a class="copiable-link" href="#index-MS_005fRDONLY"> &para;</a></span></dt>
<dd><p>This bit on specifies that no writing to the filesystem shall be allowed
while it is mounted.  This cannot be overridden by <code class="code">ioctl</code>.  This
option is available on nearly all filesystems.
</p>
</dd>
<dt><a id="index-MS_005fNOSUID"></a><span><code class="code">MS_NOSUID</code><a class="copiable-link" href="#index-MS_005fNOSUID"> &para;</a></span></dt>
<dd><p>This bit on specifies that Setuid and Setgid permissions on files in the
filesystem shall be ignored while it is mounted.
</p>
</dd>
<dt><a id="index-MS_005fNOEXEC"></a><span><code class="code">MS_NOEXEC</code><a class="copiable-link" href="#index-MS_005fNOEXEC"> &para;</a></span></dt>
<dd><p>This bit on specifies that no files in the filesystem shall be executed
while the filesystem is mounted.
</p>
</dd>
<dt><a id="index-MS_005fNODEV"></a><span><code class="code">MS_NODEV</code><a class="copiable-link" href="#index-MS_005fNODEV"> &para;</a></span></dt>
<dd><p>This bit on specifies that no device special files in the filesystem
shall be accessible while the filesystem is mounted.
</p>
</dd>
<dt><a id="index-MS_005fSYNCHRONOUS"></a><span><code class="code">MS_SYNCHRONOUS</code><a class="copiable-link" href="#index-MS_005fSYNCHRONOUS"> &para;</a></span></dt>
<dd><p>This bit on specifies that all writes to the filesystem while it is
mounted shall be synchronous; i.e., data shall be synced before each
write completes rather than held in the buffer cache.
</p>
</dd>
<dt><a id="index-MS_005fMANDLOCK"></a><span><code class="code">MS_MANDLOCK</code><a class="copiable-link" href="#index-MS_005fMANDLOCK"> &para;</a></span></dt>
<dd><p>This bit on specifies that mandatory locks on files shall be permitted while
the filesystem is mounted.
</p>
</dd>
<dt><a id="index-MS_005fNOATIME"></a><span><code class="code">MS_NOATIME</code><a class="copiable-link" href="#index-MS_005fNOATIME"> &para;</a></span></dt>
<dd><p>This bit on specifies that access times of files shall not be updated when
the files are accessed while the filesystem is mounted.
</p>
</dd>
<dt><a id="index-MS_005fNODIRATIME"></a><span><code class="code">MS_NODIRATIME</code><a class="copiable-link" href="#index-MS_005fNODIRATIME"> &para;</a></span></dt>
<dd><p>This bit on specifies that access times of directories shall not be updated
when the directories are accessed while the filesystem in mounted.
</p>

</dd>
</dl>

<p>Any bits not covered by the above masks should be set off; otherwise,
results are undefined.
</p>
<p>The meaning of <var class="var">data</var> depends on the filesystem type and is controlled
entirely by the filesystem driver in the kernel.
</p>
<p>Example:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">#include &lt;sys/mount.h&gt;

mount(&quot;/dev/hdb&quot;, &quot;/cdrom&quot;, &quot;iso9660&quot;, MS_MGC_VAL | MS_RDONLY | MS_NOSUID, &quot;&quot;);

mount(&quot;/dev/hda2&quot;, &quot;/mnt&quot;, &quot;&quot;, MS_MGC_VAL | MS_REMOUNT, &quot;&quot;);

</pre></div></div>

<p>Appropriate arguments for <code class="code">mount</code> are conventionally recorded in
the <samp class="file">fstab</samp> table.  See <a class="xref" href="Mount-Information.html">Mount Information</a>.
</p>
<p>The return value is zero if the mount or remount is successful.  Otherwise,
it is <code class="code">-1</code> and <code class="code">errno</code> is set appropriately.  The values of
<code class="code">errno</code> are filesystem dependent, but here is a general list:
</p>
<dl class="table">
<dt><code class="code">EPERM</code></dt>
<dd><p>The process is not superuser.
</p></dd>
<dt><code class="code">ENODEV</code></dt>
<dd><p>The file system type <var class="var">fstype</var> is not known to the kernel.
</p></dd>
<dt><code class="code">ENOTBLK</code></dt>
<dd><p>The file <var class="var">dev</var> is not a block device special file.
</p></dd>
<dt><code class="code">EBUSY</code></dt>
<dd>
<ul class="itemize mark-bullet">
<li>The device is already mounted.

</li><li>The mount point is busy.  (E.g. it is some process&rsquo; working directory or
has a filesystem mounted on it already).

</li><li>The request is to remount read-only, but there are files open for writing.
</li></ul>

</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><ul class="itemize mark-bullet">
<li>A remount was attempted, but there is no filesystem mounted over the
specified mount point.

</li><li>The supposed filesystem has an invalid superblock.

</li></ul>

</dd>
<dt><code class="code">EACCES</code></dt>
<dd><ul class="itemize mark-bullet">
<li>The filesystem is inherently read-only (possibly due to a switch on the
device) and the process attempted to mount it read/write (by setting the
<code class="code">MS_RDONLY</code> bit off).

</li><li><var class="var">special_file</var> or <var class="var">dir</var> is not accessible due to file permissions.

</li><li><var class="var">special_file</var> is not accessible because it is in a filesystem that is
mounted with the <code class="code">MS_NODEV</code> option.

</li></ul>

</dd>
<dt><code class="code">EM_FILE</code></dt>
<dd><p>The table of dummy devices is full.  <code class="code">mount</code> needs to create a
dummy device (aka &ldquo;unnamed&rdquo; device) if the filesystem being mounted is
not one that uses a device.
</p>
</dd>
</dl>

</dd></dl>


<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-umount2"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">umount2</strong> <code class="def-code-arguments">(const char *<var class="var">file</var>, int <var class="var">flags</var>)</code><a class="copiable-link" href="#index-umount2"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p><code class="code">umount2</code> unmounts a filesystem.
</p>
<p>You can identify the filesystem to unmount either by the device special
file that contains the filesystem or by the mount point.  The effect is
the same.  Specify either as the string <var class="var">file</var>.
</p>
<p><var class="var">flags</var> contains the one-bit field identified by the following
mask macro:
</p>
<dl class="vtable">
<dt><a id="index-MNT_005fFORCE"></a><span><code class="code">MNT_FORCE</code><a class="copiable-link" href="#index-MNT_005fFORCE"> &para;</a></span></dt>
<dd><p>This bit on means to force the unmounting even if the filesystem is
busy, by making it unbusy first.  If the bit is off and the filesystem is
busy, <code class="code">umount2</code> fails with <code class="code">errno</code> = <code class="code">EBUSY</code>.  Depending
on the filesystem, this may override all, some, or no busy conditions.
</p>
</dd>
</dl>

<p>All other bits in <var class="var">flags</var> should be set to zero; otherwise, the result
is undefined.
</p>
<p>Example:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">#include &lt;sys/mount.h&gt;

umount2(&quot;/mnt&quot;, MNT_FORCE);

umount2(&quot;/dev/hdd1&quot;, 0);

</pre></div></div>

<p>After the filesystem is unmounted, the directory that was the mount point
is visible, as are any files in it.
</p>
<p>As part of unmounting, <code class="code">umount2</code> syncs the filesystem.
</p>
<p>If the unmounting is successful, the return value is zero.  Otherwise, it
is <code class="code">-1</code> and <code class="code">errno</code> is set accordingly:
</p>
<dl class="table">
<dt><code class="code">EPERM</code></dt>
<dd><p>The process is not superuser.
</p></dd>
<dt><code class="code">EBUSY</code></dt>
<dd><p>The filesystem cannot be unmounted because it is busy.  E.g. it contains
a directory that is some process&rsquo;s working directory or a file that some
process has open.  With some filesystems in some cases, you can avoid
this failure with the <code class="code">MNT_FORCE</code> option.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p><var class="var">file</var> validly refers to a file, but that file is neither a mount
point nor a device special file of a currently mounted filesystem.
</p>
</dd>
</dl>

<p>This function is not available on all systems.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-umount"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">umount</strong> <code class="def-code-arguments">(const char *<var class="var">file</var>)</code><a class="copiable-link" href="#index-umount"> &para;</a></span></dt>
<dd>

<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p><code class="code">umount</code> does the same thing as <code class="code">umount2</code> with <var class="var">flags</var> set
to zeroes.  It is more widely available than <code class="code">umount2</code> but since it
lacks the possibility to forcefully unmount a filesystem is deprecated
when <code class="code">umount2</code> is also available.
</p></dd></dl>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Mount-Information.html">Mount Information</a>, Up: <a href="Filesystem-Handling.html">Controlling and Querying Mounts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
