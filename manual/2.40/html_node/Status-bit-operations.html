<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Status bit operations (The GNU C Library)</title>

<meta name="description" content="Status bit operations (The GNU C Library)">
<meta name="keywords" content="Status bit operations (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Floating-Point-Errors.html" rel="up" title="Floating Point Errors">
<link href="Math-Error-Reporting.html" rel="next" title="Math Error Reporting">
<link href="Infinity-and-NaN.html" rel="prev" title="Infinity and NaN">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Status-bit-operations">
<div class="nav-panel">
<p>
Next: <a href="Math-Error-Reporting.html" accesskey="n" rel="next">Error Reporting by Mathematical Functions</a>, Previous: <a href="Infinity-and-NaN.html" accesskey="p" rel="prev">Infinity and NaN</a>, Up: <a href="Floating-Point-Errors.html" accesskey="u" rel="up">Errors in Floating-Point Calculations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Examining-the-FPU-status-word"><span>20.5.3 Examining the FPU status word<a class="copiable-link" href="#Examining-the-FPU-status-word"> &para;</a></span></h4>

<p>ISO&nbsp;C99<!-- /@w --> defines functions to query and manipulate the
floating-point status word.  You can use these functions to check for
untrapped exceptions when it&rsquo;s convenient, rather than worrying about
them in the middle of a calculation.
</p>
<p>These constants represent the various IEEE&nbsp;754<!-- /@w --> exceptions.  Not all
FPUs report all the different exceptions.  Each constant is defined if
and only if the FPU you are compiling for supports that exception, so
you can test for FPU support with &lsquo;<samp class="samp">#ifdef</samp>&rsquo;.  They are defined in
<samp class="file">fenv.h</samp>.
</p>
<dl class="vtable">
<dt><a id="index-FE_005fINEXACT"></a><span><code class="code">FE_INEXACT</code><a class="copiable-link" href="#index-FE_005fINEXACT"> &para;</a></span></dt>
<dd>
<p>The inexact exception.
</p></dd>
<dt><a id="index-FE_005fDIVBYZERO"></a><span><code class="code">FE_DIVBYZERO</code><a class="copiable-link" href="#index-FE_005fDIVBYZERO"> &para;</a></span></dt>
<dd>
<p>The divide by zero exception.
</p></dd>
<dt><a id="index-FE_005fUNDERFLOW"></a><span><code class="code">FE_UNDERFLOW</code><a class="copiable-link" href="#index-FE_005fUNDERFLOW"> &para;</a></span></dt>
<dd>
<p>The underflow exception.
</p></dd>
<dt><a id="index-FE_005fOVERFLOW"></a><span><code class="code">FE_OVERFLOW</code><a class="copiable-link" href="#index-FE_005fOVERFLOW"> &para;</a></span></dt>
<dd>
<p>The overflow exception.
</p></dd>
<dt><a id="index-FE_005fINVALID"></a><span><code class="code">FE_INVALID</code><a class="copiable-link" href="#index-FE_005fINVALID"> &para;</a></span></dt>
<dd>
<p>The invalid exception.
</p></dd>
</dl>

<p>The macro <code class="code">FE_ALL_EXCEPT</code> is the bitwise OR of all exception macros
which are supported by the FP implementation.
</p>
<p>These functions allow you to clear exception flags, test for exceptions,
and save and restore the set of exceptions flagged.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-feclearexcept"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">feclearexcept</strong> <code class="def-code-arguments">(int <var class="var">excepts</var>)</code><a class="copiable-link" href="#index-feclearexcept"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe !posix
| AC-Safe !posix
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function clears all of the supported exception flags indicated by
<var class="var">excepts</var>.
</p>
<p>The function returns zero in case the operation was successful, a
non-zero value otherwise.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-feraiseexcept"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">feraiseexcept</strong> <code class="def-code-arguments">(int <var class="var">excepts</var>)</code><a class="copiable-link" href="#index-feraiseexcept"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function raises the supported exceptions indicated by
<var class="var">excepts</var>.  If more than one exception bit in <var class="var">excepts</var> is set
the order in which the exceptions are raised is undefined except that
overflow (<code class="code">FE_OVERFLOW</code>) or underflow (<code class="code">FE_UNDERFLOW</code>) are
raised before inexact (<code class="code">FE_INEXACT</code>).  Whether for overflow or
underflow the inexact exception is also raised is also implementation
dependent.
</p>
<p>The function returns zero in case the operation was successful, a
non-zero value otherwise.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fesetexcept"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fesetexcept</strong> <code class="def-code-arguments">(int <var class="var">excepts</var>)</code><a class="copiable-link" href="#index-fesetexcept"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function sets the supported exception flags indicated by
<var class="var">excepts</var>, like <code class="code">feraiseexcept</code>, but without causing enabled
traps to be taken.  <code class="code">fesetexcept</code> is from TS 18661-1:2014.
</p>
<p>The function returns zero in case the operation was successful, a
non-zero value otherwise.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fetestexcept"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fetestexcept</strong> <code class="def-code-arguments">(int <var class="var">excepts</var>)</code><a class="copiable-link" href="#index-fetestexcept"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Test whether the exception flags indicated by the parameter <var class="var">except</var>
are currently set.  If any of them are, a nonzero value is returned
which specifies which exceptions are set.  Otherwise the result is zero.
</p></dd></dl>

<p>To understand these functions, imagine that the status word is an
integer variable named <var class="var">status</var>.  <code class="code">feclearexcept</code> is then
equivalent to &lsquo;<samp class="samp">status &amp;= ~excepts</samp>&rsquo; and <code class="code">fetestexcept</code> is
equivalent to &lsquo;<samp class="samp">(status &amp; excepts)</samp>&rsquo;.  The actual implementation may
be very different, of course.
</p>
<p>Exception flags are only cleared when the program explicitly requests it,
by calling <code class="code">feclearexcept</code>.  If you want to check for exceptions
from a set of calculations, you should clear all the flags first.  Here
is a simple example of the way to use <code class="code">fetestexcept</code>:
</p>
<div class="example smallexample">
<pre class="example-preformatted">{
  double f;
  int raised;
  feclearexcept (FE_ALL_EXCEPT);
  f = compute ();
  raised = fetestexcept (FE_OVERFLOW | FE_INVALID);
  if (raised &amp; FE_OVERFLOW) { /* ... */ }
  if (raised &amp; FE_INVALID) { /* ... */ }
  /* ... */
}
</pre></div>

<p>You cannot explicitly set bits in the status word.  You can, however,
save the entire status word and restore it later.  This is done with the
following functions:
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fegetexceptflag"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fegetexceptflag</strong> <code class="def-code-arguments">(fexcept_t *<var class="var">flagp</var>, int <var class="var">excepts</var>)</code><a class="copiable-link" href="#index-fegetexceptflag"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function stores in the variable pointed to by <var class="var">flagp</var> an
implementation-defined value representing the current setting of the
exception flags indicated by <var class="var">excepts</var>.
</p>
<p>The function returns zero in case the operation was successful, a
non-zero value otherwise.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fesetexceptflag"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fesetexceptflag</strong> <code class="def-code-arguments">(const fexcept_t *<var class="var">flagp</var>, int <var class="var">excepts</var>)</code><a class="copiable-link" href="#index-fesetexceptflag"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function restores the flags for the exceptions indicated by
<var class="var">excepts</var> to the values stored in the variable pointed to by
<var class="var">flagp</var>.
</p>
<p>The function returns zero in case the operation was successful, a
non-zero value otherwise.
</p></dd></dl>

<p>Note that the value stored in <code class="code">fexcept_t</code> bears no resemblance to
the bit mask returned by <code class="code">fetestexcept</code>.  The type may not even be
an integer.  Do not attempt to modify an <code class="code">fexcept_t</code> variable.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fetestexceptflag"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fetestexceptflag</strong> <code class="def-code-arguments">(const fexcept_t *<var class="var">flagp</var>, int <var class="var">excepts</var>)</code><a class="copiable-link" href="#index-fetestexceptflag"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Test whether the exception flags indicated by the parameter
<var class="var">excepts</var> are set in the variable pointed to by <var class="var">flagp</var>.  If
any of them are, a nonzero value is returned which specifies which
exceptions are set.  Otherwise the result is zero.
<code class="code">fetestexceptflag</code> is from TS 18661-1:2014.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Math-Error-Reporting.html">Error Reporting by Mathematical Functions</a>, Previous: <a href="Infinity-and-NaN.html">Infinity and NaN</a>, Up: <a href="Floating-Point-Errors.html">Errors in Floating-Point Calculations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
