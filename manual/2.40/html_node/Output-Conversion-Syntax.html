<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Output Conversion Syntax (The GNU C Library)</title>

<meta name="description" content="Output Conversion Syntax (The GNU C Library)">
<meta name="keywords" content="Output Conversion Syntax (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Output.html" rel="up" title="Formatted Output">
<link href="Table-of-Output-Conversions.html" rel="next" title="Table of Output Conversions">
<link href="Formatted-Output-Basics.html" rel="prev" title="Formatted Output Basics">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Output-Conversion-Syntax">
<div class="nav-panel">
<p>
Next: <a href="Table-of-Output-Conversions.html" accesskey="n" rel="next">Table of Output Conversions</a>, Previous: <a href="Formatted-Output-Basics.html" accesskey="p" rel="prev">Formatted Output Basics</a>, Up: <a href="Formatted-Output.html" accesskey="u" rel="up">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Output-Conversion-Syntax-1"><span>12.12.2 Output Conversion Syntax<a class="copiable-link" href="#Output-Conversion-Syntax-1"> &para;</a></span></h4>

<p>This section provides details about the precise syntax of conversion
specifications that can appear in a <code class="code">printf</code> template
string.
</p>
<p>Characters in the template string that are not part of a conversion
specification are printed as-is to the output stream.  Multibyte
character sequences (see <a class="pxref" href="Character-Set-Handling.html">Character Set Handling</a>) are permitted in a
template string.
</p>
<p>The conversion specifications in a <code class="code">printf</code> template string have
the general form:
</p>
<div class="example smallexample">
<pre class="example-preformatted">% <span class="r">[</span> <var class="var">param-no</var> <span class="r">$]</span> <var class="var">flags</var> <var class="var">width</var> <span class="r">[</span> . <var class="var">precision</var> <span class="r">]</span> <var class="var">type</var> <var class="var">conversion</var>
</pre></div>

<p>or
</p>
<div class="example smallexample">
<pre class="example-preformatted">% <span class="r">[</span> <var class="var">param-no</var> <span class="r">$]</span> <var class="var">flags</var> <var class="var">width</var> . <span class="r">*</span> <span class="r">[</span> <var class="var">param-no</var> <span class="r">$]</span> <var class="var">type</var> <var class="var">conversion</var>
</pre></div>

<p>For example, in the conversion specifier &lsquo;<samp class="samp">%-10.8ld</samp>&rsquo;, the &lsquo;<samp class="samp">-</samp>&rsquo;
is a flag, &lsquo;<samp class="samp">10</samp>&rsquo; specifies the field width, the precision is
&lsquo;<samp class="samp">8</samp>&rsquo;, the letter &lsquo;<samp class="samp">l</samp>&rsquo; is a type modifier, and &lsquo;<samp class="samp">d</samp>&rsquo; specifies
the conversion style.  (This particular type specifier says to
print a <code class="code">long int</code> argument in decimal notation, with a minimum of
8 digits left-justified in a field at least 10 characters wide.)
</p>
<p>In more detail, output conversion specifications consist of an
initial &lsquo;<samp class="samp">%</samp>&rsquo; character followed in sequence by:
</p>
<ul class="itemize mark-bullet">
<li>An optional specification of the parameter used for this format.
Normally the parameters to the <code class="code">printf</code> function are assigned to the
formats in the order of appearance in the format string.  But in some
situations (such as message translation) this is not desirable and this
extension allows an explicit parameter to be specified.

<p>The <var class="var">param-no</var> parts of the format must be integers in the range of
1 to the maximum number of arguments present to the function call.  Some
implementations limit this number to a certain upper bound.  The exact
limit can be retrieved by the following constant.
</p>
<dl class="first-defvr">
<dt class="defvr" id="index-NL_005fARGMAX"><span class="category-def">Macro: </span><span><strong class="def-name">NL_ARGMAX</strong><a class="copiable-link" href="#index-NL_005fARGMAX"> &para;</a></span></dt>
<dd><p>The value of <code class="code">NL_ARGMAX</code> is the maximum value allowed for the
specification of a positional parameter in a <code class="code">printf</code> call.  The
actual value in effect at runtime can be retrieved by using
<code class="code">sysconf</code> using the <code class="code">_SC_NL_ARGMAX</code> parameter see <a class="pxref" href="Sysconf-Definition.html">Definition of <code class="code">sysconf</code></a>.
</p>
<p>Some systems have a quite low limit such as <em class="math">9</em> for System&nbsp;V<!-- /@w -->
systems.  The GNU C Library has no real limit.
</p></dd></dl>

<p>If any of the formats has a specification for the parameter position all
of them in the format string shall have one.  Otherwise the behavior is
undefined.
</p>
</li><li>Zero or more <em class="dfn">flag characters</em> that modify the normal behavior of
the conversion specification.
<a class="index-entry-id" id="index-flag-character-_0028printf_0029"></a>

</li><li>An optional decimal integer specifying the <em class="dfn">minimum field width</em>.
If the normal conversion produces fewer characters than this, the field
is padded with spaces to the specified width.  This is a <em class="emph">minimum</em>
value; if the normal conversion produces more characters than this, the
field is <em class="emph">not</em> truncated.  Normally, the output is right-justified
within the field.
<a class="index-entry-id" id="index-minimum-field-width-_0028printf_0029"></a>

<p>You can also specify a field width of &lsquo;<samp class="samp">*</samp>&rsquo;.  This means that the
next argument in the argument list (before the actual value to be
printed) is used as the field width.  The value must be an <code class="code">int</code>.
If the value is negative, this means to set the &lsquo;<samp class="samp">-</samp>&rsquo; flag (see
below) and to use the absolute value as the field width.
</p>
</li><li>An optional <em class="dfn">precision</em> to specify the number of digits to be
written for the numeric conversions.  If the precision is specified, it
consists of a period (&lsquo;<samp class="samp">.</samp>&rsquo;) followed optionally by a decimal integer
(which defaults to zero if omitted).
<a class="index-entry-id" id="index-precision-_0028printf_0029"></a>

<p>You can also specify a precision of &lsquo;<samp class="samp">*</samp>&rsquo;.  This means that the next
argument in the argument list (before the actual value to be printed) is
used as the precision.  The value must be an <code class="code">int</code>, and is ignored
if it is negative.  If you specify &lsquo;<samp class="samp">*</samp>&rsquo; for both the field width and
precision, the field width argument precedes the precision argument.
Other C library versions may not recognize this syntax.
</p>
</li><li>An optional <em class="dfn">type modifier character</em>, which is used to specify the
data type of the corresponding argument if it differs from the default
type.  (For example, the integer conversions assume a type of <code class="code">int</code>,
but you can specify &lsquo;<samp class="samp">h</samp>&rsquo;, &lsquo;<samp class="samp">l</samp>&rsquo;, or &lsquo;<samp class="samp">L</samp>&rsquo; for other integer
types.)
<a class="index-entry-id" id="index-type-modifier-character-_0028printf_0029"></a>

</li><li>A character that specifies the conversion to be applied.
</li></ul>

<p>The exact options that are permitted and how they are interpreted vary
between the different conversion specifiers.  See the descriptions of the
individual conversions for information about the particular options that
they use.
</p>
<p>With the &lsquo;<samp class="samp">-Wformat</samp>&rsquo; option, the GNU C compiler checks calls to
<code class="code">printf</code> and related functions.  It examines the format string and
verifies that the correct number and types of arguments are supplied.
There is also a GNU C syntax to tell the compiler that a function you
write uses a <code class="code">printf</code>-style format string.
See <a data-manual="gcc" href="https://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html#Function-Attributes">Declaring Attributes of Functions</a> in <cite class="cite">Using GNU CC</cite>, for more information.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Table-of-Output-Conversions.html">Table of Output Conversions</a>, Previous: <a href="Formatted-Output-Basics.html">Formatted Output Basics</a>, Up: <a href="Formatted-Output.html">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
