<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>System Options (The GNU C Library)</title>

<meta name="description" content="System Options (The GNU C Library)">
<meta name="keywords" content="System Options (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="System-Configuration.html" rel="up" title="System Configuration">
<link href="Version-Supported.html" rel="next" title="Version Supported">
<link href="General-Limits.html" rel="prev" title="General Limits">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="System-Options">
<div class="nav-panel">
<p>
Next: <a href="Version-Supported.html" accesskey="n" rel="next">Which Version of POSIX is Supported</a>, Previous: <a href="General-Limits.html" accesskey="p" rel="prev">General Capacity Limits</a>, Up: <a href="System-Configuration.html" accesskey="u" rel="up">System Configuration Parameters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Overall-System-Options"><span>33.2 Overall System Options<a class="copiable-link" href="#Overall-System-Options"> &para;</a></span></h3>
<a class="index-entry-id" id="index-POSIX-optional-features"></a>
<a class="index-entry-id" id="index-optional-POSIX-features"></a>

<p>POSIX defines certain system-specific options that not all POSIX systems
support.  Since these options are provided in the kernel, not in the
library, simply using the GNU C Library does not guarantee any of these
features are supported; it depends on the system you are using.
</p>
<a class="index-entry-id" id="index-unistd_002eh-27"></a>
<p>You can test for the availability of a given option using the macros in
this section, together with the function <code class="code">sysconf</code>.  The macros are
defined only if you include <samp class="file">unistd.h</samp>.
</p>
<p>For the following macros, if the macro is defined in <samp class="file">unistd.h</samp>,
then the option is supported.  Otherwise, the option may or may not be
supported; use <code class="code">sysconf</code> to find out.  See <a class="xref" href="Sysconf.html">Using <code class="code">sysconf</code></a>.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPOSIX_005fJOB_005fCONTROL-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">_POSIX_JOB_CONTROL</strong><a class="copiable-link" href="#index-_005fPOSIX_005fJOB_005fCONTROL-1"> &para;</a></span></dt>
<dd>
<p>If this symbol is defined, it indicates that the system supports job
control.  Otherwise, the implementation behaves as if all processes
within a session belong to a single process group.  See <a class="xref" href="Job-Control.html">Job Control</a>.
Systems conforming to the 2001 revision of POSIX, or newer, will
always define this symbol.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPOSIX_005fSAVED_005fIDS-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">_POSIX_SAVED_IDS</strong><a class="copiable-link" href="#index-_005fPOSIX_005fSAVED_005fIDS-1"> &para;</a></span></dt>
<dd>
<p>If this symbol is defined, it indicates that the system remembers the
effective user and group IDs of a process before it executes an
executable file with the set-user-ID or set-group-ID bits set, and that
explicitly changing the effective user or group IDs back to these values
is permitted.  If this option is not defined, then if a nonprivileged
process changes its effective user or group ID to the real user or group
ID of the process, it can&rsquo;t change it back again.  See <a class="xref" href="Enable_002fDisable-Setuid.html">Enabling and Disabling Setuid Access</a>.
</p></dd></dl>

<p>For the following macros, if the macro is defined in <samp class="file">unistd.h</samp>,
then its value indicates whether the option is supported.  A value of
<code class="code">-1</code> means no, and any other value means yes.  If the macro is not
defined, then the option may or may not be supported; use <code class="code">sysconf</code>
to find out.  See <a class="xref" href="Sysconf.html">Using <code class="code">sysconf</code></a>.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPOSIX2_005fC_005fDEV"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">_POSIX2_C_DEV</strong><a class="copiable-link" href="#index-_005fPOSIX2_005fC_005fDEV"> &para;</a></span></dt>
<dd>
<p>If this symbol is defined, it indicates that the system has the POSIX.2
C compiler command, <code class="code">c89</code>.  The GNU C Library always defines this
as <code class="code">1</code>, on the assumption that you would not have installed it if
you didn&rsquo;t have a C compiler.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPOSIX2_005fFORT_005fDEV"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">_POSIX2_FORT_DEV</strong><a class="copiable-link" href="#index-_005fPOSIX2_005fFORT_005fDEV"> &para;</a></span></dt>
<dd>
<p>If this symbol is defined, it indicates that the system has the POSIX.2
Fortran compiler command, <code class="code">fort77</code>.  The GNU C Library never
defines this, because we don&rsquo;t know what the system has.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPOSIX2_005fFORT_005fRUN"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">_POSIX2_FORT_RUN</strong><a class="copiable-link" href="#index-_005fPOSIX2_005fFORT_005fRUN"> &para;</a></span></dt>
<dd>
<p>If this symbol is defined, it indicates that the system has the POSIX.2
<code class="code">asa</code> command to interpret Fortran carriage control.  The GNU C Library
never defines this, because we don&rsquo;t know what the system has.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPOSIX2_005fLOCALEDEF"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">_POSIX2_LOCALEDEF</strong><a class="copiable-link" href="#index-_005fPOSIX2_005fLOCALEDEF"> &para;</a></span></dt>
<dd>
<p>If this symbol is defined, it indicates that the system has the POSIX.2
<code class="code">localedef</code> command.  The GNU C Library never defines this, because
we don&rsquo;t know what the system has.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPOSIX2_005fSW_005fDEV"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">_POSIX2_SW_DEV</strong><a class="copiable-link" href="#index-_005fPOSIX2_005fSW_005fDEV"> &para;</a></span></dt>
<dd>
<p>If this symbol is defined, it indicates that the system has the POSIX.2
commands <code class="code">ar</code>, <code class="code">make</code>, and <code class="code">strip</code>.  The GNU C Library
always defines this as <code class="code">1</code>, on the assumption that you had to have
<code class="code">ar</code> and <code class="code">make</code> to install the library, and it&rsquo;s unlikely that
<code class="code">strip</code> would be absent when those are present.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Version-Supported.html">Which Version of POSIX is Supported</a>, Previous: <a href="General-Limits.html">General Capacity Limits</a>, Up: <a href="System-Configuration.html">System Configuration Parameters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
