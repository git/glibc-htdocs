<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Dynamic Linker Diagnostics Values (The GNU C Library)</title>

<meta name="description" content="Dynamic Linker Diagnostics Values (The GNU C Library)">
<meta name="keywords" content="Dynamic Linker Diagnostics Values (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Dynamic-Linker-Diagnostics.html" rel="up" title="Dynamic Linker Diagnostics">
<link href="Dynamic-Linker-Diagnostics-Format.html" rel="prev" title="Dynamic Linker Diagnostics Format">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Dynamic-Linker-Diagnostics-Values">
<div class="nav-panel">
<p>
Previous: <a href="Dynamic-Linker-Diagnostics-Format.html" accesskey="p" rel="prev">Dynamic Linker Diagnostics Format</a>, Up: <a href="Dynamic-Linker-Diagnostics.html" accesskey="u" rel="up">Dynamic Linker Diagnostics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Dynamic-Linker-Diagnostics-Values-1"><span>37.1.1.2 Dynamic Linker Diagnostics Values<a class="copiable-link" href="#Dynamic-Linker-Diagnostics-Values-1"> &para;</a></span></h4>

<p>As mentioned above, the set of diagnostics may change between
the GNU C Library releases.  Nevertheless, the following table documents a few
common diagnostic items.  All numbers are in hexadecimal, with a
&lsquo;<samp class="samp">0x</samp>&rsquo; prefix.
</p>
<dl class="table">
<dt><code class="code">dl_dst_lib=<var class="var">string</var></code></dt>
<dd><p>The <code class="code">$LIB</code> dynamic string token expands to <var class="var">string</var>.
</p>
</dd>
<dt><a id="index-HWCAP-_0028diagnostics_0029"></a><span><code class="code">dl_hwcap=<var class="var">integer</var></code><a class="copiable-link" href="#index-HWCAP-_0028diagnostics_0029"> &para;</a></span></dt>
<dt><code class="code">dl_hwcap2=<var class="var">integer</var></code></dt>
<dd><p>The HWCAP and HWCAP2 values, as returned for <code class="code">getauxval</code>, and as
used in other places depending on the architecture.
</p>
</dd>
<dt><a id="index-page-size-_0028diagnostics_0029"></a><span><code class="code">dl_pagesize=<var class="var">integer</var></code><a class="copiable-link" href="#index-page-size-_0028diagnostics_0029"> &para;</a></span></dt>
<dd><p>The system page size is <var class="var">integer</var> bytes.
</p>
</dd>
<dt><code class="code">dl_platform=<var class="var">string</var></code></dt>
<dd><p>The <code class="code">$PLATFORM</code> dynamic string token expands to <var class="var">string</var>.
</p>
</dd>
<dt><code class="code">dso.libc=<var class="var">string</var></code></dt>
<dd><p>This is the soname of the shared <code class="code">libc</code> object that is part of
the GNU C Library.  On most architectures, this is <code class="code">libc.so.6</code>.
</p>
</dd>
<dt><code class="code">env[<var class="var">index</var>]=<var class="var">string</var></code></dt>
<dt><code class="code">env_filtered[<var class="var">index</var>]=<var class="var">string</var></code></dt>
<dd><p>An environment variable from the process environment.  The integer
<var class="var">index</var> is the array index in the environment array.  Variables
under <code class="code">env</code> include the variable value after the &lsquo;<samp class="samp">=</samp>&rsquo; (assuming
that it was present), variables under <code class="code">env_filtered</code> do not.
</p>
</dd>
<dt><code class="code">path.prefix=<var class="var">string</var></code></dt>
<dd><p>This indicates that the GNU C Library was configured using
&lsquo;<samp class="samp">--prefix=<var class="var">string</var></samp>&rsquo;.
</p>
</dd>
<dt><code class="code">path.sysconfdir=<var class="var">string</var></code></dt>
<dd><p>The GNU C Library was configured (perhaps implicitly) with
&lsquo;<samp class="samp">--sysconfdir=<var class="var">string</var></samp>&rsquo; (typically <code class="code">/etc</code>).
</p>
</dd>
<dt><code class="code">path.system_dirs[<var class="var">index</var>]=<var class="var">string</var></code></dt>
<dd><p>These items list the elements of the built-in array that describes the
default library search path.  The value <var class="var">string</var> is a directory file
name with a trailing &lsquo;<samp class="samp">/</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">path.rtld=<var class="var">string</var></code></dt>
<dd><p>This string indicates the application binary interface (ABI) file name
of the run-time dynamic linker.
</p>
</dd>
<dt><code class="code">version.release=&quot;stable&quot;</code></dt>
<dt><code class="code">version.release=&quot;development&quot;</code></dt>
<dd><p>The value <code class="code">&quot;stable&quot;</code> indicates that this build of the GNU C Library is
from a release branch.  Releases labeled as <code class="code">&quot;development&quot;</code> are
unreleased development versions.
</p>
</dd>
<dt><a id="index-version-_0028diagnostics_0029"></a><span><code class="code">version.version=&quot;<var class="var">major</var>.<var class="var">minor</var>&quot;</code><a class="copiable-link" href="#index-version-_0028diagnostics_0029"> &para;</a></span></dt>
<dt><code class="code">version.version=&quot;<var class="var">major</var>.<var class="var">minor</var>.9000&quot;</code></dt>
<dd><p>The GNU C Library version.  Development releases end in &lsquo;<samp class="samp">.9000</samp>&rsquo;.
</p>
</dd>
<dt><a id="index-auxiliary-vector-_0028diagnostics_0029"></a><span><code class="code">auxv[<var class="var">index</var>].a_type=<var class="var">type</var></code><a class="copiable-link" href="#index-auxiliary-vector-_0028diagnostics_0029"> &para;</a></span></dt>
<dt><code class="code">auxv[<var class="var">index</var>].a_val=<var class="var">integer</var></code></dt>
<dt><code class="code">auxv[<var class="var">index</var>].a_val_string=<var class="var">string</var></code></dt>
<dd><p>An entry in the auxiliary vector (specific to Linux).  The values
<var class="var">type</var> (an integer) and <var class="var">integer</var> correspond to the members of
<code class="code">struct auxv</code>.  If the value is a string, <code class="code">a_val_string</code> is
used instead of <code class="code">a_val</code>, so that values have consistent types.
</p>
<p>The <code class="code">AT_HWCAP</code> and <code class="code">AT_HWCAP2</code> values in this output do not
reflect adjustment by the GNU C Library.
</p>
</dd>
<dt><code class="code">uname.sysname=<var class="var">string</var></code></dt>
<dt><code class="code">uname.nodename=<var class="var">string</var></code></dt>
<dt><code class="code">uname.release=<var class="var">string</var></code></dt>
<dt><code class="code">uname.version=<var class="var">string</var></code></dt>
<dt><code class="code">uname.machine=<var class="var">string</var></code></dt>
<dt><code class="code">uname.domain=<var class="var">string</var></code></dt>
<dd><p>These Linux-specific items show the values of <code class="code">struct utsname</code>, as
reported by the <code class="code">uname</code> function.  See <a class="xref" href="Platform-Type.html">Platform Type Identification</a>.
</p>
</dd>
<dt><code class="code">aarch64.cpu_features.&hellip;</code></dt>
<dd><p>These items are specific to the AArch64 architectures.  They report data
the GNU C Library uses to activate conditionally supported features such as
BTI and MTE, and to select alternative function implementations.
</p>
</dd>
<dt><code class="code">aarch64.processor[<var class="var">index</var>].&hellip;</code></dt>
<dd><p>These are additional items for the AArch64 architecture and are
described below.
</p>
</dd>
<dt><code class="code">aarch64.processor[<var class="var">index</var>].requested=<var class="var">kernel-cpu</var></code></dt>
<dd><p>The kernel is told to run the subsequent probing on the CPU numbered
<var class="var">kernel-cpu</var>.  The values <var class="var">kernel-cpu</var> and <var class="var">index</var> can be
distinct if there are gaps in the process CPU affinity mask.  This line
is not included if CPU affinity mask information is not available.
</p>
</dd>
<dt><code class="code">aarch64.processor[<var class="var">index</var>].observed=<var class="var">kernel-cpu</var></code></dt>
<dd><p>This line reports the kernel CPU number <var class="var">kernel-cpu</var> on which the
probing code initially ran.  If the CPU number cannot be obtained,
this line is not printed.
</p>
</dd>
<dt><code class="code">aarch64.processor[<var class="var">index</var>].observed_node=<var class="var">node</var></code></dt>
<dd><p>This reports the observed NUMA node number, as reported by the
<code class="code">getcpu</code> system call.  If this information cannot be obtained, this
line is not printed.
</p>
</dd>
<dt><code class="code">aarch64.processor[<var class="var">index</var>].midr_el1=<var class="var">value</var></code></dt>
<dd><p>The value of the <code class="code">midr_el1</code> system register on the processor
<var class="var">index</var>.  This line is only printed if the kernel indicates that
this system register is supported.
</p>
</dd>
<dt><code class="code">aarch64.processor[<var class="var">index</var>].dczid_el0=<var class="var">value</var></code></dt>
<dd><p>The value of the <code class="code">dczid_el0</code> system register on the processor
<var class="var">index</var>.
</p>
</dd>
<dt><a id="index-CPUID-_0028diagnostics_0029"></a><span><code class="code">x86.cpu_features.&hellip;</code><a class="copiable-link" href="#index-CPUID-_0028diagnostics_0029"> &para;</a></span></dt>
<dd><p>These items are specific to the i386 and x86-64 architectures.  They
reflect supported CPU features and information on cache geometry, mostly
collected using the CPUID instruction.
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].&hellip;</code></dt>
<dd><p>These are additional items for the i386 and x86-64 architectures, as
described below.  They mostly contain raw data from the CPUID
instruction.  The probes are performed for each active CPU for the
<code class="code">ld.so</code> process, and data for different probed CPUs receives a
uniqe <var class="var">index</var> value.  Some CPUID data is expected to differ from CPU
core to CPU core.  In some cases, CPUs are not correctly initialized and
indicate the presence of different feature sets.
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].requested=<var class="var">kernel-cpu</var></code></dt>
<dd><p>The kernel is told to run the subsequent probing on the CPU numbered
<var class="var">kernel-cpu</var>.  The values <var class="var">kernel-cpu</var> and <var class="var">index</var> can be
distinct if there are gaps in the process CPU affinity mask.  This line
is not included if CPU affinity mask information is not available.
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].observed=<var class="var">kernel-cpu</var></code></dt>
<dd><p>This line reports the kernel CPU number <var class="var">kernel-cpu</var> on which the
probing code initially ran.  If the CPU number cannot be obtained,
this line is not printed.
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].observed_node=<var class="var">node</var></code></dt>
<dd><p>This reports the observed NUMA node number, as reported by the
<code class="code">getcpu</code> system call.  If this information cannot be obtained, this
line is not printed.
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid_leaves=<var class="var">count</var></code></dt>
<dd><p>This line indicates that <var class="var">count</var> distinct CPUID leaves were
encountered.  (This reflects internal <code class="code">ld.so</code> storage space, it
does not directly correspond to <code class="code">CPUID</code> enumeration ranges.)
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].ecx_limit=<var class="var">value</var></code></dt>
<dd><p>The CPUID data extraction code uses a brute-force approach to enumerate
subleaves (see the &lsquo;<samp class="samp">.subleaf_eax</samp>&rsquo; lines below).  The last
<code class="code">%rcx</code> value used in a CPUID query on this probed CPU was
<var class="var">value</var>.
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid.eax[<var class="var">query_eax</var>].eax=<var class="var">eax</var></code></dt>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid.eax[<var class="var">query_eax</var>].ebx=<var class="var">ebx</var></code></dt>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid.eax[<var class="var">query_eax</var>].ecx=<var class="var">ecx</var></code></dt>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid.eax[<var class="var">query_eax</var>].edx=<var class="var">edx</var></code></dt>
<dd><p>These lines report the register contents after executing the CPUID
instruction with &lsquo;<samp class="samp">%rax == <var class="var">query_eax</var></samp>&rsquo; and &lsquo;<samp class="samp">%rcx == 0</samp>&rsquo; (a
<em class="dfn">leaf</em>).  For the first probed CPU (with a zero <var class="var">index</var>), only
leaves with non-zero register contents are reported.  For subsequent
CPUs, only leaves whose register contents differs from the previously
probed CPUs (with <var class="var">index</var> one less) are reported.
</p>
<p>Basic and extended leaves are reported using the same syntax.  This
means there is a large jump in <var class="var">query_eax</var> for the first reported
extended leaf.
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid.subleaf_eax[<var class="var">query_eax</var>].ecx[<var class="var">query_ecx</var>].eax=<var class="var">eax</var></code></dt>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid.subleaf_eax[<var class="var">query_eax</var>].ecx[<var class="var">query_ecx</var>].ebx=<var class="var">ebx</var></code></dt>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid.subleaf_eax[<var class="var">query_eax</var>].ecx[<var class="var">query_ecx</var>].ecx=<var class="var">ecx</var></code></dt>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid.subleaf_eax[<var class="var">query_eax</var>].ecx[<var class="var">query_ecx</var>].edx=<var class="var">edx</var></code></dt>
<dd><p>This is similar to the leaves above, but for a <em class="dfn">subleaf</em>.  For
subleaves, the CPUID instruction is executed with &lsquo;<samp class="samp">%rax ==
<var class="var">query_eax</var></samp>&rsquo; and &lsquo;<samp class="samp">%rcx == <var class="var">query_ecx</var></samp>&rsquo;, so the result
depends on both register values.  The same rules about filtering zero
and identical results apply.
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid.subleaf_eax[<var class="var">query_eax</var>].ecx[<var class="var">query_ecx</var>].until_ecx=<var class="var">ecx_limit</var></code></dt>
<dd><p>Some CPUID results are the same regardless the <var class="var">query_ecx</var> value.
If this situation is detected, a line with the &lsquo;<samp class="samp">.until_ecx</samp>&rsquo;
selector ins included, and this indicates that the CPUID register
contents is the same for <code class="code">%rcx</code> values between <var class="var">query_ecx</var>
and <var class="var">ecx_limit</var> (inclusive).
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].cpuid.subleaf_eax[<var class="var">query_eax</var>].ecx[<var class="var">query_ecx</var>].ecx_query_mask=0xff</code></dt>
<dd><p>This line indicates that in an &lsquo;<samp class="samp">.until_ecx</samp>&rsquo; range, the CPUID
instruction preserved the lowested 8 bits of the input <code class="code">%rcx</code> in
the output <code class="code">%rcx</code> registers.  Otherwise, the subleaves in the range
have identical values.  This special treatment is necessary to report
compact range information in case such copying occurs (because the
subleaves would otherwise be all different).
</p>
</dd>
<dt><code class="code">x86.processor[<var class="var">index</var>].xgetbv.ecx[<var class="var">query_ecx</var>]=<var class="var">result</var></code></dt>
<dd><p>This line shows the 64-bit <var class="var">result</var> value in the <code class="code">%rdx:%rax</code>
register pair after executing the XGETBV instruction with <code class="code">%rcx</code>
set to <var class="var">query_ecx</var>.  Zero values and values matching the previously
probed CPU are omitted.  Nothing is printed if the system does not
support the XGETBV instruction.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Dynamic-Linker-Diagnostics-Format.html">Dynamic Linker Diagnostics Format</a>, Up: <a href="Dynamic-Linker-Diagnostics.html">Dynamic Linker Diagnostics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
