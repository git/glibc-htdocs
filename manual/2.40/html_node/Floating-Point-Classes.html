<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Floating Point Classes (The GNU C Library)</title>

<meta name="description" content="Floating Point Classes (The GNU C Library)">
<meta name="keywords" content="Floating Point Classes (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Arithmetic.html" rel="up" title="Arithmetic">
<link href="Floating-Point-Errors.html" rel="next" title="Floating Point Errors">
<link href="Floating-Point-Numbers.html" rel="prev" title="Floating Point Numbers">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Floating-Point-Classes">
<div class="nav-panel">
<p>
Next: <a href="Floating-Point-Errors.html" accesskey="n" rel="next">Errors in Floating-Point Calculations</a>, Previous: <a href="Floating-Point-Numbers.html" accesskey="p" rel="prev">Floating Point Numbers</a>, Up: <a href="Arithmetic.html" accesskey="u" rel="up">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Floating_002dPoint-Number-Classification-Functions"><span>20.4 Floating-Point Number Classification Functions<a class="copiable-link" href="#Floating_002dPoint-Number-Classification-Functions"> &para;</a></span></h3>
<a class="index-entry-id" id="index-floating_002dpoint-classes"></a>
<a class="index-entry-id" id="index-classes_002c-floating_002dpoint"></a>
<a class="index-entry-id" id="index-math_002eh-1"></a>

<p>ISO&nbsp;C99<!-- /@w --> defines macros that let you determine what sort of
floating-point number a variable holds.
</p>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-fpclassify"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">fpclassify</strong> <code class="def-code-arguments">(<em class="emph">float-type</em> <var class="var">x</var>)</code><a class="copiable-link" href="#index-fpclassify"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is a generic macro which works on all floating-point types and
which returns a value of type <code class="code">int</code>.  The possible values are:
</p>
<dl class="vtable">
<dt><a id="index-FP_005fNAN"></a><span><code class="code">FP_NAN</code><a class="copiable-link" href="#index-FP_005fNAN"> &para;</a></span></dt>
<dd>
<p>The floating-point number <var class="var">x</var> is &ldquo;Not a Number&rdquo; (see <a class="pxref" href="Infinity-and-NaN.html">Infinity and NaN</a>)
</p></dd>
<dt><a id="index-FP_005fINFINITE"></a><span><code class="code">FP_INFINITE</code><a class="copiable-link" href="#index-FP_005fINFINITE"> &para;</a></span></dt>
<dd>
<p>The value of <var class="var">x</var> is either plus or minus infinity (see <a class="pxref" href="Infinity-and-NaN.html">Infinity and NaN</a>)
</p></dd>
<dt><a id="index-FP_005fZERO"></a><span><code class="code">FP_ZERO</code><a class="copiable-link" href="#index-FP_005fZERO"> &para;</a></span></dt>
<dd>
<p>The value of <var class="var">x</var> is zero.  In floating-point formats like IEEE&nbsp;754<!-- /@w -->, where zero can be signed, this value is also returned if
<var class="var">x</var> is negative zero.
</p></dd>
<dt><a id="index-FP_005fSUBNORMAL"></a><span><code class="code">FP_SUBNORMAL</code><a class="copiable-link" href="#index-FP_005fSUBNORMAL"> &para;</a></span></dt>
<dd>
<p>Numbers whose absolute value is too small to be represented in the
normal format are represented in an alternate, <em class="dfn">denormalized</em> format
(see <a class="pxref" href="Floating-Point-Concepts.html">Floating Point Representation Concepts</a>).  This format is less precise but can
represent values closer to zero.  <code class="code">fpclassify</code> returns this value
for values of <var class="var">x</var> in this alternate format.
</p></dd>
<dt><a id="index-FP_005fNORMAL"></a><span><code class="code">FP_NORMAL</code><a class="copiable-link" href="#index-FP_005fNORMAL"> &para;</a></span></dt>
<dd>
<p>This value is returned for all other values of <var class="var">x</var>.  It indicates
that there is nothing special about the number.
</p></dd>
</dl>

</dd></dl>

<p><code class="code">fpclassify</code> is most useful if more than one property of a number
must be tested.  There are more specific macros which only test one
property at a time.  Generally these macros execute faster than
<code class="code">fpclassify</code>, since there is special hardware support for them.
You should therefore use the specific macros whenever possible.
</p>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-iscanonical"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">iscanonical</strong> <code class="def-code-arguments">(<em class="emph">float-type</em> <var class="var">x</var>)</code><a class="copiable-link" href="#index-iscanonical"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>In some floating-point formats, some values have canonical (preferred)
and noncanonical encodings (for IEEE interchange binary formats, all
encodings are canonical).  This macro returns a nonzero value if
<var class="var">x</var> has a canonical encoding.  It is from TS 18661-1:2014.
</p>
<p>Note that some formats have multiple encodings of a value which are
all equally canonical; <code class="code">iscanonical</code> returns a nonzero value for
all such encodings.  Also, formats may have encodings that do not
correspond to any valid value of the type.  In ISO C terms these are
<em class="dfn">trap representations</em>; in the GNU C Library, <code class="code">iscanonical</code> returns
zero for such encodings.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-isfinite"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">isfinite</strong> <code class="def-code-arguments">(<em class="emph">float-type</em> <var class="var">x</var>)</code><a class="copiable-link" href="#index-isfinite"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value if <var class="var">x</var> is finite: not plus or
minus infinity, and not NaN.  It is equivalent to
</p>
<div class="example smallexample">
<pre class="example-preformatted">(fpclassify (x) != FP_NAN &amp;&amp; fpclassify (x) != FP_INFINITE)
</pre></div>

<p><code class="code">isfinite</code> is implemented as a macro which accepts any
floating-point type.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-isnormal"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">isnormal</strong> <code class="def-code-arguments">(<em class="emph">float-type</em> <var class="var">x</var>)</code><a class="copiable-link" href="#index-isnormal"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value if <var class="var">x</var> is finite and normalized.
It is equivalent to
</p>
<div class="example smallexample">
<pre class="example-preformatted">(fpclassify (x) == FP_NORMAL)
</pre></div>
</dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-isnan"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">isnan</strong> <code class="def-code-arguments">(<em class="emph">float-type</em> <var class="var">x</var>)</code><a class="copiable-link" href="#index-isnan"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value if <var class="var">x</var> is NaN.  It is equivalent
to
</p>
<div class="example smallexample">
<pre class="example-preformatted">(fpclassify (x) == FP_NAN)
</pre></div>
</dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-issignaling"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">issignaling</strong> <code class="def-code-arguments">(<em class="emph">float-type</em> <var class="var">x</var>)</code><a class="copiable-link" href="#index-issignaling"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value if <var class="var">x</var> is a signaling NaN
(sNaN).  It is from TS 18661-1:2014.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-issubnormal"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">issubnormal</strong> <code class="def-code-arguments">(<em class="emph">float-type</em> <var class="var">x</var>)</code><a class="copiable-link" href="#index-issubnormal"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value if <var class="var">x</var> is subnormal.  It is
from TS 18661-1:2014.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-iszero"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">iszero</strong> <code class="def-code-arguments">(<em class="emph">float-type</em> <var class="var">x</var>)</code><a class="copiable-link" href="#index-iszero"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value if <var class="var">x</var> is zero.  It is from TS
18661-1:2014.
</p></dd></dl>

<p>Another set of floating-point classification functions was provided by
BSD.  The GNU C Library also supports these functions; however, we
recommend that you use the ISO C99 macros in new code.  Those are standard
and will be available more widely.  Also, since they are macros, you do
not have to worry about the type of their argument.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-isinf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">isinf</strong> <code class="def-code-arguments">(double <var class="var">x</var>)</code><a class="copiable-link" href="#index-isinf"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-isinff"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">isinff</strong> <code class="def-code-arguments">(float <var class="var">x</var>)</code><a class="copiable-link" href="#index-isinff"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-isinfl"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">isinfl</strong> <code class="def-code-arguments">(long double <var class="var">x</var>)</code><a class="copiable-link" href="#index-isinfl"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns <code class="code">-1</code> if <var class="var">x</var> represents negative infinity,
<code class="code">1</code> if <var class="var">x</var> represents positive infinity, and <code class="code">0</code> otherwise.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-isnan-1"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">isnan</strong> <code class="def-code-arguments">(double <var class="var">x</var>)</code><a class="copiable-link" href="#index-isnan-1"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-isnanf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">isnanf</strong> <code class="def-code-arguments">(float <var class="var">x</var>)</code><a class="copiable-link" href="#index-isnanf"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-isnanl"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">isnanl</strong> <code class="def-code-arguments">(long double <var class="var">x</var>)</code><a class="copiable-link" href="#index-isnanl"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns a nonzero value if <var class="var">x</var> is a &ldquo;not a number&rdquo;
value, and zero otherwise.
</p>
<p><strong class="strong">NB:</strong> The <code class="code">isnan</code> macro defined by ISO&nbsp;C99<!-- /@w --> overrides
the BSD function.  This is normally not a problem, because the two
routines behave identically.  However, if you really need to get the BSD
function for some reason, you can write
</p>
<div class="example smallexample">
<pre class="example-preformatted">(isnan) (x)
</pre></div>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-finite"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">finite</strong> <code class="def-code-arguments">(double <var class="var">x</var>)</code><a class="copiable-link" href="#index-finite"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-finitef"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">finitef</strong> <code class="def-code-arguments">(float <var class="var">x</var>)</code><a class="copiable-link" href="#index-finitef"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-finitel"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">finitel</strong> <code class="def-code-arguments">(long double <var class="var">x</var>)</code><a class="copiable-link" href="#index-finitel"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns a nonzero value if <var class="var">x</var> is neither infinite nor
a &ldquo;not a number&rdquo; value, and zero otherwise.
</p></dd></dl>

<p><strong class="strong">Portability Note:</strong> The functions listed in this section are BSD
extensions.
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Floating-Point-Errors.html">Errors in Floating-Point Calculations</a>, Previous: <a href="Floating-Point-Numbers.html">Floating Point Numbers</a>, Up: <a href="Arithmetic.html">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
