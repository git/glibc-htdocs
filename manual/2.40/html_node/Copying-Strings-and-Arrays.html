<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Copying Strings and Arrays (The GNU C Library)</title>

<meta name="description" content="Copying Strings and Arrays (The GNU C Library)">
<meta name="keywords" content="Copying Strings and Arrays (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="String-and-Array-Utilities.html" rel="up" title="String and Array Utilities">
<link href="Concatenating-Strings.html" rel="next" title="Concatenating Strings">
<link href="String-Length.html" rel="prev" title="String Length">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Copying-Strings-and-Arrays">
<div class="nav-panel">
<p>
Next: <a href="Concatenating-Strings.html" accesskey="n" rel="next">Concatenating Strings</a>, Previous: <a href="String-Length.html" accesskey="p" rel="prev">String Length</a>, Up: <a href="String-and-Array-Utilities.html" accesskey="u" rel="up">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Copying-Strings-and-Arrays-1"><span>5.4 Copying Strings and Arrays<a class="copiable-link" href="#Copying-Strings-and-Arrays-1"> &para;</a></span></h3>

<p>You can use the functions described in this section to copy the contents
of strings, wide strings, and arrays.  The &lsquo;<samp class="samp">str</samp>&rsquo; and &lsquo;<samp class="samp">mem</samp>&rsquo;
functions are declared in <samp class="file">string.h</samp> while the &lsquo;<samp class="samp">w</samp>&rsquo; functions
are declared in <samp class="file">wchar.h</samp>.
<a class="index-entry-id" id="index-string_002eh-3"></a>
<a class="index-entry-id" id="index-wchar_002eh"></a>
<a class="index-entry-id" id="index-copying-strings-and-arrays"></a>
<a class="index-entry-id" id="index-string-copy-functions"></a>
<a class="index-entry-id" id="index-array-copy-functions"></a>
<a class="index-entry-id" id="index-concatenating-strings"></a>
<a class="index-entry-id" id="index-string-concatenation-functions"></a>
</p>
<p>A helpful way to remember the ordering of the arguments to the functions
in this section is that it corresponds to an assignment expression, with
the destination array specified to the left of the source array.  Most
of these functions return the address of the destination array; a few
return the address of the destination&rsquo;s terminating null, or of just
past the destination.
</p>
<p>Most of these functions do not work properly if the source and
destination arrays overlap.  For example, if the beginning of the
destination array overlaps the end of the source array, the original
contents of that part of the source array may get overwritten before it
is copied.  Even worse, in the case of the string functions, the null
byte marking the end of the string may be lost, and the copy
function might get stuck in a loop trashing all the memory allocated to
your program.
</p>
<p>All functions that have problems copying between overlapping arrays are
explicitly identified in this manual.  In addition to functions in this
section, there are a few others like <code class="code">sprintf</code> (see <a class="pxref" href="Formatted-Output-Functions.html">Formatted Output Functions</a>) and <code class="code">scanf</code> (see <a class="pxref" href="Formatted-Input-Functions.html">Formatted Input Functions</a>).
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-memcpy"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">memcpy</strong> <code class="def-code-arguments">(void *restrict <var class="var">to</var>, const void *restrict <var class="var">from</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-memcpy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">memcpy</code> function copies <var class="var">size</var> bytes from the object
beginning at <var class="var">from</var> into the object beginning at <var class="var">to</var>.  The
behavior of this function is undefined if the two arrays <var class="var">to</var> and
<var class="var">from</var> overlap; use <code class="code">memmove</code> instead if overlapping is possible.
</p>
<p>The value returned by <code class="code">memcpy</code> is the value of <var class="var">to</var>.
</p>
<p>Here is an example of how you might use <code class="code">memcpy</code> to copy the
contents of an array:
</p>
<div class="example smallexample">
<pre class="example-preformatted">struct foo *oldarray, *newarray;
int arraysize;
...
memcpy (new, old, arraysize * sizeof (struct foo));
</pre></div>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wmemcpy"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wmemcpy</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">wto</var>, const wchar_t *restrict <var class="var">wfrom</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-wmemcpy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wmemcpy</code> function copies <var class="var">size</var> wide characters from the object
beginning at <var class="var">wfrom</var> into the object beginning at <var class="var">wto</var>.  The
behavior of this function is undefined if the two arrays <var class="var">wto</var> and
<var class="var">wfrom</var> overlap; use <code class="code">wmemmove</code> instead if overlapping is possible.
</p>
<p>The following is a possible implementation of <code class="code">wmemcpy</code> but there
are more optimizations possible.
</p>
<div class="example smallexample">
<pre class="example-preformatted">wchar_t *
wmemcpy (wchar_t *restrict wto, const wchar_t *restrict wfrom,
         size_t size)
{
  return (wchar_t *) memcpy (wto, wfrom, size * sizeof (wchar_t));
}
</pre></div>

<p>The value returned by <code class="code">wmemcpy</code> is the value of <var class="var">wto</var>.
</p>
<p>This function was introduced in Amendment&nbsp;1<!-- /@w --> to ISO&nbsp;C90<!-- /@w -->.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mempcpy"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">mempcpy</strong> <code class="def-code-arguments">(void *restrict <var class="var">to</var>, const void *restrict <var class="var">from</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-mempcpy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">mempcpy</code> function is nearly identical to the <code class="code">memcpy</code>
function.  It copies <var class="var">size</var> bytes from the object beginning at
<code class="code">from</code> into the object pointed to by <var class="var">to</var>.  But instead of
returning the value of <var class="var">to</var> it returns a pointer to the byte
following the last written byte in the object beginning at <var class="var">to</var>.
I.e., the value is <code class="code">((void *) ((char *) <var class="var">to</var> + <var class="var">size</var>))</code>.
</p>
<p>This function is useful in situations where a number of objects shall be
copied to consecutive memory positions.
</p>
<div class="example smallexample">
<pre class="example-preformatted">void *
combine (void *o1, size_t s1, void *o2, size_t s2)
{
  void *result = malloc (s1 + s2);
  if (result != NULL)
    mempcpy (mempcpy (result, o1, s1), o2, s2);
  return result;
}
</pre></div>

<p>This function is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wmempcpy"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wmempcpy</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">wto</var>, const wchar_t *restrict <var class="var">wfrom</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-wmempcpy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wmempcpy</code> function is nearly identical to the <code class="code">wmemcpy</code>
function.  It copies <var class="var">size</var> wide characters from the object
beginning at <code class="code">wfrom</code> into the object pointed to by <var class="var">wto</var>.  But
instead of returning the value of <var class="var">wto</var> it returns a pointer to the
wide character following the last written wide character in the object
beginning at <var class="var">wto</var>.  I.e., the value is <code class="code"><var class="var">wto</var> + <var class="var">size</var></code>.
</p>
<p>This function is useful in situations where a number of objects shall be
copied to consecutive memory positions.
</p>
<p>The following is a possible implementation of <code class="code">wmemcpy</code> but there
are more optimizations possible.
</p>
<div class="example smallexample">
<pre class="example-preformatted">wchar_t *
wmempcpy (wchar_t *restrict wto, const wchar_t *restrict wfrom,
          size_t size)
{
  return (wchar_t *) mempcpy (wto, wfrom, size * sizeof (wchar_t));
}
</pre></div>

<p>This function is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-memmove"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">memmove</strong> <code class="def-code-arguments">(void *<var class="var">to</var>, const void *<var class="var">from</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-memmove"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">memmove</code> copies the <var class="var">size</var> bytes at <var class="var">from</var> into the
<var class="var">size</var> bytes at <var class="var">to</var>, even if those two blocks of space
overlap.  In the case of overlap, <code class="code">memmove</code> is careful to copy the
original values of the bytes in the block at <var class="var">from</var>, including those
bytes which also belong to the block at <var class="var">to</var>.
</p>
<p>The value returned by <code class="code">memmove</code> is the value of <var class="var">to</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wmemmove"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wmemmove</strong> <code class="def-code-arguments">(wchar_t *<var class="var">wto</var>, const wchar_t *<var class="var">wfrom</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-wmemmove"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">wmemmove</code> copies the <var class="var">size</var> wide characters at <var class="var">wfrom</var>
into the <var class="var">size</var> wide characters at <var class="var">wto</var>, even if those two
blocks of space overlap.  In the case of overlap, <code class="code">wmemmove</code> is
careful to copy the original values of the wide characters in the block
at <var class="var">wfrom</var>, including those wide characters which also belong to the
block at <var class="var">wto</var>.
</p>
<p>The following is a possible implementation of <code class="code">wmemcpy</code> but there
are more optimizations possible.
</p>
<div class="example smallexample">
<pre class="example-preformatted">wchar_t *
wmempcpy (wchar_t *restrict wto, const wchar_t *restrict wfrom,
          size_t size)
{
  return (wchar_t *) mempcpy (wto, wfrom, size * sizeof (wchar_t));
}
</pre></div>

<p>The value returned by <code class="code">wmemmove</code> is the value of <var class="var">wto</var>.
</p>
<p>This function is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-memccpy"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">memccpy</strong> <code class="def-code-arguments">(void *restrict <var class="var">to</var>, const void *restrict <var class="var">from</var>, int <var class="var">c</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-memccpy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function copies no more than <var class="var">size</var> bytes from <var class="var">from</var> to
<var class="var">to</var>, stopping if a byte matching <var class="var">c</var> is found.  The return
value is a pointer into <var class="var">to</var> one byte past where <var class="var">c</var> was copied,
or a null pointer if no byte matching <var class="var">c</var> appeared in the first
<var class="var">size</var> bytes of <var class="var">from</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-memset"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">memset</strong> <code class="def-code-arguments">(void *<var class="var">block</var>, int <var class="var">c</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-memset"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function copies the value of <var class="var">c</var> (converted to an
<code class="code">unsigned char</code>) into each of the first <var class="var">size</var> bytes of the
object beginning at <var class="var">block</var>.  It returns the value of <var class="var">block</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wmemset"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wmemset</strong> <code class="def-code-arguments">(wchar_t *<var class="var">block</var>, wchar_t <var class="var">wc</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-wmemset"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function copies the value of <var class="var">wc</var> into each of the first
<var class="var">size</var> wide characters of the object beginning at <var class="var">block</var>.  It
returns the value of <var class="var">block</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strcpy"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">strcpy</strong> <code class="def-code-arguments">(char *restrict <var class="var">to</var>, const char *restrict <var class="var">from</var>)</code><a class="copiable-link" href="#index-strcpy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This copies bytes from the string <var class="var">from</var> (up to and including
the terminating null byte) into the string <var class="var">to</var>.  Like
<code class="code">memcpy</code>, this function has undefined results if the strings
overlap.  The return value is the value of <var class="var">to</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcscpy"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wcscpy</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">wto</var>, const wchar_t *restrict <var class="var">wfrom</var>)</code><a class="copiable-link" href="#index-wcscpy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This copies wide characters from the wide string <var class="var">wfrom</var> (up to and
including the terminating null wide character) into the string
<var class="var">wto</var>.  Like <code class="code">wmemcpy</code>, this function has undefined results if
the strings overlap.  The return value is the value of <var class="var">wto</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strdup"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">strdup</strong> <code class="def-code-arguments">(const char *<var class="var">s</var>)</code><a class="copiable-link" href="#index-strdup"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function copies the string <var class="var">s</var> into a newly
allocated string.  The string is allocated using <code class="code">malloc</code>; see
<a class="ref" href="Unconstrained-Allocation.html">Unconstrained Allocation</a>.  If <code class="code">malloc</code> cannot allocate space
for the new string, <code class="code">strdup</code> returns a null pointer.  Otherwise it
returns a pointer to the new string.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcsdup"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wcsdup</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">ws</var>)</code><a class="copiable-link" href="#index-wcsdup"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function copies the wide string <var class="var">ws</var>
into a newly allocated string.  The string is allocated using
<code class="code">malloc</code>; see <a class="ref" href="Unconstrained-Allocation.html">Unconstrained Allocation</a>.  If <code class="code">malloc</code>
cannot allocate space for the new string, <code class="code">wcsdup</code> returns a null
pointer.  Otherwise it returns a pointer to the new wide string.
</p>
<p>This function is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-stpcpy"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">stpcpy</strong> <code class="def-code-arguments">(char *restrict <var class="var">to</var>, const char *restrict <var class="var">from</var>)</code><a class="copiable-link" href="#index-stpcpy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is like <code class="code">strcpy</code>, except that it returns a pointer to
the end of the string <var class="var">to</var> (that is, the address of the terminating
null byte <code class="code">to + strlen (from)</code>) rather than the beginning.
</p>
<p>For example, this program uses <code class="code">stpcpy</code> to concatenate &lsquo;<samp class="samp">foo</samp>&rsquo;
and &lsquo;<samp class="samp">bar</samp>&rsquo; to produce &lsquo;<samp class="samp">foobar</samp>&rsquo;, which it then prints.
</p>
<div class="example smallexample">
<pre class="example-preformatted">

#include &lt;string.h&gt;
#include &lt;stdio.h&gt;

int
main (void)
{
  char buffer[10];
  char *to = buffer;
  to = stpcpy (to, &quot;foo&quot;);
  to = stpcpy (to, &quot;bar&quot;);
  puts (buffer);
  return 0;
}
</pre></div>

<p>This function is part of POSIX.1-2008 and later editions, but was
available in the GNU C Library and other systems as an extension long before
it was standardized.
</p>
<p>Its behavior is undefined if the strings overlap.  The function is
declared in <samp class="file">string.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcpcpy"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wcpcpy</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">wto</var>, const wchar_t *restrict <var class="var">wfrom</var>)</code><a class="copiable-link" href="#index-wcpcpy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is like <code class="code">wcscpy</code>, except that it returns a pointer to
the end of the string <var class="var">wto</var> (that is, the address of the terminating
null wide character <code class="code">wto + wcslen (wfrom)</code>) rather than the beginning.
</p>
<p>This function is not part of ISO or POSIX but was found useful while
developing the GNU C Library itself.
</p>
<p>The behavior of <code class="code">wcpcpy</code> is undefined if the strings overlap.
</p>
<p><code class="code">wcpcpy</code> is a GNU extension and is declared in <samp class="file">wchar.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-strdupa"><span class="category-def">Macro: </span><span><code class="def-type">char *</code> <strong class="def-name">strdupa</strong> <code class="def-code-arguments">(const char *<var class="var">s</var>)</code><a class="copiable-link" href="#index-strdupa"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro is similar to <code class="code">strdup</code> but allocates the new string
using <code class="code">alloca</code> instead of <code class="code">malloc</code> (see <a class="pxref" href="Variable-Size-Automatic.html">Automatic Storage with Variable Size</a>).  This means of course the returned string has the same
limitations as any block of memory allocated using <code class="code">alloca</code>.
</p>
<p>For obvious reasons <code class="code">strdupa</code> is implemented only as a macro;
you cannot get the address of this function.  Despite this limitation
it is a useful function.  The following code shows a situation where
using <code class="code">malloc</code> would be a lot more expensive.
</p>
<div class="example smallexample">
<pre class="example-preformatted">

#include &lt;paths.h&gt;
#include &lt;string.h&gt;
#include &lt;stdio.h&gt;

const char path[] = _PATH_STDPATH;

int
main (void)
{
  char *wr_path = strdupa (path);
  char *cp = strtok (wr_path, &quot;:&quot;);

  while (cp != NULL)
    {
      puts (cp);
      cp = strtok (NULL, &quot;:&quot;);
    }
  return 0;
}
</pre></div>

<p>Please note that calling <code class="code">strtok</code> using <var class="var">path</var> directly is
invalid.  It is also not allowed to call <code class="code">strdupa</code> in the argument
list of <code class="code">strtok</code> since <code class="code">strdupa</code> uses <code class="code">alloca</code>
(see <a class="pxref" href="Variable-Size-Automatic.html">Automatic Storage with Variable Size</a>) can interfere with the parameter
passing.
</p>
<p>This function is only available if GNU CC is used.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-bcopy"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">bcopy</strong> <code class="def-code-arguments">(const void *<var class="var">from</var>, void *<var class="var">to</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-bcopy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is a partially obsolete alternative for <code class="code">memmove</code>, derived from
BSD.  Note that it is not quite equivalent to <code class="code">memmove</code>, because the
arguments are not in the same order and there is no return value.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-bzero"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">bzero</strong> <code class="def-code-arguments">(void *<var class="var">block</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-bzero"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is a partially obsolete alternative for <code class="code">memset</code>, derived from
BSD.  Note that it is not as general as <code class="code">memset</code>, because the only
value it can store is zero.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Concatenating-Strings.html">Concatenating Strings</a>, Previous: <a href="String-Length.html">String Length</a>, Up: <a href="String-and-Array-Utilities.html">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
