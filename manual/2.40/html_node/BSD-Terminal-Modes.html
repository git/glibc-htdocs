<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>BSD Terminal Modes (The GNU C Library)</title>

<meta name="description" content="BSD Terminal Modes (The GNU C Library)">
<meta name="keywords" content="BSD Terminal Modes (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Low_002dLevel-Terminal-Interface.html" rel="up" title="Low-Level Terminal Interface">
<link href="Line-Control.html" rel="next" title="Line Control">
<link href="Terminal-Modes.html" rel="prev" title="Terminal Modes">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="BSD-Terminal-Modes">
<div class="nav-panel">
<p>
Next: <a href="Line-Control.html" accesskey="n" rel="next">Line Control Functions</a>, Previous: <a href="Terminal-Modes.html" accesskey="p" rel="prev">Terminal Modes</a>, Up: <a href="Low_002dLevel-Terminal-Interface.html" accesskey="u" rel="up">Low-Level Terminal Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="BSD-Terminal-Modes-1"><span>17.5 BSD Terminal Modes<a class="copiable-link" href="#BSD-Terminal-Modes-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-terminal-modes_002c-BSD"></a>

<p>The usual way to get and set terminal modes is with the functions described
in <a class="ref" href="Terminal-Modes.html">Terminal Modes</a>.  However, on some systems you can use the
BSD-derived functions in this section to do some of the same things.  On
many systems, these functions do not exist.  Even with the GNU C Library,
the functions simply fail with <code class="code">errno</code> = <code class="code">ENOSYS</code> with many
kernels, including Linux.
</p>
<p>The symbols used in this section are declared in <samp class="file">sgtty.h</samp>.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-sgttyb"><span class="category-def">Data Type: </span><span><strong class="def-name">struct sgttyb</strong><a class="copiable-link" href="#index-struct-sgttyb"> &para;</a></span></dt>
<dd>
<p>This structure is an input or output parameter list for <code class="code">gtty</code> and
<code class="code">stty</code>.
</p>
<dl class="table">
<dt><code class="code">char sg_ispeed</code></dt>
<dd><p>Line speed for input
</p></dd>
<dt><code class="code">char sg_ospeed</code></dt>
<dd><p>Line speed for output
</p></dd>
<dt><code class="code">char sg_erase</code></dt>
<dd><p>Erase character
</p></dd>
<dt><code class="code">char sg_kill</code></dt>
<dd><p>Kill character
</p></dd>
<dt><code class="code">int sg_flags</code></dt>
<dd><p>Various flags
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gtty"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">gtty</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, struct sgttyb *<var class="var">attributes</var>)</code><a class="copiable-link" href="#index-gtty"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function gets the attributes of a terminal.
</p>
<p><code class="code">gtty</code> sets *<var class="var">attributes</var> to describe the terminal attributes
of the terminal which is open with file descriptor <var class="var">filedes</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-stty"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">stty</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, const struct sgttyb *<var class="var">attributes</var>)</code><a class="copiable-link" href="#index-stty"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function sets the attributes of a terminal.
</p>
<p><code class="code">stty</code> sets the terminal attributes of the terminal which is open with
file descriptor <var class="var">filedes</var> to those described by *<var class="var">attributes</var>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Line-Control.html">Line Control Functions</a>, Previous: <a href="Terminal-Modes.html">Terminal Modes</a>, Up: <a href="Low_002dLevel-Terminal-Interface.html">Low-Level Terminal Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
