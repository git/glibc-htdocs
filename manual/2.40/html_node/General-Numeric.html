<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>General Numeric (The GNU C Library)</title>

<meta name="description" content="General Numeric (The GNU C Library)">
<meta name="keywords" content="General Numeric (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="The-Lame-Way-to-Locale-Data.html" rel="up" title="The Lame Way to Locale Data">
<link href="Currency-Symbol.html" rel="next" title="Currency Symbol">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="General-Numeric">
<div class="nav-panel">
<p>
Next: <a href="Currency-Symbol.html" accesskey="n" rel="next">Printing the Currency Symbol</a>, Up: <a href="The-Lame-Way-to-Locale-Data.html" accesskey="u" rel="up"><code class="code">localeconv</code>: It is portable but &hellip;</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Generic-Numeric-Formatting-Parameters"><span>7.7.1.1 Generic Numeric Formatting Parameters<a class="copiable-link" href="#Generic-Numeric-Formatting-Parameters"> &para;</a></span></h4>

<p>These are the standard members of <code class="code">struct lconv</code>; there may be
others.
</p>
<dl class="table">
<dt><code class="code">char *decimal_point</code></dt>
<dt><code class="code">char *mon_decimal_point</code></dt>
<dd><p>These are the decimal-point separators used in formatting non-monetary
and monetary quantities, respectively.  In the &lsquo;<samp class="samp">C</samp>&rsquo; locale, the
value of <code class="code">decimal_point</code> is <code class="code">&quot;.&quot;</code>, and the value of
<code class="code">mon_decimal_point</code> is <code class="code">&quot;&quot;</code>.
<a class="index-entry-id" id="index-decimal_002dpoint-separator"></a>
</p>
</dd>
<dt><code class="code">char *thousands_sep</code></dt>
<dt><code class="code">char *mon_thousands_sep</code></dt>
<dd><p>These are the separators used to delimit groups of digits to the left of
the decimal point in formatting non-monetary and monetary quantities,
respectively.  In the &lsquo;<samp class="samp">C</samp>&rsquo; locale, both members have a value of
<code class="code">&quot;&quot;</code> (the empty string).
</p>
</dd>
<dt><code class="code">char *grouping</code></dt>
<dt><code class="code">char *mon_grouping</code></dt>
<dd><p>These are strings that specify how to group the digits to the left of
the decimal point.  <code class="code">grouping</code> applies to non-monetary quantities
and <code class="code">mon_grouping</code> applies to monetary quantities.  Use either
<code class="code">thousands_sep</code> or <code class="code">mon_thousands_sep</code> to separate the digit
groups.
<a class="index-entry-id" id="index-grouping-of-digits"></a>
</p>
<p>Each member of these strings is to be interpreted as an integer value of
type <code class="code">char</code>.  Successive numbers (from left to right) give the
sizes of successive groups (from right to left, starting at the decimal
point.)  The last member is either <code class="code">0</code>, in which case the previous
member is used over and over again for all the remaining groups, or
<code class="code">CHAR_MAX</code>, in which case there is no more grouping&mdash;or, put
another way, any remaining digits form one large group without
separators.
</p>
<p>For example, if <code class="code">grouping</code> is <code class="code">&quot;\04\03\02&quot;</code>, the correct
grouping for the number <code class="code">123456787654321</code> is &lsquo;<samp class="samp">12</samp>&rsquo;, &lsquo;<samp class="samp">34</samp>&rsquo;,
&lsquo;<samp class="samp">56</samp>&rsquo;, &lsquo;<samp class="samp">78</samp>&rsquo;, &lsquo;<samp class="samp">765</samp>&rsquo;, &lsquo;<samp class="samp">4321</samp>&rsquo;.  This uses a group of 4
digits at the end, preceded by a group of 3 digits, preceded by groups
of 2 digits (as many as needed).  With a separator of &lsquo;<samp class="samp">,</samp>&rsquo;, the
number would be printed as &lsquo;<samp class="samp">12,34,56,78,765,4321</samp>&rsquo;.
</p>
<p>A value of <code class="code">&quot;\03&quot;</code> indicates repeated groups of three digits, as
normally used in the U.S.
</p>
<p>In the standard &lsquo;<samp class="samp">C</samp>&rsquo; locale, both <code class="code">grouping</code> and
<code class="code">mon_grouping</code> have a value of <code class="code">&quot;&quot;</code>.  This value specifies no
grouping at all.
</p>
</dd>
<dt><code class="code">char int_frac_digits</code></dt>
<dt><code class="code">char frac_digits</code></dt>
<dd><p>These are small integers indicating how many fractional digits (to the
right of the decimal point) should be displayed in a monetary value in
international and local formats, respectively.  (Most often, both
members have the same value.)
</p>
<p>In the standard &lsquo;<samp class="samp">C</samp>&rsquo; locale, both of these members have the value
<code class="code">CHAR_MAX</code>, meaning &ldquo;unspecified&rdquo;.  The ISO standard doesn&rsquo;t say
what to do when you find this value; we recommend printing no
fractional digits.  (This locale also specifies the empty string for
<code class="code">mon_decimal_point</code>, so printing any fractional digits would be
confusing!)
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Currency-Symbol.html">Printing the Currency Symbol</a>, Up: <a href="The-Lame-Way-to-Locale-Data.html"><code class="code">localeconv</code>: It is portable but &hellip;</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
