<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>NSS Module Names (The GNU C Library)</title>

<meta name="description" content="NSS Module Names (The GNU C Library)">
<meta name="keywords" content="NSS Module Names (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="NSS-Module-Internals.html" rel="up" title="NSS Module Internals">
<link href="NSS-Modules-Interface.html" rel="next" title="NSS Modules Interface">
<link href="NSS-Module-Internals.html" rel="prev" title="NSS Module Internals">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="NSS-Module-Names">
<div class="nav-panel">
<p>
Next: <a href="NSS-Modules-Interface.html" accesskey="n" rel="next">The Interface of the Function in NSS Modules</a>, Previous: <a href="NSS-Module-Internals.html" accesskey="p" rel="prev">NSS Module Internals</a>, Up: <a href="NSS-Module-Internals.html" accesskey="u" rel="up">NSS Module Internals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="The-Naming-Scheme-of-the-NSS-Modules"><span>30.3.1 The Naming Scheme of the NSS Modules<a class="copiable-link" href="#The-Naming-Scheme-of-the-NSS-Modules"> &para;</a></span></h4>

<p>The name of each function consists of various parts:
</p>
<blockquote class="quotation">
<p>_nss_<var class="var">service</var>_<var class="var">function</var>
</p></blockquote>

<p><var class="var">service</var> of course corresponds to the name of the module this
function is found in.<a class="footnote" id="DOCF4" href="#FOOT4"><sup>4</sup></a>  The <var class="var">function</var> part is derived
from the interface function in the C library itself.  If the user calls
the function <code class="code">gethostbyname</code> and the service used is <code class="code">files</code>
the function
</p>
<div class="example smallexample">
<pre class="example-preformatted">       _nss_files_gethostbyname_r
</pre></div>

<p>in the module
</p>
<div class="example smallexample">
<pre class="example-preformatted">       libnss_files.so.2
</pre></div>

<a class="index-entry-id" id="index-reentrant-NSS-functions"></a>
<p>is used.  You see, what is explained above in not the whole truth.  In
fact the NSS modules only contain reentrant versions of the lookup
functions.  I.e., if the user would call the <code class="code">gethostbyname_r</code>
function this also would end in the above function.  For all user
interface functions the C library maps this call to a call to the
reentrant function.  For reentrant functions this is trivial since the
interface is (nearly) the same.  For the non-reentrant version the
library keeps internal buffers which are used to replace the user
supplied buffer.
</p>
<p>I.e., the reentrant functions <em class="emph">can</em> have counterparts.  No service
module is forced to have functions for all databases and all kinds to
access them.  If a function is not available it is simply treated as if
the function would return <code class="code">unavail</code>
(see <a class="pxref" href="Actions-in-the-NSS-configuration.html">Actions in the NSS configuration</a>).
</p>
<p>The file name <samp class="file">libnss_files.so.2</samp> would be on a Solaris&nbsp;2<!-- /@w -->
system <samp class="file">nss_files.so.2</samp>.  This is the difference mentioned above.
Sun&rsquo;s NSS modules are usable as modules which get indirectly loaded
only.
</p>
<p>The NSS modules in the GNU C Library are prepared to be used as normal
libraries themselves.  This is <em class="emph">not</em> true at the moment, though.
However,  the organization of the name space in the modules does not make it
impossible like it is for Solaris.  Now you can see why the modules are
still libraries.<a class="footnote" id="DOCF5" href="#FOOT5"><sup>5</sup></a>
</p>

</div>
<div class="footnotes-segment">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5 class="footnote-body-heading"><a id="FOOT4" href="#DOCF4">(4)</a></h5>
<p>Now you might ask why this information is
duplicated.  The answer is that we want to make it possible to link
directly with these shared objects.</p>
<h5 class="footnote-body-heading"><a id="FOOT5" href="#DOCF5">(5)</a></h5>
<p>There is a second explanation: we were too
lazy to change the Makefiles to allow the generation of shared objects
not starting with <samp class="file">lib</samp> but don&rsquo;t tell this to anybody.</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="NSS-Modules-Interface.html">The Interface of the Function in NSS Modules</a>, Previous: <a href="NSS-Module-Internals.html">NSS Module Internals</a>, Up: <a href="NSS-Module-Internals.html">NSS Module Internals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
