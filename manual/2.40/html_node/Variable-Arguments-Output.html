<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Variable Arguments Output (The GNU C Library)</title>

<meta name="description" content="Variable Arguments Output (The GNU C Library)">
<meta name="keywords" content="Variable Arguments Output (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Output.html" rel="up" title="Formatted Output">
<link href="Parsing-a-Template-String.html" rel="next" title="Parsing a Template String">
<link href="Dynamic-Output.html" rel="prev" title="Dynamic Output">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Variable-Arguments-Output">
<div class="nav-panel">
<p>
Next: <a href="Parsing-a-Template-String.html" accesskey="n" rel="next">Parsing a Template String</a>, Previous: <a href="Dynamic-Output.html" accesskey="p" rel="prev">Dynamically Allocating Formatted Output</a>, Up: <a href="Formatted-Output.html" accesskey="u" rel="up">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Variable-Arguments-Output-Functions"><span>12.12.9 Variable Arguments Output Functions<a class="copiable-link" href="#Variable-Arguments-Output-Functions"> &para;</a></span></h4>

<p>The functions <code class="code">vprintf</code> and friends are provided so that you can
define your own variadic <code class="code">printf</code>-like functions that make use of
the same internals as the built-in formatted output functions.
</p>
<p>The most natural way to define such functions would be to use a language
construct to say, &ldquo;Call <code class="code">printf</code> and pass this template plus all
of my arguments after the first five.&rdquo;  But there is no way to do this
in C, and it would be hard to provide a way, since at the C language
level there is no way to tell how many arguments your function received.
</p>
<p>Since that method is impossible, we provide alternative functions, the
<code class="code">vprintf</code> series, which lets you pass a <code class="code">va_list</code> to describe
&ldquo;all of my arguments after the first five.&rdquo;
</p>
<p>When it is sufficient to define a macro rather than a real function,
the GNU C compiler provides a way to do this much more easily with macros.
For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">#define myprintf(a, b, c, d, e, rest...) \
	    printf (mytemplate , ## rest)
</pre></div>

<p>See <a data-manual="cpp" href="https://gcc.gnu.org/onlinedocs/cpp/Variadic-Macros.html#Variadic-Macros">Variadic Macros</a> in <cite class="cite">The C preprocessor</cite>, for details.
But this is limited to macros, and does not apply to real functions at all.
</p>
<p>Before calling <code class="code">vprintf</code> or the other functions listed in this
section, you <em class="emph">must</em> call <code class="code">va_start</code> (see <a class="pxref" href="Variadic-Functions.html">Variadic Functions</a>) to initialize a pointer to the variable arguments.  Then you
can call <code class="code">va_arg</code> to fetch the arguments that you want to handle
yourself.  This advances the pointer past those arguments.
</p>
<p>Once your <code class="code">va_list</code> pointer is pointing at the argument of your
choice, you are ready to call <code class="code">vprintf</code>.  That argument and all
subsequent arguments that were passed to your function are used by
<code class="code">vprintf</code> along with the template that you specified separately.
</p>
<p><strong class="strong">Portability Note:</strong> The value of the <code class="code">va_list</code> pointer is
undetermined after the call to <code class="code">vprintf</code>, so you must not use
<code class="code">va_arg</code> after you call <code class="code">vprintf</code>.  Instead, you should call
<code class="code">va_end</code> to retire the pointer from service.  You can call
<code class="code">va_start</code> again and begin fetching the arguments from the start of
the variable argument list.  (Alternatively, you can use <code class="code">va_copy</code>
to make a copy of the <code class="code">va_list</code> pointer before calling
<code class="code">vfprintf</code>.)  Calling <code class="code">vprintf</code> does not destroy the argument
list of your function, merely the particular pointer that you passed to
it.
</p>
<p>Prototypes for these functions are declared in <samp class="file">stdio.h</samp>.
<a class="index-entry-id" id="index-stdio_002eh-7"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vprintf</strong> <code class="def-code-arguments">(const char *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vprintf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">printf</code> except that, instead of taking
a variable number of arguments directly, it takes an argument list
pointer <var class="var">ap</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vwprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vwprintf</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vwprintf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">wprintf</code> except that, instead of taking
a variable number of arguments directly, it takes an argument list
pointer <var class="var">ap</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vfprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vfprintf</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, const char *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vfprintf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the equivalent of <code class="code">fprintf</code> with the variable argument list
specified directly as for <code class="code">vprintf</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vfwprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vfwprintf</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, const wchar_t *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vfwprintf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the equivalent of <code class="code">fwprintf</code> with the variable argument list
specified directly as for <code class="code">vwprintf</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vsprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vsprintf</strong> <code class="def-code-arguments">(char *<var class="var">s</var>, const char *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vsprintf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the equivalent of <code class="code">sprintf</code> with the variable argument list
specified directly as for <code class="code">vprintf</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vswprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vswprintf</strong> <code class="def-code-arguments">(wchar_t *<var class="var">ws</var>, size_t <var class="var">size</var>, const wchar_t *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vswprintf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the equivalent of <code class="code">swprintf</code> with the variable argument list
specified directly as for <code class="code">vwprintf</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vsnprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vsnprintf</strong> <code class="def-code-arguments">(char *<var class="var">s</var>, size_t <var class="var">size</var>, const char *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vsnprintf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the equivalent of <code class="code">snprintf</code> with the variable argument list
specified directly as for <code class="code">vprintf</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-vasprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">vasprintf</strong> <code class="def-code-arguments">(char **<var class="var">ptr</var>, const char *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-vasprintf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">vasprintf</code> function is the equivalent of <code class="code">asprintf</code> with the
variable argument list specified directly as for <code class="code">vprintf</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fvprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">obstack_vprintf</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack</var>, const char *<var class="var">template</var>, va_list <var class="var">ap</var>)</code><a class="copiable-link" href="#index-obstack_005fvprintf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack locale
| AS-Unsafe corrupt heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">obstack_vprintf</code> function is the equivalent of
<code class="code">obstack_printf</code> with the variable argument list specified directly
as for <code class="code">vprintf</code>.
</p></dd></dl>

<p>Here&rsquo;s an example showing how you might use <code class="code">vfprintf</code>.  This is a
function that prints error messages to the stream <code class="code">stderr</code>, along
with a prefix indicating the name of the program
(see <a class="pxref" href="Error-Messages.html">Error Messages</a>, for a description of
<code class="code">program_invocation_short_name</code>).
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">#include &lt;stdio.h&gt;
#include &lt;stdarg.h&gt;

void
eprintf (const char *template, ...)
{
  va_list ap;
  extern char *program_invocation_short_name;

  fprintf (stderr, &quot;%s: &quot;, program_invocation_short_name);
  va_start (ap, template);
  vfprintf (stderr, template, ap);
  va_end (ap);
}
</pre></div></div>

<p>You could call <code class="code">eprintf</code> like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">eprintf (&quot;file `%s' does not exist\n&quot;, filename);
</pre></div>

<p>In GNU C, there is a special construct you can use to let the compiler
know that a function uses a <code class="code">printf</code>-style format string.  Then it
can check the number and types of arguments in each call to the
function, and warn you when they do not match the format string.
For example, take this declaration of <code class="code">eprintf</code>:
</p>
<div class="example smallexample">
<pre class="example-preformatted">void eprintf (const char *template, ...)
	__attribute__ ((format (printf, 1, 2)));
</pre></div>

<p>This tells the compiler that <code class="code">eprintf</code> uses a format string like
<code class="code">printf</code> (as opposed to <code class="code">scanf</code>; see <a class="pxref" href="Formatted-Input.html">Formatted Input</a>);
the format string appears as the first argument;
and the arguments to satisfy the format begin with the second.
See <a data-manual="gcc" href="https://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html#Function-Attributes">Declaring Attributes of Functions</a> in <cite class="cite">Using GNU CC</cite>, for more information.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Parsing-a-Template-String.html">Parsing a Template String</a>, Previous: <a href="Dynamic-Output.html">Dynamically Allocating Formatted Output</a>, Up: <a href="Formatted-Output.html">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
