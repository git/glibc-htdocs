<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Argp User Customization (The GNU C Library)</title>

<meta name="description" content="Argp User Customization (The GNU C Library)">
<meta name="keywords" content="Argp User Customization (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Argp.html" rel="up" title="Argp">
<link href="Argp-Examples.html" rel="prev" title="Argp Examples">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Argp-User-Customization">
<div class="nav-panel">
<p>
Previous: <a href="Argp-Examples.html" accesskey="p" rel="prev">Argp Examples</a>, Up: <a href="Argp.html" accesskey="u" rel="up">Parsing Program Options with Argp</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Argp-User-Customization-1"><span>26.3.12 Argp User Customization<a class="copiable-link" href="#Argp-User-Customization-1"> &para;</a></span></h4>

<a class="index-entry-id" id="index-ARGP_005fHELP_005fFMT-environment-variable"></a>
<p>The formatting of argp &lsquo;<samp class="samp">--help</samp>&rsquo; output may be controlled to some
extent by a program&rsquo;s users, by setting the <code class="code">ARGP_HELP_FMT</code>
environment variable to a comma-separated list of tokens.  Whitespace is
ignored:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">dup-args</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">no-dup-args</samp>&rsquo;</dt>
<dd><p>These turn <em class="dfn">duplicate-argument-mode</em> on or off.  In duplicate
argument mode, if an option that accepts an argument has multiple names,
the argument is shown for each name.  Otherwise, it is only shown for the
first long option.  A note is subsequently printed so the user knows that
it applies to other names as well.  The default is &lsquo;<samp class="samp">no-dup-args</samp>&rsquo;,
which is less consistent, but prettier.
</p>
</dd>
<dt>&lsquo;<samp class="samp">dup-args-note</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">no-dup-args-note</samp>&rsquo;</dt>
<dd><p>These will enable or disable the note informing the user of suppressed
option argument duplication.  The default is &lsquo;<samp class="samp">dup-args-note</samp>&rsquo;.
</p>
</dd>
<dt>&lsquo;<samp class="samp">short-opt-col=<var class="var">n</var></samp>&rsquo;</dt>
<dd><p>This prints the first short option in column <var class="var">n</var>.  The default is 2.
</p>
</dd>
<dt>&lsquo;<samp class="samp">long-opt-col=<var class="var">n</var></samp>&rsquo;</dt>
<dd><p>This prints the first long option in column <var class="var">n</var>.  The default is 6.
</p>
</dd>
<dt>&lsquo;<samp class="samp">doc-opt-col=<var class="var">n</var></samp>&rsquo;</dt>
<dd><p>This prints &lsquo;documentation options&rsquo; (see <a class="pxref" href="Argp-Option-Flags.html">Flags for Argp Options</a>) in
column <var class="var">n</var>.  The default is 2.
</p>
</dd>
<dt>&lsquo;<samp class="samp">opt-doc-col=<var class="var">n</var></samp>&rsquo;</dt>
<dd><p>This prints the documentation for options starting in column
<var class="var">n</var>.  The default is 29.
</p>
</dd>
<dt>&lsquo;<samp class="samp">header-col=<var class="var">n</var></samp>&rsquo;</dt>
<dd><p>This will indent the group headers that document groups of options to
column <var class="var">n</var>.  The default is 1.
</p>
</dd>
<dt>&lsquo;<samp class="samp">usage-indent=<var class="var">n</var></samp>&rsquo;</dt>
<dd><p>This will indent continuation lines in &lsquo;<samp class="samp">Usage:</samp>&rsquo; messages to column
<var class="var">n</var>.  The default is 12.
</p>
</dd>
<dt>&lsquo;<samp class="samp">rmargin=<var class="var">n</var></samp>&rsquo;</dt>
<dd><p>This will word wrap help output at or before column <var class="var">n</var>.  The default
is 79.
</p></dd>
</dl>

<ul class="mini-toc">
<li><a href="Suboptions.html" accesskey="1">Parsing of Suboptions</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Argp-Examples.html">Argp Examples</a>, Up: <a href="Argp.html">Parsing Program Options with Argp</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
