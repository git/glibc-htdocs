<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Streams and Threads (The GNU C Library)</title>

<meta name="description" content="Streams and Threads (The GNU C Library)">
<meta name="keywords" content="Streams and Threads (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="I_002fO-on-Streams.html" rel="up" title="I/O on Streams">
<link href="Streams-and-I18N.html" rel="next" title="Streams and I18N">
<link href="Closing-Streams.html" rel="prev" title="Closing Streams">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Streams-and-Threads">
<div class="nav-panel">
<p>
Next: <a href="Streams-and-I18N.html" accesskey="n" rel="next">Streams in Internationalized Applications</a>, Previous: <a href="Closing-Streams.html" accesskey="p" rel="prev">Closing Streams</a>, Up: <a href="I_002fO-on-Streams.html" accesskey="u" rel="up">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Streams-and-Threads-1"><span>12.5 Streams and Threads<a class="copiable-link" href="#Streams-and-Threads-1"> &para;</a></span></h3>

<a class="index-entry-id" id="index-threads"></a>
<a class="index-entry-id" id="index-multi_002dthreaded-application"></a>
<p>Streams can be used in multi-threaded applications in the same way they
are used in single-threaded applications.  But the programmer must be
aware of the possible complications.  It is important to know about
these also if the program one writes never use threads since the design
and implementation of many stream functions are heavily influenced by the
requirements added by multi-threaded programming.
</p>
<p>The POSIX standard requires that by default the stream operations are
atomic.  I.e., issuing two stream operations for the same stream in two
threads at the same time will cause the operations to be executed as if
they were issued sequentially.  The buffer operations performed while
reading or writing are protected from other uses of the same stream.  To
do this each stream has an internal lock object which has to be
(implicitly) acquired before any work can be done.
</p>
<p>But there are situations where this is not enough and there are also
situations where this is not wanted.  The implicit locking is not enough
if the program requires more than one stream function call to happen
atomically.  One example would be if an output line a program wants to
generate is created by several function calls.  The functions by
themselves would ensure only atomicity of their own operation, but not
atomicity over all the function calls.  For this it is necessary to
perform the stream locking in the application code.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-flockfile"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">flockfile</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-flockfile"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">flockfile</code> function acquires the internal locking object
associated with the stream <var class="var">stream</var>.  This ensures that no other
thread can explicitly through <code class="code">flockfile</code>/<code class="code">ftrylockfile</code> or
implicitly through the call of a stream function lock the stream.  The
thread will block until the lock is acquired.  An explicit call to
<code class="code">funlockfile</code> has to be used to release the lock.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ftrylockfile"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">ftrylockfile</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-ftrylockfile"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">ftrylockfile</code> function tries to acquire the internal locking
object associated with the stream <var class="var">stream</var> just like
<code class="code">flockfile</code>.  But unlike <code class="code">flockfile</code> this function does not
block if the lock is not available.  <code class="code">ftrylockfile</code> returns zero if
the lock was successfully acquired.  Otherwise the stream is locked by
another thread.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-funlockfile"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">funlockfile</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-funlockfile"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">funlockfile</code> function releases the internal locking object of
the stream <var class="var">stream</var>.  The stream must have been locked before by a
call to <code class="code">flockfile</code> or a successful call of <code class="code">ftrylockfile</code>.
The implicit locking performed by the stream operations do not count.
The <code class="code">funlockfile</code> function does not return an error status and the
behavior of a call for a stream which is not locked by the current
thread is undefined.
</p></dd></dl>

<p>The following example shows how the functions above can be used to
generate an output line atomically even in multi-threaded applications
(yes, the same job could be done with one <code class="code">fprintf</code> call but it is
sometimes not possible):
</p>
<div class="example smallexample">
<pre class="example-preformatted">FILE *fp;
{
   ...
   flockfile (fp);
   fputs (&quot;This is test number &quot;, fp);
   fprintf (fp, &quot;%d\n&quot;, test);
   funlockfile (fp)
}
</pre></div>

<p>Without the explicit locking it would be possible for another thread to
use the stream <var class="var">fp</var> after the <code class="code">fputs</code> call returns and before
<code class="code">fprintf</code> was called with the result that the number does not
follow the word &lsquo;<samp class="samp">number</samp>&rsquo;.
</p>
<p>From this description it might already be clear that the locking objects
in streams are no simple mutexes.  Since locking the same stream twice
in the same thread is allowed the locking objects must be equivalent to
recursive mutexes.  These mutexes keep track of the owner and the number
of times the lock is acquired.  The same number of <code class="code">funlockfile</code>
calls by the same threads is necessary to unlock the stream completely.
For instance:
</p>
<div class="example smallexample">
<pre class="example-preformatted">void
foo (FILE *fp)
{
  ftrylockfile (fp);
  fputs (&quot;in foo\n&quot;, fp);
  /* <span class="r">This is very wrong!!!</span>  */
  funlockfile (fp);
}
</pre></div>

<p>It is important here that the <code class="code">funlockfile</code> function is only called
if the <code class="code">ftrylockfile</code> function succeeded in locking the stream.  It
is therefore always wrong to ignore the result of <code class="code">ftrylockfile</code>.
And it makes no sense since otherwise one would use <code class="code">flockfile</code>.
The result of code like that above is that either <code class="code">funlockfile</code>
tries to free a stream that hasn&rsquo;t been locked by the current thread or it
frees the stream prematurely.  The code should look like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">void
foo (FILE *fp)
{
  if (ftrylockfile (fp) == 0)
    {
      fputs (&quot;in foo\n&quot;, fp);
      funlockfile (fp);
    }
}
</pre></div>

<p>Now that we covered why it is necessary to have locking it is
necessary to talk about situations when locking is unwanted and what can
be done.  The locking operations (explicit or implicit) don&rsquo;t come for
free.  Even if a lock is not taken the cost is not zero.  The operations
which have to be performed require memory operations that are safe in
multi-processor environments.  With the many local caches involved in
such systems this is quite costly.  So it is best to avoid the locking
completely if it is not needed &ndash; because the code in question is never
used in a context where two or more threads may use a stream at a time.
This can be determined most of the time for application code; for
library code which can be used in many contexts one should default to be
conservative and use locking.
</p>
<p>There are two basic mechanisms to avoid locking.  The first is to use
the <code class="code">_unlocked</code> variants of the stream operations.  The POSIX
standard defines quite a few of those and the GNU C Library adds a few
more.  These variants of the functions behave just like the functions
with the name without the suffix except that they do not lock the
stream.  Using these functions is very desirable since they are
potentially much faster.  This is not only because the locking
operation itself is avoided.  More importantly, functions like
<code class="code">putc</code> and <code class="code">getc</code> are very simple and traditionally (before the
introduction of threads) were implemented as macros which are very fast
if the buffer is not empty.  With the addition of locking requirements
these functions are no longer implemented as macros since they would
expand to too much code.
But these macros are still available with the same functionality under the new
names <code class="code">putc_unlocked</code> and <code class="code">getc_unlocked</code>.  This possibly huge
difference of speed also suggests the use of the <code class="code">_unlocked</code>
functions even if locking is required.  The difference is that the
locking then has to be performed in the program:
</p>
<div class="example smallexample">
<pre class="example-preformatted">void
foo (FILE *fp, char *buf)
{
  flockfile (fp);
  while (*buf != '/')
    putc_unlocked (*buf++, fp);
  funlockfile (fp);
}
</pre></div>

<p>If in this example the <code class="code">putc</code> function would be used and the
explicit locking would be missing the <code class="code">putc</code> function would have to
acquire the lock in every call, potentially many times depending on when
the loop terminates.  Writing it the way illustrated above allows the
<code class="code">putc_unlocked</code> macro to be used which means no locking and direct
manipulation of the buffer of the stream.
</p>
<p>A second way to avoid locking is by using a non-standard function which
was introduced in Solaris and is available in the GNU C Library as well.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005ffsetlocking"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">__fsetlocking</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, int <var class="var">type</var>)</code><a class="copiable-link" href="#index-_005f_005ffsetlocking"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:stream
| AS-Unsafe lock
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">__fsetlocking</code> function can be used to select whether the
stream operations will implicitly acquire the locking object of the
stream <var class="var">stream</var>.  By default this is done but it can be disabled and
reinstated using this function.  There are three values defined for the
<var class="var">type</var> parameter.
</p>
<dl class="vtable">
<dt><a id="index-FSETLOCKING_005fINTERNAL"></a><span><code class="code">FSETLOCKING_INTERNAL</code><a class="copiable-link" href="#index-FSETLOCKING_005fINTERNAL"> &para;</a></span></dt>
<dd><p>The stream <code class="code">stream</code> will from now on use the default internal
locking.  Every stream operation with exception of the <code class="code">_unlocked</code>
variants will implicitly lock the stream.
</p>
</dd>
<dt><a id="index-FSETLOCKING_005fBYCALLER"></a><span><code class="code">FSETLOCKING_BYCALLER</code><a class="copiable-link" href="#index-FSETLOCKING_005fBYCALLER"> &para;</a></span></dt>
<dd><p>After the <code class="code">__fsetlocking</code> function returns, the user is responsible
for locking the stream.  None of the stream operations will implicitly
do this anymore until the state is set back to
<code class="code">FSETLOCKING_INTERNAL</code>.
</p>
</dd>
<dt><a id="index-FSETLOCKING_005fQUERY"></a><span><code class="code">FSETLOCKING_QUERY</code><a class="copiable-link" href="#index-FSETLOCKING_005fQUERY"> &para;</a></span></dt>
<dd><p><code class="code">__fsetlocking</code> only queries the current locking state of the
stream.  The return value will be <code class="code">FSETLOCKING_INTERNAL</code> or
<code class="code">FSETLOCKING_BYCALLER</code> depending on the state.
</p></dd>
</dl>

<p>The return value of <code class="code">__fsetlocking</code> is either
<code class="code">FSETLOCKING_INTERNAL</code> or <code class="code">FSETLOCKING_BYCALLER</code> depending on
the state of the stream before the call.
</p>
<p>This function and the values for the <var class="var">type</var> parameter are declared
in <samp class="file">stdio_ext.h</samp>.
</p></dd></dl>

<p>This function is especially useful when program code has to be used
which is written without knowledge about the <code class="code">_unlocked</code> functions
(or if the programmer was too lazy to use them).
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Streams-and-I18N.html">Streams in Internationalized Applications</a>, Previous: <a href="Closing-Streams.html">Closing Streams</a>, Up: <a href="I_002fO-on-Streams.html">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
