<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Basic Signal Handling (The GNU C Library)</title>

<meta name="description" content="Basic Signal Handling (The GNU C Library)">
<meta name="keywords" content="Basic Signal Handling (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Signal-Actions.html" rel="up" title="Signal Actions">
<link href="Advanced-Signal-Handling.html" rel="next" title="Advanced Signal Handling">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Basic-Signal-Handling">
<div class="nav-panel">
<p>
Next: <a href="Advanced-Signal-Handling.html" accesskey="n" rel="next">Advanced Signal Handling</a>, Up: <a href="Signal-Actions.html" accesskey="u" rel="up">Specifying Signal Actions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Basic-Signal-Handling-1"><span>25.3.1 Basic Signal Handling<a class="copiable-link" href="#Basic-Signal-Handling-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-signal-function"></a>

<p>The <code class="code">signal</code> function provides a simple interface for establishing
an action for a particular signal.  The function and associated macros
are declared in the header file <samp class="file">signal.h</samp>.
<a class="index-entry-id" id="index-signal_002eh-2"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-sighandler_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">sighandler_t</strong><a class="copiable-link" href="#index-sighandler_005ft"> &para;</a></span></dt>
<dd>
<p>This is the type of signal handler functions.  Signal handlers take one
integer argument specifying the signal number, and have return type
<code class="code">void</code>.  So, you should define handler functions like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">void <var class="var">handler</var> (int <code class="code">signum</code>) { ... }
</pre></div>

<p>The name <code class="code">sighandler_t</code> for this data type is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-signal-2"><span class="category-def">Function: </span><span><code class="def-type">sighandler_t</code> <strong class="def-name">signal</strong> <code class="def-code-arguments">(int <var class="var">signum</var>, sighandler_t <var class="var">action</var>)</code><a class="copiable-link" href="#index-signal-2"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe sigintr
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">signal</code> function establishes <var class="var">action</var> as the action for
the signal <var class="var">signum</var>.
</p>
<p>The first argument, <var class="var">signum</var>, identifies the signal whose behavior
you want to control, and should be a signal number.  The proper way to
specify a signal number is with one of the symbolic signal names
(see <a class="pxref" href="Standard-Signals.html">Standard Signals</a>)&mdash;don&rsquo;t use an explicit number, because
the numerical code for a given kind of signal may vary from operating
system to operating system.
</p>
<p>The second argument, <var class="var">action</var>, specifies the action to use for the
signal <var class="var">signum</var>.  This can be one of the following:
</p>
<dl class="table">
<dt><a class="index-entry-id" id="index-default-action-for-a-signal"></a>
<a id="index-SIG_005fDFL"></a><span><code class="code">SIG_DFL</code><a class="copiable-link" href="#index-SIG_005fDFL"> &para;</a></span></dt>
<dd><p><code class="code">SIG_DFL</code> specifies the default action for the particular signal.
The default actions for various kinds of signals are stated in
<a class="ref" href="Standard-Signals.html">Standard Signals</a>.
</p>
</dd>
<dt><a class="index-entry-id" id="index-ignore-action-for-a-signal"></a>
<a id="index-SIG_005fIGN"></a><span><code class="code">SIG_IGN</code><a class="copiable-link" href="#index-SIG_005fIGN"> &para;</a></span></dt>
<dd><p><code class="code">SIG_IGN</code> specifies that the signal should be ignored.
</p>
<p>Your program generally should not ignore signals that represent serious
events or that are normally used to request termination.  You cannot
ignore the <code class="code">SIGKILL</code> or <code class="code">SIGSTOP</code> signals at all.  You can
ignore program error signals like <code class="code">SIGSEGV</code>, but ignoring the error
won&rsquo;t enable the program to continue executing meaningfully.  Ignoring
user requests such as <code class="code">SIGINT</code>, <code class="code">SIGQUIT</code>, and <code class="code">SIGTSTP</code>
is unfriendly.
</p>
<p>When you do not wish signals to be delivered during a certain part of
the program, the thing to do is to block them, not ignore them.
See <a class="xref" href="Blocking-Signals.html">Blocking Signals</a>.
</p>
</dd>
<dt><code class="code"><var class="var">handler</var></code></dt>
<dd><p>Supply the address of a handler function in your program, to specify
running this handler as the way to deliver the signal.
</p>
<p>For more information about defining signal handler functions,
see <a class="ref" href="Defining-Handlers.html">Defining Signal Handlers</a>.
</p></dd>
</dl>

<p>If you set the action for a signal to <code class="code">SIG_IGN</code>, or if you set it
to <code class="code">SIG_DFL</code> and the default action is to ignore that signal, then
any pending signals of that type are discarded (even if they are
blocked).  Discarding the pending signals means that they will never be
delivered, not even if you subsequently specify another action and
unblock this kind of signal.
</p>
<p>The <code class="code">signal</code> function returns the action that was previously in
effect for the specified <var class="var">signum</var>.  You can save this value and
restore it later by calling <code class="code">signal</code> again.
</p>
<p>If <code class="code">signal</code> can&rsquo;t honor the request, it returns <code class="code">SIG_ERR</code>
instead.  The following <code class="code">errno</code> error conditions are defined for
this function:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>You specified an invalid <var class="var">signum</var>; or you tried to ignore or provide
a handler for <code class="code">SIGKILL</code> or <code class="code">SIGSTOP</code>.
</p></dd>
</dl>
</dd></dl>

<p><strong class="strong">Compatibility Note:</strong> A problem encountered when working with the
<code class="code">signal</code> function is that it has different semantics on BSD and
SVID systems.  The difference is that on SVID systems the signal handler
is deinstalled after signal delivery.  On BSD systems the
handler must be explicitly deinstalled.  In the GNU C Library we use the
BSD version by default.  To use the SVID version you can either use the
function <code class="code">sysv_signal</code> (see below) or use the <code class="code">_XOPEN_SOURCE</code>
feature select macro (see <a class="pxref" href="Feature-Test-Macros.html">Feature Test Macros</a>).  In general, use of these
functions should be avoided because of compatibility problems.  It
is better to use <code class="code">sigaction</code> if it is available since the results
are much more reliable.
</p>
<p>Here is a simple example of setting up a handler to delete temporary
files when certain fatal signals happen:
</p>
<div class="example smallexample">
<pre class="example-preformatted">#include &lt;signal.h&gt;

void
termination_handler (int signum)
{
  struct temp_file *p;

  for (p = temp_file_list; p; p = p-&gt;next)
    unlink (p-&gt;name);
}

int
main (void)
{
  ...
  if (signal (SIGINT, termination_handler) == SIG_IGN)
    signal (SIGINT, SIG_IGN);
  if (signal (SIGHUP, termination_handler) == SIG_IGN)
    signal (SIGHUP, SIG_IGN);
  if (signal (SIGTERM, termination_handler) == SIG_IGN)
    signal (SIGTERM, SIG_IGN);
  ...
}
</pre></div>

<p>Note that if a given signal was previously set to be ignored, this code
avoids altering that setting.  This is because non-job-control shells
often ignore certain signals when starting children, and it is important
for the children to respect this.
</p>
<p>We do not handle <code class="code">SIGQUIT</code> or the program error signals in this
example because these are designed to provide information for debugging
(a core dump), and the temporary files may give useful information.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sysv_005fsignal"><span class="category-def">Function: </span><span><code class="def-type">sighandler_t</code> <strong class="def-name">sysv_signal</strong> <code class="def-code-arguments">(int <var class="var">signum</var>, sighandler_t <var class="var">action</var>)</code><a class="copiable-link" href="#index-sysv_005fsignal"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">sysv_signal</code> implements the behavior of the standard
<code class="code">signal</code> function as found on SVID systems.  The difference to BSD
systems is that the handler is deinstalled after a delivery of a signal.
</p>
<p><strong class="strong">Compatibility Note:</strong> As said above for <code class="code">signal</code>, this
function should be avoided when possible.  <code class="code">sigaction</code> is the
preferred method.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ssignal"><span class="category-def">Function: </span><span><code class="def-type">sighandler_t</code> <strong class="def-name">ssignal</strong> <code class="def-code-arguments">(int <var class="var">signum</var>, sighandler_t <var class="var">action</var>)</code><a class="copiable-link" href="#index-ssignal"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe sigintr
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">ssignal</code> function does the same thing as <code class="code">signal</code>; it is
provided only for compatibility with SVID.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIG_005fERR"><span class="category-def">Macro: </span><span><code class="def-type">sighandler_t</code> <strong class="def-name">SIG_ERR</strong><a class="copiable-link" href="#index-SIG_005fERR"> &para;</a></span></dt>
<dd>
<p>The value of this macro is used as the return value from <code class="code">signal</code>
to indicate an error.
</p></dd></dl>



</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Advanced-Signal-Handling.html">Advanced Signal Handling</a>, Up: <a href="Signal-Actions.html">Specifying Signal Actions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
