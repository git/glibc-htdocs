<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Translation with gettext (The GNU C Library)</title>

<meta name="description" content="Translation with gettext (The GNU C Library)">
<meta name="keywords" content="Translation with gettext (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Message-catalogs-with-gettext.html" rel="up" title="Message catalogs with gettext">
<link href="Locating-gettext-catalog.html" rel="next" title="Locating gettext catalog">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Translation-with-gettext">
<div class="nav-panel">
<p>
Next: <a href="Locating-gettext-catalog.html" accesskey="n" rel="next">How to determine which catalog to be used</a>, Up: <a href="Message-catalogs-with-gettext.html" accesskey="u" rel="up">The <code class="code">gettext</code> family of functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="What-has-to-be-done-to-translate-a-message_003f"><span>8.2.1.1 What has to be done to translate a message?<a class="copiable-link" href="#What-has-to-be-done-to-translate-a-message_003f"> &para;</a></span></h4>

<p>The <code class="code">gettext</code> functions have a very simple interface.  The most
basic function just takes the string which shall be translated as the
argument and it returns the translation.  This is fundamentally
different from the <code class="code">catgets</code> approach where an extra key is
necessary and the original string is only used for the error case.
</p>
<p>If the string which has to be translated is the only argument this of
course means the string itself is the key.  I.e., the translation will
be selected based on the original string.  The message catalogs must
therefore contain the original strings plus one translation for any such
string.  The task of the <code class="code">gettext</code> function is to compare the
argument string with the available strings in the catalog and return the
appropriate translation.  Of course this process is optimized so that
this process is not more expensive than an access using an atomic key
like in <code class="code">catgets</code>.
</p>
<p>The <code class="code">gettext</code> approach has some advantages but also some
disadvantages.  Please see the GNU <samp class="file">gettext</samp> manual for a detailed
discussion of the pros and cons.
</p>
<p>All the definitions and declarations for <code class="code">gettext</code> can be found in
the <samp class="file">libintl.h</samp> header file.  On systems where these functions are
not part of the C library they can be found in a separate library named
<samp class="file">libintl.a</samp> (or accordingly different for shared libraries).
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gettext"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">gettext</strong> <code class="def-code-arguments">(const char *<var class="var">msgid</var>)</code><a class="copiable-link" href="#index-gettext"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">gettext</code> function searches the currently selected message
catalogs for a string which is equal to <var class="var">msgid</var>.  If there is such a
string available it is returned.  Otherwise the argument string
<var class="var">msgid</var> is returned.
</p>
<p>Please note that although the return value is <code class="code">char *</code> the
returned string must not be changed.  This broken type results from the
history of the function and does not reflect the way the function should
be used.
</p>
<p>Please note that above we wrote &ldquo;message catalogs&rdquo; (plural).  This is
a specialty of the GNU implementation of these functions and we will
say more about this when we talk about the ways message catalogs are
selected (see <a class="pxref" href="Locating-gettext-catalog.html">How to determine which catalog to be used</a>).
</p>
<p>The <code class="code">gettext</code> function does not modify the value of the global
<code class="code">errno</code> variable.  This is necessary to make it possible to write
something like
</p>
<div class="example smallexample">
<pre class="example-preformatted">  printf (gettext (&quot;Operation failed: %m\n&quot;));
</pre></div>

<p>Here the <code class="code">errno</code> value is used in the <code class="code">printf</code> function while
processing the <code class="code">%m</code> format element and if the <code class="code">gettext</code>
function would change this value (it is called before <code class="code">printf</code> is
called) we would get a wrong message.
</p>
<p>So there is no easy way to detect a missing message catalog besides
comparing the argument string with the result.  But it is normally the
task of the user to react on missing catalogs.  The program cannot guess
when a message catalog is really necessary since for a user who speaks
the language the program was developed in, the message does not need any translation.
</p></dd></dl>

<p>The remaining two functions to access the message catalog add some
functionality to select a message catalog which is not the default one.
This is important if parts of the program are developed independently.
Every part can have its own message catalog and all of them can be used
at the same time.  The C library itself is an example: internally it
uses the <code class="code">gettext</code> functions but since it must not depend on a
currently selected default message catalog it must specify all ambiguous
information.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-dgettext"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">dgettext</strong> <code class="def-code-arguments">(const char *<var class="var">domainname</var>, const char *<var class="var">msgid</var>)</code><a class="copiable-link" href="#index-dgettext"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">dgettext</code> function acts just like the <code class="code">gettext</code>
function.  It only takes an additional first argument <var class="var">domainname</var>
which guides the selection of the message catalogs which are searched
for the translation.  If the <var class="var">domainname</var> parameter is the null
pointer the <code class="code">dgettext</code> function is exactly equivalent to
<code class="code">gettext</code> since the default value for the domain name is used.
</p>
<p>As for <code class="code">gettext</code> the return value type is <code class="code">char *</code> which is an
anachronism.  The returned string must never be modified.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-dcgettext"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">dcgettext</strong> <code class="def-code-arguments">(const char *<var class="var">domainname</var>, const char *<var class="var">msgid</var>, int <var class="var">category</var>)</code><a class="copiable-link" href="#index-dcgettext"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">dcgettext</code> adds another argument to those which
<code class="code">dgettext</code> takes.  This argument <var class="var">category</var> specifies the last
piece of information needed to localize the message catalog.  I.e., the
domain name and the locale category exactly specify which message
catalog has to be used (relative to a given directory, see below).
</p>
<p>The <code class="code">dgettext</code> function can be expressed in terms of
<code class="code">dcgettext</code> by using
</p>
<div class="example smallexample">
<pre class="example-preformatted">dcgettext (domain, string, LC_MESSAGES)
</pre></div>

<p>instead of
</p>
<div class="example smallexample">
<pre class="example-preformatted">dgettext (domain, string)
</pre></div>

<p>This also shows which values are expected for the third parameter.  One
has to use the available selectors for the categories available in
<samp class="file">locale.h</samp>.  Normally the available values are <code class="code">LC_CTYPE</code>,
<code class="code">LC_COLLATE</code>, <code class="code">LC_MESSAGES</code>, <code class="code">LC_MONETARY</code>,
<code class="code">LC_NUMERIC</code>, and <code class="code">LC_TIME</code>.  Please note that <code class="code">LC_ALL</code>
must not be used and even though the names might suggest this, there is
no relation to the environment variable of this name.
</p>
<p>The <code class="code">dcgettext</code> function is only implemented for compatibility with
other systems which have <code class="code">gettext</code> functions.  There is not really
any situation where it is necessary (or useful) to use a different value
than <code class="code">LC_MESSAGES</code> for the <var class="var">category</var> parameter.  We are
dealing with messages here and any other choice can only be irritating.
</p>
<p>As for <code class="code">gettext</code> the return value type is <code class="code">char *</code> which is an
anachronism.  The returned string must never be modified.
</p></dd></dl>

<p>When using the three functions above in a program it is a frequent case
that the <var class="var">msgid</var> argument is a constant string.  So it is worthwhile to
optimize this case.  Thinking shortly about this one will realize that
as long as no new message catalog is loaded the translation of a message
will not change.  This optimization is actually implemented by the
<code class="code">gettext</code>, <code class="code">dgettext</code> and <code class="code">dcgettext</code> functions.
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Locating-gettext-catalog.html">How to determine which catalog to be used</a>, Up: <a href="Message-catalogs-with-gettext.html">The <code class="code">gettext</code> family of functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
