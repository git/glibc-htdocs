<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Socket Pairs (The GNU C Library)</title>

<meta name="description" content="Socket Pairs (The GNU C Library)">
<meta name="keywords" content="Socket Pairs (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Open_002fClose-Sockets.html" rel="up" title="Open/Close Sockets">
<link href="Closing-a-Socket.html" rel="prev" title="Closing a Socket">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Socket-Pairs">
<div class="nav-panel">
<p>
Previous: <a href="Closing-a-Socket.html" accesskey="p" rel="prev">Closing a Socket</a>, Up: <a href="Open_002fClose-Sockets.html" accesskey="u" rel="up">Opening and Closing Sockets</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Socket-Pairs-1"><span>16.8.3 Socket Pairs<a class="copiable-link" href="#Socket-Pairs-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-creating-a-socket-pair"></a>
<a class="index-entry-id" id="index-socket-pair"></a>
<a class="index-entry-id" id="index-opening-a-socket-pair"></a>

<a class="index-entry-id" id="index-sys_002fsocket_002eh-8"></a>
<p>A <em class="dfn">socket pair</em> consists of a pair of connected (but unnamed)
sockets.  It is very similar to a pipe and is used in much the same
way.  Socket pairs are created with the <code class="code">socketpair</code> function,
declared in <samp class="file">sys/socket.h</samp>.  A socket pair is much like a pipe; the
main difference is that the socket pair is bidirectional, whereas the
pipe has one input-only end and one output-only end (see <a class="pxref" href="Pipes-and-FIFOs.html">Pipes and FIFOs</a>).
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-socketpair"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">socketpair</strong> <code class="def-code-arguments">(int <var class="var">namespace</var>, int <var class="var">style</var>, int <var class="var">protocol</var>, int <var class="var">filedes</var><code class="t">[2]</code>)</code><a class="copiable-link" href="#index-socketpair"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function creates a socket pair, returning the file descriptors in
<code class="code"><var class="var">filedes</var>[0]</code> and <code class="code"><var class="var">filedes</var>[1]</code>.  The socket pair
is a full-duplex communications channel, so that both reading and writing
may be performed at either end.
</p>
<p>The <var class="var">namespace</var>, <var class="var">style</var> and <var class="var">protocol</var> arguments are
interpreted as for the <code class="code">socket</code> function.  <var class="var">style</var> should be
one of the communication styles listed in <a class="ref" href="Communication-Styles.html">Communication Styles</a>.
The <var class="var">namespace</var> argument specifies the namespace, which must be
<code class="code">AF_LOCAL</code> (see <a class="pxref" href="Local-Namespace.html">The Local Namespace</a>); <var class="var">protocol</var> specifies the
communications protocol, but zero is the only meaningful value.
</p>
<p>If <var class="var">style</var> specifies a connectionless communication style, then
the two sockets you get are not <em class="emph">connected</em>, strictly speaking,
but each of them knows the other as the default destination address,
so they can send packets to each other.
</p>
<p>The <code class="code">socketpair</code> function returns <code class="code">0</code> on success and <code class="code">-1</code>
on failure.  The following <code class="code">errno</code> error conditions are defined
for this function:
</p>
<dl class="table">
<dt><code class="code">EMFILE</code></dt>
<dd><p>The process has too many file descriptors open.
</p>
</dd>
<dt><code class="code">EAFNOSUPPORT</code></dt>
<dd><p>The specified namespace is not supported.
</p>
</dd>
<dt><code class="code">EPROTONOSUPPORT</code></dt>
<dd><p>The specified protocol is not supported.
</p>
</dd>
<dt><code class="code">EOPNOTSUPP</code></dt>
<dd><p>The specified protocol does not support the creation of socket pairs.
</p></dd>
</dl>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Closing-a-Socket.html">Closing a Socket</a>, Up: <a href="Open_002fClose-Sockets.html">Opening and Closing Sockets</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
