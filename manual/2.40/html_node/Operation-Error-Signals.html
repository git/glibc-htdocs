<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Operation Error Signals (The GNU C Library)</title>

<meta name="description" content="Operation Error Signals (The GNU C Library)">
<meta name="keywords" content="Operation Error Signals (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Standard-Signals.html" rel="up" title="Standard Signals">
<link href="Miscellaneous-Signals.html" rel="next" title="Miscellaneous Signals">
<link href="Job-Control-Signals.html" rel="prev" title="Job Control Signals">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Operation-Error-Signals">
<div class="nav-panel">
<p>
Next: <a href="Miscellaneous-Signals.html" accesskey="n" rel="next">Miscellaneous Signals</a>, Previous: <a href="Job-Control-Signals.html" accesskey="p" rel="prev">Job Control Signals</a>, Up: <a href="Standard-Signals.html" accesskey="u" rel="up">Standard Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Operation-Error-Signals-1"><span>25.2.6 Operation Error Signals<a class="copiable-link" href="#Operation-Error-Signals-1"> &para;</a></span></h4>

<p>These signals are used to report various errors generated by an
operation done by the program.  They do not necessarily indicate a
programming error in the program, but an error that prevents an
operating system call from completing.  The default action for all of
them is to cause the process to terminate.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGPIPE"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGPIPE</strong><a class="copiable-link" href="#index-SIGPIPE"> &para;</a></span></dt>
<dd>
<a class="index-entry-id" id="index-pipe-signal"></a>
<a class="index-entry-id" id="index-broken-pipe-signal"></a>
<p>Broken pipe.  If you use pipes or FIFOs, you have to design your
application so that one process opens the pipe for reading before
another starts writing.  If the reading process never starts, or
terminates unexpectedly, writing to the pipe or FIFO raises a
<code class="code">SIGPIPE</code> signal.  If <code class="code">SIGPIPE</code> is blocked, handled or
ignored, the offending call fails with <code class="code">EPIPE</code> instead.
</p>
<p>Pipes and FIFO special files are discussed in more detail in <a class="ref" href="Pipes-and-FIFOs.html">Pipes and FIFOs</a>.
</p>
<p>Another cause of <code class="code">SIGPIPE</code> is when you try to output to a socket
that isn&rsquo;t connected.  See <a class="xref" href="Sending-Data.html">Sending Data</a>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGLOST"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGLOST</strong><a class="copiable-link" href="#index-SIGLOST"> &para;</a></span></dt>
<dd>
<a class="index-entry-id" id="index-lost-resource-signal"></a>
<p>Resource lost.  This signal is generated when you have an advisory lock
on an NFS file, and the NFS server reboots and forgets about your lock.
</p>
<p>On GNU/Hurd systems, <code class="code">SIGLOST</code> is generated when any server program
dies unexpectedly.  It is usually fine to ignore the signal; whatever
call was made to the server that died just returns an error.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGXCPU"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGXCPU</strong><a class="copiable-link" href="#index-SIGXCPU"> &para;</a></span></dt>
<dd>
<p>CPU time limit exceeded.  This signal is generated when the process
exceeds its soft resource limit on CPU time.  See <a class="xref" href="Limits-on-Resources.html">Limiting Resource Usage</a>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGXFSZ"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGXFSZ</strong><a class="copiable-link" href="#index-SIGXFSZ"> &para;</a></span></dt>
<dd>
<p>File size limit exceeded.  This signal is generated when the process
attempts to extend a file so it exceeds the process&rsquo;s soft resource
limit on file size.  See <a class="xref" href="Limits-on-Resources.html">Limiting Resource Usage</a>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Miscellaneous-Signals.html">Miscellaneous Signals</a>, Previous: <a href="Job-Control-Signals.html">Job Control Signals</a>, Up: <a href="Standard-Signals.html">Standard Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
