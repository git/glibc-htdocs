<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Non-reentrant String Conversion (The GNU C Library)</title>

<meta name="description" content="Non-reentrant String Conversion (The GNU C Library)">
<meta name="keywords" content="Non-reentrant String Conversion (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Non_002dreentrant-Conversion.html" rel="up" title="Non-reentrant Conversion">
<link href="Shift-State.html" rel="next" title="Shift State">
<link href="Non_002dreentrant-Character-Conversion.html" rel="prev" title="Non-reentrant Character Conversion">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Non_002dreentrant-String-Conversion">
<div class="nav-panel">
<p>
Next: <a href="Shift-State.html" accesskey="n" rel="next">States in Non-reentrant Functions</a>, Previous: <a href="Non_002dreentrant-Character-Conversion.html" accesskey="p" rel="prev">Non-reentrant Conversion of Single Characters</a>, Up: <a href="Non_002dreentrant-Conversion.html" accesskey="u" rel="up">Non-reentrant Conversion Function</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Non_002dreentrant-Conversion-of-Strings"><span>6.4.2 Non-reentrant Conversion of Strings<a class="copiable-link" href="#Non_002dreentrant-Conversion-of-Strings"> &para;</a></span></h4>

<p>For convenience the ISO&nbsp;C90<!-- /@w --> standard also defines functions to
convert entire strings instead of single characters.  These functions
suffer from the same problems as their reentrant counterparts from
Amendment&nbsp;1<!-- /@w --> to ISO&nbsp;C90<!-- /@w -->; see <a class="ref" href="Converting-Strings.html">Converting Multibyte and Wide Character Strings</a>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mbstowcs"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">mbstowcs</strong> <code class="def-code-arguments">(wchar_t *<var class="var">wstring</var>, const char *<var class="var">string</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-mbstowcs"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">mbstowcs</code> (&ldquo;multibyte string to wide character string&rdquo;)
function converts the null-terminated string of multibyte characters
<var class="var">string</var> to an array of wide character codes, storing not more than
<var class="var">size</var> wide characters into the array beginning at <var class="var">wstring</var>.
The terminating null character counts towards the size, so if <var class="var">size</var>
is less than the actual number of wide characters resulting from
<var class="var">string</var>, no terminating null character is stored.
</p>
<p>The conversion of characters from <var class="var">string</var> begins in the initial
shift state.
</p>
<p>If an invalid multibyte character sequence is found, the <code class="code">mbstowcs</code>
function returns a value of <em class="math">-1</em>.  Otherwise, it returns the number
of wide characters stored in the array <var class="var">wstring</var>.  This number does
not include the terminating null character, which is present if the
number is less than <var class="var">size</var>.
</p>
<p>Here is an example showing how to convert a string of multibyte
characters, allocating enough space for the result.
</p>
<div class="example smallexample">
<pre class="example-preformatted">wchar_t *
mbstowcs_alloc (const char *string)
{
  size_t size = strlen (string) + 1;
  wchar_t *buf = xmalloc (size * sizeof (wchar_t));

  size = mbstowcs (buf, string, size);
  if (size == (size_t) -1)
    return NULL;
  buf = xreallocarray (buf, size + 1, sizeof *buf);
  return buf;
}
</pre></div>

<p>If <var class="var">wstring</var> is a null pointer then no output is written and the
conversion proceeds as above, and the result is returned.  In practice
such behaviour is useful for calculating the exact number of wide
characters required to convert <var class="var">string</var>.  This behaviour of
accepting a null pointer for <var class="var">wstring</var> is an XPG4.2<!-- /@w --> extension
that is not specified in ISO&nbsp;C<!-- /@w --> and is optional in POSIX<!-- /@w -->.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcstombs"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">wcstombs</strong> <code class="def-code-arguments">(char *<var class="var">string</var>, const wchar_t *<var class="var">wstring</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-wcstombs"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wcstombs</code> (&ldquo;wide character string to multibyte string&rdquo;)
function converts the null-terminated wide character array <var class="var">wstring</var>
into a string containing multibyte characters, storing not more than
<var class="var">size</var> bytes starting at <var class="var">string</var>, followed by a terminating
null character if there is room.  The conversion of characters begins in
the initial shift state.
</p>
<p>The terminating null character counts towards the size, so if <var class="var">size</var>
is less than or equal to the number of bytes needed in <var class="var">wstring</var>, no
terminating null character is stored.
</p>
<p>If a code that does not correspond to a valid multibyte character is
found, the <code class="code">wcstombs</code> function returns a value of <em class="math">-1</em>.
Otherwise, the return value is the number of bytes stored in the array
<var class="var">string</var>.  This number does not include the terminating null character,
which is present if the number is less than <var class="var">size</var>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Shift-State.html">States in Non-reentrant Functions</a>, Previous: <a href="Non_002dreentrant-Character-Conversion.html">Non-reentrant Conversion of Single Characters</a>, Up: <a href="Non_002dreentrant-Conversion.html">Non-reentrant Conversion Function</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
