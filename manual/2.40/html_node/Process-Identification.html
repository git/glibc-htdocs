<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Process Identification (The GNU C Library)</title>

<meta name="description" content="Process Identification (The GNU C Library)">
<meta name="keywords" content="Process Identification (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Processes.html" rel="up" title="Processes">
<link href="Creating-a-Process.html" rel="next" title="Creating a Process">
<link href="Process-Creation-Concepts.html" rel="prev" title="Process Creation Concepts">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Process-Identification">
<div class="nav-panel">
<p>
Next: <a href="Creating-a-Process.html" accesskey="n" rel="next">Creating a Process</a>, Previous: <a href="Process-Creation-Concepts.html" accesskey="p" rel="prev">Process Creation Concepts</a>, Up: <a href="Processes.html" accesskey="u" rel="up">Processes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Process-Identification-1"><span>27.3 Process Identification<a class="copiable-link" href="#Process-Identification-1"> &para;</a></span></h3>

<a class="index-entry-id" id="index-process-ID"></a>
<p>Each process is named by a <em class="dfn">process ID</em> number, a value of type
<code class="code">pid_t</code>.  A process ID is allocated to each process when it is
created.  Process IDs are reused over time.  The lifetime of a process
ends when the parent process of the corresponding process waits on the
process ID after the process has terminated.  See <a class="xref" href="Process-Completion.html">Process Completion</a>.  (The parent process can arrange for such waiting to
happen implicitly.)  A process ID uniquely identifies a process only
during the lifetime of the process.  As a rule of thumb, this means
that the process must still be running.
</p>
<p>Process IDs can also denote process groups and sessions.
See <a class="xref" href="Job-Control.html">Job Control</a>.
</p>
<a class="index-entry-id" id="index-thread-ID"></a>
<a class="index-entry-id" id="index-task-ID"></a>
<a class="index-entry-id" id="index-thread-group"></a>
<p>On Linux, threads created by <code class="code">pthread_create</code> also receive a
<em class="dfn">thread ID</em>.  The thread ID of the initial (main) thread is the
same as the process ID of the entire process.  Thread IDs for
subsequently created threads are distinct.  They are allocated from
the same numbering space as process IDs.  Process IDs and thread IDs
are sometimes also referred to collectively as <em class="dfn">task IDs</em>.  In
contrast to processes, threads are never waited for explicitly, so a
thread ID becomes eligible for reuse as soon as a thread exits or is
canceled.  This is true even for joinable threads, not just detached
threads.  Threads are assigned to a <em class="dfn">thread group</em>.  In
the GNU C Library implementation running on Linux, the process ID is the
thread group ID of all threads in the process.
</p>
<p>You can get the process ID of a process by calling <code class="code">getpid</code>.  The
function <code class="code">getppid</code> returns the process ID of the parent of the
current process (this is also known as the <em class="dfn">parent process ID</em>).
Your program should include the header files <samp class="file">unistd.h</samp> and
<samp class="file">sys/types.h</samp> to use these functions.
<a class="index-entry-id" id="index-sys_002ftypes_002eh-1"></a>
<a class="index-entry-id" id="index-unistd_002eh-17"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-pid_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">pid_t</strong><a class="copiable-link" href="#index-pid_005ft"> &para;</a></span></dt>
<dd>
<p>The <code class="code">pid_t</code> data type is a signed integer type which is capable
of representing a process ID.  In the GNU C Library, this is an <code class="code">int</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getpid"><span class="category-def">Function: </span><span><code class="def-type">pid_t</code> <strong class="def-name">getpid</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-getpid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getpid</code> function returns the process ID of the current process.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getppid"><span class="category-def">Function: </span><span><code class="def-type">pid_t</code> <strong class="def-name">getppid</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-getppid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getppid</code> function returns the process ID of the parent of the
current process.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gettid"><span class="category-def">Function: </span><span><code class="def-type">pid_t</code> <strong class="def-name">gettid</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-gettid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">gettid</code> function returns the thread ID of the current
thread.  The returned value is obtained from the Linux kernel and is
not subject to caching.  See the discussion of thread IDs above,
especially regarding reuse of the IDs of threads which have exited.
</p>
<p>This function is specific to Linux.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Creating-a-Process.html">Creating a Process</a>, Previous: <a href="Process-Creation-Concepts.html">Process Creation Concepts</a>, Up: <a href="Processes.html">Processes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
