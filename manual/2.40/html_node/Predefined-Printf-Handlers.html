<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Predefined Printf Handlers (The GNU C Library)</title>

<meta name="description" content="Predefined Printf Handlers (The GNU C Library)">
<meta name="keywords" content="Predefined Printf Handlers (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Customizing-Printf.html" rel="up" title="Customizing Printf">
<link href="Printf-Extension-Example.html" rel="prev" title="Printf Extension Example">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Predefined-Printf-Handlers">
<div class="nav-panel">
<p>
Previous: <a href="Printf-Extension-Example.html" accesskey="p" rel="prev"><code class="code">printf</code> Extension Example</a>, Up: <a href="Customizing-Printf.html" accesskey="u" rel="up">Customizing <code class="code">printf</code></a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Predefined-printf-Handlers"><span>12.13.5 Predefined <code class="code">printf</code> Handlers<a class="copiable-link" href="#Predefined-printf-Handlers"> &para;</a></span></h4>

<p>The GNU C Library also contains a concrete and useful application of the
<code class="code">printf</code> handler extension.  There are two functions available
which implement a special way to print floating-point numbers.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-printf_005fsize"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">printf_size</strong> <code class="def-code-arguments">(FILE *<var class="var">fp</var>, const struct printf_info *<var class="var">info</var>, const void *const *<var class="var">args</var>)</code><a class="copiable-link" href="#index-printf_005fsize"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:fp locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Print a given floating point number as for the format <code class="code">%f</code> except
that there is a postfix character indicating the divisor for the
number to make this less than 1000.  There are two possible divisors:
powers of 1024 or powers of 1000.  Which one is used depends on the
format character specified while registered this handler.  If the
character is of lower case, 1024 is used.  For upper case characters,
1000 is used.
</p>
<p>The postfix tag corresponds to bytes, kilobytes, megabytes, gigabytes,
etc.  The full table is:
</p>

<p>The default precision is 3, i.e., 1024 is printed with a lower-case
format character as if it were <code class="code">%.3fk</code> and will yield <code class="code">1.000k</code>.
</p></dd></dl>

<p>Due to the requirements of <code class="code">register_printf_function</code> we must also
provide the function which returns information about the arguments.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-printf_005fsize_005finfo"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">printf_size_info</strong> <code class="def-code-arguments">(const struct printf_info *<var class="var">info</var>, size_t <var class="var">n</var>, int *<var class="var">argtypes</var>)</code><a class="copiable-link" href="#index-printf_005fsize_005finfo"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function will return in <var class="var">argtypes</var> the information about the
used parameters in the way the <code class="code">vfprintf</code> implementation expects
it.  The format always takes one argument.
</p></dd></dl>

<p>To use these functions both functions must be registered with a call like
</p>
<div class="example smallexample">
<pre class="example-preformatted">register_printf_function ('B', printf_size, printf_size_info);
</pre></div>

<p>Here we register the functions to print numbers as powers of 1000 since
the format character <code class="code">'B'</code> is an upper-case character.  If we
would additionally use <code class="code">'b'</code> in a line like
</p>
<div class="example smallexample">
<pre class="example-preformatted">register_printf_function ('b', printf_size, printf_size_info);
</pre></div>

<p>we could also print using a power of 1024.  Please note that all that is
different in these two lines is the format specifier.  The
<code class="code">printf_size</code> function knows about the difference between lower and upper
case format specifiers.
</p>
<p>The use of <code class="code">'B'</code> and <code class="code">'b'</code> is no coincidence.  Rather it is
the preferred way to use this functionality since it is available on
some other systems which also use format specifiers.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Printf-Extension-Example.html"><code class="code">printf</code> Extension Example</a>, Up: <a href="Customizing-Printf.html">Customizing <code class="code">printf</code></a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
