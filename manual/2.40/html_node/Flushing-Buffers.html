<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Flushing Buffers (The GNU C Library)</title>

<meta name="description" content="Flushing Buffers (The GNU C Library)">
<meta name="keywords" content="Flushing Buffers (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Stream-Buffering.html" rel="up" title="Stream Buffering">
<link href="Controlling-Buffering.html" rel="next" title="Controlling Buffering">
<link href="Buffering-Concepts.html" rel="prev" title="Buffering Concepts">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Flushing-Buffers">
<div class="nav-panel">
<p>
Next: <a href="Controlling-Buffering.html" accesskey="n" rel="next">Controlling Which Kind of Buffering</a>, Previous: <a href="Buffering-Concepts.html" accesskey="p" rel="prev">Buffering Concepts</a>, Up: <a href="Stream-Buffering.html" accesskey="u" rel="up">Stream Buffering</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Flushing-Buffers-1"><span>12.20.2 Flushing Buffers<a class="copiable-link" href="#Flushing-Buffers-1"> &para;</a></span></h4>

<a class="index-entry-id" id="index-flushing-a-stream"></a>
<p><em class="dfn">Flushing</em> output on a buffered stream means transmitting all
accumulated characters to the file.  There are many circumstances when
buffered output on a stream is flushed automatically:
</p>
<ul class="itemize mark-bullet">
<li>When you try to do output and the output buffer is full.

</li><li>When the stream is closed.  See <a class="xref" href="Closing-Streams.html">Closing Streams</a>.

</li><li>When the program terminates by calling <code class="code">exit</code>.
See <a class="xref" href="Normal-Termination.html">Normal Termination</a>.

</li><li>When a newline is written, if the stream is line buffered.

</li><li>Whenever an input operation on <em class="emph">any</em> stream actually reads data
from its file.
</li></ul>

<p>If you want to flush the buffered output at another time, call
<code class="code">fflush</code>, which is declared in the header file <samp class="file">stdio.h</samp>.
<a class="index-entry-id" id="index-stdio_002eh-11"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fflush"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fflush</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-fflush"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function causes any buffered output on <var class="var">stream</var> to be delivered
to the file.  If <var class="var">stream</var> is a null pointer, then
<code class="code">fflush</code> causes buffered output on <em class="emph">all</em> open output streams
to be flushed.
</p>
<p>This function returns <code class="code">EOF</code> if a write error occurs, or zero
otherwise.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fflush_005funlocked"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fflush_unlocked</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-fflush_005funlocked"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:stream
| AS-Unsafe corrupt
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fflush_unlocked</code> function is equivalent to the <code class="code">fflush</code>
function except that it does not implicitly lock the stream.
</p></dd></dl>

<p>The <code class="code">fflush</code> function can be used to flush all streams currently
opened.  While this is useful in some situations it does often more than
necessary since it might be done in situations when terminal input is
required and the program wants to be sure that all output is visible on
the terminal.  But this means that only line buffered streams have to be
flushed.  Solaris introduced a function especially for this.  It was
always available in the GNU C Library in some form but never officially
exported.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005fflushlbf"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">_flushlbf</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005fflushlbf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">_flushlbf</code> function flushes all line buffered streams
currently opened.
</p>
<p>This function is declared in the <samp class="file">stdio_ext.h</samp> header.
</p></dd></dl>

<p><strong class="strong">Compatibility Note:</strong> Some brain-damaged operating systems have
been known to be so thoroughly fixated on line-oriented input and output
that flushing a line buffered stream causes a newline to be written!
Fortunately, this &ldquo;feature&rdquo; seems to be becoming less common.  You do
not need to worry about this with the GNU C Library.
</p>
<p>In some situations it might be useful to not flush the output pending
for a stream but instead simply forget it.  If transmission is costly
and the output is not needed anymore this is valid reasoning.  In this
situation a non-standard function introduced in Solaris and available in
the GNU C Library can be used.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005ffpurge"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">__fpurge</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-_005f_005ffpurge"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:stream
| AS-Unsafe corrupt
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">__fpurge</code> function causes the buffer of the stream
<var class="var">stream</var> to be emptied.  If the stream is currently in read mode all
input in the buffer is lost.  If the stream is in output mode the
buffered output is not written to the device (or whatever other
underlying storage) and the buffer is cleared.
</p>
<p>This function is declared in <samp class="file">stdio_ext.h</samp>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Controlling-Buffering.html">Controlling Which Kind of Buffering</a>, Previous: <a href="Buffering-Concepts.html">Buffering Concepts</a>, Up: <a href="Stream-Buffering.html">Stream Buffering</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
