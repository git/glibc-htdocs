<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Finding Tokens in a String (The GNU C Library)</title>

<meta name="description" content="Finding Tokens in a String (The GNU C Library)">
<meta name="keywords" content="Finding Tokens in a String (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="String-and-Array-Utilities.html" rel="up" title="String and Array Utilities">
<link href="Erasing-Sensitive-Data.html" rel="next" title="Erasing Sensitive Data">
<link href="Search-Functions.html" rel="prev" title="Search Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Finding-Tokens-in-a-String">
<div class="nav-panel">
<p>
Next: <a href="Erasing-Sensitive-Data.html" accesskey="n" rel="next">Erasing Sensitive Data</a>, Previous: <a href="Search-Functions.html" accesskey="p" rel="prev">Search Functions</a>, Up: <a href="String-and-Array-Utilities.html" accesskey="u" rel="up">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Finding-Tokens-in-a-String-1"><span>5.10 Finding Tokens in a String<a class="copiable-link" href="#Finding-Tokens-in-a-String-1"> &para;</a></span></h3>

<a class="index-entry-id" id="index-tokenizing-strings"></a>
<a class="index-entry-id" id="index-breaking-a-string-into-tokens"></a>
<a class="index-entry-id" id="index-parsing-tokens-from-a-string"></a>
<p>It&rsquo;s fairly common for programs to have a need to do some simple kinds
of lexical analysis and parsing, such as splitting a command string up
into tokens.  You can do this with the <code class="code">strtok</code> function, declared
in the header file <samp class="file">string.h</samp>.
<a class="index-entry-id" id="index-string_002eh-8"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strtok"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">strtok</strong> <code class="def-code-arguments">(char *restrict <var class="var">newstring</var>, const char *restrict <var class="var">delimiters</var>)</code><a class="copiable-link" href="#index-strtok"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:strtok
| AS-Unsafe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>A string can be split into tokens by making a series of calls to the
function <code class="code">strtok</code>.
</p>
<p>The string to be split up is passed as the <var class="var">newstring</var> argument on
the first call only.  The <code class="code">strtok</code> function uses this to set up
some internal state information.  Subsequent calls to get additional
tokens from the same string are indicated by passing a null pointer as
the <var class="var">newstring</var> argument.  Calling <code class="code">strtok</code> with another
non-null <var class="var">newstring</var> argument reinitializes the state information.
It is guaranteed that no other library function ever calls <code class="code">strtok</code>
behind your back (which would mess up this internal state information).
</p>
<p>The <var class="var">delimiters</var> argument is a string that specifies a set of delimiters
that may surround the token being extracted.  All the initial bytes
that are members of this set are discarded.  The first byte that is
<em class="emph">not</em> a member of this set of delimiters marks the beginning of the
next token.  The end of the token is found by looking for the next
byte that is a member of the delimiter set.  This byte in the
original string <var class="var">newstring</var> is overwritten by a null byte, and the
pointer to the beginning of the token in <var class="var">newstring</var> is returned.
</p>
<p>On the next call to <code class="code">strtok</code>, the searching begins at the next
byte beyond the one that marked the end of the previous token.
Note that the set of delimiters <var class="var">delimiters</var> do not have to be the
same on every call in a series of calls to <code class="code">strtok</code>.
</p>
<p>If the end of the string <var class="var">newstring</var> is reached, or if the remainder of
string consists only of delimiter bytes, <code class="code">strtok</code> returns
a null pointer.
</p>
<p>In a multibyte string, characters consisting of
more than one byte are not treated as single entities.  Each byte is treated
separately.  The function is not locale-dependent.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcstok"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wcstok</strong> <code class="def-code-arguments">(wchar_t *<var class="var">newstring</var>, const wchar_t *<var class="var">delimiters</var>, wchar_t **<var class="var">save_ptr</var>)</code><a class="copiable-link" href="#index-wcstok"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>A string can be split into tokens by making a series of calls to the
function <code class="code">wcstok</code>.
</p>
<p>The string to be split up is passed as the <var class="var">newstring</var> argument on
the first call only.  The <code class="code">wcstok</code> function uses this to set up
some internal state information.  Subsequent calls to get additional
tokens from the same wide string are indicated by passing a
null pointer as the <var class="var">newstring</var> argument, which causes the pointer
previously stored in <var class="var">save_ptr</var> to be used instead.
</p>
<p>The <var class="var">delimiters</var> argument is a wide string that specifies
a set of delimiters that may surround the token being extracted.  All
the initial wide characters that are members of this set are discarded.
The first wide character that is <em class="emph">not</em> a member of this set of
delimiters marks the beginning of the next token.  The end of the token
is found by looking for the next wide character that is a member of the
delimiter set.  This wide character in the original wide
string <var class="var">newstring</var> is overwritten by a null wide character, the
pointer past the overwritten wide character is saved in <var class="var">save_ptr</var>,
and the pointer to the beginning of the token in <var class="var">newstring</var> is
returned.
</p>
<p>On the next call to <code class="code">wcstok</code>, the searching begins at the next
wide character beyond the one that marked the end of the previous token.
Note that the set of delimiters <var class="var">delimiters</var> do not have to be the
same on every call in a series of calls to <code class="code">wcstok</code>.
</p>
<p>If the end of the wide string <var class="var">newstring</var> is reached, or
if the remainder of string consists only of delimiter wide characters,
<code class="code">wcstok</code> returns a null pointer.
</p></dd></dl>

<p><strong class="strong">Warning:</strong> Since <code class="code">strtok</code> and <code class="code">wcstok</code> alter the string
they is parsing, you should always copy the string to a temporary buffer
before parsing it with <code class="code">strtok</code>/<code class="code">wcstok</code> (see <a class="pxref" href="Copying-Strings-and-Arrays.html">Copying Strings and Arrays</a>).  If you allow <code class="code">strtok</code> or <code class="code">wcstok</code> to modify
a string that came from another part of your program, you are asking for
trouble; that string might be used for other purposes after
<code class="code">strtok</code> or <code class="code">wcstok</code> has modified it, and it would not have
the expected value.
</p>
<p>The string that you are operating on might even be a constant.  Then
when <code class="code">strtok</code> or <code class="code">wcstok</code> tries to modify it, your program
will get a fatal signal for writing in read-only memory.  See <a class="xref" href="Program-Error-Signals.html">Program Error Signals</a>.  Even if the operation of <code class="code">strtok</code> or <code class="code">wcstok</code>
would not require a modification of the string (e.g., if there is
exactly one token) the string can (and in the GNU C Library case will) be
modified.
</p>
<p>This is a special case of a general principle: if a part of a program
does not have as its purpose the modification of a certain data
structure, then it is error-prone to modify the data structure
temporarily.
</p>
<p>The function <code class="code">strtok</code> is not reentrant, whereas <code class="code">wcstok</code> is.
See <a class="xref" href="Nonreentrancy.html">Signal Handling and Nonreentrant Functions</a>, for a discussion of where and why reentrancy is
important.
</p>
<p>Here is a simple example showing the use of <code class="code">strtok</code>.
</p>
<div class="example smallexample">
<pre class="example-preformatted">#include &lt;string.h&gt;
#include &lt;stddef.h&gt;

...

const char string[] = &quot;words separated by spaces -- and, punctuation!&quot;;
const char delimiters[] = &quot; .,;:!-&quot;;
char *token, *cp;

...

cp = strdupa (string);                /* Make writable copy.  */
token = strtok (cp, delimiters);      /* token =&gt; &quot;words&quot; */
token = strtok (NULL, delimiters);    /* token =&gt; &quot;separated&quot; */
token = strtok (NULL, delimiters);    /* token =&gt; &quot;by&quot; */
token = strtok (NULL, delimiters);    /* token =&gt; &quot;spaces&quot; */
token = strtok (NULL, delimiters);    /* token =&gt; &quot;and&quot; */
token = strtok (NULL, delimiters);    /* token =&gt; &quot;punctuation&quot; */
token = strtok (NULL, delimiters);    /* token =&gt; NULL */
</pre></div>

<p>The GNU C Library contains two more functions for tokenizing a string
which overcome the limitation of non-reentrancy.  They are not
available available for wide strings.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strtok_005fr"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">strtok_r</strong> <code class="def-code-arguments">(char *<var class="var">newstring</var>, const char *<var class="var">delimiters</var>, char **<var class="var">save_ptr</var>)</code><a class="copiable-link" href="#index-strtok_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Just like <code class="code">strtok</code>, this function splits the string into several
tokens which can be accessed by successive calls to <code class="code">strtok_r</code>.
The difference is that, as in <code class="code">wcstok</code>, the information about the
next token is stored in the space pointed to by the third argument,
<var class="var">save_ptr</var>, which is a pointer to a string pointer.  Calling
<code class="code">strtok_r</code> with a null pointer for <var class="var">newstring</var> and leaving
<var class="var">save_ptr</var> between the calls unchanged does the job without
hindering reentrancy.
</p>
<p>This function is defined in POSIX.1 and can be found on many systems
which support multi-threading.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strsep"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">strsep</strong> <code class="def-code-arguments">(char **<var class="var">string_ptr</var>, const char *<var class="var">delimiter</var>)</code><a class="copiable-link" href="#index-strsep"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function has a similar functionality as <code class="code">strtok_r</code> with the
<var class="var">newstring</var> argument replaced by the <var class="var">save_ptr</var> argument.  The
initialization of the moving pointer has to be done by the user.
Successive calls to <code class="code">strsep</code> move the pointer along the tokens
separated by <var class="var">delimiter</var>, returning the address of the next token
and updating <var class="var">string_ptr</var> to point to the beginning of the next
token.
</p>
<p>One difference between <code class="code">strsep</code> and <code class="code">strtok_r</code> is that if the
input string contains more than one byte from <var class="var">delimiter</var> in a
row <code class="code">strsep</code> returns an empty string for each pair of bytes
from <var class="var">delimiter</var>.  This means that a program normally should test
for <code class="code">strsep</code> returning an empty string before processing it.
</p>
<p>This function was introduced in 4.3BSD and therefore is widely available.
</p></dd></dl>

<p>Here is how the above example looks like when <code class="code">strsep</code> is used.
</p>
<div class="example smallexample">
<pre class="example-preformatted">#include &lt;string.h&gt;
#include &lt;stddef.h&gt;

...

const char string[] = &quot;words separated by spaces -- and, punctuation!&quot;;
const char delimiters[] = &quot; .,;:!-&quot;;
char *running;
char *token;

...

running = strdupa (string);
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;words&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;separated&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;by&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;spaces&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;and&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;punctuation&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; &quot;&quot; */
token = strsep (&amp;running, delimiters);    /* token =&gt; NULL */
</pre></div>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-basename"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">basename</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>)</code><a class="copiable-link" href="#index-basename"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The GNU version of the <code class="code">basename</code> function returns the last
component of the path in <var class="var">filename</var>.  This function is the preferred
usage, since it does not modify the argument, <var class="var">filename</var>, and
respects trailing slashes.  The prototype for <code class="code">basename</code> can be
found in <samp class="file">string.h</samp>.  Note, this function is overridden by the XPG
version, if <samp class="file">libgen.h</samp> is included.
</p>
<p>Example of using GNU <code class="code">basename</code>:
</p>
<div class="example smallexample">
<pre class="example-preformatted">#include &lt;string.h&gt;

int
main (int argc, char *argv[])
{
  char *prog = basename (argv[0]);

  if (argc &lt; 2)
    {
      fprintf (stderr, &quot;Usage %s &lt;arg&gt;\n&quot;, prog);
      exit (1);
    }

  ...
}
</pre></div>

<p><strong class="strong">Portability Note:</strong> This function may produce different results
on different systems.
</p>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-basename-1"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">basename</strong> <code class="def-code-arguments">(char *<var class="var">path</var>)</code><a class="copiable-link" href="#index-basename-1"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the standard XPG defined <code class="code">basename</code>.  It is similar in
spirit to the GNU version, but may modify the <var class="var">path</var> by removing
trailing &rsquo;/&rsquo; bytes.  If the <var class="var">path</var> is made up entirely of &rsquo;/&rsquo;
bytes, then &quot;/&quot; will be returned.  Also, if <var class="var">path</var> is
<code class="code">NULL</code> or an empty string, then &quot;.&quot; is returned.  The prototype for
the XPG version can be found in <samp class="file">libgen.h</samp>.
</p>
<p>Example of using XPG <code class="code">basename</code>:
</p>
<div class="example smallexample">
<pre class="example-preformatted">#include &lt;libgen.h&gt;

int
main (int argc, char *argv[])
{
  char *prog;
  char *path = strdupa (argv[0]);

  prog = basename (path);

  if (argc &lt; 2)
    {
      fprintf (stderr, &quot;Usage %s &lt;arg&gt;\n&quot;, prog);
      exit (1);
    }

  ...

}
</pre></div>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-dirname"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">dirname</strong> <code class="def-code-arguments">(char *<var class="var">path</var>)</code><a class="copiable-link" href="#index-dirname"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">dirname</code> function is the compliment to the XPG version of
<code class="code">basename</code>.  It returns the parent directory of the file specified
by <var class="var">path</var>.  If <var class="var">path</var> is <code class="code">NULL</code>, an empty string, or
contains no &rsquo;/&rsquo; bytes, then &quot;.&quot; is returned.  The prototype for this
function can be found in <samp class="file">libgen.h</samp>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Erasing-Sensitive-Data.html">Erasing Sensitive Data</a>, Previous: <a href="Search-Functions.html">Search Functions</a>, Up: <a href="String-and-Array-Utilities.html">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
