<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Elision Tunables (The GNU C Library)</title>

<meta name="description" content="Elision Tunables (The GNU C Library)">
<meta name="keywords" content="Elision Tunables (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Tunables.html" rel="up" title="Tunables">
<link href="POSIX-Thread-Tunables.html" rel="next" title="POSIX Thread Tunables">
<link href="Dynamic-Linking-Tunables.html" rel="prev" title="Dynamic Linking Tunables">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Elision-Tunables">
<div class="nav-panel">
<p>
Next: <a href="POSIX-Thread-Tunables.html" accesskey="n" rel="next">POSIX Thread Tunables</a>, Previous: <a href="Dynamic-Linking-Tunables.html" accesskey="p" rel="prev">Dynamic Linking Tunables</a>, Up: <a href="Tunables.html" accesskey="u" rel="up">Tunables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Elision-Tunables-1"><span>39.4 Elision Tunables<a class="copiable-link" href="#Elision-Tunables-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-elision-tunables"></a>
<a class="index-entry-id" id="index-tunables_002c-elision"></a>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002eelision"><span class="category-def">Tunable namespace: </span><span><strong class="def-name">glibc.elision</strong><a class="copiable-link" href="#index-glibc_002eelision"> &para;</a></span></dt>
<dd><p>Contended locks are usually slow and can lead to performance and scalability
issues in multithread code. Lock elision will use memory transactions to under
certain conditions, to elide locks and improve performance.
Elision behavior can be modified by setting the following tunables in
the <code class="code">elision</code> namespace:
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002eelision_002eenable"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.elision.enable</strong><a class="copiable-link" href="#index-glibc_002eelision_002eenable"> &para;</a></span></dt>
<dd><p>The <code class="code">glibc.elision.enable</code> tunable enables lock elision if the feature is
supported by the hardware.  If elision is not supported by the hardware this
tunable has no effect.
</p>
<p>Elision tunables are supported for 64-bit Intel, IBM POWER, and z System
architectures.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002eelision_002eskip_005flock_005fbusy"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.elision.skip_lock_busy</strong><a class="copiable-link" href="#index-glibc_002eelision_002eskip_005flock_005fbusy"> &para;</a></span></dt>
<dd><p>The <code class="code">glibc.elision.skip_lock_busy</code> tunable sets how many times to use a
non-transactional lock after a transactional failure has occurred because the
lock is already acquired.  Expressed in number of lock acquisition attempts.
</p>
<p>The default value of this tunable is &lsquo;<samp class="samp">3</samp>&rsquo;.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002eelision_002eskip_005flock_005finternal_005fabort"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.elision.skip_lock_internal_abort</strong><a class="copiable-link" href="#index-glibc_002eelision_002eskip_005flock_005finternal_005fabort"> &para;</a></span></dt>
<dd><p>The <code class="code">glibc.elision.skip_lock_internal_abort</code> tunable sets how many times
the thread should avoid using elision if a transaction aborted for any reason
other than a different thread&rsquo;s memory accesses.  Expressed in number of lock
acquisition attempts.
</p>
<p>The default value of this tunable is &lsquo;<samp class="samp">3</samp>&rsquo;.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002eelision_002eskip_005flock_005fafter_005fretries"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.elision.skip_lock_after_retries</strong><a class="copiable-link" href="#index-glibc_002eelision_002eskip_005flock_005fafter_005fretries"> &para;</a></span></dt>
<dd><p>The <code class="code">glibc.elision.skip_lock_after_retries</code> tunable sets how many times
to try to elide a lock with transactions, that only failed due to a different
thread&rsquo;s memory accesses, before falling back to regular lock.
Expressed in number of lock elision attempts.
</p>
<p>This tunable is supported only on IBM POWER, and z System architectures.
</p>
<p>The default value of this tunable is &lsquo;<samp class="samp">3</samp>&rsquo;.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002eelision_002etries"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.elision.tries</strong><a class="copiable-link" href="#index-glibc_002eelision_002etries"> &para;</a></span></dt>
<dd><p>The <code class="code">glibc.elision.tries</code> sets how many times to retry elision if there is
chance for the transaction to finish execution e.g., it wasn&rsquo;t
aborted due to the lock being already acquired.  If elision is not supported
by the hardware this tunable is set to &lsquo;<samp class="samp">0</samp>&rsquo; to avoid retries.
</p>
<p>The default value of this tunable is &lsquo;<samp class="samp">3</samp>&rsquo;.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002eelision_002eskip_005ftrylock_005finternal_005fabort"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.elision.skip_trylock_internal_abort</strong><a class="copiable-link" href="#index-glibc_002eelision_002eskip_005ftrylock_005finternal_005fabort"> &para;</a></span></dt>
<dd><p>The <code class="code">glibc.elision.skip_trylock_internal_abort</code> tunable sets how many
times the thread should avoid trying the lock if a transaction aborted due to
reasons other than a different thread&rsquo;s memory accesses.  Expressed in number
of try lock attempts.
</p>
<p>The default value of this tunable is &lsquo;<samp class="samp">3</samp>&rsquo;.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="POSIX-Thread-Tunables.html">POSIX Thread Tunables</a>, Previous: <a href="Dynamic-Linking-Tunables.html">Dynamic Linking Tunables</a>, Up: <a href="Tunables.html">Tunables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
