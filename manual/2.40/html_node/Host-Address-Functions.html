<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Host Address Functions (The GNU C Library)</title>

<meta name="description" content="Host Address Functions (The GNU C Library)">
<meta name="keywords" content="Host Address Functions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Host-Addresses.html" rel="up" title="Host Addresses">
<link href="Host-Names.html" rel="next" title="Host Names">
<link href="Host-Address-Data-Type.html" rel="prev" title="Host Address Data Type">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Host-Address-Functions">
<div class="nav-panel">
<p>
Next: <a href="Host-Names.html" accesskey="n" rel="next">Host Names</a>, Previous: <a href="Host-Address-Data-Type.html" accesskey="p" rel="prev">Host Address Data Type</a>, Up: <a href="Host-Addresses.html" accesskey="u" rel="up">Host Addresses</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Host-Address-Functions-1"><span>16.6.2.3 Host Address Functions<a class="copiable-link" href="#Host-Address-Functions-1"> &para;</a></span></h4>

<a class="index-entry-id" id="index-arpa_002finet_002eh"></a>
<p>These additional functions for manipulating Internet addresses are
declared in the header file <samp class="file">arpa/inet.h</samp>.  They represent Internet
addresses in network byte order, and network numbers and
local-address-within-network numbers in host byte order.  See <a class="xref" href="Byte-Order.html">Byte Order Conversion</a>, for an explanation of network and host byte order.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-inet_005faton"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">inet_aton</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>, struct in_addr *<var class="var">addr</var>)</code><a class="copiable-link" href="#index-inet_005faton"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function converts the IPv4 Internet host address <var class="var">name</var>
from the standard numbers-and-dots notation into binary data and stores
it in the <code class="code">struct in_addr</code> that <var class="var">addr</var> points to.
<code class="code">inet_aton</code> returns nonzero if the address is valid, zero if not.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-inet_005faddr"><span class="category-def">Function: </span><span><code class="def-type">uint32_t</code> <strong class="def-name">inet_addr</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>)</code><a class="copiable-link" href="#index-inet_005faddr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function converts the IPv4 Internet host address <var class="var">name</var> from the
standard numbers-and-dots notation into binary data.  If the input is
not valid, <code class="code">inet_addr</code> returns <code class="code">INADDR_NONE</code>.  This is an
obsolete interface to <code class="code">inet_aton</code>, described immediately above.  It
is obsolete because <code class="code">INADDR_NONE</code> is a valid address
(255.255.255.255), and <code class="code">inet_aton</code> provides a cleaner way to
indicate error return.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-inet_005fnetwork"><span class="category-def">Function: </span><span><code class="def-type">uint32_t</code> <strong class="def-name">inet_network</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>)</code><a class="copiable-link" href="#index-inet_005fnetwork"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function extracts the network number from the address <var class="var">name</var>,
given in the standard numbers-and-dots notation.  The returned address is
in host order.  If the input is not valid, <code class="code">inet_network</code> returns
<code class="code">-1</code>.
</p>
<p>The function works only with traditional IPv4 class A, B and C network
types.  It doesn&rsquo;t work with classless addresses and shouldn&rsquo;t be used
anymore.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-inet_005fntoa"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">inet_ntoa</strong> <code class="def-code-arguments">(struct in_addr <var class="var">addr</var>)</code><a class="copiable-link" href="#index-inet_005fntoa"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe race
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function converts the IPv4 Internet host address <var class="var">addr</var> to a
string in the standard numbers-and-dots notation.  The return value is
a pointer into a statically-allocated buffer.  Subsequent calls will
overwrite the same buffer, so you should copy the string if you need
to save it.
</p>
<p>In multi-threaded programs each thread has its own statically-allocated
buffer.  But still subsequent calls of <code class="code">inet_ntoa</code> in the same
thread will overwrite the result of the last call.
</p>
<p>Instead of <code class="code">inet_ntoa</code> the newer function <code class="code">inet_ntop</code> which is
described below should be used since it handles both IPv4 and IPv6
addresses.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-inet_005fmakeaddr"><span class="category-def">Function: </span><span><code class="def-type">struct in_addr</code> <strong class="def-name">inet_makeaddr</strong> <code class="def-code-arguments">(uint32_t <var class="var">net</var>, uint32_t <var class="var">local</var>)</code><a class="copiable-link" href="#index-inet_005fmakeaddr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function makes an IPv4 Internet host address by combining the network
number <var class="var">net</var> with the local-address-within-network number
<var class="var">local</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-inet_005flnaof"><span class="category-def">Function: </span><span><code class="def-type">uint32_t</code> <strong class="def-name">inet_lnaof</strong> <code class="def-code-arguments">(struct in_addr <var class="var">addr</var>)</code><a class="copiable-link" href="#index-inet_005flnaof"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the local-address-within-network part of the
Internet host address <var class="var">addr</var>.
</p>
<p>The function works only with traditional IPv4 class A, B and C network
types.  It doesn&rsquo;t work with classless addresses and shouldn&rsquo;t be used
anymore.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-inet_005fnetof"><span class="category-def">Function: </span><span><code class="def-type">uint32_t</code> <strong class="def-name">inet_netof</strong> <code class="def-code-arguments">(struct in_addr <var class="var">addr</var>)</code><a class="copiable-link" href="#index-inet_005fnetof"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the network number part of the Internet host
address <var class="var">addr</var>.
</p>
<p>The function works only with traditional IPv4 class A, B and C network
types.  It doesn&rsquo;t work with classless addresses and shouldn&rsquo;t be used
anymore.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-inet_005fpton"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">inet_pton</strong> <code class="def-code-arguments">(int <var class="var">af</var>, const char *<var class="var">cp</var>, void *<var class="var">buf</var>)</code><a class="copiable-link" href="#index-inet_005fpton"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function converts an Internet address (either IPv4 or IPv6) from
presentation (textual) to network (binary) format.  <var class="var">af</var> should be
either <code class="code">AF_INET</code> or <code class="code">AF_INET6</code>, as appropriate for the type of
address being converted.  <var class="var">cp</var> is a pointer to the input string, and
<var class="var">buf</var> is a pointer to a buffer for the result.  It is the caller&rsquo;s
responsibility to make sure the buffer is large enough.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-inet_005fntop"><span class="category-def">Function: </span><span><code class="def-type">const char *</code> <strong class="def-name">inet_ntop</strong> <code class="def-code-arguments">(int <var class="var">af</var>, const void *<var class="var">cp</var>, char *<var class="var">buf</var>, socklen_t <var class="var">len</var>)</code><a class="copiable-link" href="#index-inet_005fntop"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function converts an Internet address (either IPv4 or IPv6) from
network (binary) to presentation (textual) form.  <var class="var">af</var> should be
either <code class="code">AF_INET</code> or <code class="code">AF_INET6</code>, as appropriate.  <var class="var">cp</var> is a
pointer to the address to be converted.  <var class="var">buf</var> should be a pointer
to a buffer to hold the result, and <var class="var">len</var> is the length of this
buffer.  The return value from the function will be this buffer address.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Host-Names.html">Host Names</a>, Previous: <a href="Host-Address-Data-Type.html">Host Address Data Type</a>, Up: <a href="Host-Addresses.html">Host Addresses</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
