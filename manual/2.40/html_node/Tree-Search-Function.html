<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Tree Search Function (The GNU C Library)</title>

<meta name="description" content="Tree Search Function (The GNU C Library)">
<meta name="keywords" content="Tree Search Function (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Searching-and-Sorting.html" rel="up" title="Searching and Sorting">
<link href="Hash-Search-Function.html" rel="prev" title="Hash Search Function">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Tree-Search-Function">
<div class="nav-panel">
<p>
Previous: <a href="Hash-Search-Function.html" accesskey="p" rel="prev">The <code class="code">hsearch</code> function.</a>, Up: <a href="Searching-and-Sorting.html" accesskey="u" rel="up">Searching and Sorting</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="The-tsearch-function_002e"><span>9.6 The <code class="code">tsearch</code> function.<a class="copiable-link" href="#The-tsearch-function_002e"> &para;</a></span></h3>

<p>Another common form to organize data for efficient search is to use
trees.  The <code class="code">tsearch</code> function family provides a nice interface to
functions to organize possibly large amounts of data by providing a mean
access time proportional to the logarithm of the number of elements.
The GNU C Library implementation even guarantees that this bound is
never exceeded even for input data which cause problems for simple
binary tree implementations.
</p>
<p>The functions described in the chapter are all described in the System&nbsp;V<!-- /@w --> and X/Open specifications and are therefore quite portable.
</p>
<p>In contrast to the <code class="code">hsearch</code> functions the <code class="code">tsearch</code> functions
can be used with arbitrary data and not only zero-terminated strings.
</p>
<p>The <code class="code">tsearch</code> functions have the advantage that no function to
initialize data structures is necessary.  A simple pointer of type
<code class="code">void *</code> initialized to <code class="code">NULL</code> is a valid tree and can be
extended or searched.  The prototypes for these functions can be found
in the header file <samp class="file">search.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tsearch"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">tsearch</strong> <code class="def-code-arguments">(const void *<var class="var">key</var>, void **<var class="var">rootp</var>, comparison_fn_t <var class="var">compar</var>)</code><a class="copiable-link" href="#index-tsearch"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:rootp
| AS-Unsafe heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">tsearch</code> function searches in the tree pointed to by
<code class="code">*<var class="var">rootp</var></code> for an element matching <var class="var">key</var>.  The function
pointed to by <var class="var">compar</var> is used to determine whether two elements
match.  See <a class="xref" href="Comparison-Functions.html">Defining the Comparison Function</a>, for a specification of the functions
which can be used for the <var class="var">compar</var> parameter.
</p>
<p>If the tree does not contain a matching entry the <var class="var">key</var> value will
be added to the tree.  <code class="code">tsearch</code> does not make a copy of the object
pointed to by <var class="var">key</var> (how could it since the size is unknown).
Instead it adds a reference to this object which means the object must
be available as long as the tree data structure is used.
</p>
<p>The tree is represented by a pointer to a pointer since it is sometimes
necessary to change the root node of the tree.  So it must not be
assumed that the variable pointed to by <var class="var">rootp</var> has the same value
after the call.  This also shows that it is not safe to call the
<code class="code">tsearch</code> function more than once at the same time using the same
tree.  It is no problem to run it more than once at a time on different
trees.
</p>
<p>The return value is a pointer to the matching element in the tree.  If a
new element was created the pointer points to the new data (which is in
fact <var class="var">key</var>).  If an entry had to be created and the program ran out
of space <code class="code">NULL</code> is returned.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tfind"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">tfind</strong> <code class="def-code-arguments">(const void *<var class="var">key</var>, void *const *<var class="var">rootp</var>, comparison_fn_t <var class="var">compar</var>)</code><a class="copiable-link" href="#index-tfind"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:rootp
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">tfind</code> function is similar to the <code class="code">tsearch</code> function.  It
locates an element matching the one pointed to by <var class="var">key</var> and returns
a pointer to this element.  But if no matching element is available no
new element is entered (note that the <var class="var">rootp</var> parameter points to a
constant pointer).  Instead the function returns <code class="code">NULL</code>.
</p></dd></dl>

<p>Another advantage of the <code class="code">tsearch</code> functions in contrast to the
<code class="code">hsearch</code> functions is that there is an easy way to remove
elements.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tdelete"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">tdelete</strong> <code class="def-code-arguments">(const void *<var class="var">key</var>, void **<var class="var">rootp</var>, comparison_fn_t <var class="var">compar</var>)</code><a class="copiable-link" href="#index-tdelete"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:rootp
| AS-Unsafe heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>To remove a specific element matching <var class="var">key</var> from the tree
<code class="code">tdelete</code> can be used.  It locates the matching element using the
same method as <code class="code">tfind</code>.  The corresponding element is then removed
and a pointer to the parent of the deleted node is returned by the
function.  If there is no matching entry in the tree nothing can be
deleted and the function returns <code class="code">NULL</code>.  If the root of the tree
is deleted <code class="code">tdelete</code> returns some unspecified value not equal to
<code class="code">NULL</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tdestroy"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">tdestroy</strong> <code class="def-code-arguments">(void *<var class="var">vroot</var>, __free_fn_t <var class="var">freefct</var>)</code><a class="copiable-link" href="#index-tdestroy"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>If the complete search tree has to be removed one can use
<code class="code">tdestroy</code>.  It frees all resources allocated by the <code class="code">tsearch</code>
functions to generate the tree pointed to by <var class="var">vroot</var>.
</p>
<p>For the data in each tree node the function <var class="var">freefct</var> is called.
The pointer to the data is passed as the argument to the function.  If
no such work is necessary <var class="var">freefct</var> must point to a function doing
nothing.  It is called in any case.
</p>
<p>This function is a GNU extension and not covered by the System&nbsp;V<!-- /@w --> or
X/Open specifications.
</p></dd></dl>

<p>In addition to the functions to create and destroy the tree data
structure, there is another function which allows you to apply a
function to all elements of the tree.  The function must have this type:
</p>
<div class="example smallexample">
<pre class="example-preformatted">void __action_fn_t (const void *nodep, VISIT value, int level);
</pre></div>

<p>The <var class="var">nodep</var> is the data value of the current node (once given as the
<var class="var">key</var> argument to <code class="code">tsearch</code>).  <var class="var">level</var> is a numeric value
which corresponds to the depth of the current node in the tree.  The
root node has the depth <em class="math">0</em> and its children have a depth of
<em class="math">1</em> and so on.  The <code class="code">VISIT</code> type is an enumeration type.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-VISIT"><span class="category-def">Data Type: </span><span><strong class="def-name">VISIT</strong><a class="copiable-link" href="#index-VISIT"> &para;</a></span></dt>
<dd><p>The <code class="code">VISIT</code> value indicates the status of the current node in the
tree and how the function is called.  The status of a node is either
&lsquo;leaf&rsquo; or &lsquo;internal node&rsquo;.  For each leaf node the function is called
exactly once, for each internal node it is called three times: before
the first child is processed, after the first child is processed and
after both children are processed.  This makes it possible to handle all
three methods of tree traversal (or even a combination of them).
</p>
<dl class="vtable">
<dt><a id="index-preorder"></a><span><code class="code">preorder</code><a class="copiable-link" href="#index-preorder"> &para;</a></span></dt>
<dd><p>The current node is an internal node and the function is called before
the first child was processed.
</p></dd>
<dt><a id="index-postorder"></a><span><code class="code">postorder</code><a class="copiable-link" href="#index-postorder"> &para;</a></span></dt>
<dd><p>The current node is an internal node and the function is called after
the first child was processed.
</p></dd>
<dt><a id="index-endorder"></a><span><code class="code">endorder</code><a class="copiable-link" href="#index-endorder"> &para;</a></span></dt>
<dd><p>The current node is an internal node and the function is called after
the second child was processed.
</p></dd>
<dt><a id="index-leaf"></a><span><code class="code">leaf</code><a class="copiable-link" href="#index-leaf"> &para;</a></span></dt>
<dd><p>The current node is a leaf.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-twalk"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">twalk</strong> <code class="def-code-arguments">(const void *<var class="var">root</var>, __action_fn_t <var class="var">action</var>)</code><a class="copiable-link" href="#index-twalk"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:root
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>For each node in the tree with a node pointed to by <var class="var">root</var>, the
<code class="code">twalk</code> function calls the function provided by the parameter
<var class="var">action</var>.  For leaf nodes the function is called exactly once with
<var class="var">value</var> set to <code class="code">leaf</code>.  For internal nodes the function is
called three times, setting the <var class="var">value</var> parameter or <var class="var">action</var> to
the appropriate value.  The <var class="var">level</var> argument for the <var class="var">action</var>
function is computed while descending the tree by increasing the value
by one for each descent to a child, starting with the value <em class="math">0</em> for
the root node.
</p>
<p>Since the functions used for the <var class="var">action</var> parameter to <code class="code">twalk</code>
must not modify the tree data, it is safe to run <code class="code">twalk</code> in more
than one thread at the same time, working on the same tree.  It is also
safe to call <code class="code">tfind</code> in parallel.  Functions which modify the tree
must not be used, otherwise the behavior is undefined.  However, it is
difficult to pass data external to the tree to the callback function
without resorting to global variables (and thread safety issues), so
see the <code class="code">twalk_r</code> function below.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-twalk_005fr"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">twalk_r</strong> <code class="def-code-arguments">(const void *<var class="var">root</var>, void (*<var class="var">action</var>) (const void *<var class="var">key</var>, VISIT <var class="var">which</var>, void *<var class="var">closure</var>), void *<var class="var">closure</var>)</code><a class="copiable-link" href="#index-twalk_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:root
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>For each node in the tree with a node pointed to by <var class="var">root</var>, the
<code class="code">twalk_r</code> function calls the function provided by the parameter
<var class="var">action</var>.  For leaf nodes the function is called exactly once with
<var class="var">which</var> set to <code class="code">leaf</code>.  For internal nodes the function is
called three times, setting the <var class="var">which</var> parameter of <var class="var">action</var> to
the appropriate value.  The <var class="var">closure</var> parameter is passed down to
each call of the <var class="var">action</var> function, unmodified.
</p>
<p>It is possible to implement the <code class="code">twalk</code> function on top of the
<code class="code">twalk_r</code> function, which is why there is no separate level
parameter.
</p>
<div class="example smallexample">
<pre class="example-preformatted">

#include &lt;search.h&gt;

struct twalk_with_twalk_r_closure
{
  void (*action) (const void *, VISIT, int);
  int depth;
};

static void
twalk_with_twalk_r_action (const void *nodep, VISIT which, void *closure0)
{
  struct twalk_with_twalk_r_closure *closure = closure0;

  switch (which)
    {
    case leaf:
      closure-&gt;action (nodep, which, closure-&gt;depth);
      break;
    case preorder:
      closure-&gt;action (nodep, which, closure-&gt;depth);
      ++closure-&gt;depth;
      break;
    case postorder:
      /* <span class="r">The preorder action incremented the depth.</span> */
      closure-&gt;action (nodep, which, closure-&gt;depth - 1);
      break;
    case endorder:
      --closure-&gt;depth;
      closure-&gt;action (nodep, which, closure-&gt;depth);
      break;
    }
}

void
twalk (const void *root, void (*action) (const void *, VISIT, int))
{
  struct twalk_with_twalk_r_closure closure = { action, 0 };
  twalk_r (root, twalk_with_twalk_r_action, &amp;closure);
}
</pre></div>
</dd></dl>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Hash-Search-Function.html">The <code class="code">hsearch</code> function.</a>, Up: <a href="Searching-and-Sorting.html">Searching and Sorting</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
