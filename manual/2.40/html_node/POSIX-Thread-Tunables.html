<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>POSIX Thread Tunables (The GNU C Library)</title>

<meta name="description" content="POSIX Thread Tunables (The GNU C Library)">
<meta name="keywords" content="POSIX Thread Tunables (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Tunables.html" rel="up" title="Tunables">
<link href="Hardware-Capability-Tunables.html" rel="next" title="Hardware Capability Tunables">
<link href="Elision-Tunables.html" rel="prev" title="Elision Tunables">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="POSIX-Thread-Tunables">
<div class="nav-panel">
<p>
Next: <a href="Hardware-Capability-Tunables.html" accesskey="n" rel="next">Hardware Capability Tunables</a>, Previous: <a href="Elision-Tunables.html" accesskey="p" rel="prev">Elision Tunables</a>, Up: <a href="Tunables.html" accesskey="u" rel="up">Tunables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="POSIX-Thread-Tunables-1"><span>39.5 POSIX Thread Tunables<a class="copiable-link" href="#POSIX-Thread-Tunables-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-pthread-mutex-tunables"></a>
<a class="index-entry-id" id="index-thread-mutex-tunables"></a>
<a class="index-entry-id" id="index-mutex-tunables"></a>
<a class="index-entry-id" id="index-tunables-thread-mutex"></a>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002epthread"><span class="category-def">Tunable namespace: </span><span><strong class="def-name">glibc.pthread</strong><a class="copiable-link" href="#index-glibc_002epthread"> &para;</a></span></dt>
<dd><p>The behavior of POSIX threads can be tuned to gain performance improvements
according to specific hardware capabilities and workload characteristics by
setting the following tunables in the <code class="code">pthread</code> namespace:
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002epthread_002emutex_005fspin_005fcount"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.pthread.mutex_spin_count</strong><a class="copiable-link" href="#index-glibc_002epthread_002emutex_005fspin_005fcount"> &para;</a></span></dt>
<dd><p>The <code class="code">glibc.pthread.mutex_spin_count</code> tunable sets the maximum number of times
a thread should spin on the lock before calling into the kernel to block.
Adaptive spin is used for mutexes initialized with the
<code class="code">PTHREAD_MUTEX_ADAPTIVE_NP</code> GNU extension.  It affects both
<code class="code">pthread_mutex_lock</code> and <code class="code">pthread_mutex_timedlock</code>.
</p>
<p>The thread spins until either the maximum spin count is reached or the lock
is acquired.
</p>
<p>The default value of this tunable is &lsquo;<samp class="samp">100</samp>&rsquo;.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002epthread_002estack_005fcache_005fsize"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.pthread.stack_cache_size</strong><a class="copiable-link" href="#index-glibc_002epthread_002estack_005fcache_005fsize"> &para;</a></span></dt>
<dd><p>This tunable configures the maximum size of the stack cache.  Once the
stack cache exceeds this size, unused thread stacks are returned to
the kernel, to bring the cache size below this limit.
</p>
<p>The value is measured in bytes.  The default is &lsquo;<samp class="samp">41943040</samp>&rsquo;
(forty mibibytes).
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002epthread_002erseq"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.pthread.rseq</strong><a class="copiable-link" href="#index-glibc_002epthread_002erseq"> &para;</a></span></dt>
<dd><p>The <code class="code">glibc.pthread.rseq</code> tunable can be set to &lsquo;<samp class="samp">0</samp>&rsquo;, to disable
restartable sequences support in the GNU C Library.  This enables applications
to perform direct restartable sequence registration with the kernel.
The default is &lsquo;<samp class="samp">1</samp>&rsquo;, which means that the GNU C Library performs
registration on behalf of the application.
</p>
<p>Restartable sequences are a Linux-specific extension.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-glibc_002epthread_002estack_005fhugetlb"><span class="category-def">Tunable: </span><span><strong class="def-name">glibc.pthread.stack_hugetlb</strong><a class="copiable-link" href="#index-glibc_002epthread_002estack_005fhugetlb"> &para;</a></span></dt>
<dd><p>This tunable controls whether to use Huge Pages in the stacks created by
<code class="code">pthread_create</code>.  This tunable only affects the stacks created by
the GNU C Library, it has no effect on stack assigned with
<code class="code">pthread_attr_setstack</code>.
</p>
<p>The default is &lsquo;<samp class="samp">1</samp>&rsquo; where the system default value is used.  Setting
its value to <code class="code">0</code> enables the use of <code class="code">madvise</code> with
<code class="code">MADV_NOHUGEPAGE</code> after stack creation with <code class="code">mmap</code>.
</p>
<p>This is a memory utilization optimization, since internal glibc setup of either
the thread descriptor and the guard page might force the kernel to move the
thread stack originally backup by Huge Pages to default pages.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Hardware-Capability-Tunables.html">Hardware Capability Tunables</a>, Previous: <a href="Elision-Tunables.html">Elision Tunables</a>, Up: <a href="Tunables.html">Tunables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
