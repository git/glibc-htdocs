<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Manipulating the Database (The GNU C Library)</title>

<meta name="description" content="Manipulating the Database (The GNU C Library)">
<meta name="keywords" content="Manipulating the Database (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="User-Accounting-Database.html" rel="up" title="User Accounting Database">
<link href="XPG-Functions.html" rel="next" title="XPG Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Manipulating-the-Database">
<div class="nav-panel">
<p>
Next: <a href="XPG-Functions.html" accesskey="n" rel="next">XPG User Accounting Database Functions</a>, Up: <a href="User-Accounting-Database.html" accesskey="u" rel="up">The User Accounting Database</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Manipulating-the-User-Accounting-Database"><span>31.12.1 Manipulating the User Accounting Database<a class="copiable-link" href="#Manipulating-the-User-Accounting-Database"> &para;</a></span></h4>

<p>These functions and the corresponding data structures are declared in
the header file <samp class="file">utmp.h</samp>.
<a class="index-entry-id" id="index-utmp_002eh"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-exit_005fstatus"><span class="category-def">Data Type: </span><span><strong class="def-name">struct exit_status</strong><a class="copiable-link" href="#index-struct-exit_005fstatus"> &para;</a></span></dt>
<dd>
<p>The <code class="code">exit_status</code> data structure is used to hold information about
the exit status of processes marked as <code class="code">DEAD_PROCESS</code> in the user
accounting database.
</p>
<dl class="table">
<dt><code class="code">short int e_termination</code></dt>
<dd><p>The exit status of the process.
</p>
</dd>
<dt><code class="code">short int e_exit</code></dt>
<dd><p>The exit status of the process.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-struct-utmp"><span class="category-def">Data Type: </span><span><strong class="def-name">struct utmp</strong><a class="copiable-link" href="#index-struct-utmp"> &para;</a></span></dt>
<dd><p>The <code class="code">utmp</code> data structure is used to hold information about entries
in the user accounting database.  On GNU systems it has the following
members:
</p>
<dl class="table">
<dt><code class="code">short int ut_type</code></dt>
<dd><p>Specifies the type of login; one of <code class="code">EMPTY</code>, <code class="code">RUN_LVL</code>,
<code class="code">BOOT_TIME</code>, <code class="code">OLD_TIME</code>, <code class="code">NEW_TIME</code>, <code class="code">INIT_PROCESS</code>,
<code class="code">LOGIN_PROCESS</code>, <code class="code">USER_PROCESS</code>, <code class="code">DEAD_PROCESS</code> or
<code class="code">ACCOUNTING</code>.
</p>
</dd>
<dt><code class="code">pid_t ut_pid</code></dt>
<dd><p>The process ID number of the login process.
</p>
</dd>
<dt><code class="code">char ut_line[]</code></dt>
<dd><p>The device name of the tty (without <samp class="file">/dev/</samp>).
</p>
</dd>
<dt><code class="code">char ut_id[]</code></dt>
<dd><p>The inittab ID of the process.
</p>
</dd>
<dt><code class="code">char ut_user[]</code></dt>
<dd><p>The user&rsquo;s login name.
</p>
</dd>
<dt><code class="code">char ut_host[]</code></dt>
<dd><p>The name of the host from which the user logged in.
</p>
</dd>
<dt><code class="code">struct exit_status ut_exit</code></dt>
<dd><p>The exit status of a process marked as <code class="code">DEAD_PROCESS</code>.
</p>
</dd>
<dt><code class="code">long ut_session</code></dt>
<dd><p>The Session ID, used for windowing.
</p>
</dd>
<dt><code class="code">struct timeval ut_tv</code></dt>
<dd><p>Time the entry was made.  For entries of type <code class="code">OLD_TIME</code> this is
the time when the system clock changed, and for entries of type
<code class="code">NEW_TIME</code> this is the time the system clock was set to.
</p>
</dd>
<dt><code class="code">int32_t ut_addr_v6[4]</code></dt>
<dd><p>The Internet address of a remote host.
</p></dd>
</dl>
</dd></dl>

<p>The <code class="code">ut_type</code>, <code class="code">ut_pid</code>, <code class="code">ut_id</code>, <code class="code">ut_tv</code>, and
<code class="code">ut_host</code> fields are not available on all systems.  Portable
applications therefore should be prepared for these situations.  To help
do this the <samp class="file">utmp.h</samp> header provides macros
<code class="code">_HAVE_UT_TYPE</code>, <code class="code">_HAVE_UT_PID</code>, <code class="code">_HAVE_UT_ID</code>,
<code class="code">_HAVE_UT_TV</code>, and <code class="code">_HAVE_UT_HOST</code> if the respective field is
available.  The programmer can handle the situations by using
<code class="code">#ifdef</code> in the program code.
</p>
<p>The following macros are defined for use as values for the
<code class="code">ut_type</code> member of the <code class="code">utmp</code> structure.  The values are
integer constants.
</p>
<dl class="vtable">
<dt><a id="index-EMPTY"></a><span><code class="code">EMPTY</code><a class="copiable-link" href="#index-EMPTY"> &para;</a></span></dt>
<dd>
<p>This macro is used to indicate that the entry contains no valid user
accounting information.
</p>
</dd>
<dt><a id="index-RUN_005fLVL"></a><span><code class="code">RUN_LVL</code><a class="copiable-link" href="#index-RUN_005fLVL"> &para;</a></span></dt>
<dd>
<p>This macro is used to identify the system&rsquo;s runlevel.
</p>
</dd>
<dt><a id="index-BOOT_005fTIME"></a><span><code class="code">BOOT_TIME</code><a class="copiable-link" href="#index-BOOT_005fTIME"> &para;</a></span></dt>
<dd>
<p>This macro is used to identify the time of system boot.
</p>
</dd>
<dt><a id="index-OLD_005fTIME"></a><span><code class="code">OLD_TIME</code><a class="copiable-link" href="#index-OLD_005fTIME"> &para;</a></span></dt>
<dd>
<p>This macro is used to identify the time when the system clock changed.
</p>
</dd>
<dt><a id="index-NEW_005fTIME"></a><span><code class="code">NEW_TIME</code><a class="copiable-link" href="#index-NEW_005fTIME"> &para;</a></span></dt>
<dd>
<p>This macro is used to identify the time after the system clock changed.
</p>
</dd>
<dt><a id="index-INIT_005fPROCESS"></a><span><code class="code">INIT_PROCESS</code><a class="copiable-link" href="#index-INIT_005fPROCESS"> &para;</a></span></dt>
<dd>
<p>This macro is used to identify a process spawned by the init process.
</p>
</dd>
<dt><a id="index-LOGIN_005fPROCESS"></a><span><code class="code">LOGIN_PROCESS</code><a class="copiable-link" href="#index-LOGIN_005fPROCESS"> &para;</a></span></dt>
<dd>
<p>This macro is used to identify the session leader of a logged in user.
</p>
</dd>
<dt><a id="index-USER_005fPROCESS"></a><span><code class="code">USER_PROCESS</code><a class="copiable-link" href="#index-USER_005fPROCESS"> &para;</a></span></dt>
<dd>
<p>This macro is used to identify a user process.
</p>
</dd>
<dt><a id="index-DEAD_005fPROCESS"></a><span><code class="code">DEAD_PROCESS</code><a class="copiable-link" href="#index-DEAD_005fPROCESS"> &para;</a></span></dt>
<dd>
<p>This macro is used to identify a terminated process.
</p>
</dd>
<dt><a id="index-ACCOUNTING"></a><span><code class="code">ACCOUNTING</code><a class="copiable-link" href="#index-ACCOUNTING"> &para;</a></span></dt>
<dd>
<p>???
</p></dd>
</dl>

<p>The size of the <code class="code">ut_line</code>, <code class="code">ut_id</code>, <code class="code">ut_user</code> and
<code class="code">ut_host</code> arrays can be found using the <code class="code">sizeof</code> operator.
</p>
<p>Many older systems have, instead of an <code class="code">ut_tv</code> member, an
<code class="code">ut_time</code> member, usually of type <code class="code">time_t</code>, for representing
the time associated with the entry.  Therefore, for backwards
compatibility only, <samp class="file">utmp.h</samp> defines <code class="code">ut_time</code> as an alias for
<code class="code">ut_tv.tv_sec</code>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setutent"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">setutent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-setutent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:utent
| AS-Unsafe lock
| AC-Unsafe lock fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function opens the user accounting database to begin scanning it.
You can then call <code class="code">getutent</code>, <code class="code">getutid</code> or <code class="code">getutline</code> to
read entries and <code class="code">pututline</code> to write entries.
</p>
<p>If the database is already open, it resets the input to the beginning of
the database.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getutent"><span class="category-def">Function: </span><span><code class="def-type">struct utmp *</code> <strong class="def-name">getutent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-getutent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe init race:utent race:utentbuf sig:ALRM timer
| AS-Unsafe heap lock
| AC-Unsafe lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">getutent</code> function reads the next entry from the user
accounting database.  It returns a pointer to the entry, which is
statically allocated and may be overwritten by subsequent calls to
<code class="code">getutent</code>.  You must copy the contents of the structure if you
wish to save the information or you can use the <code class="code">getutent_r</code>
function which stores the data in a user-provided buffer.
</p>
<p>A null pointer is returned in case no further entry is available.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-endutent"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">endutent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-endutent"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:utent
| AS-Unsafe lock
| AC-Unsafe lock fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function closes the user accounting database.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getutid"><span class="category-def">Function: </span><span><code class="def-type">struct utmp *</code> <strong class="def-name">getutid</strong> <code class="def-code-arguments">(const struct utmp *<var class="var">id</var>)</code><a class="copiable-link" href="#index-getutid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe init race:utent sig:ALRM timer
| AS-Unsafe lock heap
| AC-Unsafe lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function searches forward from the current point in the database
for an entry that matches <var class="var">id</var>.  If the <code class="code">ut_type</code> member of the
<var class="var">id</var> structure is one of <code class="code">RUN_LVL</code>, <code class="code">BOOT_TIME</code>,
<code class="code">OLD_TIME</code> or <code class="code">NEW_TIME</code> the entries match if the
<code class="code">ut_type</code> members are identical.  If the <code class="code">ut_type</code> member of
the <var class="var">id</var> structure is <code class="code">INIT_PROCESS</code>, <code class="code">LOGIN_PROCESS</code>,
<code class="code">USER_PROCESS</code> or <code class="code">DEAD_PROCESS</code>, the entries match if the
<code class="code">ut_type</code> member of the entry read from the database is one of
these four, and the <code class="code">ut_id</code> members match.  However if the
<code class="code">ut_id</code> member of either the <var class="var">id</var> structure or the entry read
from the database is empty it checks if the <code class="code">ut_line</code> members match
instead.  If a matching entry is found, <code class="code">getutid</code> returns a pointer
to the entry, which is statically allocated, and may be overwritten by a
subsequent call to <code class="code">getutent</code>, <code class="code">getutid</code> or <code class="code">getutline</code>.
You must copy the contents of the structure if you wish to save the
information.
</p>
<p>A null pointer is returned in case the end of the database is reached
without a match.
</p>
<p>The <code class="code">getutid</code> function may cache the last read entry.  Therefore,
if you are using <code class="code">getutid</code> to search for multiple occurrences, it
is necessary to zero out the static data after each call.  Otherwise
<code class="code">getutid</code> could just return a pointer to the same entry over and
over again.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getutline"><span class="category-def">Function: </span><span><code class="def-type">struct utmp *</code> <strong class="def-name">getutline</strong> <code class="def-code-arguments">(const struct utmp *<var class="var">line</var>)</code><a class="copiable-link" href="#index-getutline"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe init race:utent sig:ALRM timer
| AS-Unsafe heap lock
| AC-Unsafe lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function searches forward from the current point in the database
until it finds an entry whose <code class="code">ut_type</code> value is
<code class="code">LOGIN_PROCESS</code> or <code class="code">USER_PROCESS</code>, and whose <code class="code">ut_line</code>
member matches the <code class="code">ut_line</code> member of the <var class="var">line</var> structure.
If it finds such an entry, it returns a pointer to the entry which is
statically allocated, and may be overwritten by a subsequent call to
<code class="code">getutent</code>, <code class="code">getutid</code> or <code class="code">getutline</code>.  You must copy the
contents of the structure if you wish to save the information.
</p>
<p>A null pointer is returned in case the end of the database is reached
without a match.
</p>
<p>The <code class="code">getutline</code> function may cache the last read entry.  Therefore
if you are using <code class="code">getutline</code> to search for multiple occurrences, it
is necessary to zero out the static data after each call.  Otherwise
<code class="code">getutline</code> could just return a pointer to the same entry over and
over again.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pututline"><span class="category-def">Function: </span><span><code class="def-type">struct utmp *</code> <strong class="def-name">pututline</strong> <code class="def-code-arguments">(const struct utmp *<var class="var">utmp</var>)</code><a class="copiable-link" href="#index-pututline"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:utent sig:ALRM timer
| AS-Unsafe lock
| AC-Unsafe lock fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">pututline</code> function inserts the entry <code class="code">*<var class="var">utmp</var></code> at
the appropriate place in the user accounting database.  If it finds that
it is not already at the correct place in the database, it uses
<code class="code">getutid</code> to search for the position to insert the entry, however
this will not modify the static structure returned by <code class="code">getutent</code>,
<code class="code">getutid</code> and <code class="code">getutline</code>.  If this search fails, the entry
is appended to the database.
</p>
<p>The <code class="code">pututline</code> function returns a pointer to a copy of the entry
inserted in the user accounting database, or a null pointer if the entry
could not be added.  The following <code class="code">errno</code> error conditions are
defined for this function:
</p>
<dl class="table">
<dt><code class="code">EPERM</code></dt>
<dd><p>The process does not have the appropriate privileges; you cannot modify
the user accounting database.
</p></dd>
</dl>
</dd></dl>

<p>All the <code class="code">get*</code> functions mentioned before store the information
they return in a static buffer.  This can be a problem in multi-threaded
programs since the data returned for the request is overwritten by the
return value data in another thread.  Therefore the GNU C Library
provides as extensions three more functions which return the data in a
user-provided buffer.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getutent_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getutent_r</strong> <code class="def-code-arguments">(struct utmp *<var class="var">buffer</var>, struct utmp **<var class="var">result</var>)</code><a class="copiable-link" href="#index-getutent_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:utent sig:ALRM timer
| AS-Unsafe lock
| AC-Unsafe lock fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getutent_r</code> is equivalent to the <code class="code">getutent</code> function.  It
returns the next entry from the database.  But instead of storing the
information in a static buffer it stores it in the buffer pointed to by
the parameter <var class="var">buffer</var>.
</p>
<p>If the call was successful, the function returns <code class="code">0</code> and the
pointer variable pointed to by the parameter <var class="var">result</var> contains a
pointer to the buffer which contains the result (this is most probably
the same value as <var class="var">buffer</var>).  If something went wrong during the
execution of <code class="code">getutent_r</code> the function returns <code class="code">-1</code>.
</p>
<p>This function is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getutid_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getutid_r</strong> <code class="def-code-arguments">(const struct utmp *<var class="var">id</var>, struct utmp *<var class="var">buffer</var>, struct utmp **<var class="var">result</var>)</code><a class="copiable-link" href="#index-getutid_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:utent sig:ALRM timer
| AS-Unsafe lock
| AC-Unsafe lock fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function retrieves just like <code class="code">getutid</code> the next entry matching
the information stored in <var class="var">id</var>.  But the result is stored in the
buffer pointed to by the parameter <var class="var">buffer</var>.
</p>
<p>If successful the function returns <code class="code">0</code> and the pointer variable
pointed to by the parameter <var class="var">result</var> contains a pointer to the
buffer with the result (probably the same as <var class="var">result</var>.  If not
successful the function return <code class="code">-1</code>.
</p>
<p>This function is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getutline_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getutline_r</strong> <code class="def-code-arguments">(const struct utmp *<var class="var">line</var>, struct utmp *<var class="var">buffer</var>, struct utmp **<var class="var">result</var>)</code><a class="copiable-link" href="#index-getutline_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:utent sig:ALRM timer
| AS-Unsafe lock
| AC-Unsafe lock fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function retrieves just like <code class="code">getutline</code> the next entry
matching the information stored in <var class="var">line</var>.  But the result is stored
in the buffer pointed to by the parameter <var class="var">buffer</var>.
</p>
<p>If successful the function returns <code class="code">0</code> and the pointer variable
pointed to by the parameter <var class="var">result</var> contains a pointer to the
buffer with the result (probably the same as <var class="var">result</var>.  If not
successful the function return <code class="code">-1</code>.
</p>
<p>This function is a GNU extension.
</p></dd></dl>


<p>In addition to the user accounting database, most systems keep a number
of similar databases.  For example most systems keep a log file with all
previous logins (usually in <samp class="file">/etc/wtmp</samp> or <samp class="file">/var/log/wtmp</samp>).
</p>
<p>For specifying which database to examine, the following function should
be used.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-utmpname"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">utmpname</strong> <code class="def-code-arguments">(const char *<var class="var">file</var>)</code><a class="copiable-link" href="#index-utmpname"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:utent
| AS-Unsafe lock heap
| AC-Unsafe lock mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">utmpname</code> function changes the name of the database to be
examined to <var class="var">file</var>, and closes any previously opened database.  By
default <code class="code">getutent</code>, <code class="code">getutid</code>, <code class="code">getutline</code> and
<code class="code">pututline</code> read from and write to the user accounting database.
</p>
<p>The following macros are defined for use as the <var class="var">file</var> argument:
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPATH_005fUTMP"><span class="category-def">Macro: </span><span><code class="def-type">char *</code> <strong class="def-name">_PATH_UTMP</strong><a class="copiable-link" href="#index-_005fPATH_005fUTMP"> &para;</a></span></dt>
<dd><p>This macro is used to specify the user accounting database.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fPATH_005fWTMP"><span class="category-def">Macro: </span><span><code class="def-type">char *</code> <strong class="def-name">_PATH_WTMP</strong><a class="copiable-link" href="#index-_005fPATH_005fWTMP"> &para;</a></span></dt>
<dd><p>This macro is used to specify the user accounting log file.
</p></dd></dl>

<p>The <code class="code">utmpname</code> function returns a value of <code class="code">0</code> if the new name
was successfully stored, and a value of <code class="code">-1</code> to indicate an error.
Note that <code class="code">utmpname</code> does not try to open the database, and that
therefore the return value does not say anything about whether the
database can be successfully opened.
</p></dd></dl>

<p>Specially for maintaining log-like databases the GNU C Library provides
the following function:
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-updwtmp"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">updwtmp</strong> <code class="def-code-arguments">(const char *<var class="var">wtmp_file</var>, const struct utmp *<var class="var">utmp</var>)</code><a class="copiable-link" href="#index-updwtmp"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe sig:ALRM timer
| AS-Unsafe 
| AC-Unsafe fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">updwtmp</code> function appends the entry *<var class="var">utmp</var> to the
database specified by <var class="var">wtmp_file</var>.  For possible values for the
<var class="var">wtmp_file</var> argument see the <code class="code">utmpname</code> function.
</p></dd></dl>

<p><strong class="strong">Portability Note:</strong> Although many operating systems provide a
subset of these functions, they are not standardized.  There are often
subtle differences in the return types, and there are considerable
differences between the various definitions of <code class="code">struct utmp</code>.  When
programming for the GNU C Library, it is probably best to stick
with the functions described in this section.  If however, you want your
program to be portable, consider using the XPG functions described in
<a class="ref" href="XPG-Functions.html">XPG User Accounting Database Functions</a>, or take a look at the BSD compatible functions in
<a class="ref" href="Logging-In-and-Out.html">Logging In and Out</a>.
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="XPG-Functions.html">XPG User Accounting Database Functions</a>, Up: <a href="User-Accounting-Database.html">The User Accounting Database</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
