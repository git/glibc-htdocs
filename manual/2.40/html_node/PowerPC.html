<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>PowerPC (The GNU C Library)</title>

<meta name="description" content="PowerPC (The GNU C Library)">
<meta name="keywords" content="PowerPC (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Platform.html" rel="up" title="Platform">
<link href="RISC_002dV.html" rel="next" title="RISC-V">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="appendixsec-level-extent" id="PowerPC">
<div class="nav-panel">
<p>
Next: <a href="RISC_002dV.html" accesskey="n" rel="next">RISC-V-specific Facilities</a>, Up: <a href="Platform.html" accesskey="u" rel="up">Platform-specific facilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="appendixsec" id="PowerPC_002dspecific-Facilities"><span>E.1 PowerPC-specific Facilities<a class="copiable-link" href="#PowerPC_002dspecific-Facilities"> &para;</a></span></h3>

<p>Facilities specific to PowerPC that are not specific to a particular
operating system are declared in <samp class="file">sys/platform/ppc.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005fppc_005fget_005ftimebase"><span class="category-def">Function: </span><span><code class="def-type">uint64_t</code> <strong class="def-name">__ppc_get_timebase</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005f_005fppc_005fget_005ftimebase"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Read the current value of the Time Base Register.
</p>
<p>The <em class="dfn">Time Base Register</em> is a 64-bit register that stores a monotonically
incremented value updated at a system-dependent frequency that may be
different from the processor frequency.  More information is available in
<cite class="cite">Power ISA 2.06b - Book II - Section 5.2</cite>.
</p>
<p><code class="code">__ppc_get_timebase</code> uses the processor&rsquo;s time base facility directly
without requiring assistance from the operating system, so it is very
efficient.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005fppc_005fget_005ftimebase_005ffreq"><span class="category-def">Function: </span><span><code class="def-type">uint64_t</code> <strong class="def-name">__ppc_get_timebase_freq</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005f_005fppc_005fget_005ftimebase_005ffreq"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Unsafe init
| AS-Unsafe corrupt:init
| AC-Unsafe corrupt:init
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Read the current frequency at which the Time Base Register is updated.
</p>
<p>This frequency is not related to the processor clock or the bus clock.
It is also possible that this frequency is not constant.  More information is
available in <cite class="cite">Power ISA 2.06b - Book II - Section 5.2</cite>.
</p></dd></dl>

<p>The following functions provide hints about the usage of resources that are
shared with other processors.  They can be used, for example, if a program
waiting on a lock intends to divert the shared resources to be used by other
processors.  More information is available in <cite class="cite">Power ISA 2.06b - Book II -
Section 3.2</cite>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005fppc_005fyield"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">__ppc_yield</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005f_005fppc_005fyield"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Provide a hint that performance will probably be improved if shared resources
dedicated to the executing processor are released for use by other processors.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005fppc_005fmdoio"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">__ppc_mdoio</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005f_005fppc_005fmdoio"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Provide a hint that performance will probably be improved if shared resources
dedicated to the executing processor are released until all outstanding storage
accesses to caching-inhibited storage have been completed.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005fppc_005fmdoom"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">__ppc_mdoom</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005f_005fppc_005fmdoom"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Provide a hint that performance will probably be improved if shared resources
dedicated to the executing processor are released until all outstanding storage
accesses to cacheable storage for which the data is not in the cache have been
completed.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005fppc_005fset_005fppr_005fmed"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">__ppc_set_ppr_med</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005f_005fppc_005fset_005fppr_005fmed"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Set the Program Priority Register to medium value (default).
</p>
<p>The <em class="dfn">Program Priority Register</em> (PPR) is a 64-bit register that controls
the program&rsquo;s priority.  By adjusting the PPR value the programmer may
improve system throughput by causing the system resources to be used
more efficiently, especially in contention situations.
The three unprivileged states available are covered by the functions
<code class="code">__ppc_set_ppr_med</code> (medium &ndash; default), <code class="code">__ppc_set_ppc_low</code> (low)
and <code class="code">__ppc_set_ppc_med_low</code> (medium low).  More information
available in <cite class="cite">Power ISA 2.06b - Book II - Section 3.1</cite>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005fppc_005fset_005fppr_005flow"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">__ppc_set_ppr_low</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005f_005fppc_005fset_005fppr_005flow"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Set the Program Priority Register to low value.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005fppc_005fset_005fppr_005fmed_005flow"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">__ppc_set_ppr_med_low</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005f_005fppc_005fset_005fppr_005fmed_005flow"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Set the Program Priority Register to medium low value.
</p></dd></dl>

<p>Power ISA 2.07 extends the priorities that can be set to the Program Priority
Register (PPR).  The following functions implement the new priority levels:
very low and medium high.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005fppc_005fset_005fppr_005fvery_005flow"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">__ppc_set_ppr_very_low</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005f_005fppc_005fset_005fppr_005fvery_005flow"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Set the Program Priority Register to very low value.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005fppc_005fset_005fppr_005fmed_005fhigh"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">__ppc_set_ppr_med_high</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-_005f_005fppc_005fset_005fppr_005fmed_005fhigh"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Set the Program Priority Register to medium high value.  The medium high
priority is privileged and may only be set during certain time intervals by
problem-state programs.  If the program priority is medium high when the time
interval expires or if an attempt is made to set the priority to medium high
when it is not allowed, the priority is set to medium.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="RISC_002dV.html">RISC-V-specific Facilities</a>, Up: <a href="Platform.html">Platform-specific facilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
