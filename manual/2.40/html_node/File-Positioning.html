<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>File Positioning (The GNU C Library)</title>

<meta name="description" content="File Positioning (The GNU C Library)">
<meta name="keywords" content="File Positioning (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="I_002fO-on-Streams.html" rel="up" title="I/O on Streams">
<link href="Portable-Positioning.html" rel="next" title="Portable Positioning">
<link href="Binary-Streams.html" rel="prev" title="Binary Streams">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="File-Positioning">
<div class="nav-panel">
<p>
Next: <a href="Portable-Positioning.html" accesskey="n" rel="next">Portable File-Position Functions</a>, Previous: <a href="Binary-Streams.html" accesskey="p" rel="prev">Text and Binary Streams</a>, Up: <a href="I_002fO-on-Streams.html" accesskey="u" rel="up">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="File-Positioning-1"><span>12.18 File Positioning<a class="copiable-link" href="#File-Positioning-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-file-positioning-on-a-stream"></a>
<a class="index-entry-id" id="index-positioning-a-stream"></a>
<a class="index-entry-id" id="index-seeking-on-a-stream"></a>

<p>The <em class="dfn">file position</em> of a stream describes where in the file the
stream is currently reading or writing.  I/O on the stream advances the
file position through the file.  On GNU systems, the file position is
represented as an integer, which counts the number of bytes from the
beginning of the file.  See <a class="xref" href="File-Position.html">File Position</a>.
</p>
<p>During I/O to an ordinary disk file, you can change the file position
whenever you wish, so as to read or write any portion of the file.  Some
other kinds of files may also permit this.  Files which support changing
the file position are sometimes referred to as <em class="dfn">random-access</em>
files.
</p>
<p>You can use the functions in this section to examine or modify the file
position indicator associated with a stream.  The symbols listed below
are declared in the header file <samp class="file">stdio.h</samp>.
<a class="index-entry-id" id="index-stdio_002eh-9"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ftell"><span class="category-def">Function: </span><span><code class="def-type">long int</code> <strong class="def-name">ftell</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-ftell"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the current file position of the stream
<var class="var">stream</var>.
</p>
<p>This function can fail if the stream doesn&rsquo;t support file positioning,
or if the file position can&rsquo;t be represented in a <code class="code">long int</code>, and
possibly for other reasons as well.  If a failure occurs, a value of
<code class="code">-1</code> is returned.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ftello"><span class="category-def">Function: </span><span><code class="def-type">off_t</code> <strong class="def-name">ftello</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-ftello"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">ftello</code> function is similar to <code class="code">ftell</code>, except that it
returns a value of type <code class="code">off_t</code>.  Systems which support this type
use it to describe all file positions, unlike the POSIX specification
which uses a long int.  The two are not necessarily the same size.
Therefore, using ftell can lead to problems if the implementation is
written on top of a POSIX compliant low-level I/O implementation, and using
<code class="code">ftello</code> is preferable whenever it is available.
</p>
<p>If this function fails it returns <code class="code">(off_t) -1</code>.  This can happen due
to missing support for file positioning or internal errors.  Otherwise
the return value is the current file position.
</p>
<p>The function is an extension defined in the Unix Single Specification
version 2.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit system this function is in fact <code class="code">ftello64</code>.  I.e., the
LFS interface transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ftello64"><span class="category-def">Function: </span><span><code class="def-type">off64_t</code> <strong class="def-name">ftello64</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-ftello64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">ftello</code> with the only difference that
the return value is of type <code class="code">off64_t</code>.  This also requires that the
stream <var class="var">stream</var> was opened using either <code class="code">fopen64</code>,
<code class="code">freopen64</code>, or <code class="code">tmpfile64</code> since otherwise the underlying
file operations to position the file pointer beyond the 2^31
bytes limit might fail.
</p>
<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a 32
bits machine this function is available under the name <code class="code">ftello</code>
and so transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fseek"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fseek</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, long int <var class="var">offset</var>, int <var class="var">whence</var>)</code><a class="copiable-link" href="#index-fseek"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fseek</code> function is used to change the file position of the
stream <var class="var">stream</var>.  The value of <var class="var">whence</var> must be one of the
constants <code class="code">SEEK_SET</code>, <code class="code">SEEK_CUR</code>, or <code class="code">SEEK_END</code>, to
indicate whether the <var class="var">offset</var> is relative to the beginning of the
file, the current file position, or the end of the file, respectively.
</p>
<p>This function returns a value of zero if the operation was successful,
and a nonzero value to indicate failure.  A successful call also clears
the end-of-file indicator of <var class="var">stream</var> and discards any characters
that were &ldquo;pushed back&rdquo; by the use of <code class="code">ungetc</code>.
</p>
<p><code class="code">fseek</code> either flushes any buffered output before setting the file
position or else remembers it so it will be written later in its proper
place in the file.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fseeko"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fseeko</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, off_t <var class="var">offset</var>, int <var class="var">whence</var>)</code><a class="copiable-link" href="#index-fseeko"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">fseek</code> but it corrects a problem with
<code class="code">fseek</code> in a system with POSIX types.  Using a value of type
<code class="code">long int</code> for the offset is not compatible with POSIX.
<code class="code">fseeko</code> uses the correct type <code class="code">off_t</code> for the <var class="var">offset</var>
parameter.
</p>
<p>For this reason it is a good idea to prefer <code class="code">ftello</code> whenever it is
available since its functionality is (if different at all) closer the
underlying definition.
</p>
<p>The functionality and return value are the same as for <code class="code">fseek</code>.
</p>
<p>The function is an extension defined in the Unix Single Specification
version 2.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit system this function is in fact <code class="code">fseeko64</code>.  I.e., the
LFS interface transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fseeko64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fseeko64</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, off64_t <var class="var">offset</var>, int <var class="var">whence</var>)</code><a class="copiable-link" href="#index-fseeko64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">fseeko</code> with the only difference that
the <var class="var">offset</var> parameter is of type <code class="code">off64_t</code>.  This also
requires that the stream <var class="var">stream</var> was opened using either
<code class="code">fopen64</code>, <code class="code">freopen64</code>, or <code class="code">tmpfile64</code> since otherwise
the underlying file operations to position the file pointer beyond the
2^31 bytes limit might fail.
</p>
<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a 32
bits machine this function is available under the name <code class="code">fseeko</code>
and so transparently replaces the old interface.
</p></dd></dl>

<p><strong class="strong">Portability Note:</strong> In non-POSIX systems, <code class="code">ftell</code>,
<code class="code">ftello</code>, <code class="code">fseek</code> and <code class="code">fseeko</code> might work reliably only
on binary streams.  See <a class="xref" href="Binary-Streams.html">Text and Binary Streams</a>.
</p>
<p>The following symbolic constants are defined for use as the <var class="var">whence</var>
argument to <code class="code">fseek</code>.  They are also used with the <code class="code">lseek</code>
function (see <a class="pxref" href="I_002fO-Primitives.html">Input and Output Primitives</a>) and to specify offsets for file locks
(see <a class="pxref" href="Control-Operations.html">Control Operations on Files</a>).
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SEEK_005fSET"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SEEK_SET</strong><a class="copiable-link" href="#index-SEEK_005fSET"> &para;</a></span></dt>
<dd>
<p>This is an integer constant which, when used as the <var class="var">whence</var>
argument to the <code class="code">fseek</code> or <code class="code">fseeko</code> functions, specifies that
the offset provided is relative to the beginning of the file.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SEEK_005fCUR"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SEEK_CUR</strong><a class="copiable-link" href="#index-SEEK_005fCUR"> &para;</a></span></dt>
<dd>
<p>This is an integer constant which, when used as the <var class="var">whence</var>
argument to the <code class="code">fseek</code> or <code class="code">fseeko</code> functions, specifies that
the offset provided is relative to the current file position.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SEEK_005fEND"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SEEK_END</strong><a class="copiable-link" href="#index-SEEK_005fEND"> &para;</a></span></dt>
<dd>
<p>This is an integer constant which, when used as the <var class="var">whence</var>
argument to the <code class="code">fseek</code> or <code class="code">fseeko</code> functions, specifies that
the offset provided is relative to the end of the file.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-rewind"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">rewind</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-rewind"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">rewind</code> function positions the stream <var class="var">stream</var> at the
beginning of the file.  It is equivalent to calling <code class="code">fseek</code> or
<code class="code">fseeko</code> on the <var class="var">stream</var> with an <var class="var">offset</var> argument of
<code class="code">0L</code> and a <var class="var">whence</var> argument of <code class="code">SEEK_SET</code>, except that
the return value is discarded and the error indicator for the stream is
reset.
</p></dd></dl>

<p>These three aliases for the &lsquo;<samp class="samp">SEEK_&hellip;</samp>&rsquo; constants exist for the
sake of compatibility with older BSD systems.  They are defined in two
different header files: <samp class="file">fcntl.h</samp> and <samp class="file">sys/file.h</samp>.
</p>
<dl class="vtable">
<dt><a id="index-L_005fSET"></a><span><code class="code">L_SET</code><a class="copiable-link" href="#index-L_005fSET"> &para;</a></span></dt>
<dd>
<p>An alias for <code class="code">SEEK_SET</code>.
</p>
</dd>
<dt><a id="index-L_005fINCR"></a><span><code class="code">L_INCR</code><a class="copiable-link" href="#index-L_005fINCR"> &para;</a></span></dt>
<dd>
<p>An alias for <code class="code">SEEK_CUR</code>.
</p>
</dd>
<dt><a id="index-L_005fXTND"></a><span><code class="code">L_XTND</code><a class="copiable-link" href="#index-L_005fXTND"> &para;</a></span></dt>
<dd>
<p>An alias for <code class="code">SEEK_END</code>.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Portable-Positioning.html">Portable File-Position Functions</a>, Previous: <a href="Binary-Streams.html">Text and Binary Streams</a>, Up: <a href="I_002fO-on-Streams.html">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
