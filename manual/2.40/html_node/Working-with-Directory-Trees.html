<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Working with Directory Trees (The GNU C Library)</title>

<meta name="description" content="Working with Directory Trees (The GNU C Library)">
<meta name="keywords" content="Working with Directory Trees (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-System-Interface.html" rel="up" title="File System Interface">
<link href="Hard-Links.html" rel="next" title="Hard Links">
<link href="Accessing-Directories.html" rel="prev" title="Accessing Directories">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Working-with-Directory-Trees">
<div class="nav-panel">
<p>
Next: <a href="Hard-Links.html" accesskey="n" rel="next">Hard Links</a>, Previous: <a href="Accessing-Directories.html" accesskey="p" rel="prev">Accessing Directories</a>, Up: <a href="File-System-Interface.html" accesskey="u" rel="up">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Working-with-Directory-Trees-1"><span>14.3 Working with Directory Trees<a class="copiable-link" href="#Working-with-Directory-Trees-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-directory-hierarchy"></a>
<a class="index-entry-id" id="index-hierarchy_002c-directory"></a>
<a class="index-entry-id" id="index-tree_002c-directory"></a>

<p>The functions described so far for handling the files in a directory
have allowed you to either retrieve the information bit by bit, or to
process all the files as a group (see <code class="code">scandir</code>).  Sometimes it is
useful to process whole hierarchies of directories and their contained
files.  The X/Open specification defines two functions to do this.  The
simpler form is derived from an early definition in System&nbsp;V<!-- /@w --> systems
and therefore this function is available on SVID-derived systems.  The
prototypes and required definitions can be found in the <samp class="file">ftw.h</samp>
header.
</p>
<p>There are four functions in this family: <code class="code">ftw</code>, <code class="code">nftw</code> and
their 64-bit counterparts <code class="code">ftw64</code> and <code class="code">nftw64</code>.  These
functions take as one of their arguments a pointer to a callback
function of the appropriate type.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-_005f_005fftw_005ffunc_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">__ftw_func_t</strong><a class="copiable-link" href="#index-_005f_005fftw_005ffunc_005ft"> &para;</a></span></dt>
<dd>

<div class="example smallexample">
<pre class="example-preformatted">int (*) (const char *, const struct stat *, int)
</pre></div>

<p>The type of callback functions given to the <code class="code">ftw</code> function.  The
first parameter points to the file name, the second parameter to an
object of type <code class="code">struct stat</code> which is filled in for the file named
in the first parameter.
</p>
<p>The last parameter is a flag giving more information about the current
file.  It can have the following values:
</p>
<dl class="vtable">
<dt><a id="index-FTW_005fF"></a><span><code class="code">FTW_F</code><a class="copiable-link" href="#index-FTW_005fF"> &para;</a></span></dt>
<dd><p>The item is either a normal file or a file which does not fit into one
of the following categories.  This could be special files, sockets etc.
</p></dd>
<dt><a id="index-FTW_005fD"></a><span><code class="code">FTW_D</code><a class="copiable-link" href="#index-FTW_005fD"> &para;</a></span></dt>
<dd><p>The item is a directory.
</p></dd>
<dt><a id="index-FTW_005fNS"></a><span><code class="code">FTW_NS</code><a class="copiable-link" href="#index-FTW_005fNS"> &para;</a></span></dt>
<dd><p>The <code class="code">stat</code> call failed and so the information pointed to by the
second parameter is invalid.
</p></dd>
<dt><a id="index-FTW_005fDNR"></a><span><code class="code">FTW_DNR</code><a class="copiable-link" href="#index-FTW_005fDNR"> &para;</a></span></dt>
<dd><p>The item is a directory which cannot be read.
</p></dd>
<dt><a id="index-FTW_005fSL"></a><span><code class="code">FTW_SL</code><a class="copiable-link" href="#index-FTW_005fSL"> &para;</a></span></dt>
<dd><p>The item is a symbolic link.  Since symbolic links are normally followed
seeing this value in a <code class="code">ftw</code> callback function means the referenced
file does not exist.  The situation for <code class="code">nftw</code> is different.
</p>
<p>This value is only available if the program is compiled with
<code class="code">_XOPEN_EXTENDED</code> defined before including
the first header.  The original SVID systems do not have symbolic links.
</p></dd>
</dl>

<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
type is in fact <code class="code">__ftw64_func_t</code> since this mode changes
<code class="code">struct stat</code> to be <code class="code">struct stat64</code>.
</p></dd></dl>

<p>For the LFS interface and for use in the function <code class="code">ftw64</code>, the
header <samp class="file">ftw.h</samp> defines another function type.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-_005f_005fftw64_005ffunc_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">__ftw64_func_t</strong><a class="copiable-link" href="#index-_005f_005fftw64_005ffunc_005ft"> &para;</a></span></dt>
<dd>

<div class="example smallexample">
<pre class="example-preformatted">int (*) (const char *, const struct stat64 *, int)
</pre></div>

<p>This type is used just like <code class="code">__ftw_func_t</code> for the callback
function, but this time is called from <code class="code">ftw64</code>.  The second
parameter to the function is a pointer to a variable of type
<code class="code">struct stat64</code> which is able to represent the larger values.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-_005f_005fnftw_005ffunc_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">__nftw_func_t</strong><a class="copiable-link" href="#index-_005f_005fnftw_005ffunc_005ft"> &para;</a></span></dt>
<dd>

<div class="example smallexample">
<pre class="example-preformatted">int (*) (const char *, const struct stat *, int, struct FTW *)
</pre></div>

<p>The first three arguments are the same as for the <code class="code">__ftw_func_t</code>
type.  However for the third argument some additional values are defined
to allow finer differentiation:
</p><dl class="vtable">
<dt><a id="index-FTW_005fDP"></a><span><code class="code">FTW_DP</code><a class="copiable-link" href="#index-FTW_005fDP"> &para;</a></span></dt>
<dd><p>The current item is a directory and all subdirectories have already been
visited and reported.  This flag is returned instead of <code class="code">FTW_D</code> if
the <code class="code">FTW_DEPTH</code> flag is passed to <code class="code">nftw</code> (see below).
</p></dd>
<dt><a id="index-FTW_005fSLN"></a><span><code class="code">FTW_SLN</code><a class="copiable-link" href="#index-FTW_005fSLN"> &para;</a></span></dt>
<dd><p>The current item is a stale symbolic link.  The file it points to does
not exist.
</p></dd>
</dl>

<p>The last parameter of the callback function is a pointer to a structure
with some extra information as described below.
</p>
<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
type is in fact <code class="code">__nftw64_func_t</code> since this mode changes
<code class="code">struct stat</code> to be <code class="code">struct stat64</code>.
</p></dd></dl>

<p>For the LFS interface there is also a variant of this data type
available which has to be used with the <code class="code">nftw64</code> function.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-_005f_005fnftw64_005ffunc_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">__nftw64_func_t</strong><a class="copiable-link" href="#index-_005f_005fnftw64_005ffunc_005ft"> &para;</a></span></dt>
<dd>

<div class="example smallexample">
<pre class="example-preformatted">int (*) (const char *, const struct stat64 *, int, struct FTW *)
</pre></div>

<p>This type is used just like <code class="code">__nftw_func_t</code> for the callback
function, but this time is called from <code class="code">nftw64</code>.  The second
parameter to the function is this time a pointer to a variable of type
<code class="code">struct stat64</code> which is able to represent the larger values.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-struct-FTW"><span class="category-def">Data Type: </span><span><strong class="def-name">struct FTW</strong><a class="copiable-link" href="#index-struct-FTW"> &para;</a></span></dt>
<dd>
<p>The information contained in this structure helps in interpreting the
name parameter and gives some information about the current state of the
traversal of the directory hierarchy.
</p>
<dl class="table">
<dt><code class="code">int base</code></dt>
<dd><p>The value is the offset into the string passed in the first parameter to
the callback function of the beginning of the file name.  The rest of
the string is the path of the file.  This information is especially
important if the <code class="code">FTW_CHDIR</code> flag was set in calling <code class="code">nftw</code>
since then the current directory is the one the current item is found
in.
</p></dd>
<dt><code class="code">int level</code></dt>
<dd><p>Whilst processing, the code tracks how many directories down it has gone
to find the current file.  This nesting level starts at <em class="math">0</em> for
files in the initial directory (or is zero for the initial file if a
file was passed).
</p></dd>
</dl>
</dd></dl>


<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ftw"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">ftw</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, __ftw_func_t <var class="var">func</var>, int <var class="var">descriptors</var>)</code><a class="copiable-link" href="#index-ftw"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">ftw</code> function calls the callback function given in the
parameter <var class="var">func</var> for every item which is found in the directory
specified by <var class="var">filename</var> and all directories below.  The function
follows symbolic links if necessary but does not process an item twice.
If <var class="var">filename</var> is not a directory then it itself is the only object
returned to the callback function.
</p>
<p>The file name passed to the callback function is constructed by taking
the <var class="var">filename</var> parameter and appending the names of all passed
directories and then the local file name.  So the callback function can
use this parameter to access the file.  <code class="code">ftw</code> also calls
<code class="code">stat</code> for the file and passes that information on to the callback
function.  If this <code class="code">stat</code> call is not successful the failure is
indicated by setting the third argument of the callback function to
<code class="code">FTW_NS</code>.  Otherwise it is set according to the description given
in the account of <code class="code">__ftw_func_t</code> above.
</p>
<p>The callback function is expected to return <em class="math">0</em> to indicate that no
error occurred and that processing should continue.  If an error
occurred in the callback function or it wants <code class="code">ftw</code> to return
immediately, the callback function can return a value other than
<em class="math">0</em>.  This is the only correct way to stop the function.  The
program must not use <code class="code">setjmp</code> or similar techniques to continue
from another place.  This would leave resources allocated by the
<code class="code">ftw</code> function unfreed.
</p>
<p>The <var class="var">descriptors</var> parameter to <code class="code">ftw</code> specifies how many file
descriptors it is allowed to consume.  The function runs faster the more
descriptors it can use.  For each level in the directory hierarchy at
most one descriptor is used, but for very deep ones any limit on open
file descriptors for the process or the system may be exceeded.
Moreover, file descriptor limits in a multi-threaded program apply to
all the threads as a group, and therefore it is a good idea to supply a
reasonable limit to the number of open descriptors.
</p>
<p>The return value of the <code class="code">ftw</code> function is <em class="math">0</em> if all callback
function calls returned <em class="math">0</em> and all actions performed by the
<code class="code">ftw</code> succeeded.  If a function call failed (other than calling
<code class="code">stat</code> on an item) the function returns <em class="math">-1</em>.  If a callback
function returns a value other than <em class="math">0</em> this value is returned as
the return value of <code class="code">ftw</code>.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32-bit system this function is in fact <code class="code">ftw64</code>, i.e., the LFS
interface transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ftw64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">ftw64</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, __ftw64_func_t <var class="var">func</var>, int <var class="var">descriptors</var>)</code><a class="copiable-link" href="#index-ftw64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">ftw</code> but it can work on filesystems
with large files.  File information is reported using a variable of type
<code class="code">struct stat64</code> which is passed by reference to the callback
function.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32-bit system this function is available under the name <code class="code">ftw</code> and
transparently replaces the old implementation.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-nftw"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">nftw</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, __nftw_func_t <var class="var">func</var>, int <var class="var">descriptors</var>, int <var class="var">flag</var>)</code><a class="copiable-link" href="#index-nftw"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe cwd
| AS-Unsafe heap
| AC-Unsafe mem fd cwd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">nftw</code> function works like the <code class="code">ftw</code> functions.  They call
the callback function <var class="var">func</var> for all items found in the directory
<var class="var">filename</var> and below.  At most <var class="var">descriptors</var> file descriptors
are consumed during the <code class="code">nftw</code> call.
</p>
<p>One difference is that the callback function is of a different type.  It
is of type <code class="code">struct&nbsp;FTW&nbsp;*</code><!-- /@w --> and provides the callback function
with the extra information described above.
</p>
<p>A second difference is that <code class="code">nftw</code> takes a fourth argument, which
is <em class="math">0</em> or a bitwise-OR combination of any of the following values.
</p>
<dl class="vtable">
<dt><a id="index-FTW_005fPHYS"></a><span><code class="code">FTW_PHYS</code><a class="copiable-link" href="#index-FTW_005fPHYS"> &para;</a></span></dt>
<dd><p>While traversing the directory symbolic links are not followed.  Instead
symbolic links are reported using the <code class="code">FTW_SL</code> value for the type
parameter to the callback function.  If the file referenced by a
symbolic link does not exist <code class="code">FTW_SLN</code> is returned instead.
</p></dd>
<dt><a id="index-FTW_005fMOUNT"></a><span><code class="code">FTW_MOUNT</code><a class="copiable-link" href="#index-FTW_005fMOUNT"> &para;</a></span></dt>
<dd><p>The callback function is only called for items which are on the same
mounted filesystem as the directory given by the <var class="var">filename</var>
parameter to <code class="code">nftw</code>.
</p></dd>
<dt><a id="index-FTW_005fCHDIR"></a><span><code class="code">FTW_CHDIR</code><a class="copiable-link" href="#index-FTW_005fCHDIR"> &para;</a></span></dt>
<dd><p>If this flag is given the current working directory is changed to the
directory of the reported object before the callback function is called.
When <code class="code">ntfw</code> finally returns the current directory is restored to
its original value.
</p></dd>
<dt><a id="index-FTW_005fDEPTH"></a><span><code class="code">FTW_DEPTH</code><a class="copiable-link" href="#index-FTW_005fDEPTH"> &para;</a></span></dt>
<dd><p>If this option is specified then all subdirectories and files within
them are processed before processing the top directory itself
(depth-first processing).  This also means the type flag given to the
callback function is <code class="code">FTW_DP</code> and not <code class="code">FTW_D</code>.
</p></dd>
<dt><a id="index-FTW_005fACTIONRETVAL"></a><span><code class="code">FTW_ACTIONRETVAL</code><a class="copiable-link" href="#index-FTW_005fACTIONRETVAL"> &para;</a></span></dt>
<dd><p>If this option is specified then return values from callbacks
are handled differently.  If the callback returns <code class="code">FTW_CONTINUE</code>,
walking continues normally.  <code class="code">FTW_STOP</code> means walking stops
and <code class="code">FTW_STOP</code> is returned to the caller.  If <code class="code">FTW_SKIP_SUBTREE</code>
is returned by the callback with <code class="code">FTW_D</code> argument, the subtree
is skipped and walking continues with next sibling of the directory.
If <code class="code">FTW_SKIP_SIBLINGS</code> is returned by the callback, all siblings
of the current entry are skipped and walking continues in its parent.
No other return values should be returned from the callbacks if
this option is set.  This option is a GNU extension.
</p></dd>
</dl>

<p>The return value is computed in the same way as for <code class="code">ftw</code>.
<code class="code">nftw</code> returns <em class="math">0</em> if no failures occurred and all callback
functions returned <em class="math">0</em>.  In case of internal errors, such as memory
problems, the return value is <em class="math">-1</em> and <code class="code">errno</code> is set
accordingly.  If the return value of a callback invocation was non-zero
then that value is returned.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32-bit system this function is in fact <code class="code">nftw64</code>, i.e., the LFS
interface transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-nftw64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">nftw64</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, __nftw64_func_t <var class="var">func</var>, int <var class="var">descriptors</var>, int <var class="var">flag</var>)</code><a class="copiable-link" href="#index-nftw64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe cwd
| AS-Unsafe heap
| AC-Unsafe mem fd cwd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">nftw</code> but it can work on filesystems
with large files.  File information is reported using a variable of type
<code class="code">struct stat64</code> which is passed by reference to the callback
function.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32-bit system this function is available under the name <code class="code">nftw</code> and
transparently replaces the old implementation.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Hard-Links.html">Hard Links</a>, Previous: <a href="Accessing-Directories.html">Accessing Directories</a>, Up: <a href="File-System-Interface.html">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
