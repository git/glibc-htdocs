<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>System V contexts (The GNU C Library)</title>

<meta name="description" content="System V contexts (The GNU C Library)">
<meta name="keywords" content="System V contexts (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Non_002dLocal-Exits.html" rel="up" title="Non-Local Exits">
<link href="Non_002dLocal-Exits-and-Signals.html" rel="prev" title="Non-Local Exits and Signals">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="System-V-contexts">
<div class="nav-panel">
<p>
Previous: <a href="Non_002dLocal-Exits-and-Signals.html" accesskey="p" rel="prev">Non-Local Exits and Signals</a>, Up: <a href="Non_002dLocal-Exits.html" accesskey="u" rel="up">Non-Local Exits</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Complete-Context-Control"><span>24.4 Complete Context Control<a class="copiable-link" href="#Complete-Context-Control"> &para;</a></span></h3>

<p>The Unix standard provides one more set of functions to control the
execution path and these functions are more powerful than those
discussed in this chapter so far.  These functions were part of the
original System&nbsp;V<!-- /@w --> API and by this route were added to the Unix
API.  Besides on branded Unix implementations these interfaces are not
widely available.  Not all platforms and/or architectures the GNU C Library
is available on provide this interface.  Use <samp class="file">configure</samp> to
detect the availability.
</p>
<p>Similar to the <code class="code">jmp_buf</code> and <code class="code">sigjmp_buf</code> types used for the
variables to contain the state of the <code class="code">longjmp</code> functions the
interfaces of interest here have an appropriate type as well.  Objects
of this type are normally much larger since more information is
contained.  The type is also used in a few more places as we will see.
The types and functions described in this section are all defined and
declared respectively in the <samp class="file">ucontext.h</samp> header file.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-ucontext_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">ucontext_t</strong><a class="copiable-link" href="#index-ucontext_005ft"> &para;</a></span></dt>
<dd>

<p>The <code class="code">ucontext_t</code> type is defined as a structure with at least the
following elements:
</p>
<dl class="table">
<dt><code class="code">ucontext_t *uc_link</code></dt>
<dd><p>This is a pointer to the next context structure which is used if the
context described in the current structure returns.
</p>
</dd>
<dt><code class="code">sigset_t uc_sigmask</code></dt>
<dd><p>Set of signals which are blocked when this context is used.
</p>
</dd>
<dt><code class="code">stack_t uc_stack</code></dt>
<dd><p>Stack used for this context.  The value need not be (and normally is
not) the stack pointer.  See <a class="xref" href="Signal-Stack.html">Using a Separate Signal Stack</a>.
</p>
</dd>
<dt><code class="code">mcontext_t uc_mcontext</code></dt>
<dd><p>This element contains the actual state of the process.  The
<code class="code">mcontext_t</code> type is also defined in this header but the definition
should be treated as opaque.  Any use of knowledge of the type makes
applications less portable.
</p>
</dd>
</dl>
</dd></dl>

<p>Objects of this type have to be created by the user.  The initialization
and modification happens through one of the following functions:
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getcontext"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getcontext</strong> <code class="def-code-arguments">(ucontext_t *<var class="var">ucp</var>)</code><a class="copiable-link" href="#index-getcontext"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:ucp
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getcontext</code> function initializes the variable pointed to by
<var class="var">ucp</var> with the context of the calling thread.  The context contains
the content of the registers, the signal mask, and the current stack.
Executing the contents would start at the point where the
<code class="code">getcontext</code> call just returned.
</p>
<p><strong class="strong">Compatibility Note:</strong> Depending on the operating system,
information about the current context&rsquo;s stack may be in the
<code class="code">uc_stack</code> field of <var class="var">ucp</var>, or it may instead be in
architecture-specific subfields of the <code class="code">uc_mcontext</code> field.
</p>
<p>The function returns <code class="code">0</code> if successful.  Otherwise it returns
<code class="code">-1</code> and sets <code class="code">errno</code> accordingly.
</p></dd></dl>

<p>The <code class="code">getcontext</code> function is similar to <code class="code">setjmp</code> but it does
not provide an indication of whether <code class="code">getcontext</code> is returning for
the first time or whether an initialized context has just been restored.
If this is necessary the user has to determine this herself.  This must
be done carefully since the context contains registers which might contain
register variables.  This is a good situation to define variables with
<code class="code">volatile</code>.
</p>
<p>Once the context variable is initialized it can be used as is or it can
be modified using the <code class="code">makecontext</code> function.  The latter is normally
done when implementing co-routines or similar constructs.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-makecontext"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">makecontext</strong> <code class="def-code-arguments">(ucontext_t *<var class="var">ucp</var>, void (*<var class="var">func</var>) (void), int <var class="var">argc</var>, &hellip;)</code><a class="copiable-link" href="#index-makecontext"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:ucp
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <var class="var">ucp</var> parameter passed to <code class="code">makecontext</code> shall be
initialized by a call to <code class="code">getcontext</code>.  The context will be
modified in a way such that if the context is resumed it will start by
calling the function <code class="code">func</code> which gets <var class="var">argc</var> integer arguments
passed.  The integer arguments which are to be passed should follow the
<var class="var">argc</var> parameter in the call to <code class="code">makecontext</code>.
</p>
<p>Before the call to this function the <code class="code">uc_stack</code> and <code class="code">uc_link</code>
element of the <var class="var">ucp</var> structure should be initialized.  The
<code class="code">uc_stack</code> element describes the stack which is used for this
context.  No two contexts which are used at the same time should use the
same memory region for a stack.
</p>
<p>The <code class="code">uc_link</code> element of the object pointed to by <var class="var">ucp</var> should
be a pointer to the context to be executed when the function <var class="var">func</var>
returns or it should be a null pointer.  See <code class="code">setcontext</code> for more
information about the exact use.
</p></dd></dl>

<p>While allocating the memory for the stack one has to be careful.  Most
modern processors keep track of whether a certain memory region is
allowed to contain code which is executed or not.  Data segments and
heap memory are normally not tagged to allow this.  The result is that
programs would fail.  Examples for such code include the calling
sequences the GNU C compiler generates for calls to nested functions.
Safe ways to allocate stacks correctly include using memory on the
original thread&rsquo;s stack or explicitly allocating memory tagged for
execution using (see <a class="pxref" href="Memory_002dmapped-I_002fO.html">Memory-mapped I/O</a>).
</p>
<p><strong class="strong">Compatibility note</strong>: The current Unix standard is very imprecise
about the way the stack is allocated.  All implementations seem to agree
that the <code class="code">uc_stack</code> element must be used but the values stored in
the elements of the <code class="code">stack_t</code> value are unclear.  The GNU C Library
and most other Unix implementations require the <code class="code">ss_sp</code> value of
the <code class="code">uc_stack</code> element to point to the base of the memory region
allocated for the stack and the size of the memory region is stored in
<code class="code">ss_size</code>.  There are implementations out there which require
<code class="code">ss_sp</code> to be set to the value the stack pointer will have (which
can, depending on the direction the stack grows, be different).  This
difference makes the <code class="code">makecontext</code> function hard to use and it
requires detection of the platform at compile time.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setcontext"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">setcontext</strong> <code class="def-code-arguments">(const ucontext_t *<var class="var">ucp</var>)</code><a class="copiable-link" href="#index-setcontext"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:ucp
| AS-Unsafe corrupt
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">setcontext</code> function restores the context described by
<var class="var">ucp</var>.  The context is not modified and can be reused as often as
wanted.
</p>
<p>If the context was created by <code class="code">getcontext</code> execution resumes with
the registers filled with the same values and the same stack as if the
<code class="code">getcontext</code> call just returned.
</p>
<p>If the context was modified with a call to <code class="code">makecontext</code> execution
continues with the function passed to <code class="code">makecontext</code> which gets the
specified parameters passed.  If this function returns execution is
resumed in the context which was referenced by the <code class="code">uc_link</code>
element of the context structure passed to <code class="code">makecontext</code> at the
time of the call.  If <code class="code">uc_link</code> was a null pointer the application
terminates normally with an exit status value of <code class="code">EXIT_SUCCESS</code>
(see <a class="pxref" href="Program-Termination.html">Program Termination</a>).
</p>
<p>If the context was created by a call to a signal handler or from any
other source then the behaviour of <code class="code">setcontext</code> is unspecified.
</p>
<p>Since the context contains information about the stack no two threads
should use the same context at the same time.  The result in most cases
would be disastrous.
</p>
<p>The <code class="code">setcontext</code> function does not return unless an error occurred
in which case it returns <code class="code">-1</code>.
</p></dd></dl>

<p>The <code class="code">setcontext</code> function simply replaces the current context with
the one described by the <var class="var">ucp</var> parameter.  This is often useful but
there are situations where the current context has to be preserved.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-swapcontext"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">swapcontext</strong> <code class="def-code-arguments">(ucontext_t *restrict <var class="var">oucp</var>, const ucontext_t *restrict <var class="var">ucp</var>)</code><a class="copiable-link" href="#index-swapcontext"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:oucp race:ucp
| AS-Unsafe corrupt
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">swapcontext</code> function is similar to <code class="code">setcontext</code> but
instead of just replacing the current context the latter is first saved
in the object pointed to by <var class="var">oucp</var> as if this was a call to
<code class="code">getcontext</code>.  The saved context would resume after the call to
<code class="code">swapcontext</code>.
</p>
<p>Once the current context is saved the context described in <var class="var">ucp</var> is
installed and execution continues as described in this context.
</p>
<p>If <code class="code">swapcontext</code> succeeds the function does not return unless the
context <var class="var">oucp</var> is used without prior modification by
<code class="code">makecontext</code>.  The return value in this case is <code class="code">0</code>.  If the
function fails it returns <code class="code">-1</code> and sets <code class="code">errno</code> accordingly.
</p></dd></dl>

<h3 class="heading" id="Example-for-SVID-Context-Handling"><span>Example for SVID Context Handling<a class="copiable-link" href="#Example-for-SVID-Context-Handling"> &para;</a></span></h3>

<p>The easiest way to use the context handling functions is as a
replacement for <code class="code">setjmp</code> and <code class="code">longjmp</code>.  The context contains
on most platforms more information which may lead to fewer surprises
but this also means using these functions is more expensive (besides
being less portable).
</p>
<div class="example smallexample">
<pre class="example-preformatted">int
random_search (int n, int (*fp) (int, ucontext_t *))
{
  volatile int cnt = 0;
  ucontext_t uc;

  /* <span class="r">Safe current context.</span>  */
  if (getcontext (&amp;uc) &lt; 0)
    return -1;

  /* <span class="r">If we have not tried <var class="var">n</var> times try again.</span>  */
  if (cnt++ &lt; n)
    /* <span class="r">Call the function with a new random number</span>
       <span class="r">and the context</span>.  */
    if (fp (rand (), &amp;uc) != 0)
      /* <span class="r">We found what we were looking for.</span>  */
      return 1;

  /* <span class="r">Not found.</span>  */
  return 0;
}
</pre></div>

<p>Using contexts in such a way enables emulating exception handling.  The
search functions passed in the <var class="var">fp</var> parameter could be very large,
nested, and complex which would make it complicated (or at least would
require a lot of code) to leave the function with an error value which
has to be passed down to the caller.  By using the context it is
possible to leave the search function in one step and allow restarting
the search which also has the nice side effect that it can be
significantly faster.
</p>
<p>Something which is harder to implement with <code class="code">setjmp</code> and
<code class="code">longjmp</code> is to switch temporarily to a different execution path
and then resume where execution was stopped.
</p>
<div class="example smallexample">
<pre class="example-preformatted">

#include &lt;signal.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;
#include &lt;ucontext.h&gt;
#include &lt;sys/time.h&gt;

/* <span class="r">Set by the signal handler.</span> */
static volatile int expired;

/* <span class="r">The contexts.</span> */
static ucontext_t uc[3];

/* <span class="r">We do only a certain number of switches.</span> */
static int switches;


/* <span class="r">This is the function doing the work.  It is just a
   skeleton, real code has to be filled in.</span> */
static void
f (int n)
{
  int m = 0;
  while (1)
    {
      /* <span class="r">This is where the work would be done.</span> */
      if (++m % 100 == 0)
        {
          putchar ('.');
          fflush (stdout);
        }

      /* <span class="r">Regularly the <var class="var">expire</var> variable must be checked.</span> */
      if (expired)
        {
          /* <span class="r">We do not want the program to run forever.</span> */
          if (++switches == 20)
            return;

          printf (&quot;\nswitching from %d to %d\n&quot;, n, 3 - n);
          expired = 0;
          /* <span class="r">Switch to the other context, saving the current one.</span> */
          swapcontext (&amp;uc[n], &amp;uc[3 - n]);
        }
    }
}

/* <span class="r">This is the signal handler which simply set the variable.</span> */
void
handler (int signal)
{
  expired = 1;
}


int
main (void)
{
  struct sigaction sa;
  struct itimerval it;
  char st1[8192];
  char st2[8192];

  /* <span class="r">Initialize the data structures for the interval timer.</span> */
  sa.sa_flags = SA_RESTART;
  sigfillset (&amp;sa.sa_mask);
  sa.sa_handler = handler;
  it.it_interval.tv_sec = 0;
  it.it_interval.tv_usec = 1;
  it.it_value = it.it_interval;

  /* <span class="r">Install the timer and get the context we can manipulate.</span> */
  if (sigaction (SIGPROF, &amp;sa, NULL) &lt; 0
      || setitimer (ITIMER_PROF, &amp;it, NULL) &lt; 0
      || getcontext (&amp;uc[1]) == -1
      || getcontext (&amp;uc[2]) == -1)
    abort ();

  /* <span class="r">Create a context with a separate stack which causes the
     function <code class="code">f</code> to be call with the parameter <code class="code">1</code>.
     Note that the <code class="code">uc_link</code> points to the main context
     which will cause the program to terminate once the function
     return.</span> */
  uc[1].uc_link = &amp;uc[0];
  uc[1].uc_stack.ss_sp = st1;
  uc[1].uc_stack.ss_size = sizeof st1;
  makecontext (&amp;uc[1], (void (*) (void)) f, 1, 1);

  /* <span class="r">Similarly, but <code class="code">2</code> is passed as the parameter to <code class="code">f</code>.</span> */
  uc[2].uc_link = &amp;uc[0];
  uc[2].uc_stack.ss_sp = st2;
  uc[2].uc_stack.ss_size = sizeof st2;
  makecontext (&amp;uc[2], (void (*) (void)) f, 1, 2);

  /* <span class="r">Start running.</span> */
  swapcontext (&amp;uc[0], &amp;uc[1]);
  putchar ('\n');

  return 0;
}
</pre></div>

<p>This an example how the context functions can be used to implement
co-routines or cooperative multi-threading.  All that has to be done is
to call every once in a while <code class="code">swapcontext</code> to continue running a
different context.  It is not recommended to do the context switching from
the signal handler directly since leaving the signal handler via
<code class="code">setcontext</code> if the signal was delivered during code that was not
asynchronous signal safe could lead to problems. Setting a variable in
the signal handler and checking it in the body of the functions which
are executed is a safer approach.  Since <code class="code">swapcontext</code> is saving the
current context it is possible to have multiple different scheduling points
in the code.  Execution will always resume where it was left.
</p></div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Non_002dLocal-Exits-and-Signals.html">Non-Local Exits and Signals</a>, Up: <a href="Non_002dLocal-Exits.html">Non-Local Exits</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
