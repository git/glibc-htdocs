<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>The gencat program (The GNU C Library)</title>

<meta name="description" content="The gencat program (The GNU C Library)">
<meta name="keywords" content="The gencat program (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Message-catalogs-a-la-X_002fOpen.html" rel="up" title="Message catalogs a la X/Open">
<link href="Common-Usage.html" rel="next" title="Common Usage">
<link href="The-message-catalog-files.html" rel="prev" title="The message catalog files">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="The-gencat-program">
<div class="nav-panel">
<p>
Next: <a href="Common-Usage.html" accesskey="n" rel="next">How to use the <code class="code">catgets</code> interface</a>, Previous: <a href="The-message-catalog-files.html" accesskey="p" rel="prev">Format of the message catalog files</a>, Up: <a href="Message-catalogs-a-la-X_002fOpen.html" accesskey="u" rel="up">X/Open Message Catalog Handling</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Generate-Message-Catalogs-files"><span>8.1.3 Generate Message Catalogs files<a class="copiable-link" href="#Generate-Message-Catalogs-files"> &para;</a></span></h4>

<a class="index-entry-id" id="index-gencat"></a>
<p>The <code class="code">gencat</code> program is specified in the X/Open standard and the
GNU implementation follows this specification and so processes
all correctly formed input files.  Additionally some extension are
implemented which help to work in a more reasonable way with the
<code class="code">catgets</code> functions.
</p>
<p>The <code class="code">gencat</code> program can be invoked in two ways:
</p>
<div class="example">
<pre class="example-preformatted">`gencat [<var class="var">Option</var> ...] [<var class="var">Output-File</var> [<var class="var">Input-File</var> ...]]`
</pre></div>

<p>This is the interface defined in the X/Open standard.  If no
<var class="var">Input-File</var> parameter is given, input will be read from standard
input.  Multiple input files will be read as if they were concatenated.
If <var class="var">Output-File</var> is also missing, the output will be written to
standard output.  To provide the interface one is used to from other
programs a second interface is provided.
</p>
<div class="example smallexample">
<pre class="example-preformatted">`gencat [<var class="var">Option</var> ...] -o <var class="var">Output-File</var> [<var class="var">Input-File</var> ...]`
</pre></div>

<p>The option &lsquo;<samp class="samp">-o</samp>&rsquo; is used to specify the output file and all file
arguments are used as input files.
</p>
<p>Beside this one can use <samp class="file">-</samp> or <samp class="file">/dev/stdin</samp> for
<var class="var">Input-File</var> to denote the standard input.  Corresponding one can
use <samp class="file">-</samp> and <samp class="file">/dev/stdout</samp> for <var class="var">Output-File</var> to denote
standard output.  Using <samp class="file">-</samp> as a file name is allowed in X/Open
while using the device names is a GNU extension.
</p>
<p>The <code class="code">gencat</code> program works by concatenating all input files and
then <strong class="strong">merging</strong> the resulting collection of message sets with a
possibly existing output file.  This is done by removing all messages
with set/message number tuples matching any of the generated messages
from the output file and then adding all the new messages.  To
regenerate a catalog file while ignoring the old contents therefore
requires removing the output file if it exists.  If the output is
written to standard output no merging takes place.
</p>
<p>The following table shows the options understood by the <code class="code">gencat</code>
program.  The X/Open standard does not specify any options for the
program so all of these are GNU extensions.
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">-V</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">--version</samp>&rsquo;</dt>
<dd><p>Print the version information and exit.
</p></dd>
<dt>&lsquo;<samp class="samp">-h</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">--help</samp>&rsquo;</dt>
<dd><p>Print a usage message listing all available options, then exit successfully.
</p></dd>
<dt>&lsquo;<samp class="samp">--new</samp>&rsquo;</dt>
<dd><p>Do not merge the new messages from the input files with the old content
of the output file.  The old content of the output file is discarded.
</p></dd>
<dt>&lsquo;<samp class="samp">-H</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">--header=name</samp>&rsquo;</dt>
<dd><p>This option is used to emit the symbolic names given to sets and
messages in the input files for use in the program.  Details about how
to use this are given in the next section.  The <var class="var">name</var> parameter to
this option specifies the name of the output file.  It will contain a
number of C preprocessor <code class="code">#define</code>s to associate a name with a
number.
</p>
<p>Please note that the generated file only contains the symbols from the
input files.  If the output is merged with the previous content of the
output file the possibly existing symbols from the file(s) which
generated the old output files are not in the generated header file.
</p></dd>
</dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Common-Usage.html">How to use the <code class="code">catgets</code> interface</a>, Previous: <a href="The-message-catalog-files.html">Format of the message catalog files</a>, Up: <a href="Message-catalogs-a-la-X_002fOpen.html">X/Open Message Catalog Handling</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
