<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Streams and Cookies (The GNU C Library)</title>

<meta name="description" content="Streams and Cookies (The GNU C Library)">
<meta name="keywords" content="Streams and Cookies (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Custom-Streams.html" rel="up" title="Custom Streams">
<link href="Hook-Functions.html" rel="next" title="Hook Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Streams-and-Cookies">
<div class="nav-panel">
<p>
Next: <a href="Hook-Functions.html" accesskey="n" rel="next">Custom Stream Hook Functions</a>, Up: <a href="Custom-Streams.html" accesskey="u" rel="up">Programming Your Own Custom Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Custom-Streams-and-Cookies"><span>12.21.2.1 Custom Streams and Cookies<a class="copiable-link" href="#Custom-Streams-and-Cookies"> &para;</a></span></h4>
<a class="index-entry-id" id="index-cookie_002c-for-custom-stream"></a>

<p>Inside every custom stream is a special object called the <em class="dfn">cookie</em>.
This is an object supplied by you which records where to fetch or store
the data read or written.  It is up to you to define a data type to use
for the cookie.  The stream functions in the library never refer
directly to its contents, and they don&rsquo;t even know what the type is;
they record its address with type <code class="code">void *</code>.
</p>
<p>To implement a custom stream, you must specify <em class="emph">how</em> to fetch or
store the data in the specified place.  You do this by defining
<em class="dfn">hook functions</em> to read, write, change &ldquo;file position&rdquo;, and close
the stream.  All four of these functions will be passed the stream&rsquo;s
cookie so they can tell where to fetch or store the data.  The library
functions don&rsquo;t know what&rsquo;s inside the cookie, but your functions will
know.
</p>
<p>When you create a custom stream, you must specify the cookie pointer,
and also the four hook functions stored in a structure of type
<code class="code">cookie_io_functions_t</code>.
</p>
<p>These facilities are declared in <samp class="file">stdio.h</samp>.
<a class="index-entry-id" id="index-stdio_002eh-14"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-cookie_005fio_005ffunctions_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">cookie_io_functions_t</strong><a class="copiable-link" href="#index-cookie_005fio_005ffunctions_005ft"> &para;</a></span></dt>
<dd>
<p>This is a structure type that holds the functions that define the
communications protocol between the stream and its cookie.  It has
the following members:
</p>
<dl class="table">
<dt><code class="code">cookie_read_function_t *read</code></dt>
<dd><p>This is the function that reads data from the cookie.  If the value is a
null pointer instead of a function, then read operations on this stream
always return <code class="code">EOF</code>.
</p>
</dd>
<dt><code class="code">cookie_write_function_t *write</code></dt>
<dd><p>This is the function that writes data to the cookie.  If the value is a
null pointer instead of a function, then data written to the stream is
discarded.
</p>
</dd>
<dt><code class="code">cookie_seek_function_t *seek</code></dt>
<dd><p>This is the function that performs the equivalent of file positioning on
the cookie.  If the value is a null pointer instead of a function, calls
to <code class="code">fseek</code> or <code class="code">fseeko</code> on this stream can only seek to
locations within the buffer; any attempt to seek outside the buffer will
return an <code class="code">ESPIPE</code> error.
</p>
</dd>
<dt><code class="code">cookie_close_function_t *close</code></dt>
<dd><p>This function performs any appropriate cleanup on the cookie when
closing the stream.  If the value is a null pointer instead of a
function, nothing special is done to close the cookie when the stream is
closed.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fopencookie"><span class="category-def">Function: </span><span><code class="def-type">FILE *</code> <strong class="def-name">fopencookie</strong> <code class="def-code-arguments">(void *<var class="var">cookie</var>, const char *<var class="var">opentype</var>, cookie_io_functions_t <var class="var">io-functions</var>)</code><a class="copiable-link" href="#index-fopencookie"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap lock
| AC-Unsafe mem lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function actually creates the stream for communicating with the
<var class="var">cookie</var> using the functions in the <var class="var">io-functions</var> argument.
The <var class="var">opentype</var> argument is interpreted as for <code class="code">fopen</code>;
see <a class="ref" href="Opening-Streams.html">Opening Streams</a>.  (But note that the &ldquo;truncate on
open&rdquo; option is ignored.)  The new stream is fully buffered.
</p>
<p>The <code class="code">fopencookie</code> function returns the newly created stream, or a null
pointer in case of an error.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Hook-Functions.html">Custom Stream Hook Functions</a>, Up: <a href="Custom-Streams.html">Programming Your Own Custom Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
