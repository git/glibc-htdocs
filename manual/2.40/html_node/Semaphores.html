<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Semaphores (The GNU C Library)</title>

<meta name="description" content="Semaphores (The GNU C Library)">
<meta name="keywords" content="Semaphores (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Inter_002dProcess-Communication.html" rel="up" title="Inter-Process Communication">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Semaphores">
<div class="nav-panel">
<p>
Up: <a href="Inter_002dProcess-Communication.html" accesskey="u" rel="up">Inter-Process Communication</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Semaphores-1"><span>28.1 Semaphores<a class="copiable-link" href="#Semaphores-1"> &para;</a></span></h3>

<p>The GNU C Library implements the semaphore APIs as defined in POSIX and
System V.  Semaphores can be used by multiple processes to coordinate shared
resources.  The following is a complete list of the semaphore functions provided
by the GNU C Library.
</p>

<ul class="mini-toc">
<li><a href="#System-V-Semaphores" accesskey="1">System V Semaphores</a></li>
<li><a href="#POSIX-Semaphores" accesskey="2">POSIX Semaphores</a></li>
</ul>
<div class="subsection-level-extent" id="System-V-Semaphores">
<h4 class="subsection"><span>28.1.1 System V Semaphores<a class="copiable-link" href="#System-V-Semaphores"> &para;</a></span></h4>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-semctl"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">semctl</strong> <code class="def-code-arguments">(int <var class="var">semid</var>, int <var class="var">semnum</var>, int <var class="var">cmd</var>);</code><a class="copiable-link" href="#index-semctl"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Unsafe corrupt/linux
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-semget"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">semget</strong> <code class="def-code-arguments">(key_t <var class="var">key</var>, int <var class="var">nsems</var>, int <var class="var">semflg</var>);</code><a class="copiable-link" href="#index-semget"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-semop"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">semop</strong> <code class="def-code-arguments">(int <var class="var">semid</var>, struct sembuf *<var class="var">sops</var>, size_t <var class="var">nsops</var>);</code><a class="copiable-link" href="#index-semop"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-semtimedop"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">semtimedop</strong> <code class="def-code-arguments">(int <var class="var">semid</var>, struct sembuf *<var class="var">sops</var>, size_t <var class="var">nsops</var>, const struct timespec *<var class="var">timeout</var>);</code><a class="copiable-link" href="#index-semtimedop"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

</div>
<div class="subsection-level-extent" id="POSIX-Semaphores">
<h4 class="subsection"><span>28.1.2 POSIX Semaphores<a class="copiable-link" href="#POSIX-Semaphores"> &para;</a></span></h4>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sem_005finit"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sem_init</strong> <code class="def-code-arguments">(sem_t *<var class="var">sem</var>, int <var class="var">pshared</var>, unsigned int <var class="var">value</var>);</code><a class="copiable-link" href="#index-sem_005finit"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sem_005fdestroy"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sem_destroy</strong> <code class="def-code-arguments">(sem_t *<var class="var">sem</var>);</code><a class="copiable-link" href="#index-sem_005fdestroy"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_002asem_005fopen"><span class="category-def">Function: </span><span><code class="def-type">sem_t</code> <strong class="def-name">*sem_open</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>, int <var class="var">oflag</var>, ...);</code><a class="copiable-link" href="#index-_002asem_005fopen"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Unsafe init
| AC-Unsafe init
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sem_005fclose"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sem_close</strong> <code class="def-code-arguments">(sem_t *<var class="var">sem</var>);</code><a class="copiable-link" href="#index-sem_005fclose"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sem_005funlink"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sem_unlink</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>);</code><a class="copiable-link" href="#index-sem_005funlink"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Unsafe init
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sem_005fwait"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sem_wait</strong> <code class="def-code-arguments">(sem_t *<var class="var">sem</var>);</code><a class="copiable-link" href="#index-sem_005fwait"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sem_005ftimedwait"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sem_timedwait</strong> <code class="def-code-arguments">(sem_t *<var class="var">sem</var>, const struct timespec *<var class="var">abstime</var>);</code><a class="copiable-link" href="#index-sem_005ftimedwait"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sem_005ftrywait"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sem_trywait</strong> <code class="def-code-arguments">(sem_t *<var class="var">sem</var>);</code><a class="copiable-link" href="#index-sem_005ftrywait"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sem_005fpost"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sem_post</strong> <code class="def-code-arguments">(sem_t *<var class="var">sem</var>);</code><a class="copiable-link" href="#index-sem_005fpost"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sem_005fgetvalue"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sem_getvalue</strong> <code class="def-code-arguments">(sem_t *<var class="var">sem</var>, int *<var class="var">sval</var>);</code><a class="copiable-link" href="#index-sem_005fgetvalue"> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

</dd></dl>
</div>
</div>
<hr>
<div class="nav-panel">
<p>
Up: <a href="Inter_002dProcess-Communication.html">Inter-Process Communication</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
