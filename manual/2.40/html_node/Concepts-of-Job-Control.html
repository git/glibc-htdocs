<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Concepts of Job Control (The GNU C Library)</title>

<meta name="description" content="Concepts of Job Control (The GNU C Library)">
<meta name="keywords" content="Concepts of Job Control (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Job-Control.html" rel="up" title="Job Control">
<link href="Controlling-Terminal.html" rel="next" title="Controlling Terminal">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
kbd.kbd {font-style: oblique}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Concepts-of-Job-Control">
<div class="nav-panel">
<p>
Next: <a href="Controlling-Terminal.html" accesskey="n" rel="next">Controlling Terminal of a Process</a>, Up: <a href="Job-Control.html" accesskey="u" rel="up">Job Control</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Concepts-of-Job-Control-1"><span>29.1 Concepts of Job Control<a class="copiable-link" href="#Concepts-of-Job-Control-1"> &para;</a></span></h3>

<a class="index-entry-id" id="index-shell"></a>
<p>The fundamental purpose of an interactive shell is to read
commands from the user&rsquo;s terminal and create processes to execute the
programs specified by those commands.  It can do this using the
<code class="code">fork</code> (see <a class="pxref" href="Creating-a-Process.html">Creating a Process</a>) and <code class="code">exec</code>
(see <a class="pxref" href="Executing-a-File.html">Executing a File</a>) functions.
</p>
<p>A single command may run just one process&mdash;but often one command uses
several processes.  If you use the &lsquo;<samp class="samp">|</samp>&rsquo; operator in a shell command,
you explicitly request several programs in their own processes.  But
even if you run just one program, it can use multiple processes
internally.  For example, a single compilation command such as &lsquo;<samp class="samp">cc
-c foo.c</samp>&rsquo; typically uses four processes (though normally only two at any
given time).  If you run <code class="code">make</code>, its job is to run other programs
in separate processes.
</p>
<p>The processes belonging to a single command are called a <em class="dfn">process
group</em> or <em class="dfn">job</em>.  This is so that you can operate on all of them at
once.  For example, typing <kbd class="kbd">C-c</kbd> sends the signal <code class="code">SIGINT</code> to
terminate all the processes in the foreground process group.
</p>
<a class="index-entry-id" id="index-session-1"></a>
<p>A <em class="dfn">session</em> is a larger group of processes.  Normally all the
processes that stem from a single login belong to the same session.
</p>
<p>Every process belongs to a process group.  When a process is created, it
becomes a member of the same process group and session as its parent
process.  You can put it in another process group using the
<code class="code">setpgid</code> function, provided the process group belongs to the same
session.
</p>
<a class="index-entry-id" id="index-session-leader"></a>
<p>The only way to put a process in a different session is to make it the
initial process of a new session, or a <em class="dfn">session leader</em>, using the
<code class="code">setsid</code> function.  This also puts the session leader into a new
process group, and you can&rsquo;t move it out of that process group again.
</p>
<p>Usually, new sessions are created by the system login program, and the
session leader is the process running the user&rsquo;s login shell.
</p>
<a class="index-entry-id" id="index-controlling-terminal"></a>
<p>A shell that supports job control must arrange to control which job can
use the terminal at any time.  Otherwise there might be multiple jobs
trying to read from the terminal at once, and confusion about which
process should receive the input typed by the user.  To prevent this,
the shell must cooperate with the terminal driver using the protocol
described in this chapter.
</p>
<a class="index-entry-id" id="index-foreground-job"></a>
<a class="index-entry-id" id="index-background-job"></a>
<p>The shell can give unlimited access to the controlling terminal to only
one process group at a time.  This is called the <em class="dfn">foreground job</em> on
that controlling terminal.  Other process groups managed by the shell
that are executing without such access to the terminal are called
<em class="dfn">background jobs</em>.
</p>
<a class="index-entry-id" id="index-stopped-job"></a>
<p>If a background job needs to read from its controlling
terminal, it is <em class="dfn">stopped</em> by the terminal driver; if the
<code class="code">TOSTOP</code> mode is set, likewise for writing.  The user can stop
a foreground job by typing the SUSP character (see <a class="pxref" href="Special-Characters.html">Special Characters</a>) and a program can stop any job by sending it a
<code class="code">SIGSTOP</code> signal.  It&rsquo;s the responsibility of the shell to notice
when jobs stop, to notify the user about them, and to provide mechanisms
for allowing the user to interactively continue stopped jobs and switch
jobs between foreground and background.
</p>
<p>See <a class="xref" href="Access-to-the-Terminal.html">Access to the Controlling Terminal</a>, for more information about I/O to the
controlling terminal.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Controlling-Terminal.html">Controlling Terminal of a Process</a>, Up: <a href="Job-Control.html">Job Control</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
