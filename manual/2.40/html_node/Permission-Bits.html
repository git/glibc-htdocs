<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Permission Bits (The GNU C Library)</title>

<meta name="description" content="Permission Bits (The GNU C Library)">
<meta name="keywords" content="Permission Bits (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-Attributes.html" rel="up" title="File Attributes">
<link href="Access-Permission.html" rel="next" title="Access Permission">
<link href="File-Owner.html" rel="prev" title="File Owner">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Permission-Bits">
<div class="nav-panel">
<p>
Next: <a href="Access-Permission.html" accesskey="n" rel="next">How Your Access to a File is Decided</a>, Previous: <a href="File-Owner.html" accesskey="p" rel="prev">File Owner</a>, Up: <a href="File-Attributes.html" accesskey="u" rel="up">File Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="The-Mode-Bits-for-Access-Permission"><span>14.9.5 The Mode Bits for Access Permission<a class="copiable-link" href="#The-Mode-Bits-for-Access-Permission"> &para;</a></span></h4>

<p>The <em class="dfn">file mode</em>, stored in the <code class="code">st_mode</code> field of the file
attributes, contains two kinds of information: the file type code, and
the access permission bits.  This section discusses only the access
permission bits, which control who can read or write the file.
See <a class="xref" href="Testing-File-Type.html">Testing the Type of a File</a>, for information about the file type code.
</p>
<p>All of the symbols listed in this section are defined in the header file
<samp class="file">sys/stat.h</samp>.
<a class="index-entry-id" id="index-sys_002fstat_002eh-4"></a>
</p>
<a class="index-entry-id" id="index-file-permission-bits"></a>
<p>These symbolic constants are defined for the file mode bits that control
access permission for the file:
</p>
<dl class="vtable">
<dt><a id="index-S_005fIRUSR"></a><span><code class="code">S_IRUSR</code><a class="copiable-link" href="#index-S_005fIRUSR"> &para;</a></span></dt>
<dt><a id="index-S_005fIREAD"></a><span><code class="code">S_IREAD</code><a class="copiable-link" href="#index-S_005fIREAD"> &para;</a></span></dt>
<dd>

<p>Read permission bit for the owner of the file.  On many systems this bit
is 0400.  <code class="code">S_IREAD</code> is an obsolete synonym provided for BSD
compatibility.
</p>
</dd>
<dt><a id="index-S_005fIWUSR"></a><span><code class="code">S_IWUSR</code><a class="copiable-link" href="#index-S_005fIWUSR"> &para;</a></span></dt>
<dt><a id="index-S_005fIWRITE"></a><span><code class="code">S_IWRITE</code><a class="copiable-link" href="#index-S_005fIWRITE"> &para;</a></span></dt>
<dd>

<p>Write permission bit for the owner of the file.  Usually 0200.
<code class="code">S_IWRITE</code><!-- /@w --> is an obsolete synonym provided for BSD compatibility.
</p>
</dd>
<dt><a id="index-S_005fIXUSR"></a><span><code class="code">S_IXUSR</code><a class="copiable-link" href="#index-S_005fIXUSR"> &para;</a></span></dt>
<dt><a id="index-S_005fIEXEC"></a><span><code class="code">S_IEXEC</code><a class="copiable-link" href="#index-S_005fIEXEC"> &para;</a></span></dt>
<dd>

<p>Execute (for ordinary files) or search (for directories) permission bit
for the owner of the file.  Usually 0100.  <code class="code">S_IEXEC</code> is an obsolete
synonym provided for BSD compatibility.
</p>
</dd>
<dt><a id="index-S_005fIRWXU"></a><span><code class="code">S_IRWXU</code><a class="copiable-link" href="#index-S_005fIRWXU"> &para;</a></span></dt>
<dd>
<p>This is equivalent to &lsquo;<samp class="samp">(S_IRUSR | S_IWUSR | S_IXUSR)</samp>&rsquo;.
</p>
</dd>
<dt><a id="index-S_005fIRGRP"></a><span><code class="code">S_IRGRP</code><a class="copiable-link" href="#index-S_005fIRGRP"> &para;</a></span></dt>
<dd>
<p>Read permission bit for the group owner of the file.  Usually 040.
</p>
</dd>
<dt><a id="index-S_005fIWGRP"></a><span><code class="code">S_IWGRP</code><a class="copiable-link" href="#index-S_005fIWGRP"> &para;</a></span></dt>
<dd>
<p>Write permission bit for the group owner of the file.  Usually 020.
</p>
</dd>
<dt><a id="index-S_005fIXGRP"></a><span><code class="code">S_IXGRP</code><a class="copiable-link" href="#index-S_005fIXGRP"> &para;</a></span></dt>
<dd>
<p>Execute or search permission bit for the group owner of the file.
Usually 010.
</p>
</dd>
<dt><a id="index-S_005fIRWXG"></a><span><code class="code">S_IRWXG</code><a class="copiable-link" href="#index-S_005fIRWXG"> &para;</a></span></dt>
<dd>
<p>This is equivalent to &lsquo;<samp class="samp">(S_IRGRP | S_IWGRP | S_IXGRP)</samp>&rsquo;.
</p>
</dd>
<dt><a id="index-S_005fIROTH"></a><span><code class="code">S_IROTH</code><a class="copiable-link" href="#index-S_005fIROTH"> &para;</a></span></dt>
<dd>
<p>Read permission bit for other users.  Usually 04.
</p>
</dd>
<dt><a id="index-S_005fIWOTH"></a><span><code class="code">S_IWOTH</code><a class="copiable-link" href="#index-S_005fIWOTH"> &para;</a></span></dt>
<dd>
<p>Write permission bit for other users.  Usually 02.
</p>
</dd>
<dt><a id="index-S_005fIXOTH"></a><span><code class="code">S_IXOTH</code><a class="copiable-link" href="#index-S_005fIXOTH"> &para;</a></span></dt>
<dd>
<p>Execute or search permission bit for other users.  Usually 01.
</p>
</dd>
<dt><a id="index-S_005fIRWXO"></a><span><code class="code">S_IRWXO</code><a class="copiable-link" href="#index-S_005fIRWXO"> &para;</a></span></dt>
<dd>
<p>This is equivalent to &lsquo;<samp class="samp">(S_IROTH | S_IWOTH | S_IXOTH)</samp>&rsquo;.
</p>
</dd>
<dt><a id="index-S_005fISUID"></a><span><code class="code">S_ISUID</code><a class="copiable-link" href="#index-S_005fISUID"> &para;</a></span></dt>
<dd>
<p>This is the set-user-ID on execute bit, usually 04000.
See <a class="xref" href="How-Change-Persona.html">How an Application Can Change Persona</a>.
</p>
</dd>
<dt><a id="index-S_005fISGID"></a><span><code class="code">S_ISGID</code><a class="copiable-link" href="#index-S_005fISGID"> &para;</a></span></dt>
<dd>
<p>This is the set-group-ID on execute bit, usually 02000.
See <a class="xref" href="How-Change-Persona.html">How an Application Can Change Persona</a>.
</p>
</dd>
<dt><a class="index-entry-id" id="index-sticky-bit"></a>
<a id="index-S_005fISVTX"></a><span><code class="code">S_ISVTX</code><a class="copiable-link" href="#index-S_005fISVTX"> &para;</a></span></dt>
<dd>
<p>This is the <em class="dfn">sticky</em> bit, usually 01000.
</p>
<p>For a directory it gives permission to delete a file in that directory
only if you own that file.  Ordinarily, a user can either delete all the
files in a directory or cannot delete any of them (based on whether the
user has write permission for the directory).  The same restriction
applies&mdash;you must have both write permission for the directory and own
the file you want to delete.  The one exception is that the owner of the
directory can delete any file in the directory, no matter who owns it
(provided the owner has given himself write permission for the
directory).  This is commonly used for the <samp class="file">/tmp</samp> directory, where
anyone may create files but not delete files created by other users.
</p>
<p>Originally the sticky bit on an executable file modified the swapping
policies of the system.  Normally, when a program terminated, its pages
in core were immediately freed and reused.  If the sticky bit was set on
the executable file, the system kept the pages in core for a while as if
the program were still running.  This was advantageous for a program
likely to be run many times in succession.  This usage is obsolete in
modern systems.  When a program terminates, its pages always remain in
core as long as there is no shortage of memory in the system.  When the
program is next run, its pages will still be in core if no shortage
arose since the last run.
</p>
<p>On some modern systems where the sticky bit has no useful meaning for an
executable file, you cannot set the bit at all for a non-directory.
If you try, <code class="code">chmod</code> fails with <code class="code">EFTYPE</code>;
see <a class="pxref" href="Setting-Permissions.html">Assigning File Permissions</a>.
</p>
<p>Some systems (particularly SunOS) have yet another use for the sticky
bit.  If the sticky bit is set on a file that is <em class="emph">not</em> executable,
it means the opposite: never cache the pages of this file at all.  The
main use of this is for the files on an NFS server machine which are
used as the swap area of diskless client machines.  The idea is that the
pages of the file will be cached in the client&rsquo;s memory, so it is a
waste of the server&rsquo;s memory to cache them a second time.  With this
usage the sticky bit also implies that the filesystem may fail to record
the file&rsquo;s modification time onto disk reliably (the idea being that
no-one cares for a swap file).
</p>
<p>This bit is only available on BSD systems (and those derived from
them).  Therefore one has to use the <code class="code">_GNU_SOURCE</code> feature select
macro, or not define any feature test macros, to get the definition
(see <a class="pxref" href="Feature-Test-Macros.html">Feature Test Macros</a>).
</p></dd>
</dl>

<p>The actual bit values of the symbols are listed in the table above
so you can decode file mode values when debugging your programs.
These bit values are correct for most systems, but they are not
guaranteed.
</p>
<p><strong class="strong">Warning:</strong> Writing explicit numbers for file permissions is bad
practice.  Not only is it not portable, it also requires everyone who
reads your program to remember what the bits mean.  To make your program
clean use the symbolic names.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Access-Permission.html">How Your Access to a File is Decided</a>, Previous: <a href="File-Owner.html">File Owner</a>, Up: <a href="File-Attributes.html">File Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
