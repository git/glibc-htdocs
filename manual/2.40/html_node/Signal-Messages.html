<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Signal Messages (The GNU C Library)</title>

<meta name="description" content="Signal Messages (The GNU C Library)">
<meta name="keywords" content="Signal Messages (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Standard-Signals.html" rel="up" title="Standard Signals">
<link href="Miscellaneous-Signals.html" rel="prev" title="Miscellaneous Signals">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Signal-Messages">
<div class="nav-panel">
<p>
Previous: <a href="Miscellaneous-Signals.html" accesskey="p" rel="prev">Miscellaneous Signals</a>, Up: <a href="Standard-Signals.html" accesskey="u" rel="up">Standard Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Signal-Messages-1"><span>25.2.8 Signal Messages<a class="copiable-link" href="#Signal-Messages-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-signal-messages"></a>

<p>We mentioned above that the shell prints a message describing the signal
that terminated a child process.  The clean way to print a message
describing a signal is to use the functions <code class="code">strsignal</code> and
<code class="code">psignal</code>.  These functions use a signal number to specify which
kind of signal to describe.  The signal number may come from the
termination status of a child process (see <a class="pxref" href="Process-Completion.html">Process Completion</a>) or it
may come from a signal handler in the same process.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strsignal"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">strsignal</strong> <code class="def-code-arguments">(int <var class="var">signum</var>)</code><a class="copiable-link" href="#index-strsignal"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:strsignal locale
| AS-Unsafe init i18n corrupt heap
| AC-Unsafe init corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns a pointer to a statically-allocated string
containing a message describing the signal <var class="var">signum</var>.  You
should not modify the contents of this string; and, since it can be
rewritten on subsequent calls, you should save a copy of it if you need
to reference it later.
</p>
<a class="index-entry-id" id="index-string_002eh-9"></a>
<p>This function is a GNU extension, declared in the header file
<samp class="file">string.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-psignal"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">psignal</strong> <code class="def-code-arguments">(int <var class="var">signum</var>, const char *<var class="var">message</var>)</code><a class="copiable-link" href="#index-psignal"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt i18n heap
| AC-Unsafe lock corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function prints a message describing the signal <var class="var">signum</var> to the
standard error output stream <code class="code">stderr</code>; see <a class="ref" href="Standard-Streams.html">Standard Streams</a>.
</p>
<p>If you call <code class="code">psignal</code> with a <var class="var">message</var> that is either a null
pointer or an empty string, <code class="code">psignal</code> just prints the message
corresponding to <var class="var">signum</var>, adding a trailing newline.
</p>
<p>If you supply a non-null <var class="var">message</var> argument, then <code class="code">psignal</code>
prefixes its output with this string.  It adds a colon and a space
character to separate the <var class="var">message</var> from the string corresponding
to <var class="var">signum</var>.
</p>
<a class="index-entry-id" id="index-stdio_002eh-18"></a>
<p>This function is a BSD feature, declared in the header file <samp class="file">signal.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sigdescr_005fnp"><span class="category-def">Function: </span><span><code class="def-type">const char *</code> <strong class="def-name">sigdescr_np</strong> <code class="def-code-arguments">(int <var class="var">signum</var>)</code><a class="copiable-link" href="#index-sigdescr_005fnp"> &para;</a></span></dt>
<dd>
<p>| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the message describing the signal <var class="var">signum</var> or
<code class="code">NULL</code> for invalid signal number (e.g &quot;Hangup&quot; for <code class="code">SIGHUP</code>).
Different than <code class="code">strsignal</code> the returned description is not translated.
The message points to a static storage whose lifetime is the whole lifetime
of the program.
</p>
<a class="index-entry-id" id="index-string_002eh-10"></a>
<p>This function is a GNU extension, declared in the header file <samp class="file">string.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sigabbrev_005fnp"><span class="category-def">Function: </span><span><code class="def-type">const char *</code> <strong class="def-name">sigabbrev_np</strong> <code class="def-code-arguments">(int <var class="var">signum</var>)</code><a class="copiable-link" href="#index-sigabbrev_005fnp"> &para;</a></span></dt>
<dd>
<p>| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the abbreviation describing the signal <var class="var">signum</var> or
<code class="code">NULL</code> for invalid signal number.  The message points to a static
storage whose lifetime is the whole lifetime of the program.
</p>
<a class="index-entry-id" id="index-string_002eh-11"></a>
<p>This function is a GNU extension, declared in the header file <samp class="file">string.h</samp>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Miscellaneous-Signals.html">Miscellaneous Signals</a>, Up: <a href="Standard-Signals.html">Standard Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
