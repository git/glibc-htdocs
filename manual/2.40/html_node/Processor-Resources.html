<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Processor Resources (The GNU C Library)</title>

<meta name="description" content="Processor Resources (The GNU C Library)">
<meta name="keywords" content="Processor Resources (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Resource-Usage-And-Limitation.html" rel="up" title="Resource Usage And Limitation">
<link href="Memory-Resources.html" rel="prev" title="Memory Resources">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Processor-Resources">
<div class="nav-panel">
<p>
Previous: <a href="Memory-Resources.html" accesskey="p" rel="prev">Querying memory available resources</a>, Up: <a href="Resource-Usage-And-Limitation.html" accesskey="u" rel="up">Resource Usage And Limitation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Learn-about-the-processors-available"><span>23.5 Learn about the processors available<a class="copiable-link" href="#Learn-about-the-processors-available"> &para;</a></span></h3>

<p>The use of threads or processes with shared memory allows an application
to take advantage of all the processing power a system can provide.  If
the task can be parallelized the optimal way to write an application is
to have at any time as many processes running as there are processors.
To determine the number of processors available to the system one can
run
</p>
<a class="index-entry-id" id="index-_005fSC_005fNPROCESSORS_005fCONF"></a>
<a class="index-entry-id" id="index-sysconf-2"></a>
<div class="example smallexample">
<pre class="example-preformatted">  sysconf (_SC_NPROCESSORS_CONF)
</pre></div>

<p>which returns the number of processors the operating system configured.
But it might be possible for the operating system to disable individual
processors and so the call
</p>
<a class="index-entry-id" id="index-_005fSC_005fNPROCESSORS_005fONLN"></a>
<a class="index-entry-id" id="index-sysconf-3"></a>
<div class="example smallexample">
<pre class="example-preformatted">  sysconf (_SC_NPROCESSORS_ONLN)
</pre></div>

<p>returns the number of processors which are currently online (i.e.,
available).
</p>
<p>For these two pieces of information the GNU C Library also provides
functions to get the information directly.  The functions are declared
in <samp class="file">sys/sysinfo.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-get_005fnprocs_005fconf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">get_nprocs_conf</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-get_005fnprocs_005fconf"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap lock
| AC-Unsafe lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">get_nprocs_conf</code> function returns the number of processors the
operating system configured.
</p>
<p>This function is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-get_005fnprocs"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">get_nprocs</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-get_005fnprocs"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">get_nprocs</code> function returns the number of available processors.
</p>
<p>This function is a GNU extension.
</p></dd></dl>

<a class="index-entry-id" id="index-load-average"></a>
<p>Before starting more threads it should be checked whether the processors
are not already overused.  Unix systems calculate something called the
<em class="dfn">load average</em>.  This is a number indicating how many processes were
running.  This number is an average over different periods of time
(normally 1, 5, and 15 minutes).
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getloadavg"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getloadavg</strong> <code class="def-code-arguments">(double <var class="var">loadavg</var>[], int <var class="var">nelem</var>)</code><a class="copiable-link" href="#index-getloadavg"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function gets the 1, 5 and 15 minute load averages of the
system.  The values are placed in <var class="var">loadavg</var>.  <code class="code">getloadavg</code> will
place at most <var class="var">nelem</var> elements into the array but never more than
three elements.  The return value is the number of elements written to
<var class="var">loadavg</var>, or -1 on error.
</p>
<p>This function is declared in <samp class="file">stdlib.h</samp>.
</p></dd></dl>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Memory-Resources.html">Querying memory available resources</a>, Up: <a href="Resource-Usage-And-Limitation.html">Resource Usage And Limitation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
