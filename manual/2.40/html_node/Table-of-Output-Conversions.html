<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Table of Output Conversions (The GNU C Library)</title>

<meta name="description" content="Table of Output Conversions (The GNU C Library)">
<meta name="keywords" content="Table of Output Conversions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Output.html" rel="up" title="Formatted Output">
<link href="Integer-Conversions.html" rel="next" title="Integer Conversions">
<link href="Output-Conversion-Syntax.html" rel="prev" title="Output Conversion Syntax">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Table-of-Output-Conversions">
<div class="nav-panel">
<p>
Next: <a href="Integer-Conversions.html" accesskey="n" rel="next">Integer Conversions</a>, Previous: <a href="Output-Conversion-Syntax.html" accesskey="p" rel="prev">Output Conversion Syntax</a>, Up: <a href="Formatted-Output.html" accesskey="u" rel="up">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Table-of-Output-Conversions-1"><span>12.12.3 Table of Output Conversions<a class="copiable-link" href="#Table-of-Output-Conversions-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-output-conversions_002c-for-printf"></a>

<p>Here is a table summarizing what all the different conversions do:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">%d</samp>&rsquo;, &lsquo;<samp class="samp">%i</samp>&rsquo;</dt>
<dd><p>Print an integer as a signed decimal number.  See <a class="xref" href="Integer-Conversions.html">Integer Conversions</a>, for details.  &lsquo;<samp class="samp">%d</samp>&rsquo; and &lsquo;<samp class="samp">%i</samp>&rsquo; are synonymous for
output, but are different when used with <code class="code">scanf</code> for input
(see <a class="pxref" href="Table-of-Input-Conversions.html">Table of Input Conversions</a>).
</p>
</dd>
<dt>&lsquo;<samp class="samp">%b</samp>&rsquo;, &lsquo;<samp class="samp">%B</samp>&rsquo;</dt>
<dd><p>Print an integer as an unsigned binary number.  &lsquo;<samp class="samp">%b</samp>&rsquo; uses
lower-case &lsquo;<samp class="samp">b</samp>&rsquo; with the &lsquo;<samp class="samp">#</samp>&rsquo; flag and &lsquo;<samp class="samp">%B</samp>&rsquo; uses
upper-case.  &lsquo;<samp class="samp">%b</samp>&rsquo; is an ISO C23 feature; &lsquo;<samp class="samp">%B</samp>&rsquo; is an
optional ISO C23 feature.  See <a class="xref" href="Integer-Conversions.html">Integer Conversions</a>, for
details.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%o</samp>&rsquo;</dt>
<dd><p>Print an integer as an unsigned octal number.  See <a class="xref" href="Integer-Conversions.html">Integer Conversions</a>, for details.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%u</samp>&rsquo;</dt>
<dd><p>Print an integer as an unsigned decimal number.  See <a class="xref" href="Integer-Conversions.html">Integer Conversions</a>, for details.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%x</samp>&rsquo;, &lsquo;<samp class="samp">%X</samp>&rsquo;</dt>
<dd><p>Print an integer as an unsigned hexadecimal number.  &lsquo;<samp class="samp">%x</samp>&rsquo; uses
lower-case letters and &lsquo;<samp class="samp">%X</samp>&rsquo; uses upper-case.  See <a class="xref" href="Integer-Conversions.html">Integer Conversions</a>, for details.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%f</samp>&rsquo;, &lsquo;<samp class="samp">%F</samp>&rsquo;</dt>
<dd><p>Print a floating-point number in normal (fixed-point) notation.
&lsquo;<samp class="samp">%f</samp>&rsquo; uses lower-case letters and &lsquo;<samp class="samp">%F</samp>&rsquo; uses upper-case.
See <a class="xref" href="Floating_002dPoint-Conversions.html">Floating-Point Conversions</a>, for details.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%e</samp>&rsquo;, &lsquo;<samp class="samp">%E</samp>&rsquo;</dt>
<dd><p>Print a floating-point number in exponential notation.  &lsquo;<samp class="samp">%e</samp>&rsquo; uses
lower-case letters and &lsquo;<samp class="samp">%E</samp>&rsquo; uses upper-case.  See <a class="xref" href="Floating_002dPoint-Conversions.html">Floating-Point Conversions</a>, for details.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%g</samp>&rsquo;, &lsquo;<samp class="samp">%G</samp>&rsquo;</dt>
<dd><p>Print a floating-point number in either normal or exponential notation,
whichever is more appropriate for its magnitude.  &lsquo;<samp class="samp">%g</samp>&rsquo; uses
lower-case letters and &lsquo;<samp class="samp">%G</samp>&rsquo; uses upper-case.  See <a class="xref" href="Floating_002dPoint-Conversions.html">Floating-Point Conversions</a>, for details.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%a</samp>&rsquo;, &lsquo;<samp class="samp">%A</samp>&rsquo;</dt>
<dd><p>Print a floating-point number in a hexadecimal fractional notation with
the exponent to base 2 represented in decimal digits.  &lsquo;<samp class="samp">%a</samp>&rsquo; uses
lower-case letters and &lsquo;<samp class="samp">%A</samp>&rsquo; uses upper-case.  See <a class="xref" href="Floating_002dPoint-Conversions.html">Floating-Point Conversions</a>, for details.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%c</samp>&rsquo;</dt>
<dd><p>Print a single character.  See <a class="xref" href="Other-Output-Conversions.html">Other Output Conversions</a>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%C</samp>&rsquo;</dt>
<dd><p>This is an alias for &lsquo;<samp class="samp">%lc</samp>&rsquo; which is supported for compatibility
with the Unix standard.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%s</samp>&rsquo;</dt>
<dd><p>Print a string.  See <a class="xref" href="Other-Output-Conversions.html">Other Output Conversions</a>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%S</samp>&rsquo;</dt>
<dd><p>This is an alias for &lsquo;<samp class="samp">%ls</samp>&rsquo; which is supported for compatibility
with the Unix standard.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%p</samp>&rsquo;</dt>
<dd><p>Print the value of a pointer.  See <a class="xref" href="Other-Output-Conversions.html">Other Output Conversions</a>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%n</samp>&rsquo;</dt>
<dd><p>Get the number of characters printed so far.  See <a class="xref" href="Other-Output-Conversions.html">Other Output Conversions</a>.
Note that this conversion specification never produces any output.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%m</samp>&rsquo;</dt>
<dd><p>Print the string corresponding to the value of <code class="code">errno</code>.
(This is a GNU extension.)
See <a class="xref" href="Other-Output-Conversions.html">Other Output Conversions</a>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">%%</samp>&rsquo;</dt>
<dd><p>Print a literal &lsquo;<samp class="samp">%</samp>&rsquo; character.  See <a class="xref" href="Other-Output-Conversions.html">Other Output Conversions</a>.
</p></dd>
</dl>

<p>If the syntax of a conversion specification is invalid, unpredictable
things will happen, so don&rsquo;t do this.  If there aren&rsquo;t enough function
arguments provided to supply values for all the conversion
specifications in the template string, or if the arguments are not of
the correct types, the results are unpredictable.  If you supply more
arguments than conversion specifications, the extra argument values are
simply ignored; this is sometimes useful.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Integer-Conversions.html">Integer Conversions</a>, Previous: <a href="Output-Conversion-Syntax.html">Output Conversion Syntax</a>, Up: <a href="Formatted-Output.html">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
