<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Foreground and Background (The GNU C Library)</title>

<meta name="description" content="Foreground and Background (The GNU C Library)">
<meta name="keywords" content="Foreground and Background (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Implementing-a-Shell.html" rel="up" title="Implementing a Shell">
<link href="Stopped-and-Terminated-Jobs.html" rel="next" title="Stopped and Terminated Jobs">
<link href="Launching-Jobs.html" rel="prev" title="Launching Jobs">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Foreground-and-Background">
<div class="nav-panel">
<p>
Next: <a href="Stopped-and-Terminated-Jobs.html" accesskey="n" rel="next">Stopped and Terminated Jobs</a>, Previous: <a href="Launching-Jobs.html" accesskey="p" rel="prev">Launching Jobs</a>, Up: <a href="Implementing-a-Shell.html" accesskey="u" rel="up">Implementing a Job Control Shell</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Foreground-and-Background-1"><span>29.5.4 Foreground and Background<a class="copiable-link" href="#Foreground-and-Background-1"> &para;</a></span></h4>

<p>Now let&rsquo;s consider what actions must be taken by the shell when it
launches a job into the foreground, and how this differs from what
must be done when a background job is launched.
</p>
<a class="index-entry-id" id="index-foreground-job_002c-launching"></a>
<p>When a foreground job is launched, the shell must first give it access
to the controlling terminal by calling <code class="code">tcsetpgrp</code>.  Then, the
shell should wait for processes in that process group to terminate or
stop.  This is discussed in more detail in <a class="ref" href="Stopped-and-Terminated-Jobs.html">Stopped and Terminated Jobs</a>.
</p>
<p>When all of the processes in the group have either completed or stopped,
the shell should regain control of the terminal for its own process
group by calling <code class="code">tcsetpgrp</code> again.  Since stop signals caused by
I/O from a background process or a SUSP character typed by the user
are sent to the process group, normally all the processes in the job
stop together.
</p>
<p>The foreground job may have left the terminal in a strange state, so the
shell should restore its own saved terminal modes before continuing.  In
case the job is merely stopped, the shell should first save the current
terminal modes so that it can restore them later if the job is
continued.  The functions for dealing with terminal modes are
<code class="code">tcgetattr</code> and <code class="code">tcsetattr</code>; these are described in
<a class="ref" href="Terminal-Modes.html">Terminal Modes</a>.
</p>
<p>Here is the sample shell&rsquo;s function for doing all of this.
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">/* <span class="r">Put job <var class="var">j</var> in the foreground.  If <var class="var">cont</var> is nonzero,</span>
   <span class="r">restore the saved terminal modes and send the process group a</span>
   <span class="r"><code class="code">SIGCONT</code> signal to wake it up before we block.</span>  */

void
put_job_in_foreground (job *j, int cont)
{
  /* <span class="r">Put the job into the foreground.</span>  */
  tcsetpgrp (shell_terminal, j-&gt;pgid);
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">  /* <span class="r">Send the job a continue signal, if necessary.</span>  */
  if (cont)
    {
      tcsetattr (shell_terminal, TCSADRAIN, &amp;j-&gt;tmodes);
      if (kill (- j-&gt;pgid, SIGCONT) &lt; 0)
        perror (&quot;kill (SIGCONT)&quot;);
    }
</pre></div><pre class="example-preformatted">

  /* <span class="r">Wait for it to report.</span>  */
  wait_for_job (j);

  /* <span class="r">Put the shell back in the foreground.</span>  */
  tcsetpgrp (shell_terminal, shell_pgid);

</pre><div class="group"><pre class="example-preformatted">  /* <span class="r">Restore the shell&rsquo;s terminal modes.</span>  */
  tcgetattr (shell_terminal, &amp;j-&gt;tmodes);
  tcsetattr (shell_terminal, TCSADRAIN, &amp;shell_tmodes);
}
</pre></div></div>

<a class="index-entry-id" id="index-background-job_002c-launching"></a>
<p>If the process group is launched as a background job, the shell should
remain in the foreground itself and continue to read commands from
the terminal.
</p>
<p>In the sample shell, there is not much that needs to be done to put
a job into the background.  Here is the function it uses:
</p>
<div class="example smallexample">
<pre class="example-preformatted">/* <span class="r">Put a job in the background.  If the cont argument is true, send</span>
   <span class="r">the process group a <code class="code">SIGCONT</code> signal to wake it up.</span>  */

void
put_job_in_background (job *j, int cont)
{
  /* <span class="r">Send the job a continue signal, if necessary.</span>  */
  if (cont)
    if (kill (-j-&gt;pgid, SIGCONT) &lt; 0)
      perror (&quot;kill (SIGCONT)&quot;);
}
</pre></div>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Stopped-and-Terminated-Jobs.html">Stopped and Terminated Jobs</a>, Previous: <a href="Launching-Jobs.html">Launching Jobs</a>, Up: <a href="Implementing-a-Shell.html">Implementing a Job Control Shell</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
