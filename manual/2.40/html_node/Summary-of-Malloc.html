<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Summary of Malloc (The GNU C Library)</title>

<meta name="description" content="Summary of Malloc (The GNU C Library)">
<meta name="keywords" content="Summary of Malloc (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Unconstrained-Allocation.html" rel="up" title="Unconstrained Allocation">
<link href="Statistics-of-Malloc.html" rel="prev" title="Statistics of Malloc">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Summary-of-Malloc">
<div class="nav-panel">
<p>
Previous: <a href="Statistics-of-Malloc.html" accesskey="p" rel="prev">Statistics for Memory Allocation with <code class="code">malloc</code></a>, Up: <a href="Unconstrained-Allocation.html" accesskey="u" rel="up">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Summary-of-malloc_002dRelated-Functions"><span>3.2.3.10 Summary of <code class="code">malloc</code>-Related Functions<a class="copiable-link" href="#Summary-of-malloc_002dRelated-Functions"> &para;</a></span></h4>

<p>Here is a summary of the functions that work with <code class="code">malloc</code>:
</p>
<dl class="table">
<dt><code class="code">void *malloc (size_t <var class="var">size</var>)</code></dt>
<dd><p>Allocate a block of <var class="var">size</var> bytes.  See <a class="xref" href="Basic-Allocation.html">Basic Memory Allocation</a>.
</p>
</dd>
<dt><code class="code">void free (void *<var class="var">addr</var>)</code></dt>
<dd><p>Free a block previously allocated by <code class="code">malloc</code>.  See <a class="xref" href="Freeing-after-Malloc.html">Freeing Memory Allocated with <code class="code">malloc</code></a>.
</p>
</dd>
<dt><code class="code">void *realloc (void *<var class="var">addr</var>, size_t <var class="var">size</var>)</code></dt>
<dd><p>Make a block previously allocated by <code class="code">malloc</code> larger or smaller,
possibly by copying it to a new location.  See <a class="xref" href="Changing-Block-Size.html">Changing the Size of a Block</a>.
</p>
</dd>
<dt><code class="code">void *reallocarray (void *<var class="var">ptr</var>, size_t <var class="var">nmemb</var>, size_t <var class="var">size</var>)</code></dt>
<dd><p>Change the size of a block previously allocated by <code class="code">malloc</code> to
<code class="code"><var class="var">nmemb</var> * <var class="var">size</var></code> bytes as with <code class="code">realloc</code>.  See <a class="xref" href="Changing-Block-Size.html">Changing the Size of a Block</a>.
</p>
</dd>
<dt><code class="code">void *calloc (size_t <var class="var">count</var>, size_t <var class="var">eltsize</var>)</code></dt>
<dd><p>Allocate a block of <var class="var">count</var> * <var class="var">eltsize</var> bytes using
<code class="code">malloc</code>, and set its contents to zero.  See <a class="xref" href="Allocating-Cleared-Space.html">Allocating Cleared Space</a>.
</p>
</dd>
<dt><code class="code">void *valloc (size_t <var class="var">size</var>)</code></dt>
<dd><p>Allocate a block of <var class="var">size</var> bytes, starting on a page boundary.
See <a class="xref" href="Aligned-Memory-Blocks.html">Allocating Aligned Memory Blocks</a>.
</p>
</dd>
<dt><code class="code">void *aligned_alloc (size_t <var class="var">alignment</var>, size_t <var class="var">size</var>)</code></dt>
<dd><p>Allocate a block of <var class="var">size</var> bytes, starting on an address that is a
multiple of <var class="var">alignment</var>.  See <a class="xref" href="Aligned-Memory-Blocks.html">Allocating Aligned Memory Blocks</a>.
</p>
</dd>
<dt><code class="code">int posix_memalign (void **<var class="var">memptr</var>, size_t <var class="var">alignment</var>, size_t <var class="var">size</var>)</code></dt>
<dd><p>Allocate a block of <var class="var">size</var> bytes, starting on an address that is a
multiple of <var class="var">alignment</var>.  See <a class="xref" href="Aligned-Memory-Blocks.html">Allocating Aligned Memory Blocks</a>.
</p>
</dd>
<dt><code class="code">void *memalign (size_t <var class="var">boundary</var>, size_t <var class="var">size</var>)</code></dt>
<dd><p>Allocate a block of <var class="var">size</var> bytes, starting on an address that is a
multiple of <var class="var">boundary</var>.  See <a class="xref" href="Aligned-Memory-Blocks.html">Allocating Aligned Memory Blocks</a>.
</p>
</dd>
<dt><code class="code">int mallopt (int <var class="var">param</var>, int <var class="var">value</var>)</code></dt>
<dd><p>Adjust a tunable parameter.  See <a class="xref" href="Malloc-Tunable-Parameters.html">Malloc Tunable Parameters</a>.
</p>
</dd>
<dt><code class="code">int mcheck (void (*<var class="var">abortfn</var>) (void))</code></dt>
<dd><p>Tell <code class="code">malloc</code> to perform occasional consistency checks on
dynamically allocated memory, and to call <var class="var">abortfn</var> when an
inconsistency is found.  See <a class="xref" href="Heap-Consistency-Checking.html">Heap Consistency Checking</a>.
</p>
</dd>
<dt><code class="code">struct mallinfo2 mallinfo2 (void)</code></dt>
<dd><p>Return information about the current dynamic memory usage.
See <a class="xref" href="Statistics-of-Malloc.html">Statistics for Memory Allocation with <code class="code">malloc</code></a>.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Statistics-of-Malloc.html">Statistics for Memory Allocation with <code class="code">malloc</code></a>, Up: <a href="Unconstrained-Allocation.html">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
