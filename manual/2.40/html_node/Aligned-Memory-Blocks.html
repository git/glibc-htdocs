<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Aligned Memory Blocks (The GNU C Library)</title>

<meta name="description" content="Aligned Memory Blocks (The GNU C Library)">
<meta name="keywords" content="Aligned Memory Blocks (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Unconstrained-Allocation.html" rel="up" title="Unconstrained Allocation">
<link href="Malloc-Tunable-Parameters.html" rel="next" title="Malloc Tunable Parameters">
<link href="Allocating-Cleared-Space.html" rel="prev" title="Allocating Cleared Space">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Aligned-Memory-Blocks">
<div class="nav-panel">
<p>
Next: <a href="Malloc-Tunable-Parameters.html" accesskey="n" rel="next">Malloc Tunable Parameters</a>, Previous: <a href="Allocating-Cleared-Space.html" accesskey="p" rel="prev">Allocating Cleared Space</a>, Up: <a href="Unconstrained-Allocation.html" accesskey="u" rel="up">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Allocating-Aligned-Memory-Blocks"><span>3.2.3.6 Allocating Aligned Memory Blocks<a class="copiable-link" href="#Allocating-Aligned-Memory-Blocks"> &para;</a></span></h4>

<a class="index-entry-id" id="index-page-boundary"></a>
<a class="index-entry-id" id="index-alignment-_0028with-malloc_0029"></a>
<a class="index-entry-id" id="index-stdlib_002eh-4"></a>
<p>The address of a block returned by <code class="code">malloc</code> or <code class="code">realloc</code> in
GNU systems is always a multiple of eight (or sixteen on 64-bit
systems).  If you need a block whose address is a multiple of a higher
power of two than that, use <code class="code">aligned_alloc</code> or <code class="code">posix_memalign</code>.
<code class="code">aligned_alloc</code> and <code class="code">posix_memalign</code> are declared in
<samp class="file">stdlib.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-aligned_005falloc"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">aligned_alloc</strong> <code class="def-code-arguments">(size_t <var class="var">alignment</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-aligned_005falloc"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">aligned_alloc</code> function allocates a block of <var class="var">size</var> bytes whose
address is a multiple of <var class="var">alignment</var>.  The <var class="var">alignment</var> must be a
power of two.
</p>
<p>The <code class="code">aligned_alloc</code> function returns a null pointer on error and sets
<code class="code">errno</code> to one of the following values:
</p>
<dl class="table">
<dt><code class="code">ENOMEM</code></dt>
<dd><p>There was insufficient memory available to satisfy the request.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p><var class="var">alignment</var> is not a power of two.
</p>
<p>This function was introduced in ISO&nbsp;C11<!-- /@w --> and hence may have better
portability to modern non-POSIX systems than <code class="code">posix_memalign</code>.
</p></dd>
</dl>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-memalign"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">memalign</strong> <code class="def-code-arguments">(size_t <var class="var">boundary</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-memalign"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">memalign</code> function allocates a block of <var class="var">size</var> bytes whose
address is a multiple of <var class="var">boundary</var>.  The <var class="var">boundary</var> must be a
power of two!  The function <code class="code">memalign</code> works by allocating a
somewhat larger block, and then returning an address within the block
that is on the specified boundary.
</p>
<p>The <code class="code">memalign</code> function returns a null pointer on error and sets
<code class="code">errno</code> to one of the following values:
</p>
<dl class="table">
<dt><code class="code">ENOMEM</code></dt>
<dd><p>There was insufficient memory available to satisfy the request.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p><var class="var">boundary</var> is not a power of two.
</p>
</dd>
</dl>

<p>The <code class="code">memalign</code> function is obsolete and <code class="code">aligned_alloc</code> or
<code class="code">posix_memalign</code> should be used instead.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-posix_005fmemalign"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">posix_memalign</strong> <code class="def-code-arguments">(void **<var class="var">memptr</var>, size_t <var class="var">alignment</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-posix_005fmemalign"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">posix_memalign</code> function is similar to the <code class="code">memalign</code>
function in that it returns a buffer of <var class="var">size</var> bytes aligned to a
multiple of <var class="var">alignment</var>.  But it adds one requirement to the
parameter <var class="var">alignment</var>: the value must be a power of two multiple of
<code class="code">sizeof (void *)</code>.
</p>
<p>If the function succeeds in allocation memory a pointer to the allocated
memory is returned in <code class="code">*<var class="var">memptr</var></code> and the return value is zero.
Otherwise the function returns an error value indicating the problem.
The possible error values returned are:
</p>
<dl class="table">
<dt><code class="code">ENOMEM</code></dt>
<dd><p>There was insufficient memory available to satisfy the request.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p><var class="var">alignment</var> is not a power of two multiple of <code class="code">sizeof (void *)</code>.
</p>
</dd>
</dl>

<p>This function was introduced in POSIX 1003.1d.  Although this function is
superseded by <code class="code">aligned_alloc</code>, it is more portable to older POSIX
systems that do not support ISO&nbsp;C11<!-- /@w -->.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-valloc"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">valloc</strong> <code class="def-code-arguments">(size_t <var class="var">size</var>)</code><a class="copiable-link" href="#index-valloc"> &para;</a></span></dt>
<dd>

<p>Preliminary:
| MT-Unsafe init
| AS-Unsafe init lock
| AC-Unsafe init lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Using <code class="code">valloc</code> is like using <code class="code">memalign</code> and passing the page size
as the value of the first argument.  It is implemented like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">void *
valloc (size_t size)
{
  return memalign (getpagesize (), size);
}
</pre></div>

<p><a class="ref" href="Query-Memory-Parameters.html">How to get information about the memory subsystem?</a> for more information about the memory
subsystem.
</p>
<p>The <code class="code">valloc</code> function is obsolete and <code class="code">aligned_alloc</code> or
<code class="code">posix_memalign</code> should be used instead.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Malloc-Tunable-Parameters.html">Malloc Tunable Parameters</a>, Previous: <a href="Allocating-Cleared-Space.html">Allocating Cleared Space</a>, Up: <a href="Unconstrained-Allocation.html">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
