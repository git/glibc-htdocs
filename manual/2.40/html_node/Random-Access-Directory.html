<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Random Access Directory (The GNU C Library)</title>

<meta name="description" content="Random Access Directory (The GNU C Library)">
<meta name="keywords" content="Random Access Directory (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Accessing-Directories.html" rel="up" title="Accessing Directories">
<link href="Scanning-Directory-Content.html" rel="next" title="Scanning Directory Content">
<link href="Simple-Directory-Lister.html" rel="prev" title="Simple Directory Lister">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Random-Access-Directory">
<div class="nav-panel">
<p>
Next: <a href="Scanning-Directory-Content.html" accesskey="n" rel="next">Scanning the Content of a Directory</a>, Previous: <a href="Simple-Directory-Lister.html" accesskey="p" rel="prev">Simple Program to List a Directory</a>, Up: <a href="Accessing-Directories.html" accesskey="u" rel="up">Accessing Directories</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Random-Access-in-a-Directory-Stream"><span>14.2.5 Random Access in a Directory Stream<a class="copiable-link" href="#Random-Access-in-a-Directory-Stream"> &para;</a></span></h4>

<a class="index-entry-id" id="index-dirent_002eh-4"></a>
<p>This section describes how to reread parts of a directory that you have
already read from an open directory stream.  All the symbols are
declared in the header file <samp class="file">dirent.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-rewinddir"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">rewinddir</strong> <code class="def-code-arguments">(DIR *<var class="var">dirstream</var>)</code><a class="copiable-link" href="#index-rewinddir"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">rewinddir</code> function is used to reinitialize the directory
stream <var class="var">dirstream</var>, so that if you call <code class="code">readdir</code> it
returns information about the first entry in the directory again.  This
function also notices if files have been added or removed to the
directory since it was opened with <code class="code">opendir</code>.  (Entries for these
files might or might not be returned by <code class="code">readdir</code> if they were
added or removed since you last called <code class="code">opendir</code> or
<code class="code">rewinddir</code>.)
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-telldir"><span class="category-def">Function: </span><span><code class="def-type">long int</code> <strong class="def-name">telldir</strong> <code class="def-code-arguments">(DIR *<var class="var">dirstream</var>)</code><a class="copiable-link" href="#index-telldir"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap/bsd lock/bsd
| AC-Unsafe mem/bsd lock/bsd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">telldir</code> function returns the file position of the directory
stream <var class="var">dirstream</var>.  You can use this value with <code class="code">seekdir</code> to
restore the directory stream to that position.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-seekdir"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">seekdir</strong> <code class="def-code-arguments">(DIR *<var class="var">dirstream</var>, long int <var class="var">pos</var>)</code><a class="copiable-link" href="#index-seekdir"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap/bsd lock/bsd
| AC-Unsafe mem/bsd lock/bsd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">seekdir</code> function sets the file position of the directory
stream <var class="var">dirstream</var> to <var class="var">pos</var>.  The value <var class="var">pos</var> must be the
result of a previous call to <code class="code">telldir</code> on this particular stream;
closing and reopening the directory can invalidate values returned by
<code class="code">telldir</code>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Scanning-Directory-Content.html">Scanning the Content of a Directory</a>, Previous: <a href="Simple-Directory-Lister.html">Simple Program to List a Directory</a>, Up: <a href="Accessing-Directories.html">Accessing Directories</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
