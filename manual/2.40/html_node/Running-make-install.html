<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Running make install (The GNU C Library)</title>

<meta name="description" content="Running make install (The GNU C Library)">
<meta name="keywords" content="Running make install (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Installation.html" rel="up" title="Installation">
<link href="Tools-for-Compilation.html" rel="next" title="Tools for Compilation">
<link href="Configuring-and-compiling.html" rel="prev" title="Configuring and compiling">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="appendixsec-level-extent" id="Running-make-install">
<div class="nav-panel">
<p>
Next: <a href="Tools-for-Compilation.html" accesskey="n" rel="next">Recommended Tools for Compilation</a>, Previous: <a href="Configuring-and-compiling.html" accesskey="p" rel="prev">Configuring and compiling the GNU C Library</a>, Up: <a href="Installation.html" accesskey="u" rel="up">Installing the GNU C Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="appendixsec" id="Installing-the-C-Library"><span>C.2 Installing the C Library<a class="copiable-link" href="#Installing-the-C-Library"> &para;</a></span></h3>
<a class="index-entry-id" id="index-installing"></a>

<p>To install the library and its header files, and the Info files of the
manual, type <code class="code">make install</code>.  This will
build things, if necessary, before installing them; however, you should
still compile everything first.  If you are installing the GNU C Library as your
primary C library, we recommend that you shut the system down to
single-user mode first, and reboot afterward.  This minimizes the risk
of breaking things when the library changes out from underneath.
</p>
<p>&lsquo;<samp class="samp">make install</samp>&rsquo; will do the entire job of upgrading from a
previous installation of the GNU C Library version 2.x.  There may sometimes
be headers
left behind from the previous installation, but those are generally
harmless.  If you want to avoid leaving headers behind you can do
things in the following order.
</p>
<p>You must first build the library (&lsquo;<samp class="samp">make</samp>&rsquo;), optionally check it
(&lsquo;<samp class="samp">make check</samp>&rsquo;), switch the include directories and then install
(&lsquo;<samp class="samp">make install</samp>&rsquo;).  The steps must be done in this order.  Not moving
the directory before install will result in an unusable mixture of header
files from both libraries, but configuring, building, and checking the
library requires the ability to compile and run programs against the old
library.  The new <samp class="file">/usr/include</samp>, after switching the include
directories and before installing the library should contain the Linux
headers, but nothing else.  If you do this, you will need to restore
any headers from libraries other than the GNU C Library yourself after installing the
library.
</p>
<p>You can install the GNU C Library somewhere other than where you configured
it to go by setting the <code class="code">DESTDIR</code> GNU standard make variable on
the command line for &lsquo;<samp class="samp">make install</samp>&rsquo;.  The value of this variable
is prepended to all the paths for installation.  This is useful when
setting up a chroot environment or preparing a binary distribution.
The directory should be specified with an absolute file name. Installing
with the <code class="code">prefix</code> and <code class="code">exec_prefix</code> GNU standard make variables
set is not supported.
</p>
<p>The GNU C Library includes a daemon called <code class="code">nscd</code>, which you
may or may not want to run.  <code class="code">nscd</code> caches name service lookups; it
can dramatically improve performance with NIS+, and may help with DNS as
well.
</p>
<p>One auxiliary program, <samp class="file">/usr/libexec/pt_chown</samp>, is installed setuid
<code class="code">root</code> if the &lsquo;<samp class="samp">--enable-pt_chown</samp>&rsquo; configuration option is used.
This program is invoked by the <code class="code">grantpt</code> function; it sets the
permissions on a pseudoterminal so it can be used by the calling process.
If you are using a Linux kernel with the <code class="code">devpts</code> filesystem enabled
and mounted at <samp class="file">/dev/pts</samp>, you don&rsquo;t need this program.
</p>
<p>After installation you should configure the time zone ruleset and install
locales for your system.  The time zone ruleset ensures that timestamps
are processed correctly for your location.  The locales ensure that
the display of information on your system matches the expectations of
your language and geographic region.
</p>
<p>The GNU C Library is able to use two kinds of localization information sources, the
first is a locale database named <samp class="file">locale-archive</samp> which is generally
installed as <samp class="file">/usr/lib/locale/locale-archive</samp>.  The locale archive has the
benefit of taking up less space and being very fast to load, but only if you
plan to install sixty or more locales.  If you plan to install one or two
locales you can instead install individual locales into their self-named
directories e.g. <samp class="file">/usr/lib/locale/en_US.utf8</samp>.  For example to install
the German locale using the character set for UTF-8 with name <code class="code">de_DE</code> into
the locale archive issue the command &lsquo;<samp class="samp">localedef -i de_DE -f UTF-8 de_DE</samp>&rsquo;,
and to install just the one locale issue the command &lsquo;<samp class="samp">localedef
--no-archive -i de_DE -f UTF-8 de_DE</samp>&rsquo;.  To configure all locales that are
supported by the GNU C Library, you can issue from your build directory the command
&lsquo;<samp class="samp">make localedata/install-locales</samp>&rsquo; to install all locales into the locale
archive or &lsquo;<samp class="samp">make localedata/install-locale-files</samp>&rsquo; to install all locales
as files in the default configured locale installation directory (derived from
&lsquo;<samp class="samp">--prefix</samp>&rsquo; or <code class="code">--localedir</code>).  To install into an alternative system
root use &lsquo;<samp class="samp">DESTDIR</samp>&rsquo; e.g. &lsquo;<samp class="samp">make localedata/install-locale-files
DESTDIR=/opt/glibc</samp>&rsquo;, but note that this does not change the configured prefix.
</p>
<p>To configure the time zone ruleset, set the <code class="code">TZ</code> environment
variable.  The script <code class="code">tzselect</code> helps you to select the right value.
As an example, for Germany, <code class="code">tzselect</code> would tell you to use
&lsquo;<samp class="samp">TZ='Europe/Berlin'</samp>&rsquo;.  For a system wide installation (the given
paths are for an installation with &lsquo;<samp class="samp">--prefix=/usr</samp>&rsquo;), link the
time zone file which is in <samp class="file">/usr/share/zoneinfo</samp> to the file
<samp class="file">/etc/localtime</samp>.  For Germany, you might execute &lsquo;<samp class="samp">ln -s
/usr/share/zoneinfo/Europe/Berlin /etc/localtime</samp>&rsquo;.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Tools-for-Compilation.html">Recommended Tools for Compilation</a>, Previous: <a href="Configuring-and-compiling.html">Configuring and compiling the GNU C Library</a>, Up: <a href="Installation.html">Installing the GNU C Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
