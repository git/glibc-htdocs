<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Reserved Names (The GNU C Library)</title>

<meta name="description" content="Reserved Names (The GNU C Library)">
<meta name="keywords" content="Reserved Names (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Using-the-Library.html" rel="up" title="Using the Library">
<link href="Feature-Test-Macros.html" rel="next" title="Feature Test Macros">
<link href="Macro-Definitions.html" rel="prev" title="Macro Definitions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Reserved-Names">
<div class="nav-panel">
<p>
Next: <a href="Feature-Test-Macros.html" accesskey="n" rel="next">Feature Test Macros</a>, Previous: <a href="Macro-Definitions.html" accesskey="p" rel="prev">Macro Definitions of Functions</a>, Up: <a href="Using-the-Library.html" accesskey="u" rel="up">Using the Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Reserved-Names-1"><span>1.3.3 Reserved Names<a class="copiable-link" href="#Reserved-Names-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-reserved-names"></a>
<a class="index-entry-id" id="index-name-space"></a>

<p>The names of all library types, macros, variables and functions that
come from the ISO&nbsp;C<!-- /@w --> standard are reserved unconditionally; your program
<strong class="strong">may not</strong> redefine these names.  All other library names are
reserved if your program explicitly includes the header file that
defines or declares them.  There are several reasons for these
restrictions:
</p>
<ul class="itemize mark-bullet">
<li>Other people reading your code could get very confused if you were using
a function named <code class="code">exit</code> to do something completely different from
what the standard <code class="code">exit</code> function does, for example.  Preventing
this situation helps to make your programs easier to understand and
contributes to modularity and maintainability.

</li><li>It avoids the possibility of a user accidentally redefining a library
function that is called by other library functions.  If redefinition
were allowed, those other functions would not work properly.

</li><li>It allows the compiler to do whatever special optimizations it pleases
on calls to these functions, without the possibility that they may have
been redefined by the user.  Some library facilities, such as those for
dealing with variadic arguments (see <a class="pxref" href="Variadic-Functions.html">Variadic Functions</a>)
and non-local exits (see <a class="pxref" href="Non_002dLocal-Exits.html">Non-Local Exits</a>), actually require a
considerable amount of cooperation on the part of the C compiler, and
with respect to the implementation, it might be easier for the compiler
to treat these as built-in parts of the language.
</li></ul>

<p>In addition to the names documented in this manual, reserved names
include all external identifiers (global functions and variables) that
begin with an underscore (&lsquo;<samp class="samp">_</samp>&rsquo;) and all identifiers regardless of
use that begin with either two underscores or an underscore followed by
a capital letter are reserved names.  This is so that the library and
header files can define functions, variables, and macros for internal
purposes without risk of conflict with names in user programs.
</p>
<p>Some additional classes of identifier names are reserved for future
extensions to the C language or the POSIX.1 environment.  While using these
names for your own purposes right now might not cause a problem, they do
raise the possibility of conflict with future versions of the C
or POSIX standards, so you should avoid these names.
</p>
<ul class="itemize mark-bullet">
<li>Names beginning with a capital &lsquo;<samp class="samp">E</samp>&rsquo; followed a digit or uppercase
letter may be used for additional error code names.  See <a class="xref" href="Error-Reporting.html">Error Reporting</a>.

</li><li>Names that begin with either &lsquo;<samp class="samp">is</samp>&rsquo; or &lsquo;<samp class="samp">to</samp>&rsquo; followed by a
lowercase letter may be used for additional character testing and
conversion functions.  See <a class="xref" href="Character-Handling.html">Character Handling</a>.

</li><li>Names that begin with &lsquo;<samp class="samp">LC_</samp>&rsquo; followed by an uppercase letter may be
used for additional macros specifying locale attributes.
See <a class="xref" href="Locales.html">Locales and Internationalization</a>.

</li><li>Names of all existing mathematics functions (see <a class="pxref" href="Mathematics.html">Mathematics</a>)
suffixed with &lsquo;<samp class="samp">f</samp>&rsquo; or &lsquo;<samp class="samp">l</samp>&rsquo; are reserved for corresponding
functions that operate on <code class="code">float</code> and <code class="code">long double</code> arguments,
respectively.

</li><li>Names that begin with &lsquo;<samp class="samp">SIG</samp>&rsquo; followed by an uppercase letter are
reserved for additional signal names.  See <a class="xref" href="Standard-Signals.html">Standard Signals</a>.

</li><li>Names that begin with &lsquo;<samp class="samp">SIG_</samp>&rsquo; followed by an uppercase letter are
reserved for additional signal actions.  See <a class="xref" href="Basic-Signal-Handling.html">Basic Signal Handling</a>.

</li><li>Names beginning with &lsquo;<samp class="samp">str</samp>&rsquo;, &lsquo;<samp class="samp">mem</samp>&rsquo;, or &lsquo;<samp class="samp">wcs</samp>&rsquo; followed by a
lowercase letter are reserved for additional string and array functions.
See <a class="xref" href="String-and-Array-Utilities.html">String and Array Utilities</a>.

</li><li>Names that end with &lsquo;<samp class="samp">_t</samp>&rsquo; are reserved for additional type names.
</li></ul>

<p>In addition, some individual header files reserve names beyond
those that they actually define.  You only need to worry about these
restrictions if your program includes that particular header file.
</p>
<ul class="itemize mark-bullet">
<li>The header file <samp class="file">dirent.h</samp> reserves names prefixed with
&lsquo;<samp class="samp">d_</samp>&rsquo;.
<a class="index-entry-id" id="index-dirent_002eh"></a>

</li><li>The header file <samp class="file">fcntl.h</samp> reserves names prefixed with
&lsquo;<samp class="samp">l_</samp>&rsquo;, &lsquo;<samp class="samp">F_</samp>&rsquo;, &lsquo;<samp class="samp">O_</samp>&rsquo;, and &lsquo;<samp class="samp">S_</samp>&rsquo;.
<a class="index-entry-id" id="index-fcntl_002eh"></a>

</li><li>The header file <samp class="file">grp.h</samp> reserves names prefixed with &lsquo;<samp class="samp">gr_</samp>&rsquo;.
<a class="index-entry-id" id="index-grp_002eh"></a>

</li><li>The header file <samp class="file">limits.h</samp> reserves names suffixed with &lsquo;<samp class="samp">_MAX</samp>&rsquo;.
<a class="index-entry-id" id="index-limits_002eh"></a>

</li><li>The header file <samp class="file">pwd.h</samp> reserves names prefixed with &lsquo;<samp class="samp">pw_</samp>&rsquo;.
<a class="index-entry-id" id="index-pwd_002eh"></a>

</li><li>The header file <samp class="file">signal.h</samp> reserves names prefixed with &lsquo;<samp class="samp">sa_</samp>&rsquo;
and &lsquo;<samp class="samp">SA_</samp>&rsquo;.
<a class="index-entry-id" id="index-signal_002eh"></a>

</li><li>The header file <samp class="file">sys/stat.h</samp> reserves names prefixed with &lsquo;<samp class="samp">st_</samp>&rsquo;
and &lsquo;<samp class="samp">S_</samp>&rsquo;.
<a class="index-entry-id" id="index-sys_002fstat_002eh"></a>

</li><li>The header file <samp class="file">sys/times.h</samp> reserves names prefixed with &lsquo;<samp class="samp">tms_</samp>&rsquo;.
<a class="index-entry-id" id="index-sys_002ftimes_002eh"></a>

</li><li>The header file <samp class="file">termios.h</samp> reserves names prefixed with &lsquo;<samp class="samp">c_</samp>&rsquo;,
&lsquo;<samp class="samp">V</samp>&rsquo;, &lsquo;<samp class="samp">I</samp>&rsquo;, &lsquo;<samp class="samp">O</samp>&rsquo;, and &lsquo;<samp class="samp">TC</samp>&rsquo;; and names prefixed with
&lsquo;<samp class="samp">B</samp>&rsquo; followed by a digit.
<a class="index-entry-id" id="index-termios_002eh"></a>
</li></ul>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Feature-Test-Macros.html">Feature Test Macros</a>, Previous: <a href="Macro-Definitions.html">Macro Definitions of Functions</a>, Up: <a href="Using-the-Library.html">Using the Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
