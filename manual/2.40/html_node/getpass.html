<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>getpass (The GNU C Library)</title>

<meta name="description" content="getpass (The GNU C Library)">
<meta name="keywords" content="getpass (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Low_002dLevel-Terminal-Interface.html" rel="up" title="Low-Level Terminal Interface">
<link href="Pseudo_002dTerminals.html" rel="next" title="Pseudo-Terminals">
<link href="Noncanon-Example.html" rel="prev" title="Noncanon Example">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="getpass">
<div class="nav-panel">
<p>
Next: <a href="Pseudo_002dTerminals.html" accesskey="n" rel="next">Pseudo-Terminals</a>, Previous: <a href="Noncanon-Example.html" accesskey="p" rel="prev">Noncanonical Mode Example</a>, Up: <a href="Low_002dLevel-Terminal-Interface.html" accesskey="u" rel="up">Low-Level Terminal Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Reading-Passphrases"><span>17.8 Reading Passphrases<a class="copiable-link" href="#Reading-Passphrases"> &para;</a></span></h3>

<p>When reading in a passphrase, it is desirable to avoid displaying it on
the screen, to help keep it secret.  The following function handles this
in a convenient way.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getpass"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">getpass</strong> <code class="def-code-arguments">(const char *<var class="var">prompt</var>)</code><a class="copiable-link" href="#index-getpass"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe term
| AS-Unsafe heap lock corrupt
| AC-Unsafe term lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p><code class="code">getpass</code> outputs <var class="var">prompt</var>, then reads a string in from the
terminal without echoing it.  It tries to connect to the real terminal,
<samp class="file">/dev/tty</samp>, if possible, to encourage users not to put plaintext
passphrases in files; otherwise, it uses <code class="code">stdin</code> and <code class="code">stderr</code>.
<code class="code">getpass</code> also disables the INTR, QUIT, and SUSP characters on the
terminal using the <code class="code">ISIG</code> terminal attribute (see <a class="pxref" href="Local-Modes.html">Local Modes</a>).
The terminal is flushed before and after <code class="code">getpass</code>, so that
characters of a mistyped passphrase are not accidentally visible.
</p>
<p>In other C libraries, <code class="code">getpass</code> may only return the first
<code class="code">PASS_MAX</code> bytes of a passphrase.  The GNU C Library has no limit, so
<code class="code">PASS_MAX</code> is undefined.
</p>
<p>The prototype for this function is in <samp class="file">unistd.h</samp>.  <code class="code">PASS_MAX</code>
would be defined in <samp class="file">limits.h</samp>.
</p></dd></dl>

<p>This precise set of operations may not suit all possible situations.  In
this case, it is recommended that users write their own <code class="code">getpass</code>
substitute.  For instance, a very simple substitute is as follows:
</p>
<div class="example smallexample">
<pre class="example-preformatted">

#include &lt;termios.h&gt;
#include &lt;stdio.h&gt;

ssize_t
my_getpass (char **lineptr, size_t *n, FILE *stream)
{
  struct termios old, new;
  int nread;

  /* <span class="r">Turn echoing off and fail if we can&rsquo;t.</span> */
  if (tcgetattr (fileno (stream), &amp;old) != 0)
    return -1;
  new = old;
  new.c_lflag &amp;= ~ECHO;
  if (tcsetattr (fileno (stream), TCSAFLUSH, &amp;new) != 0)
    return -1;

  /* <span class="r">Read the passphrase</span> */
  nread = getline (lineptr, n, stream);

  /* <span class="r">Restore terminal.</span> */
  (void) tcsetattr (fileno (stream), TCSAFLUSH, &amp;old);

  return nread;
}
</pre></div>

<p>The substitute takes the same parameters as <code class="code">getline</code>
(see <a class="pxref" href="Line-Input.html">Line-Oriented Input</a>); the user must print any prompt desired.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Pseudo_002dTerminals.html">Pseudo-Terminals</a>, Previous: <a href="Noncanon-Example.html">Noncanonical Mode Example</a>, Up: <a href="Low_002dLevel-Terminal-Interface.html">Low-Level Terminal Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
