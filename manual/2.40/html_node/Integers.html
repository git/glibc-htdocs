<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Integers (The GNU C Library)</title>

<meta name="description" content="Integers (The GNU C Library)">
<meta name="keywords" content="Integers (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Arithmetic.html" rel="up" title="Arithmetic">
<link href="Integer-Division.html" rel="next" title="Integer Division">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Integers">
<div class="nav-panel">
<p>
Next: <a href="Integer-Division.html" accesskey="n" rel="next">Integer Division</a>, Up: <a href="Arithmetic.html" accesskey="u" rel="up">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Integers-1"><span>20.1 Integers<a class="copiable-link" href="#Integers-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-integer"></a>

<p>The C language defines several integer data types: integer, short integer,
long integer, and character, all in both signed and unsigned varieties.
The GNU C compiler extends the language to contain long long integers
as well.
<a class="index-entry-id" id="index-signedness"></a>
</p>
<p>The C integer types were intended to allow code to be portable among
machines with different inherent data sizes (word sizes), so each type
may have different ranges on different machines.  The problem with
this is that a program often needs to be written for a particular range
of integers, and sometimes must be written for a particular size of
storage, regardless of what machine the program runs on.
</p>
<p>To address this problem, the GNU C Library contains C type definitions
you can use to declare integers that meet your exact needs.  Because the
GNU C Library header files are customized to a specific machine, your
program source code doesn&rsquo;t have to be.
</p>
<p>These <code class="code">typedef</code>s are in <samp class="file">stdint.h</samp>.
<a class="index-entry-id" id="index-stdint_002eh"></a>
</p>
<p>If you require that an integer be represented in exactly N bits, use one
of the following types, with the obvious mapping to bit size and signedness:
</p>
<ul class="itemize mark-bullet">
<li>int8_t
</li><li>int16_t
</li><li>int32_t
</li><li>int64_t
</li><li>uint8_t
</li><li>uint16_t
</li><li>uint32_t
</li><li>uint64_t
</li></ul>

<p>If your C compiler and target machine do not allow integers of a certain
size, the corresponding above type does not exist.
</p>
<p>If you don&rsquo;t need a specific storage size, but want the smallest data
structure with <em class="emph">at least</em> N bits, use one of these:
</p>
<ul class="itemize mark-bullet">
<li>int_least8_t
</li><li>int_least16_t
</li><li>int_least32_t
</li><li>int_least64_t
</li><li>uint_least8_t
</li><li>uint_least16_t
</li><li>uint_least32_t
</li><li>uint_least64_t
</li></ul>

<p>If you don&rsquo;t need a specific storage size, but want the data structure
that allows the fastest access while having at least N bits (and
among data structures with the same access speed, the smallest one), use
one of these:
</p>
<ul class="itemize mark-bullet">
<li>int_fast8_t
</li><li>int_fast16_t
</li><li>int_fast32_t
</li><li>int_fast64_t
</li><li>uint_fast8_t
</li><li>uint_fast16_t
</li><li>uint_fast32_t
</li><li>uint_fast64_t
</li></ul>

<p>If you want an integer with the widest range possible on the platform on
which it is being used, use one of the following.  If you use these,
you should write code that takes into account the variable size and range
of the integer.
</p>
<ul class="itemize mark-bullet">
<li>intmax_t
</li><li>uintmax_t
</li></ul>

<p>The GNU C Library also provides macros that tell you the maximum and
minimum possible values for each integer data type.  The macro names
follow these examples: <code class="code">INT32_MAX</code>, <code class="code">UINT8_MAX</code>,
<code class="code">INT_FAST32_MIN</code>, <code class="code">INT_LEAST64_MIN</code>, <code class="code">UINTMAX_MAX</code>,
<code class="code">INTMAX_MAX</code>, <code class="code">INTMAX_MIN</code>.  Note that there are no macros for
unsigned integer minima.  These are always zero.  Similarly, there
are macros such as <code class="code">INTMAX_WIDTH</code> for the width of these types.
Those macros for integer type widths come from TS 18661-1:2014.
<a class="index-entry-id" id="index-maximum-possible-integer"></a>
<a class="index-entry-id" id="index-minimum-possible-integer"></a>
</p>
<p>There are similar macros for use with C&rsquo;s built in integer types which
should come with your C compiler.  These are described in <a class="ref" href="Data-Type-Measurements.html">Data Type Measurements</a>.
</p>
<p>Don&rsquo;t forget you can use the C <code class="code">sizeof</code> function with any of these
data types to get the number of bytes of storage each uses.
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Integer-Division.html">Integer Division</a>, Up: <a href="Arithmetic.html">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
