<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Memory Allocation and C (The GNU C Library)</title>

<meta name="description" content="Memory Allocation and C (The GNU C Library)">
<meta name="keywords" content="Memory Allocation and C (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Memory-Allocation.html" rel="up" title="Memory Allocation">
<link href="The-GNU-Allocator.html" rel="next" title="The GNU Allocator">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Memory-Allocation-and-C">
<div class="nav-panel">
<p>
Next: <a href="The-GNU-Allocator.html" accesskey="n" rel="next">The GNU Allocator</a>, Up: <a href="Memory-Allocation.html" accesskey="u" rel="up">Allocating Storage For Program Data</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Memory-Allocation-in-C-Programs"><span>3.2.1 Memory Allocation in C Programs<a class="copiable-link" href="#Memory-Allocation-in-C-Programs"> &para;</a></span></h4>

<p>The C language supports two kinds of memory allocation through the
variables in C programs:
</p>
<ul class="itemize mark-bullet">
<li><em class="dfn">Static allocation</em> is what happens when you declare a static or
global variable.  Each static or global variable defines one block of
space, of a fixed size.  The space is allocated once, when your program
is started (part of the exec operation), and is never freed.
<a class="index-entry-id" id="index-static-memory-allocation"></a>
<a class="index-entry-id" id="index-static-storage-class"></a>

</li><li><em class="dfn">Automatic allocation</em> happens when you declare an automatic
variable, such as a function argument or a local variable.  The space
for an automatic variable is allocated when the compound statement
containing the declaration is entered, and is freed when that
compound statement is exited.
<a class="index-entry-id" id="index-automatic-memory-allocation"></a>
<a class="index-entry-id" id="index-automatic-storage-class"></a>

<p>In GNU C, the size of the automatic storage can be an expression
that varies.  In other C implementations, it must be a constant.
</p></li></ul>

<p>A third important kind of memory allocation, <em class="dfn">dynamic allocation</em>,
is not supported by C variables but is available via GNU C Library
functions.
<a class="index-entry-id" id="index-dynamic-memory-allocation"></a>
</p>
<ul class="mini-toc">
<li><a href="#Dynamic-Memory-Allocation" accesskey="1">Dynamic Memory Allocation</a></li>
</ul>
<div class="subsubsection-level-extent" id="Dynamic-Memory-Allocation">
<h4 class="subsubsection"><span>3.2.1.1 Dynamic Memory Allocation<a class="copiable-link" href="#Dynamic-Memory-Allocation"> &para;</a></span></h4>
<a class="index-entry-id" id="index-dynamic-memory-allocation-1"></a>

<p><em class="dfn">Dynamic memory allocation</em> is a technique in which programs
determine as they are running where to store some information.  You need
dynamic allocation when the amount of memory you need, or how long you
continue to need it, depends on factors that are not known before the
program runs.
</p>
<p>For example, you may need a block to store a line read from an input
file; since there is no limit to how long a line can be, you must
allocate the memory dynamically and make it dynamically larger as you
read more of the line.
</p>
<p>Or, you may need a block for each record or each definition in the input
data; since you can&rsquo;t know in advance how many there will be, you must
allocate a new block for each record or definition as you read it.
</p>
<p>When you use dynamic allocation, the allocation of a block of memory is
an action that the program requests explicitly.  You call a function or
macro when you want to allocate space, and specify the size with an
argument.  If you want to free the space, you do so by calling another
function or macro.  You can do these things whenever you want, as often
as you want.
</p>
<p>Dynamic allocation is not supported by C variables; there is no storage
class &ldquo;dynamic&rdquo;, and there can never be a C variable whose value is
stored in dynamically allocated space.  The only way to get dynamically
allocated memory is via a system call (which is generally via a GNU C Library
function call), and the only way to refer to dynamically
allocated space is through a pointer.  Because it is less convenient,
and because the actual process of dynamic allocation requires more
computation time, programmers generally use dynamic allocation only when
neither static nor automatic allocation will serve.
</p>
<p>For example, if you want to allocate dynamically some space to hold a
<code class="code">struct foobar</code>, you cannot declare a variable of type <code class="code">struct
foobar</code> whose contents are the dynamically allocated space.  But you can
declare a variable of pointer type <code class="code">struct foobar *</code> and assign it the
address of the space.  Then you can use the operators &lsquo;<samp class="samp">*</samp>&rsquo; and
&lsquo;<samp class="samp">-&gt;</samp>&rsquo; on this pointer variable to refer to the contents of the space:
</p>
<div class="example smallexample">
<pre class="example-preformatted">{
  struct foobar *ptr = malloc (sizeof *ptr);
  ptr-&gt;name = x;
  ptr-&gt;next = current_foobar;
  current_foobar = ptr;
}
</pre></div>

</div>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="The-GNU-Allocator.html">The GNU Allocator</a>, Up: <a href="Memory-Allocation.html">Allocating Storage For Program Data</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
