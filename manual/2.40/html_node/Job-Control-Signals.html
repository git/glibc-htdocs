<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Job Control Signals (The GNU C Library)</title>

<meta name="description" content="Job Control Signals (The GNU C Library)">
<meta name="keywords" content="Job Control Signals (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Standard-Signals.html" rel="up" title="Standard Signals">
<link href="Operation-Error-Signals.html" rel="next" title="Operation Error Signals">
<link href="Asynchronous-I_002fO-Signals.html" rel="prev" title="Asynchronous I/O Signals">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
kbd.kbd {font-style: oblique}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Job-Control-Signals">
<div class="nav-panel">
<p>
Next: <a href="Operation-Error-Signals.html" accesskey="n" rel="next">Operation Error Signals</a>, Previous: <a href="Asynchronous-I_002fO-Signals.html" accesskey="p" rel="prev">Asynchronous I/O Signals</a>, Up: <a href="Standard-Signals.html" accesskey="u" rel="up">Standard Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Job-Control-Signals-1"><span>25.2.5 Job Control Signals<a class="copiable-link" href="#Job-Control-Signals-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-job-control-signals"></a>

<p>These signals are used to support job control.  If your system
doesn&rsquo;t support job control, then these macros are defined but the
signals themselves can&rsquo;t be raised or handled.
</p>
<p>You should generally leave these signals alone unless you really
understand how job control works.  See <a class="xref" href="Job-Control.html">Job Control</a>.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGCHLD"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGCHLD</strong><a class="copiable-link" href="#index-SIGCHLD"> &para;</a></span></dt>
<dd>
<a class="index-entry-id" id="index-child-process-signal"></a>
<p>This signal is sent to a parent process whenever one of its child
processes terminates or stops.
</p>
<p>The default action for this signal is to ignore it.  If you establish a
handler for this signal while there are child processes that have
terminated but not reported their status via <code class="code">wait</code> or
<code class="code">waitpid</code> (see <a class="pxref" href="Process-Completion.html">Process Completion</a>), whether your new handler
applies to those processes or not depends on the particular operating
system.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGCLD"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGCLD</strong><a class="copiable-link" href="#index-SIGCLD"> &para;</a></span></dt>
<dd>
<p>This is an obsolete name for <code class="code">SIGCHLD</code>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGCONT"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGCONT</strong><a class="copiable-link" href="#index-SIGCONT"> &para;</a></span></dt>
<dd>
<a class="index-entry-id" id="index-continue-signal"></a>
<p>You can send a <code class="code">SIGCONT</code> signal to a process to make it continue.
This signal is special&mdash;it always makes the process continue if it is
stopped, before the signal is delivered.  The default behavior is to do
nothing else.  You cannot block this signal.  You can set a handler, but
<code class="code">SIGCONT</code> always makes the process continue regardless.
</p>
<p>Most programs have no reason to handle <code class="code">SIGCONT</code>; they simply
resume execution without realizing they were ever stopped.  You can use
a handler for <code class="code">SIGCONT</code> to make a program do something special when
it is stopped and continued&mdash;for example, to reprint a prompt when it
is suspended while waiting for input.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGSTOP"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGSTOP</strong><a class="copiable-link" href="#index-SIGSTOP"> &para;</a></span></dt>
<dd>
<p>The <code class="code">SIGSTOP</code> signal stops the process.  It cannot be handled,
ignored, or blocked.
</p></dd></dl>
<a class="index-entry-id" id="index-stop-signal"></a>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGTSTP"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGTSTP</strong><a class="copiable-link" href="#index-SIGTSTP"> &para;</a></span></dt>
<dd>
<p>The <code class="code">SIGTSTP</code> signal is an interactive stop signal.  Unlike
<code class="code">SIGSTOP</code>, this signal can be handled and ignored.
</p>
<p>Your program should handle this signal if you have a special need to
leave files or system tables in a secure state when a process is
stopped.  For example, programs that turn off echoing should handle
<code class="code">SIGTSTP</code> so they can turn echoing back on before stopping.
</p>
<p>This signal is generated when the user types the SUSP character
(normally <kbd class="kbd">C-z</kbd>).  For more information about terminal driver
support, see <a class="ref" href="Special-Characters.html">Special Characters</a>.
</p></dd></dl>
<a class="index-entry-id" id="index-interactive-stop-signal"></a>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGTTIN"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGTTIN</strong><a class="copiable-link" href="#index-SIGTTIN"> &para;</a></span></dt>
<dd>
<p>A process cannot read from the user&rsquo;s terminal while it is running
as a background job.  When any process in a background job tries to
read from the terminal, all of the processes in the job are sent a
<code class="code">SIGTTIN</code> signal.  The default action for this signal is to
stop the process.  For more information about how this interacts with
the terminal driver, see <a class="ref" href="Access-to-the-Terminal.html">Access to the Controlling Terminal</a>.
</p></dd></dl>
<a class="index-entry-id" id="index-terminal-input-signal"></a>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-SIGTTOU"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">SIGTTOU</strong><a class="copiable-link" href="#index-SIGTTOU"> &para;</a></span></dt>
<dd>
<p>This is similar to <code class="code">SIGTTIN</code>, but is generated when a process in a
background job attempts to write to the terminal or set its modes.
Again, the default action is to stop the process.  <code class="code">SIGTTOU</code> is
only generated for an attempt to write to the terminal if the
<code class="code">TOSTOP</code> output mode is set; see <a class="pxref" href="Output-Modes.html">Output Modes</a>.
</p></dd></dl>
<a class="index-entry-id" id="index-terminal-output-signal"></a>

<p>While a process is stopped, no more signals can be delivered to it until
it is continued, except <code class="code">SIGKILL</code> signals and (obviously)
<code class="code">SIGCONT</code> signals.  The signals are marked as pending, but not
delivered until the process is continued.  The <code class="code">SIGKILL</code> signal
always causes termination of the process and can&rsquo;t be blocked, handled
or ignored.  You can ignore <code class="code">SIGCONT</code>, but it always causes the
process to be continued anyway if it is stopped.  Sending a
<code class="code">SIGCONT</code> signal to a process causes any pending stop signals for
that process to be discarded.  Likewise, any pending <code class="code">SIGCONT</code>
signals for a process are discarded when it receives a stop signal.
</p>
<p>When a process in an orphaned process group (see <a class="pxref" href="Orphaned-Process-Groups.html">Orphaned Process Groups</a>) receives a <code class="code">SIGTSTP</code>, <code class="code">SIGTTIN</code>, or <code class="code">SIGTTOU</code>
signal and does not handle it, the process does not stop.  Stopping the
process would probably not be very useful, since there is no shell
program that will notice it stop and allow the user to continue it.
What happens instead depends on the operating system you are using.
Some systems may do nothing; others may deliver another signal instead,
such as <code class="code">SIGKILL</code> or <code class="code">SIGHUP</code>.  On GNU/Hurd systems, the process
dies with <code class="code">SIGKILL</code>; this avoids the problem of many stopped,
orphaned processes lying around the system.
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Operation-Error-Signals.html">Operation Error Signals</a>, Previous: <a href="Asynchronous-I_002fO-Signals.html">Asynchronous I/O Signals</a>, Up: <a href="Standard-Signals.html">Standard Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
