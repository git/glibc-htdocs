<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Remembering a Signal (The GNU C Library)</title>

<meta name="description" content="Remembering a Signal (The GNU C Library)">
<meta name="keywords" content="Remembering a Signal (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Blocking-Signals.html" rel="up" title="Blocking Signals">
<link href="Checking-for-Pending-Signals.html" rel="prev" title="Checking for Pending Signals">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Remembering-a-Signal">
<div class="nav-panel">
<p>
Previous: <a href="Checking-for-Pending-Signals.html" accesskey="p" rel="prev">Checking for Pending Signals</a>, Up: <a href="Blocking-Signals.html" accesskey="u" rel="up">Blocking Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Remembering-a-Signal-to-Act-On-Later"><span>25.7.7 Remembering a Signal to Act On Later<a class="copiable-link" href="#Remembering-a-Signal-to-Act-On-Later"> &para;</a></span></h4>

<p>Instead of blocking a signal using the library facilities, you can get
almost the same results by making the handler set a flag to be tested
later, when you &ldquo;unblock&rdquo;.  Here is an example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">/* <span class="r">If this flag is nonzero, don&rsquo;t handle the signal right away.</span> */
volatile sig_atomic_t signal_pending;

/* <span class="r">This is nonzero if a signal arrived and was not handled.</span> */
volatile sig_atomic_t defer_signal;

void
handler (int signum)
{
  if (defer_signal)
    signal_pending = signum;
  else
    ... /* <span class="r">&ldquo;Really&rdquo; handle the signal.</span> */
}

...

void
update_mumble (int frob)
{
  /* <span class="r">Prevent signals from having immediate effect.</span> */
  defer_signal++;
  /* <span class="r">Now update <code class="code">mumble</code>, without worrying about interruption.</span> */
  mumble.a = 1;
  mumble.b = hack ();
  mumble.c = frob;
  /* <span class="r">We have updated <code class="code">mumble</code>.  Handle any signal that came in.</span> */
  defer_signal--;
  if (defer_signal == 0 &amp;&amp; signal_pending != 0)
    raise (signal_pending);
}
</pre></div>

<p>Note how the particular signal that arrives is stored in
<code class="code">signal_pending</code>.  That way, we can handle several types of
inconvenient signals with the same mechanism.
</p>
<p>We increment and decrement <code class="code">defer_signal</code> so that nested critical
sections will work properly; thus, if <code class="code">update_mumble</code> were called
with <code class="code">signal_pending</code> already nonzero, signals would be deferred
not only within <code class="code">update_mumble</code>, but also within the caller.  This
is also why we do not check <code class="code">signal_pending</code> if <code class="code">defer_signal</code>
is still nonzero.
</p>
<p>The incrementing and decrementing of <code class="code">defer_signal</code> each require more
than one instruction; it is possible for a signal to happen in the
middle.  But that does not cause any problem.  If the signal happens
early enough to see the value from before the increment or decrement,
that is equivalent to a signal which came before the beginning of the
increment or decrement, which is a case that works properly.
</p>
<p>It is absolutely vital to decrement <code class="code">defer_signal</code> before testing
<code class="code">signal_pending</code>, because this avoids a subtle bug.  If we did
these things in the other order, like this,
</p>
<div class="example smallexample">
<pre class="example-preformatted">  if (defer_signal == 1 &amp;&amp; signal_pending != 0)
    raise (signal_pending);
  defer_signal--;
</pre></div>

<p>then a signal arriving in between the <code class="code">if</code> statement and the decrement
would be effectively &ldquo;lost&rdquo; for an indefinite amount of time.  The
handler would merely set <code class="code">defer_signal</code>, but the program having
already tested this variable, it would not test the variable again.
</p>
<a class="index-entry-id" id="index-timing-error-in-signal-handling"></a>
<p>Bugs like these are called <em class="dfn">timing errors</em>.  They are especially bad
because they happen only rarely and are nearly impossible to reproduce.
You can&rsquo;t expect to find them with a debugger as you would find a
reproducible bug.  So it is worth being especially careful to avoid
them.
</p>
<p>(You would not be tempted to write the code in this order, given the use
of <code class="code">defer_signal</code> as a counter which must be tested along with
<code class="code">signal_pending</code>.  After all, testing for zero is cleaner than
testing for one.  But if you did not use <code class="code">defer_signal</code> as a
counter, and gave it values of zero and one only, then either order
might seem equally simple.  This is a further advantage of using a
counter for <code class="code">defer_signal</code>: it will reduce the chance you will
write the code in the wrong order and create a subtle bug.)
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Checking-for-Pending-Signals.html">Checking for Pending Signals</a>, Up: <a href="Blocking-Signals.html">Blocking Signals</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
