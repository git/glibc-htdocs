<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Flags for Wordexp (The GNU C Library)</title>

<meta name="description" content="Flags for Wordexp (The GNU C Library)">
<meta name="keywords" content="Flags for Wordexp (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Word-Expansion.html" rel="up" title="Word Expansion">
<link href="Wordexp-Example.html" rel="next" title="Wordexp Example">
<link href="Calling-Wordexp.html" rel="prev" title="Calling Wordexp">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Flags-for-Wordexp">
<div class="nav-panel">
<p>
Next: <a href="Wordexp-Example.html" accesskey="n" rel="next"><code class="code">wordexp</code> Example</a>, Previous: <a href="Calling-Wordexp.html" accesskey="p" rel="prev">Calling <code class="code">wordexp</code></a>, Up: <a href="Word-Expansion.html" accesskey="u" rel="up">Shell-Style Word Expansion</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Flags-for-Word-Expansion"><span>10.4.3 Flags for Word Expansion<a class="copiable-link" href="#Flags-for-Word-Expansion"> &para;</a></span></h4>

<p>This section describes the flags that you can specify in the
<var class="var">flags</var> argument to <code class="code">wordexp</code>.  Choose the flags you want,
and combine them with the C operator <code class="code">|</code>.
</p>
<dl class="vtable">
<dt><a id="index-WRDE_005fAPPEND"></a><span><code class="code">WRDE_APPEND</code><a class="copiable-link" href="#index-WRDE_005fAPPEND"> &para;</a></span></dt>
<dd>
<p>Append the words from this expansion to the vector of words produced by
previous calls to <code class="code">wordexp</code>.  This way you can effectively expand
several words as if they were concatenated with spaces between them.
</p>
<p>In order for appending to work, you must not modify the contents of the
word vector structure between calls to <code class="code">wordexp</code>.  And, if you set
<code class="code">WRDE_DOOFFS</code> in the first call to <code class="code">wordexp</code>, you must also
set it when you append to the results.
</p>
</dd>
<dt><a id="index-WRDE_005fDOOFFS"></a><span><code class="code">WRDE_DOOFFS</code><a class="copiable-link" href="#index-WRDE_005fDOOFFS"> &para;</a></span></dt>
<dd>
<p>Leave blank slots at the beginning of the vector of words.
The <code class="code">we_offs</code> field says how many slots to leave.
The blank slots contain null pointers.
</p>
</dd>
<dt><a id="index-WRDE_005fNOCMD"></a><span><code class="code">WRDE_NOCMD</code><a class="copiable-link" href="#index-WRDE_005fNOCMD"> &para;</a></span></dt>
<dd>
<p>Don&rsquo;t do command substitution; if the input requests command substitution,
report an error.
</p>
</dd>
<dt><a id="index-WRDE_005fREUSE"></a><span><code class="code">WRDE_REUSE</code><a class="copiable-link" href="#index-WRDE_005fREUSE"> &para;</a></span></dt>
<dd>
<p>Reuse a word vector made by a previous call to <code class="code">wordexp</code>.
Instead of allocating a new vector of words, this call to <code class="code">wordexp</code>
will use the vector that already exists (making it larger if necessary).
</p>
<p>Note that the vector may move, so it is not safe to save an old pointer
and use it again after calling <code class="code">wordexp</code>.  You must fetch
<code class="code">we_pathv</code> anew after each call.
</p>
</dd>
<dt><a id="index-WRDE_005fSHOWERR"></a><span><code class="code">WRDE_SHOWERR</code><a class="copiable-link" href="#index-WRDE_005fSHOWERR"> &para;</a></span></dt>
<dd>
<p>Do show any error messages printed by commands run by command substitution.
More precisely, allow these commands to inherit the standard error output
stream of the current process.  By default, <code class="code">wordexp</code> gives these
commands a standard error stream that discards all output.
</p>
</dd>
<dt><a id="index-WRDE_005fUNDEF"></a><span><code class="code">WRDE_UNDEF</code><a class="copiable-link" href="#index-WRDE_005fUNDEF"> &para;</a></span></dt>
<dd>
<p>If the input refers to a shell variable that is not defined, report an
error.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Wordexp-Example.html"><code class="code">wordexp</code> Example</a>, Previous: <a href="Calling-Wordexp.html">Calling <code class="code">wordexp</code></a>, Up: <a href="Word-Expansion.html">Shell-Style Word Expansion</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
