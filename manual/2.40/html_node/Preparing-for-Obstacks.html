<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Preparing for Obstacks (The GNU C Library)</title>

<meta name="description" content="Preparing for Obstacks (The GNU C Library)">
<meta name="keywords" content="Preparing for Obstacks (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Obstacks.html" rel="up" title="Obstacks">
<link href="Allocation-in-an-Obstack.html" rel="next" title="Allocation in an Obstack">
<link href="Creating-Obstacks.html" rel="prev" title="Creating Obstacks">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Preparing-for-Obstacks">
<div class="nav-panel">
<p>
Next: <a href="Allocation-in-an-Obstack.html" accesskey="n" rel="next">Allocation in an Obstack</a>, Previous: <a href="Creating-Obstacks.html" accesskey="p" rel="prev">Creating Obstacks</a>, Up: <a href="Obstacks.html" accesskey="u" rel="up">Obstacks</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Preparing-for-Using-Obstacks"><span>3.2.6.2 Preparing for Using Obstacks<a class="copiable-link" href="#Preparing-for-Using-Obstacks"> &para;</a></span></h4>

<p>Each source file in which you plan to use the obstack functions
must include the header file <samp class="file">obstack.h</samp>, like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">#include &lt;obstack.h&gt;
</pre></div>

<a class="index-entry-id" id="index-obstack_005fchunk_005falloc"></a>
<a class="index-entry-id" id="index-obstack_005fchunk_005ffree"></a>
<p>Also, if the source file uses the macro <code class="code">obstack_init</code>, it must
declare or define two functions or macros that will be called by the
obstack library.  One, <code class="code">obstack_chunk_alloc</code>, is used to allocate
the chunks of memory into which objects are packed.  The other,
<code class="code">obstack_chunk_free</code>, is used to return chunks when the objects in
them are freed.  These macros should appear before any use of obstacks
in the source file.
</p>
<p>Usually these are defined to use <code class="code">malloc</code> via the intermediary
<code class="code">xmalloc</code> (see <a class="pxref" href="Unconstrained-Allocation.html">Unconstrained Allocation</a>).  This is done with
the following pair of macro definitions:
</p>
<div class="example smallexample">
<pre class="example-preformatted">#define obstack_chunk_alloc xmalloc
#define obstack_chunk_free free
</pre></div>

<p>Though the memory you get using obstacks really comes from <code class="code">malloc</code>,
using obstacks is faster because <code class="code">malloc</code> is called less often, for
larger blocks of memory.  See <a class="xref" href="Obstack-Chunks.html">Obstack Chunks</a>, for full details.
</p>
<p>At run time, before the program can use a <code class="code">struct obstack</code> object
as an obstack, it must initialize the obstack by calling
<code class="code">obstack_init</code>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005finit"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">obstack_init</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>)</code><a class="copiable-link" href="#index-obstack_005finit"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack-ptr
| AS-Safe 
| AC-Safe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Initialize obstack <var class="var">obstack-ptr</var> for allocation of objects.  This
function calls the obstack&rsquo;s <code class="code">obstack_chunk_alloc</code> function.  If
allocation of memory fails, the function pointed to by
<code class="code">obstack_alloc_failed_handler</code> is called.  The <code class="code">obstack_init</code>
function always returns 1 (Compatibility notice: Former versions of
obstack returned 0 if allocation failed).
</p></dd></dl>

<p>Here are two examples of how to allocate the space for an obstack and
initialize it.  First, an obstack that is a static variable:
</p>
<div class="example smallexample">
<pre class="example-preformatted">static struct obstack myobstack;
...
obstack_init (&amp;myobstack);
</pre></div>

<p>Second, an obstack that is itself dynamically allocated:
</p>
<div class="example smallexample">
<pre class="example-preformatted">struct obstack *myobstack_ptr
  = (struct obstack *) xmalloc (sizeof (struct obstack));

obstack_init (myobstack_ptr);
</pre></div>

<dl class="first-defvr first-defvar-alias-first-defvr">
<dt class="defvr defvar-alias-defvr" id="index-obstack_005falloc_005ffailed_005fhandler"><span class="category-def">Variable: </span><span><strong class="def-name">obstack_alloc_failed_handler</strong><a class="copiable-link" href="#index-obstack_005falloc_005ffailed_005fhandler"> &para;</a></span></dt>
<dd>
<p>The value of this variable is a pointer to a function that
<code class="code">obstack</code> uses when <code class="code">obstack_chunk_alloc</code> fails to allocate
memory.  The default action is to print a message and abort.
You should supply a function that either calls <code class="code">exit</code>
(see <a class="pxref" href="Program-Termination.html">Program Termination</a>) or <code class="code">longjmp</code> (see <a class="pxref" href="Non_002dLocal-Exits.html">Non-Local Exits</a>) and doesn&rsquo;t return.
</p>
<div class="example smallexample">
<pre class="example-preformatted">void my_obstack_alloc_failed (void)
...
obstack_alloc_failed_handler = &amp;my_obstack_alloc_failed;
</pre></div>

</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Allocation-in-an-Obstack.html">Allocation in an Obstack</a>, Previous: <a href="Creating-Obstacks.html">Creating Obstacks</a>, Up: <a href="Obstacks.html">Obstacks</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
