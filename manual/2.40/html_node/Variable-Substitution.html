<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Variable Substitution (The GNU C Library)</title>

<meta name="description" content="Variable Substitution (The GNU C Library)">
<meta name="keywords" content="Variable Substitution (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Word-Expansion.html" rel="up" title="Word Expansion">
<link href="Tilde-Expansion.html" rel="prev" title="Tilde Expansion">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Variable-Substitution">
<div class="nav-panel">
<p>
Previous: <a href="Tilde-Expansion.html" accesskey="p" rel="prev">Details of Tilde Expansion</a>, Up: <a href="Word-Expansion.html" accesskey="u" rel="up">Shell-Style Word Expansion</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Details-of-Variable-Substitution"><span>10.4.6 Details of Variable Substitution<a class="copiable-link" href="#Details-of-Variable-Substitution"> &para;</a></span></h4>

<p>Part of ordinary shell syntax is the use of &lsquo;<samp class="samp">$<var class="var">variable</var></samp>&rsquo; to
substitute the value of a shell variable into a command.  This is called
<em class="dfn">variable substitution</em>, and it is one part of doing word expansion.
</p>
<p>There are two basic ways you can write a variable reference for
substitution:
</p>
<dl class="table">
<dt><code class="code">${<var class="var">variable</var>}</code></dt>
<dd><p>If you write braces around the variable name, then it is completely
unambiguous where the variable name ends.  You can concatenate
additional letters onto the end of the variable value by writing them
immediately after the close brace.  For example, &lsquo;<samp class="samp">${foo}s</samp>&rsquo;
expands into &lsquo;<samp class="samp">tractors</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">$<var class="var">variable</var></code></dt>
<dd><p>If you do not put braces around the variable name, then the variable
name consists of all the alphanumeric characters and underscores that
follow the &lsquo;<samp class="samp">$</samp>&rsquo;.  The next punctuation character ends the variable
name.  Thus, &lsquo;<samp class="samp">$foo-bar</samp>&rsquo; refers to the variable <code class="code">foo</code> and expands
into &lsquo;<samp class="samp">tractor-bar</samp>&rsquo;.
</p></dd>
</dl>

<p>When you use braces, you can also use various constructs to modify the
value that is substituted, or test it in various ways.
</p>
<dl class="table">
<dt><code class="code">${<var class="var">variable</var>:-<var class="var">default</var>}</code></dt>
<dd><p>Substitute the value of <var class="var">variable</var>, but if that is empty or
undefined, use <var class="var">default</var> instead.
</p>
</dd>
<dt><code class="code">${<var class="var">variable</var>:=<var class="var">default</var>}</code></dt>
<dd><p>Substitute the value of <var class="var">variable</var>, but if that is empty or
undefined, use <var class="var">default</var> instead and set the variable to
<var class="var">default</var>.
</p>
</dd>
<dt><code class="code">${<var class="var">variable</var>:?<var class="var">message</var>}</code></dt>
<dd><p>If <var class="var">variable</var> is defined and not empty, substitute its value.
</p>
<p>Otherwise, print <var class="var">message</var> as an error message on the standard error
stream, and consider word expansion a failure.
</p>

</dd>
<dt><code class="code">${<var class="var">variable</var>:+<var class="var">replacement</var>}</code></dt>
<dd><p>Substitute <var class="var">replacement</var>, but only if <var class="var">variable</var> is defined and
nonempty.  Otherwise, substitute nothing for this construct.
</p></dd>
</dl>

<dl class="table">
<dt><code class="code">${#<var class="var">variable</var>}</code></dt>
<dd><p>Substitute a numeral which expresses in base ten the number of
characters in the value of <var class="var">variable</var>.  &lsquo;<samp class="samp">${#foo}</samp>&rsquo; stands for
&lsquo;<samp class="samp">7</samp>&rsquo;, because &lsquo;<samp class="samp">tractor</samp>&rsquo; is seven characters.
</p></dd>
</dl>

<p>These variants of variable substitution let you remove part of the
variable&rsquo;s value before substituting it.  The <var class="var">prefix</var> and
<var class="var">suffix</var> are not mere strings; they are wildcard patterns, just
like the patterns that you use to match multiple file names.  But
in this context, they match against parts of the variable value
rather than against file names.
</p>
<dl class="table">
<dt><code class="code">${<var class="var">variable</var>%%<var class="var">suffix</var>}</code></dt>
<dd><p>Substitute the value of <var class="var">variable</var>, but first discard from that
variable any portion at the end that matches the pattern <var class="var">suffix</var>.
</p>
<p>If there is more than one alternative for how to match against
<var class="var">suffix</var>, this construct uses the longest possible match.
</p>
<p>Thus, &lsquo;<samp class="samp">${foo%%r*}</samp>&rsquo; substitutes &lsquo;<samp class="samp">t</samp>&rsquo;, because the largest
match for &lsquo;<samp class="samp">r*</samp>&rsquo; at the end of &lsquo;<samp class="samp">tractor</samp>&rsquo; is &lsquo;<samp class="samp">ractor</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">${<var class="var">variable</var>%<var class="var">suffix</var>}</code></dt>
<dd><p>Substitute the value of <var class="var">variable</var>, but first discard from that
variable any portion at the end that matches the pattern <var class="var">suffix</var>.
</p>
<p>If there is more than one alternative for how to match against
<var class="var">suffix</var>, this construct uses the shortest possible alternative.
</p>
<p>Thus, &lsquo;<samp class="samp">${foo%r*}</samp>&rsquo; substitutes &lsquo;<samp class="samp">tracto</samp>&rsquo;, because the shortest
match for &lsquo;<samp class="samp">r*</samp>&rsquo; at the end of &lsquo;<samp class="samp">tractor</samp>&rsquo; is just &lsquo;<samp class="samp">r</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">${<var class="var">variable</var>##<var class="var">prefix</var>}</code></dt>
<dd><p>Substitute the value of <var class="var">variable</var>, but first discard from that
variable any portion at the beginning that matches the pattern <var class="var">prefix</var>.
</p>
<p>If there is more than one alternative for how to match against
<var class="var">prefix</var>, this construct uses the longest possible match.
</p>
<p>Thus, &lsquo;<samp class="samp">${foo##*t}</samp>&rsquo; substitutes &lsquo;<samp class="samp">or</samp>&rsquo;, because the largest
match for &lsquo;<samp class="samp">*t</samp>&rsquo; at the beginning of &lsquo;<samp class="samp">tractor</samp>&rsquo; is &lsquo;<samp class="samp">tract</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">${<var class="var">variable</var>#<var class="var">prefix</var>}</code></dt>
<dd><p>Substitute the value of <var class="var">variable</var>, but first discard from that
variable any portion at the beginning that matches the pattern <var class="var">prefix</var>.
</p>
<p>If there is more than one alternative for how to match against
<var class="var">prefix</var>, this construct uses the shortest possible alternative.
</p>
<p>Thus, &lsquo;<samp class="samp">${foo#*t}</samp>&rsquo; substitutes &lsquo;<samp class="samp">ractor</samp>&rsquo;, because the shortest
match for &lsquo;<samp class="samp">*t</samp>&rsquo; at the beginning of &lsquo;<samp class="samp">tractor</samp>&rsquo; is just &lsquo;<samp class="samp">t</samp>&rsquo;.
</p>
</dd>
</dl>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Tilde-Expansion.html">Details of Tilde Expansion</a>, Up: <a href="Word-Expansion.html">Shell-Style Word Expansion</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
