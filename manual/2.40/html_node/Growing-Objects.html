<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Growing Objects (The GNU C Library)</title>

<meta name="description" content="Growing Objects (The GNU C Library)">
<meta name="keywords" content="Growing Objects (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Obstacks.html" rel="up" title="Obstacks">
<link href="Extra-Fast-Growing.html" rel="next" title="Extra Fast Growing">
<link href="Obstack-Functions.html" rel="prev" title="Obstack Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Growing-Objects">
<div class="nav-panel">
<p>
Next: <a href="Extra-Fast-Growing.html" accesskey="n" rel="next">Extra Fast Growing Objects</a>, Previous: <a href="Obstack-Functions.html" accesskey="p" rel="prev">Obstack Functions and Macros</a>, Up: <a href="Obstacks.html" accesskey="u" rel="up">Obstacks</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Growing-Objects-1"><span>3.2.6.6 Growing Objects<a class="copiable-link" href="#Growing-Objects-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-growing-objects-_0028in-obstacks_0029"></a>
<a class="index-entry-id" id="index-changing-the-size-of-a-block-_0028obstacks_0029"></a>

<p>Because memory in obstack chunks is used sequentially, it is possible to
build up an object step by step, adding one or more bytes at a time to the
end of the object.  With this technique, you do not need to know how much
data you will put in the object until you come to the end of it.  We call
this the technique of <em class="dfn">growing objects</em>.  The special functions
for adding data to the growing object are described in this section.
</p>
<p>You don&rsquo;t need to do anything special when you start to grow an object.
Using one of the functions to add data to the object automatically
starts it.  However, it is necessary to say explicitly when the object is
finished.  This is done with the function <code class="code">obstack_finish</code>.
</p>
<p>The actual address of the object thus built up is not known until the
object is finished.  Until then, it always remains possible that you will
add so much data that the object must be copied into a new chunk.
</p>
<p>While the obstack is in use for a growing object, you cannot use it for
ordinary allocation of another object.  If you try to do so, the space
already added to the growing object will become part of the other object.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fblank"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">obstack_blank</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>, int <var class="var">size</var>)</code><a class="copiable-link" href="#index-obstack_005fblank"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack-ptr
| AS-Safe 
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The most basic function for adding to a growing object is
<code class="code">obstack_blank</code>, which adds space without initializing it.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fgrow"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">obstack_grow</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>, void *<var class="var">data</var>, int <var class="var">size</var>)</code><a class="copiable-link" href="#index-obstack_005fgrow"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack-ptr
| AS-Safe 
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>To add a block of initialized space, use <code class="code">obstack_grow</code>, which is
the growing-object analogue of <code class="code">obstack_copy</code>.  It adds <var class="var">size</var>
bytes of data to the growing object, copying the contents from
<var class="var">data</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fgrow0"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">obstack_grow0</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>, void *<var class="var">data</var>, int <var class="var">size</var>)</code><a class="copiable-link" href="#index-obstack_005fgrow0"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack-ptr
| AS-Safe 
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the growing-object analogue of <code class="code">obstack_copy0</code>.  It adds
<var class="var">size</var> bytes copied from <var class="var">data</var>, followed by an additional null
character.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005f1grow"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">obstack_1grow</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>, char <var class="var">c</var>)</code><a class="copiable-link" href="#index-obstack_005f1grow"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack-ptr
| AS-Safe 
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>To add one character at a time, use the function <code class="code">obstack_1grow</code>.
It adds a single byte containing <var class="var">c</var> to the growing object.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fptr_005fgrow"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">obstack_ptr_grow</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>, void *<var class="var">data</var>)</code><a class="copiable-link" href="#index-obstack_005fptr_005fgrow"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack-ptr
| AS-Safe 
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Adding the value of a pointer one can use the function
<code class="code">obstack_ptr_grow</code>.  It adds <code class="code">sizeof (void *)</code> bytes
containing the value of <var class="var">data</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fint_005fgrow"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">obstack_int_grow</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>, int <var class="var">data</var>)</code><a class="copiable-link" href="#index-obstack_005fint_005fgrow"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack-ptr
| AS-Safe 
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>A single value of type <code class="code">int</code> can be added by using the
<code class="code">obstack_int_grow</code> function.  It adds <code class="code">sizeof (int)</code> bytes to
the growing object and initializes them with the value of <var class="var">data</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005ffinish"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">obstack_finish</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>)</code><a class="copiable-link" href="#index-obstack_005ffinish"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack-ptr
| AS-Safe 
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>When you are finished growing the object, use the function
<code class="code">obstack_finish</code> to close it off and return its final address.
</p>
<p>Once you have finished the object, the obstack is available for ordinary
allocation or for growing another object.
</p>
<p>This function can return a null pointer under the same conditions as
<code class="code">obstack_alloc</code> (see <a class="pxref" href="Allocation-in-an-Obstack.html">Allocation in an Obstack</a>).
</p></dd></dl>

<p>When you build an object by growing it, you will probably need to know
afterward how long it became.  You need not keep track of this as you grow
the object, because you can find out the length from the obstack just
before finishing the object with the function <code class="code">obstack_object_size</code>,
declared as follows:
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fobject_005fsize"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">obstack_object_size</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>)</code><a class="copiable-link" href="#index-obstack_005fobject_005fsize"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack-ptr
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the current size of the growing object, in bytes.
Remember to call this function <em class="emph">before</em> finishing the object.
After it is finished, <code class="code">obstack_object_size</code> will return zero.
</p></dd></dl>

<p>If you have started growing an object and wish to cancel it, you should
finish it and then free it, like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">obstack_free (obstack_ptr, obstack_finish (obstack_ptr));
</pre></div>

<p>This has no effect if no object was growing.
</p>
<a class="index-entry-id" id="index-shrinking-objects"></a>
<p>You can use <code class="code">obstack_blank</code> with a negative size argument to make
the current object smaller.  Just don&rsquo;t try to shrink it beyond zero
length&mdash;there&rsquo;s no telling what will happen if you do that.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Extra-Fast-Growing.html">Extra Fast Growing Objects</a>, Previous: <a href="Obstack-Functions.html">Obstack Functions and Macros</a>, Up: <a href="Obstacks.html">Obstacks</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
