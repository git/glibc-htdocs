<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Converting Strings (The GNU C Library)</title>

<meta name="description" content="Converting Strings (The GNU C Library)">
<meta name="keywords" content="Converting Strings (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Restartable-multibyte-conversion.html" rel="up" title="Restartable multibyte conversion">
<link href="Multibyte-Conversion-Example.html" rel="next" title="Multibyte Conversion Example">
<link href="Converting-a-Character.html" rel="prev" title="Converting a Character">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Converting-Strings">
<div class="nav-panel">
<p>
Next: <a href="Multibyte-Conversion-Example.html" accesskey="n" rel="next">A Complete Multibyte Conversion Example</a>, Previous: <a href="Converting-a-Character.html" accesskey="p" rel="prev">Converting Single Characters</a>, Up: <a href="Restartable-multibyte-conversion.html" accesskey="u" rel="up">Restartable Multibyte Conversion Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Converting-Multibyte-and-Wide-Character-Strings"><span>6.3.4 Converting Multibyte and Wide Character Strings<a class="copiable-link" href="#Converting-Multibyte-and-Wide-Character-Strings"> &para;</a></span></h4>

<p>The functions described in the previous section only convert a single
character at a time.  Most operations to be performed in real-world
programs include strings and therefore the ISO&nbsp;C<!-- /@w --> standard also
defines conversions on entire strings.  However, the defined set of
functions is quite limited; therefore, the GNU C Library contains a few
extensions that can help in some important situations.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mbsrtowcs"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">mbsrtowcs</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">dst</var>, const char **restrict <var class="var">src</var>, size_t <var class="var">len</var>, mbstate_t *restrict <var class="var">ps</var>)</code><a class="copiable-link" href="#index-mbsrtowcs"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:mbsrtowcs/!ps
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">mbsrtowcs</code> function (&ldquo;multibyte string restartable to wide
character string&rdquo;) converts the NUL-terminated multibyte character
string at <code class="code">*<var class="var">src</var></code> into an equivalent wide character string,
including the NUL wide character at the end.  The conversion is started
using the state information from the object pointed to by <var class="var">ps</var> or
from an internal object of <code class="code">mbsrtowcs</code> if <var class="var">ps</var> is a null
pointer.  Before returning, the state object is updated to match the state
after the last converted character.  The state is the initial state if the
terminating NUL byte is reached and converted.
</p>
<p>If <var class="var">dst</var> is not a null pointer, the result is stored in the array
pointed to by <var class="var">dst</var>; otherwise, the conversion result is not
available since it is stored in an internal buffer.
</p>
<p>If <var class="var">len</var> wide characters are stored in the array <var class="var">dst</var> before
reaching the end of the input string, the conversion stops and <var class="var">len</var>
is returned.  If <var class="var">dst</var> is a null pointer, <var class="var">len</var> is never checked.
</p>
<p>Another reason for a premature return from the function call is if the
input string contains an invalid multibyte sequence.  In this case the
global variable <code class="code">errno</code> is set to <code class="code">EILSEQ</code> and the function
returns <code class="code">(size_t) -1</code>.
</p>

<p>In all other cases the function returns the number of wide characters
converted during this call.  If <var class="var">dst</var> is not null, <code class="code">mbsrtowcs</code>
stores in the pointer pointed to by <var class="var">src</var> either a null pointer (if
the NUL byte in the input string was reached) or the address of the byte
following the last converted multibyte character.
</p>
<p>Like <code class="code">mbstowcs</code> the <var class="var">dst</var> parameter may be a null pointer and
the function can be used to count the number of wide characters that
would be required.
</p>
<a class="index-entry-id" id="index-wchar_002eh-12"></a>
<p><code class="code">mbsrtowcs</code> was introduced in Amendment&nbsp;1<!-- /@w --> to ISO&nbsp;C90<!-- /@w --> and is
declared in <samp class="file">wchar.h</samp>.
</p></dd></dl>

<p>The definition of the <code class="code">mbsrtowcs</code> function has one important
limitation.  The requirement that <var class="var">dst</var> has to be a NUL-terminated
string provides problems if one wants to convert buffers with text.  A
buffer is not normally a collection of NUL-terminated strings but instead a
continuous collection of lines, separated by newline characters.  Now
assume that a function to convert one line from a buffer is needed.  Since
the line is not NUL-terminated, the source pointer cannot directly point
into the unmodified text buffer.  This means, either one inserts the NUL
byte at the appropriate place for the time of the <code class="code">mbsrtowcs</code>
function call (which is not doable for a read-only buffer or in a
multi-threaded application) or one copies the line in an extra buffer
where it can be terminated by a NUL byte.  Note that it is not in general
possible to limit the number of characters to convert by setting the
parameter <var class="var">len</var> to any specific value.  Since it is not known how
many bytes each multibyte character sequence is in length, one can only
guess.
</p>
<a class="index-entry-id" id="index-stateful-2"></a>
<p>There is still a problem with the method of NUL-terminating a line right
after the newline character, which could lead to very strange results.
As said in the description of the <code class="code">mbsrtowcs</code> function above, the
conversion state is guaranteed to be in the initial shift state after
processing the NUL byte at the end of the input string.  But this NUL
byte is not really part of the text (i.e., the conversion state after
the newline in the original text could be something different than the
initial shift state and therefore the first character of the next line
is encoded using this state).  But the state in question is never
accessible to the user since the conversion stops after the NUL byte
(which resets the state).  Most stateful character sets in use today
require that the shift state after a newline be the initial state&ndash;but
this is not a strict guarantee.  Therefore, simply NUL-terminating a
piece of a running text is not always an adequate solution and,
therefore, should never be used in generally used code.
</p>
<p>The generic conversion interface (see <a class="pxref" href="Generic-Charset-Conversion.html">Generic Charset Conversion</a>)
does not have this limitation (it simply works on buffers, not
strings), and the GNU C Library contains a set of functions that take
additional parameters specifying the maximal number of bytes that are
consumed from the input string.  This way the problem of
<code class="code">mbsrtowcs</code>&rsquo;s example above could be solved by determining the line
length and passing this length to the function.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcsrtombs"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">wcsrtombs</strong> <code class="def-code-arguments">(char *restrict <var class="var">dst</var>, const wchar_t **restrict <var class="var">src</var>, size_t <var class="var">len</var>, mbstate_t *restrict <var class="var">ps</var>)</code><a class="copiable-link" href="#index-wcsrtombs"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:wcsrtombs/!ps
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wcsrtombs</code> function (&ldquo;wide character string restartable to
multibyte string&rdquo;) converts the NUL-terminated wide character string at
<code class="code">*<var class="var">src</var></code> into an equivalent multibyte character string and
stores the result in the array pointed to by <var class="var">dst</var>.  The NUL wide
character is also converted.  The conversion starts in the state
described in the object pointed to by <var class="var">ps</var> or by a state object
local to <code class="code">wcsrtombs</code> in case <var class="var">ps</var> is a null pointer.  If
<var class="var">dst</var> is a null pointer, the conversion is performed as usual but the
result is not available.  If all characters of the input string were
successfully converted and if <var class="var">dst</var> is not a null pointer, the
pointer pointed to by <var class="var">src</var> gets assigned a null pointer.
</p>
<p>If one of the wide characters in the input string has no valid multibyte
character equivalent, the conversion stops early, sets the global
variable <code class="code">errno</code> to <code class="code">EILSEQ</code>, and returns <code class="code">(size_t) -1</code>.
</p>
<p>Another reason for a premature stop is if <var class="var">dst</var> is not a null
pointer and the next converted character would require more than
<var class="var">len</var> bytes in total to the array <var class="var">dst</var>.  In this case (and if
<var class="var">dst</var> is not a null pointer) the pointer pointed to by <var class="var">src</var> is
assigned a value pointing to the wide character right after the last one
successfully converted.
</p>
<p>Except in the case of an encoding error the return value of the
<code class="code">wcsrtombs</code> function is the number of bytes in all the multibyte
character sequences which were or would have been (if <var class="var">dst</var> was
not a null) stored in <var class="var">dst</var>.  Before returning, the state in the
object pointed to by <var class="var">ps</var> (or the internal object in case <var class="var">ps</var>
is a null pointer) is updated to reflect the state after the last
conversion.  The state is the initial shift state in case the
terminating NUL wide character was converted.
</p>
<a class="index-entry-id" id="index-wchar_002eh-13"></a>
<p>The <code class="code">wcsrtombs</code> function was introduced in Amendment&nbsp;1<!-- /@w --> to
ISO&nbsp;C90<!-- /@w --> and is declared in <samp class="file">wchar.h</samp>.
</p></dd></dl>

<p>The restriction mentioned above for the <code class="code">mbsrtowcs</code> function applies
here also.  There is no possibility of directly controlling the number of
input characters.  One has to place the NUL wide character at the correct
place or control the consumed input indirectly via the available output
array size (the <var class="var">len</var> parameter).
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mbsnrtowcs"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">mbsnrtowcs</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">dst</var>, const char **restrict <var class="var">src</var>, size_t <var class="var">nmc</var>, size_t <var class="var">len</var>, mbstate_t *restrict <var class="var">ps</var>)</code><a class="copiable-link" href="#index-mbsnrtowcs"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:mbsnrtowcs/!ps
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">mbsnrtowcs</code> function is very similar to the <code class="code">mbsrtowcs</code>
function.  All the parameters are the same except for <var class="var">nmc</var>, which is
new.  The return value is the same as for <code class="code">mbsrtowcs</code>.
</p>
<p>This new parameter specifies how many bytes at most can be used from the
multibyte character string.  In other words, the multibyte character
string <code class="code">*<var class="var">src</var></code> need not be NUL-terminated.  But if a NUL byte
is found within the <var class="var">nmc</var> first bytes of the string, the conversion
stops there.
</p>
<p>Like <code class="code">mbstowcs</code> the <var class="var">dst</var> parameter may be a null pointer and
the function can be used to count the number of wide characters that
would be required.
</p>
<p>This function is a GNU extension.  It is meant to work around the
problems mentioned above.  Now it is possible to convert a buffer with
multibyte character text piece by piece without having to care about
inserting NUL bytes and the effect of NUL bytes on the conversion state.
</p></dd></dl>

<p>A function to convert a multibyte string into a wide character string
and display it could be written like this (this is not a really useful
example):
</p>
<div class="example smallexample">
<pre class="example-preformatted">void
showmbs (const char *src, FILE *fp)
{
  mbstate_t state;
  int cnt = 0;
  memset (&amp;state, '\0', sizeof (state));
  while (1)
    {
      wchar_t linebuf[100];
      const char *endp = strchr (src, '\n');
      size_t n;

      /* <span class="r">Exit if there is no more line.</span>  */
      if (endp == NULL)
        break;

      n = mbsnrtowcs (linebuf, &amp;src, endp - src, 99, &amp;state);
      linebuf[n] = L'\0';
      fprintf (fp, &quot;line %d: \&quot;%S\&quot;\n&quot;, linebuf);
    }
}
</pre></div>

<p>There is no problem with the state after a call to <code class="code">mbsnrtowcs</code>.
Since we don&rsquo;t insert characters in the strings that were not in there
right from the beginning and we use <var class="var">state</var> only for the conversion
of the given buffer, there is no problem with altering the state.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcsnrtombs"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">wcsnrtombs</strong> <code class="def-code-arguments">(char *restrict <var class="var">dst</var>, const wchar_t **restrict <var class="var">src</var>, size_t <var class="var">nwc</var>, size_t <var class="var">len</var>, mbstate_t *restrict <var class="var">ps</var>)</code><a class="copiable-link" href="#index-wcsnrtombs"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:wcsnrtombs/!ps
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wcsnrtombs</code> function implements the conversion from wide
character strings to multibyte character strings.  It is similar to
<code class="code">wcsrtombs</code> but, just like <code class="code">mbsnrtowcs</code>, it takes an extra
parameter, which specifies the length of the input string.
</p>
<p>No more than <var class="var">nwc</var> wide characters from the input string
<code class="code">*<var class="var">src</var></code> are converted.  If the input string contains a NUL
wide character in the first <var class="var">nwc</var> characters, the conversion stops at
this place.
</p>
<p>The <code class="code">wcsnrtombs</code> function is a GNU extension and just like
<code class="code">mbsnrtowcs</code> helps in situations where no NUL-terminated input
strings are available.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Multibyte-Conversion-Example.html">A Complete Multibyte Conversion Example</a>, Previous: <a href="Converting-a-Character.html">Converting Single Characters</a>, Up: <a href="Restartable-multibyte-conversion.html">Restartable Multibyte Conversion Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
