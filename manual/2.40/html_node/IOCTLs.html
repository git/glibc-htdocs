<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>IOCTLs (The GNU C Library)</title>

<meta name="description" content="IOCTLs (The GNU C Library)">
<meta name="keywords" content="IOCTLs (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Low_002dLevel-I_002fO.html" rel="up" title="Low-Level I/O">
<link href="Other-Low_002dLevel-I_002fO-APIs.html" rel="next" title="Other Low-Level I/O APIs">
<link href="Interrupt-Input.html" rel="prev" title="Interrupt Input">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="IOCTLs">
<div class="nav-panel">
<p>
Next: <a href="Other-Low_002dLevel-I_002fO-APIs.html" accesskey="n" rel="next">Other low-level-I/O-related functions</a>, Previous: <a href="Interrupt-Input.html" accesskey="p" rel="prev">Interrupt-Driven Input</a>, Up: <a href="Low_002dLevel-I_002fO.html" accesskey="u" rel="up">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Generic-I_002fO-Control-operations"><span>13.20 Generic I/O Control operations<a class="copiable-link" href="#Generic-I_002fO-Control-operations"> &para;</a></span></h3>
<a class="index-entry-id" id="index-generic-i_002fo-control-operations"></a>
<a class="index-entry-id" id="index-IOCTLs"></a>

<p>GNU systems can handle most input/output operations on many different
devices and objects in terms of a few file primitives - <code class="code">read</code>,
<code class="code">write</code> and <code class="code">lseek</code>.  However, most devices also have a few
peculiar operations which do not fit into this model.  Such as:
</p>
<ul class="itemize mark-bullet">
<li>Changing the character font used on a terminal.

</li><li>Telling a magnetic tape system to rewind or fast forward.  (Since they
cannot move in byte increments, <code class="code">lseek</code> is inapplicable).

</li><li>Ejecting a disk from a drive.

</li><li>Playing an audio track from a CD-ROM drive.

</li><li>Maintaining routing tables for a network.

</li></ul>

<p>Although some such objects such as sockets and terminals
<a class="footnote" id="DOCF3" href="#FOOT3"><sup>3</sup></a> have special functions of their own, it would
not be practical to create functions for all these cases.
</p>
<p>Instead these minor operations, known as <em class="dfn">IOCTL</em>s, are assigned code
numbers and multiplexed through the <code class="code">ioctl</code> function, defined in
<code class="code">sys/ioctl.h</code>.  The code numbers themselves are defined in many
different headers.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-ioctl"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">ioctl</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, int <var class="var">command</var>, &hellip;)</code><a class="copiable-link" href="#index-ioctl"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">ioctl</code> function performs the generic I/O operation
<var class="var">command</var> on <var class="var">filedes</var>.
</p>
<p>A third argument is usually present, either a single number or a pointer
to a structure.  The meaning of this argument, the returned value, and
any error codes depends upon the command used.  Often <em class="math">-1</em> is
returned for a failure.
</p>
</dd></dl>

<p>On some systems, IOCTLs used by different devices share the same numbers.
Thus, although use of an inappropriate IOCTL <em class="emph">usually</em> only produces
an error, you should not attempt to use device-specific IOCTLs on an
unknown device.
</p>
<p>Most IOCTLs are OS-specific and/or only used in special system utilities,
and are thus beyond the scope of this document.  For an example of the use
of an IOCTL, see <a class="ref" href="Out_002dof_002dBand-Data.html">Out-of-Band Data</a>.
</p>
</div>
<div class="footnotes-segment">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5 class="footnote-body-heading"><a id="FOOT3" href="#DOCF3">(3)</a></h5>
<p>Actually, the terminal-specific functions are implemented with
IOCTLs on many platforms.</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Other-Low_002dLevel-I_002fO-APIs.html">Other low-level-I/O-related functions</a>, Previous: <a href="Interrupt-Input.html">Interrupt-Driven Input</a>, Up: <a href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
