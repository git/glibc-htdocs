<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Setting Groups (The GNU C Library)</title>

<meta name="description" content="Setting Groups (The GNU C Library)">
<meta name="keywords" content="Setting Groups (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Users-and-Groups.html" rel="up" title="Users and Groups">
<link href="Enable_002fDisable-Setuid.html" rel="next" title="Enable/Disable Setuid">
<link href="Setting-User-ID.html" rel="prev" title="Setting User ID">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Setting-Groups">
<div class="nav-panel">
<p>
Next: <a href="Enable_002fDisable-Setuid.html" accesskey="n" rel="next">Enabling and Disabling Setuid Access</a>, Previous: <a href="Setting-User-ID.html" accesskey="p" rel="prev">Setting the User ID</a>, Up: <a href="Users-and-Groups.html" accesskey="u" rel="up">Users and Groups</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Setting-the-Group-IDs"><span>31.7 Setting the Group IDs<a class="copiable-link" href="#Setting-the-Group-IDs"> &para;</a></span></h3>

<p>This section describes the functions for altering the group IDs (real
and effective) of a process.  To use these facilities, you must include
the header files <samp class="file">sys/types.h</samp> and <samp class="file">unistd.h</samp>.
<a class="index-entry-id" id="index-unistd_002eh-24"></a>
<a class="index-entry-id" id="index-sys_002ftypes_002eh-6"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setegid"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">setegid</strong> <code class="def-code-arguments">(gid_t <var class="var">newgid</var>)</code><a class="copiable-link" href="#index-setegid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function sets the effective group ID of the process to
<var class="var">newgid</var>, provided that the process is allowed to change its group
ID.  Just as with <code class="code">seteuid</code>, if the process is privileged it may
change its effective group ID to any value; if it isn&rsquo;t, but it has a
file group ID, then it may change to its real group ID or file group ID;
otherwise it may not change its effective group ID.
</p>
<p>Note that a process is only privileged if its effective <em class="emph">user</em> ID
is zero.  The effective group ID only affects access permissions.
</p>
<p>The return values and error conditions for <code class="code">setegid</code> are the same
as those for <code class="code">seteuid</code>.
</p>
<p>This function is only present if <code class="code">_POSIX_SAVED_IDS</code> is defined.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setgid"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">setgid</strong> <code class="def-code-arguments">(gid_t <var class="var">newgid</var>)</code><a class="copiable-link" href="#index-setgid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function sets both the real and effective group ID of the process
to <var class="var">newgid</var>, provided that the process is privileged.  It also
deletes the file group ID, if any.
</p>
<p>If the process is not privileged, then <code class="code">setgid</code> behaves like
<code class="code">setegid</code>.
</p>
<p>The return values and error conditions for <code class="code">setgid</code> are the same
as those for <code class="code">seteuid</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setregid"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">setregid</strong> <code class="def-code-arguments">(gid_t <var class="var">rgid</var>, gid_t <var class="var">egid</var>)</code><a class="copiable-link" href="#index-setregid"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function sets the real group ID of the process to <var class="var">rgid</var> and
the effective group ID to <var class="var">egid</var>.  If <var class="var">rgid</var> is <code class="code">-1</code>, it
means not to change the real group ID; likewise if <var class="var">egid</var> is
<code class="code">-1</code>, it means not to change the effective group ID.
</p>
<p>The <code class="code">setregid</code> function is provided for compatibility with 4.3 BSD
Unix, which does not support file IDs.  You can use this function to
swap the effective and real group IDs of the process.  (Privileged
processes are not limited to this usage.)  If file IDs are supported,
you should use that feature instead of using this function.
See <a class="xref" href="Enable_002fDisable-Setuid.html">Enabling and Disabling Setuid Access</a>.
</p>
<p>The return values and error conditions for <code class="code">setregid</code> are the same
as those for <code class="code">setreuid</code>.
</p></dd></dl>

<p><code class="code">setuid</code> and <code class="code">setgid</code> behave differently depending on whether
the effective user ID at the time is zero.  If it is not zero, they
behave like <code class="code">seteuid</code> and <code class="code">setegid</code>.  If it is, they change
both effective and real IDs and delete the file ID.  To avoid confusion,
we recommend you always use <code class="code">seteuid</code> and <code class="code">setegid</code> except
when you know the effective user ID is zero and your intent is to change
the persona permanently.  This case is rare&mdash;most of the programs that
need it, such as <code class="code">login</code> and <code class="code">su</code>, have already been written.
</p>
<p>Note that if your program is setuid to some user other than <code class="code">root</code>,
there is no way to drop privileges permanently.
</p>
<p>The system also lets privileged processes change their supplementary
group IDs.  To use <code class="code">setgroups</code> or <code class="code">initgroups</code>, your programs
should include the header file <samp class="file">grp.h</samp>.
<a class="index-entry-id" id="index-grp_002eh-1"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setgroups"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">setgroups</strong> <code class="def-code-arguments">(size_t <var class="var">count</var>, const gid_t *<var class="var">groups</var>)</code><a class="copiable-link" href="#index-setgroups"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function sets the process&rsquo;s supplementary group IDs.  It can only
be called from privileged processes.  The <var class="var">count</var> argument specifies
the number of group IDs in the array <var class="var">groups</var>.
</p>
<p>This function returns <code class="code">0</code> if successful and <code class="code">-1</code> on error.
The following <code class="code">errno</code> error conditions are defined for this
function:
</p>
<dl class="table">
<dt><code class="code">EPERM</code></dt>
<dd><p>The calling process is not privileged.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-initgroups-1"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">initgroups</strong> <code class="def-code-arguments">(const char *<var class="var">user</var>, gid_t <var class="var">group</var>)</code><a class="copiable-link" href="#index-initgroups-1"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt mem fd lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">initgroups</code> function sets the process&rsquo;s supplementary group
IDs to be the normal default for the user name <var class="var">user</var>.  The group
<var class="var">group</var> is automatically included.
</p>
<p>This function works by scanning the group database for all the groups
<var class="var">user</var> belongs to.  It then calls <code class="code">setgroups</code> with the list it
has constructed.
</p>
<p>The return values and error conditions are the same as for
<code class="code">setgroups</code>.
</p></dd></dl>

<p>If you are interested in the groups a particular user belongs to, but do
not want to change the process&rsquo;s supplementary group IDs, you can use
<code class="code">getgrouplist</code>.  To use <code class="code">getgrouplist</code>, your programs should
include the header file <samp class="file">grp.h</samp>.
<a class="index-entry-id" id="index-grp_002eh-2"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getgrouplist"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getgrouplist</strong> <code class="def-code-arguments">(const char *<var class="var">user</var>, gid_t <var class="var">group</var>, gid_t *<var class="var">groups</var>, int *<var class="var">ngroups</var>)</code><a class="copiable-link" href="#index-getgrouplist"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt mem fd lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getgrouplist</code> function scans the group database for all the
groups <var class="var">user</var> belongs to.  Up to *<var class="var">ngroups</var> group IDs
corresponding to these groups are stored in the array <var class="var">groups</var>; the
return value from the function is the number of group IDs actually
stored.  If *<var class="var">ngroups</var> is smaller than the total number of groups
found, then <code class="code">getgrouplist</code> returns a value of <code class="code">-1</code> and stores
the actual number of groups in *<var class="var">ngroups</var>.  The group <var class="var">group</var> is
automatically included in the list of groups returned by
<code class="code">getgrouplist</code>.
</p>
<p>Here&rsquo;s how to use <code class="code">getgrouplist</code> to read all supplementary groups
for <var class="var">user</var>:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">gid_t *
supplementary_groups (char *user)
{
  int ngroups = 16;
  gid_t *groups
    = (gid_t *) xmalloc (ngroups * sizeof (gid_t));
  struct passwd *pw = getpwnam (user);

  if (pw == NULL)
    return NULL;

  if (getgrouplist (pw-&gt;pw_name, pw-&gt;pw_gid, groups, &amp;ngroups) &lt; 0)
    {
      groups = xreallocarray (ngroups, sizeof *groups);
      getgrouplist (pw-&gt;pw_name, pw-&gt;pw_gid, groups, &amp;ngroups);
    }
  return groups;
}
</pre></div></div>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Enable_002fDisable-Setuid.html">Enabling and Disabling Setuid Access</a>, Previous: <a href="Setting-User-ID.html">Setting the User ID</a>, Up: <a href="Users-and-Groups.html">Users and Groups</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
