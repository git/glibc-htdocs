<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>FP Comparison Functions (The GNU C Library)</title>

<meta name="description" content="FP Comparison Functions (The GNU C Library)">
<meta name="keywords" content="FP Comparison Functions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Arithmetic-Functions.html" rel="up" title="Arithmetic Functions">
<link href="Misc-FP-Arithmetic.html" rel="next" title="Misc FP Arithmetic">
<link href="FP-Bit-Twiddling.html" rel="prev" title="FP Bit Twiddling">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="FP-Comparison-Functions">
<div class="nav-panel">
<p>
Next: <a href="Misc-FP-Arithmetic.html" accesskey="n" rel="next">Miscellaneous FP arithmetic functions</a>, Previous: <a href="FP-Bit-Twiddling.html" accesskey="p" rel="prev">Setting and modifying single bits of FP values</a>, Up: <a href="Arithmetic-Functions.html" accesskey="u" rel="up">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Floating_002dPoint-Comparison-Functions"><span>20.8.6 Floating-Point Comparison Functions<a class="copiable-link" href="#Floating_002dPoint-Comparison-Functions"> &para;</a></span></h4>
<a class="index-entry-id" id="index-unordered-comparison"></a>

<p>The standard C comparison operators provoke exceptions when one or other
of the operands is NaN.  For example,
</p>
<div class="example smallexample">
<pre class="example-preformatted">int v = a &lt; 1.0;
</pre></div>

<p>will raise an exception if <var class="var">a</var> is NaN.  (This does <em class="emph">not</em>
happen with <code class="code">==</code> and <code class="code">!=</code>; those merely return false and true,
respectively, when NaN is examined.)  Frequently this exception is
undesirable.  ISO&nbsp;C99<!-- /@w --> therefore defines comparison functions that
do not raise exceptions when NaN is examined.  All of the functions are
implemented as macros which allow their arguments to be of any
floating-point type.  The macros are guaranteed to evaluate their
arguments only once.  TS 18661-1:2014 adds such a macro for an
equality comparison that <em class="emph">does</em> raise an exception for a NaN
argument; it also adds functions that provide a total ordering on all
floating-point values, including NaNs, without raising any exceptions
even for signaling NaNs.
</p>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-isgreater"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">isgreater</strong> <code class="def-code-arguments">(<em class="emph">real-floating</em> <var class="var">x</var>, <em class="emph">real-floating</em> <var class="var">y</var>)</code><a class="copiable-link" href="#index-isgreater"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro determines whether the argument <var class="var">x</var> is greater than
<var class="var">y</var>.  It is equivalent to <code class="code">(<var class="var">x</var>) &gt; (<var class="var">y</var>)</code>, but no
exception is raised if <var class="var">x</var> or <var class="var">y</var> are NaN.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-isgreaterequal"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">isgreaterequal</strong> <code class="def-code-arguments">(<em class="emph">real-floating</em> <var class="var">x</var>, <em class="emph">real-floating</em> <var class="var">y</var>)</code><a class="copiable-link" href="#index-isgreaterequal"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro determines whether the argument <var class="var">x</var> is greater than or
equal to <var class="var">y</var>.  It is equivalent to <code class="code">(<var class="var">x</var>) &gt;= (<var class="var">y</var>)</code>, but no
exception is raised if <var class="var">x</var> or <var class="var">y</var> are NaN.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-isless"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">isless</strong> <code class="def-code-arguments">(<em class="emph">real-floating</em> <var class="var">x</var>, <em class="emph">real-floating</em> <var class="var">y</var>)</code><a class="copiable-link" href="#index-isless"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro determines whether the argument <var class="var">x</var> is less than <var class="var">y</var>.
It is equivalent to <code class="code">(<var class="var">x</var>) &lt; (<var class="var">y</var>)</code>, but no exception is
raised if <var class="var">x</var> or <var class="var">y</var> are NaN.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-islessequal"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">islessequal</strong> <code class="def-code-arguments">(<em class="emph">real-floating</em> <var class="var">x</var>, <em class="emph">real-floating</em> <var class="var">y</var>)</code><a class="copiable-link" href="#index-islessequal"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro determines whether the argument <var class="var">x</var> is less than or equal
to <var class="var">y</var>.  It is equivalent to <code class="code">(<var class="var">x</var>) &lt;= (<var class="var">y</var>)</code>, but no
exception is raised if <var class="var">x</var> or <var class="var">y</var> are NaN.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-islessgreater"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">islessgreater</strong> <code class="def-code-arguments">(<em class="emph">real-floating</em> <var class="var">x</var>, <em class="emph">real-floating</em> <var class="var">y</var>)</code><a class="copiable-link" href="#index-islessgreater"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro determines whether the argument <var class="var">x</var> is less or greater
than <var class="var">y</var>.  It is equivalent to <code class="code">(<var class="var">x</var>) &lt; (<var class="var">y</var>) ||
(<var class="var">x</var>) &gt; (<var class="var">y</var>)</code> (although it only evaluates <var class="var">x</var> and <var class="var">y</var>
once), but no exception is raised if <var class="var">x</var> or <var class="var">y</var> are NaN.
</p>
<p>This macro is not equivalent to <code class="code"><var class="var">x</var> != <var class="var">y</var></code>, because that
expression is true if <var class="var">x</var> or <var class="var">y</var> are NaN.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-isunordered"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">isunordered</strong> <code class="def-code-arguments">(<em class="emph">real-floating</em> <var class="var">x</var>, <em class="emph">real-floating</em> <var class="var">y</var>)</code><a class="copiable-link" href="#index-isunordered"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro determines whether its arguments are unordered.  In other
words, it is true if <var class="var">x</var> or <var class="var">y</var> are NaN, and false otherwise.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-iseqsig"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">iseqsig</strong> <code class="def-code-arguments">(<em class="emph">real-floating</em> <var class="var">x</var>, <em class="emph">real-floating</em> <var class="var">y</var>)</code><a class="copiable-link" href="#index-iseqsig"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro determines whether its arguments are equal.  It is
equivalent to <code class="code">(<var class="var">x</var>) == (<var class="var">y</var>)</code>, but it raises the invalid
exception and sets <code class="code">errno</code> to <code class="code">EDOM</code> if either argument is a
NaN.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-totalorder"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">totalorder</strong> <code class="def-code-arguments">(const double *<var class="var">x</var>, const double *<var class="var">y</var>)</code><a class="copiable-link" href="#index-totalorder"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-totalorderf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">totalorderf</strong> <code class="def-code-arguments">(const float *<var class="var">x</var>, const float *<var class="var">y</var>)</code><a class="copiable-link" href="#index-totalorderf"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-totalorderl"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">totalorderl</strong> <code class="def-code-arguments">(const long double *<var class="var">x</var>, const long double *<var class="var">y</var>)</code><a class="copiable-link" href="#index-totalorderl"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-totalorderfN"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">totalorderfN</strong> <code class="def-code-arguments">(const _Float<var class="var">N</var> *<var class="var">x</var>, const _Float<var class="var">N</var> *<var class="var">y</var>)</code><a class="copiable-link" href="#index-totalorderfN"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-totalorderfNx"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">totalorderfNx</strong> <code class="def-code-arguments">(const _Float<var class="var">N</var>x *<var class="var">x</var>, const _Float<var class="var">N</var>x *<var class="var">y</var>)</code><a class="copiable-link" href="#index-totalorderfNx"> &para;</a></span></dt>
<dd>


<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions determine whether the total order relationship,
defined in IEEE 754-2008, is true for <code class="code">*<var class="var">x</var></code> and
<code class="code">*<var class="var">y</var></code>, returning
nonzero if it is true and zero if it is false.  No exceptions are
raised even for signaling NaNs.  The relationship is true if they are
the same floating-point value (including sign for zero and NaNs, and
payload for NaNs), or if <code class="code">*<var class="var">x</var></code> comes before <code class="code">*<var class="var">y</var></code>
in the following
order: negative quiet NaNs, in order of decreasing payload; negative
signaling NaNs, in order of decreasing payload; negative infinity;
finite numbers, in ascending order, with negative zero before positive
zero; positive infinity; positive signaling NaNs, in order of
increasing payload; positive quiet NaNs, in order of increasing
payload.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-totalordermag"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">totalordermag</strong> <code class="def-code-arguments">(const double *<var class="var">x</var>, const double *<var class="var">y</var>)</code><a class="copiable-link" href="#index-totalordermag"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-totalordermagf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">totalordermagf</strong> <code class="def-code-arguments">(const float *<var class="var">x</var>, const float *<var class="var">y</var>)</code><a class="copiable-link" href="#index-totalordermagf"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-totalordermagl"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">totalordermagl</strong> <code class="def-code-arguments">(const long double *<var class="var">x</var>, const long double *<var class="var">y</var>)</code><a class="copiable-link" href="#index-totalordermagl"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-totalordermagfN"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">totalordermagfN</strong> <code class="def-code-arguments">(const _Float<var class="var">N</var> *<var class="var">x</var>, const _Float<var class="var">N</var> *<var class="var">y</var>)</code><a class="copiable-link" href="#index-totalordermagfN"> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-totalordermagfNx"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">totalordermagfNx</strong> <code class="def-code-arguments">(const _Float<var class="var">N</var>x *<var class="var">x</var>, const _Float<var class="var">N</var>x *<var class="var">y</var>)</code><a class="copiable-link" href="#index-totalordermagfNx"> &para;</a></span></dt>
<dd>


<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions determine whether the total order relationship,
defined in IEEE 754-2008, is true for the absolute values of <code class="code">*<var class="var">x</var></code>
and <code class="code">*<var class="var">y</var></code>, returning nonzero if it is true and zero if it is false.
No exceptions are raised even for signaling NaNs.
</p></dd></dl>

<p>Not all machines provide hardware support for these operations.  On
machines that don&rsquo;t, the macros can be very slow.  Therefore, you should
not use these functions when NaN is not a concern.
</p>
<p><strong class="strong">NB:</strong> There are no macros <code class="code">isequal</code> or <code class="code">isunequal</code>.
They are unnecessary, because the <code class="code">==</code> and <code class="code">!=</code> operators do
<em class="emph">not</em> throw an exception if one or both of the operands are NaN.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Misc-FP-Arithmetic.html">Miscellaneous FP arithmetic functions</a>, Previous: <a href="FP-Bit-Twiddling.html">Setting and modifying single bits of FP values</a>, Up: <a href="Arithmetic-Functions.html">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
