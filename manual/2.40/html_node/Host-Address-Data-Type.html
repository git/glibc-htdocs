<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Host Address Data Type (The GNU C Library)</title>

<meta name="description" content="Host Address Data Type (The GNU C Library)">
<meta name="keywords" content="Host Address Data Type (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Host-Addresses.html" rel="up" title="Host Addresses">
<link href="Host-Address-Functions.html" rel="next" title="Host Address Functions">
<link href="Abstract-Host-Addresses.html" rel="prev" title="Abstract Host Addresses">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Host-Address-Data-Type">
<div class="nav-panel">
<p>
Next: <a href="Host-Address-Functions.html" accesskey="n" rel="next">Host Address Functions</a>, Previous: <a href="Abstract-Host-Addresses.html" accesskey="p" rel="prev">Internet Host Addresses</a>, Up: <a href="Host-Addresses.html" accesskey="u" rel="up">Host Addresses</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Host-Address-Data-Type-1"><span>16.6.2.2 Host Address Data Type<a class="copiable-link" href="#Host-Address-Data-Type-1"> &para;</a></span></h4>

<p>IPv4 Internet host addresses are represented in some contexts as integers
(type <code class="code">uint32_t</code>).  In other contexts, the integer is
packaged inside a structure of type <code class="code">struct in_addr</code>.  It would
be better if the usage were made consistent, but it is not hard to extract
the integer from the structure or put the integer into a structure.
</p>
<p>You will find older code that uses <code class="code">unsigned long int</code> for
IPv4 Internet host addresses instead of <code class="code">uint32_t</code> or <code class="code">struct
in_addr</code>.  Historically <code class="code">unsigned long int</code> was a 32-bit number but
with 64-bit machines this has changed.  Using <code class="code">unsigned long int</code>
might break the code if it is used on machines where this type doesn&rsquo;t
have 32 bits.  <code class="code">uint32_t</code> is specified by Unix98 and guaranteed to have
32 bits.
</p>
<p>IPv6 Internet host addresses have 128 bits and are packaged inside a
structure of type <code class="code">struct in6_addr</code>.
</p>
<p>The following basic definitions for Internet addresses are declared in
the header file <samp class="file">netinet/in.h</samp>:
<a class="index-entry-id" id="index-netinet_002fin_002eh-1"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-in_005faddr"><span class="category-def">Data Type: </span><span><strong class="def-name">struct in_addr</strong><a class="copiable-link" href="#index-struct-in_005faddr"> &para;</a></span></dt>
<dd>
<p>This data type is used in certain contexts to contain an IPv4 Internet
host address.  It has just one field, named <code class="code">s_addr</code>, which records
the host address number as an <code class="code">uint32_t</code>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-INADDR_005fLOOPBACK"><span class="category-def">Macro: </span><span><code class="def-type">uint32_t</code> <strong class="def-name">INADDR_LOOPBACK</strong><a class="copiable-link" href="#index-INADDR_005fLOOPBACK"> &para;</a></span></dt>
<dd>
<p>You can use this constant to stand for &ldquo;the address of this machine,&rdquo;
instead of finding its actual address.  It is the IPv4 Internet address
&lsquo;<samp class="samp">127.0.0.1</samp>&rsquo;, which is usually called &lsquo;<samp class="samp">localhost</samp>&rsquo;.  This
special constant saves you the trouble of looking up the address of your
own machine.  Also, the system usually implements <code class="code">INADDR_LOOPBACK</code>
specially, avoiding any network traffic for the case of one machine
talking to itself.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-INADDR_005fANY"><span class="category-def">Macro: </span><span><code class="def-type">uint32_t</code> <strong class="def-name">INADDR_ANY</strong><a class="copiable-link" href="#index-INADDR_005fANY"> &para;</a></span></dt>
<dd>
<p>You can use this constant to stand for &ldquo;any incoming address&rdquo; when
binding to an address.  See <a class="xref" href="Setting-Address.html">Setting the Address of a Socket</a>.  This is the usual
address to give in the <code class="code">sin_addr</code> member of <code class="code">struct&nbsp;sockaddr_in</code><!-- /@w --> when you want to accept Internet connections.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-INADDR_005fBROADCAST"><span class="category-def">Macro: </span><span><code class="def-type">uint32_t</code> <strong class="def-name">INADDR_BROADCAST</strong><a class="copiable-link" href="#index-INADDR_005fBROADCAST"> &para;</a></span></dt>
<dd>
<p>This constant is the address you use to send a broadcast message.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-INADDR_005fNONE"><span class="category-def">Macro: </span><span><code class="def-type">uint32_t</code> <strong class="def-name">INADDR_NONE</strong><a class="copiable-link" href="#index-INADDR_005fNONE"> &para;</a></span></dt>
<dd>
<p>This constant is returned by some functions to indicate an error.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-struct-in6_005faddr"><span class="category-def">Data Type: </span><span><strong class="def-name">struct in6_addr</strong><a class="copiable-link" href="#index-struct-in6_005faddr"> &para;</a></span></dt>
<dd>
<p>This data type is used to store an IPv6 address.  It stores 128 bits of
data, which can be accessed (via a union) in a variety of ways.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-in6addr_005floopback"><span class="category-def">Constant: </span><span><code class="def-type">struct in6_addr</code> <strong class="def-name">in6addr_loopback</strong><a class="copiable-link" href="#index-in6addr_005floopback"> &para;</a></span></dt>
<dd>
<p>This constant is the IPv6 address &lsquo;<samp class="samp">::1</samp>&rsquo;, the loopback address.  See
above for a description of what this means.  The macro
<code class="code">IN6ADDR_LOOPBACK_INIT</code> is provided to allow you to initialize your
own variables to this value.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-in6addr_005fany"><span class="category-def">Constant: </span><span><code class="def-type">struct in6_addr</code> <strong class="def-name">in6addr_any</strong><a class="copiable-link" href="#index-in6addr_005fany"> &para;</a></span></dt>
<dd>
<p>This constant is the IPv6 address &lsquo;<samp class="samp">::</samp>&rsquo;, the unspecified address.  See
above for a description of what this means.  The macro
<code class="code">IN6ADDR_ANY_INIT</code> is provided to allow you to initialize your
own variables to this value.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Host-Address-Functions.html">Host Address Functions</a>, Previous: <a href="Abstract-Host-Addresses.html">Internet Host Addresses</a>, Up: <a href="Host-Addresses.html">Host Addresses</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
