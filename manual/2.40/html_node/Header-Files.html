<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Header Files (The GNU C Library)</title>

<meta name="description" content="Header Files (The GNU C Library)">
<meta name="keywords" content="Header Files (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Using-the-Library.html" rel="up" title="Using the Library">
<link href="Macro-Definitions.html" rel="next" title="Macro Definitions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Header-Files">
<div class="nav-panel">
<p>
Next: <a href="Macro-Definitions.html" accesskey="n" rel="next">Macro Definitions of Functions</a>, Up: <a href="Using-the-Library.html" accesskey="u" rel="up">Using the Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Header-Files-1"><span>1.3.1 Header Files<a class="copiable-link" href="#Header-Files-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-header-files"></a>

<p>Libraries for use by C programs really consist of two parts: <em class="dfn">header
files</em> that define types and macros and declare variables and
functions; and the actual library or <em class="dfn">archive</em> that contains the
definitions of the variables and functions.
</p>
<p>(Recall that in C, a <em class="dfn">declaration</em> merely provides information that
a function or variable exists and gives its type.  For a function
declaration, information about the types of its arguments might be
provided as well.  The purpose of declarations is to allow the compiler
to correctly process references to the declared variables and functions.
A <em class="dfn">definition</em>, on the other hand, actually allocates storage for a
variable or says what a function does.)
<a class="index-entry-id" id="index-definition-_0028compared-to-declaration_0029"></a>
<a class="index-entry-id" id="index-declaration-_0028compared-to-definition_0029"></a>
</p>
<p>In order to use the facilities in the GNU C Library, you should be sure
that your program source files include the appropriate header files.
This is so that the compiler has declarations of these facilities
available and can correctly process references to them.  Once your
program has been compiled, the linker resolves these references to
the actual definitions provided in the archive file.
</p>
<p>Header files are included into a program source file by the
&lsquo;<samp class="samp">#include</samp>&rsquo; preprocessor directive.  The C language supports two
forms of this directive; the first,
</p>
<div class="example smallexample">
<pre class="example-preformatted">#include &quot;<var class="var">header</var>&quot;
</pre></div>

<p>is typically used to include a header file <var class="var">header</var> that you write
yourself; this would contain definitions and declarations describing the
interfaces between the different parts of your particular application.
By contrast,
</p>
<div class="example smallexample">
<pre class="example-preformatted">#include &lt;file.h&gt;
</pre></div>

<p>is typically used to include a header file <samp class="file">file.h</samp> that contains
definitions and declarations for a standard library.  This file would
normally be installed in a standard place by your system administrator.
You should use this second form for the C library header files.
</p>
<p>Typically, &lsquo;<samp class="samp">#include</samp>&rsquo; directives are placed at the top of the C
source file, before any other code.  If you begin your source files with
some comments explaining what the code in the file does (a good idea),
put the &lsquo;<samp class="samp">#include</samp>&rsquo; directives immediately afterwards, following the
feature test macro definition (see <a class="pxref" href="Feature-Test-Macros.html">Feature Test Macros</a>).
</p>
<p>For more information about the use of header files and &lsquo;<samp class="samp">#include</samp>&rsquo;
directives, see <a data-manual="cpp.info" href="https://gcc.gnu.org/onlinedocs/cpp/Header-Files.html#Header-Files">Header Files</a> in <cite class="cite">The GNU C Preprocessor
Manual</cite>.
</p>
<p>The GNU C Library provides several header files, each of which contains
the type and macro definitions and variable and function declarations
for a group of related facilities.  This means that your programs may
need to include several header files, depending on exactly which
facilities you are using.
</p>
<p>Some library header files include other library header files
automatically.  However, as a matter of programming style, you should
not rely on this; it is better to explicitly include all the header
files required for the library facilities you are using.  The GNU C Library
header files have been written in such a way that it doesn&rsquo;t
matter if a header file is accidentally included more than once;
including a header file a second time has no effect.  Likewise, if your
program needs to include multiple header files, the order in which they
are included doesn&rsquo;t matter.
</p>
<p><strong class="strong">Compatibility Note:</strong> Inclusion of standard header files in any
order and any number of times works in any ISO&nbsp;C<!-- /@w --> implementation.
However, this has traditionally not been the case in many older C
implementations.
</p>
<p>Strictly speaking, you don&rsquo;t <em class="emph">have to</em> include a header file to use
a function it declares; you could declare the function explicitly
yourself, according to the specifications in this manual.  But it is
usually better to include the header file because it may define types
and macros that are not otherwise available and because it may define
more efficient macro replacements for some functions.  It is also a sure
way to have the correct declaration.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Macro-Definitions.html">Macro Definitions of Functions</a>, Up: <a href="Using-the-Library.html">Using the Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
