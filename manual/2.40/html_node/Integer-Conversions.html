<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Integer Conversions (The GNU C Library)</title>

<meta name="description" content="Integer Conversions (The GNU C Library)">
<meta name="keywords" content="Integer Conversions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Output.html" rel="up" title="Formatted Output">
<link href="Floating_002dPoint-Conversions.html" rel="next" title="Floating-Point Conversions">
<link href="Table-of-Output-Conversions.html" rel="prev" title="Table of Output Conversions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Integer-Conversions">
<div class="nav-panel">
<p>
Next: <a href="Floating_002dPoint-Conversions.html" accesskey="n" rel="next">Floating-Point Conversions</a>, Previous: <a href="Table-of-Output-Conversions.html" accesskey="p" rel="prev">Table of Output Conversions</a>, Up: <a href="Formatted-Output.html" accesskey="u" rel="up">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Integer-Conversions-1"><span>12.12.4 Integer Conversions<a class="copiable-link" href="#Integer-Conversions-1"> &para;</a></span></h4>

<p>This section describes the options for the &lsquo;<samp class="samp">%d</samp>&rsquo;, &lsquo;<samp class="samp">%i</samp>&rsquo;,
&lsquo;<samp class="samp">%b</samp>&rsquo;, &lsquo;<samp class="samp">%B</samp>&rsquo;, &lsquo;<samp class="samp">%o</samp>&rsquo;, &lsquo;<samp class="samp">%u</samp>&rsquo;, &lsquo;<samp class="samp">%x</samp>&rsquo;, and &lsquo;<samp class="samp">%X</samp>&rsquo; conversion
specifications.  These conversions print integers in various formats.
</p>
<p>The &lsquo;<samp class="samp">%d</samp>&rsquo; and &lsquo;<samp class="samp">%i</samp>&rsquo; conversion specifications both print an
<code class="code">int</code> argument as a signed decimal number; while &lsquo;<samp class="samp">%b</samp>&rsquo;, &lsquo;<samp class="samp">%o</samp>&rsquo;,
&lsquo;<samp class="samp">%u</samp>&rsquo;, and &lsquo;<samp class="samp">%x</samp>&rsquo; print the argument as an unsigned binary, octal,
decimal, or hexadecimal number (respectively).  The &lsquo;<samp class="samp">%X</samp>&rsquo; conversion
specification is just like &lsquo;<samp class="samp">%x</samp>&rsquo; except that it uses the characters
&lsquo;<samp class="samp">ABCDEF</samp>&rsquo; as digits instead of &lsquo;<samp class="samp">abcdef</samp>&rsquo;.  The &lsquo;<samp class="samp">%B</samp>&rsquo;
conversion specification is just like &lsquo;<samp class="samp">%b</samp>&rsquo; except that, with the
&lsquo;<samp class="samp">#</samp>&rsquo; flag, the output starts with &lsquo;<samp class="samp">0B</samp>&rsquo; instead of &lsquo;<samp class="samp">0b</samp>&rsquo;.
</p>
<p>The following flags are meaningful:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">-</samp>&rsquo;</dt>
<dd><p>Left-justify the result in the field (instead of the normal
right-justification).
</p>
</dd>
<dt>&lsquo;<samp class="samp">+</samp>&rsquo;</dt>
<dd><p>For the signed &lsquo;<samp class="samp">%d</samp>&rsquo; and &lsquo;<samp class="samp">%i</samp>&rsquo; conversions, print a
plus sign if the value is positive.
</p>
</dd>
<dt>&lsquo;<samp class="samp"> </samp>&rsquo;</dt>
<dd><p>For the signed &lsquo;<samp class="samp">%d</samp>&rsquo; and &lsquo;<samp class="samp">%i</samp>&rsquo; conversions, if the result
doesn&rsquo;t start with a plus or minus sign, prefix it with a space
character instead.  Since the &lsquo;<samp class="samp">+</samp>&rsquo; flag ensures that the result
includes a sign, this flag is ignored if you supply both of them.
</p>
</dd>
<dt>&lsquo;<samp class="samp">#</samp>&rsquo;</dt>
<dd><p>For the &lsquo;<samp class="samp">%o</samp>&rsquo; conversion, this forces the leading digit to be
&lsquo;<samp class="samp">0</samp>&rsquo;, as if by increasing the precision.  For &lsquo;<samp class="samp">%x</samp>&rsquo; or
&lsquo;<samp class="samp">%X</samp>&rsquo;, this prefixes a leading &lsquo;<samp class="samp">0x</samp>&rsquo; or &lsquo;<samp class="samp">0X</samp>&rsquo;
(respectively) to the result.  For &lsquo;<samp class="samp">%b</samp>&rsquo; or &lsquo;<samp class="samp">%B</samp>&rsquo;, this
prefixes a leading &lsquo;<samp class="samp">0b</samp>&rsquo; or &lsquo;<samp class="samp">0B</samp>&rsquo; (respectively)
to the result.  This doesn&rsquo;t do anything useful for the &lsquo;<samp class="samp">%d</samp>&rsquo;,
&lsquo;<samp class="samp">%i</samp>&rsquo;, or &lsquo;<samp class="samp">%u</samp>&rsquo; conversions.  Using this flag produces output
which can be parsed by the <code class="code">strtoul</code> function (see <a class="pxref" href="Parsing-of-Integers.html">Parsing of Integers</a>) and <code class="code">scanf</code> with the &lsquo;<samp class="samp">%i</samp>&rsquo; conversion
(see <a class="pxref" href="Numeric-Input-Conversions.html">Numeric Input Conversions</a>).
</p>
<p>For the &lsquo;<samp class="samp">%m</samp>&rsquo; conversion, print an error constant or decimal error
number, instead of a (possibly translated) error message.
</p>
</dd>
<dt>&lsquo;<samp class="samp">'</samp>&rsquo;</dt>
<dd><p>Separate the digits into groups as specified by the locale specified for
the <code class="code">LC_NUMERIC</code> category; see <a class="pxref" href="General-Numeric.html">Generic Numeric Formatting Parameters</a>.  This flag is a
GNU extension.
</p>
</dd>
<dt>&lsquo;<samp class="samp">0</samp>&rsquo;</dt>
<dd><p>Pad the field with zeros instead of spaces.  The zeros are placed after
any indication of sign or base.  This flag is ignored if the &lsquo;<samp class="samp">-</samp>&rsquo;
flag is also specified, or if a precision is specified.
</p></dd>
</dl>

<p>If a precision is supplied, it specifies the minimum number of digits to
appear; leading zeros are produced if necessary.  If you don&rsquo;t specify a
precision, the number is printed with as many digits as it needs.  If
you convert a value of zero with an explicit precision of zero, then no
characters at all are produced.
</p>
<p>Without a type modifier, the corresponding argument is treated as an
<code class="code">int</code> (for the signed conversions &lsquo;<samp class="samp">%i</samp>&rsquo; and &lsquo;<samp class="samp">%d</samp>&rsquo;) or
<code class="code">unsigned int</code> (for the unsigned conversions &lsquo;<samp class="samp">%b</samp>&rsquo;,
&lsquo;<samp class="samp">%B</samp>&rsquo;, &lsquo;<samp class="samp">%o</samp>&rsquo;, &lsquo;<samp class="samp">%u</samp>&rsquo;,
&lsquo;<samp class="samp">%x</samp>&rsquo;, and &lsquo;<samp class="samp">%X</samp>&rsquo;).  Recall that since <code class="code">printf</code> and friends
are variadic, any <code class="code">char</code> and <code class="code">short</code> arguments are
automatically converted to <code class="code">int</code> by the default argument
promotions.  For arguments of other integer types, you can use these
modifiers:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">hh</samp>&rsquo;</dt>
<dd><p>Specifies that the argument is a <code class="code">signed char</code> or <code class="code">unsigned
char</code>, as appropriate.  A <code class="code">char</code> argument is converted to an
<code class="code">int</code> or <code class="code">unsigned int</code> by the default argument promotions
anyway, but the &lsquo;<samp class="samp">hh</samp>&rsquo; modifier says to convert it back to a
<code class="code">char</code> again.
</p>
<p>This modifier was introduced in ISO&nbsp;C99<!-- /@w -->.
</p>
</dd>
<dt>&lsquo;<samp class="samp">h</samp>&rsquo;</dt>
<dd><p>Specifies that the argument is a <code class="code">short int</code> or <code class="code">unsigned
short int</code>, as appropriate.  A <code class="code">short</code> argument is converted to an
<code class="code">int</code> or <code class="code">unsigned int</code> by the default argument promotions
anyway, but the &lsquo;<samp class="samp">h</samp>&rsquo; modifier says to convert it back to a
<code class="code">short</code> again.
</p>
</dd>
<dt>&lsquo;<samp class="samp">j</samp>&rsquo;</dt>
<dd><p>Specifies that the argument is a <code class="code">intmax_t</code> or <code class="code">uintmax_t</code>, as
appropriate.
</p>
<p>This modifier was introduced in ISO&nbsp;C99<!-- /@w -->.
</p>
</dd>
<dt>&lsquo;<samp class="samp">l</samp>&rsquo;</dt>
<dd><p>Specifies that the argument is a <code class="code">long int</code> or <code class="code">unsigned long
int</code>, as appropriate.  Two &lsquo;<samp class="samp">l</samp>&rsquo; characters are like the &lsquo;<samp class="samp">L</samp>&rsquo;
modifier, below.
</p>
<p>If used with &lsquo;<samp class="samp">%c</samp>&rsquo; or &lsquo;<samp class="samp">%s</samp>&rsquo; the corresponding parameter is
considered as a wide character or wide character string respectively.
This use of &lsquo;<samp class="samp">l</samp>&rsquo; was introduced in Amendment&nbsp;1<!-- /@w --> to ISO&nbsp;C90<!-- /@w -->.
</p>
</dd>
<dt>&lsquo;<samp class="samp">L</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">ll</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">q</samp>&rsquo;</dt>
<dd><p>Specifies that the argument is a <code class="code">long long int</code>.  (This type is
an extension supported by the GNU C compiler.  On systems that don&rsquo;t
support extra-long integers, this is the same as <code class="code">long int</code>.)
</p>
<p>The &lsquo;<samp class="samp">q</samp>&rsquo; modifier is another name for the same thing, which comes
from 4.4 BSD; a <code class="code">long&nbsp;long&nbsp;int</code><!-- /@w --> is sometimes called a &ldquo;quad&rdquo;
<code class="code">int</code>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">t</samp>&rsquo;</dt>
<dd><p>Specifies that the argument is a <code class="code">ptrdiff_t</code>.
</p>
<p>This modifier was introduced in ISO&nbsp;C99<!-- /@w -->.
</p>
</dd>
<dt>&lsquo;<samp class="samp">w<var class="var">n</var></samp>&rsquo;</dt>
<dd><p>Specifies that the argument is a <code class="code">int<var class="var">n</var>_t</code> or
<code class="code">int_least<var class="var">n</var>_t</code> (which are the same type), for conversions
taking signed integers, or <code class="code">uint<var class="var">n</var>_t</code> or
<code class="code">uint_least<var class="var">n</var>_t</code> (which are the same type), for conversions
taking unsigned integers.  If the type is narrower than <code class="code">int</code>,
the promoted argument is converted back to the specified type.
</p>
<p>This modifier was introduced in ISO&nbsp;C23<!-- /@w -->.
</p>
</dd>
<dt>&lsquo;<samp class="samp">wf<var class="var">n</var></samp>&rsquo;</dt>
<dd><p>Specifies that the argument is a <code class="code">int_fast<var class="var">n</var>_t</code> or
<code class="code">uint_fast<var class="var">n</var>_t</code>, as appropriate.  If the type is narrower
than <code class="code">int</code>, the promoted argument is converted back to the
specified type.
</p>
<p>This modifier was introduced in ISO&nbsp;C23<!-- /@w -->.
</p>
</dd>
<dt>&lsquo;<samp class="samp">z</samp>&rsquo;</dt>
<dt>&lsquo;<samp class="samp">Z</samp>&rsquo;</dt>
<dd><p>Specifies that the argument is a <code class="code">size_t</code>.
</p>
<p>&lsquo;<samp class="samp">z</samp>&rsquo; was introduced in ISO&nbsp;C99<!-- /@w -->.  &lsquo;<samp class="samp">Z</samp>&rsquo; is a GNU extension
predating this addition and should not be used in new code.
</p></dd>
</dl>

<p>Here is an example.  Using the template string:
</p>
<div class="example smallexample">
<pre class="example-preformatted">&quot;|%5d|%-5d|%+5d|%+-5d|% 5d|%05d|%5.0d|%5.2d|%d|\n&quot;
</pre></div>

<p>to print numbers using the different options for the &lsquo;<samp class="samp">%d</samp>&rsquo;
conversion gives results like:
</p>
<div class="example smallexample">
<pre class="example-preformatted">|    0|0    |   +0|+0   |    0|00000|     |   00|0|
|    1|1    |   +1|+1   |    1|00001|    1|   01|1|
|   -1|-1   |   -1|-1   |   -1|-0001|   -1|  -01|-1|
|100000|100000|+100000|+100000| 100000|100000|100000|100000|100000|
</pre></div>

<p>In particular, notice what happens in the last case where the number
is too large to fit in the minimum field width specified.
</p>
<p>Here are some more examples showing how unsigned integers print under
various format options, using the template string:
</p>
<div class="example smallexample">
<pre class="example-preformatted">&quot;|%5u|%5o|%5x|%5X|%#5o|%#5x|%#5X|%#10.8x|\n&quot;
</pre></div>

<div class="example smallexample">
<pre class="example-preformatted">|    0|    0|    0|    0|    0|    0|    0|  00000000|
|    1|    1|    1|    1|   01|  0x1|  0X1|0x00000001|
|100000|303240|186a0|186A0|0303240|0x186a0|0X186A0|0x000186a0|
</pre></div>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Floating_002dPoint-Conversions.html">Floating-Point Conversions</a>, Previous: <a href="Table-of-Output-Conversions.html">Table of Output Conversions</a>, Up: <a href="Formatted-Output.html">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
