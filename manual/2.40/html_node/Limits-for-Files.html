<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Limits for Files (The GNU C Library)</title>

<meta name="description" content="Limits for Files (The GNU C Library)">
<meta name="keywords" content="Limits for Files (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="System-Configuration.html" rel="up" title="System Configuration">
<link href="Options-for-Files.html" rel="next" title="Options for Files">
<link href="Minimums.html" rel="prev" title="Minimums">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Limits-for-Files">
<div class="nav-panel">
<p>
Next: <a href="Options-for-Files.html" accesskey="n" rel="next">Optional Features in File Support</a>, Previous: <a href="Minimums.html" accesskey="p" rel="prev">Minimum Values for General Capacity Limits</a>, Up: <a href="System-Configuration.html" accesskey="u" rel="up">System Configuration Parameters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Limits-on-File-System-Capacity"><span>33.6 Limits on File System Capacity<a class="copiable-link" href="#Limits-on-File-System-Capacity"> &para;</a></span></h3>

<p>The POSIX.1 standard specifies a number of parameters that describe the
limitations of the file system.  It&rsquo;s possible for the system to have a
fixed, uniform limit for a parameter, but this isn&rsquo;t the usual case.  On
most systems, it&rsquo;s possible for different file systems (and, for some
parameters, even different files) to have different maximum limits.  For
example, this is very likely if you use NFS to mount some of the file
systems from other machines.
</p>
<a class="index-entry-id" id="index-limits_002eh-3"></a>
<p>Each of the following macros is defined in <samp class="file">limits.h</samp> only if the
system has a fixed, uniform limit for the parameter in question.  If the
system allows different file systems or files to have different limits,
then the macro is undefined; use <code class="code">pathconf</code> or <code class="code">fpathconf</code> to
find out the limit that applies to a particular file.  See <a class="xref" href="Pathconf.html">Using <code class="code">pathconf</code></a>.
</p>
<p>Each parameter also has another macro, with a name starting with
&lsquo;<samp class="samp">_POSIX</samp>&rsquo;, which gives the lowest value that the limit is allowed to
have on <em class="emph">any</em> POSIX system.  See <a class="xref" href="File-Minimums.html">Minimum Values for File System Limits</a>.
</p>
<a class="index-entry-id" id="index-limits_002c-link-count-of-files"></a>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-LINK_005fMAX"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">LINK_MAX</strong><a class="copiable-link" href="#index-LINK_005fMAX"> &para;</a></span></dt>
<dd>
<p>The uniform system limit (if any) for the number of names for a given
file.  See <a class="xref" href="Hard-Links.html">Hard Links</a>.
</p></dd></dl>

<a class="index-entry-id" id="index-limits_002c-terminal-input-queue"></a>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-MAX_005fCANON"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">MAX_CANON</strong><a class="copiable-link" href="#index-MAX_005fCANON"> &para;</a></span></dt>
<dd>
<p>The uniform system limit (if any) for the amount of text in a line of
input when input editing is enabled.  See <a class="xref" href="Canonical-or-Not.html">Two Styles of Input: Canonical or Not</a>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-MAX_005fINPUT"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">MAX_INPUT</strong><a class="copiable-link" href="#index-MAX_005fINPUT"> &para;</a></span></dt>
<dd>
<p>The uniform system limit (if any) for the total number of characters
typed ahead as input.  See <a class="xref" href="I_002fO-Queues.html">I/O Queues</a>.
</p></dd></dl>

<a class="index-entry-id" id="index-limits_002c-file-name-length"></a>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-NAME_005fMAX"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">NAME_MAX</strong><a class="copiable-link" href="#index-NAME_005fMAX"> &para;</a></span></dt>
<dd>
<p>The uniform system limit (if any) for the length of a file name component, not
including the terminating null character.
</p>
<p><strong class="strong">Portability Note:</strong> On some systems, the GNU C Library defines
<code class="code">NAME_MAX</code>, but does not actually enforce this limit.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-PATH_005fMAX"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">PATH_MAX</strong><a class="copiable-link" href="#index-PATH_005fMAX"> &para;</a></span></dt>
<dd>
<p>The uniform system limit (if any) for the length of an entire file name (that
is, the argument given to system calls such as <code class="code">open</code>), including the
terminating null character.
</p>
<p><strong class="strong">Portability Note:</strong> The GNU C Library does not enforce this limit
even if <code class="code">PATH_MAX</code> is defined.
</p></dd></dl>

<a class="index-entry-id" id="index-limits_002c-pipe-buffer-size"></a>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-PIPE_005fBUF"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">PIPE_BUF</strong><a class="copiable-link" href="#index-PIPE_005fBUF"> &para;</a></span></dt>
<dd>
<p>The uniform system limit (if any) for the number of bytes that can be
written atomically to a pipe.  If multiple processes are writing to the
same pipe simultaneously, output from different processes might be
interleaved in chunks of this size.  See <a class="xref" href="Pipes-and-FIFOs.html">Pipes and FIFOs</a>.
</p></dd></dl>

<p>These are alternative macro names for some of the same information.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-MAXNAMLEN"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">MAXNAMLEN</strong><a class="copiable-link" href="#index-MAXNAMLEN"> &para;</a></span></dt>
<dd>
<p>This is the BSD name for <code class="code">NAME_MAX</code>.  It is defined in
<samp class="file">dirent.h</samp>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-FILENAME_005fMAX"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">FILENAME_MAX</strong><a class="copiable-link" href="#index-FILENAME_005fMAX"> &para;</a></span></dt>
<dd>
<p>The value of this macro is an integer constant expression that
represents the maximum length of a file name string.  It is defined in
<samp class="file">stdio.h</samp>.
</p>
<p>Unlike <code class="code">PATH_MAX</code>, this macro is defined even if there is no actual
limit imposed.  In such a case, its value is typically a very large
number.  <strong class="strong">This is always the case on GNU/Hurd systems.</strong>
</p>
<p><strong class="strong">Usage Note:</strong> Don&rsquo;t use <code class="code">FILENAME_MAX</code> as the size of an
array in which to store a file name!  You can&rsquo;t possibly make an array
that big!  Use dynamic allocation (see <a class="pxref" href="Memory-Allocation.html">Allocating Storage For Program Data</a>) instead.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Options-for-Files.html">Optional Features in File Support</a>, Previous: <a href="Minimums.html">Minimum Values for General Capacity Limits</a>, Up: <a href="System-Configuration.html">System Configuration Parameters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
