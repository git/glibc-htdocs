<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Stopped and Terminated Jobs (The GNU C Library)</title>

<meta name="description" content="Stopped and Terminated Jobs (The GNU C Library)">
<meta name="keywords" content="Stopped and Terminated Jobs (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Implementing-a-Shell.html" rel="up" title="Implementing a Shell">
<link href="Continuing-Stopped-Jobs.html" rel="next" title="Continuing Stopped Jobs">
<link href="Foreground-and-Background.html" rel="prev" title="Foreground and Background">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Stopped-and-Terminated-Jobs">
<div class="nav-panel">
<p>
Next: <a href="Continuing-Stopped-Jobs.html" accesskey="n" rel="next">Continuing Stopped Jobs</a>, Previous: <a href="Foreground-and-Background.html" accesskey="p" rel="prev">Foreground and Background</a>, Up: <a href="Implementing-a-Shell.html" accesskey="u" rel="up">Implementing a Job Control Shell</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Stopped-and-Terminated-Jobs-1"><span>29.5.5 Stopped and Terminated Jobs<a class="copiable-link" href="#Stopped-and-Terminated-Jobs-1"> &para;</a></span></h4>

<a class="index-entry-id" id="index-stopped-jobs_002c-detecting"></a>
<a class="index-entry-id" id="index-terminated-jobs_002c-detecting"></a>
<p>When a foreground process is launched, the shell must block until all of
the processes in that job have either terminated or stopped.  It can do
this by calling the <code class="code">waitpid</code> function; see <a class="ref" href="Process-Completion.html">Process Completion</a>.  Use the <code class="code">WUNTRACED</code> option so that status is reported
for processes that stop as well as processes that terminate.
</p>
<p>The shell must also check on the status of background jobs so that it
can report terminated and stopped jobs to the user; this can be done by
calling <code class="code">waitpid</code> with the <code class="code">WNOHANG</code> option.  A good place to
put a such a check for terminated and stopped jobs is just before
prompting for a new command.
</p>
<a class="index-entry-id" id="index-SIGCHLD_002c-handling-of"></a>
<p>The shell can also receive asynchronous notification that there is
status information available for a child process by establishing a
handler for <code class="code">SIGCHLD</code> signals.  See <a class="xref" href="Signal-Handling.html">Signal Handling</a>.
</p>
<p>In the sample shell program, the <code class="code">SIGCHLD</code> signal is normally
ignored.  This is to avoid reentrancy problems involving the global data
structures the shell manipulates.  But at specific times when the shell
is not using these data structures&mdash;such as when it is waiting for
input on the terminal&mdash;it makes sense to enable a handler for
<code class="code">SIGCHLD</code>.  The same function that is used to do the synchronous
status checks (<code class="code">do_job_notification</code>, in this case) can also be
called from within this handler.
</p>
<p>Here are the parts of the sample shell program that deal with checking
the status of jobs and reporting the information to the user.
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">/* <span class="r">Store the status of the process <var class="var">pid</var> that was returned by waitpid.</span>
   <span class="r">Return 0 if all went well, nonzero otherwise.</span>  */

int
mark_process_status (pid_t pid, int status)
{
  job *j;
  process *p;
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">  if (pid &gt; 0)
    {
      /* <span class="r">Update the record for the process.</span>  */
      for (j = first_job; j; j = j-&gt;next)
        for (p = j-&gt;first_process; p; p = p-&gt;next)
          if (p-&gt;pid == pid)
            {
              p-&gt;status = status;
              if (WIFSTOPPED (status))
                p-&gt;stopped = 1;
              else
                {
                  p-&gt;completed = 1;
                  if (WIFSIGNALED (status))
                    fprintf (stderr, &quot;%d: Terminated by signal %d.\n&quot;,
                             (int) pid, WTERMSIG (p-&gt;status));
                }
              return 0;
             }
      fprintf (stderr, &quot;No child process %d.\n&quot;, pid);
      return -1;
    }
</pre></div><div class="group"><pre class="example-preformatted">  else if (pid == 0 || errno == ECHILD)
    /* <span class="r">No processes ready to report.</span>  */
    return -1;
  else {
    /* <span class="r">Other weird errors.</span>  */
    perror (&quot;waitpid&quot;);
    return -1;
  }
}
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">/* <span class="r">Check for processes that have status information available,</span>
   <span class="r">without blocking.</span>  */

void
update_status (void)
{
  int status;
  pid_t pid;

  do
    pid = waitpid (WAIT_ANY, &amp;status, WUNTRACED|WNOHANG);
  while (!mark_process_status (pid, status));
}
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">/* <span class="r">Check for processes that have status information available,</span>
   <span class="r">blocking until all processes in the given job have reported.</span>  */

void
wait_for_job (job *j)
{
  int status;
  pid_t pid;

  do
    pid = waitpid (WAIT_ANY, &amp;status, WUNTRACED);
  while (!mark_process_status (pid, status)
         &amp;&amp; !job_is_stopped (j)
         &amp;&amp; !job_is_completed (j));
}
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">/* <span class="r">Format information about job status for the user to look at.</span>  */

void
format_job_info (job *j, const char *status)
{
  fprintf (stderr, &quot;%ld (%s): %s\n&quot;, (long)j-&gt;pgid, status, j-&gt;command);
}
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">/* <span class="r">Notify the user about stopped or terminated jobs.</span>
   <span class="r">Delete terminated jobs from the active job list.</span>  */

void
do_job_notification (void)
{
  job *j, *jlast, *jnext;

  /* <span class="r">Update status information for child processes.</span>  */
  update_status ();

  jlast = NULL;
  for (j = first_job; j; j = jnext)
    {
      jnext = j-&gt;next;

      /* <span class="r">If all processes have completed, tell the user the job has</span>
         <span class="r">completed and delete it from the list of active jobs.</span>  */
      if (job_is_completed (j)) {
        format_job_info (j, &quot;completed&quot;);
        if (jlast)
          jlast-&gt;next = jnext;
        else
          first_job = jnext;
        free_job (j);
      }

      /* <span class="r">Notify the user about stopped jobs,</span>
         <span class="r">marking them so that we won&rsquo;t do this more than once.</span>  */
      else if (job_is_stopped (j) &amp;&amp; !j-&gt;notified) {
        format_job_info (j, &quot;stopped&quot;);
        j-&gt;notified = 1;
        jlast = j;
      }

      /* <span class="r">Don&rsquo;t say anything about jobs that are still running.</span>  */
      else
        jlast = j;
    }
}
</pre></div></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Continuing-Stopped-Jobs.html">Continuing Stopped Jobs</a>, Previous: <a href="Foreground-and-Background.html">Foreground and Background</a>, Up: <a href="Implementing-a-Shell.html">Implementing a Job Control Shell</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
