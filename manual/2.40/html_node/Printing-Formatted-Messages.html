<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Printing Formatted Messages (The GNU C Library)</title>

<meta name="description" content="Printing Formatted Messages (The GNU C Library)">
<meta name="keywords" content="Printing Formatted Messages (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Messages.html" rel="up" title="Formatted Messages">
<link href="Adding-Severity-Classes.html" rel="next" title="Adding Severity Classes">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Printing-Formatted-Messages">
<div class="nav-panel">
<p>
Next: <a href="Adding-Severity-Classes.html" accesskey="n" rel="next">Adding Severity Classes</a>, Up: <a href="Formatted-Messages.html" accesskey="u" rel="up">Formatted Messages</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Printing-Formatted-Messages-1"><span>12.22.1 Printing Formatted Messages<a class="copiable-link" href="#Printing-Formatted-Messages-1"> &para;</a></span></h4>

<p>Messages can be printed to standard error and/or to the console.  To
select the destination the programmer can use the following two values,
bitwise OR combined if wanted, for the <var class="var">classification</var> parameter of
<code class="code">fmtmsg</code>:
</p>
<dl class="vtable">
<dt><a id="index-MM_005fPRINT"></a><span><code class="code">MM_PRINT</code><a class="copiable-link" href="#index-MM_005fPRINT"> &para;</a></span></dt>
<dd><p>Display the message in standard error.
</p></dd>
<dt><a id="index-MM_005fCONSOLE"></a><span><code class="code">MM_CONSOLE</code><a class="copiable-link" href="#index-MM_005fCONSOLE"> &para;</a></span></dt>
<dd><p>Display the message on the system console.
</p></dd>
</dl>

<p>The erroneous piece of the system can be signalled by exactly one of the
following values which also is bitwise ORed with the
<var class="var">classification</var> parameter to <code class="code">fmtmsg</code>:
</p>
<dl class="vtable">
<dt><a id="index-MM_005fHARD"></a><span><code class="code">MM_HARD</code><a class="copiable-link" href="#index-MM_005fHARD"> &para;</a></span></dt>
<dd><p>The source of the condition is some hardware.
</p></dd>
<dt><a id="index-MM_005fSOFT"></a><span><code class="code">MM_SOFT</code><a class="copiable-link" href="#index-MM_005fSOFT"> &para;</a></span></dt>
<dd><p>The source of the condition is some software.
</p></dd>
<dt><a id="index-MM_005fFIRM"></a><span><code class="code">MM_FIRM</code><a class="copiable-link" href="#index-MM_005fFIRM"> &para;</a></span></dt>
<dd><p>The source of the condition is some firmware.
</p></dd>
</dl>

<p>A third component of the <var class="var">classification</var> parameter to <code class="code">fmtmsg</code>
can describe the part of the system which detects the problem.  This is
done by using exactly one of the following values:
</p>
<dl class="vtable">
<dt><a id="index-MM_005fAPPL"></a><span><code class="code">MM_APPL</code><a class="copiable-link" href="#index-MM_005fAPPL"> &para;</a></span></dt>
<dd><p>The erroneous condition is detected by the application.
</p></dd>
<dt><a id="index-MM_005fUTIL"></a><span><code class="code">MM_UTIL</code><a class="copiable-link" href="#index-MM_005fUTIL"> &para;</a></span></dt>
<dd><p>The erroneous condition is detected by a utility.
</p></dd>
<dt><a id="index-MM_005fOPSYS"></a><span><code class="code">MM_OPSYS</code><a class="copiable-link" href="#index-MM_005fOPSYS"> &para;</a></span></dt>
<dd><p>The erroneous condition is detected by the operating system.
</p></dd>
</dl>

<p>A last component of <var class="var">classification</var> can signal the results of this
message.  Exactly one of the following values can be used:
</p>
<dl class="vtable">
<dt><a id="index-MM_005fRECOVER"></a><span><code class="code">MM_RECOVER</code><a class="copiable-link" href="#index-MM_005fRECOVER"> &para;</a></span></dt>
<dd><p>It is a recoverable error.
</p></dd>
<dt><a id="index-MM_005fNRECOV"></a><span><code class="code">MM_NRECOV</code><a class="copiable-link" href="#index-MM_005fNRECOV"> &para;</a></span></dt>
<dd><p>It is a non-recoverable error.
</p></dd>
</dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fmtmsg"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fmtmsg</strong> <code class="def-code-arguments">(long int <var class="var">classification</var>, const char *<var class="var">label</var>, int <var class="var">severity</var>, const char *<var class="var">text</var>, const char *<var class="var">action</var>, const char *<var class="var">tag</var>)</code><a class="copiable-link" href="#index-fmtmsg"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Display a message described by its parameters on the device(s) specified
in the <var class="var">classification</var> parameter.  The <var class="var">label</var> parameter
identifies the source of the message.  The string should consist of two
colon separated parts where the first part has not more than 10 and the
second part not more than 14 characters.  The <var class="var">text</var> parameter
describes the condition of the error, the <var class="var">action</var> parameter possible
steps to recover from the error and the <var class="var">tag</var> parameter is a
reference to the online documentation where more information can be
found.  It should contain the <var class="var">label</var> value and a unique
identification number.
</p>
<p>Each of the parameters can be a special value which means this value
is to be omitted.  The symbolic names for these values are:
</p>
<dl class="vtable">
<dt><a id="index-MM_005fNULLLBL"></a><span><code class="code">MM_NULLLBL</code><a class="copiable-link" href="#index-MM_005fNULLLBL"> &para;</a></span></dt>
<dd><p>Ignore <var class="var">label</var> parameter.
</p></dd>
<dt><a id="index-MM_005fNULLSEV"></a><span><code class="code">MM_NULLSEV</code><a class="copiable-link" href="#index-MM_005fNULLSEV"> &para;</a></span></dt>
<dd><p>Ignore <var class="var">severity</var> parameter.
</p></dd>
<dt><a id="index-MM_005fNULLMC"></a><span><code class="code">MM_NULLMC</code><a class="copiable-link" href="#index-MM_005fNULLMC"> &para;</a></span></dt>
<dd><p>Ignore <var class="var">classification</var> parameter.  This implies that nothing is
actually printed.
</p></dd>
<dt><a id="index-MM_005fNULLTXT"></a><span><code class="code">MM_NULLTXT</code><a class="copiable-link" href="#index-MM_005fNULLTXT"> &para;</a></span></dt>
<dd><p>Ignore <var class="var">text</var> parameter.
</p></dd>
<dt><a id="index-MM_005fNULLACT"></a><span><code class="code">MM_NULLACT</code><a class="copiable-link" href="#index-MM_005fNULLACT"> &para;</a></span></dt>
<dd><p>Ignore <var class="var">action</var> parameter.
</p></dd>
<dt><a id="index-MM_005fNULLTAG"></a><span><code class="code">MM_NULLTAG</code><a class="copiable-link" href="#index-MM_005fNULLTAG"> &para;</a></span></dt>
<dd><p>Ignore <var class="var">tag</var> parameter.
</p></dd>
</dl>

<p>There is another way certain fields can be omitted from the output to
standard error.  This is described below in the description of
environment variables influencing the behavior.
</p>
<p>The <var class="var">severity</var> parameter can have one of the values in the following
table:
<a class="index-entry-id" id="index-severity-class"></a>
</p>
<dl class="vtable">
<dt><a id="index-MM_005fNOSEV"></a><span><code class="code">MM_NOSEV</code><a class="copiable-link" href="#index-MM_005fNOSEV"> &para;</a></span></dt>
<dd><p>Nothing is printed, this value is the same as <code class="code">MM_NULLSEV</code>.
</p></dd>
<dt><a id="index-MM_005fHALT"></a><span><code class="code">MM_HALT</code><a class="copiable-link" href="#index-MM_005fHALT"> &para;</a></span></dt>
<dd><p>This value is printed as <code class="code">HALT</code>.
</p></dd>
<dt><a id="index-MM_005fERROR"></a><span><code class="code">MM_ERROR</code><a class="copiable-link" href="#index-MM_005fERROR"> &para;</a></span></dt>
<dd><p>This value is printed as <code class="code">ERROR</code>.
</p></dd>
<dt><a id="index-MM_005fWARNING"></a><span><code class="code">MM_WARNING</code><a class="copiable-link" href="#index-MM_005fWARNING"> &para;</a></span></dt>
<dd><p>This value is printed as <code class="code">WARNING</code>.
</p></dd>
<dt><a id="index-MM_005fINFO"></a><span><code class="code">MM_INFO</code><a class="copiable-link" href="#index-MM_005fINFO"> &para;</a></span></dt>
<dd><p>This value is printed as <code class="code">INFO</code>.
</p></dd>
</dl>

<p>The numeric value of these five macros are between <code class="code">0</code> and
<code class="code">4</code>.  Using the environment variable <code class="code">SEV_LEVEL</code> or using the
<code class="code">addseverity</code> function one can add more severity levels with their
corresponding string to print.  This is described below
(see <a class="pxref" href="Adding-Severity-Classes.html">Adding Severity Classes</a>).
</p>
<p>If no parameter is ignored the output looks like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted"><var class="var">label</var>: <var class="var">severity-string</var>: <var class="var">text</var>
TO FIX: <var class="var">action</var> <var class="var">tag</var>
</pre></div>

<p>The colons, new line characters and the <code class="code">TO FIX</code> string are
inserted if necessary, i.e., if the corresponding parameter is not
ignored.
</p>
<p>This function is specified in the X/Open Portability Guide.  It is also
available on all systems derived from System V.
</p>
<p>The function returns the value <code class="code">MM_OK</code> if no error occurred.  If
only the printing to standard error failed, it returns <code class="code">MM_NOMSG</code>.
If printing to the console fails, it returns <code class="code">MM_NOCON</code>.  If
nothing is printed <code class="code">MM_NOTOK</code> is returned.  Among situations where
all outputs fail this last value is also returned if a parameter value
is incorrect.
</p></dd></dl>

<p>There are two environment variables which influence the behavior of
<code class="code">fmtmsg</code>.  The first is <code class="code">MSGVERB</code>.  It is used to control the
output actually happening on standard error (<em class="emph">not</em> the console
output).  Each of the five fields can explicitly be enabled.  To do
this the user has to put the <code class="code">MSGVERB</code> variable with a format like
the following in the environment before calling the <code class="code">fmtmsg</code> function
the first time:
</p>
<div class="example smallexample">
<pre class="example-preformatted">MSGVERB=<var class="var">keyword</var>[:<var class="var">keyword</var>[:...]]
</pre></div>

<p>Valid <var class="var">keyword</var>s are <code class="code">label</code>, <code class="code">severity</code>, <code class="code">text</code>,
<code class="code">action</code>, and <code class="code">tag</code>.  If the environment variable is not given
or is the empty string, a not supported keyword is given or the value is
somehow else invalid, no part of the message is masked out.
</p>
<p>The second environment variable which influences the behavior of
<code class="code">fmtmsg</code> is <code class="code">SEV_LEVEL</code>.  This variable and the change in the
behavior of <code class="code">fmtmsg</code> is not specified in the X/Open Portability
Guide.  It is available in System V systems, though.  It can be used to
introduce new severity levels.  By default, only the five severity levels
described above are available.  Any other numeric value would make
<code class="code">fmtmsg</code> print nothing.
</p>
<p>If the user puts <code class="code">SEV_LEVEL</code> with a format like
</p>
<div class="example smallexample">
<pre class="example-preformatted">SEV_LEVEL=[<var class="var">description</var>[:<var class="var">description</var>[:...]]]
</pre></div>

<p>in the environment of the process before the first call to
<code class="code">fmtmsg</code>, where <var class="var">description</var> has a value of the form
</p>
<div class="example smallexample">
<pre class="example-preformatted"><var class="var">severity-keyword</var>,<var class="var">level</var>,<var class="var">printstring</var>
</pre></div>

<p>The <var class="var">severity-keyword</var> part is not used by <code class="code">fmtmsg</code> but it has
to be present.  The <var class="var">level</var> part is a string representation of a
number.  The numeric value must be a number greater than 4.  This value
must be used in the <var class="var">severity</var> parameter of <code class="code">fmtmsg</code> to select
this class.  It is not possible to overwrite any of the predefined
classes.  The <var class="var">printstring</var> is the string printed when a message of
this class is processed by <code class="code">fmtmsg</code> (see above, <code class="code">fmtsmg</code> does
not print the numeric value but instead the string representation).
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Adding-Severity-Classes.html">Adding Severity Classes</a>, Up: <a href="Formatted-Messages.html">Formatted Messages</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
