<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>ISO Random (The GNU C Library)</title>

<meta name="description" content="ISO Random (The GNU C Library)">
<meta name="keywords" content="ISO Random (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Pseudo_002dRandom-Numbers.html" rel="up" title="Pseudo-Random Numbers">
<link href="BSD-Random.html" rel="next" title="BSD Random">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="ISO-Random">
<div class="nav-panel">
<p>
Next: <a href="BSD-Random.html" accesskey="n" rel="next">BSD Random Number Functions</a>, Up: <a href="Pseudo_002dRandom-Numbers.html" accesskey="u" rel="up">Pseudo-Random Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="ISO-C-Random-Number-Functions"><span>19.8.1 ISO C Random Number Functions<a class="copiable-link" href="#ISO-C-Random-Number-Functions"> &para;</a></span></h4>

<p>This section describes the random number functions that are part of
the ISO&nbsp;C<!-- /@w --> standard.
</p>
<p>To use these facilities, you should include the header file
<samp class="file">stdlib.h</samp> in your program.
<a class="index-entry-id" id="index-stdlib_002eh-12"></a>
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-RAND_005fMAX"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">RAND_MAX</strong><a class="copiable-link" href="#index-RAND_005fMAX"> &para;</a></span></dt>
<dd>
<p>The value of this macro is an integer constant representing the largest
value the <code class="code">rand</code> function can return.  In the GNU C Library, it is
<code class="code">2147483647</code>, which is the largest signed integer representable in
32 bits.  In other libraries, it may be as low as <code class="code">32767</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-rand"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">rand</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href="#index-rand"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">rand</code> function returns the next pseudo-random number in the
series.  The value ranges from <code class="code">0</code> to <code class="code">RAND_MAX</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-srand"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">srand</strong> <code class="def-code-arguments">(unsigned int <var class="var">seed</var>)</code><a class="copiable-link" href="#index-srand"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function establishes <var class="var">seed</var> as the seed for a new series of
pseudo-random numbers.  If you call <code class="code">rand</code> before a seed has been
established with <code class="code">srand</code>, it uses the value <code class="code">1</code> as a default
seed.
</p>
<p>To produce a different pseudo-random series each time your program is
run, do <code class="code">srand (time (0))</code>.
</p></dd></dl>

<p>POSIX.1 extended the C standard functions to support reproducible random
numbers in multi-threaded programs.  However, the extension is badly
designed and unsuitable for serious work.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-rand_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">rand_r</strong> <code class="def-code-arguments">(unsigned int *<var class="var">seed</var>)</code><a class="copiable-link" href="#index-rand_005fr"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns a random number in the range 0 to <code class="code">RAND_MAX</code>
just as <code class="code">rand</code> does.  However, all its state is stored in the
<var class="var">seed</var> argument.  This means the RNG&rsquo;s state can only have as many
bits as the type <code class="code">unsigned int</code> has.  This is far too few to
provide a good RNG.
</p>
<p>If your program requires a reentrant RNG, we recommend you use the
reentrant GNU extensions to the SVID random number generator.  The
POSIX.1 interface should only be used when the GNU extensions are not
available.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="BSD-Random.html">BSD Random Number Functions</a>, Up: <a href="Pseudo_002dRandom-Numbers.html">Pseudo-Random Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
