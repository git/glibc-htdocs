<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.40.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Opening Streams (The GNU C Library)</title>

<meta name="description" content="Opening Streams (The GNU C Library)">
<meta name="keywords" content="Opening Streams (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="I_002fO-on-Streams.html" rel="up" title="I/O on Streams">
<link href="Closing-Streams.html" rel="next" title="Closing Streams">
<link href="Standard-Streams.html" rel="prev" title="Standard Streams">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Opening-Streams">
<div class="nav-panel">
<p>
Next: <a href="Closing-Streams.html" accesskey="n" rel="next">Closing Streams</a>, Previous: <a href="Standard-Streams.html" accesskey="p" rel="prev">Standard Streams</a>, Up: <a href="I_002fO-on-Streams.html" accesskey="u" rel="up">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Opening-Streams-1"><span>12.3 Opening Streams<a class="copiable-link" href="#Opening-Streams-1"> &para;</a></span></h3>

<a class="index-entry-id" id="index-opening-a-stream"></a>
<p>Opening a file with the <code class="code">fopen</code> function creates a new stream and
establishes a connection between the stream and a file.  This may
involve creating a new file.
</p>
<a class="index-entry-id" id="index-stdio_002eh-2"></a>
<p>Everything described in this section is declared in the header file
<samp class="file">stdio.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fopen"><span class="category-def">Function: </span><span><code class="def-type">FILE *</code> <strong class="def-name">fopen</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, const char *<var class="var">opentype</var>)</code><a class="copiable-link" href="#index-fopen"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap lock
| AC-Unsafe mem fd lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fopen</code> function opens a stream for I/O to the file
<var class="var">filename</var>, and returns a pointer to the stream.
</p>
<p>The <var class="var">opentype</var> argument is a string that controls how the file is
opened and specifies attributes of the resulting stream.  It must begin
with one of the following sequences of characters:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">r</samp>&rsquo;</dt>
<dd><p>Open an existing file for reading only.
</p>
</dd>
<dt>&lsquo;<samp class="samp">w</samp>&rsquo;</dt>
<dd><p>Open the file for writing only.  If the file already exists, it is
truncated to zero length.  Otherwise a new file is created.
</p>
</dd>
<dt>&lsquo;<samp class="samp">a</samp>&rsquo;</dt>
<dd><p>Open a file for append access; that is, writing at the end of file only.
If the file already exists, its initial contents are unchanged and
output to the stream is appended to the end of the file.
Otherwise, a new, empty file is created.
</p>
</dd>
<dt>&lsquo;<samp class="samp">r+</samp>&rsquo;</dt>
<dd><p>Open an existing file for both reading and writing.  The initial contents
of the file are unchanged and the initial file position is at the
beginning of the file.
</p>
</dd>
<dt>&lsquo;<samp class="samp">w+</samp>&rsquo;</dt>
<dd><p>Open a file for both reading and writing.  If the file already exists, it
is truncated to zero length.  Otherwise, a new file is created.
</p>
</dd>
<dt>&lsquo;<samp class="samp">a+</samp>&rsquo;</dt>
<dd><p>Open or create file for both reading and appending.  If the file exists,
its initial contents are unchanged.  Otherwise, a new file is created.
The initial file position for reading is at the beginning of the file,
but output is always appended to the end of the file.
</p></dd>
</dl>

<p>As you can see, &lsquo;<samp class="samp">+</samp>&rsquo; requests a stream that can do both input and
output.  When using such a stream, you must call <code class="code">fflush</code>
(see <a class="pxref" href="Stream-Buffering.html">Stream Buffering</a>) or a file positioning function such as
<code class="code">fseek</code> (see <a class="pxref" href="File-Positioning.html">File Positioning</a>) when switching from reading
to writing or vice versa.  Otherwise, internal buffers might not be
emptied properly.
</p>
<p>Additional characters may appear after these to specify flags for the
call.  Always put the mode (&lsquo;<samp class="samp">r</samp>&rsquo;, &lsquo;<samp class="samp">w+</samp>&rsquo;, etc.) first; that is
the only part you are guaranteed will be understood by all systems.
</p>
<p>The GNU C Library defines additional characters for use in <var class="var">opentype</var>:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">c</samp>&rsquo;</dt>
<dd><p>The file is opened with cancellation in the I/O functions disabled.
</p>
</dd>
<dt>&lsquo;<samp class="samp">e</samp>&rsquo;</dt>
<dd><p>The underlying file descriptor will be closed if you use any of the
<code class="code">exec&hellip;</code> functions (see <a class="pxref" href="Executing-a-File.html">Executing a File</a>).  (This is
equivalent to having set <code class="code">FD_CLOEXEC</code> on that descriptor.
See <a class="xref" href="Descriptor-Flags.html">File Descriptor Flags</a>.)
</p>
</dd>
<dt>&lsquo;<samp class="samp">m</samp>&rsquo;</dt>
<dd><p>The file is opened and accessed using <code class="code">mmap</code>.  This is only
supported with files opened for reading.
</p>
</dd>
<dt>&lsquo;<samp class="samp">x</samp>&rsquo;</dt>
<dd><p>Insist on creating a new file&mdash;if a file <var class="var">filename</var> already
exists, <code class="code">fopen</code> fails rather than opening it.  If you use
&lsquo;<samp class="samp">x</samp>&rsquo; you are guaranteed that you will not clobber an existing
file.  This is equivalent to the <code class="code">O_EXCL</code> option to the
<code class="code">open</code> function (see <a class="pxref" href="Opening-and-Closing-Files.html">Opening and Closing Files</a>).
</p>
<p>The &lsquo;<samp class="samp">x</samp>&rsquo; modifier is part of ISO&nbsp;C11<!-- /@w -->, which says the file is
created with exclusive access; in the GNU C Library this means the
equivalent of <code class="code">O_EXCL</code>.
</p></dd>
</dl>

<p>The character &lsquo;<samp class="samp">b</samp>&rsquo; in <var class="var">opentype</var> has a standard meaning; it
requests a binary stream rather than a text stream.  But this makes no
difference in POSIX systems (including GNU systems).  If both
&lsquo;<samp class="samp">+</samp>&rsquo; and &lsquo;<samp class="samp">b</samp>&rsquo; are specified, they can appear in either order.
See <a class="xref" href="Binary-Streams.html">Text and Binary Streams</a>.
</p>
<a class="index-entry-id" id="index-stream-orientation"></a>
<a class="index-entry-id" id="index-orientation_002c-stream"></a>
<p>If the <var class="var">opentype</var> string contains the sequence
<code class="code">,ccs=<var class="var">STRING</var></code> then <var class="var">STRING</var> is taken as the name of a
coded character set and <code class="code">fopen</code> will mark the stream as
wide-oriented with appropriate conversion functions in place to convert
from and to the character set <var class="var">STRING</var>.  Any other stream
is opened initially unoriented and the orientation is decided with the
first file operation.  If the first operation is a wide character
operation, the stream is not only marked as wide-oriented, also the
conversion functions to convert to the coded character set used for the
current locale are loaded.  This will not change anymore from this point
on even if the locale selected for the <code class="code">LC_CTYPE</code> category is
changed.
</p>
<p>Any other characters in <var class="var">opentype</var> are simply ignored.  They may be
meaningful in other systems.
</p>
<p>If the open fails, <code class="code">fopen</code> returns a null pointer.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit machine this function is in fact <code class="code">fopen64</code> since the LFS
interface replaces transparently the old interface.
</p></dd></dl>

<p>You can have multiple streams (or file descriptors) pointing to the same
file open at the same time.  If you do only input, this works
straightforwardly, but you must be careful if any output streams are
included.  See <a class="xref" href="Stream_002fDescriptor-Precautions.html">Dangers of Mixing Streams and Descriptors</a>.  This is equally true
whether the streams are in one program (not usual) or in several
programs (which can easily happen).  It may be advantageous to use the
file locking facilities to avoid simultaneous access.  See <a class="xref" href="File-Locks.html">File Locks</a>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fopen64"><span class="category-def">Function: </span><span><code class="def-type">FILE *</code> <strong class="def-name">fopen64</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, const char *<var class="var">opentype</var>)</code><a class="copiable-link" href="#index-fopen64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap lock
| AC-Unsafe mem fd lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">fopen</code> but the stream it returns a
pointer for is opened using <code class="code">open64</code>.  Therefore this stream can be
used even on files larger than 2^31 bytes on 32 bit machines.
</p>
<p>Please note that the return type is still <code class="code">FILE *</code>.  There is no
special <code class="code">FILE</code> type for the LFS interface.
</p>
<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a 32
bits machine this function is available under the name <code class="code">fopen</code>
and so transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-FOPEN_005fMAX"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">FOPEN_MAX</strong><a class="copiable-link" href="#index-FOPEN_005fMAX"> &para;</a></span></dt>
<dd>
<p>The value of this macro is an integer constant expression that
represents the minimum number of streams that the implementation
guarantees can be open simultaneously.  You might be able to open more
than this many streams, but that is not guaranteed.  The value of this
constant is at least eight, which includes the three standard streams
<code class="code">stdin</code>, <code class="code">stdout</code>, and <code class="code">stderr</code>.  In POSIX.1 systems this
value is determined by the <code class="code">OPEN_MAX</code> parameter; see <a class="pxref" href="General-Limits.html">General Capacity Limits</a>.  In BSD and GNU, it is controlled by the <code class="code">RLIMIT_NOFILE</code>
resource limit; see <a class="pxref" href="Limits-on-Resources.html">Limiting Resource Usage</a>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-freopen"><span class="category-def">Function: </span><span><code class="def-type">FILE *</code> <strong class="def-name">freopen</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, const char *<var class="var">opentype</var>, FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-freopen"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe corrupt fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is like a combination of <code class="code">fclose</code> and <code class="code">fopen</code>.
It first closes the stream referred to by <var class="var">stream</var>, ignoring any
errors that are detected in the process.  (Because errors are ignored,
you should not use <code class="code">freopen</code> on an output stream if you have
actually done any output using the stream.)  Then the file named by
<var class="var">filename</var> is opened with mode <var class="var">opentype</var> as for <code class="code">fopen</code>,
and associated with the same stream object <var class="var">stream</var>.
</p>
<p>If the operation fails, a null pointer is returned; otherwise,
<code class="code">freopen</code> returns <var class="var">stream</var>.  On Linux, <code class="code">freopen</code> may also
fail and set <code class="code">errno</code> to <code class="code">EBUSY</code> when the kernel structure for
the old file descriptor was not initialized completely before <code class="code">freopen</code>
was called.  This can only happen in multi-threaded programs, when two
threads race to allocate the same file descriptor number.  To avoid the
possibility of this race, do not use <code class="code">close</code> to close the underlying
file descriptor for a <code class="code">FILE</code>; either use <code class="code">freopen</code> while the
file is still open, or use <code class="code">open</code> and then <code class="code">dup2</code> to install
the new file descriptor.
</p>
<p><code class="code">freopen</code> has traditionally been used to connect a standard stream
such as <code class="code">stdin</code> with a file of your own choice.  This is useful in
programs in which use of a standard stream for certain purposes is
hard-coded.  In the GNU C Library, you can simply close the standard
streams and open new ones with <code class="code">fopen</code>.  But other systems lack
this ability, so using <code class="code">freopen</code> is more portable.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit machine this function is in fact <code class="code">freopen64</code> since the LFS
interface replaces transparently the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-freopen64"><span class="category-def">Function: </span><span><code class="def-type">FILE *</code> <strong class="def-name">freopen64</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, const char *<var class="var">opentype</var>, FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-freopen64"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe corrupt fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">freopen</code>.  The only difference is that
on 32 bit machine the stream returned is able to read beyond the
2^31 bytes limits imposed by the normal interface.  It should be
noted that the stream pointed to by <var class="var">stream</var> need not be opened
using <code class="code">fopen64</code> or <code class="code">freopen64</code> since its mode is not important
for this function.
</p>
<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a 32
bits machine this function is available under the name <code class="code">freopen</code>
and so transparently replaces the old interface.
</p></dd></dl>

<p>In some situations it is useful to know whether a given stream is
available for reading or writing.  This information is normally not
available and would have to be remembered separately.  Solaris
introduced a few functions to get this information from the stream
descriptor and these functions are also available in the GNU C Library.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005ffreadable"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">__freadable</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-_005f_005ffreadable"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">__freadable</code> function determines whether the stream
<var class="var">stream</var> was opened to allow reading.  In this case the return value
is nonzero.  For write-only streams the function returns zero.
</p>
<p>This function is declared in <samp class="file">stdio_ext.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005ffwritable"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">__fwritable</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-_005f_005ffwritable"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">__fwritable</code> function determines whether the stream
<var class="var">stream</var> was opened to allow writing.  In this case the return value
is nonzero.  For read-only streams the function returns zero.
</p>
<p>This function is declared in <samp class="file">stdio_ext.h</samp>.
</p></dd></dl>

<p>For slightly different kinds of problems there are two more functions.
They provide even finer-grained information.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005ffreading"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">__freading</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-_005f_005ffreading"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">__freading</code> function determines whether the stream
<var class="var">stream</var> was last read from or whether it is opened read-only.  In
this case the return value is nonzero, otherwise it is zero.
Determining whether a stream opened for reading and writing was last
used for writing allows to draw conclusions about the content about the
buffer, among other things.
</p>
<p>This function is declared in <samp class="file">stdio_ext.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005f_005ffwriting"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">__fwriting</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href="#index-_005f_005ffwriting"> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">__fwriting</code> function determines whether the stream
<var class="var">stream</var> was last written to or whether it is opened write-only.  In
this case the return value is nonzero, otherwise it is zero.
</p>
<p>This function is declared in <samp class="file">stdio_ext.h</samp>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Closing-Streams.html">Closing Streams</a>, Previous: <a href="Standard-Streams.html">Standard Streams</a>, Up: <a href="I_002fO-on-Streams.html">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
