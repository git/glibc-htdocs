#!/bin/bash -ex
# Public domain.  Originally written by Karl Berry, 2008.
# Automation by Carlos O'Donell, 2014.
# 
# Update libc manual on www.gnu.org, which is kept in the 
# CVS "webpages repository" for libc, https://savannah.gnu.org/cvs/?group=libc.
# More documentation: https://sourceware.org/glibc/wiki/Website%20Maintenance.
# 

###############################################################################
# Helper functions:
###############################################################################

# Replace references of the form:
# href="$1.html#ANCHOR"
# With references of the form entirely defined by $2 with substitution
# support for \1 ($1) and \2 (ANCHOR).
function rewrite_url ()
{
    perl -pi -e "s,href=\"($1)\\.html#(.*?)\",href=\"$2\"," libc.html
    pushd libc
    perl -pi -e "s,href=\"\\.\\./($1)/(.*?)#.*?\",href=\"$2\"," *.html
    popd
}

# Check for prog $1 which must accept $2 and return exit code 0.
function check_prog ()
{
    if $1 $2 >& /dev/null; then
	echo "PASS: Found $1."
    else
	echo "FAIL: Did not find $1."
	exit 1
    fi
}

# Update the variables in index.html like date and time.
# $1 is the version number to use in the update.
function update_index ()
{
    # Edit index.html for the date and copyright year.
    sed -i -e "s/Last updated.*\./Last updated $(date \+%B\ %-d\,\ %Y)/g" index.html
    sed -i -e "s/Copyright.*Free/Copyright $(date \+%Y) Free/g" index.html
}

# Update the version in index.html for a specific manual.
# $1 is the version number to use in the update.
# All instances of VVV are replaced with the version $1.
function update_manual_index ()
{
    sed -i -e "s/VVV/${1}/g" index.html
}

###############################################################################
# Main program:
###############################################################################

# Need to know which version to build.
if [ $# -ne 1 ]; then
    echo "$0: Usage: <glibc version e.g. 2.20>"
    exit 1
fi
ver=$1
# Assume this script is in a current VCS checkout.
vcs_dir=$PWD

# Verify requirements:
# Need makeinfo 7.x. Make the requirement relatively inflexible such that when
# we move to makeinfo 8.x this fails and we review the generated html.
if makeinfo --version | head -1 | grep 'texi2any (GNU texinfo) 7\..*$' >& /dev/null; then
    echo "PASS: Found makeinfo 7.x."
else
    echo "FAIL: Did not find makeinfo 7.x."
    exit 1
fi
check_prog "perl" "--help"
check_prog "wget" "--help"
check_prog "make" "--help"
check_prog "cvs" "-v"
check_prog "tar" "--help"
check_prog "gzip" "--help"
check_prog "sed" "--help"

# Do all of the work in a temporary directory.
tmpdir=$(mktemp -d)
pushd "$tmpdir"
wget "https://ftp.gnu.org/gnu/glibc/glibc-${ver}.tar.xz"
tar xf "glibc-${ver}.tar.xz"
pushd "glibc-${ver}/manual"

# The idea is to use the latest released texinfo.tex,
# available from ftp.gnu.org:gnu/texinfo/texinfo.tex.
mv texinfo.tex texinfo.tex.dist
wget -nv https://ftp.gnu.org/gnu/texinfo/texinfo.tex
# popd glibc-${ver}/manual
popd

rm -rf glibc-build
mkdir glibc-build
pushd glibc-build
# We're not actually going to install anything,
# so this is just to avoid accidents.
$tmpdir/glibc-$ver/configure --prefix=$tmpdir/glibc-install >& $tmpdir/glibc.conf

# Just in case some particular make works best.
make=make

# Must make non-split HTML first since it creates a "libc" file
# which also happens to be the same target directory as the
# non-split build.
"$make" MAKEINFO="makeinfo --no-split" html >& $tmpdir/glibc.makenosplit
mv manual/libc manual/libc.html

# Build the split HTML (without --no-split), and other formats.
for fmt in info html dvi pdf; do
    echo "$tmpdir/glibc.make$fmt"
    $make $fmt >& $tmpdir/glibc.make$fmt </dev/null
    tail -2 $tmpdir/glibc.make$fmt
done

pushd manual
man=`pwd`

#
# At this point we have:
# libc.html (single html version files)
# libc/* (split html version files)
#

# Rewrite flex URL. This is texinfo bug 45612:
# https://savannah.gnu.org/bugs/index.php?45612 
rewrite_url "flex" "https://github.com/westes/flex"

# Make plain text:
makeinfo --no-split -o libc.txt --plaintext -P $tmpdir/glibc-$ver/manual/ \
	$tmpdir/glibc-$ver/manual/libc.texinfo
gzip libc.txt
 
# Make archives for web page:
tar czf libc-texi.tar.gz *.texi* -C $tmpdir/glibc-$ver/manual .
tar czf libc-info.tar.gz libc.info*
tar czf libc-html_node.tar.gz libc
gzip libc.dvi
# popd manual
popd
# popd glibc-build
popd

echo "Using existing checkout directory $vcs_dir"
pushd $vcs_dir
rm -rf "$ver"
mkdir "$ver"
cp template.html "$ver/index.html"
git add "$ver"
pushd "$ver"
update_manual_index "$ver"
git add index.html
rm -rf html_node
mkdir html_node
git add html_node
pushd html_node
cp $man/libc-html_node.tar.gz .
git add libc-html_node.tar.gz
cp $man/libc/* .
git add *.html
# popd html_node
popd
rm -rf dvi
mkdir dvi
git add dvi
cp $man/libc.dvi.gz dvi/
git add dvi/libc.dvi.gz
rm -rf html_mono
mkdir html_mono
git add html_mono
cp $man/libc.html html_mono/
git add html_mono/libc.html
rm -rf info
mkdir info
git add info
cp $man/libc-info.tar.gz info/
git add info/libc-info.tar.gz
rm -rf pdf
mkdir pdf
git add pdf
cp $man/libc.pdf pdf/
git add pdf/libc.pdf
rm -rf texi
mkdir texi
git add texi
cp $man/libc-texi.tar.gz texi/
git add texi/libc-texi.tar.gz
rm -rf text
mkdir text
git add text
cp $man/libc.txt.gz text/
git add text/libc.txt.gz
# popd "$ver"
popd
update_index "$ver"
rm latest
ln -s "$ver" latest
echo "Please commit results using 'git commit -m \"manual: Uploading the glibc-$ver manual.\""
echo "Please check results at http://sourceware.org/glibc/manual."
echo "Please delete $tmpdir/ after you verify the results."
# popd libc/manual
popd
# popd $tmpdir
popd
exit 0
