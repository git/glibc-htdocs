<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.38.

Copyright (C) 1993-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>gmon Tunables (The GNU C Library)</title>

<meta name="description" content="gmon Tunables (The GNU C Library)">
<meta name="keywords" content="gmon Tunables (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Tunables.html" rel="up" title="Tunables">
<link href="Memory-Related-Tunables.html" rel="prev" title="Memory Related Tunables">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="section" id="gmon-Tunables">
<div class="header">
<p>
Previous: <a href="Memory-Related-Tunables.html" accesskey="p" rel="prev">Memory Related Tunables</a>, Up: <a href="Tunables.html" accesskey="u" rel="up">Tunables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="gmon-Tunables-1"></span><h3 class="section">38.8 gmon Tunables</h3>
<span id="index-gmon-tunables"></span>

<dl class="def">
<dt id="index-glibc_002egmon"><span class="category">Tunable namespace: </span><span><strong>glibc.gmon</strong><a href='#index-glibc_002egmon' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>This tunable namespace affects the behaviour of the gmon profiler.
gmon is a component of the GNU C Library which is normally used in
conjunction with gprof.
</p>
<p>When GCC compiles a program with the <code>-pg</code> option, it instruments
the program with calls to the <code>mcount</code> function, to record the
program&rsquo;s call graph. At program startup, a memory buffer is allocated
to store this call graph; the size of the buffer is calculated using a
heuristic based on code size. If during execution, the buffer is found
to be too small, profiling will be aborted and no <samp>gmon.out</samp> file
will be produced. In that case, you will see the following message
printed to standard error:
</p>
<div class="example">
<pre class="example">mcount: call graph buffer size limit exceeded, gmon.out will not be generated
</pre></div>

<p>Most of the symbols discussed in this section are defined in the header
<code>sys/gmon.h</code>. However, some symbols (for example <code>mcount</code>)
are not defined in any header file, since they are only intended to be
called from code generated by the compiler.
</p></dd></dl>

<dl class="def">
<dt id="index-glibc_002emem_002eminarcs"><span class="category">Tunable: </span><span><strong>glibc.mem.minarcs</strong><a href='#index-glibc_002emem_002eminarcs' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>The heuristic for sizing the call graph buffer is known to be
insufficient for small programs; hence, the calculated value is clamped
to be at least a minimum size. The default minimum (in units of
call graph entries, <code>struct tostruct</code>), is given by the macro
<code>MINARCS</code>. If you have some program with an unusually complex
call graph, for which the heuristic fails to allocate enough space,
you can use this tunable to increase the minimum to a larger value.
</p></dd></dl>

<dl class="def">
<dt id="index-glibc_002emem_002emaxarcs"><span class="category">Tunable: </span><span><strong>glibc.mem.maxarcs</strong><a href='#index-glibc_002emem_002emaxarcs' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>To prevent excessive memory consumption when profiling very large
programs, the call graph buffer is allowed to have a maximum of
<code>MAXARCS</code> entries. For some very large programs, the default
value of <code>MAXARCS</code> defined in <samp>sys/gmon.h</samp> is too small; in
that case, you can use this tunable to increase it.
</p>
<p>Note the value of the <code>maxarcs</code> tunable must be greater or equal
to that of the <code>minarcs</code> tunable; if this constraint is violated,
a warning will printed to standard error at program startup, and
the <code>minarcs</code> value will be used as the maximum as well.
</p>
<p>Setting either tunable too high may result in a call graph buffer
whose size exceeds the available memory; in that case, an out of memory
error will be printed at program startup, the profiler will be
disabled, and no <samp>gmon.out</samp> file will be generated.
</p></dd></dl>
</div>
<hr>
<div class="header">
<p>
Previous: <a href="Memory-Related-Tunables.html">Memory Related Tunables</a>, Up: <a href="Tunables.html">Tunables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
