<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.38.

Copyright (C) 1993-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Source Fortification (The GNU C Library)</title>

<meta name="description" content="Source Fortification (The GNU C Library)">
<meta name="keywords" content="Source Fortification (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Maintenance.html" rel="up" title="Maintenance">
<link href="Symbol-handling.html" rel="next" title="Symbol handling">
<link href="Source-Layout.html" rel="prev" title="Source Layout">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="appendixsec" id="Source-Fortification">
<div class="header">
<p>
Next: <a href="Symbol-handling.html" accesskey="n" rel="next">Symbol handling in the GNU C Library</a>, Previous: <a href="Source-Layout.html" accesskey="p" rel="prev">Adding New Functions</a>, Up: <a href="Maintenance.html" accesskey="u" rel="up">Library Maintenance</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Fortification-of-function-calls"></span><h3 class="appendixsec">D.2 Fortification of function calls</h3>

<p>This section contains implementation details of the GNU C Library and may not
remain stable across releases.
</p>
<p>The <code>_FORTIFY_SOURCE</code> macro may be defined by users to control
hardening of calls into some functions in the GNU C Library.  The definition
should be at the top of the source file before any headers are included
or at the pre-processor commandline using the <code>-D</code> switch.  The
hardening primarily focuses on accesses to buffers passed to the
functions but may also include checks for validity of other inputs to
the functions.
</p>
<p>When the <code>_FORTIFY_SOURCE</code> macro is defined, it enables code that
validates inputs passed to some functions in the GNU C Library to determine if
they are safe.  If the compiler is unable to determine that the inputs
to the function call are safe, the call may be replaced by a call to its
hardened variant that does additional safety checks at runtime.  Some
hardened variants need the size of the buffer to perform access
validation and this is provided by the <code>__builtin_object_size</code> or
the <code>__builtin_dynamic_object_size</code> builtin functions.
<code>_FORTIFY_SOURCE</code> also enables additional compile time diagnostics,
such as unchecked return values from some functions, to encourage
developers to add error checking for those functions.
</p>
<p>At runtime, if any of those safety checks fail, the program will
terminate with a <code>SIGABRT</code> signal.  <code>_FORTIFY_SOURCE</code> may be
defined to one of the following values:
</p>
<ul>
<li> <em class='math'>1</em>: This enables buffer bounds checking using the value
returned by the <code>__builtin_object_size</code> compiler builtin function.
If the function returns <code>(size_t) -1</code>, the function call is left
untouched.  Additionally, this level also enables validation of flags to
the <code>open</code>, <code>open64</code>, <code>openat</code> and <code>openat64</code>
functions.

</li><li> <em class='math'>2</em>: This behaves like <em class='math'>1</em>, with the addition of some
checks that may trap code that is conforming but unsafe, e.g. accepting
<code>%n</code> only in read-only format strings.

</li><li> <em class='math'>3</em>: This enables buffer bounds checking using the value
returned by the <code>__builtin_dynamic_object_size</code> compiler builtin
function.  If the function returns <code>(size_t) -1</code>, the function call
is left untouched.  Fortification at this level may have a impact on
program performance if the function call that is fortified is frequently
encountered and the size expression returned by
<code>__builtin_dynamic_object_size</code> is complex.
</li></ul>

<p>In general, the fortified variants of the function calls use the name of
the function with a <code>__</code> prefix and a <code>_chk</code> suffix.  There
are some exceptions, e.g. the <code>printf</code> family of functions where,
depending on the architecture, one may also see fortified variants have
the <code>_chkieee128</code> suffix or the <code>__nldbl___</code> prefix to their
names.
</p>
<p>Another exception is the <code>open</code> family of functions, where their
fortified replacements have the <code>__</code> prefix and a <code>_2</code> suffix.
The <code>FD_SET</code>, <code>FD_CLR</code> and <code>FD_ISSET</code> macros use the
<code>__fdelt_chk</code> function on fortification.
</p>
<p>The following functions and macros are fortified in the GNU C Library:
</p>
<ul>
<li> <code>asprintf</code>

</li><li> <code>confstr</code>

</li><li> <code>dprintf</code>

</li><li> <code>explicit_bzero</code>

</li><li> <code>FD_SET</code>

</li><li> <code>FD_CLR</code>

</li><li> <code>FD_ISSET</code>

</li><li> <code>fgets</code>

</li><li> <code>fgets_unlocked</code>

</li><li> <code>fgetws</code>

</li><li> <code>fgetws_unlocked</code>

</li><li> <code>fprintf</code>

</li><li> <code>fread</code>

</li><li> <code>fread_unlocked</code>

</li><li> <code>fwprintf</code>

</li><li> <code>getcwd</code>

</li><li> <code>getdomainname</code>

</li><li> <code>getgroups</code>

</li><li> <code>gethostname</code>

</li><li> <code>getlogin_r</code>

</li><li> <code>gets</code>

</li><li> <code>getwd</code>

</li><li> <code>longjmp</code>

</li><li> <code>mbsnrtowcs</code>

</li><li> <code>mbsrtowcs</code>

</li><li> <code>mbstowcs</code>

</li><li> <code>memcpy</code>

</li><li> <code>memmove</code>

</li><li> <code>mempcpy</code>

</li><li> <code>memset</code>

</li><li> <code>mq_open</code>

</li><li> <code>obstack_printf</code>

</li><li> <code>obstack_vprintf</code>

</li><li> <code>open</code>

</li><li> <code>open64</code>

</li><li> <code>openat</code>

</li><li> <code>openat64</code>

</li><li> <code>poll</code>

</li><li> <code>ppoll64</code>

</li><li> <code>ppoll</code>

</li><li> <code>pread64</code>

</li><li> <code>pread</code>

</li><li> <code>printf</code>

</li><li> <code>ptsname_r</code>

</li><li> <code>read</code>

</li><li> <code>readlinkat</code>

</li><li> <code>readlink</code>

</li><li> <code>realpath</code>

</li><li> <code>recv</code>

</li><li> <code>recvfrom</code>

</li><li> <code>snprintf</code>

</li><li> <code>sprintf</code>

</li><li> <code>stpcpy</code>

</li><li> <code>stpncpy</code>

</li><li> <code>strcat</code>

</li><li> <code>strcpy</code>

</li><li> <code>strlcat</code>

</li><li> <code>strlcpy</code>

</li><li> <code>strncat</code>

</li><li> <code>strncpy</code>

</li><li> <code>swprintf</code>

</li><li> <code>syslog</code>

</li><li> <code>ttyname_r</code>

</li><li> <code>vasprintf</code>

</li><li> <code>vdprintf</code>

</li><li> <code>vfprintf</code>

</li><li> <code>vfwprintf</code>

</li><li> <code>vprintf</code>

</li><li> <code>vsnprintf</code>

</li><li> <code>vsprintf</code>

</li><li> <code>vswprintf</code>

</li><li> <code>vsyslog</code>

</li><li> <code>vwprintf</code>

</li><li> <code>wcpcpy</code>

</li><li> <code>wcpncpy</code>

</li><li> <code>wcrtomb</code>

</li><li> <code>wcscat</code>

</li><li> <code>wcscpy</code>

</li><li> <code>wcslcat</code>

</li><li> <code>wcslcpy</code>

</li><li> <code>wcsncat</code>

</li><li> <code>wcsncpy</code>

</li><li> <code>wcsnrtombs</code>

</li><li> <code>wcsrtombs</code>

</li><li> <code>wcstombs</code>

</li><li> <code>wctomb</code>

</li><li> <code>wmemcpy</code>

</li><li> <code>wmemmove</code>

</li><li> <code>wmempcpy</code>

</li><li> <code>wmemset</code>

</li><li> <code>wprintf</code>

</li></ul>


</div>
<hr>
<div class="header">
<p>
Next: <a href="Symbol-handling.html">Symbol handling in the GNU C Library</a>, Previous: <a href="Source-Layout.html">Adding New Functions</a>, Up: <a href="Maintenance.html">Library Maintenance</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
