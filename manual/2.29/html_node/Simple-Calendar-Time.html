<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU C Library.

This is
The GNU C Library Reference Manual, for version
2.29.

Copyright (C) 1993-2019 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Simple Calendar Time (The GNU C Library)</title>

<meta name="description" content="Simple Calendar Time (The GNU C Library)">
<meta name="keywords" content="Simple Calendar Time (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Calendar-Time.html" rel="up" title="Calendar Time">
<link href="High_002dResolution-Calendar.html" rel="next" title="High-Resolution Calendar">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="subsection" id="Simple-Calendar-Time">
<div class="header">
<p>
Next: <a href="High_002dResolution-Calendar.html" accesskey="n" rel="next">High-Resolution Calendar</a>, Up: <a href="Calendar-Time.html" accesskey="u" rel="up">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Simple-Calendar-Time-1"></span><h4 class="subsection">21.4.1 Simple Calendar Time</h4>

<p>This section describes the <code>time_t</code> data type for representing calendar
time as simple time, and the functions which operate on simple time objects.
These facilities are declared in the header file <samp>time.h</samp>.
<span id="index-time_002eh-2"></span>
</p>
<span id="index-epoch"></span>
<dl class="def">
<dt id="index-time_005ft"><span class="category">Data Type: </span><span><strong>time_t</strong><a href='#index-time_005ft' class='copiable-anchor'> &para;</a></span></dt>
<dd>
<p>This is the data type used to represent simple time.  Sometimes, it also
represents an elapsed time.  When interpreted as a calendar time value,
it represents the number of seconds elapsed since 00:00:00 on January 1,
1970, Coordinated Universal Time.  (This calendar time is sometimes
referred to as the <em>epoch</em>.)  POSIX requires that this count not
include leap seconds, but on some systems this count includes leap seconds
if you set <code>TZ</code> to certain values (see <a href="TZ-Variable.html">Specifying the Time Zone with <code>TZ</code></a>).
</p>
<p>Note that a simple time has no concept of local time zone.  Calendar
Time <var>T</var> is the same instant in time regardless of where on the
globe the computer is.
</p>
<p>In the GNU C Library, <code>time_t</code> is equivalent to <code>long int</code>.
In other systems, <code>time_t</code> might be either an integer or
floating-point type.
</p></dd></dl>

<p>The function <code>difftime</code> tells you the elapsed time between two
simple calendar times, which is not always as easy to compute as just
subtracting.  See <a href="Elapsed-Time.html">Elapsed Time</a>.
</p>
<dl class="def">
<dt id="index-time-1"><span class="category">Function: </span><span><em>time_t</em> <strong>time</strong> <em>(time_t *<var>result</var>)</em><a href='#index-time-1' class='copiable-anchor'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code>time</code> function returns the current calendar time as a value of
type <code>time_t</code>.  If the argument <var>result</var> is not a null pointer,
the calendar time value is also stored in <code>*<var>result</var></code>.  If the
current calendar time is not available, the value
<code><span class="nolinebreak">(time_t)(-1)</span></code><!-- /@w --> is returned.
</p></dd></dl>

<dl class="def">
<dt id="index-stime"><span class="category">Function: </span><span><em>int</em> <strong>stime</strong> <em>(const time_t *<var>newtime</var>)</em><a href='#index-stime' class='copiable-anchor'> &para;</a></span></dt>
<dd>

<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code>stime</code> sets the system clock, i.e., it tells the system that the
current calendar time is <var>newtime</var>, where <code>newtime</code> is
interpreted as described in the above definition of <code>time_t</code>.
</p>
<p><code>settimeofday</code> is a newer function which sets the system clock to
better than one second precision.  <code>settimeofday</code> is generally a
better choice than <code>stime</code>.  See <a href="High_002dResolution-Calendar.html">High-Resolution Calendar</a>.
</p>
<p>Only the superuser can set the system clock.
</p>
<p>If the function succeeds, the return value is zero.  Otherwise, it is
<code>-1</code> and <code>errno</code> is set accordingly:
</p>
<dl compact="compact">
<dt><span><code>EPERM</code></span></dt>
<dd><p>The process is not superuser.
</p></dd>
</dl>
</dd></dl>



</div>
<hr>
<div class="header">
<p>
Next: <a href="High_002dResolution-Calendar.html">High-Resolution Calendar</a>, Up: <a href="Calendar-Time.html">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
