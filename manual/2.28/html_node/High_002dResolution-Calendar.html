<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU C Library.

This is
The GNU C Library Reference Manual, for version
2.28.

Copyright (C) 1993-2018 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>High-Resolution Calendar (The GNU C Library)</title>

<meta name="description" content="High-Resolution Calendar (The GNU C Library)">
<meta name="keywords" content="High-Resolution Calendar (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Calendar-Time.html" rel="up" title="Calendar Time">
<link href="Broken_002ddown-Time.html" rel="next" title="Broken-down Time">
<link href="Simple-Calendar-Time.html" rel="prev" title="Simple Calendar Time">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="subsection" id="High_002dResolution-Calendar">
<div class="header">
<p>
Next: <a href="Broken_002ddown-Time.html" accesskey="n" rel="next">Broken-down Time</a>, Previous: <a href="Simple-Calendar-Time.html" accesskey="p" rel="prev">Simple Calendar Time</a>, Up: <a href="Calendar-Time.html" accesskey="u" rel="up">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="High_002dResolution-Calendar-1"></span><h4 class="subsection">21.4.2 High-Resolution Calendar</h4>

<p>The <code>time_t</code> data type used to represent simple times has a
resolution of only one second.  Some applications need more precision.
</p>
<p>So, the GNU C Library also contains functions which are capable of
representing calendar times to a higher resolution than one second.  The
functions and the associated data types described in this section are
declared in <samp>sys/time.h</samp>.
<span id="index-sys_002ftime_002eh-1"></span>
</p>
<dl class="def">
<dt id="index-struct-timezone"><span class="category">Data Type: </span><span><strong>struct timezone</strong><a href='#index-struct-timezone' class='copiable-anchor'> &para;</a></span></dt>
<dd>
<p>The <code>struct timezone</code> structure is used to hold minimal information
about the local time zone.  It has the following members:
</p>
<dl compact="compact">
<dt><span><code>int tz_minuteswest</code></span></dt>
<dd><p>This is the number of minutes west of UTC.
</p>
</dd>
<dt><span><code>int tz_dsttime</code></span></dt>
<dd><p>If nonzero, Daylight Saving Time applies during some part of the year.
</p></dd>
</dl>

<p>The <code>struct timezone</code> type is obsolete and should never be used.
Instead, use the facilities described in <a href="Time-Zone-Functions.html">Functions and Variables for Time Zones</a>.
</p></dd></dl>

<dl class="def">
<dt id="index-gettimeofday"><span class="category">Function: </span><span><em>int</em> <strong>gettimeofday</strong> <em>(struct timeval *<var>tp</var>, struct timezone *<var>tzp</var>)</em><a href='#index-gettimeofday' class='copiable-anchor'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code>gettimeofday</code> function returns the current calendar time as
the elapsed time since the epoch in the <code>struct timeval</code> structure
indicated by <var>tp</var>.  (see <a href="Elapsed-Time.html">Elapsed Time</a> for a description of
<code>struct timeval</code>).  Information about the time zone is returned in
the structure pointed to by <var>tzp</var>.  If the <var>tzp</var> argument is a null
pointer, time zone information is ignored.
</p>
<p>The return value is <code>0</code> on success and <code>-1</code> on failure.  The
following <code>errno</code> error condition is defined for this function:
</p>
<dl compact="compact">
<dt><span><code>ENOSYS</code></span></dt>
<dd><p>The operating system does not support getting time zone information, and
<var>tzp</var> is not a null pointer.  GNU systems do not
support using <code>struct&nbsp;timezone</code><!-- /@w --> to represent time zone
information; that is an obsolete feature of 4.3 BSD.
Instead, use the facilities described in <a href="Time-Zone-Functions.html">Functions and Variables for Time Zones</a>.
</p></dd>
</dl>
</dd></dl>

<dl class="def">
<dt id="index-settimeofday"><span class="category">Function: </span><span><em>int</em> <strong>settimeofday</strong> <em>(const struct timeval *<var>tp</var>, const struct timezone *<var>tzp</var>)</em><a href='#index-settimeofday' class='copiable-anchor'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code>settimeofday</code> function sets the current calendar time in the
system clock according to the arguments.  As for <code>gettimeofday</code>,
the calendar time is represented as the elapsed time since the epoch.
As for <code>gettimeofday</code>, time zone information is ignored if
<var>tzp</var> is a null pointer.
</p>
<p>You must be a privileged user in order to use <code>settimeofday</code>.
</p>
<p>Some kernels automatically set the system clock from some source such as
a hardware clock when they start up.  Others, including Linux, place the
system clock in an &ldquo;invalid&rdquo; state (in which attempts to read the clock
fail).  A call of <code>stime</code> removes the system clock from an invalid
state, and system startup scripts typically run a program that calls
<code>stime</code>.
</p>
<p><code>settimeofday</code> causes a sudden jump forwards or backwards, which
can cause a variety of problems in a system.  Use <code>adjtime</code> (below)
to make a smooth transition from one time to another by temporarily
speeding up or slowing down the clock.
</p>
<p>With a Linux kernel, <code>adjtimex</code> does the same thing and can also
make permanent changes to the speed of the system clock so it doesn&rsquo;t
need to be corrected as often.
</p>
<p>The return value is <code>0</code> on success and <code>-1</code> on failure.  The
following <code>errno</code> error conditions are defined for this function:
</p>
<dl compact="compact">
<dt><span><code>EPERM</code></span></dt>
<dd><p>This process cannot set the clock because it is not privileged.
</p>
</dd>
<dt><span><code>ENOSYS</code></span></dt>
<dd><p>The operating system does not support setting time zone information, and
<var>tzp</var> is not a null pointer.
</p></dd>
</dl>
</dd></dl>

<dl class="def">
<dt id="index-adjtime"><span class="category">Function: </span><span><em>int</em> <strong>adjtime</strong> <em>(const struct timeval *<var>delta</var>, struct timeval *<var>olddelta</var>)</em><a href='#index-adjtime' class='copiable-anchor'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function speeds up or slows down the system clock in order to make
a gradual adjustment.  This ensures that the calendar time reported by
the system clock is always monotonically increasing, which might not
happen if you simply set the clock.
</p>
<p>The <var>delta</var> argument specifies a relative adjustment to be made to
the clock time.  If negative, the system clock is slowed down for a
while until it has lost this much elapsed time.  If positive, the system
clock is speeded up for a while.
</p>
<p>If the <var>olddelta</var> argument is not a null pointer, the <code>adjtime</code>
function returns information about any previous time adjustment that
has not yet completed.
</p>
<p>This function is typically used to synchronize the clocks of computers
in a local network.  You must be a privileged user to use it.
</p>
<p>With a Linux kernel, you can use the <code>adjtimex</code> function to
permanently change the clock speed.
</p>
<p>The return value is <code>0</code> on success and <code>-1</code> on failure.  The
following <code>errno</code> error condition is defined for this function:
</p>
<dl compact="compact">
<dt><span><code>EPERM</code></span></dt>
<dd><p>You do not have privilege to set the time.
</p></dd>
</dl>
</dd></dl>

<p><strong>Portability Note:</strong>  The <code>gettimeofday</code>, <code>settimeofday</code>,
and <code>adjtime</code> functions are derived from BSD.
</p>

<p>Symbols for the following function are declared in <samp>sys/timex.h</samp>.
</p>
<dl class="def">
<dt id="index-adjtimex"><span class="category">Function: </span><span><em>int</em> <strong>adjtimex</strong> <em>(struct timex *<var>timex</var>)</em><a href='#index-adjtimex' class='copiable-anchor'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p><code>adjtimex</code> is functionally identical to <code>ntp_adjtime</code>.
See <a href="High-Accuracy-Clock.html">High Accuracy Clock</a>.
</p>
<p>This function is present only with a Linux kernel.
</p>
</dd></dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Broken_002ddown-Time.html">Broken-down Time</a>, Previous: <a href="Simple-Calendar-Time.html">Simple Calendar Time</a>, Up: <a href="Calendar-Time.html">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
