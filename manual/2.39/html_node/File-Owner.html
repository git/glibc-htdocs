<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>File Owner (The GNU C Library)</title>

<meta name="description" content="File Owner (The GNU C Library)">
<meta name="keywords" content="File Owner (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-Attributes.html" rel="up" title="File Attributes">
<link href="Permission-Bits.html" rel="next" title="Permission Bits">
<link href="Testing-File-Type.html" rel="prev" title="Testing File Type">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="File-Owner">
<div class="nav-panel">
<p>
Next: <a href="Permission-Bits.html" accesskey="n" rel="next">The Mode Bits for Access Permission</a>, Previous: <a href="Testing-File-Type.html" accesskey="p" rel="prev">Testing the Type of a File</a>, Up: <a href="File-Attributes.html" accesskey="u" rel="up">File Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="File-Owner-1">14.9.4 File Owner</h4>
<a class="index-entry-id" id="index-file-owner"></a>
<a class="index-entry-id" id="index-owner-of-a-file"></a>
<a class="index-entry-id" id="index-group-owner-of-a-file"></a>

<p>Every file has an <em class="dfn">owner</em> which is one of the registered user names
defined on the system.  Each file also has a <em class="dfn">group</em> which is one of
the defined groups.  The file owner can often be useful for showing you
who edited the file (especially when you edit with GNU Emacs), but its
main purpose is for access control.
</p>
<p>The file owner and group play a role in determining access because the
file has one set of access permission bits for the owner, another set
that applies to users who belong to the file&rsquo;s group, and a third set of
bits that applies to everyone else.  See <a class="xref" href="Access-Permission.html">How Your Access to a File is Decided</a>, for the
details of how access is decided based on this data.
</p>
<p>When a file is created, its owner is set to the effective user ID of the
process that creates it (see <a class="pxref" href="Process-Persona.html">The Persona of a Process</a>).  The file&rsquo;s group ID
may be set to either the effective group ID of the process, or the group
ID of the directory that contains the file, depending on the system
where the file is stored.  When you access a remote file system, it
behaves according to its own rules, not according to the system your
program is running on.  Thus, your program must be prepared to encounter
either kind of behavior no matter what kind of system you run it on.
</p>
<a class="index-entry-id" id="index-chown-1"></a>
<a class="index-entry-id" id="index-chgrp"></a>
<p>You can change the owner and/or group owner of an existing file using
the <code class="code">chown</code> function.  This is the primitive for the <code class="code">chown</code>
and <code class="code">chgrp</code> shell commands.
</p>
<a class="index-entry-id" id="index-unistd_002eh-9"></a>
<p>The prototype for this function is declared in <samp class="file">unistd.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-chown"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">chown</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, uid_t <var class="var">owner</var>, gid_t <var class="var">group</var>)</code><a class="copiable-link" href='#index-chown'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">chown</code> function changes the owner of the file <var class="var">filename</var> to
<var class="var">owner</var>, and its group owner to <var class="var">group</var>.
</p>
<p>Changing the owner of the file on certain systems clears the set-user-ID
and set-group-ID permission bits.  (This is because those bits may not
be appropriate for the new owner.)  Other file permission bits are not
changed.
</p>
<p>The return value is <code class="code">0</code> on success and <code class="code">-1</code> on failure.
In addition to the usual file name errors (see <a class="pxref" href="File-Name-Errors.html">File Name Errors</a>),
the following <code class="code">errno</code> error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EPERM</code></dt>
<dd><p>This process lacks permission to make the requested change.
</p>
<p>Only privileged users or the file&rsquo;s owner can change the file&rsquo;s group.
On most file systems, only privileged users can change the file owner;
some file systems allow you to change the owner if you are currently the
owner.  When you access a remote file system, the behavior you encounter
is determined by the system that actually holds the file, not by the
system your program is running on.
</p>
<p>See <a class="xref" href="Options-for-Files.html">Optional Features in File Support</a>, for information about the
<code class="code">_POSIX_CHOWN_RESTRICTED</code> macro.
</p>
</dd>
<dt><code class="code">EROFS</code></dt>
<dd><p>The file is on a read-only file system.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fchown"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fchown</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, uid_t <var class="var">owner</var>, gid_t <var class="var">group</var>)</code><a class="copiable-link" href='#index-fchown'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is like <code class="code">chown</code>, except that it changes the owner of the open
file with descriptor <var class="var">filedes</var>.
</p>
<p>The return value from <code class="code">fchown</code> is <code class="code">0</code> on success and <code class="code">-1</code>
on failure.  The following <code class="code">errno</code> error codes are defined for this
function:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is not a valid file descriptor.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>The <var class="var">filedes</var> argument corresponds to a pipe or socket, not an ordinary
file.
</p>
</dd>
<dt><code class="code">EPERM</code></dt>
<dd><p>This process lacks permission to make the requested change.  For details
see <code class="code">chmod</code> above.
</p>
</dd>
<dt><code class="code">EROFS</code></dt>
<dd><p>The file resides on a read-only file system.
</p></dd>
</dl>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Permission-Bits.html">The Mode Bits for Access Permission</a>, Previous: <a href="Testing-File-Type.html">Testing the Type of a File</a>, Up: <a href="File-Attributes.html">File Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
