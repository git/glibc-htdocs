<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Symbolic Links (The GNU C Library)</title>

<meta name="description" content="Symbolic Links (The GNU C Library)">
<meta name="keywords" content="Symbolic Links (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-System-Interface.html" rel="up" title="File System Interface">
<link href="Deleting-Files.html" rel="next" title="Deleting Files">
<link href="Hard-Links.html" rel="prev" title="Hard Links">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Symbolic-Links">
<div class="nav-panel">
<p>
Next: <a href="Deleting-Files.html" accesskey="n" rel="next">Deleting Files</a>, Previous: <a href="Hard-Links.html" accesskey="p" rel="prev">Hard Links</a>, Up: <a href="File-System-Interface.html" accesskey="u" rel="up">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Symbolic-Links-1">14.5 Symbolic Links</h3>
<a class="index-entry-id" id="index-soft-link"></a>
<a class="index-entry-id" id="index-link_002c-soft"></a>
<a class="index-entry-id" id="index-symbolic-link"></a>
<a class="index-entry-id" id="index-link_002c-symbolic"></a>

<p>GNU systems support <em class="dfn">soft links</em> or <em class="dfn">symbolic links</em>.  This
is a kind of &ldquo;file&rdquo; that is essentially a pointer to another file
name.  Unlike hard links, symbolic links can be made to directories or
across file systems with no restrictions.  You can also make a symbolic
link to a name which is not the name of any file.  (Opening this link
will fail until a file by that name is created.)  Likewise, if the
symbolic link points to an existing file which is later deleted, the
symbolic link continues to point to the same file name even though the
name no longer names any file.
</p>
<p>The reason symbolic links work the way they do is that special things
happen when you try to open the link.  The <code class="code">open</code> function realizes
you have specified the name of a link, reads the file name contained in
the link, and opens that file name instead.  The <code class="code">stat</code> function
likewise operates on the file that the symbolic link points to, instead
of on the link itself.
</p>
<p>By contrast, other operations such as deleting or renaming the file
operate on the link itself.  The functions <code class="code">readlink</code> and
<code class="code">lstat</code> also refrain from following symbolic links, because their
purpose is to obtain information about the link.  <code class="code">link</code>, the
function that makes a hard link, does too.  It makes a hard link to the
symbolic link, which one rarely wants.
</p>
<p>Some systems have, for some functions operating on files, a limit on
how many symbolic links are followed when resolving a path name.  The
limit if it exists is published in the <samp class="file">sys/param.h</samp> header file.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-MAXSYMLINKS"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">MAXSYMLINKS</strong><a class="copiable-link" href='#index-MAXSYMLINKS'> &para;</a></span></dt>
<dd>

<p>The macro <code class="code">MAXSYMLINKS</code> specifies how many symlinks some function
will follow before returning <code class="code">ELOOP</code>.  Not all functions behave the
same and this value is not the same as that returned for
<code class="code">_SC_SYMLOOP</code> by <code class="code">sysconf</code>.  In fact, the <code class="code">sysconf</code>
result can indicate that there is no fixed limit although
<code class="code">MAXSYMLINKS</code> exists and has a finite value.
</p></dd></dl>

<p>Prototypes for most of the functions listed in this section are in
<samp class="file">unistd.h</samp>.
<a class="index-entry-id" id="index-unistd_002eh-6"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-symlink"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">symlink</strong> <code class="def-code-arguments">(const char *<var class="var">oldname</var>, const char *<var class="var">newname</var>)</code><a class="copiable-link" href='#index-symlink'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">symlink</code> function makes a symbolic link to <var class="var">oldname</var> named
<var class="var">newname</var>.
</p>
<p>The normal return value from <code class="code">symlink</code> is <code class="code">0</code>.  A return value
of <code class="code">-1</code> indicates an error.  In addition to the usual file name
syntax errors (see <a class="pxref" href="File-Name-Errors.html">File Name Errors</a>), the following <code class="code">errno</code>
error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EEXIST</code></dt>
<dd><p>There is already an existing file named <var class="var">newname</var>.
</p>
</dd>
<dt><code class="code">EROFS</code></dt>
<dd><p>The file <var class="var">newname</var> would exist on a read-only file system.
</p>
</dd>
<dt><code class="code">ENOSPC</code></dt>
<dd><p>The directory or file system cannot be extended to make the new link.
</p>
</dd>
<dt><code class="code">EIO</code></dt>
<dd><p>A hardware error occurred while reading or writing data on the disk.
</p>
</dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-readlink"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">readlink</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, char *<var class="var">buffer</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-readlink'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">readlink</code> function gets the value of the symbolic link
<var class="var">filename</var>.  The file name that the link points to is copied into
<var class="var">buffer</var>.  This file name string is <em class="emph">not</em> null-terminated;
<code class="code">readlink</code> normally returns the number of characters copied.  The
<var class="var">size</var> argument specifies the maximum number of characters to copy,
usually the allocation size of <var class="var">buffer</var>.
</p>
<p>If the return value equals <var class="var">size</var>, you cannot tell whether or not
there was room to return the entire name.  So make a bigger buffer and
call <code class="code">readlink</code> again.  Here is an example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">char *
readlink_malloc (const char *filename)
{
  size_t size = 50;
  char *buffer = NULL;

  while (1)
    {
      buffer = xreallocarray (buffer, size, 2);
      size *= 2;
      ssize_t nchars = readlink (filename, buffer, size);
      if (nchars &lt; 0)
        {
          free (buffer);
          return NULL;
        }
      if (nchars &lt; size)
        return buffer;
    }
}
</pre></div>

<p>A value of <code class="code">-1</code> is returned in case of error.  In addition to the
usual file name errors (see <a class="pxref" href="File-Name-Errors.html">File Name Errors</a>), the following
<code class="code">errno</code> error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The named file is not a symbolic link.
</p>
</dd>
<dt><code class="code">EIO</code></dt>
<dd><p>A hardware error occurred while reading or writing data on the disk.
</p></dd>
</dl>
</dd></dl>

<p>In some situations it is desirable to resolve all the
symbolic links to get the real
name of a file where no prefix names a symbolic link which is followed
and no filename in the path is <code class="code">.</code> or <code class="code">..</code>.  This is for
instance desirable if files have to be compared in which case different
names can refer to the same inode.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-canonicalize_005ffile_005fname"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">canonicalize_file_name</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>)</code><a class="copiable-link" href='#index-canonicalize_005ffile_005fname'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">canonicalize_file_name</code> function returns the absolute name of
the file named by <var class="var">name</var> which contains no <code class="code">.</code>, <code class="code">..</code>
components nor any repeated path separators (<code class="code">/</code>) or symlinks.  The
result is passed back as the return value of the function in a block of
memory allocated with <code class="code">malloc</code>.  If the result is not used anymore
the memory should be freed with a call to <code class="code">free</code>.
</p>
<p>If any of the path components are missing the function returns a NULL
pointer.  This is also what is returned if the length of the path
reaches or exceeds <code class="code">PATH_MAX</code> characters.  In any case
<code class="code">errno</code> is set accordingly.
</p>
<dl class="table">
<dt><code class="code">ENAMETOOLONG</code></dt>
<dd><p>The resulting path is too long.  This error only occurs on systems which
have a limit on the file name length.
</p>
</dd>
<dt><code class="code">EACCES</code></dt>
<dd><p>At least one of the path components is not readable.
</p>
</dd>
<dt><code class="code">ENOENT</code></dt>
<dd><p>The input file name is empty.
</p>
</dd>
<dt><code class="code">ENOENT</code></dt>
<dd><p>At least one of the path components does not exist.
</p>
</dd>
<dt><code class="code">ELOOP</code></dt>
<dd><p>More than <code class="code">MAXSYMLINKS</code> many symlinks have been followed.
</p></dd>
</dl>

<p>This function is a GNU extension and is declared in <samp class="file">stdlib.h</samp>.
</p></dd></dl>

<p>The Unix standard includes a similar function which differs from
<code class="code">canonicalize_file_name</code> in that the user has to provide the buffer
where the result is placed in.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-realpath"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">realpath</strong> <code class="def-code-arguments">(const char *restrict <var class="var">name</var>, char *restrict <var class="var">resolved</var>)</code><a class="copiable-link" href='#index-realpath'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>A call to <code class="code">realpath</code> where the <var class="var">resolved</var> parameter is
<code class="code">NULL</code> behaves exactly like <code class="code">canonicalize_file_name</code>.  The
function allocates a buffer for the file name and returns a pointer to
it.  If <var class="var">resolved</var> is not <code class="code">NULL</code> it points to a buffer into
which the result is copied.  It is the callers responsibility to
allocate a buffer which is large enough.  On systems which define
<code class="code">PATH_MAX</code> this means the buffer must be large enough for a
pathname of this size.  For systems without limitations on the pathname
length the requirement cannot be met and programs should not call
<code class="code">realpath</code> with anything but <code class="code">NULL</code> for the second parameter.
</p>
<p>One other difference is that the buffer <var class="var">resolved</var> (if nonzero) will
contain the part of the path component which does not exist or is not
readable if the function returns <code class="code">NULL</code> and <code class="code">errno</code> is set to
<code class="code">EACCES</code> or <code class="code">ENOENT</code>.
</p>
<p>This function is declared in <samp class="file">stdlib.h</samp>.
</p></dd></dl>

<p>The advantage of using this function is that it is more widely
available.  The drawback is that it reports failures for long paths on
systems which have no limits on the file name length.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Deleting-Files.html">Deleting Files</a>, Previous: <a href="Hard-Links.html">Hard Links</a>, Up: <a href="File-System-Interface.html">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
