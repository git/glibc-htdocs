<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>BSD Random (The GNU C Library)</title>

<meta name="description" content="BSD Random (The GNU C Library)">
<meta name="keywords" content="BSD Random (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Pseudo_002dRandom-Numbers.html" rel="up" title="Pseudo-Random Numbers">
<link href="SVID-Random.html" rel="next" title="SVID Random">
<link href="ISO-Random.html" rel="prev" title="ISO Random">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="BSD-Random">
<div class="nav-panel">
<p>
Next: <a href="SVID-Random.html" accesskey="n" rel="next">SVID Random Number Function</a>, Previous: <a href="ISO-Random.html" accesskey="p" rel="prev">ISO C Random Number Functions</a>, Up: <a href="Pseudo_002dRandom-Numbers.html" accesskey="u" rel="up">Pseudo-Random Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="BSD-Random-Number-Functions">19.8.2 BSD Random Number Functions</h4>

<p>This section describes a set of random number generation functions that
are derived from BSD.  There is no advantage to using these functions
with the GNU C Library; we support them for BSD compatibility only.
</p>
<p>The prototypes for these functions are in <samp class="file">stdlib.h</samp>.
<a class="index-entry-id" id="index-stdlib_002eh-13"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-random"><span class="category-def">Function: </span><span><code class="def-type">long int</code> <strong class="def-name">random</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href='#index-random'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the next pseudo-random number in the sequence.
The value returned ranges from <code class="code">0</code> to <code class="code">2147483647</code>.
</p>
<p><strong class="strong">NB:</strong> Temporarily this function was defined to return a
<code class="code">int32_t</code> value to indicate that the return value always contains
32 bits even if <code class="code">long int</code> is wider.  The standard demands it
differently.  Users must always be aware of the 32-bit limitation,
though.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-srandom"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">srandom</strong> <code class="def-code-arguments">(unsigned int <var class="var">seed</var>)</code><a class="copiable-link" href='#index-srandom'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">srandom</code> function sets the state of the random number
generator based on the integer <var class="var">seed</var>.  If you supply a <var class="var">seed</var> value
of <code class="code">1</code>, this will cause <code class="code">random</code> to reproduce the default set
of random numbers.
</p>
<p>To produce a different set of pseudo-random numbers each time your
program runs, do <code class="code">srandom (time (0))</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-initstate"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">initstate</strong> <code class="def-code-arguments">(unsigned int <var class="var">seed</var>, char *<var class="var">state</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-initstate'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">initstate</code> function is used to initialize the random number
generator state.  The argument <var class="var">state</var> is an array of <var class="var">size</var>
bytes, used to hold the state information.  It is initialized based on
<var class="var">seed</var>.  The size must be between 8 and 256 bytes, and should be a
power of two.  The bigger the <var class="var">state</var> array, the better.
</p>
<p>The return value is the previous value of the state information array.
You can use this value later as an argument to <code class="code">setstate</code> to
restore that state.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setstate"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">setstate</strong> <code class="def-code-arguments">(char *<var class="var">state</var>)</code><a class="copiable-link" href='#index-setstate'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">setstate</code> function restores the random number state
information <var class="var">state</var>.  The argument must have been the result of
a previous call to <var class="var">initstate</var> or <var class="var">setstate</var>.
</p>
<p>The return value is the previous value of the state information array.
You can use this value later as an argument to <code class="code">setstate</code> to
restore that state.
</p>
<p>If the function fails the return value is <code class="code">NULL</code>.
</p></dd></dl>

<p>The four functions described so far in this section all work on a state
which is shared by all threads.  The state is not directly accessible to
the user and can only be modified by these functions.  This makes it
hard to deal with situations where each thread should have its own
pseudo-random number generator.
</p>
<p>The GNU C Library contains four additional functions which contain the
state as an explicit parameter and therefore make it possible to handle
thread-local PRNGs.  Besides this there is no difference.  In fact, the
four functions already discussed are implemented internally using the
following interfaces.
</p>
<p>The <samp class="file">stdlib.h</samp> header contains a definition of the following type:
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-random_005fdata"><span class="category-def">Data Type: </span><span><strong class="def-name">struct random_data</strong><a class="copiable-link" href='#index-struct-random_005fdata'> &para;</a></span></dt>
<dd>

<p>Objects of type <code class="code">struct random_data</code> contain the information
necessary to represent the state of the PRNG.  Although a complete
definition of the type is present the type should be treated as opaque.
</p></dd></dl>

<p>The functions modifying the state follow exactly the already described
functions.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-random_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">random_r</strong> <code class="def-code-arguments">(struct random_data *restrict <var class="var">buf</var>, int32_t *restrict <var class="var">result</var>)</code><a class="copiable-link" href='#index-random_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:buf
| AS-Safe 
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">random_r</code> function behaves exactly like the <code class="code">random</code>
function except that it uses and modifies the state in the object
pointed to by the first parameter instead of the global state.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-srandom_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">srandom_r</strong> <code class="def-code-arguments">(unsigned int <var class="var">seed</var>, struct random_data *<var class="var">buf</var>)</code><a class="copiable-link" href='#index-srandom_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:buf
| AS-Safe 
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">srandom_r</code> function behaves exactly like the <code class="code">srandom</code>
function except that it uses and modifies the state in the object
pointed to by the second parameter instead of the global state.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-initstate_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">initstate_r</strong> <code class="def-code-arguments">(unsigned int <var class="var">seed</var>, char *restrict <var class="var">statebuf</var>, size_t <var class="var">statelen</var>, struct random_data *restrict <var class="var">buf</var>)</code><a class="copiable-link" href='#index-initstate_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:buf
| AS-Safe 
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">initstate_r</code> function behaves exactly like the <code class="code">initstate</code>
function except that it uses and modifies the state in the object
pointed to by the fourth parameter instead of the global state.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-setstate_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">setstate_r</strong> <code class="def-code-arguments">(char *restrict <var class="var">statebuf</var>, struct random_data *restrict <var class="var">buf</var>)</code><a class="copiable-link" href='#index-setstate_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:buf
| AS-Safe 
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">setstate_r</code> function behaves exactly like the <code class="code">setstate</code>
function except that it uses and modifies the state in the object
pointed to by the first parameter instead of the global state.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="SVID-Random.html">SVID Random Number Function</a>, Previous: <a href="ISO-Random.html">ISO C Random Number Functions</a>, Up: <a href="Pseudo_002dRandom-Numbers.html">Pseudo-Random Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
