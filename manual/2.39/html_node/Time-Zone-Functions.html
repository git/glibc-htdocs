<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Time Zone Functions (The GNU C Library)</title>

<meta name="description" content="Time Zone Functions (The GNU C Library)">
<meta name="keywords" content="Time Zone Functions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Calendar-Time.html" rel="up" title="Calendar Time">
<link href="Time-Functions-Example.html" rel="next" title="Time Functions Example">
<link href="TZ-Variable.html" rel="prev" title="TZ Variable">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Time-Zone-Functions">
<div class="nav-panel">
<p>
Next: <a href="Time-Functions-Example.html" accesskey="n" rel="next">Time Functions Example</a>, Previous: <a href="TZ-Variable.html" accesskey="p" rel="prev">Specifying the Time Zone with <code class="code">TZ</code></a>, Up: <a href="Calendar-Time.html" accesskey="u" rel="up">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Functions-and-Variables-for-Time-Zones">22.5.7 Functions and Variables for Time Zones</h4>

<dl class="first-deftypevr first-deftypevar-alias-first-deftypevr">
<dt class="deftypevr deftypevar-alias-deftypevr" id="index-tzname"><span class="category-def">Variable: </span><span><code class="def-type">char *</code> <strong class="def-name">tzname</strong> <code class="def-code-arguments">[2]</code><a class="copiable-link" href='#index-tzname'> &para;</a></span></dt>
<dd>
<p>The array <code class="code">tzname</code> contains two strings, which are the standard
abbreviations of the pair of time zones (standard and Daylight
Saving) that the user has selected.  <code class="code">tzname[0]</code> abbreviates
the standard time zone (for example, <code class="code">&quot;EST&quot;</code>), and <code class="code">tzname[1]</code>
abbreviates the time zone when Daylight Saving Time is in use (for
example, <code class="code">&quot;EDT&quot;</code>).  These correspond to the <var class="var">std</var> and <var class="var">dst</var>
strings (respectively) from the <code class="code">TZ</code> environment variable.  If
Daylight Saving Time is never used, <code class="code">tzname[1]</code> is the empty string.
</p>
<p>The <code class="code">tzname</code> array is initialized from the <code class="code">TZ</code> environment
variable whenever <code class="code">tzset</code>, <code class="code">ctime</code>, <code class="code">strftime</code>,
<code class="code">mktime</code>, or <code class="code">localtime</code> is called.  If multiple abbreviations
have been used (e.g. <code class="code">&quot;EWT&quot;</code> and <code class="code">&quot;EDT&quot;</code> for U.S. Eastern War
Time and Eastern Daylight Time), the array contains the most recent
abbreviation.
</p>
<p>The <code class="code">tzname</code> array is required for POSIX.1 compatibility, but in
GNU programs it is better to use the <code class="code">tm_zone</code> member of the
broken-down time structure, since <code class="code">tm_zone</code> reports the correct
abbreviation even when it is not the latest one.
</p>
<p>Though the strings are declared as <code class="code">char *</code> the user must refrain
from modifying these strings.  Modifying the strings will almost certainly
lead to trouble.
</p>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tzset"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">tzset</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href='#index-tzset'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env locale
| AS-Unsafe heap lock
| AC-Unsafe lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">tzset</code> function initializes the <code class="code">tzname</code> variable from
the value of the <code class="code">TZ</code> environment variable.  It is not usually
necessary for your program to call this function, because it is called
automatically when you use the other time conversion functions that
depend on the time zone.
</p></dd></dl>

<p>The following variables are defined for compatibility with System V
Unix.  Like <code class="code">tzname</code>, these variables are set by calling
<code class="code">tzset</code> or the other time conversion functions.
</p>
<dl class="first-deftypevr first-deftypevar-alias-first-deftypevr">
<dt class="deftypevr deftypevar-alias-deftypevr" id="index-timezone"><span class="category-def">Variable: </span><span><code class="def-type">long int</code> <strong class="def-name">timezone</strong><a class="copiable-link" href='#index-timezone'> &para;</a></span></dt>
<dd>
<p>This contains the difference between UTC and the latest local standard
time, in seconds west of UTC.  For example, in the U.S. Eastern time
zone, the value is <code class="code">5*60*60</code>.  Unlike the <code class="code">tm_gmtoff</code> member
of the broken-down time structure, this value is not adjusted for
daylight saving, and its sign is reversed.  In GNU programs it is better
to use <code class="code">tm_gmtoff</code>, since it contains the correct offset even when
it is not the latest one.
</p></dd></dl>

<dl class="first-deftypevr first-deftypevar-alias-first-deftypevr">
<dt class="deftypevr deftypevar-alias-deftypevr" id="index-daylight"><span class="category-def">Variable: </span><span><code class="def-type">int</code> <strong class="def-name">daylight</strong><a class="copiable-link" href='#index-daylight'> &para;</a></span></dt>
<dd>
<p>This variable has a nonzero value if Daylight Saving Time rules apply.
A nonzero value does not necessarily mean that Daylight Saving Time is
now in effect; it means only that Daylight Saving Time is sometimes in
effect.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Time-Functions-Example.html">Time Functions Example</a>, Previous: <a href="TZ-Variable.html">Specifying the Time Zone with <code class="code">TZ</code></a>, Up: <a href="Calendar-Time.html">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
