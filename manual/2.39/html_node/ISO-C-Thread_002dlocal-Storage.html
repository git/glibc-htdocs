<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>ISO C Thread-local Storage (The GNU C Library)</title>

<meta name="description" content="ISO C Thread-local Storage (The GNU C Library)">
<meta name="keywords" content="ISO C Thread-local Storage (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="ISO-C-Threads.html" rel="up" title="ISO C Threads">
<link href="ISO-C-Condition-Variables.html" rel="prev" title="ISO C Condition Variables">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="ISO-C-Thread_002dlocal-Storage">
<div class="nav-panel">
<p>
Previous: <a href="ISO-C-Condition-Variables.html" accesskey="p" rel="prev">Condition Variables</a>, Up: <a href="ISO-C-Threads.html" accesskey="u" rel="up">ISO C Threads</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Thread_002dlocal-Storage">36.1.6 Thread-local Storage</h4>
<a class="index-entry-id" id="index-thread_002dlocal-storage"></a>

<p>The GNU C Library implements functions to provide <em class="dfn">thread-local
storage</em>, a mechanism by which variables can be defined to have unique
per-thread storage, lifetimes that match the thread lifetime, and
destructors that cleanup the unique per-thread storage.
</p>
<p>Several data types and macros exist for working with thread-local
storage:
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-tss_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">tss_t</strong><a class="copiable-link" href='#index-tss_005ft'> &para;</a></span></dt>
<dd>
<p>The <code class="code">tss_t</code> data type identifies a thread-specific storage
object.  Even if shared, every thread will have its own instance of
the variable, with different values.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-tss_005fdtor_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">tss_dtor_t</strong><a class="copiable-link" href='#index-tss_005fdtor_005ft'> &para;</a></span></dt>
<dd>
<p>The <code class="code">tss_dtor_t</code> is a function pointer of type <code class="code">void (*)
(void *)</code>, to be used as a thread-specific storage destructor.  The
function will be called when the current thread calls <code class="code">thrd_exit</code>
(but never when calling <code class="code">tss_delete</code> or <code class="code">exit</code>).
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-thread_005flocal"><span class="category-def">Macro: </span><span><strong class="def-name">thread_local</strong><a class="copiable-link" href='#index-thread_005flocal'> &para;</a></span></dt>
<dd>
<p><code class="code">thread_local</code> is used to mark a variable with thread storage
duration, which means it is created when the thread starts and cleaned
up when the thread ends.
</p>
<p><em class="emph">Note:</em> For C++, C++11 or later is required to use the
<code class="code">thread_local</code> keyword.
</p></dd></dl>

<dl class="first-defvr">
<dt class="defvr" id="index-TSS_005fDTOR_005fITERATIONS"><span class="category-def">Macro: </span><span><strong class="def-name">TSS_DTOR_ITERATIONS</strong><a class="copiable-link" href='#index-TSS_005fDTOR_005fITERATIONS'> &para;</a></span></dt>
<dd>
<p><code class="code">TSS_DTOR_ITERATIONS</code> is an integer constant expression
representing the maximum number of iterations over all thread-local
destructors at the time of thread termination.  This value provides a
bounded limit to the destruction of thread-local storage; e.g.,
consider a destructor that creates more thread-local storage.
</p></dd></dl>

<p>The following functions are used to manage thread-local storage:
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tss_005fcreate"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">tss_create</strong> <code class="def-code-arguments">(tss_t *<var class="var">tss_key</var>, tss_dtor_t <var class="var">destructor</var>)</code><a class="copiable-link" href='#index-tss_005fcreate'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">tss_create</code> creates a new thread-specific storage key and stores
it in the object pointed to by <var class="var">tss_key</var>.  Although the same key
value may be used by different threads, the values bound to the key by
<code class="code">tss_set</code> are maintained on a per-thread basis and persist for
the life of the calling thread.
</p>
<p>If <code class="code">destructor</code> is not NULL, a destructor function will be set,
and called when the thread finishes its execution by calling
<code class="code">thrd_exit</code>.
</p>
<p>This function returns <code class="code">thrd_success</code> if <code class="code">tss_key</code> is
successfully set to a unique value for the thread; otherwise,
<code class="code">thrd_error</code> is returned and the value of <code class="code">tss_key</code> is
undefined.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tss_005fset"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">tss_set</strong> <code class="def-code-arguments">(tss_t <var class="var">tss_key</var>, void *<var class="var">val</var>)</code><a class="copiable-link" href='#index-tss_005fset'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">tss_set</code> sets the value of the thread-specific storage
identified by <var class="var">tss_key</var> for the current thread to <var class="var">val</var>.
Different threads may set different values to the same key.
</p>
<p>This function returns either <code class="code">thrd_success</code> or <code class="code">thrd_error</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tss_005fget"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">tss_get</strong> <code class="def-code-arguments">(tss_t <var class="var">tss_key</var>)</code><a class="copiable-link" href='#index-tss_005fget'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">tss_get</code> returns the value identified by <var class="var">tss_key</var> held in
thread-specific storage for the current thread.  Different threads may
get different values identified by the same key.  On failure,
<code class="code">tss_get</code> returns zero.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tss_005fdelete"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">tss_delete</strong> <code class="def-code-arguments">(tss_t <var class="var">tss_key</var>)</code><a class="copiable-link" href='#index-tss_005fdelete'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">tss_delete</code> destroys the thread-specific storage identified by
<var class="var">tss_key</var>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="ISO-C-Condition-Variables.html">Condition Variables</a>, Up: <a href="ISO-C-Threads.html">ISO C Threads</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
