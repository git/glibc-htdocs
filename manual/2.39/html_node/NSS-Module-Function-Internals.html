<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>NSS Module Function Internals (The GNU C Library)</title>

<meta name="description" content="NSS Module Function Internals (The GNU C Library)">
<meta name="keywords" content="NSS Module Function Internals (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Extending-NSS.html" rel="up" title="Extending NSS">
<link href="Adding-another-Service-to-NSS.html" rel="prev" title="Adding another Service to NSS">


</head>

<body lang="en">
<div class="subsection-level-extent" id="NSS-Module-Function-Internals">
<div class="nav-panel">
<p>
Previous: <a href="Adding-another-Service-to-NSS.html" accesskey="p" rel="prev">Adding another Service to NSS</a>, Up: <a href="Extending-NSS.html" accesskey="u" rel="up">Extending NSS</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Internals-of-the-NSS-Module-Functions">30.4.2 Internals of the NSS Module Functions</h4>

<p>Until now we only provided the syntactic interface for the functions in
the NSS module.  In fact there is not much more we can say since the
implementation obviously is different for each function.  But a few
general rules must be followed by all functions.
</p>
<p>In fact there are four kinds of different functions which may appear in
the interface.  All derive from the traditional ones for system databases.
<var class="var">db</var> in the following table is normally an abbreviation for the
database (e.g., it is <code class="code">pw</code> for the user database).
</p>
<dl class="table">
<dt><code class="code">enum nss_status _nss_<var class="var">database</var>_set<var class="var">db</var>ent (void)</code></dt>
<dd><p>This function prepares the service for following operations.  For a
simple file based lookup this means files could be opened, for other
services this function simply is a noop.
</p>
<p>One special case for this function is that it takes an additional
argument for some <var class="var">database</var>s (i.e., the interface is
<code class="code">int set<var class="var">db</var>ent (int)</code>).  <a class="ref" href="Host-Names.html">Host Names</a>, which describes the
<code class="code">sethostent</code> function.
</p>
<p>The return value should be <var class="var">NSS_STATUS_SUCCESS</var> or according to the
table above in case of an error (see <a class="pxref" href="NSS-Modules-Interface.html">The Interface of the Function in NSS Modules</a>).
</p>
</dd>
<dt><code class="code">enum nss_status _nss_<var class="var">database</var>_end<var class="var">db</var>ent (void)</code></dt>
<dd><p>This function simply closes all files which are still open or removes
buffer caches.  If there are no files or buffers to remove this is again
a simple noop.
</p>
<p>There normally is no return value other than <var class="var">NSS_STATUS_SUCCESS</var>.
</p>
</dd>
<dt><code class="code">enum nss_status _nss_<var class="var">database</var>_get<var class="var">db</var>ent_r (<var class="var">STRUCTURE</var> *result, char *buffer, size_t buflen, int *errnop)</code></dt>
<dd><p>Since this function will be called several times in a row to retrieve
one entry after the other it must keep some kind of state.  But this
also means the functions are not really reentrant.  They are reentrant
only in that simultaneous calls to this function will not try to
write the retrieved data in the same place (as it would be the case for
the non-reentrant functions); instead, it writes to the structure
pointed to by the <var class="var">result</var> parameter.  But the calls share a common
state and in the case of a file access this means they return neighboring
entries in the file.
</p>
<p>The buffer of length <var class="var">buflen</var> pointed to by <var class="var">buffer</var> can be used
for storing some additional data for the result.  It is <em class="emph">not</em>
guaranteed that the same buffer will be passed for the next call of this
function.  Therefore one must not misuse this buffer to save some state
information from one call to another.
</p>
<p>Before the function returns with a failure code, the implementation
should store the value of the local <code class="code">errno</code> variable in the variable
pointed to be <var class="var">errnop</var>.  This is important to guarantee the module
working in statically linked programs.  The stored value must not be
zero.
</p>
<p>As explained above this function could also have an additional last
argument.  This depends on the database used; it happens only for
<code class="code">host</code> and <code class="code">networks</code>.
</p>
<p>The function shall return <code class="code">NSS_STATUS_SUCCESS</code> as long as there are
more entries.  When the last entry was read it should return
<code class="code">NSS_STATUS_NOTFOUND</code>.  When the buffer given as an argument is too
small for the data to be returned <code class="code">NSS_STATUS_TRYAGAIN</code> should be
returned.  When the service was not formerly initialized by a call to
<code class="code">_nss_<var class="var">DATABASE</var>_set<var class="var">db</var>ent</code> all return values allowed for
this function can also be returned here.
</p>
</dd>
<dt><code class="code">enum nss_status _nss_<var class="var">DATABASE</var>_get<var class="var">db</var>by<var class="var">XX</var>_r (<var class="var">PARAMS</var>, <var class="var">STRUCTURE</var> *result, char *buffer, size_t buflen, int *errnop)</code></dt>
<dd><p>This function shall return the entry from the database which is
addressed by the <var class="var">PARAMS</var>.  The type and number of these arguments
vary.  It must be individually determined by looking to the user-level
interface functions.  All arguments given to the non-reentrant version
are here described by <var class="var">PARAMS</var>.
</p>
<p>The result must be stored in the structure pointed to by <var class="var">result</var>.
If there are additional data to return (say strings, where the
<var class="var">result</var> structure only contains pointers) the function must use the
<var class="var">buffer</var> of length <var class="var">buflen</var>.  There must not be any references
to non-constant global data.
</p>
<p>The implementation of this function should honor the <var class="var">stayopen</var>
flag set by the <code class="code">set<var class="var">DB</var>ent</code> function whenever this makes sense.
</p>
<p>Before the function returns, the implementation should store the value of
the local <code class="code">errno</code> variable in the variable pointed to by
<var class="var">errnop</var>.  This is important to guarantee the module works in
statically linked programs.
</p>
<p>Again, this function takes an additional last argument for the
<code class="code">host</code> and <code class="code">networks</code> database.
</p>
<p>The return value should as always follow the rules given above
(see <a class="pxref" href="NSS-Modules-Interface.html">The Interface of the Function in NSS Modules</a>).
</p>
</dd>
</dl>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Adding-another-Service-to-NSS.html">Adding another Service to NSS</a>, Up: <a href="Extending-NSS.html">Extending NSS</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
