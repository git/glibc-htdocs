<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Input Conversion Syntax (The GNU C Library)</title>

<meta name="description" content="Input Conversion Syntax (The GNU C Library)">
<meta name="keywords" content="Input Conversion Syntax (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Input.html" rel="up" title="Formatted Input">
<link href="Table-of-Input-Conversions.html" rel="next" title="Table of Input Conversions">
<link href="Formatted-Input-Basics.html" rel="prev" title="Formatted Input Basics">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Input-Conversion-Syntax">
<div class="nav-panel">
<p>
Next: <a href="Table-of-Input-Conversions.html" accesskey="n" rel="next">Table of Input Conversions</a>, Previous: <a href="Formatted-Input-Basics.html" accesskey="p" rel="prev">Formatted Input Basics</a>, Up: <a href="Formatted-Input.html" accesskey="u" rel="up">Formatted Input</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Input-Conversion-Syntax-1">12.14.2 Input Conversion Syntax</h4>

<p>A <code class="code">scanf</code> template string is a string that contains ordinary
multibyte characters interspersed with conversion specifications that
start with &lsquo;<samp class="samp">%</samp>&rsquo;.
</p>
<p>Any whitespace character (as defined by the <code class="code">isspace</code> function;
see <a class="pxref" href="Classification-of-Characters.html">Classification of Characters</a>) in the template causes any number
of whitespace characters in the input stream to be read and discarded.
The whitespace characters that are matched need not be exactly the same
whitespace characters that appear in the template string.  For example,
write &lsquo;<samp class="samp"> , </samp>&rsquo; in the template to recognize a comma with optional
whitespace before and after.
</p>
<p>Other characters in the template string that are not part of conversion
specifications must match characters in the input stream exactly; if
this is not the case, a matching failure occurs.
</p>
<p>The conversion specifications in a <code class="code">scanf</code> template string
have the general form:
</p>
<div class="example smallexample">
<pre class="example-preformatted">% <var class="var">flags</var> <var class="var">width</var> <var class="var">type</var> <var class="var">conversion</var>
</pre></div>

<p>In more detail, an input conversion specification consists of an initial
&lsquo;<samp class="samp">%</samp>&rsquo; character followed in sequence by:
</p>
<ul class="itemize mark-bullet">
<li>An optional <em class="dfn">flag character</em> &lsquo;<samp class="samp">*</samp>&rsquo;, which says to ignore the text
read for this specification.  When <code class="code">scanf</code> finds a conversion
specification that uses this flag, it reads input as directed by the
rest of the conversion specification, but it discards this input, does
not use a pointer argument, and does not increment the count of
successful assignments.
<a class="index-entry-id" id="index-flag-character-_0028scanf_0029"></a>

</li><li>An optional flag character &lsquo;<samp class="samp">a</samp>&rsquo; (valid with string conversions only)
which requests allocation of a buffer long enough to store the string in.
(This is a GNU extension.)
See <a class="xref" href="Dynamic-String-Input.html">Dynamically Allocating String Conversions</a>.

</li><li>An optional decimal integer that specifies the <em class="dfn">maximum field
width</em>.  Reading of characters from the input stream stops either when
this maximum is reached or when a non-matching character is found,
whichever happens first.  Most conversions discard initial whitespace
characters (those that don&rsquo;t are explicitly documented), and these
discarded characters don&rsquo;t count towards the maximum field width.
String input conversions store a null character to mark the end of the
input; the maximum field width does not include this terminator.
<a class="index-entry-id" id="index-maximum-field-width-_0028scanf_0029"></a>

</li><li>An optional <em class="dfn">type modifier character</em>.  For example, you can
specify a type modifier of &lsquo;<samp class="samp">l</samp>&rsquo; with integer conversions such as
&lsquo;<samp class="samp">%d</samp>&rsquo; to specify that the argument is a pointer to a <code class="code">long int</code>
rather than a pointer to an <code class="code">int</code>.
<a class="index-entry-id" id="index-type-modifier-character-_0028scanf_0029"></a>

</li><li>A character that specifies the conversion to be applied.
</li></ul>

<p>The exact options that are permitted and how they are interpreted vary
between the different conversion specifiers.  See the descriptions of the
individual conversions for information about the particular options that
they allow.
</p>
<p>With the &lsquo;<samp class="samp">-Wformat</samp>&rsquo; option, the GNU C compiler checks calls to
<code class="code">scanf</code> and related functions.  It examines the format string and
verifies that the correct number and types of arguments are supplied.
There is also a GNU C syntax to tell the compiler that a function you
write uses a <code class="code">scanf</code>-style format string.
See <a data-manual="gcc" href="https://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html#Function-Attributes">Declaring Attributes of Functions</a> in <cite class="cite">Using GNU CC</cite>, for more information.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Table-of-Input-Conversions.html">Table of Input Conversions</a>, Previous: <a href="Formatted-Input-Basics.html">Formatted Input Basics</a>, Up: <a href="Formatted-Input.html">Formatted Input</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
