<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Deleting Files (The GNU C Library)</title>

<meta name="description" content="Deleting Files (The GNU C Library)">
<meta name="keywords" content="Deleting Files (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-System-Interface.html" rel="up" title="File System Interface">
<link href="Renaming-Files.html" rel="next" title="Renaming Files">
<link href="Symbolic-Links.html" rel="prev" title="Symbolic Links">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Deleting-Files">
<div class="nav-panel">
<p>
Next: <a href="Renaming-Files.html" accesskey="n" rel="next">Renaming Files</a>, Previous: <a href="Symbolic-Links.html" accesskey="p" rel="prev">Symbolic Links</a>, Up: <a href="File-System-Interface.html" accesskey="u" rel="up">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Deleting-Files-1">14.6 Deleting Files</h3>
<a class="index-entry-id" id="index-deleting-a-file"></a>
<a class="index-entry-id" id="index-removing-a-file"></a>
<a class="index-entry-id" id="index-unlinking-a-file"></a>

<p>You can delete a file with <code class="code">unlink</code> or <code class="code">remove</code>.
</p>
<p>Deletion actually deletes a file name.  If this is the file&rsquo;s only name,
then the file is deleted as well.  If the file has other remaining names
(see <a class="pxref" href="Hard-Links.html">Hard Links</a>), it remains accessible under those names.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-unlink"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">unlink</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>)</code><a class="copiable-link" href='#index-unlink'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">unlink</code> function deletes the file name <var class="var">filename</var>.  If
this is a file&rsquo;s sole name, the file itself is also deleted.  (Actually,
if any process has the file open when this happens, deletion is
postponed until all processes have closed the file.)
</p>
<a class="index-entry-id" id="index-unistd_002eh-7"></a>
<p>The function <code class="code">unlink</code> is declared in the header file <samp class="file">unistd.h</samp>.
</p>
<p>This function returns <code class="code">0</code> on successful completion, and <code class="code">-1</code>
on error.  In addition to the usual file name errors
(see <a class="pxref" href="File-Name-Errors.html">File Name Errors</a>), the following <code class="code">errno</code> error conditions are
defined for this function:
</p>
<dl class="table">
<dt><code class="code">EACCES</code></dt>
<dd><p>Write permission is denied for the directory from which the file is to be
removed, or the directory has the sticky bit set and you do not own the file.
</p>
</dd>
<dt><code class="code">EBUSY</code></dt>
<dd><p>This error indicates that the file is being used by the system in such a
way that it can&rsquo;t be unlinked.  For example, you might see this error if
the file name specifies the root directory or a mount point for a file
system.
</p>
</dd>
<dt><code class="code">ENOENT</code></dt>
<dd><p>The file name to be deleted doesn&rsquo;t exist.
</p>
</dd>
<dt><code class="code">EPERM</code></dt>
<dd><p>On some systems <code class="code">unlink</code> cannot be used to delete the name of a
directory, or at least can only be used this way by a privileged user.
To avoid such problems, use <code class="code">rmdir</code> to delete directories.  (On
GNU/Linux and GNU/Hurd systems <code class="code">unlink</code> can never delete the name of a directory.)
</p>
</dd>
<dt><code class="code">EROFS</code></dt>
<dd><p>The directory containing the file name to be deleted is on a read-only
file system and can&rsquo;t be modified.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-rmdir"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">rmdir</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>)</code><a class="copiable-link" href='#index-rmdir'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<a class="index-entry-id" id="index-directories_002c-deleting"></a>
<a class="index-entry-id" id="index-deleting-a-directory"></a>
<p>The <code class="code">rmdir</code> function deletes a directory.  The directory must be
empty before it can be removed; in other words, it can only contain
entries for <samp class="file">.</samp> and <samp class="file">..</samp>.
</p>
<p>In most other respects, <code class="code">rmdir</code> behaves like <code class="code">unlink</code>.  There
are two additional <code class="code">errno</code> error conditions defined for
<code class="code">rmdir</code>:
</p>
<dl class="table">
<dt><code class="code">ENOTEMPTY</code></dt>
<dt><code class="code">EEXIST</code></dt>
<dd><p>The directory to be deleted is not empty.
</p></dd>
</dl>

<p>These two error codes are synonymous; some systems use one, and some use
the other.  GNU/Linux and GNU/Hurd systems always use <code class="code">ENOTEMPTY</code>.
</p>
<p>The prototype for this function is declared in the header file
<samp class="file">unistd.h</samp>.
<a class="index-entry-id" id="index-unistd_002eh-8"></a>
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-remove"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">remove</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>)</code><a class="copiable-link" href='#index-remove'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the ISO&nbsp;C<!-- /@w --> function to remove a file.  It works like
<code class="code">unlink</code> for files and like <code class="code">rmdir</code> for directories.
<code class="code">remove</code> is declared in <samp class="file">stdio.h</samp>.
<a class="index-entry-id" id="index-stdio_002eh-16"></a>
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Renaming-Files.html">Renaming Files</a>, Previous: <a href="Symbolic-Links.html">Symbolic Links</a>, Up: <a href="File-System-Interface.html">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
