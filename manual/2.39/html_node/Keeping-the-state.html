<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Keeping the state (The GNU C Library)</title>

<meta name="description" content="Keeping the state (The GNU C Library)">
<meta name="keywords" content="Keeping the state (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Restartable-multibyte-conversion.html" rel="up" title="Restartable multibyte conversion">
<link href="Converting-a-Character.html" rel="next" title="Converting a Character">
<link href="Selecting-the-Conversion.html" rel="prev" title="Selecting the Conversion">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Keeping-the-state">
<div class="nav-panel">
<p>
Next: <a href="Converting-a-Character.html" accesskey="n" rel="next">Converting Single Characters</a>, Previous: <a href="Selecting-the-Conversion.html" accesskey="p" rel="prev">Selecting the conversion and its properties</a>, Up: <a href="Restartable-multibyte-conversion.html" accesskey="u" rel="up">Restartable Multibyte Conversion Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Representing-the-state-of-the-conversion">6.3.2 Representing the state of the conversion</h4>

<a class="index-entry-id" id="index-stateful"></a>
<p>In the introduction of this chapter it was said that certain character
sets use a <em class="dfn">stateful</em> encoding.  That is, the encoded values depend
in some way on the previous bytes in the text.
</p>
<p>Since the conversion functions allow converting a text in more than one
step we must have a way to pass this information from one call of the
functions to another.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-mbstate_005ft"><span class="category-def">Data type: </span><span><strong class="def-name">mbstate_t</strong><a class="copiable-link" href='#index-mbstate_005ft'> &para;</a></span></dt>
<dd>
<a class="index-entry-id" id="index-shift-state"></a>
<p>A variable of type <code class="code">mbstate_t</code> can contain all the information
about the <em class="dfn">shift state</em> needed from one call to a conversion
function to another.
</p>
<a class="index-entry-id" id="index-wchar_002eh-5"></a>
<p><code class="code">mbstate_t</code> is defined in <samp class="file">wchar.h</samp>.  It was introduced in
Amendment&nbsp;1<!-- /@w --> to ISO&nbsp;C90<!-- /@w -->.
</p></dd></dl>

<p>To use objects of type <code class="code">mbstate_t</code> the programmer has to define such
objects (normally as local variables on the stack) and pass a pointer to
the object to the conversion functions.  This way the conversion function
can update the object if the current multibyte character set is stateful.
</p>
<p>There is no specific function or initializer to put the state object in
any specific state.  The rules are that the object should always
represent the initial state before the first use, and this is achieved by
clearing the whole variable with code such as follows:
</p>
<div class="example smallexample">
<pre class="example-preformatted">{
  mbstate_t state;
  memset (&amp;state, '\0', sizeof (state));
  /* <span class="r">from now on <var class="var">state</var> can be used.</span>  */
  &hellip;
}
</pre></div>

<p>When using the conversion functions to generate output it is often
necessary to test whether the current state corresponds to the initial
state.  This is necessary, for example, to decide whether to emit
escape sequences to set the state to the initial state at certain
sequence points.  Communication protocols often require this.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mbsinit"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">mbsinit</strong> <code class="def-code-arguments">(const mbstate_t *<var class="var">ps</var>)</code><a class="copiable-link" href='#index-mbsinit'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">mbsinit</code> function determines whether the state object pointed
to by <var class="var">ps</var> is in the initial state.  If <var class="var">ps</var> is a null pointer or
the object is in the initial state the return value is nonzero.  Otherwise
it is zero.
</p>
<a class="index-entry-id" id="index-wchar_002eh-6"></a>
<p><code class="code">mbsinit</code> was introduced in Amendment&nbsp;1<!-- /@w --> to ISO&nbsp;C90<!-- /@w --> and is
declared in <samp class="file">wchar.h</samp>.
</p></dd></dl>

<p>Code using <code class="code">mbsinit</code> often looks similar to this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">{
  mbstate_t state;
  memset (&amp;state, '\0', sizeof (state));
  /* <span class="r">Use <var class="var">state</var>.</span>  */
  &hellip;
  if (! mbsinit (&amp;state))
    {
      /* <span class="r">Emit code to return to initial state.</span>  */
      const wchar_t empty[] = L&quot;&quot;;
      const wchar_t *srcp = empty;
      wcsrtombs (outbuf, &amp;srcp, outbuflen, &amp;state);
    }
  &hellip;
}
</pre></div>

<p>The code to emit the escape sequence to get back to the initial state is
interesting.  The <code class="code">wcsrtombs</code> function can be used to determine the
necessary output code (see <a class="pxref" href="Converting-Strings.html">Converting Multibyte and Wide Character Strings</a>).  Please note that with
the GNU C Library it is not necessary to perform this extra action for the
conversion from multibyte text to wide character text since the wide
character encoding is not stateful.  But there is nothing mentioned in
any standard that prohibits making <code class="code">wchar_t</code> use a stateful
encoding.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Converting-a-Character.html">Converting Single Characters</a>, Previous: <a href="Selecting-the-Conversion.html">Selecting the conversion and its properties</a>, Up: <a href="Restartable-multibyte-conversion.html">Restartable Multibyte Conversion Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
