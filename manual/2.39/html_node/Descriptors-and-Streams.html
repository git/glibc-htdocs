<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Descriptors and Streams (The GNU C Library)</title>

<meta name="description" content="Descriptors and Streams (The GNU C Library)">
<meta name="keywords" content="Descriptors and Streams (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Low_002dLevel-I_002fO.html" rel="up" title="Low-Level I/O">
<link href="Stream_002fDescriptor-Precautions.html" rel="next" title="Stream/Descriptor Precautions">
<link href="File-Position-Primitive.html" rel="prev" title="File Position Primitive">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Descriptors-and-Streams">
<div class="nav-panel">
<p>
Next: <a href="Stream_002fDescriptor-Precautions.html" accesskey="n" rel="next">Dangers of Mixing Streams and Descriptors</a>, Previous: <a href="File-Position-Primitive.html" accesskey="p" rel="prev">Setting the File Position of a Descriptor</a>, Up: <a href="Low_002dLevel-I_002fO.html" accesskey="u" rel="up">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Descriptors-and-Streams-1">13.4 Descriptors and Streams</h3>
<a class="index-entry-id" id="index-streams_002c-and-file-descriptors"></a>
<a class="index-entry-id" id="index-converting-file-descriptor-to-stream"></a>
<a class="index-entry-id" id="index-extracting-file-descriptor-from-stream"></a>

<p>Given an open file descriptor, you can create a stream for it with the
<code class="code">fdopen</code> function.  You can get the underlying file descriptor for
an existing stream with the <code class="code">fileno</code> function.  These functions are
declared in the header file <samp class="file">stdio.h</samp>.
<a class="index-entry-id" id="index-stdio_002eh-15"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fdopen"><span class="category-def">Function: </span><span><code class="def-type">FILE *</code> <strong class="def-name">fdopen</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, const char *<var class="var">opentype</var>)</code><a class="copiable-link" href='#index-fdopen'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap lock
| AC-Unsafe mem lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fdopen</code> function returns a new stream for the file descriptor
<var class="var">filedes</var>.
</p>
<p>The <var class="var">opentype</var> argument is interpreted in the same way as for the
<code class="code">fopen</code> function (see <a class="pxref" href="Opening-Streams.html">Opening Streams</a>), except that
the &lsquo;<samp class="samp">b</samp>&rsquo; option is not permitted; this is because GNU systems make no
distinction between text and binary files.  Also, <code class="code">&quot;w&quot;</code> and
<code class="code">&quot;w+&quot;</code> do not cause truncation of the file; these have an effect only
when opening a file, and in this case the file has already been opened.
You must make sure that the <var class="var">opentype</var> argument matches the actual
mode of the open file descriptor.
</p>
<p>The return value is the new stream.  If the stream cannot be created
(for example, if the modes for the file indicated by the file descriptor
do not permit the access specified by the <var class="var">opentype</var> argument), a
null pointer is returned instead.
</p>
<p>In some other systems, <code class="code">fdopen</code> may fail to detect that the modes
for file descriptors do not permit the access specified by
<code class="code">opentype</code>.  The GNU C Library always checks for this.
</p></dd></dl>

<p>For an example showing the use of the <code class="code">fdopen</code> function,
see <a class="ref" href="Creating-a-Pipe.html">Creating a Pipe</a>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fileno"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fileno</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href='#index-fileno'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the file descriptor associated with the stream
<var class="var">stream</var>.  If an error is detected (for example, if the <var class="var">stream</var>
is not valid) or if <var class="var">stream</var> does not do I/O to a file,
<code class="code">fileno</code> returns <em class="math">-1</em>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fileno_005funlocked"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fileno_unlocked</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>)</code><a class="copiable-link" href='#index-fileno_005funlocked'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fileno_unlocked</code> function is equivalent to the <code class="code">fileno</code>
function except that it does not implicitly lock the stream if the state
is <code class="code">FSETLOCKING_INTERNAL</code>.
</p>
<p>This function is a GNU extension.
</p></dd></dl>

<a class="index-entry-id" id="index-standard-file-descriptors"></a>
<a class="index-entry-id" id="index-file-descriptors_002c-standard"></a>
<p>There are also symbolic constants defined in <samp class="file">unistd.h</samp> for the
file descriptors belonging to the standard streams <code class="code">stdin</code>,
<code class="code">stdout</code>, and <code class="code">stderr</code>; see <a class="ref" href="Standard-Streams.html">Standard Streams</a>.
<a class="index-entry-id" id="index-unistd_002eh-2"></a>
</p>
<dl class="vtable">
<dt id='index-STDIN_005fFILENO'><span><code class="code">STDIN_FILENO</code><a class="copiable-link" href='#index-STDIN_005fFILENO'> &para;</a></span></dt>
<dd>
<p>This macro has value <code class="code">0</code>, which is the file descriptor for
standard input.
<a class="index-entry-id" id="index-standard-input-file-descriptor"></a>
</p>
</dd>
<dt id='index-STDOUT_005fFILENO'><span><code class="code">STDOUT_FILENO</code><a class="copiable-link" href='#index-STDOUT_005fFILENO'> &para;</a></span></dt>
<dd>
<p>This macro has value <code class="code">1</code>, which is the file descriptor for
standard output.
<a class="index-entry-id" id="index-standard-output-file-descriptor"></a>
</p>
</dd>
<dt id='index-STDERR_005fFILENO'><span><code class="code">STDERR_FILENO</code><a class="copiable-link" href='#index-STDERR_005fFILENO'> &para;</a></span></dt>
<dd>
<p>This macro has value <code class="code">2</code>, which is the file descriptor for
standard error output.
</p></dd>
</dl>
<a class="index-entry-id" id="index-standard-error-file-descriptor"></a>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Stream_002fDescriptor-Precautions.html">Dangers of Mixing Streams and Descriptors</a>, Previous: <a href="File-Position-Primitive.html">Setting the File Position of a Descriptor</a>, Up: <a href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
