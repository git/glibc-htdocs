<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Reading Attributes (The GNU C Library)</title>

<meta name="description" content="Reading Attributes (The GNU C Library)">
<meta name="keywords" content="Reading Attributes (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-Attributes.html" rel="up" title="File Attributes">
<link href="Testing-File-Type.html" rel="next" title="Testing File Type">
<link href="Attribute-Meanings.html" rel="prev" title="Attribute Meanings">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Reading-Attributes">
<div class="nav-panel">
<p>
Next: <a href="Testing-File-Type.html" accesskey="n" rel="next">Testing the Type of a File</a>, Previous: <a href="Attribute-Meanings.html" accesskey="p" rel="prev">The meaning of the File Attributes</a>, Up: <a href="File-Attributes.html" accesskey="u" rel="up">File Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Reading-the-Attributes-of-a-File">14.9.2 Reading the Attributes of a File</h4>

<p>To examine the attributes of files, use the functions <code class="code">stat</code>,
<code class="code">fstat</code> and <code class="code">lstat</code>.  They return the attribute information in
a <code class="code">struct stat</code> object.  All three functions are declared in the
header file <samp class="file">sys/stat.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-stat"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">stat</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, struct stat *<var class="var">buf</var>)</code><a class="copiable-link" href='#index-stat'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">stat</code> function returns information about the attributes of the
file named by <var class="var">filename</var><!-- /@w --> in the structure pointed to by <var class="var">buf</var>.
</p>
<p>If <var class="var">filename</var> is the name of a symbolic link, the attributes you get
describe the file that the link points to.  If the link points to a
nonexistent file name, then <code class="code">stat</code> fails reporting a nonexistent
file.
</p>
<p>The return value is <code class="code">0</code> if the operation is successful, or
<code class="code">-1</code> on failure.  In addition to the usual file name errors
(see <a class="pxref" href="File-Name-Errors.html">File Name Errors</a>, the following <code class="code">errno</code> error conditions
are defined for this function:
</p>
<dl class="table">
<dt><code class="code">ENOENT</code></dt>
<dd><p>The file named by <var class="var">filename</var> doesn&rsquo;t exist.
</p></dd>
</dl>

<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
function is in fact <code class="code">stat64</code> since the LFS interface transparently
replaces the normal implementation.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-stat64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">stat64</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, struct stat64 *<var class="var">buf</var>)</code><a class="copiable-link" href='#index-stat64'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">stat</code> but it is also able to work on
files larger than 2^31 bytes on 32-bit systems.  To be able to do
this the result is stored in a variable of type <code class="code">struct stat64</code> to
which <var class="var">buf</var> must point.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
function is available under the name <code class="code">stat</code> and so transparently
replaces the interface for small files on 32-bit machines.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fstat"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fstat</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, struct stat *<var class="var">buf</var>)</code><a class="copiable-link" href='#index-fstat'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fstat</code> function is like <code class="code">stat</code>, except that it takes an
open file descriptor as an argument instead of a file name.
See <a class="xref" href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a>.
</p>
<p>Like <code class="code">stat</code>, <code class="code">fstat</code> returns <code class="code">0</code> on success and <code class="code">-1</code>
on failure.  The following <code class="code">errno</code> error conditions are defined for
<code class="code">fstat</code>:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is not a valid file descriptor.
</p></dd>
</dl>

<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
function is in fact <code class="code">fstat64</code> since the LFS interface transparently
replaces the normal implementation.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fstat64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fstat64</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, struct stat64 *<var class="var">buf</var>)</code><a class="copiable-link" href='#index-fstat64'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">fstat</code> but is able to work on large
files on 32-bit platforms.  For large files the file descriptor
<var class="var">filedes</var> should be obtained by <code class="code">open64</code> or <code class="code">creat64</code>.
The <var class="var">buf</var> pointer points to a variable of type <code class="code">struct stat64</code>
which is able to represent the larger values.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
function is available under the name <code class="code">fstat</code> and so transparently
replaces the interface for small files on 32-bit machines.
</p></dd></dl>


<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-lstat"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">lstat</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, struct stat *<var class="var">buf</var>)</code><a class="copiable-link" href='#index-lstat'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">lstat</code> function is like <code class="code">stat</code>, except that it does not
follow symbolic links.  If <var class="var">filename</var> is the name of a symbolic
link, <code class="code">lstat</code> returns information about the link itself; otherwise
<code class="code">lstat</code> works like <code class="code">stat</code>.  See <a class="xref" href="Symbolic-Links.html">Symbolic Links</a>.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
function is in fact <code class="code">lstat64</code> since the LFS interface transparently
replaces the normal implementation.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-lstat64"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">lstat64</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, struct stat64 *<var class="var">buf</var>)</code><a class="copiable-link" href='#index-lstat64'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">lstat</code> but it is also able to work on
files larger than 2^31 bytes on 32-bit systems.  To be able to do
this the result is stored in a variable of type <code class="code">struct stat64</code> to
which <var class="var">buf</var> must point.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this
function is available under the name <code class="code">lstat</code> and so transparently
replaces the interface for small files on 32-bit machines.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Testing-File-Type.html">Testing the Type of a File</a>, Previous: <a href="Attribute-Meanings.html">The meaning of the File Attributes</a>, Up: <a href="File-Attributes.html">File Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
