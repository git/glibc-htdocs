<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Thread-specific Data (The GNU C Library)</title>

<meta name="description" content="Thread-specific Data (The GNU C Library)">
<meta name="keywords" content="Thread-specific Data (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="POSIX-Threads.html" rel="up" title="POSIX Threads">
<link href="Non_002dPOSIX-Extensions.html" rel="next" title="Non-POSIX Extensions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Thread_002dspecific-Data">
<div class="nav-panel">
<p>
Next: <a href="Non_002dPOSIX-Extensions.html" accesskey="n" rel="next">Non-POSIX Extensions</a>, Up: <a href="POSIX-Threads.html" accesskey="u" rel="up">POSIX Threads</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Thread_002dspecific-Data-1">36.2.1 Thread-specific Data</h4>

<p>The GNU C Library implements functions to allow users to create and manage
data specific to a thread.  Such data may be destroyed at thread exit,
if a destructor is provided.  The following functions are defined:
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pthread_005fkey_005fcreate"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">pthread_key_create</strong> <code class="def-code-arguments">(pthread_key_t *<var class="var">key</var>, void (*<var class="var">destructor</var>)(void*))</code><a class="copiable-link" href='#index-pthread_005fkey_005fcreate'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Create a thread-specific data key for the calling thread, referenced by
<var class="var">key</var>.
</p>
<p>Objects declared with the C++11 <code class="code">thread_local</code> keyword are destroyed
before thread-specific data, so they should not be used in thread-specific
data destructors or even as members of the thread-specific data, since the
latter is passed as an argument to the destructor function.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pthread_005fkey_005fdelete"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">pthread_key_delete</strong> <code class="def-code-arguments">(pthread_key_t <var class="var">key</var>)</code><a class="copiable-link" href='#index-pthread_005fkey_005fdelete'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Destroy the thread-specific data <var class="var">key</var> in the calling thread.  The
destructor for the thread-specific data is not called during destruction, nor
is it called during thread exit.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_002apthread_005fgetspecific"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">*pthread_getspecific</strong> <code class="def-code-arguments">(pthread_key_t <var class="var">key</var>)</code><a class="copiable-link" href='#index-_002apthread_005fgetspecific'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Return the thread-specific data associated with <var class="var">key</var> in the calling
thread.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pthread_005fsetspecific"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">pthread_setspecific</strong> <code class="def-code-arguments">(pthread_key_t <var class="var">key</var>, const void *<var class="var">value</var>)</code><a class="copiable-link" href='#index-pthread_005fsetspecific'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Associate the thread-specific <var class="var">value</var> with <var class="var">key</var> in the calling thread.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Non_002dPOSIX-Extensions.html">Non-POSIX Extensions</a>, Up: <a href="POSIX-Threads.html">POSIX Threads</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
