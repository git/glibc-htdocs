<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Time Basics (The GNU C Library)</title>

<meta name="description" content="Time Basics (The GNU C Library)">
<meta name="keywords" content="Time Basics (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Date-and-Time.html" rel="up" title="Date and Time">
<link href="Time-Types.html" rel="next" title="Time Types">


</head>

<body lang="en">
<div class="section-level-extent" id="Time-Basics">
<div class="nav-panel">
<p>
Next: <a href="Time-Types.html" accesskey="n" rel="next">Time Types</a>, Up: <a href="Date-and-Time.html" accesskey="u" rel="up">Date and Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Time-Basics-1">22.1 Time Basics</h3>
<a class="index-entry-id" id="index-time"></a>

<p>Discussing time in a technical manual can be difficult because the word
&ldquo;time&rdquo; in English refers to lots of different things.  In this manual,
we use a rigorous terminology to avoid confusion, and the only thing we
use the simple word &ldquo;time&rdquo; for is to talk about the abstract concept.
</p>
<p>A <em class="dfn">calendar time</em> is a point in the time continuum, for example
November 4, 1990, at 18:02.5 UTC.  Sometimes this is called &ldquo;absolute
time&rdquo;.
<a class="index-entry-id" id="index-calendar-time"></a>
</p>
<p>We don&rsquo;t speak of a &ldquo;date&rdquo;, because that is inherent in a calendar
time.
<a class="index-entry-id" id="index-date"></a>
</p>
<p>An <em class="dfn">interval</em> is a contiguous part of the time continuum between two
calendar times, for example the hour between 9:00 and 10:00 on July 4,
1980.
<a class="index-entry-id" id="index-interval"></a>
</p>
<p>An <em class="dfn">elapsed time</em> is the length of an interval, for example, 35
minutes.  People sometimes sloppily use the word &ldquo;interval&rdquo; to refer
to the elapsed time of some interval.
<a class="index-entry-id" id="index-elapsed-time"></a>
<a class="index-entry-id" id="index-time_002c-elapsed"></a>
</p>
<p>An <em class="dfn">amount of time</em> is a sum of elapsed times, which need not be of
any specific intervals.  For example, the amount of time it takes to
read a book might be 9 hours, independently of when and in how many
sittings it is read.
</p>
<p>A <em class="dfn">period</em> is the elapsed time of an interval between two events,
especially when they are part of a sequence of regularly repeating
events.
<a class="index-entry-id" id="index-period-of-time"></a>
</p>
<p>A <em class="dfn">simple calendar time</em> is a calendar time represented as an
elapsed time since a fixed, implementation-specific calendar time
called the <em class="dfn">epoch</em>.  This representation is convenient for doing
calculations on calendar times, such as finding the elapsed time
between two calendar times.  Simple calendar times are independent of
time zone; they represent the same instant in time regardless of where
on the globe the computer is.
</p>
<p>POSIX says that simple calendar times do not include leap seconds, but
some (otherwise POSIX-conformant) systems can be configured to include
leap seconds in simple calendar times.
<a class="index-entry-id" id="index-leap-seconds"></a>
<a class="index-entry-id" id="index-seconds_002c-leap"></a>
<a class="index-entry-id" id="index-simple-time"></a>
<a class="index-entry-id" id="index-simple-calendar-time"></a>
<a class="index-entry-id" id="index-calendar-time_002c-simple"></a>
<a class="index-entry-id" id="index-epoch"></a>
</p>
<p>A <em class="dfn">broken-down time</em> is a calendar time represented by its
components in the Gregorian calendar: year, month, day, hour, minute,
and second.  A broken-down time value is relative to a specific time
zone, and so it is also sometimes called a <em class="dfn">local time</em>.
Broken-down times are most useful for input and output, as they are
easier for people to understand, but more difficult to calculate with.
<a class="index-entry-id" id="index-broken_002ddown-time"></a>
<a class="index-entry-id" id="index-local-time"></a>
<a class="index-entry-id" id="index-Gregorian-calendar"></a>
<a class="index-entry-id" id="index-calendar_002c-Gregorian"></a>
</p>
<p><em class="dfn">CPU time</em> measures the amount of time that a single process has
actively used a CPU to perform computations.  It does not include the
time that process has spent waiting for external events.  The system
tracks the CPU time used by each process separately.
<a class="index-entry-id" id="index-CPU-time"></a>
</p>
<p><em class="dfn">Processor time</em> measures the amount of time <em class="emph">any</em> CPU has
been in use by <em class="emph">any</em> process.  It is a basic system resource,
since there&rsquo;s a limit to how much can exist in any given interval (the
elapsed time of the interval times the number of CPUs in the computer)
</p>
<p>People often call this CPU time, but we reserve the latter term in
this manual for the definition above.
<a class="index-entry-id" id="index-processor-time"></a>
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Time-Types.html">Time Types</a>, Up: <a href="Date-and-Time.html">Date and Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
