<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>64-bit time symbol handling (The GNU C Library)</title>

<meta name="description" content="64-bit time symbol handling (The GNU C Library)">
<meta name="keywords" content="64-bit time symbol handling (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Symbol-handling.html" rel="up" title="Symbol handling">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="appendixsubsec-level-extent" id="g_t64_002dbit-time-symbol-handling">
<div class="nav-panel">
<p>
Up: <a href="Symbol-handling.html" accesskey="u" rel="up">Symbol handling in the GNU C Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="appendixsubsec" id="g_t64_002dbit-time-symbol-handling-in-the-GNU-C-Library">D.3.1 64-bit time symbol handling in the GNU C Library</h4>

<p>With respect to time handling, GNU C Library configurations fall in two
classes depending on the value of <code class="code">__TIMESIZE</code>:
</p>
<dl class="table">
<dt><code class="code"><code class="code">__TIMESIZE == 32</code></code></dt>
<dd>
<p>These <em class="dfn">dual-time</em> configurations have both 32-bit and 64-bit time
support.  32-bit time support provides type <code class="code">time_t</code> and cannot
handle dates beyond <em class="dfn">Y2038</em>.  64-bit time support provides type
<code class="code">__time64_t</code> and can handle dates beyond <em class="dfn">Y2038</em>.
</p>
<p>In these configurations, time-related types have two declarations,
a 64-bit one, and a 32-bit one; and time-related functions generally
have two definitions: a 64-bit one, and a 32-bit one which is a wrapper
around the former.  Therefore, for every <code class="code">time_t</code>-related symbol,
there is a corresponding <code class="code">__time64_t</code>-related symbol, the name of
which is usually the 32-bit symbol&rsquo;s name with <code class="code">__</code> (a double
underscore) prepended and <code class="code">64</code> appended.  For instance, the
64-bit-time counterpart of <code class="code">clock_gettime</code> is
<code class="code">__clock_gettime64</code>.
</p>
</dd>
<dt><code class="code"><code class="code">__TIMESIZE == 64</code></code></dt>
<dd>
<p>These <em class="dfn">single-time</em> configurations only have a 64-bit <code class="code">time_t</code>
and related functions, which can handle dates beyond 2038-01-19
03:14:07 (aka <em class="dfn">Y2038</em>).
</p>
<p>In these configurations, time-related types only have a 64-bit
declaration; and time-related functions only have one 64-bit definition.
However, for every <code class="code">time_t</code>-related symbol, there is a
corresponding <code class="code">__time64_t</code>-related macro, the name of which is
derived as in the dual-time configuration case, and which expands to
the symbol&rsquo;s name.  For instance, the macro <code class="code">__clock_gettime64</code>
expands to <code class="code">clock_gettime</code>.
</p>
<p>These macros are purely internal to the GNU C Library and exist only so that
a single definition of the 64-bit time functions can be used on both
single-time and dual-time configurations, and so that glibc code can
freely call the 64-bit functions internally in all configurations.
</p>
</dd>
</dl>


<p>Note: at this point, 64-bit time support in dual-time configurations is
work-in-progress, so for these configurations, the public API only makes
the 32-bit time support available.  In a later change, the public API
will allow user code to choose the time size for a given compilation
unit.
</p>
<p>64-bit variants of time-related types or functions are defined for all
configurations and use 64-bit-time symbol names (for dual-time
configurations) or macros (for single-time configurations).
</p>
<p>32-bit variants of time-related types or functions are defined only for
dual-time configurations.
</p>
<p>Here is an example with <code class="code">localtime</code>:
</p>
<p>Function <code class="code">localtime</code> is declared in <samp class="file">time/time.h</samp> as
</p><div class="example smallexample">
<pre class="example-preformatted">extern struct tm *localtime (const time_t *__timer) __THROW;
libc_hidden_proto (localtime)
</pre></div>

<p>For single-time configurations, <code class="code">__localtime64</code> is a macro which
evaluates to <code class="code">localtime</code>; for dual-time configurations,
<code class="code">__localtime64</code> is a function similar to <code class="code">localtime</code> except
it uses Y2038-proof types:
</p><div class="example smallexample">
<pre class="example-preformatted">#if __TIMESIZE == 64
# define __localtime64 localtime
#else
extern struct tm *__localtime64 (const __time64_t *__timer) __THROW;
libc_hidden_proto (__localtime64)
#endif
</pre></div>

<p>(note: type <code class="code">time_t</code> is replaced with <code class="code">__time64_t</code> because
<code class="code">time_t</code> is not Y2038-proof, but <code class="code">struct tm</code> is not
replaced because it is already Y2038-proof.)
</p>
<p>The 64-bit-time implementation of <code class="code">localtime</code> is written as follows
and is compiled for both dual-time and single-time configuration classes.
</p>
<div class="example smallexample">
<pre class="example-preformatted">struct tm *
__localtime64 (const __time64_t *t)
{
  return __tz_convert (*t, 1, &amp;_tmbuf);
}
libc_hidden_def (__localtime64)
</pre></div>

<p>The 32-bit-time implementation is a wrapper and is only compiled for
dual-time configurations:
</p>
<div class="example smallexample">
<pre class="example-preformatted">#if __TIMESIZE != 64

struct tm *
localtime (const time_t *t)
{
  __time64_t t64 = *t;
  return __localtime64 (&amp;t64);
}
libc_hidden_def (localtime)

#endif
</pre></div>

</div>
<hr>
<div class="nav-panel">
<p>
Up: <a href="Symbol-handling.html">Symbol handling in the GNU C Library</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
