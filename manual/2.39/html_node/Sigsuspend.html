<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Sigsuspend (The GNU C Library)</title>

<meta name="description" content="Sigsuspend (The GNU C Library)">
<meta name="keywords" content="Sigsuspend (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Waiting-for-a-Signal.html" rel="up" title="Waiting for a Signal">
<link href="Pause-Problems.html" rel="prev" title="Pause Problems">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Sigsuspend">
<div class="nav-panel">
<p>
Previous: <a href="Pause-Problems.html" accesskey="p" rel="prev">Problems with <code class="code">pause</code></a>, Up: <a href="Waiting-for-a-Signal.html" accesskey="u" rel="up">Waiting for a Signal</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Using-sigsuspend">25.8.3 Using <code class="code">sigsuspend</code></h4>

<p>The clean and reliable way to wait for a signal to arrive is to block it
and then use <code class="code">sigsuspend</code>.  By using <code class="code">sigsuspend</code> in a loop,
you can wait for certain kinds of signals, while letting other kinds of
signals be handled by their handlers.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sigsuspend"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sigsuspend</strong> <code class="def-code-arguments">(const sigset_t *<var class="var">set</var>)</code><a class="copiable-link" href='#index-sigsuspend'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:sigprocmask/!bsd!linux
| AS-Unsafe lock/hurd
| AC-Unsafe lock/hurd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function replaces the process&rsquo;s signal mask with <var class="var">set</var> and then
suspends the process until a signal is delivered whose action is either
to terminate the process or invoke a signal handling function.  In other
words, the program is effectively suspended until one of the signals that
is not a member of <var class="var">set</var> arrives.
</p>
<p>If the process is woken up by delivery of a signal that invokes a handler
function, and the handler function returns, then <code class="code">sigsuspend</code> also
returns.
</p>
<p>The mask remains <var class="var">set</var> only as long as <code class="code">sigsuspend</code> is waiting.
The function <code class="code">sigsuspend</code> always restores the previous signal mask
when it returns.
</p>
<p>The return value and error conditions are the same as for <code class="code">pause</code>.
</p></dd></dl>

<p>With <code class="code">sigsuspend</code>, you can replace the <code class="code">pause</code> or <code class="code">sleep</code>
loop in the previous section with something completely reliable:
</p>
<div class="example smallexample">
<pre class="example-preformatted">sigset_t mask, oldmask;

&hellip;

/* <span class="r">Set up the mask of signals to temporarily block.</span> */
sigemptyset (&amp;mask);
sigaddset (&amp;mask, SIGUSR1);

&hellip;

/* <span class="r">Wait for a signal to arrive.</span> */
sigprocmask (SIG_BLOCK, &amp;mask, &amp;oldmask);
while (!usr_interrupt)
  sigsuspend (&amp;oldmask);
sigprocmask (SIG_UNBLOCK, &amp;mask, NULL);
</pre></div>

<p>This last piece of code is a little tricky.  The key point to remember
here is that when <code class="code">sigsuspend</code> returns, it resets the process&rsquo;s
signal mask to the original value, the value from before the call to
<code class="code">sigsuspend</code>&mdash;in this case, the <code class="code">SIGUSR1</code> signal is once
again blocked.  The second call to <code class="code">sigprocmask</code> is
necessary to explicitly unblock this signal.
</p>
<p>One other point: you may be wondering why the <code class="code">while</code> loop is
necessary at all, since the program is apparently only waiting for one
<code class="code">SIGUSR1</code> signal.  The answer is that the mask passed to
<code class="code">sigsuspend</code> permits the process to be woken up by the delivery of
other kinds of signals, as well&mdash;for example, job control signals.  If
the process is woken up by a signal that doesn&rsquo;t set
<code class="code">usr_interrupt</code>, it just suspends itself again until the &ldquo;right&rdquo;
kind of signal eventually arrives.
</p>
<p>This technique takes a few more lines of preparation, but that is needed
just once for each kind of wait criterion you want to use.  The code
that actually waits is just four lines.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Pause-Problems.html">Problems with <code class="code">pause</code></a>, Up: <a href="Waiting-for-a-Signal.html">Waiting for a Signal</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
