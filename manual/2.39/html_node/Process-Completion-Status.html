<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Process Completion Status (The GNU C Library)</title>

<meta name="description" content="Process Completion Status (The GNU C Library)">
<meta name="keywords" content="Process Completion Status (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Processes.html" rel="up" title="Processes">
<link href="BSD-Wait-Functions.html" rel="next" title="BSD Wait Functions">
<link href="Process-Completion.html" rel="prev" title="Process Completion">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Process-Completion-Status">
<div class="nav-panel">
<p>
Next: <a href="BSD-Wait-Functions.html" accesskey="n" rel="next">BSD Process Wait Function</a>, Previous: <a href="Process-Completion.html" accesskey="p" rel="prev">Process Completion</a>, Up: <a href="Processes.html" accesskey="u" rel="up">Processes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Process-Completion-Status-1">27.8 Process Completion Status</h3>

<p>If the exit status value (see <a class="pxref" href="Program-Termination.html">Program Termination</a>) of the child
process is zero, then the status value reported by <code class="code">waitpid</code> or
<code class="code">wait</code> is also zero.  You can test for other kinds of information
encoded in the returned status value using the following macros.
These macros are defined in the header file <samp class="file">sys/wait.h</samp>.
<a class="index-entry-id" id="index-sys_002fwait_002eh-1"></a>
</p>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-WIFEXITED"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">WIFEXITED</strong> <code class="def-code-arguments">(int <var class="var">status</var>)</code><a class="copiable-link" href='#index-WIFEXITED'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value if the child process terminated
normally with <code class="code">exit</code> or <code class="code">_exit</code>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-WEXITSTATUS"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">WEXITSTATUS</strong> <code class="def-code-arguments">(int <var class="var">status</var>)</code><a class="copiable-link" href='#index-WEXITSTATUS'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>If <code class="code">WIFEXITED</code> is true of <var class="var">status</var>, this macro returns the
low-order 8 bits of the exit status value from the child process.
See <a class="xref" href="Exit-Status.html">Exit Status</a>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-WIFSIGNALED"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">WIFSIGNALED</strong> <code class="def-code-arguments">(int <var class="var">status</var>)</code><a class="copiable-link" href='#index-WIFSIGNALED'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value if the child process terminated
because it received a signal that was not handled.
See <a class="xref" href="Signal-Handling.html">Signal Handling</a>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-WTERMSIG"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">WTERMSIG</strong> <code class="def-code-arguments">(int <var class="var">status</var>)</code><a class="copiable-link" href='#index-WTERMSIG'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>If <code class="code">WIFSIGNALED</code> is true of <var class="var">status</var>, this macro returns the
signal number of the signal that terminated the child process.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-WCOREDUMP"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">WCOREDUMP</strong> <code class="def-code-arguments">(int <var class="var">status</var>)</code><a class="copiable-link" href='#index-WCOREDUMP'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value if the child process terminated
and produced a core dump.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-WIFSTOPPED"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">WIFSTOPPED</strong> <code class="def-code-arguments">(int <var class="var">status</var>)</code><a class="copiable-link" href='#index-WIFSTOPPED'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value if the child process is stopped.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-WSTOPSIG"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">WSTOPSIG</strong> <code class="def-code-arguments">(int <var class="var">status</var>)</code><a class="copiable-link" href='#index-WSTOPSIG'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>If <code class="code">WIFSTOPPED</code> is true of <var class="var">status</var>, this macro returns the
signal number of the signal that caused the child process to stop.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="BSD-Wait-Functions.html">BSD Process Wait Function</a>, Previous: <a href="Process-Completion.html">Process Completion</a>, Up: <a href="Processes.html">Processes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
