<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Parsing of Floats (The GNU C Library)</title>

<meta name="description" content="Parsing of Floats (The GNU C Library)">
<meta name="keywords" content="Parsing of Floats (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Parsing-of-Numbers.html" rel="up" title="Parsing of Numbers">
<link href="Parsing-of-Integers.html" rel="prev" title="Parsing of Integers">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span.w-nolinebreak-text {white-space: nowrap}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
ul.mark-minus {list-style-type: "\2212"}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Parsing-of-Floats">
<div class="nav-panel">
<p>
Previous: <a href="Parsing-of-Integers.html" accesskey="p" rel="prev">Parsing of Integers</a>, Up: <a href="Parsing-of-Numbers.html" accesskey="u" rel="up">Parsing of Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Parsing-of-Floats-1">20.11.2 Parsing of Floats</h4>

<a class="index-entry-id" id="index-stdlib_002eh-19"></a>
<p>The &lsquo;<samp class="samp">str</samp>&rsquo; functions are declared in <samp class="file">stdlib.h</samp> and those
beginning with &lsquo;<samp class="samp">wcs</samp>&rsquo; are declared in <samp class="file">wchar.h</samp>.  One might
wonder about the use of <code class="code">restrict</code> in the prototypes of the
functions in this section.  It is seemingly useless but the ISO&nbsp;C<!-- /@w -->
standard uses it (for the functions defined there) so we have to do it
as well.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strtod"><span class="category-def">Function: </span><span><code class="def-type">double</code> <strong class="def-name">strtod</strong> <code class="def-code-arguments">(const char *restrict <var class="var">string</var>, char **restrict <var class="var">tailptr</var>)</code><a class="copiable-link" href='#index-strtod'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">strtod</code> (&ldquo;string-to-double&rdquo;) function converts the initial
part of <var class="var">string</var> to a floating-point number, which is returned as a
value of type <code class="code">double</code>.
</p>
<p>This function attempts to decompose <var class="var">string</var> as follows:
</p>
<ul class="itemize mark-bullet">
<li>A (possibly empty) sequence of whitespace characters.  Which characters
are whitespace is determined by the <code class="code">isspace</code> function
(see <a class="pxref" href="Classification-of-Characters.html">Classification of Characters</a>).  These are discarded.

</li><li>An optional plus or minus sign (&lsquo;<samp class="samp">+</samp>&rsquo; or &lsquo;<samp class="samp">-</samp>&rsquo;).

</li><li>A floating point number in decimal or hexadecimal format.  The
decimal format is:
<ul class="itemize mark-minus">
<li>A nonempty sequence of digits optionally containing a decimal-point
character&mdash;normally &lsquo;<samp class="samp">.</samp>&rsquo;, but it depends on the locale
(see <a class="pxref" href="General-Numeric.html">Generic Numeric Formatting Parameters</a>).

</li><li>An optional exponent part, consisting of a character &lsquo;<samp class="samp">e</samp>&rsquo; or
&lsquo;<samp class="samp">E</samp>&rsquo;, an optional sign, and a sequence of digits.

</li></ul>

<p>The hexadecimal format is as follows:
</p><ul class="itemize mark-minus">
<li>A 0x or 0X followed by a nonempty sequence of hexadecimal digits
optionally containing a decimal-point character&mdash;normally &lsquo;<samp class="samp">.</samp>&rsquo;, but
it depends on the locale (see <a class="pxref" href="General-Numeric.html">Generic Numeric Formatting Parameters</a>).

</li><li>An optional binary-exponent part, consisting of a character &lsquo;<samp class="samp">p</samp>&rsquo; or
&lsquo;<samp class="samp">P</samp>&rsquo;, an optional sign, and a sequence of digits.

</li></ul>

</li><li>Any remaining characters in the string.  If <var class="var">tailptr</var> is not a null
pointer, a pointer to this tail of the string is stored in
<code class="code">*<var class="var">tailptr</var></code>.
</li></ul>

<p>If the string is empty, contains only whitespace, or does not contain an
initial substring that has the expected syntax for a floating-point
number, no conversion is performed.  In this case, <code class="code">strtod</code> returns
a value of zero and the value returned in <code class="code">*<var class="var">tailptr</var></code> is the
value of <var class="var">string</var>.
</p>
<p>In a locale other than the standard <code class="code">&quot;C&quot;</code> or <code class="code">&quot;POSIX&quot;</code> locales,
this function may recognize additional locale-dependent syntax.
</p>
<p>If the string has valid syntax for a floating-point number but the value
is outside the range of a <code class="code">double</code>, <code class="code">strtod</code> will signal
overflow or underflow as described in <a class="ref" href="Math-Error-Reporting.html">Error Reporting by Mathematical Functions</a>.
</p>
<p><code class="code">strtod</code> recognizes four special input strings.  The strings
<code class="code">&quot;inf&quot;</code> and <code class="code">&quot;infinity&quot;</code> are converted to <em class="math">&#x221E;</em>,
or to the largest representable value if the floating-point format
doesn&rsquo;t support infinities.  You can prepend a <code class="code">&quot;+&quot;</code> or <code class="code">&quot;-&quot;</code>
to specify the sign.  Case is ignored when scanning these strings.
</p>
<p>The strings <code class="code">&quot;nan&quot;</code> and <code class="code">&quot;nan(<var class="var">chars&hellip;</var>)&quot;</code> are converted
to NaN.  Again, case is ignored.  If <var class="var">chars&hellip;</var> are provided, they
are used in some unspecified fashion to select a particular
representation of NaN (there can be several).
</p>
<p>Since zero is a valid result as well as the value returned on error, you
should check for errors in the same way as for <code class="code">strtol</code>, by
examining <code class="code">errno</code> and <var class="var">tailptr</var>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strtof"><span class="category-def">Function: </span><span><code class="def-type">float</code> <strong class="def-name">strtof</strong> <code class="def-code-arguments">(const char *<var class="var">string</var>, char **<var class="var">tailptr</var>)</code><a class="copiable-link" href='#index-strtof'> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-strtold"><span class="category-def">Function: </span><span><code class="def-type">long double</code> <strong class="def-name">strtold</strong> <code class="def-code-arguments">(const char *<var class="var">string</var>, char **<var class="var">tailptr</var>)</code><a class="copiable-link" href='#index-strtold'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions are analogous to <code class="code">strtod</code>, but return <code class="code">float</code>
and <code class="code">long double</code> values respectively.  They report errors in the
same way as <code class="code">strtod</code>.  <code class="code">strtof</code> can be substantially faster
than <code class="code">strtod</code>, but has less precision; conversely, <code class="code">strtold</code>
can be much slower but has more precision (on systems where <code class="code">long
double</code> is a separate type).
</p>
<p>These functions have been GNU extensions and are new to ISO&nbsp;C99<!-- /@w -->.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strtofN"><span class="category-def">Function: </span><span><code class="def-type">_FloatN</code> <strong class="def-name">strtofN</strong> <code class="def-code-arguments">(const char *<var class="var">string</var>, char **<var class="var">tailptr</var>)</code><a class="copiable-link" href='#index-strtofN'> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-strtofNx"><span class="category-def">Function: </span><span><code class="def-type">_FloatNx</code> <strong class="def-name">strtofNx</strong> <code class="def-code-arguments">(const char *<var class="var">string</var>, char **<var class="var">tailptr</var>)</code><a class="copiable-link" href='#index-strtofNx'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions are like <code class="code">strtod</code>, except for the return type.
</p>
<p>They were introduced in ISO/IEC&nbsp;TS&nbsp;<span class="w-nolinebreak-text">18661-3</span><!-- /@w --> and are available on machines
that support the related types; see <a class="pxref" href="Mathematics.html">Mathematics</a>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcstod"><span class="category-def">Function: </span><span><code class="def-type">double</code> <strong class="def-name">wcstod</strong> <code class="def-code-arguments">(const wchar_t *restrict <var class="var">string</var>, wchar_t **restrict <var class="var">tailptr</var>)</code><a class="copiable-link" href='#index-wcstod'> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-wcstof"><span class="category-def">Function: </span><span><code class="def-type">float</code> <strong class="def-name">wcstof</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">string</var>, wchar_t **<var class="var">tailptr</var>)</code><a class="copiable-link" href='#index-wcstof'> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-wcstold"><span class="category-def">Function: </span><span><code class="def-type">long double</code> <strong class="def-name">wcstold</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">string</var>, wchar_t **<var class="var">tailptr</var>)</code><a class="copiable-link" href='#index-wcstold'> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-wcstofN"><span class="category-def">Function: </span><span><code class="def-type">_FloatN</code> <strong class="def-name">wcstofN</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">string</var>, wchar_t **<var class="var">tailptr</var>)</code><a class="copiable-link" href='#index-wcstofN'> &para;</a></span></dt>
<dt class="deftypefnx deftypefunx-alias-deftypefnx def-cmd-deftypefn" id="index-wcstofNx"><span class="category-def">Function: </span><span><code class="def-type">_FloatNx</code> <strong class="def-name">wcstofNx</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">string</var>, wchar_t **<var class="var">tailptr</var>)</code><a class="copiable-link" href='#index-wcstofNx'> &para;</a></span></dt>
<dd>


<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wcstod</code>, <code class="code">wcstof</code>, <code class="code">wcstol</code>, <code class="code">wcstof<var class="var">N</var></code>,
and <code class="code">wcstof<var class="var">N</var>x</code> functions are equivalent in nearly all aspects
to the <code class="code">strtod</code>, <code class="code">strtof</code>, <code class="code">strtold</code>,
<code class="code">strtof<var class="var">N</var></code>, and <code class="code">strtof<var class="var">N</var>x</code> functions, but they
handle wide character strings.
</p>
<p>The <code class="code">wcstod</code> function was introduced in Amendment&nbsp;1<!-- /@w --> of ISO&nbsp;C90<!-- /@w -->.  The <code class="code">wcstof</code> and <code class="code">wcstold</code> functions were introduced in
ISO&nbsp;C99<!-- /@w -->.
</p>
<p>The <code class="code">wcstof<var class="var">N</var></code> and <code class="code">wcstof<var class="var">N</var>x</code> functions are not in
any standard, but are added to provide completeness for the
non-deprecated interface of wide character string to floating-point
conversion functions.  They are only available on machines that support
the related types; see <a class="pxref" href="Mathematics.html">Mathematics</a>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-atof"><span class="category-def">Function: </span><span><code class="def-type">double</code> <strong class="def-name">atof</strong> <code class="def-code-arguments">(const char *<var class="var">string</var>)</code><a class="copiable-link" href='#index-atof'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to the <code class="code">strtod</code> function, except that it
need not detect overflow and underflow errors.  The <code class="code">atof</code> function
is provided mostly for compatibility with existing code; using
<code class="code">strtod</code> is more robust.
</p></dd></dl>

<p>The GNU C Library also provides &lsquo;<samp class="samp">_l</samp>&rsquo; versions of these functions,
which take an additional argument, the locale to use in conversion.
</p>
<p>See also <a class="ref" href="Parsing-of-Integers.html">Parsing of Integers</a>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Parsing-of-Integers.html">Parsing of Integers</a>, Up: <a href="Parsing-of-Numbers.html">Parsing of Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
