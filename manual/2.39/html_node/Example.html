<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Example (The GNU C Library)</title>

<meta name="description" content="Example (The GNU C Library)">
<meta name="keywords" content="Example (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Messages.html" rel="up" title="Formatted Messages">
<link href="Adding-Severity-Classes.html" rel="prev" title="Adding Severity Classes">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Example">
<div class="nav-panel">
<p>
Previous: <a href="Adding-Severity-Classes.html" accesskey="p" rel="prev">Adding Severity Classes</a>, Up: <a href="Formatted-Messages.html" accesskey="u" rel="up">Formatted Messages</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="How-to-use-fmtmsg-and-addseverity">12.22.3 How to use <code class="code">fmtmsg</code> and <code class="code">addseverity</code></h4>

<p>Here is a simple example program to illustrate the use of both
functions described in this section.
</p>
<div class="example smallexample">
<pre class="example-preformatted">

#include &lt;fmtmsg.h&gt;

int
main (void)
{
  addseverity (5, &quot;NOTE2&quot;);
  fmtmsg (MM_PRINT, &quot;only1field&quot;, MM_INFO, &quot;text2&quot;, &quot;action2&quot;, &quot;tag2&quot;);
  fmtmsg (MM_PRINT, &quot;UX:cat&quot;, 5, &quot;invalid syntax&quot;, &quot;refer to manual&quot;,
          &quot;UX:cat:001&quot;);
  fmtmsg (MM_PRINT, &quot;label:foo&quot;, 6, &quot;text&quot;, &quot;action&quot;, &quot;tag&quot;);
  return 0;
}
</pre></div>

<p>The second call to <code class="code">fmtmsg</code> illustrates a use of this function as
it usually occurs on System V systems, which heavily use this function.
It seems worthwhile to give a short explanation here of how this system
works on System V.  The value of the
<var class="var">label</var> field (<code class="code">UX:cat</code>) says that the error occurred in the
Unix program <code class="code">cat</code>.  The explanation of the error follows and the
value for the <var class="var">action</var> parameter is <code class="code">&quot;refer to manual&quot;</code>.  One
could be more specific here, if necessary.  The <var class="var">tag</var> field contains,
as proposed above, the value of the string given for the <var class="var">label</var>
parameter, and additionally a unique ID (<code class="code">001</code> in this case).  For
a GNU environment this string could contain a reference to the
corresponding node in the Info page for the program.
</p>
<p>Running this program without specifying the <code class="code">MSGVERB</code> and
<code class="code">SEV_LEVEL</code> function produces the following output:
</p>
<div class="example smallexample">
<pre class="example-preformatted">UX:cat: NOTE2: invalid syntax
TO FIX: refer to manual UX:cat:001
</pre></div>

<p>We see the different fields of the message and how the extra glue (the
colons and the <code class="code">TO FIX</code> string) is printed.  But only one of the
three calls to <code class="code">fmtmsg</code> produced output.  The first call does not
print anything because the <var class="var">label</var> parameter is not in the correct
form.  The string must contain two fields, separated by a colon
(see <a class="pxref" href="Printing-Formatted-Messages.html">Printing Formatted Messages</a>).  The third <code class="code">fmtmsg</code> call
produced no output since the class with the numeric value <code class="code">6</code> is
not defined.  Although a class with numeric value <code class="code">5</code> is also not
defined by default, the call to <code class="code">addseverity</code> introduces it and
the second call to <code class="code">fmtmsg</code> produces the above output.
</p>
<p>When we change the environment of the program to contain
<code class="code">SEV_LEVEL=XXX,6,NOTE</code> when running it we get a different result:
</p>
<div class="example smallexample">
<pre class="example-preformatted">UX:cat: NOTE2: invalid syntax
TO FIX: refer to manual UX:cat:001
label:foo: NOTE: text
TO FIX: action tag
</pre></div>

<p>Now the third call to <code class="code">fmtmsg</code> produced some output and we see how
the string <code class="code">NOTE</code> from the environment variable appears in the
message.
</p>
<p>Now we can reduce the output by specifying which fields we are
interested in.  If we additionally set the environment variable
<code class="code">MSGVERB</code> to the value <code class="code">severity:label:action</code> we get the
following output:
</p>
<div class="example smallexample">
<pre class="example-preformatted">UX:cat: NOTE2
TO FIX: refer to manual
label:foo: NOTE
TO FIX: action
</pre></div>

<p>I.e., the output produced by the <var class="var">text</var> and the <var class="var">tag</var> parameters
to <code class="code">fmtmsg</code> vanished.  Please also note that now there is no colon
after the <code class="code">NOTE</code> and <code class="code">NOTE2</code> strings in the output.  This is
not necessary since there is no more output on this line because the text
is missing.
</p></div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Adding-Severity-Classes.html">Adding Severity Classes</a>, Up: <a href="Formatted-Messages.html">Formatted Messages</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
