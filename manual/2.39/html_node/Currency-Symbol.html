<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Currency Symbol (The GNU C Library)</title>

<meta name="description" content="Currency Symbol (The GNU C Library)">
<meta name="keywords" content="Currency Symbol (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="The-Lame-Way-to-Locale-Data.html" rel="up" title="The Lame Way to Locale Data">
<link href="Sign-of-Money-Amount.html" rel="next" title="Sign of Money Amount">
<link href="General-Numeric.html" rel="prev" title="General Numeric">


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Currency-Symbol">
<div class="nav-panel">
<p>
Next: <a href="Sign-of-Money-Amount.html" accesskey="n" rel="next">Printing the Sign of a Monetary Amount</a>, Previous: <a href="General-Numeric.html" accesskey="p" rel="prev">Generic Numeric Formatting Parameters</a>, Up: <a href="The-Lame-Way-to-Locale-Data.html" accesskey="u" rel="up"><code class="code">localeconv</code>: It is portable but &hellip;</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Printing-the-Currency-Symbol">7.7.1.2 Printing the Currency Symbol</h4>
<a class="index-entry-id" id="index-currency-symbols"></a>

<p>These members of the <code class="code">struct lconv</code> structure specify how to print
the symbol to identify a monetary value&mdash;the international analog of
&lsquo;<samp class="samp">$</samp>&rsquo; for US dollars.
</p>
<p>Each country has two standard currency symbols.  The <em class="dfn">local currency
symbol</em> is used commonly within the country, while the
<em class="dfn">international currency symbol</em> is used internationally to refer to
that country&rsquo;s currency when it is necessary to indicate the country
unambiguously.
</p>
<p>For example, many countries use the dollar as their monetary unit, and
when dealing with international currencies it&rsquo;s important to specify
that one is dealing with (say) Canadian dollars instead of U.S. dollars
or Australian dollars.  But when the context is known to be Canada,
there is no need to make this explicit&mdash;dollar amounts are implicitly
assumed to be in Canadian dollars.
</p>
<dl class="table">
<dt><code class="code">char *currency_symbol</code></dt>
<dd><p>The local currency symbol for the selected locale.
</p>
<p>In the standard &lsquo;<samp class="samp">C</samp>&rsquo; locale, this member has a value of <code class="code">&quot;&quot;</code>
(the empty string), meaning &ldquo;unspecified&rdquo;.  The ISO standard doesn&rsquo;t
say what to do when you find this value; we recommend you simply print
the empty string as you would print any other string pointed to by this
variable.
</p>
</dd>
<dt><code class="code">char *int_curr_symbol</code></dt>
<dd><p>The international currency symbol for the selected locale.
</p>
<p>The value of <code class="code">int_curr_symbol</code> should normally consist of a
three-letter abbreviation determined by the international standard
<cite class="cite">ISO 4217 Codes for the Representation of Currency and Funds</cite>,
followed by a one-character separator (often a space).
</p>
<p>In the standard &lsquo;<samp class="samp">C</samp>&rsquo; locale, this member has a value of <code class="code">&quot;&quot;</code>
(the empty string), meaning &ldquo;unspecified&rdquo;.  We recommend you simply print
the empty string as you would print any other string pointed to by this
variable.
</p>
</dd>
<dt><code class="code">char p_cs_precedes</code></dt>
<dt><code class="code">char n_cs_precedes</code></dt>
<dt><code class="code">char int_p_cs_precedes</code></dt>
<dt><code class="code">char int_n_cs_precedes</code></dt>
<dd><p>These members are <code class="code">1</code> if the <code class="code">currency_symbol</code> or
<code class="code">int_curr_symbol</code> strings should precede the value of a monetary
amount, or <code class="code">0</code> if the strings should follow the value.  The
<code class="code">p_cs_precedes</code> and <code class="code">int_p_cs_precedes</code> members apply to
positive amounts (or zero), and the <code class="code">n_cs_precedes</code> and
<code class="code">int_n_cs_precedes</code> members apply to negative amounts.
</p>
<p>In the standard &lsquo;<samp class="samp">C</samp>&rsquo; locale, all of these members have a value of
<code class="code">CHAR_MAX</code>, meaning &ldquo;unspecified&rdquo;.  The ISO standard doesn&rsquo;t say
what to do when you find this value.  We recommend printing the
currency symbol before the amount, which is right for most countries.
In other words, treat all nonzero values alike in these members.
</p>
<p>The members with the <code class="code">int_</code> prefix apply to the
<code class="code">int_curr_symbol</code> while the other two apply to
<code class="code">currency_symbol</code>.
</p>
</dd>
<dt><code class="code">char p_sep_by_space</code></dt>
<dt><code class="code">char n_sep_by_space</code></dt>
<dt><code class="code">char int_p_sep_by_space</code></dt>
<dt><code class="code">char int_n_sep_by_space</code></dt>
<dd><p>These members are <code class="code">1</code> if a space should appear between the
<code class="code">currency_symbol</code> or <code class="code">int_curr_symbol</code> strings and the
amount, or <code class="code">0</code> if no space should appear.  The
<code class="code">p_sep_by_space</code> and <code class="code">int_p_sep_by_space</code> members apply to
positive amounts (or zero), and the <code class="code">n_sep_by_space</code> and
<code class="code">int_n_sep_by_space</code> members apply to negative amounts.
</p>
<p>In the standard &lsquo;<samp class="samp">C</samp>&rsquo; locale, all of these members have a value of
<code class="code">CHAR_MAX</code>, meaning &ldquo;unspecified&rdquo;.  The ISO standard doesn&rsquo;t say
what you should do when you find this value; we suggest you treat it as
1 (print a space).  In other words, treat all nonzero values alike in
these members.
</p>
<p>The members with the <code class="code">int_</code> prefix apply to the
<code class="code">int_curr_symbol</code> while the other two apply to
<code class="code">currency_symbol</code>.  There is one specialty with the
<code class="code">int_curr_symbol</code>, though.  Since all legal values contain a space
at the end of the string one either prints this space (if the currency
symbol must appear in front and must be separated) or one has to avoid
printing this character at all (especially when at the end of the
string).
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Sign-of-Money-Amount.html">Printing the Sign of a Monetary Amount</a>, Previous: <a href="General-Numeric.html">Generic Numeric Formatting Parameters</a>, Up: <a href="The-Lame-Way-to-Locale-Data.html"><code class="code">localeconv</code>: It is portable but &hellip;</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
