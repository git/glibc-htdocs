<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Control Functions (The GNU C Library)</title>

<meta name="description" content="Control Functions (The GNU C Library)">
<meta name="keywords" content="Control Functions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Arithmetic.html" rel="up" title="Arithmetic">
<link href="Arithmetic-Functions.html" rel="next" title="Arithmetic Functions">
<link href="Rounding.html" rel="prev" title="Rounding">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Control-Functions">
<div class="nav-panel">
<p>
Next: <a href="Arithmetic-Functions.html" accesskey="n" rel="next">Arithmetic Functions</a>, Previous: <a href="Rounding.html" accesskey="p" rel="prev">Rounding Modes</a>, Up: <a href="Arithmetic.html" accesskey="u" rel="up">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Floating_002dPoint-Control-Functions">20.7 Floating-Point Control Functions</h3>

<p>IEEE&nbsp;754<!-- /@w --> floating-point implementations allow the programmer to
decide whether traps will occur for each of the exceptions, by setting
bits in the <em class="dfn">control word</em>.  In C, traps result in the program
receiving the <code class="code">SIGFPE</code> signal; see <a class="ref" href="Signal-Handling.html">Signal Handling</a>.
</p>
<p><strong class="strong">NB:</strong> IEEE&nbsp;754<!-- /@w --> says that trap handlers are given details of
the exceptional situation, and can set the result value.  C signals do
not provide any mechanism to pass this information back and forth.
Trapping exceptions in C is therefore not very useful.
</p>
<p>It is sometimes necessary to save the state of the floating-point unit
while you perform some calculation.  The library provides functions
which save and restore the exception flags, the set of exceptions that
generate traps, and the rounding mode.  This information is known as the
<em class="dfn">floating-point environment</em>.
</p>
<p>The functions to save and restore the floating-point environment all use
a variable of type <code class="code">fenv_t</code> to store information.  This type is
defined in <samp class="file">fenv.h</samp>.  Its size and contents are
implementation-defined.  You should not attempt to manipulate a variable
of this type directly.
</p>
<p>To save the state of the FPU, use one of these functions:
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fegetenv"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fegetenv</strong> <code class="def-code-arguments">(fenv_t *<var class="var">envp</var>)</code><a class="copiable-link" href='#index-fegetenv'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Store the floating-point environment in the variable pointed to by
<var class="var">envp</var>.
</p>
<p>The function returns zero in case the operation was successful, a
non-zero value otherwise.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-feholdexcept"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">feholdexcept</strong> <code class="def-code-arguments">(fenv_t *<var class="var">envp</var>)</code><a class="copiable-link" href='#index-feholdexcept'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Store the current floating-point environment in the object pointed to by
<var class="var">envp</var>.  Then clear all exception flags, and set the FPU to trap no
exceptions.  Not all FPUs support trapping no exceptions; if
<code class="code">feholdexcept</code> cannot set this mode, it returns nonzero value.  If it
succeeds, it returns zero.
</p></dd></dl>

<p>The functions which restore the floating-point environment can take these
kinds of arguments:
</p>
<ul class="itemize mark-bullet">
<li>Pointers to <code class="code">fenv_t</code> objects, which were initialized previously by a
call to <code class="code">fegetenv</code> or <code class="code">feholdexcept</code>.
</li><li><a class="index-entry-id" id="index-FE_005fDFL_005fENV"></a>
The special macro <code class="code">FE_DFL_ENV</code> which represents the floating-point
environment as it was available at program start.
</li><li>Implementation defined macros with names starting with <code class="code">FE_</code> and
having type <code class="code">fenv_t *</code>.

<a class="index-entry-id" id="index-FE_005fNOMASK_005fENV"></a>
<p>If possible, the GNU C Library defines a macro <code class="code">FE_NOMASK_ENV</code>
which represents an environment where every exception raised causes a
trap to occur.  You can test for this macro using <code class="code">#ifdef</code>.  It is
only defined if <code class="code">_GNU_SOURCE</code> is defined.
</p>
<p>Some platforms might define other predefined environments.
</p></li></ul>

<p>To set the floating-point environment, you can use either of these
functions:
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fesetenv"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fesetenv</strong> <code class="def-code-arguments">(const fenv_t *<var class="var">envp</var>)</code><a class="copiable-link" href='#index-fesetenv'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Set the floating-point environment to that described by <var class="var">envp</var>.
</p>
<p>The function returns zero in case the operation was successful, a
non-zero value otherwise.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-feupdateenv"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">feupdateenv</strong> <code class="def-code-arguments">(const fenv_t *<var class="var">envp</var>)</code><a class="copiable-link" href='#index-feupdateenv'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Like <code class="code">fesetenv</code>, this function sets the floating-point environment
to that described by <var class="var">envp</var>.  However, if any exceptions were
flagged in the status word before <code class="code">feupdateenv</code> was called, they
remain flagged after the call.  In other words, after <code class="code">feupdateenv</code>
is called, the status word is the bitwise OR of the previous status word
and the one saved in <var class="var">envp</var>.
</p>
<p>The function returns zero in case the operation was successful, a
non-zero value otherwise.
</p></dd></dl>

<p>TS 18661-1:2014 defines additional functions to save and restore
floating-point control modes (such as the rounding mode and whether
traps are enabled) while leaving other status (such as raised flags)
unchanged.
</p>
<a class="index-entry-id" id="index-FE_005fDFL_005fMODE"></a>
<p>The special macro <code class="code">FE_DFL_MODE</code> may be passed to
<code class="code">fesetmode</code>.  It represents the floating-point control modes at
program start.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fegetmode"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fegetmode</strong> <code class="def-code-arguments">(femode_t *<var class="var">modep</var>)</code><a class="copiable-link" href='#index-fegetmode'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Store the floating-point control modes in the variable pointed to by
<var class="var">modep</var>.
</p>
<p>The function returns zero in case the operation was successful, a
non-zero value otherwise.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fesetmode"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fesetmode</strong> <code class="def-code-arguments">(const femode_t *<var class="var">modep</var>)</code><a class="copiable-link" href='#index-fesetmode'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Set the floating-point control modes to those described by
<var class="var">modep</var>.
</p>
<p>The function returns zero in case the operation was successful, a
non-zero value otherwise.
</p></dd></dl>

<p>To control for individual exceptions if raising them causes a trap to
occur, you can use the following two functions.
</p>
<p><strong class="strong">Portability Note:</strong> These functions are all GNU extensions.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-feenableexcept"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">feenableexcept</strong> <code class="def-code-arguments">(int <var class="var">excepts</var>)</code><a class="copiable-link" href='#index-feenableexcept'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function enables traps for each of the exceptions as indicated by
the parameter <var class="var">excepts</var>.  The individual exceptions are described in
<a class="ref" href="Status-bit-operations.html">Examining the FPU status word</a>.  Only the specified exceptions are
enabled, the status of the other exceptions is not changed.
</p>
<p>The function returns the previous enabled exceptions in case the
operation was successful, <code class="code">-1</code> otherwise.
</p>
<p>Note: Enabling traps for an exception for which the exception flag is
currently already set (see <a class="pxref" href="Status-bit-operations.html">Examining the FPU status word</a>) has unspecified
consequences: it may or may not trigger a trap immediately.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fedisableexcept"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fedisableexcept</strong> <code class="def-code-arguments">(int <var class="var">excepts</var>)</code><a class="copiable-link" href='#index-fedisableexcept'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function disables traps for each of the exceptions as indicated by
the parameter <var class="var">excepts</var>.  The individual exceptions are described in
<a class="ref" href="Status-bit-operations.html">Examining the FPU status word</a>.  Only the specified exceptions are
disabled, the status of the other exceptions is not changed.
</p>
<p>The function returns the previous enabled exceptions in case the
operation was successful, <code class="code">-1</code> otherwise.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fegetexcept"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fegetexcept</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href='#index-fegetexcept'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The function returns a bitmask of all currently enabled exceptions.  It
returns <code class="code">-1</code> in case of failure.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Arithmetic-Functions.html">Arithmetic Functions</a>, Previous: <a href="Rounding.html">Rounding Modes</a>, Up: <a href="Arithmetic.html">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
