<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Complex Numbers (The GNU C Library)</title>

<meta name="description" content="Complex Numbers (The GNU C Library)">
<meta name="keywords" content="Complex Numbers (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Arithmetic.html" rel="up" title="Arithmetic">
<link href="Operations-on-Complex.html" rel="next" title="Operations on Complex">
<link href="Arithmetic-Functions.html" rel="prev" title="Arithmetic Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Complex-Numbers">
<div class="nav-panel">
<p>
Next: <a href="Operations-on-Complex.html" accesskey="n" rel="next">Projections, Conjugates, and Decomposing of Complex Numbers</a>, Previous: <a href="Arithmetic-Functions.html" accesskey="p" rel="prev">Arithmetic Functions</a>, Up: <a href="Arithmetic.html" accesskey="u" rel="up">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Complex-Numbers-1">20.9 Complex Numbers</h3>
<a class="index-entry-id" id="index-complex_002eh-1"></a>
<a class="index-entry-id" id="index-complex-numbers"></a>

<p>ISO&nbsp;C99<!-- /@w --> introduces support for complex numbers in C.  This is done
with a new type qualifier, <code class="code">complex</code>.  It is a keyword if and only
if <samp class="file">complex.h</samp> has been included.  There are three complex types,
corresponding to the three real types:  <code class="code">float complex</code>,
<code class="code">double complex</code>, and <code class="code">long double complex</code>.
</p>
<p>Likewise, on machines that have support for <code class="code">_Float<var class="var">N</var></code> or
<code class="code">_Float<var class="var">N</var>x</code> enabled, the complex types <code class="code">_Float<var class="var">N</var>
complex</code> and <code class="code">_Float<var class="var">N</var>x complex</code> are also available if
<samp class="file">complex.h</samp> has been included; see <a class="pxref" href="Mathematics.html">Mathematics</a>.
</p>
<p>To construct complex numbers you need a way to indicate the imaginary
part of a number.  There is no standard notation for an imaginary
floating point constant.  Instead, <samp class="file">complex.h</samp> defines two macros
that can be used to create complex numbers.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-_005fComplex_005fI"><span class="category-def">Macro: </span><span><code class="def-type">const float complex</code> <strong class="def-name">_Complex_I</strong><a class="copiable-link" href='#index-_005fComplex_005fI'> &para;</a></span></dt>
<dd>
<p>This macro is a representation of the complex number &ldquo;<em class="math">0+1i</em>&rdquo;.
Multiplying a real floating-point value by <code class="code">_Complex_I</code> gives a
complex number whose value is purely imaginary.  You can use this to
construct complex constants:
</p>
<div class="example smallexample">
<pre class="example-preformatted"><em class="math">3.0 + 4.0i</em> = <code class="code">3.0 + 4.0 * _Complex_I</code>
</pre></div>

<p>Note that <code class="code">_Complex_I * _Complex_I</code> has the value <code class="code">-1</code>, but
the type of that value is <code class="code">complex</code>.
</p></dd></dl>


<p><code class="code">_Complex_I</code> is a bit of a mouthful.  <samp class="file">complex.h</samp> also defines
a shorter name for the same constant.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-I"><span class="category-def">Macro: </span><span><code class="def-type">const float complex</code> <strong class="def-name">I</strong><a class="copiable-link" href='#index-I'> &para;</a></span></dt>
<dd>
<p>This macro has exactly the same value as <code class="code">_Complex_I</code>.  Most of the
time it is preferable.  However, it causes problems if you want to use
the identifier <code class="code">I</code> for something else.  You can safely write
</p>
<div class="example smallexample">
<pre class="example-preformatted">#include &lt;complex.h&gt;
#undef I
</pre></div>

<p>if you need <code class="code">I</code> for your own purposes.  (In that case we recommend
you also define some other short name for <code class="code">_Complex_I</code>, such as
<code class="code">J</code>.)
</p>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Operations-on-Complex.html">Projections, Conjugates, and Decomposing of Complex Numbers</a>, Previous: <a href="Arithmetic-Functions.html">Arithmetic Functions</a>, Up: <a href="Arithmetic.html">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
