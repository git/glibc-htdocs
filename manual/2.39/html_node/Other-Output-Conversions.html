<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Other Output Conversions (The GNU C Library)</title>

<meta name="description" content="Other Output Conversions (The GNU C Library)">
<meta name="keywords" content="Other Output Conversions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Output.html" rel="up" title="Formatted Output">
<link href="Formatted-Output-Functions.html" rel="next" title="Formatted Output Functions">
<link href="Floating_002dPoint-Conversions.html" rel="prev" title="Floating-Point Conversions">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Other-Output-Conversions">
<div class="nav-panel">
<p>
Next: <a href="Formatted-Output-Functions.html" accesskey="n" rel="next">Formatted Output Functions</a>, Previous: <a href="Floating_002dPoint-Conversions.html" accesskey="p" rel="prev">Floating-Point Conversions</a>, Up: <a href="Formatted-Output.html" accesskey="u" rel="up">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Other-Output-Conversions-1">12.12.6 Other Output Conversions</h4>

<p>This section describes miscellaneous conversions for <code class="code">printf</code>.
</p>
<p>The &lsquo;<samp class="samp">%c</samp>&rsquo; conversion prints a single character.  In case there is no
&lsquo;<samp class="samp">l</samp>&rsquo; modifier the <code class="code">int</code> argument is first converted to an
<code class="code">unsigned char</code>.  Then, if used in a wide stream function, the
character is converted into the corresponding wide character.  The
&lsquo;<samp class="samp">-</samp>&rsquo; flag can be used to specify left-justification in the field,
but no other flags are defined, and no precision or type modifier can be
given.  For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">printf (&quot;%c%c%c%c%c&quot;, 'h', 'e', 'l', 'l', 'o');
</pre></div>

<p>prints &lsquo;<samp class="samp">hello</samp>&rsquo;.
</p>
<p>If there is an &lsquo;<samp class="samp">l</samp>&rsquo; modifier present the argument is expected to be
of type <code class="code">wint_t</code>.  If used in a multibyte function the wide
character is converted into a multibyte character before being added to
the output.  In this case more than one output byte can be produced.
</p>
<p>The &lsquo;<samp class="samp">%s</samp>&rsquo; conversion prints a string.  If no &lsquo;<samp class="samp">l</samp>&rsquo; modifier is
present the corresponding argument must be of type <code class="code">char *</code> (or
<code class="code">const char *</code>).  If used in a wide stream function the string is
first converted to a wide character string.  A precision can be
specified to indicate the maximum number of characters to write;
otherwise characters in the string up to but not including the
terminating null character are written to the output stream.  The
&lsquo;<samp class="samp">-</samp>&rsquo; flag can be used to specify left-justification in the field,
but no other flags or type modifiers are defined for this conversion.
For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">printf (&quot;%3s%-6s&quot;, &quot;no&quot;, &quot;where&quot;);
</pre></div>

<p>prints &lsquo;<samp class="samp"> nowhere </samp>&rsquo;.
</p>
<p>If there is an &lsquo;<samp class="samp">l</samp>&rsquo; modifier present, the argument is expected to
be of type <code class="code">wchar_t</code> (or <code class="code">const wchar_t *</code>).
</p>
<p>If you accidentally pass a null pointer as the argument for a &lsquo;<samp class="samp">%s</samp>&rsquo;
conversion, the GNU C Library prints it as &lsquo;<samp class="samp">(null)</samp>&rsquo;.  We think this
is more useful than crashing.  But it&rsquo;s not good practice to pass a null
argument intentionally.
</p>
<p>The &lsquo;<samp class="samp">%m</samp>&rsquo; conversion prints the string corresponding to the error
code in <code class="code">errno</code>.  See <a class="xref" href="Error-Messages.html">Error Messages</a>.  Thus:
</p>
<div class="example smallexample">
<pre class="example-preformatted">fprintf (stderr, &quot;can't open `%s': %m\n&quot;, filename);
</pre></div>

<p>is equivalent to:
</p>
<div class="example smallexample">
<pre class="example-preformatted">fprintf (stderr, &quot;can't open `%s': %s\n&quot;, filename, strerror (errno));
</pre></div>

<p>The &lsquo;<samp class="samp">%m</samp>&rsquo; conversion can be used with the &lsquo;<samp class="samp">#</samp>&rsquo; flag to print an
error constant, as provided by <code class="code">strerrorname_np</code>.  Both &lsquo;<samp class="samp">%m</samp>&rsquo;
and &lsquo;<samp class="samp">%#m</samp>&rsquo; are GNU C Library extensions.
</p>
<p>The &lsquo;<samp class="samp">%p</samp>&rsquo; conversion prints a pointer value.  The corresponding
argument must be of type <code class="code">void *</code>.  In practice, you can use any
type of pointer.
</p>
<p>In the GNU C Library, non-null pointers are printed as unsigned integers,
as if a &lsquo;<samp class="samp">%#x</samp>&rsquo; conversion were used.  Null pointers print as
&lsquo;<samp class="samp">(nil)</samp>&rsquo;.  (Pointers might print differently in other systems.)
</p>
<p>For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">printf (&quot;%p&quot;, &quot;testing&quot;);
</pre></div>

<p>prints &lsquo;<samp class="samp">0x</samp>&rsquo; followed by a hexadecimal number&mdash;the address of the
string constant <code class="code">&quot;testing&quot;</code>.  It does not print the word
&lsquo;<samp class="samp">testing</samp>&rsquo;.
</p>
<p>You can supply the &lsquo;<samp class="samp">-</samp>&rsquo; flag with the &lsquo;<samp class="samp">%p</samp>&rsquo; conversion to
specify left-justification, but no other flags, precision, or type
modifiers are defined.
</p>
<p>The &lsquo;<samp class="samp">%n</samp>&rsquo; conversion is unlike any of the other output conversions.
It uses an argument which must be a pointer to an <code class="code">int</code>, but
instead of printing anything it stores the number of characters printed
so far by this call at that location.  The &lsquo;<samp class="samp">h</samp>&rsquo; and &lsquo;<samp class="samp">l</samp>&rsquo; type
modifiers are permitted to specify that the argument is of type
<code class="code">short int *</code> or <code class="code">long int *</code> instead of <code class="code">int *</code>, but no
flags, field width, or precision are permitted.
</p>
<p>For example,
</p>
<div class="example smallexample">
<pre class="example-preformatted">int nchar;
printf (&quot;%d %s%n\n&quot;, 3, &quot;bears&quot;, &amp;nchar);
</pre></div>

<p>prints:
</p>
<div class="example smallexample">
<pre class="example-preformatted">3 bears
</pre></div>

<p>and sets <code class="code">nchar</code> to <code class="code">7</code>, because &lsquo;<samp class="samp">3 bears</samp>&rsquo; is seven
characters.
</p>

<p>The &lsquo;<samp class="samp">%%</samp>&rsquo; conversion prints a literal &lsquo;<samp class="samp">%</samp>&rsquo; character.  This
conversion doesn&rsquo;t use an argument, and no flags, field width,
precision, or type modifiers are permitted.
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Formatted-Output-Functions.html">Formatted Output Functions</a>, Previous: <a href="Floating_002dPoint-Conversions.html">Floating-Point Conversions</a>, Up: <a href="Formatted-Output.html">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
