<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Source Layout (The GNU C Library)</title>

<meta name="description" content="Source Layout (The GNU C Library)">
<meta name="keywords" content="Source Layout (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Maintenance.html" rel="up" title="Maintenance">
<link href="Source-Fortification.html" rel="next" title="Source Fortification">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="appendixsec-level-extent" id="Source-Layout">
<div class="nav-panel">
<p>
Next: <a href="Source-Fortification.html" accesskey="n" rel="next">Fortification of function calls</a>, Up: <a href="Maintenance.html" accesskey="u" rel="up">Library Maintenance</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="appendixsec" id="Adding-New-Functions">D.1 Adding New Functions</h3>

<p>The process of building the library is driven by the makefiles, which
make heavy use of special features of GNU <code class="code">make</code>.  The makefiles
are very complex, and you probably don&rsquo;t want to try to understand them.
But what they do is fairly straightforward, and only requires that you
define a few variables in the right places.
</p>
<p>The library sources are divided into subdirectories, grouped by topic.
</p>
<p>The <samp class="file">string</samp> subdirectory has all the string-manipulation
functions, <samp class="file">math</samp> has all the mathematical functions, etc.
</p>
<p>Each subdirectory contains a simple makefile, called <samp class="file">Makefile</samp>,
which defines a few <code class="code">make</code> variables and then includes the global
makefile <samp class="file">Rules</samp> with a line like:
</p>
<div class="example smallexample">
<pre class="example-preformatted">include ../Rules
</pre></div>

<p>The basic variables that a subdirectory makefile defines are:
</p>
<dl class="table">
<dt><code class="code">subdir</code></dt>
<dd><p>The name of the subdirectory, for example <samp class="file">stdio</samp>.
This variable <strong class="strong">must</strong> be defined.
</p>
</dd>
<dt><code class="code">headers</code></dt>
<dd><p>The names of the header files in this section of the library,
such as <samp class="file">stdio.h</samp>.
</p>
</dd>
<dt><code class="code">routines</code></dt>
<dt><code class="code">aux</code></dt>
<dd><p>The names of the modules (source files) in this section of the library.
These should be simple names, such as &lsquo;<samp class="samp">strlen</samp>&rsquo; (rather than
complete file names, such as <samp class="file">strlen.c</samp>).  Use <code class="code">routines</code> for
modules that define functions in the library, and <code class="code">aux</code> for
auxiliary modules containing things like data definitions.  But the
values of <code class="code">routines</code> and <code class="code">aux</code> are just concatenated, so there
really is no practical difference.
</p>
</dd>
<dt><code class="code">tests</code></dt>
<dd><p>The names of test programs for this section of the library.  These
should be simple names, such as &lsquo;<samp class="samp">tester</samp>&rsquo; (rather than complete file
names, such as <samp class="file">tester.c</samp>).  &lsquo;<samp class="samp">make&nbsp;tests</samp>&rsquo;<!-- /@w --> will build and
run all the test programs.  If a test program needs input, put the test
data in a file called <samp class="file"><var class="var">test-program</var>.input</samp>; it will be given to
the test program on its standard input.  If a test program wants to be
run with arguments, put the arguments (all on a single line) in a file
called <samp class="file"><var class="var">test-program</var>.args</samp>.  Test programs should exit with
zero status when the test passes, and nonzero status when the test
indicates a bug in the library or error in building.
</p>
</dd>
<dt><code class="code">others</code></dt>
<dd><p>The names of &ldquo;other&rdquo; programs associated with this section of the
library.  These are programs which are not tests per se, but are other
small programs included with the library.  They are built by
&lsquo;<samp class="samp">make&nbsp;others</samp>&rsquo;<!-- /@w -->.
</p>
</dd>
<dt><code class="code">install-lib</code></dt>
<dt><code class="code">install-data</code></dt>
<dt><code class="code">install</code></dt>
<dd><p>Files to be installed by &lsquo;<samp class="samp">make&nbsp;install</samp>&rsquo;<!-- /@w -->.  Files listed in
&lsquo;<samp class="samp">install-lib</samp>&rsquo; are installed in the directory specified by
&lsquo;<samp class="samp">libdir</samp>&rsquo; in <samp class="file">configparms</samp> or <samp class="file">Makeconfig</samp>
(see <a class="pxref" href="Installation.html">Installing the GNU C Library</a>).  Files listed in <code class="code">install-data</code> are
installed in the directory specified by &lsquo;<samp class="samp">datadir</samp>&rsquo; in
<samp class="file">configparms</samp> or <samp class="file">Makeconfig</samp>.  Files listed in <code class="code">install</code>
are installed in the directory specified by &lsquo;<samp class="samp">bindir</samp>&rsquo; in
<samp class="file">configparms</samp> or <samp class="file">Makeconfig</samp>.
</p>
</dd>
<dt><code class="code">distribute</code></dt>
<dd><p>Other files from this subdirectory which should be put into a
distribution tar file.  You need not list here the makefile itself or
the source and header files listed in the other standard variables.
Only define <code class="code">distribute</code> if there are files used in an unusual way
that should go into the distribution.
</p>
</dd>
<dt><code class="code">generated</code></dt>
<dd><p>Files which are generated by <samp class="file">Makefile</samp> in this subdirectory.
These files will be removed by &lsquo;<samp class="samp">make&nbsp;clean</samp>&rsquo;<!-- /@w -->, and they will
never go into a distribution.
</p>
</dd>
<dt><code class="code">extra-objs</code></dt>
<dd><p>Extra object files which are built by <samp class="file">Makefile</samp> in this
subdirectory.  This should be a list of file names like <samp class="file">foo.o</samp>;
the files will actually be found in whatever directory object files are
being built in.  These files will be removed by &lsquo;<samp class="samp">make&nbsp;clean</samp>&rsquo;<!-- /@w -->.
This variable is used for secondary object files needed to build
<code class="code">others</code> or <code class="code">tests</code>.
</p></dd>
</dl>


<ul class="mini-toc">
<li><a href="Adding-Platform_002dspecific.html" accesskey="1">Platform-specific types, macros and functions</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Source-Fortification.html">Fortification of function calls</a>, Up: <a href="Maintenance.html">Library Maintenance</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
