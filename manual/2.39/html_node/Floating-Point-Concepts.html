<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Floating Point Concepts (The GNU C Library)</title>

<meta name="description" content="Floating Point Concepts (The GNU C Library)">
<meta name="keywords" content="Floating Point Concepts (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Floating-Type-Macros.html" rel="up" title="Floating Type Macros">
<link href="Floating-Point-Parameters.html" rel="next" title="Floating Point Parameters">
<style type="text/css">
<!--
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Floating-Point-Concepts">
<div class="nav-panel">
<p>
Next: <a href="Floating-Point-Parameters.html" accesskey="n" rel="next">Floating Point Parameters</a>, Up: <a href="Floating-Type-Macros.html" accesskey="u" rel="up">Floating Type Macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Floating-Point-Representation-Concepts">A.5.3.1 Floating Point Representation Concepts</h4>

<p>This section introduces the terminology for describing floating point
representations.
</p>
<p>You are probably already familiar with most of these concepts in terms
of scientific or exponential notation for floating point numbers.  For
example, the number <code class="code">123456.0</code> could be expressed in exponential
notation as <code class="code">1.23456e+05</code>, a shorthand notation indicating that the
mantissa <code class="code">1.23456</code> is multiplied by the base <code class="code">10</code> raised to
power <code class="code">5</code>.
</p>
<p>More formally, the internal representation of a floating point number
can be characterized in terms of the following parameters:
</p>
<ul class="itemize mark-bullet">
<li><a class="index-entry-id" id="index-sign-_0028of-floating-point-number_0029"></a>
The <em class="dfn">sign</em> is either <code class="code">-1</code> or <code class="code">1</code>.

</li><li><a class="index-entry-id" id="index-base-_0028of-floating-point-number_0029"></a>
<a class="index-entry-id" id="index-radix-_0028of-floating-point-number_0029"></a>
The <em class="dfn">base</em> or <em class="dfn">radix</em> for exponentiation, an integer greater
than <code class="code">1</code>.  This is a constant for a particular representation.

</li><li><a class="index-entry-id" id="index-exponent-_0028of-floating-point-number_0029"></a>
The <em class="dfn">exponent</em> to which the base is raised.  The upper and lower
bounds of the exponent value are constants for a particular
representation.

<a class="index-entry-id" id="index-bias-_0028of-floating-point-number-exponent_0029"></a>
<p>Sometimes, in the actual bits representing the floating point number,
the exponent is <em class="dfn">biased</em> by adding a constant to it, to make it
always be represented as an unsigned quantity.  This is only important
if you have some reason to pick apart the bit fields making up the
floating point number by hand, which is something for which the GNU C Library
provides no support.  So this is ignored in the discussion that
follows.
</p>
</li><li><a class="index-entry-id" id="index-mantissa-_0028of-floating-point-number_0029"></a>
<a class="index-entry-id" id="index-significand-_0028of-floating-point-number_0029"></a>
The <em class="dfn">mantissa</em> or <em class="dfn">significand</em> is an unsigned integer which is a
part of each floating point number.

</li><li><a class="index-entry-id" id="index-precision-_0028of-floating-point-number_0029"></a>
The <em class="dfn">precision</em> of the mantissa.  If the base of the representation
is <var class="var">b</var>, then the precision is the number of base-<var class="var">b</var> digits in
the mantissa.  This is a constant for a particular representation.

<a class="index-entry-id" id="index-hidden-bit-_0028of-floating-point-number-mantissa_0029"></a>
<p>Many floating point representations have an implicit <em class="dfn">hidden bit</em> in
the mantissa.  This is a bit which is present virtually in the mantissa,
but not stored in memory because its value is always 1 in a normalized
number.  The precision figure (see above) includes any hidden bits.
</p>
<p>Again, the GNU C Library provides no facilities for dealing with such
low-level aspects of the representation.
</p></li></ul>

<p>The mantissa of a floating point number represents an implicit fraction
whose denominator is the base raised to the power of the precision.  Since
the largest representable mantissa is one less than this denominator, the
value of the fraction is always strictly less than <code class="code">1</code>.  The
mathematical value of a floating point number is then the product of this
fraction, the sign, and the base raised to the exponent.
</p>
<a class="index-entry-id" id="index-normalized-floating-point-number"></a>
<p>We say that the floating point number is <em class="dfn">normalized</em> if the
fraction is at least <code class="code">1/<var class="var">b</var></code>, where <var class="var">b</var> is the base.  In
other words, the mantissa would be too large to fit if it were
multiplied by the base.  Non-normalized numbers are sometimes called
<em class="dfn">denormal</em>; they contain less precision than the representation
normally can hold.
</p>
<p>If the number is not normalized, then you can subtract <code class="code">1</code> from the
exponent while multiplying the mantissa by the base, and get another
floating point number with the same value.  <em class="dfn">Normalization</em> consists
of doing this repeatedly until the number is normalized.  Two distinct
normalized floating point numbers cannot be equal in value.
</p>
<p>(There is an exception to this rule: if the mantissa is zero, it is
considered normalized.  Another exception happens on certain machines
where the exponent is as small as the representation can hold.  Then
it is impossible to subtract <code class="code">1</code> from the exponent, so a number
may be normalized even if its fraction is less than <code class="code">1/<var class="var">b</var></code>.)
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Floating-Point-Parameters.html">Floating Point Parameters</a>, Up: <a href="Floating-Type-Macros.html">Floating Type Macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
