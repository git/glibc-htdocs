<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Low-Level Time String Parsing (The GNU C Library)</title>

<meta name="description" content="Low-Level Time String Parsing (The GNU C Library)">
<meta name="keywords" content="Low-Level Time String Parsing (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Parsing-Date-and-Time.html" rel="up" title="Parsing Date and Time">
<link href="General-Time-String-Parsing.html" rel="next" title="General Time String Parsing">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Low_002dLevel-Time-String-Parsing">
<div class="nav-panel">
<p>
Next: <a href="General-Time-String-Parsing.html" accesskey="n" rel="next">A More User-friendly Way to Parse Times and Dates</a>, Up: <a href="Parsing-Date-and-Time.html" accesskey="u" rel="up">Convert textual time and date information back</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Interpret-string-according-to-given-format">22.5.5.1 Interpret string according to given format</h4>

<p>The first function is rather low-level.  It is nevertheless frequently
used in software since it is better known.  Its interface and
implementation are heavily influenced by the <code class="code">getdate</code> function,
which is defined and implemented in terms of calls to <code class="code">strptime</code>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strptime"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">strptime</strong> <code class="def-code-arguments">(const char *<var class="var">s</var>, const char *<var class="var">fmt</var>, struct tm *<var class="var">tp</var>)</code><a class="copiable-link" href='#index-strptime'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env locale
| AS-Unsafe heap lock
| AC-Unsafe lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">strptime</code> function parses the input string <var class="var">s</var> according
to the format string <var class="var">fmt</var> and stores its results in the
structure <var class="var">tp</var>.
</p>
<p>The input string could be generated by a <code class="code">strftime</code> call or
obtained any other way.  It does not need to be in a human-recognizable
format; e.g. a date passed as <code class="code">&quot;02:1999:9&quot;</code> is acceptable, even
though it is ambiguous without context.  As long as the format string
<var class="var">fmt</var> matches the input string the function will succeed.
</p>
<p>The user has to make sure, though, that the input can be parsed in a
unambiguous way.  The string <code class="code">&quot;1999112&quot;</code> can be parsed using the
format <code class="code">&quot;%Y%m%d&quot;</code> as 1999-1-12, 1999-11-2, or even 19991-1-2.  It
is necessary to add appropriate separators to reliably get results.
</p>
<p>The format string consists of the same components as the format string
of the <code class="code">strftime</code> function.  The only difference is that the flags
<code class="code">_</code>, <code class="code">-</code>, <code class="code">0</code>, and <code class="code">^</code> are not allowed.
Several of the distinct formats of <code class="code">strftime</code> do the same work in
<code class="code">strptime</code> since differences like case of the input do not matter.
For reasons of symmetry all formats are supported, though.
</p>
<p>The modifiers <code class="code">E</code> and <code class="code">O</code> are also allowed everywhere the
<code class="code">strftime</code> function allows them.
</p>
<p>The formats are:
</p>
<dl class="table">
<dt><code class="code">%a</code></dt>
<dt><code class="code">%A</code></dt>
<dd><p>The weekday name according to the current locale, in abbreviated form or
the full name.
</p>
</dd>
<dt><code class="code">%b</code></dt>
<dt><code class="code">%B</code></dt>
<dt><code class="code">%h</code></dt>
<dd><p>A month name according to the current locale.  All three specifiers
will recognize both abbreviated and full month names.  If the
locale provides two different grammatical forms of month names,
all three specifiers will recognize both forms.
</p>
<p>As a GNU extension, the <code class="code">O</code> modifier can be used with these
specifiers; it has no effect, as both grammatical forms of month
names are recognized.
</p>
</dd>
<dt><code class="code">%c</code></dt>
<dd><p>The date and time representation for the current locale.
</p>
</dd>
<dt><code class="code">%Ec</code></dt>
<dd><p>Like <code class="code">%c</code> but the locale&rsquo;s alternative date and time format is used.
</p>
</dd>
<dt><code class="code">%C</code></dt>
<dd><p>The century of the year.
</p>
<p>It makes sense to use this format only if the format string also
contains the <code class="code">%y</code> format.
</p>
</dd>
<dt><code class="code">%EC</code></dt>
<dd><p>The locale&rsquo;s representation of the period.
</p>
<p>Unlike <code class="code">%C</code> it sometimes makes sense to use this format since some
cultures represent years relative to the beginning of eras instead of
using the Gregorian years.
</p>
</dd>
<dt><code class="code">%d</code></dt>
<dt><code class="code">%e</code></dt>
<dd><p>The day of the month as a decimal number (range <code class="code">1</code> through <code class="code">31</code>).
Leading zeroes are permitted but not required.
</p>
</dd>
<dt><code class="code">%Od</code></dt>
<dt><code class="code">%Oe</code></dt>
<dd><p>Same as <code class="code">%d</code> but using the locale&rsquo;s alternative numeric symbols.
</p>
<p>Leading zeroes are permitted but not required.
</p>
</dd>
<dt><code class="code">%D</code></dt>
<dd><p>Equivalent to <code class="code">%m/%d/%y</code>.
</p>
</dd>
<dt><code class="code">%F</code></dt>
<dd><p>Equivalent to <code class="code">%Y-%m-%d</code>, which is the ISO&nbsp;8601<!-- /@w --> date
format.
</p>
<p>This is a GNU extension following an ISO&nbsp;C99<!-- /@w --> extension to
<code class="code">strftime</code>.
</p>
</dd>
<dt><code class="code">%g</code></dt>
<dd><p>The year corresponding to the ISO week number, but without the century
(range <code class="code">00</code> through <code class="code">99</code>).
</p>
<p><em class="emph">Note:</em> Currently, this is not fully implemented.  The format is
recognized, input is consumed but no field in <var class="var">tm</var> is set.
</p>
<p>This format is a GNU extension following a GNU extension of <code class="code">strftime</code>.
</p>
</dd>
<dt><code class="code">%G</code></dt>
<dd><p>The year corresponding to the ISO week number.
</p>
<p><em class="emph">Note:</em> Currently, this is not fully implemented.  The format is
recognized, input is consumed but no field in <var class="var">tm</var> is set.
</p>
<p>This format is a GNU extension following a GNU extension of <code class="code">strftime</code>.
</p>
</dd>
<dt><code class="code">%H</code></dt>
<dt><code class="code">%k</code></dt>
<dd><p>The hour as a decimal number, using a 24-hour clock (range <code class="code">00</code> through
<code class="code">23</code>).
</p>
<p><code class="code">%k</code> is a GNU extension following a GNU extension of <code class="code">strftime</code>.
</p>
</dd>
<dt><code class="code">%OH</code></dt>
<dd><p>Same as <code class="code">%H</code> but using the locale&rsquo;s alternative numeric symbols.
</p>
</dd>
<dt><code class="code">%I</code></dt>
<dt><code class="code">%l</code></dt>
<dd><p>The hour as a decimal number, using a 12-hour clock (range <code class="code">01</code> through
<code class="code">12</code>).
</p>
<p><code class="code">%l</code> is a GNU extension following a GNU extension of <code class="code">strftime</code>.
</p>
</dd>
<dt><code class="code">%OI</code></dt>
<dd><p>Same as <code class="code">%I</code> but using the locale&rsquo;s alternative numeric symbols.
</p>
</dd>
<dt><code class="code">%j</code></dt>
<dd><p>The day of the year as a decimal number (range <code class="code">1</code> through <code class="code">366</code>).
</p>
<p>Leading zeroes are permitted but not required.
</p>
</dd>
<dt><code class="code">%m</code></dt>
<dd><p>The month as a decimal number (range <code class="code">1</code> through <code class="code">12</code>).
</p>
<p>Leading zeroes are permitted but not required.
</p>
</dd>
<dt><code class="code">%Om</code></dt>
<dd><p>Same as <code class="code">%m</code> but using the locale&rsquo;s alternative numeric symbols.
</p>
</dd>
<dt><code class="code">%M</code></dt>
<dd><p>The minute as a decimal number (range <code class="code">0</code> through <code class="code">59</code>).
</p>
<p>Leading zeroes are permitted but not required.
</p>
</dd>
<dt><code class="code">%OM</code></dt>
<dd><p>Same as <code class="code">%M</code> but using the locale&rsquo;s alternative numeric symbols.
</p>
</dd>
<dt><code class="code">%n</code></dt>
<dt><code class="code">%t</code></dt>
<dd><p>Matches any white space.
</p>
</dd>
<dt><code class="code">%p</code></dt>
<dt><code class="code">%P</code></dt>
<dd><p>The locale-dependent equivalent to &lsquo;<samp class="samp">AM</samp>&rsquo; or &lsquo;<samp class="samp">PM</samp>&rsquo;.
</p>
<p>This format is not useful unless <code class="code">%I</code> or <code class="code">%l</code> is also used.
Another complication is that the locale might not define these values at
all and therefore the conversion fails.
</p>
<p><code class="code">%P</code> is a GNU extension following a GNU extension to <code class="code">strftime</code>.
</p>
</dd>
<dt><code class="code">%r</code></dt>
<dd><p>The complete time using the AM/PM format of the current locale.
</p>
<p>A complication is that the locale might not define this format at all
and therefore the conversion fails.
</p>
</dd>
<dt><code class="code">%R</code></dt>
<dd><p>The hour and minute in decimal numbers using the format <code class="code">%H:%M</code>.
</p>
<p><code class="code">%R</code> is a GNU extension following a GNU extension to <code class="code">strftime</code>.
</p>
</dd>
<dt><code class="code">%s</code></dt>
<dd><p>The number of seconds since the epoch, i.e., since 1970-01-01 00:00:00 UTC.
Leap seconds are not counted unless leap second support is available.
</p>
<p><code class="code">%s</code> is a GNU extension following a GNU extension to <code class="code">strftime</code>.
</p>
</dd>
<dt><code class="code">%S</code></dt>
<dd><p>The seconds as a decimal number (range <code class="code">0</code> through <code class="code">60</code>).
</p>
<p>Leading zeroes are permitted but not required.
</p>
<p><strong class="strong">NB:</strong> The Unix specification says the upper bound on this value
is <code class="code">61</code>, a result of a decision to allow double leap seconds.  You
will not see the value <code class="code">61</code> because no minute has more than one
leap second, but the myth persists.
</p>
</dd>
<dt><code class="code">%OS</code></dt>
<dd><p>Same as <code class="code">%S</code> but using the locale&rsquo;s alternative numeric symbols.
</p>
</dd>
<dt><code class="code">%T</code></dt>
<dd><p>Equivalent to the use of <code class="code">%H:%M:%S</code> in this place.
</p>
</dd>
<dt><code class="code">%u</code></dt>
<dd><p>The day of the week as a decimal number (range <code class="code">1</code> through
<code class="code">7</code>), Monday being <code class="code">1</code>.
</p>
<p>Leading zeroes are permitted but not required.
</p>
<p><em class="emph">Note:</em> Currently, this is not fully implemented.  The format is
recognized, input is consumed but no field in <var class="var">tm</var> is set.
</p>
</dd>
<dt><code class="code">%U</code></dt>
<dd><p>The week number of the current year as a decimal number (range <code class="code">0</code>
through <code class="code">53</code>).
</p>
<p>Leading zeroes are permitted but not required.
</p>
</dd>
<dt><code class="code">%OU</code></dt>
<dd><p>Same as <code class="code">%U</code> but using the locale&rsquo;s alternative numeric symbols.
</p>
</dd>
<dt><code class="code">%V</code></dt>
<dd><p>The ISO&nbsp;8601:1988<!-- /@w --> week number as a decimal number (range <code class="code">1</code>
through <code class="code">53</code>).
</p>
<p>Leading zeroes are permitted but not required.
</p>
<p><em class="emph">Note:</em> Currently, this is not fully implemented.  The format is
recognized, input is consumed but no field in <var class="var">tm</var> is set.
</p>
</dd>
<dt><code class="code">%w</code></dt>
<dd><p>The day of the week as a decimal number (range <code class="code">0</code> through
<code class="code">6</code>), Sunday being <code class="code">0</code>.
</p>
<p>Leading zeroes are permitted but not required.
</p>
<p><em class="emph">Note:</em> Currently, this is not fully implemented.  The format is
recognized, input is consumed but no field in <var class="var">tm</var> is set.
</p>
</dd>
<dt><code class="code">%Ow</code></dt>
<dd><p>Same as <code class="code">%w</code> but using the locale&rsquo;s alternative numeric symbols.
</p>
</dd>
<dt><code class="code">%W</code></dt>
<dd><p>The week number of the current year as a decimal number (range <code class="code">0</code>
through <code class="code">53</code>).
</p>
<p>Leading zeroes are permitted but not required.
</p>
<p><em class="emph">Note:</em> Currently, this is not fully implemented.  The format is
recognized, input is consumed but no field in <var class="var">tm</var> is set.
</p>
</dd>
<dt><code class="code">%OW</code></dt>
<dd><p>Same as <code class="code">%W</code> but using the locale&rsquo;s alternative numeric symbols.
</p>
</dd>
<dt><code class="code">%x</code></dt>
<dd><p>The date using the locale&rsquo;s date format.
</p>
</dd>
<dt><code class="code">%Ex</code></dt>
<dd><p>Like <code class="code">%x</code> but the locale&rsquo;s alternative data representation is used.
</p>
</dd>
<dt><code class="code">%X</code></dt>
<dd><p>The time using the locale&rsquo;s time format.
</p>
</dd>
<dt><code class="code">%EX</code></dt>
<dd><p>Like <code class="code">%X</code> but the locale&rsquo;s alternative time representation is used.
</p>
</dd>
<dt><code class="code">%y</code></dt>
<dd><p>The year without a century as a decimal number (range <code class="code">0</code> through
<code class="code">99</code>).
</p>
<p>Leading zeroes are permitted but not required.
</p>
<p>Note that it is questionable to use this format without
the <code class="code">%C</code> format.  The <code class="code">strptime</code> function does regard input
values in the range <em class="math">68</em> to <em class="math">99</em> as the years <em class="math">1969</em> to
<em class="math">1999</em> and the values <em class="math">0</em> to <em class="math">68</em> as the years
<em class="math">2000</em> to <em class="math">2068</em>.  But maybe this heuristic fails for some
input data.
</p>
<p>Therefore it is best to avoid <code class="code">%y</code> completely and use <code class="code">%Y</code>
instead.
</p>
</dd>
<dt><code class="code">%Ey</code></dt>
<dd><p>The offset from <code class="code">%EC</code> in the locale&rsquo;s alternative representation.
</p>
</dd>
<dt><code class="code">%Oy</code></dt>
<dd><p>The offset of the year (from <code class="code">%C</code>) using the locale&rsquo;s alternative
numeric symbols.
</p>
</dd>
<dt><code class="code">%Y</code></dt>
<dd><p>The year as a decimal number, using the Gregorian calendar.
</p>
</dd>
<dt><code class="code">%EY</code></dt>
<dd><p>The full alternative year representation.
</p>
</dd>
<dt><code class="code">%z</code></dt>
<dd><p>The offset from GMT in ISO&nbsp;8601<!-- /@w -->/RFC822 format.
</p>
</dd>
<dt><code class="code">%Z</code></dt>
<dd><p>The time zone abbreviation.
</p>
<p><em class="emph">Note:</em> Currently, this is not fully implemented.  The format is
recognized, input is consumed but no field in <var class="var">tm</var> is set.
</p>
</dd>
<dt><code class="code">%%</code></dt>
<dd><p>A literal &lsquo;<samp class="samp">%</samp>&rsquo; character.
</p></dd>
</dl>

<p>All other characters in the format string must have a matching character
in the input string.  Exceptions are white spaces in the input string
which can match zero or more whitespace characters in the format string.
</p>
<p><strong class="strong">Portability Note:</strong> The XPG standard advises applications to use
at least one whitespace character (as specified by <code class="code">isspace</code>) or
other non-alphanumeric characters between any two conversion
specifications.  The GNU C Library does not have this limitation but
other libraries might have trouble parsing formats like
<code class="code">&quot;%d%m%Y%H%M%S&quot;</code>.
</p>
<p>The <code class="code">strptime</code> function processes the input string from right to
left.  Each of the three possible input elements (white space, literal,
or format) are handled one after the other.  If the input cannot be
matched to the format string the function stops.  The remainder of the
format and input strings are not processed.
</p>
<p>The function returns a pointer to the first character it was unable to
process.  If the input string contains more characters than required by
the format string the return value points right after the last consumed
input character.  If the whole input string is consumed the return value
points to the <code class="code">NULL</code> byte at the end of the string.  If an error
occurs, i.e., <code class="code">strptime</code> fails to match all of the format string,
the function returns <code class="code">NULL</code>.
</p></dd></dl>

<p>The specification of the function in the XPG standard is rather vague,
leaving out a few important pieces of information.  Most importantly, it
does not specify what happens to those elements of <var class="var">tm</var> which are
not directly initialized by the different formats.  The
implementations on different Unix systems vary here.
</p>
<p>The GNU C Library implementation does not touch those fields which are not
directly initialized.  Exceptions are the <code class="code">tm_wday</code> and
<code class="code">tm_yday</code> elements, which are recomputed if any of the year, month,
or date elements changed.  This has two implications:
</p>
<ul class="itemize mark-bullet">
<li>Before calling the <code class="code">strptime</code> function for a new input string, you
should prepare the <var class="var">tm</var> structure you pass.  Normally this will mean
initializing all values to zero.  Alternatively, you can set all
fields to values like <code class="code">INT_MAX</code>, allowing you to determine which
elements were set by the function call.  Zero does not work here since
it is a valid value for many of the fields.

<p>Careful initialization is necessary if you want to find out whether a
certain field in <var class="var">tm</var> was initialized by the function call.
</p>
</li><li>You can construct a <code class="code">struct tm</code> value with several consecutive
<code class="code">strptime</code> calls.  A useful application of this is e.g. the parsing
of two separate strings, one containing date information and the other
time information.  By parsing one after the other without clearing the
structure in-between, you can construct a complete broken-down time.
</li></ul>

<p>The following example shows a function which parses a string which
contains the date information in either US style or ISO&nbsp;8601<!-- /@w --> form:
</p>
<div class="example smallexample">
<pre class="example-preformatted">const char *
parse_date (const char *input, struct tm *tm)
{
  const char *cp;

  /* <span class="r">First clear the result structure.</span>  */
  memset (tm, '\0', sizeof (*tm));

  /* <span class="r">Try the ISO format first.</span>  */
  cp = strptime (input, &quot;%F&quot;, tm);
  if (cp == NULL)
    {
      /* <span class="r">Does not match.  Try the US form.</span>  */
      cp = strptime (input, &quot;%D&quot;, tm);
    }

  return cp;
}
</pre></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="General-Time-String-Parsing.html">A More User-friendly Way to Parse Times and Dates</a>, Up: <a href="Parsing-Date-and-Time.html">Convert textual time and date information back</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
