<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Line Input (The GNU C Library)</title>

<meta name="description" content="Line Input (The GNU C Library)">
<meta name="keywords" content="Line Input (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="I_002fO-on-Streams.html" rel="up" title="I/O on Streams">
<link href="Unreading.html" rel="next" title="Unreading">
<link href="Character-Input.html" rel="prev" title="Character Input">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Line-Input">
<div class="nav-panel">
<p>
Next: <a href="Unreading.html" accesskey="n" rel="next">Unreading</a>, Previous: <a href="Character-Input.html" accesskey="p" rel="prev">Character Input</a>, Up: <a href="I_002fO-on-Streams.html" accesskey="u" rel="up">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Line_002dOriented-Input">12.9 Line-Oriented Input</h3>

<p>Since many programs interpret input on the basis of lines, it is
convenient to have functions to read a line of text from a stream.
</p>
<p>Standard C has functions to do this, but they aren&rsquo;t very safe: null
characters and even (for <code class="code">gets</code>) long lines can confuse them.  So
the GNU C Library provides the nonstandard <code class="code">getline</code> function that
makes it easy to read lines reliably.
</p>
<p>Another GNU extension, <code class="code">getdelim</code>, generalizes <code class="code">getline</code>.  It
reads a delimited record, defined as everything through the next
occurrence of a specified delimiter character.
</p>
<p>All these functions are declared in <samp class="file">stdio.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getline"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">getline</strong> <code class="def-code-arguments">(char **<var class="var">lineptr</var>, size_t *<var class="var">n</var>, FILE *<var class="var">stream</var>)</code><a class="copiable-link" href='#index-getline'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt heap
| AC-Unsafe lock corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function reads an entire line from <var class="var">stream</var>, storing the text
(including the newline and a terminating null character) in a buffer
and storing the buffer address in <code class="code">*<var class="var">lineptr</var></code>.
</p>
<p>Before calling <code class="code">getline</code>, you should place in <code class="code">*<var class="var">lineptr</var></code>
the address of a buffer <code class="code">*<var class="var">n</var></code> bytes long, allocated with
<code class="code">malloc</code>.  If this buffer is long enough to hold the line,
<code class="code">getline</code> stores the line in this buffer.  Otherwise,
<code class="code">getline</code> makes the buffer bigger using <code class="code">realloc</code>, storing the
new buffer address back in <code class="code">*<var class="var">lineptr</var></code> and the increased size
back in <code class="code">*<var class="var">n</var></code>.
See <a class="xref" href="Unconstrained-Allocation.html">Unconstrained Allocation</a>.
</p>
<p>If you set <code class="code">*<var class="var">lineptr</var></code> to a null pointer, and <code class="code">*<var class="var">n</var></code>
to zero, before the call, then <code class="code">getline</code> allocates the initial
buffer for you by calling <code class="code">malloc</code>.  This buffer remains allocated
even if <code class="code">getline</code> encounters errors and is unable to read any bytes.
</p>
<p>In either case, when <code class="code">getline</code> returns,  <code class="code">*<var class="var">lineptr</var></code> is
a <code class="code">char *</code> which points to the text of the line.
</p>
<p>When <code class="code">getline</code> is successful, it returns the number of characters
read (including the newline, but not including the terminating null).
This value enables you to distinguish null characters that are part of
the line from the null character inserted as a terminator.
</p>
<p>This function is a GNU extension, but it is the recommended way to read
lines from a stream.  The alternative standard functions are unreliable.
</p>
<p>If an error occurs or end of file is reached without any bytes read,
<code class="code">getline</code> returns <code class="code">-1</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getdelim"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">getdelim</strong> <code class="def-code-arguments">(char **<var class="var">lineptr</var>, size_t *<var class="var">n</var>, int <var class="var">delimiter</var>, FILE *<var class="var">stream</var>)</code><a class="copiable-link" href='#index-getdelim'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt heap
| AC-Unsafe lock corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is like <code class="code">getline</code> except that the character which
tells it to stop reading is not necessarily newline.  The argument
<var class="var">delimiter</var> specifies the delimiter character; <code class="code">getdelim</code> keeps
reading until it sees that character (or end of file).
</p>
<p>The text is stored in <var class="var">lineptr</var>, including the delimiter character
and a terminating null.  Like <code class="code">getline</code>, <code class="code">getdelim</code> makes
<var class="var">lineptr</var> bigger if it isn&rsquo;t big enough.
</p>
<p><code class="code">getline</code> is in fact implemented in terms of <code class="code">getdelim</code>, just
like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">ssize_t
getline (char **lineptr, size_t *n, FILE *stream)
{
  return getdelim (lineptr, n, '\n', stream);
}
</pre></div>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fgets"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">fgets</strong> <code class="def-code-arguments">(char *<var class="var">s</var>, int <var class="var">count</var>, FILE *<var class="var">stream</var>)</code><a class="copiable-link" href='#index-fgets'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fgets</code> function reads characters from the stream <var class="var">stream</var>
up to and including a newline character and stores them in the string
<var class="var">s</var>, adding a null character to mark the end of the string.  You
must supply <var class="var">count</var> characters worth of space in <var class="var">s</var>, but the
number of characters read is at most <var class="var">count</var> &minus; 1.  The extra
character space is used to hold the null character at the end of the
string.
</p>
<p>If the system is already at end of file when you call <code class="code">fgets</code>, then
the contents of the array <var class="var">s</var> are unchanged and a null pointer is
returned.  A null pointer is also returned if a read error occurs.
Otherwise, the return value is the pointer <var class="var">s</var>.
</p>
<p><strong class="strong">Warning:</strong>  If the input data has a null character, you can&rsquo;t tell.
So don&rsquo;t use <code class="code">fgets</code> unless you know the data cannot contain a null.
Don&rsquo;t use it to read files edited by the user because, if the user inserts
a null character, you should either handle it properly or print a clear
error message.  We recommend using <code class="code">getline</code> instead of <code class="code">fgets</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fgetws"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">fgetws</strong> <code class="def-code-arguments">(wchar_t *<var class="var">ws</var>, int <var class="var">count</var>, FILE *<var class="var">stream</var>)</code><a class="copiable-link" href='#index-fgetws'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fgetws</code> function reads wide characters from the stream
<var class="var">stream</var> up to and including a newline character and stores them in
the string <var class="var">ws</var>, adding a null wide character to mark the end of the
string.  You must supply <var class="var">count</var> wide characters worth of space in
<var class="var">ws</var>, but the number of characters read is at most <var class="var">count</var>
&minus; 1.  The extra character space is used to hold the null wide
character at the end of the string.
</p>
<p>If the system is already at end of file when you call <code class="code">fgetws</code>, then
the contents of the array <var class="var">ws</var> are unchanged and a null pointer is
returned.  A null pointer is also returned if a read error occurs.
Otherwise, the return value is the pointer <var class="var">ws</var>.
</p>
<p><strong class="strong">Warning:</strong> If the input data has a null wide character (which are
null bytes in the input stream), you can&rsquo;t tell.  So don&rsquo;t use
<code class="code">fgetws</code> unless you know the data cannot contain a null.  Don&rsquo;t use
it to read files edited by the user because, if the user inserts a null
character, you should either handle it properly or print a clear error
message.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fgets_005funlocked"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">fgets_unlocked</strong> <code class="def-code-arguments">(char *<var class="var">s</var>, int <var class="var">count</var>, FILE *<var class="var">stream</var>)</code><a class="copiable-link" href='#index-fgets_005funlocked'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:stream
| AS-Unsafe corrupt
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fgets_unlocked</code> function is equivalent to the <code class="code">fgets</code>
function except that it does not implicitly lock the stream.
</p>
<p>This function is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fgetws_005funlocked"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">fgetws_unlocked</strong> <code class="def-code-arguments">(wchar_t *<var class="var">ws</var>, int <var class="var">count</var>, FILE *<var class="var">stream</var>)</code><a class="copiable-link" href='#index-fgetws_005funlocked'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:stream
| AS-Unsafe corrupt
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">fgetws_unlocked</code> function is equivalent to the <code class="code">fgetws</code>
function except that it does not implicitly lock the stream.
</p>
<p>This function is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-gets"><span class="category-def">Deprecated function: </span><span><code class="def-type">char *</code> <strong class="def-name">gets</strong> <code class="def-code-arguments">(char *<var class="var">s</var>)</code><a class="copiable-link" href='#index-gets'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Unsafe lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The function <code class="code">gets</code> reads characters from the stream <code class="code">stdin</code>
up to the next newline character, and stores them in the string <var class="var">s</var>.
The newline character is discarded (note that this differs from the
behavior of <code class="code">fgets</code>, which copies the newline character into the
string).  If <code class="code">gets</code> encounters a read error or end-of-file, it
returns a null pointer; otherwise it returns <var class="var">s</var>.
</p>
<p><strong class="strong">Warning:</strong> The <code class="code">gets</code> function is <strong class="strong">very dangerous</strong>
because it provides no protection against overflowing the string
<var class="var">s</var>.  The GNU C Library includes it for compatibility only.  You
should <strong class="strong">always</strong> use <code class="code">fgets</code> or <code class="code">getline</code> instead.  To
remind you of this, the linker (if using GNU <code class="code">ld</code>) will issue a
warning whenever you use <code class="code">gets</code>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Unreading.html">Unreading</a>, Previous: <a href="Character-Input.html">Character Input</a>, Up: <a href="I_002fO-on-Streams.html">Input/Output on Streams</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
