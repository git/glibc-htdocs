<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Standard Environment (The GNU C Library)</title>

<meta name="description" content="Standard Environment (The GNU C Library)">
<meta name="keywords" content="Standard Environment (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Environment-Variables.html" rel="up" title="Environment Variables">
<link href="Environment-Access.html" rel="prev" title="Environment Access">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Standard-Environment">
<div class="nav-panel">
<p>
Previous: <a href="Environment-Access.html" accesskey="p" rel="prev">Environment Access</a>, Up: <a href="Environment-Variables.html" accesskey="u" rel="up">Environment Variables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Standard-Environment-Variables">26.4.2 Standard Environment Variables</h4>
<a class="index-entry-id" id="index-standard-environment-variables"></a>

<p>These environment variables have standard meanings.  This doesn&rsquo;t mean
that they are always present in the environment; but if these variables
<em class="emph">are</em> present, they have these meanings.  You shouldn&rsquo;t try to use
these environment variable names for some other purpose.
</p>
<dl class="table">
<dt id='index-HOME-environment-variable'><span><code class="code">HOME</code><a class="copiable-link" href='#index-HOME-environment-variable'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-home-directory"></a>

<p>This is a string representing the user&rsquo;s <em class="dfn">home directory</em>, or
initial default working directory.
</p>
<p>The user can set <code class="code">HOME</code> to any value.
If you need to make sure to obtain the proper home directory
for a particular user, you should not use <code class="code">HOME</code>; instead,
look up the user&rsquo;s name in the user database (see <a class="pxref" href="User-Database.html">User Database</a>).
</p>
<p>For most purposes, it is better to use <code class="code">HOME</code>, precisely because
this lets the user specify the value.
</p>
</dd>
<dt id='index-LOGNAME-environment-variable'><span><code class="code">LOGNAME</code><a class="copiable-link" href='#index-LOGNAME-environment-variable'> &para;</a></span></dt>
<dd>
<p>This is the name that the user used to log in.  Since the value in the
environment can be tweaked arbitrarily, this is not a reliable way to
identify the user who is running a program; a function like
<code class="code">getlogin</code> (see <a class="pxref" href="Who-Logged-In.html">Identifying Who Logged In</a>) is better for that purpose.
</p>
<p>For most purposes, it is better to use <code class="code">LOGNAME</code>, precisely because
this lets the user specify the value.
</p>
</dd>
<dt id='index-PATH-environment-variable'><span><code class="code">PATH</code><a class="copiable-link" href='#index-PATH-environment-variable'> &para;</a></span></dt>
<dd>
<p>A <em class="dfn">path</em> is a sequence of directory names which is used for
searching for a file.  The variable <code class="code">PATH</code> holds a path used
for searching for programs to be run.
</p>
<p>The <code class="code">execlp</code> and <code class="code">execvp</code> functions (see <a class="pxref" href="Executing-a-File.html">Executing a File</a>)
use this environment variable, as do many shells and other utilities
which are implemented in terms of those functions.
</p>
<p>The syntax of a path is a sequence of directory names separated by
colons.  An empty string instead of a directory name stands for the
current directory (see <a class="pxref" href="Working-Directory.html">Working Directory</a>).
</p>
<p>A typical value for this environment variable might be a string like:
</p>
<div class="example smallexample">
<pre class="example-preformatted">:/bin:/etc:/usr/bin:/usr/new/X11:/usr/new:/usr/local/bin
</pre></div>

<p>This means that if the user tries to execute a program named <code class="code">foo</code>,
the system will look for files named <samp class="file">foo</samp>, <samp class="file">/bin/foo</samp>,
<samp class="file">/etc/foo</samp>, and so on.  The first of these files that exists is
the one that is executed.
</p>
</dd>
<dt id='index-TERM-environment-variable'><span><code class="code">TERM</code><a class="copiable-link" href='#index-TERM-environment-variable'> &para;</a></span></dt>
<dd>
<p>This specifies the kind of terminal that is receiving program output.
Some programs can make use of this information to take advantage of
special escape sequences or terminal modes supported by particular kinds
of terminals.  Many programs which use the termcap library
(see <a data-manual="termcap" href="../termcap_html/Finding-a-Terminal-Description.html#Finding-a-Terminal-Description">Find</a> in <cite class="cite">The Termcap Library
Manual</cite>) use the <code class="code">TERM</code> environment variable, for example.
</p>
</dd>
<dt id='index-TZ-environment-variable'><span><code class="code">TZ</code><a class="copiable-link" href='#index-TZ-environment-variable'> &para;</a></span></dt>
<dd>
<p>This specifies the time zone.  See <a class="xref" href="TZ-Variable.html">Specifying the Time Zone with <code class="code">TZ</code></a>, for information about
the format of this string and how it is used.
</p>
</dd>
<dt id='index-LANG-environment-variable-1'><span><code class="code">LANG</code><a class="copiable-link" href='#index-LANG-environment-variable-1'> &para;</a></span></dt>
<dd>
<p>This specifies the default locale to use for attribute categories where
neither <code class="code">LC_ALL</code> nor the specific environment variable for that
category is set.  See <a class="xref" href="Locales.html">Locales and Internationalization</a>, for more information about
locales.
</p>

</dd>
<dt id='index-LC_005fALL-environment-variable-1'><span><code class="code">LC_ALL</code><a class="copiable-link" href='#index-LC_005fALL-environment-variable-1'> &para;</a></span></dt>
<dd>
<p>If this environment variable is set it overrides the selection for all
the locales done using the other <code class="code">LC_*</code> environment variables.  The
value of the other <code class="code">LC_*</code> environment variables is simply ignored
in this case.
</p>
</dd>
<dt id='index-LC_005fCOLLATE-environment-variable'><span><code class="code">LC_COLLATE</code><a class="copiable-link" href='#index-LC_005fCOLLATE-environment-variable'> &para;</a></span></dt>
<dd>
<p>This specifies what locale to use for string sorting.
</p>
</dd>
<dt id='index-LC_005fCTYPE-environment-variable'><span><code class="code">LC_CTYPE</code><a class="copiable-link" href='#index-LC_005fCTYPE-environment-variable'> &para;</a></span></dt>
<dd>
<p>This specifies what locale to use for character sets and character
classification.
</p>
</dd>
<dt id='index-LC_005fMESSAGES-environment-variable-1'><span><code class="code">LC_MESSAGES</code><a class="copiable-link" href='#index-LC_005fMESSAGES-environment-variable-1'> &para;</a></span></dt>
<dd>
<p>This specifies what locale to use for printing messages and to parse
responses.
</p>
</dd>
<dt id='index-LC_005fMONETARY-environment-variable'><span><code class="code">LC_MONETARY</code><a class="copiable-link" href='#index-LC_005fMONETARY-environment-variable'> &para;</a></span></dt>
<dd>
<p>This specifies what locale to use for formatting monetary values.
</p>
</dd>
<dt id='index-LC_005fNUMERIC-environment-variable'><span><code class="code">LC_NUMERIC</code><a class="copiable-link" href='#index-LC_005fNUMERIC-environment-variable'> &para;</a></span></dt>
<dd>
<p>This specifies what locale to use for formatting numbers.
</p>
</dd>
<dt id='index-LC_005fTIME-environment-variable'><span><code class="code">LC_TIME</code><a class="copiable-link" href='#index-LC_005fTIME-environment-variable'> &para;</a></span></dt>
<dd>
<p>This specifies what locale to use for formatting date/time values.
</p>
</dd>
<dt id='index-NLSPATH-environment-variable-1'><span><code class="code">NLSPATH</code><a class="copiable-link" href='#index-NLSPATH-environment-variable-1'> &para;</a></span></dt>
<dd>
<p>This specifies the directories in which the <code class="code">catopen</code> function
looks for message translation catalogs.
</p>
</dd>
<dt id='index-_005fPOSIX_005fOPTION_005fORDER-environment-variable_002e'><span><code class="code">_POSIX_OPTION_ORDER</code><a class="copiable-link" href='#index-_005fPOSIX_005fOPTION_005fORDER-environment-variable_002e'> &para;</a></span></dt>
<dd>
<p>If this environment variable is defined, it suppresses the usual
reordering of command line arguments by <code class="code">getopt</code> and
<code class="code">argp_parse</code>.  See <a class="xref" href="Argument-Syntax.html">Program Argument Syntax Conventions</a>.
</p>
</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Environment-Access.html">Environment Access</a>, Up: <a href="Environment-Variables.html">Environment Variables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
