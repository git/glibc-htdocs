<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Formatted Output Functions (The GNU C Library)</title>

<meta name="description" content="Formatted Output Functions (The GNU C Library)">
<meta name="keywords" content="Formatted Output Functions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Output.html" rel="up" title="Formatted Output">
<link href="Dynamic-Output.html" rel="next" title="Dynamic Output">
<link href="Other-Output-Conversions.html" rel="prev" title="Other Output Conversions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Formatted-Output-Functions">
<div class="nav-panel">
<p>
Next: <a href="Dynamic-Output.html" accesskey="n" rel="next">Dynamically Allocating Formatted Output</a>, Previous: <a href="Other-Output-Conversions.html" accesskey="p" rel="prev">Other Output Conversions</a>, Up: <a href="Formatted-Output.html" accesskey="u" rel="up">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Formatted-Output-Functions-1">12.12.7 Formatted Output Functions</h4>

<p>This section describes how to call <code class="code">printf</code> and related functions.
Prototypes for these functions are in the header file <samp class="file">stdio.h</samp>.
Because these functions take a variable number of arguments, you
<em class="emph">must</em> declare prototypes for them before using them.  Of course,
the easiest way to make sure you have all the right prototypes is to
just include <samp class="file">stdio.h</samp>.
<a class="index-entry-id" id="index-stdio_002eh-6"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-printf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">printf</strong> <code class="def-code-arguments">(const char *<var class="var">template</var>, &hellip;)</code><a class="copiable-link" href='#index-printf'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">printf</code> function prints the optional arguments under the
control of the template string <var class="var">template</var> to the stream
<code class="code">stdout</code>.  It returns the number of characters printed, or a
negative value if there was an output error.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">wprintf</strong> <code class="def-code-arguments">(const wchar_t *<var class="var">template</var>, &hellip;)</code><a class="copiable-link" href='#index-wprintf'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wprintf</code> function prints the optional arguments under the
control of the wide template string <var class="var">template</var> to the stream
<code class="code">stdout</code>.  It returns the number of wide characters printed, or a
negative value if there was an output error.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fprintf</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, const char *<var class="var">template</var>, &hellip;)</code><a class="copiable-link" href='#index-fprintf'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is just like <code class="code">printf</code>, except that the output is
written to the stream <var class="var">stream</var> instead of <code class="code">stdout</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fwprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fwprintf</strong> <code class="def-code-arguments">(FILE *<var class="var">stream</var>, const wchar_t *<var class="var">template</var>, &hellip;)</code><a class="copiable-link" href='#index-fwprintf'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is just like <code class="code">wprintf</code>, except that the output is
written to the stream <var class="var">stream</var> instead of <code class="code">stdout</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sprintf</strong> <code class="def-code-arguments">(char *<var class="var">s</var>, const char *<var class="var">template</var>, &hellip;)</code><a class="copiable-link" href='#index-sprintf'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is like <code class="code">printf</code>, except that the output is stored in the character
array <var class="var">s</var> instead of written to a stream.  A null character is written
to mark the end of the string.
</p>
<p>The <code class="code">sprintf</code> function returns the number of characters stored in
the array <var class="var">s</var>, not including the terminating null character.
</p>
<p>The behavior of this function is undefined if copying takes place
between objects that overlap&mdash;for example, if <var class="var">s</var> is also given
as an argument to be printed under control of the &lsquo;<samp class="samp">%s</samp>&rsquo; conversion.
See <a class="xref" href="Copying-Strings-and-Arrays.html">Copying Strings and Arrays</a>.
</p>
<p><strong class="strong">Warning:</strong> The <code class="code">sprintf</code> function can be <strong class="strong">dangerous</strong>
because it can potentially output more characters than can fit in the
allocation size of the string <var class="var">s</var>.  Remember that the field width
given in a conversion specification is only a <em class="emph">minimum</em> value.
</p>
<p>To avoid this problem, you can use <code class="code">snprintf</code> or <code class="code">asprintf</code>,
described below.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-swprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">swprintf</strong> <code class="def-code-arguments">(wchar_t *<var class="var">ws</var>, size_t <var class="var">size</var>, const wchar_t *<var class="var">template</var>, &hellip;)</code><a class="copiable-link" href='#index-swprintf'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is like <code class="code">wprintf</code>, except that the output is stored in the
wide character array <var class="var">ws</var> instead of written to a stream.  A null
wide character is written to mark the end of the string.  The <var class="var">size</var>
argument specifies the maximum number of characters to produce.  The
trailing null character is counted towards this limit, so you should
allocate at least <var class="var">size</var> wide characters for the string <var class="var">ws</var>.
</p>
<p>The return value is the number of characters generated for the given
input, excluding the trailing null.  If not all output fits into the
provided buffer a negative value is returned, and <code class="code">errno</code> is set to
<code class="code">E2BIG</code>.  (The setting of <code class="code">errno</code> is a GNU extension.)  You
should try again with a bigger output string.  <em class="emph">Note:</em> this is
different from how <code class="code">snprintf</code> handles this situation.
</p>
<p>Note that the corresponding narrow stream function takes fewer
parameters.  <code class="code">swprintf</code> in fact corresponds to the <code class="code">snprintf</code>
function.  Since the <code class="code">sprintf</code> function can be dangerous and should
be avoided the ISO&nbsp;C<!-- /@w --> committee refused to make the same mistake
again and decided to not define a function exactly corresponding to
<code class="code">sprintf</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-snprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">snprintf</strong> <code class="def-code-arguments">(char *<var class="var">s</var>, size_t <var class="var">size</var>, const char *<var class="var">template</var>, &hellip;)</code><a class="copiable-link" href='#index-snprintf'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">snprintf</code> function is similar to <code class="code">sprintf</code>, except that
the <var class="var">size</var> argument specifies the maximum number of characters to
produce.  The trailing null character is counted towards this limit, so
you should allocate at least <var class="var">size</var> characters for the string <var class="var">s</var>.
If <var class="var">size</var> is zero, nothing, not even the null byte, shall be written and
<var class="var">s</var> may be a null pointer.
</p>
<p>The return value is the number of characters which would be generated
for the given input, excluding the trailing null.  If this value is
greater than or equal to <var class="var">size</var>, not all characters from the result have
been stored in <var class="var">s</var>.  If this happens, you should be wary of using
the truncated result as that could lead to security, encoding, or
other bugs in your program (see <a class="pxref" href="Truncating-Strings.html">Truncating Strings while Copying</a>).
Instead, you should try again with a bigger output
string.  Here is an example of doing this:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">/* <span class="r">Construct a message describing the value of a variable</span>
   <span class="r">whose name is <var class="var">name</var> and whose value is <var class="var">value</var>.</span> */
char *
make_message (char *name, char *value)
{
  /* <span class="r">Guess we need no more than 100 bytes of space.</span> */
  size_t size = 100;
  char *buffer = xmalloc (size);
</pre></div><div class="group"><pre class="example-preformatted"> /* <span class="r">Try to print in the allocated space.</span> */
  int buflen = snprintf (buffer, size, &quot;value of %s is %s&quot;,
		         name, value);
  if (! (0 &lt;= buflen &amp;&amp; buflen &lt; SIZE_MAX))
    fatal (&quot;integer overflow&quot;);
</pre></div><div class="group"><pre class="example-preformatted">  if (buflen &gt;= size)
    {
      /* <span class="r">Reallocate buffer now that we know
	 how much space is needed.</span> */
      size = buflen;
      size++;
      buffer = xrealloc (buffer, size);

      /* <span class="r">Try again.</span> */
      snprintf (buffer, size, &quot;value of %s is %s&quot;,
		name, value);
    }
  /* <span class="r">The last call worked, return the string.</span> */
  return buffer;
}
</pre></div></div>

<p>In practice, it is often easier just to use <code class="code">asprintf</code>, below.
</p>
<p><strong class="strong">Attention:</strong> In versions of the GNU C Library prior to 2.1 the
return value is the number of characters stored, not including the
terminating null; unless there was not enough space in <var class="var">s</var> to
store the result in which case <code class="code">-1</code> is returned.  This was
changed in order to comply with the ISO&nbsp;C99<!-- /@w --> standard.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Dynamic-Output.html">Dynamically Allocating Formatted Output</a>, Previous: <a href="Other-Output-Conversions.html">Other Output Conversions</a>, Up: <a href="Formatted-Output.html">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
