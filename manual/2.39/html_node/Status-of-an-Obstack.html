<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Status of an Obstack (The GNU C Library)</title>

<meta name="description" content="Status of an Obstack (The GNU C Library)">
<meta name="keywords" content="Status of an Obstack (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Obstacks.html" rel="up" title="Obstacks">
<link href="Obstacks-Data-Alignment.html" rel="next" title="Obstacks Data Alignment">
<link href="Extra-Fast-Growing.html" rel="prev" title="Extra Fast Growing">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Status-of-an-Obstack">
<div class="nav-panel">
<p>
Next: <a href="Obstacks-Data-Alignment.html" accesskey="n" rel="next">Alignment of Data in Obstacks</a>, Previous: <a href="Extra-Fast-Growing.html" accesskey="p" rel="prev">Extra Fast Growing Objects</a>, Up: <a href="Obstacks.html" accesskey="u" rel="up">Obstacks</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Status-of-an-Obstack-1">3.2.6.8 Status of an Obstack</h4>
<a class="index-entry-id" id="index-obstack-status"></a>
<a class="index-entry-id" id="index-status-of-obstack"></a>

<p>Here are functions that provide information on the current status of
allocation in an obstack.  You can use them to learn about an object while
still growing it.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fbase"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">obstack_base</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>)</code><a class="copiable-link" href='#index-obstack_005fbase'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the tentative address of the beginning of the
currently growing object in <var class="var">obstack-ptr</var>.  If you finish the object
immediately, it will have that address.  If you make it larger first, it
may outgrow the current chunk&mdash;then its address will change!
</p>
<p>If no object is growing, this value says where the next object you
allocate will start (once again assuming it fits in the current
chunk).
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fnext_005ffree"><span class="category-def">Function: </span><span><code class="def-type">void *</code> <strong class="def-name">obstack_next_free</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>)</code><a class="copiable-link" href='#index-obstack_005fnext_005ffree'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the address of the first free byte in the current
chunk of obstack <var class="var">obstack-ptr</var>.  This is the end of the currently
growing object.  If no object is growing, <code class="code">obstack_next_free</code>
returns the same value as <code class="code">obstack_base</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fobject_005fsize-1"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">obstack_object_size</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack-ptr</var>)</code><a class="copiable-link" href='#index-obstack_005fobject_005fsize-1'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack-ptr
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the size in bytes of the currently growing object.
This is equivalent to
</p>
<div class="example smallexample">
<pre class="example-preformatted">obstack_next_free (<var class="var">obstack-ptr</var>) - obstack_base (<var class="var">obstack-ptr</var>)
</pre></div>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Obstacks-Data-Alignment.html">Alignment of Data in Obstacks</a>, Previous: <a href="Extra-Fast-Growing.html">Extra Fast Growing Objects</a>, Up: <a href="Obstacks.html">Obstacks</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
