<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Argp (The GNU C Library)</title>

<meta name="description" content="Argp (The GNU C Library)">
<meta name="keywords" content="Argp (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Parsing-Program-Arguments.html" rel="up" title="Parsing Program Arguments">
<link href="Suboptions.html" rel="next" title="Suboptions">
<link href="Getopt.html" rel="prev" title="Getopt">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Argp">
<div class="nav-panel">
<p>
Next: <a href="Suboptions.html" accesskey="n" rel="next">Parsing of Suboptions</a>, Previous: <a href="Getopt.html" accesskey="p" rel="prev">Parsing program options using <code class="code">getopt</code></a>, Up: <a href="Parsing-Program-Arguments.html" accesskey="u" rel="up">Parsing Program Arguments</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Parsing-Program-Options-with-Argp">26.3 Parsing Program Options with Argp</h3>
<a class="index-entry-id" id="index-argp-_0028program-argument-parser_0029"></a>
<a class="index-entry-id" id="index-argument-parsing-with-argp"></a>
<a class="index-entry-id" id="index-option-parsing-with-argp"></a>

<p><em class="dfn">Argp</em> is an interface for parsing unix-style argument vectors.
See <a class="xref" href="Program-Arguments.html">Program Arguments</a>.
</p>
<p>Argp provides features unavailable in the more commonly used
<code class="code">getopt</code> interface.  These features include automatically producing
output in response to the &lsquo;<samp class="samp">--help</samp>&rsquo; and &lsquo;<samp class="samp">--version</samp>&rsquo; options, as
described in the GNU coding standards.  Using argp makes it less likely
that programmers will neglect to implement these additional options or
keep them up to date.
</p>
<p>Argp also provides the ability to merge several independently defined
option parsers into one, mediating conflicts between them and making the
result appear seamless.  A library can export an argp option parser that
user programs might employ in conjunction with their own option parsers,
resulting in less work for the user programs.  Some programs may use only
argument parsers exported by libraries, thereby achieving consistent and
efficient option-parsing for abstractions implemented by the libraries.
</p>
<a class="index-entry-id" id="index-argp_002eh"></a>
<p>The header file <samp class="file">&lt;argp.h&gt;</samp> should be included to use argp.
</p>
<ul class="mini-toc">
<li><a href="#The-argp_005fparse-Function" accesskey="1">The <code class="code">argp_parse</code> Function</a></li>
<li><a href="Argp-Global-Variables.html" accesskey="2">Argp Global Variables</a></li>
<li><a href="Argp-Parsers.html" accesskey="3">Specifying Argp Parsers</a></li>
<li><a href="Argp-Option-Vectors.html" accesskey="4">Specifying Options in an Argp Parser</a></li>
<li><a href="Argp-Parser-Functions.html" accesskey="5">Argp Parser Functions</a></li>
<li><a href="Argp-Children.html" accesskey="6">Combining Multiple Argp Parsers</a></li>
<li><a href="Argp-Flags.html" accesskey="7">Flags for <code class="code">argp_parse</code></a></li>
<li><a href="Argp-Help-Filtering.html" accesskey="8">Customizing Argp Help Output</a></li>
<li><a href="Argp-Help.html" accesskey="9">The <code class="code">argp_help</code> Function</a></li>
<li><a href="Argp-Help-Flags.html">Flags for the <code class="code">argp_help</code> Function</a></li>
<li><a href="Argp-Examples.html">Argp Examples</a></li>
<li><a href="Argp-User-Customization.html">Argp User Customization</a></li>
<li><a href="Suboptions-Example.html">Parsing of Suboptions Example</a></li>
</ul>
<div class="subsection-level-extent" id="The-argp_005fparse-Function">
<h4 class="subsection">26.3.1 The <code class="code">argp_parse</code> Function</h4>

<p>The main interface to argp is the <code class="code">argp_parse</code> function.  In many
cases, calling <code class="code">argp_parse</code> is the only argument-parsing code
needed in <code class="code">main</code>.
See <a class="xref" href="Program-Arguments.html">Program Arguments</a>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-argp_005fparse"><span class="category-def">Function: </span><span><code class="def-type">error_t</code> <strong class="def-name">argp_parse</strong> <code class="def-code-arguments">(const struct argp *<var class="var">argp</var>, int <var class="var">argc</var>, char **<var class="var">argv</var>, unsigned <var class="var">flags</var>, int *<var class="var">arg_index</var>, void *<var class="var">input</var>)</code><a class="copiable-link" href='#index-argp_005fparse'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:argpbuf locale env
| AS-Unsafe heap i18n lock corrupt
| AC-Unsafe mem lock corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">argp_parse</code> function parses the arguments in <var class="var">argv</var>, of
length <var class="var">argc</var>, using the argp parser <var class="var">argp</var>.  See <a class="xref" href="Argp-Parsers.html">Specifying Argp Parsers</a>.  Passing a null pointer for <var class="var">argp</var> is the same as using
a <code class="code">struct argp</code> containing all zeros.
</p>
<p><var class="var">flags</var> is a set of flag bits that modify the parsing behavior.
See <a class="xref" href="Argp-Flags.html">Flags for <code class="code">argp_parse</code></a>.  <var class="var">input</var> is passed through to the argp parser
<var class="var">argp</var>, and has meaning defined by <var class="var">argp</var>.  A typical usage is
to pass a pointer to a structure which is used for specifying
parameters to the parser and passing back the results.
</p>
<p>Unless the <code class="code">ARGP_NO_EXIT</code> or <code class="code">ARGP_NO_HELP</code> flags are included
in <var class="var">flags</var>, calling <code class="code">argp_parse</code> may result in the program
exiting.  This behavior is true if an error is detected, or when an
unknown option is encountered.  See <a class="xref" href="Program-Termination.html">Program Termination</a>.
</p>
<p>If <var class="var">arg_index</var> is non-null, the index of the first unparsed option
in <var class="var">argv</var> is returned as a value.
</p>
<p>The return value is zero for successful parsing, or an error code
(see <a class="pxref" href="Error-Codes.html">Error Codes</a>) if an error is detected.  Different argp parsers
may return arbitrary error codes, but the standard error codes are:
<code class="code">ENOMEM</code> if a memory allocation error occurred, or <code class="code">EINVAL</code> if
an unknown option or option argument is encountered.
</p></dd></dl>


</div>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Suboptions.html">Parsing of Suboptions</a>, Previous: <a href="Getopt.html">Parsing program options using <code class="code">getopt</code></a>, Up: <a href="Parsing-Program-Arguments.html">Parsing Program Arguments</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
