<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Checking for Errors (The GNU C Library)</title>

<meta name="description" content="Checking for Errors (The GNU C Library)">
<meta name="keywords" content="Checking for Errors (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Error-Reporting.html" rel="up" title="Error Reporting">
<link href="Error-Codes.html" rel="next" title="Error Codes">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span.w-nolinebreak-text {white-space: nowrap}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Checking-for-Errors">
<div class="nav-panel">
<p>
Next: <a href="Error-Codes.html" accesskey="n" rel="next">Error Codes</a>, Up: <a href="Error-Reporting.html" accesskey="u" rel="up">Error Reporting</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Checking-for-Errors-1">2.1 Checking for Errors</h3>

<p>Most library functions return a special value to indicate that they have
failed.  The special value is typically <code class="code">-1</code>, a null pointer, or a
constant such as <code class="code">EOF</code> that is defined for that purpose.  But this
return value tells you only that an error has occurred.  To find out
what kind of error it was, you need to look at the error code stored in the
variable <code class="code">errno</code>.  This variable is declared in the header file
<samp class="file">errno.h</samp>.
<a class="index-entry-id" id="index-errno_002eh-1"></a>
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-errno-1"><span class="category-def">Variable: </span><span><code class="def-type">volatile int</code> <strong class="def-name">errno</strong><a class="copiable-link" href='#index-errno-1'> &para;</a></span></dt>
<dd>
<p>The variable <code class="code">errno</code> contains the system error number.  You can
change the value of <code class="code">errno</code>.
</p>
<p>Since <code class="code">errno</code> is declared <code class="code">volatile</code>, it might be changed
asynchronously by a signal handler; see <a class="ref" href="Defining-Handlers.html">Defining Signal Handlers</a>.
However, a properly written signal handler saves and restores the value
of <code class="code">errno</code>, so you generally do not need to worry about this
possibility except when writing signal handlers.
</p>
<p>The initial value of <code class="code">errno</code> at program startup is zero.  In many
cases, when a library function encounters an error, it will set
<code class="code">errno</code> to a non-zero value to indicate what specific error
condition occurred.  The documentation for each function lists the
error conditions that are possible for that function.  Not all library
functions use this mechanism; some return an error code directly,
instead.
</p>
<p><strong class="strong">Warning:</strong> Many library functions may set <code class="code">errno</code> to some
meaningless non-zero value even if they did not encounter any errors,
and even if they return error codes directly.  Therefore, it is
usually incorrect to check <em class="emph">whether</em> an error occurred by
inspecting the value of <code class="code">errno</code>.  The proper way to check for
error is documented for each function.
</p>
<p><strong class="strong">Portability Note:</strong> ISO&nbsp;C<!-- /@w --> specifies <code class="code">errno</code> as a
&ldquo;modifiable lvalue&rdquo; rather than as a variable, permitting it to be
implemented as a macro.  For example, its expansion might involve a
function call, like <code class="code"><span class="w-nolinebreak-text">*__errno_location</span>&nbsp;()</code><!-- /@w -->.  In fact, that is
what it is
on GNU/Linux and GNU/Hurd systems.  The GNU C Library, on each system, does
whatever is right for the particular system.
</p>
<p>There are a few library functions, like <code class="code">sqrt</code> and <code class="code">atan</code>,
that return a perfectly legitimate value in case of an error, but also
set <code class="code">errno</code>.  For these functions, if you want to check to see
whether an error occurred, the recommended method is to set <code class="code">errno</code>
to zero before calling the function, and then check its value afterward.
</p></dd></dl>

<a class="index-entry-id" id="index-errno_002eh-2"></a>
<p>All the error codes have symbolic names; they are macros defined in
<samp class="file">errno.h</samp>.  The names start with &lsquo;<samp class="samp">E</samp>&rsquo; and an upper-case
letter or digit; you should consider names of this form to be
reserved names.  See <a class="xref" href="Reserved-Names.html">Reserved Names</a>.
</p>
<p>The error code values are all positive integers and are all distinct,
with one exception: <code class="code">EWOULDBLOCK</code> and <code class="code">EAGAIN</code> are the same.
Since the values are distinct, you can use them as labels in a
<code class="code">switch</code> statement; just don&rsquo;t use both <code class="code">EWOULDBLOCK</code> and
<code class="code">EAGAIN</code>.  Your program should not make any other assumptions about
the specific values of these symbolic constants.
</p>
<p>The value of <code class="code">errno</code> doesn&rsquo;t necessarily have to correspond to any
of these macros, since some library functions might return other error
codes of their own for other situations.  The only values that are
guaranteed to be meaningful for a particular library function are the
ones that this manual lists for that function.
</p>
<p>Except on GNU/Hurd systems, almost any system call can return <code class="code">EFAULT</code> if
it is given an invalid pointer as an argument.  Since this could only
happen as a result of a bug in your program, and since it will not
happen on GNU/Hurd systems, we have saved space by not mentioning
<code class="code">EFAULT</code> in the descriptions of individual functions.
</p>
<p>In some Unix systems, many system calls can also return <code class="code">EFAULT</code> if
given as an argument a pointer into the stack, and the kernel for some
obscure reason fails in its attempt to extend the stack.  If this ever
happens, you should probably try using statically or dynamically
allocated memory instead of stack memory on that system.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Error-Codes.html">Error Codes</a>, Up: <a href="Error-Reporting.html">Error Reporting</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
