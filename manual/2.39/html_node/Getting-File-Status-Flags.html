<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Getting File Status Flags (The GNU C Library)</title>

<meta name="description" content="Getting File Status Flags (The GNU C Library)">
<meta name="keywords" content="Getting File Status Flags (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-Status-Flags.html" rel="up" title="File Status Flags">
<link href="Operating-Modes.html" rel="prev" title="Operating Modes">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Getting-File-Status-Flags">
<div class="nav-panel">
<p>
Previous: <a href="Operating-Modes.html" accesskey="p" rel="prev">I/O Operating Modes</a>, Up: <a href="File-Status-Flags.html" accesskey="u" rel="up">File Status Flags</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Getting-and-Setting-File-Status-Flags">13.15.4 Getting and Setting File Status Flags</h4>

<p>The <code class="code">fcntl</code> function can fetch or change file status flags.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-F_005fGETFL-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">F_GETFL</strong><a class="copiable-link" href='#index-F_005fGETFL-1'> &para;</a></span></dt>
<dd>
<p>This macro is used as the <var class="var">command</var> argument to <code class="code">fcntl</code>, to
read the file status flags for the open file with descriptor
<var class="var">filedes</var>.
</p>
<p>The normal return value from <code class="code">fcntl</code> with this command is a
nonnegative number which can be interpreted as the bitwise OR of the
individual flags.  Since the file access modes are not single-bit values,
you can mask off other bits in the returned flags with <code class="code">O_ACCMODE</code>
to compare them.
</p>
<p>In case of an error, <code class="code">fcntl</code> returns <em class="math">-1</em>.  The following
<code class="code">errno</code> error conditions are defined for this command:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is invalid.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-F_005fSETFL-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">F_SETFL</strong><a class="copiable-link" href='#index-F_005fSETFL-1'> &para;</a></span></dt>
<dd>
<p>This macro is used as the <var class="var">command</var> argument to <code class="code">fcntl</code>, to set
the file status flags for the open file corresponding to the
<var class="var">filedes</var> argument.  This command requires a third <code class="code">int</code>
argument to specify the new flags, so the call looks like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">fcntl (<var class="var">filedes</var>, F_SETFL, <var class="var">new-flags</var>)
</pre></div>

<p>You can&rsquo;t change the access mode for the file in this way; that is,
whether the file descriptor was opened for reading or writing.
</p>
<p>The normal return value from <code class="code">fcntl</code> with this command is an
unspecified value other than <em class="math">-1</em>, which indicates an error.  The
error conditions are the same as for the <code class="code">F_GETFL</code> command.
</p></dd></dl>

<p>If you want to modify the file status flags, you should get the current
flags with <code class="code">F_GETFL</code> and modify the value.  Don&rsquo;t assume that the
flags listed here are the only ones that are implemented; your program
may be run years from now and more flags may exist then.  For example,
here is a function to set or clear the flag <code class="code">O_NONBLOCK</code> without
altering any other flags:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">/* <span class="r">Set the <code class="code">O_NONBLOCK</code> flag of <var class="var">desc</var> if <var class="var">value</var> is nonzero,</span>
   <span class="r">or clear the flag if <var class="var">value</var> is 0.</span>
   <span class="r">Return 0 on success, or -1 on error with <code class="code">errno</code> set.</span> */

int
set_nonblock_flag (int desc, int value)
{
  int oldflags = fcntl (desc, F_GETFL, 0);
  /* <span class="r">If reading the flags failed, return error indication now.</span> */
  if (oldflags == -1)
    return -1;
  /* <span class="r">Set just the flag we want to set.</span> */
  if (value != 0)
    oldflags |= O_NONBLOCK;
  else
    oldflags &amp;= ~O_NONBLOCK;
  /* <span class="r">Store modified flag word in the descriptor.</span> */
  return fcntl (desc, F_SETFL, oldflags);
}
</pre></div></div>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Operating-Modes.html">I/O Operating Modes</a>, Up: <a href="File-Status-Flags.html">File Status Flags</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
