<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Argp Parsers (The GNU C Library)</title>

<meta name="description" content="Argp Parsers (The GNU C Library)">
<meta name="keywords" content="Argp Parsers (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Argp.html" rel="up" title="Argp">
<link href="Argp-Flags.html" rel="next" title="Argp Flags">
<link href="Argp-Global-Variables.html" rel="prev" title="Argp Global Variables">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Argp-Parsers">
<div class="nav-panel">
<p>
Next: <a href="Argp-Flags.html" accesskey="n" rel="next">Flags for <code class="code">argp_parse</code></a>, Previous: <a href="Argp-Global-Variables.html" accesskey="p" rel="prev">Argp Global Variables</a>, Up: <a href="Argp.html" accesskey="u" rel="up">Parsing Program Options with Argp</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Specifying-Argp-Parsers">26.3.3 Specifying Argp Parsers</h4>

<p>The first argument to the <code class="code">argp_parse</code> function is a pointer to a
<code class="code">struct argp</code>, which is known as an <em class="dfn">argp parser</em>:
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-argp"><span class="category-def">Data Type: </span><span><strong class="def-name">struct argp</strong><a class="copiable-link" href='#index-struct-argp'> &para;</a></span></dt>
<dd>
<p>This structure specifies how to parse a given set of options and
arguments, perhaps in conjunction with other argp parsers.  It has the
following fields:
</p>
<dl class="table">
<dt><code class="code">const struct argp_option *options</code></dt>
<dd><p>A pointer to a vector of <code class="code">argp_option</code> structures specifying which
options this argp parser understands; it may be zero if there are no
options at all.  See <a class="xref" href="Argp-Option-Vectors.html">Specifying Options in an Argp Parser</a>.
</p>
</dd>
<dt><code class="code">argp_parser_t parser</code></dt>
<dd><p>A pointer to a function that defines actions for this parser; it is
called for each option parsed, and at other well-defined points in the
parsing process.  A value of zero is the same as a pointer to a function
that always returns <code class="code">ARGP_ERR_UNKNOWN</code>.  See <a class="xref" href="Argp-Parser-Functions.html">Argp Parser Functions</a>.
</p>
</dd>
<dt><code class="code">const char *args_doc</code></dt>
<dd><p>If non-zero, a string describing what non-option arguments are called by
this parser.  This is only used to print the &lsquo;<samp class="samp">Usage:</samp>&rsquo; message.  If
it contains newlines, the strings separated by them are considered
alternative usage patterns and printed on separate lines.  Lines after
the first are prefixed by &lsquo;<samp class="samp"> or: </samp>&rsquo; instead of &lsquo;<samp class="samp">Usage:</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">const char *doc</code></dt>
<dd><p>If non-zero, a string containing extra text to be printed before and
after the options in a long help message, with the two sections
separated by a vertical tab (<code class="code">'\v'</code>, <code class="code">'\013'</code>) character.  By
convention, the documentation before the options is just a short string
explaining what the program does.  Documentation printed after the
options describe behavior in more detail.
</p>
</dd>
<dt><code class="code">const struct argp_child *children</code></dt>
<dd><p>A pointer to a vector of <code class="code">argp_child</code> structures.  This pointer
specifies which additional argp parsers should be combined with this
one.  See <a class="xref" href="Argp-Children.html">Combining Multiple Argp Parsers</a>.
</p>
</dd>
<dt><code class="code">char *(*help_filter)(int <var class="var">key</var>, const char *<var class="var">text</var>, void *<var class="var">input</var>)</code></dt>
<dd><p>If non-zero, a pointer to a function that filters the output of help
messages.  See <a class="xref" href="Argp-Help-Filtering.html">Customizing Argp Help Output</a>.
</p>
</dd>
<dt><code class="code">const char *argp_domain</code></dt>
<dd><p>If non-zero, the strings used in the argp library are translated using
the domain described by this string.  If zero, the current default domain
is used.
</p>
</dd>
</dl>
</dd></dl>

<p>Of the above group, <code class="code">options</code>, <code class="code">parser</code>, <code class="code">args_doc</code>, and
the <code class="code">doc</code> fields are usually all that are needed.  If an argp
parser is defined as an initialized C variable, only the fields used
need be specified in the initializer.  The rest will default to zero due
to the way C structure initialization works.  This design is exploited in
most argp structures; the most-used fields are grouped near the
beginning, the unused fields left unspecified.
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Argp-Flags.html">Flags for <code class="code">argp_parse</code></a>, Previous: <a href="Argp-Global-Variables.html">Argp Global Variables</a>, Up: <a href="Argp.html">Parsing Program Options with Argp</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
