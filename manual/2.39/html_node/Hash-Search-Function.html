<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Hash Search Function (The GNU C Library)</title>

<meta name="description" content="Hash Search Function (The GNU C Library)">
<meta name="keywords" content="Hash Search Function (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Searching-and-Sorting.html" rel="up" title="Searching and Sorting">
<link href="Tree-Search-Function.html" rel="next" title="Tree Search Function">
<link href="Search_002fSort-Example.html" rel="prev" title="Search/Sort Example">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Hash-Search-Function">
<div class="nav-panel">
<p>
Next: <a href="Tree-Search-Function.html" accesskey="n" rel="next">The <code class="code">tsearch</code> function.</a>, Previous: <a href="Search_002fSort-Example.html" accesskey="p" rel="prev">Searching and Sorting Example</a>, Up: <a href="Searching-and-Sorting.html" accesskey="u" rel="up">Searching and Sorting</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="The-hsearch-function_002e">9.5 The <code class="code">hsearch</code> function.</h3>

<p>The functions mentioned so far in this chapter are for searching in a sorted
or unsorted array.  There are other methods to organize information
which later should be searched.  The costs of insert, delete and search
differ.  One possible implementation is using hashing tables.
The following functions are declared in the header file <samp class="file">search.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-hcreate"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">hcreate</strong> <code class="def-code-arguments">(size_t <var class="var">nel</var>)</code><a class="copiable-link" href='#index-hcreate'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:hsearch
| AS-Unsafe heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">hcreate</code> function creates a hashing table which can contain at
least <var class="var">nel</var> elements.  There is no possibility to grow this table so
it is necessary to choose the value for <var class="var">nel</var> wisely.  The method
used to implement this function might make it necessary to make the
number of elements in the hashing table larger than the expected maximal
number of elements.  Hashing tables usually work inefficiently if they are
filled 80% or more.  The constant access time guaranteed by hashing can
only be achieved if few collisions exist.  See Knuth&rsquo;s &ldquo;The Art of
Computer Programming, Part 3: Searching and Sorting&rdquo; for more
information.
</p>
<p>The weakest aspect of this function is that there can be at most one
hashing table used through the whole program.  The table is allocated
in local memory out of control of the programmer.  As an extension the GNU C Library
provides an additional set of functions with a reentrant
interface which provides a similar interface but which allows keeping
arbitrarily many hashing tables.
</p>
<p>It is possible to use more than one hashing table in the program run if
the former table is first destroyed by a call to <code class="code">hdestroy</code>.
</p>
<p>The function returns a non-zero value if successful.  If it returns zero,
something went wrong.  This could either mean there is already a hashing
table in use or the program ran out of memory.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-hdestroy"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">hdestroy</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href='#index-hdestroy'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:hsearch
| AS-Unsafe heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">hdestroy</code> function can be used to free all the resources
allocated in a previous call of <code class="code">hcreate</code>.  After a call to this
function it is again possible to call <code class="code">hcreate</code> and allocate a new
table with possibly different size.
</p>
<p>It is important to remember that the elements contained in the hashing
table at the time <code class="code">hdestroy</code> is called are <em class="emph">not</em> freed by this
function.  It is the responsibility of the program code to free those
strings (if necessary at all).  Freeing all the element memory is not
possible without extra, separately kept information since there is no
function to iterate through all available elements in the hashing table.
If it is really necessary to free a table and all elements the
programmer has to keep a list of all table elements and before calling
<code class="code">hdestroy</code> s/he has to free all element&rsquo;s data using this list.
This is a very unpleasant mechanism and it also shows that this kind of
hashing table is mainly meant for tables which are created once and
used until the end of the program run.
</p></dd></dl>

<p>Entries of the hashing table and keys for the search are defined using
this type:
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-ENTRY"><span class="category-def">Data type: </span><span><strong class="def-name">ENTRY</strong><a class="copiable-link" href='#index-ENTRY'> &para;</a></span></dt>
<dd><dl class="table">
<dt><code class="code">char *key</code></dt>
<dd><p>Pointer to a zero-terminated string of characters describing the key for
the search or the element in the hashing table.
</p>
<p>This is a limiting restriction of the functionality of the
<code class="code">hsearch</code> functions: They can only be used for data sets which
use the NUL character always and solely to terminate keys.  It is not
possible to handle general binary data for keys.
</p>
</dd>
<dt><code class="code">void *data</code></dt>
<dd><p>Generic pointer for use by the application.  The hashing table
implementation preserves this pointer in entries, but does not use it
in any way otherwise.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-struct-entry"><span class="category-def">Data type: </span><span><strong class="def-name">struct entry</strong><a class="copiable-link" href='#index-struct-entry'> &para;</a></span></dt>
<dd><p>The underlying type of <code class="code">ENTRY</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-hsearch"><span class="category-def">Function: </span><span><code class="def-type">ENTRY *</code> <strong class="def-name">hsearch</strong> <code class="def-code-arguments">(ENTRY <var class="var">item</var>, ACTION <var class="var">action</var>)</code><a class="copiable-link" href='#index-hsearch'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:hsearch
| AS-Unsafe 
| AC-Unsafe corrupt/action==ENTER
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>To search in a hashing table created using <code class="code">hcreate</code> the
<code class="code">hsearch</code> function must be used.  This function can perform a simple
search for an element (if <var class="var">action</var> has the value <code class="code">FIND</code>) or it can
alternatively insert the key element into the hashing table.  Entries
are never replaced.
</p>
<p>The key is denoted by a pointer to an object of type <code class="code">ENTRY</code>.  For
locating the corresponding position in the hashing table only the
<code class="code">key</code> element of the structure is used.
</p>
<p>If an entry with a matching key is found the <var class="var">action</var> parameter is
irrelevant.  The found entry is returned.  If no matching entry is found
and the <var class="var">action</var> parameter has the value <code class="code">FIND</code> the function
returns a <code class="code">NULL</code> pointer.  If no entry is found and the
<var class="var">action</var> parameter has the value <code class="code">ENTER</code> a new entry is added
to the hashing table which is initialized with the parameter <var class="var">item</var>.
A pointer to the newly added entry is returned.
</p></dd></dl>

<p>As mentioned before, the hashing table used by the functions described so
far is global and there can be at any time at most one hashing table in
the program.  A solution is to use the following functions which are a
GNU extension.  All have in common that they operate on a hashing table
which is described by the content of an object of the type <code class="code">struct
hsearch_data</code>.  This type should be treated as opaque, none of its
members should be changed directly.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-hcreate_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">hcreate_r</strong> <code class="def-code-arguments">(size_t <var class="var">nel</var>, struct hsearch_data *<var class="var">htab</var>)</code><a class="copiable-link" href='#index-hcreate_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:htab
| AS-Unsafe heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>



<p>The <code class="code">hcreate_r</code> function initializes the object pointed to by
<var class="var">htab</var> to contain a hashing table with at least <var class="var">nel</var> elements.
So this function is equivalent to the <code class="code">hcreate</code> function except
that the initialized data structure is controlled by the user.
</p>
<p>This allows having more than one hashing table at one time.  The memory
necessary for the <code class="code">struct hsearch_data</code> object can be allocated
dynamically.  It must be initialized with zero before calling this
function.
</p>
<p>The return value is non-zero if the operation was successful.  If the
return value is zero, something went wrong, which probably means the
program ran out of memory.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-hdestroy_005fr"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">hdestroy_r</strong> <code class="def-code-arguments">(struct hsearch_data *<var class="var">htab</var>)</code><a class="copiable-link" href='#index-hdestroy_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:htab
| AS-Unsafe heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">hdestroy_r</code> function frees all resources allocated by the
<code class="code">hcreate_r</code> function for this very same object <var class="var">htab</var>.  As for
<code class="code">hdestroy</code> it is the program&rsquo;s responsibility to free the strings
for the elements of the table.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-hsearch_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">hsearch_r</strong> <code class="def-code-arguments">(ENTRY <var class="var">item</var>, ACTION <var class="var">action</var>, ENTRY **<var class="var">retval</var>, struct hsearch_data *<var class="var">htab</var>)</code><a class="copiable-link" href='#index-hsearch_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:htab
| AS-Safe 
| AC-Unsafe corrupt/action==ENTER
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">hsearch_r</code> function is equivalent to <code class="code">hsearch</code>.  The
meaning of the first two arguments is identical.  But instead of
operating on a single global hashing table the function works on the
table described by the object pointed to by <var class="var">htab</var> (which is
initialized by a call to <code class="code">hcreate_r</code>).
</p>
<p>Another difference to <code class="code">hcreate</code> is that the pointer to the found
entry in the table is not the return value of the function.  It is
returned by storing it in a pointer variable pointed to by the
<var class="var">retval</var> parameter.  The return value of the function is an integer
value indicating success if it is non-zero and failure if it is zero.
In the latter case the global variable <code class="code">errno</code> signals the reason for
the failure.
</p>
<dl class="table">
<dt><code class="code">ENOMEM</code></dt>
<dd><p>The table is filled and <code class="code">hsearch_r</code> was called with a so far
unknown key and <var class="var">action</var> set to <code class="code">ENTER</code>.
</p></dd>
<dt><code class="code">ESRCH</code></dt>
<dd><p>The <var class="var">action</var> parameter is <code class="code">FIND</code> and no corresponding element
is found in the table.
</p></dd>
</dl>
</dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Tree-Search-Function.html">The <code class="code">tsearch</code> function.</a>, Previous: <a href="Search_002fSort-Example.html">Searching and Sorting Example</a>, Up: <a href="Searching-and-Sorting.html">Searching and Sorting</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
