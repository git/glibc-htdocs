<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Scatter-Gather (The GNU C Library)</title>

<meta name="description" content="Scatter-Gather (The GNU C Library)">
<meta name="keywords" content="Scatter-Gather (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Low_002dLevel-I_002fO.html" rel="up" title="Low-Level I/O">
<link href="Copying-File-Data.html" rel="next" title="Copying File Data">
<link href="Stream_002fDescriptor-Precautions.html" rel="prev" title="Stream/Descriptor Precautions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Scatter_002dGather">
<div class="nav-panel">
<p>
Next: <a href="Copying-File-Data.html" accesskey="n" rel="next">Copying data between two files</a>, Previous: <a href="Stream_002fDescriptor-Precautions.html" accesskey="p" rel="prev">Dangers of Mixing Streams and Descriptors</a>, Up: <a href="Low_002dLevel-I_002fO.html" accesskey="u" rel="up">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Fast-Scatter_002dGather-I_002fO">13.6 Fast Scatter-Gather I/O</h3>
<a class="index-entry-id" id="index-scatter_002dgather"></a>

<p>Some applications may need to read or write data to multiple buffers,
which are separated in memory.  Although this can be done easily enough
with multiple calls to <code class="code">read</code> and <code class="code">write</code>, it is inefficient
because there is overhead associated with each kernel call.
</p>
<p>Instead, many platforms provide special high-speed primitives to perform
these <em class="dfn">scatter-gather</em> operations in a single kernel call.  The GNU C Library
will provide an emulation on any system that lacks these
primitives, so they are not a portability threat.  They are defined in
<code class="code">sys/uio.h</code>.
</p>
<p>These functions are controlled with arrays of <code class="code">iovec</code> structures,
which describe the location and size of each buffer.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-iovec"><span class="category-def">Data Type: </span><span><strong class="def-name">struct iovec</strong><a class="copiable-link" href='#index-struct-iovec'> &para;</a></span></dt>
<dd>

<p>The <code class="code">iovec</code> structure describes a buffer.  It contains two fields:
</p>
<dl class="table">
<dt><code class="code">void *iov_base</code></dt>
<dd><p>Contains the address of a buffer.
</p>
</dd>
<dt><code class="code">size_t iov_len</code></dt>
<dd><p>Contains the length of the buffer.
</p>
</dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-readv"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">readv</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, const struct iovec *<var class="var">vector</var>, int <var class="var">count</var>)</code><a class="copiable-link" href='#index-readv'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">readv</code> function reads data from <var class="var">filedes</var> and scatters it
into the buffers described in <var class="var">vector</var>, which is taken to be
<var class="var">count</var> structures long.  As each buffer is filled, data is sent to the
next.
</p>
<p>Note that <code class="code">readv</code> is not guaranteed to fill all the buffers.
It may stop at any point, for the same reasons <code class="code">read</code> would.
</p>
<p>The return value is a count of bytes (<em class="emph">not</em> buffers) read, <em class="math">0</em>
indicating end-of-file, or <em class="math">-1</em> indicating an error.  The possible
errors are the same as in <code class="code">read</code>.
</p>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-writev"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">writev</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, const struct iovec *<var class="var">vector</var>, int <var class="var">count</var>)</code><a class="copiable-link" href='#index-writev'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">writev</code> function gathers data from the buffers described in
<var class="var">vector</var>, which is taken to be <var class="var">count</var> structures long, and writes
them to <code class="code">filedes</code>.  As each buffer is written, it moves on to the
next.
</p>
<p>Like <code class="code">readv</code>, <code class="code">writev</code> may stop midstream under the same
conditions <code class="code">write</code> would.
</p>
<p>The return value is a count of bytes written, or <em class="math">-1</em> indicating an
error.  The possible errors are the same as in <code class="code">write</code>.
</p>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-preadv"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">preadv</strong> <code class="def-code-arguments">(int <var class="var">fd</var>, const struct iovec *<var class="var">iov</var>, int <var class="var">iovcnt</var>, off_t <var class="var">offset</var>)</code><a class="copiable-link" href='#index-preadv'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function is similar to the <code class="code">readv</code> function, with the difference
it adds an extra <var class="var">offset</var> parameter of type <code class="code">off_t</code> similar to
<code class="code">pread</code>.  The data is read from the file starting at position
<var class="var">offset</var>.  The position of the file descriptor itself is not affected
by the operation.  The value is the same as before the call.
</p>
<p>When the source file is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> the
<code class="code">preadv</code> function is in fact <code class="code">preadv64</code> and the type
<code class="code">off_t</code> has 64 bits, which makes it possible to handle files up to
2^63 bytes in length.
</p>
<p>The return value is a count of bytes (<em class="emph">not</em> buffers) read, <em class="math">0</em>
indicating end-of-file, or <em class="math">-1</em> indicating an error.  The possible
errors are the same as in <code class="code">readv</code> and <code class="code">pread</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-preadv64"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">preadv64</strong> <code class="def-code-arguments">(int <var class="var">fd</var>, const struct iovec *<var class="var">iov</var>, int <var class="var">iovcnt</var>, off64_t <var class="var">offset</var>)</code><a class="copiable-link" href='#index-preadv64'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function is similar to the <code class="code">preadv</code> function with the difference
is that the <var class="var">offset</var> parameter is of type <code class="code">off64_t</code> instead of
<code class="code">off_t</code>.  It makes it possible on 32 bit machines to address
files larger than 2^31 bytes and up to 2^63 bytes.  The
file descriptor <code class="code">filedes</code> must be opened using <code class="code">open64</code> since
otherwise the large offsets possible with <code class="code">off64_t</code> will lead to
errors with a descriptor in small file mode.
</p>
<p>When the source file is compiled using <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit machine this function is actually available under the name
<code class="code">preadv</code> and so transparently replaces the 32 bit interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pwritev"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">pwritev</strong> <code class="def-code-arguments">(int <var class="var">fd</var>, const struct iovec *<var class="var">iov</var>, int <var class="var">iovcnt</var>, off_t <var class="var">offset</var>)</code><a class="copiable-link" href='#index-pwritev'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function is similar to the <code class="code">writev</code> function, with the difference
it adds an extra <var class="var">offset</var> parameter of type <code class="code">off_t</code> similar to
<code class="code">pwrite</code>.  The data is written to the file starting at position
<var class="var">offset</var>.  The position of the file descriptor itself is not affected
by the operation.  The value is the same as before the call.
</p>
<p>However, on Linux, if a file is opened with <code class="code">O_APPEND</code>,  <code class="code">pwrite</code>
appends data to the end of the file, regardless of the value of
<code class="code">offset</code>.
</p>
<p>When the source file is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> the
<code class="code">pwritev</code> function is in fact <code class="code">pwritev64</code> and the type
<code class="code">off_t</code> has 64 bits, which makes it possible to handle files up to
2^63 bytes in length.
</p>
<p>The return value is a count of bytes (<em class="emph">not</em> buffers) written, <em class="math">0</em>
indicating end-of-file, or <em class="math">-1</em> indicating an error.  The possible
errors are the same as in <code class="code">writev</code> and <code class="code">pwrite</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pwritev64"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">pwritev64</strong> <code class="def-code-arguments">(int <var class="var">fd</var>, const struct iovec *<var class="var">iov</var>, int <var class="var">iovcnt</var>, off64_t <var class="var">offset</var>)</code><a class="copiable-link" href='#index-pwritev64'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function is similar to the <code class="code">pwritev</code> function with the difference
is that the <var class="var">offset</var> parameter is of type <code class="code">off64_t</code> instead of
<code class="code">off_t</code>.  It makes it possible on 32 bit machines to address
files larger than 2^31 bytes and up to 2^63 bytes.  The
file descriptor <code class="code">filedes</code> must be opened using <code class="code">open64</code> since
otherwise the large offsets possible with <code class="code">off64_t</code> will lead to
errors with a descriptor in small file mode.
</p>
<p>When the source file is compiled using <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit machine this function is actually available under the name
<code class="code">pwritev</code> and so transparently replaces the 32 bit interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-preadv2"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">preadv2</strong> <code class="def-code-arguments">(int <var class="var">fd</var>, const struct iovec *<var class="var">iov</var>, int <var class="var">iovcnt</var>, off_t <var class="var">offset</var>, int <var class="var">flags</var>)</code><a class="copiable-link" href='#index-preadv2'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function is similar to the <code class="code">preadv</code> function, with the
difference it adds an extra <var class="var">flags</var> parameter of type <code class="code">int</code>.
Additionally, if <var class="var">offset</var> is <em class="math">-1</em>, the current file position
is used and updated (like the <code class="code">readv</code> function).
</p>
<p>The supported <var class="var">flags</var> are dependent of the underlying system.  For
Linux it supports:
</p>
<dl class="vtable">
<dt id='index-RWF_005fHIPRI'><span><code class="code">RWF_HIPRI</code><a class="copiable-link" href='#index-RWF_005fHIPRI'> &para;</a></span></dt>
<dd><p>High priority request.  This adds a flag that tells the file system that
this is a high priority request for which it is worth to poll the hardware.
The flag is purely advisory and can be ignored if not supported.  The
<var class="var">fd</var> must be opened using <code class="code">O_DIRECT</code>.
</p>
</dd>
<dt id='index-RWF_005fDSYNC'><span><code class="code">RWF_DSYNC</code><a class="copiable-link" href='#index-RWF_005fDSYNC'> &para;</a></span></dt>
<dd><p>Per-IO synchronization as if the file was opened with <code class="code">O_DSYNC</code> flag.
</p>
</dd>
<dt id='index-RWF_005fSYNC'><span><code class="code">RWF_SYNC</code><a class="copiable-link" href='#index-RWF_005fSYNC'> &para;</a></span></dt>
<dd><p>Per-IO synchronization as if the file was opened with <code class="code">O_SYNC</code> flag.
</p>
</dd>
<dt id='index-RWF_005fNOWAIT'><span><code class="code">RWF_NOWAIT</code><a class="copiable-link" href='#index-RWF_005fNOWAIT'> &para;</a></span></dt>
<dd><p>Use nonblocking mode for this operation; that is, this call to <code class="code">preadv2</code>
will fail and set <code class="code">errno</code> to <code class="code">EAGAIN</code> if the operation would block.
</p>
</dd>
<dt id='index-RWF_005fAPPEND'><span><code class="code">RWF_APPEND</code><a class="copiable-link" href='#index-RWF_005fAPPEND'> &para;</a></span></dt>
<dd><p>Per-IO synchronization as if the file was opened with <code class="code">O_APPEND</code> flag.
</p></dd>
</dl>

<p>When the source file is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> the
<code class="code">preadv2</code> function is in fact <code class="code">preadv64v2</code> and the type
<code class="code">off_t</code> has 64 bits, which makes it possible to handle files up to
2^63 bytes in length.
</p>
<p>The return value is a count of bytes (<em class="emph">not</em> buffers) read, <em class="math">0</em>
indicating end-of-file, or <em class="math">-1</em> indicating an error.  The possible
errors are the same as in <code class="code">preadv</code> with the addition of:
</p>
<dl class="table">
<dt><code class="code">EOPNOTSUPP</code></dt>
<dd>
<p>An unsupported <var class="var">flags</var> was used.
</p>
</dd>
</dl>

</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-preadv64v2"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">preadv64v2</strong> <code class="def-code-arguments">(int <var class="var">fd</var>, const struct iovec *<var class="var">iov</var>, int <var class="var">iovcnt</var>, off64_t <var class="var">offset</var>, int <var class="var">flags</var>)</code><a class="copiable-link" href='#index-preadv64v2'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function is similar to the <code class="code">preadv2</code> function with the difference
is that the <var class="var">offset</var> parameter is of type <code class="code">off64_t</code> instead of
<code class="code">off_t</code>.  It makes it possible on 32 bit machines to address
files larger than 2^31 bytes and up to 2^63 bytes.  The
file descriptor <code class="code">filedes</code> must be opened using <code class="code">open64</code> since
otherwise the large offsets possible with <code class="code">off64_t</code> will lead to
errors with a descriptor in small file mode.
</p>
<p>When the source file is compiled using <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit machine this function is actually available under the name
<code class="code">preadv2</code> and so transparently replaces the 32 bit interface.
</p></dd></dl>


<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pwritev2"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">pwritev2</strong> <code class="def-code-arguments">(int <var class="var">fd</var>, const struct iovec *<var class="var">iov</var>, int <var class="var">iovcnt</var>, off_t <var class="var">offset</var>, int <var class="var">flags</var>)</code><a class="copiable-link" href='#index-pwritev2'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function is similar to the <code class="code">pwritev</code> function, with the
difference it adds an extra <var class="var">flags</var> parameter of type <code class="code">int</code>.
Additionally, if <var class="var">offset</var> is <em class="math">-1</em>, the current file position
should is used and updated (like the <code class="code">writev</code> function).
</p>
<p>The supported <var class="var">flags</var> are dependent of the underlying system.  For
Linux, the supported flags are the same as those for <code class="code">preadv2</code>.
</p>
<p>When the source file is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> the
<code class="code">pwritev2</code> function is in fact <code class="code">pwritev64v2</code> and the type
<code class="code">off_t</code> has 64 bits, which makes it possible to handle files up to
2^63 bytes in length.
</p>
<p>The return value is a count of bytes (<em class="emph">not</em> buffers) write, <em class="math">0</em>
indicating end-of-file, or <em class="math">-1</em> indicating an error.  The possible
errors are the same as in <code class="code">preadv2</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pwritev64v2"><span class="category-def">Function: </span><span><code class="def-type">ssize_t</code> <strong class="def-name">pwritev64v2</strong> <code class="def-code-arguments">(int <var class="var">fd</var>, const struct iovec *<var class="var">iov</var>, int <var class="var">iovcnt</var>, off64_t <var class="var">offset</var>, int <var class="var">flags</var>)</code><a class="copiable-link" href='#index-pwritev64v2'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function is similar to the <code class="code">pwritev2</code> function with the difference
is that the <var class="var">offset</var> parameter is of type <code class="code">off64_t</code> instead of
<code class="code">off_t</code>.  It makes it possible on 32 bit machines to address
files larger than 2^31 bytes and up to 2^63 bytes.  The
file descriptor <code class="code">filedes</code> must be opened using <code class="code">open64</code> since
otherwise the large offsets possible with <code class="code">off64_t</code> will lead to
errors with a descriptor in small file mode.
</p>
<p>When the source file is compiled using <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bit machine this function is actually available under the name
<code class="code">pwritev2</code> and so transparently replaces the 32 bit interface.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Copying-File-Data.html">Copying data between two files</a>, Previous: <a href="Stream_002fDescriptor-Precautions.html">Dangers of Mixing Streams and Descriptors</a>, Up: <a href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
