<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Getting the Time (The GNU C Library)</title>

<meta name="description" content="Getting the Time (The GNU C Library)">
<meta name="keywords" content="Getting the Time (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Calendar-Time.html" rel="up" title="Calendar Time">
<link href="Setting-and-Adjusting-the-Time.html" rel="next" title="Setting and Adjusting the Time">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span.w-nolinebreak-text {white-space: nowrap}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Getting-the-Time">
<div class="nav-panel">
<p>
Next: <a href="Setting-and-Adjusting-the-Time.html" accesskey="n" rel="next">Setting and Adjusting the Time</a>, Up: <a href="Calendar-Time.html" accesskey="u" rel="up">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Getting-the-Time-1">22.5.1 Getting the Time</h4>

<p>The GNU C Library provides several functions for getting the current
calendar time, with different levels of resolution.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-time-1"><span class="category-def">Function: </span><span><code class="def-type">time_t</code> <strong class="def-name">time</strong> <code class="def-code-arguments">(time_t *<var class="var">result</var>)</code><a class="copiable-link" href='#index-time-1'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is the simplest function for getting the current calendar time.
It returns the calendar time as a value of type <code class="code">time_t</code>; on
POSIX systems, that means it has a resolution of one second.  It
uses the same clock as &lsquo;<samp class="samp"><span class="w-nolinebreak-text">clock_gettime</span>&nbsp;<span class="w-nolinebreak-text">(CLOCK_REALTIME_COARSE)</span></samp>&rsquo;<!-- /@w -->,
when the clock is available or &lsquo;<samp class="samp"><span class="w-nolinebreak-text">clock_gettime</span>&nbsp;<span class="w-nolinebreak-text">(CLOCK_REALTIME)</span></samp>&rsquo;<!-- /@w -->
otherwise.
</p>
<p>If the argument <var class="var">result</var> is not a null pointer, the calendar time
value is also stored in <code class="code">*<var class="var">result</var></code>.
</p>
<p>This function cannot fail.
</p></dd></dl>

<p>Some applications need more precise timekeeping than is possible with
a <code class="code">time_t</code> alone.  Some applications also need more control over
what is meant by &ldquo;the current time.&rdquo;  For these applications, POSIX
provides a function <code class="code">clock_gettime</code> that can retrieve the time
with up to nanosecond precision, from a variety of different clocks.
Clocks can be system-wide, measuring time the same for all processes;
or they can be per-process or per-thread, measuring CPU time consumed
by a particular process, or some other similar resource.  Each clock
has its own resolution and epoch.  You can find the resolution of a
clock with the function <code class="code">clock_getres</code>.  There is no function to
get the epoch for a clock; either it is fixed and documented, or the
clock is not meant to be used to measure absolute times.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-clockid_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">clockid_t</strong><a class="copiable-link" href='#index-clockid_005ft'> &para;</a></span></dt>
<dd>
<p>The type <code class="code">clockid_t</code> is used for constants that indicate which of
several system clocks one wishes to use.
</p></dd></dl>

<p>All systems that support this family of functions will define at least
this clock constant:
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-CLOCK_005fREALTIME"><span class="category-def">Macro: </span><span><code class="def-type">clockid_t</code> <strong class="def-name">CLOCK_REALTIME</strong><a class="copiable-link" href='#index-CLOCK_005fREALTIME'> &para;</a></span></dt>
<dd>
<p>This clock uses the POSIX epoch, 00:00:00 on January 1, 1970, Coordinated
Universal Time. It is close to, but not necessarily in lock-step with, the
clocks of <code class="code">time</code> (above) and of <code class="code">gettimeofday</code> (below).
</p></dd></dl>

<a class="index-entry-id" id="index-monotonic-time"></a>
<p>A second clock constant which is not universal, but still very common,
is for a clock measuring <em class="dfn">monotonic time</em>.  Monotonic time is
useful for measuring elapsed times, because it guarantees that those
measurements are not affected by changes to the system clock.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-CLOCK_005fMONOTONIC"><span class="category-def">Macro: </span><span><code class="def-type">clockid_t</code> <strong class="def-name">CLOCK_MONOTONIC</strong><a class="copiable-link" href='#index-CLOCK_005fMONOTONIC'> &para;</a></span></dt>
<dd>
<p>System-wide clock that continuously measures the advancement of
calendar time, ignoring discontinuous changes to the system&rsquo;s
setting for absolute calendar time.
</p>
<p>The epoch for this clock is an unspecified point in the past.
The epoch may change if the system is rebooted or suspended.
Therefore, <code class="code">CLOCK_MONOTONIC</code> cannot be used to measure
absolute time, only elapsed time.
</p></dd></dl>

<p>Systems may support more than just these two clocks.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-clock_005fgettime"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">clock_gettime</strong> <code class="def-code-arguments">(clockid_t <var class="var">clock</var>, struct timespec *<var class="var">ts</var>)</code><a class="copiable-link" href='#index-clock_005fgettime'> &para;</a></span></dt>
<dd>
<p>Get the current time according to the clock identified by <var class="var">clock</var>,
storing it as seconds and nanoseconds in <code class="code">*<var class="var">ts</var></code>.
See <a class="xref" href="Time-Types.html">Time Types</a>, for a description of <code class="code">struct timespec</code>.
</p>
<p>The return value is <code class="code">0</code> on success and <code class="code">-1</code> on failure.  The
following <code class="code">errno</code> error condition is defined for this function:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The clock identified by <var class="var">clock</var> is not supported.
</p></dd>
</dl>
</dd></dl>

<p><code class="code">clock_gettime</code> reports the time scaled to seconds and
nanoseconds, but the actual resolution of each clock may not be as
fine as one nanosecond, and may not be the same for all clocks.  POSIX
also provides a function for finding out the actual resolution of a
clock:
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-clock_005fgetres"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">clock_getres</strong> <code class="def-code-arguments">(clockid_t <var class="var">clock</var>, struct timespec *<var class="var">res</var>)</code><a class="copiable-link" href='#index-clock_005fgetres'> &para;</a></span></dt>
<dd>
<p>Get the actual resolution of the clock identified by <var class="var">clock</var>,
storing it in <code class="code">*<var class="var">ts</var></code>.
</p>
<p>For instance, if the clock hardware for <code class="code">CLOCK_REALTIME</code>
uses a quartz crystal that oscillates at 32.768 kHz,
then its resolution would be 30.518 microseconds,
and &lsquo;<samp class="samp"><span class="w-nolinebreak-text">clock_getres</span>&nbsp;<span class="w-nolinebreak-text">(CLOCK_REALTIME,</span>&nbsp;&amp;r)</samp>&rsquo;<!-- /@w --> would set
<code class="code">r.tv_sec</code> to 0 and <code class="code">r.tv_nsec</code> to 30518.
</p>
<p>The return value is <code class="code">0</code> on success and <code class="code">-1</code> on failure.  The
following <code class="code">errno</code> error condition is defined for this function:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The clock identified by <var class="var">clock</var> is not supported.
</p></dd>
</dl>
</dd></dl>

<p>These functions, and the constants that identify particular clocks,
are declared in <samp class="file">time.h</samp>.
</p>
<p><strong class="strong">Portability Note:</strong> On some systems, including systems that use
older versions of the GNU C Library, programs that use <code class="code">clock_gettime</code>
or <code class="code">clock_setres</code> must be linked with the <code class="code">-lrt</code> library.
This has not been necessary with the GNU C Library since version 2.17.
</p>
<p>The GNU C Library also provides an older, but still widely used, function
for getting the current time with a resolution of microseconds.  This
function is declared in <samp class="file">sys/time.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gettimeofday"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">gettimeofday</strong> <code class="def-code-arguments">(struct timeval *<var class="var">tp</var>, void *<var class="var">tzp</var>)</code><a class="copiable-link" href='#index-gettimeofday'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Get the current calendar time, storing it as seconds and microseconds
in <code class="code">*<var class="var">tp</var></code>.  See <a class="xref" href="Time-Types.html">Time Types</a>, for a description of
<code class="code">struct timeval</code>.  The clock of <code class="code">gettimeofday</code> is close to,
but not necessarily in lock-step with, the clocks of <code class="code">time</code> and of
&lsquo;<samp class="samp"><span class="w-nolinebreak-text">clock_gettime</span>&nbsp;<span class="w-nolinebreak-text">(CLOCK_REALTIME)</span></samp>&rsquo;<!-- /@w --> (see above).
</p>
<p>On some historic systems, if <var class="var">tzp</var> was not a null pointer,
information about a system-wide time zone would be written to
<code class="code">*<var class="var">tzp</var></code>.  This feature is obsolete and not supported on
GNU systems.  You should always supply a null pointer for this
argument.  Instead, use the facilities described in <a class="ref" href="Time-Zone-Functions.html">Functions and Variables for Time Zones</a> and in <a class="ref" href="Broken_002ddown-Time.html">Broken-down Time</a> for working with time zones.
</p>
<p>This function cannot fail, and its return value is always <code class="code">0</code>.
</p>
<p><strong class="strong">Portability Note:</strong> As of the 2008 revision of POSIX, this
function is considered obsolete.  The GNU C Library will continue to provide
this function indefinitely, but new programs should use
<code class="code">clock_gettime</code> instead.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Setting-and-Adjusting-the-Time.html">Setting and Adjusting the Time</a>, Up: <a href="Calendar-Time.html">Calendar Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
