<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Wide Character Case Conversion (The GNU C Library)</title>

<meta name="description" content="Wide Character Case Conversion (The GNU C Library)">
<meta name="keywords" content="Wide Character Case Conversion (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Character-Handling.html" rel="up" title="Character Handling">
<link href="Using-Wide-Char-Classes.html" rel="prev" title="Using Wide Char Classes">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Wide-Character-Case-Conversion">
<div class="nav-panel">
<p>
Previous: <a href="Using-Wide-Char-Classes.html" accesskey="p" rel="prev">Notes on using the wide character classes</a>, Up: <a href="Character-Handling.html" accesskey="u" rel="up">Character Handling</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Mapping-of-wide-characters_002e">4.5 Mapping of wide characters.</h3>

<p>The classification functions are also generalized by the ISO&nbsp;C<!-- /@w -->
standard.  Instead of just allowing the two standard mappings, a
locale can contain others.  Again, the <code class="code">localedef</code> program
already supports generating such locale data files.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-wctrans_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">wctrans_t</strong><a class="copiable-link" href='#index-wctrans_005ft'> &para;</a></span></dt>
<dd>
<p>This data type is defined as a scalar type which can hold a value
representing the locale-dependent character mapping.  There is no way to
construct such a value apart from using the return value of the
<code class="code">wctrans</code> function.
</p>
<a class="index-entry-id" id="index-wctype_002eh-14"></a>
<p>This type is defined in <samp class="file">wctype.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wctrans"><span class="category-def">Function: </span><span><code class="def-type">wctrans_t</code> <strong class="def-name">wctrans</strong> <code class="def-code-arguments">(const char *<var class="var">property</var>)</code><a class="copiable-link" href='#index-wctrans'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">wctrans</code> function has to be used to find out whether a named
mapping is defined in the current locale selected for the
<code class="code">LC_CTYPE</code> category.  If the returned value is non-zero, you can use
it afterwards in calls to <code class="code">towctrans</code>.  If the return value is
zero no such mapping is known in the current locale.
</p>
<p>Beside locale-specific mappings there are two mappings which are
guaranteed to be available in every locale:
</p>
<table class="multitable">
<tbody><tr><td width="50%"><code class="code">&quot;tolower&quot;</code></td><td width="50%"><code class="code">&quot;toupper&quot;</code></td></tr>
</tbody>
</table>

<a class="index-entry-id" id="index-wctype_002eh-15"></a>
<p>These functions are declared in <samp class="file">wctype.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-towctrans"><span class="category-def">Function: </span><span><code class="def-type">wint_t</code> <strong class="def-name">towctrans</strong> <code class="def-code-arguments">(wint_t <var class="var">wc</var>, wctrans_t <var class="var">desc</var>)</code><a class="copiable-link" href='#index-towctrans'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">towctrans</code> maps the input character <var class="var">wc</var>
according to the rules of the mapping for which <var class="var">desc</var> is a
descriptor, and returns the value it finds.  <var class="var">desc</var> must be
obtained by a successful call to <code class="code">wctrans</code>.
</p>
<a class="index-entry-id" id="index-wctype_002eh-16"></a>
<p>This function is declared in <samp class="file">wctype.h</samp>.
</p></dd></dl>

<p>For the generally available mappings, the ISO&nbsp;C<!-- /@w --> standard defines
convenient shortcuts so that it is not necessary to call <code class="code">wctrans</code>
for them.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-towlower"><span class="category-def">Function: </span><span><code class="def-type">wint_t</code> <strong class="def-name">towlower</strong> <code class="def-code-arguments">(wint_t <var class="var">wc</var>)</code><a class="copiable-link" href='#index-towlower'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>If <var class="var">wc</var> is an upper-case letter, <code class="code">towlower</code> returns the corresponding
lower-case letter.  If <var class="var">wc</var> is not an upper-case letter,
<var class="var">wc</var> is returned unchanged.
</p>
<p><code class="code">towlower</code> can be implemented using
</p>
<div class="example smallexample">
<pre class="example-preformatted">towctrans (wc, wctrans (&quot;tolower&quot;))
</pre></div>

<a class="index-entry-id" id="index-wctype_002eh-17"></a>
<p>This function is declared in <samp class="file">wctype.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-towupper"><span class="category-def">Function: </span><span><code class="def-type">wint_t</code> <strong class="def-name">towupper</strong> <code class="def-code-arguments">(wint_t <var class="var">wc</var>)</code><a class="copiable-link" href='#index-towupper'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>If <var class="var">wc</var> is a lower-case letter, <code class="code">towupper</code> returns the corresponding
upper-case letter.  Otherwise <var class="var">wc</var> is returned unchanged.
</p>
<p><code class="code">towupper</code> can be implemented using
</p>
<div class="example smallexample">
<pre class="example-preformatted">towctrans (wc, wctrans (&quot;toupper&quot;))
</pre></div>

<a class="index-entry-id" id="index-wctype_002eh-18"></a>
<p>This function is declared in <samp class="file">wctype.h</samp>.
</p></dd></dl>

<p>The same warnings given in the last section for the use of the wide
character classification functions apply here.  It is not possible to
simply cast a <code class="code">char</code> type value to a <code class="code">wint_t</code> and use it as an
argument to <code class="code">towctrans</code> calls.
</p></div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Using-Wide-Char-Classes.html">Notes on using the wide character classes</a>, Up: <a href="Character-Handling.html">Character Handling</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
