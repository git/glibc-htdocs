<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Matching POSIX Regexps (The GNU C Library)</title>

<meta name="description" content="Matching POSIX Regexps (The GNU C Library)">
<meta name="keywords" content="Matching POSIX Regexps (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Regular-Expressions.html" rel="up" title="Regular Expressions">
<link href="Regexp-Subexpressions.html" rel="next" title="Regexp Subexpressions">
<link href="Flags-for-POSIX-Regexps.html" rel="prev" title="Flags for POSIX Regexps">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Matching-POSIX-Regexps">
<div class="nav-panel">
<p>
Next: <a href="Regexp-Subexpressions.html" accesskey="n" rel="next">Match Results with Subexpressions</a>, Previous: <a href="Flags-for-POSIX-Regexps.html" accesskey="p" rel="prev">Flags for POSIX Regular Expressions</a>, Up: <a href="Regular-Expressions.html" accesskey="u" rel="up">Regular Expression Matching</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Matching-a-Compiled-POSIX-Regular-Expression">10.3.3 Matching a Compiled POSIX Regular Expression</h4>

<p>Once you have compiled a regular expression, as described in <a class="ref" href="POSIX-Regexp-Compilation.html">POSIX Regular Expression Compilation</a>, you can match it against strings using
<code class="code">regexec</code>.  A match anywhere inside the string counts as success,
unless the regular expression contains anchor characters (&lsquo;<samp class="samp">^</samp>&rsquo; or
&lsquo;<samp class="samp">$</samp>&rsquo;).
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-regexec"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">regexec</strong> <code class="def-code-arguments">(const regex_t *restrict <var class="var">compiled</var>, const char *restrict <var class="var">string</var>, size_t <var class="var">nmatch</var>, regmatch_t <var class="var">matchptr</var>[restrict], int <var class="var">eflags</var>)</code><a class="copiable-link" href='#index-regexec'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function tries to match the compiled regular expression
<code class="code">*<var class="var">compiled</var></code> against <var class="var">string</var>.
</p>
<p><code class="code">regexec</code> returns <code class="code">0</code> if the regular expression matches;
otherwise, it returns a nonzero value.  See the table below for
what nonzero values mean.  You can use <code class="code">regerror</code> to produce an
error message string describing the reason for a nonzero value;
see <a class="ref" href="Regexp-Cleanup.html">POSIX Regexp Matching Cleanup</a>.
</p>
<p>The argument <var class="var">eflags</var> is a word of bit flags that enable various
options.
</p>
<p>If you want to get information about what part of <var class="var">string</var> actually
matched the regular expression or its subexpressions, use the arguments
<var class="var">matchptr</var> and <var class="var">nmatch</var>.  Otherwise, pass <code class="code">0</code> for
<var class="var">nmatch</var>, and <code class="code">NULL</code> for <var class="var">matchptr</var>.  See <a class="xref" href="Regexp-Subexpressions.html">Match Results with Subexpressions</a>.
</p></dd></dl>

<p>You must match the regular expression with the same set of current
locales that were in effect when you compiled the regular expression.
</p>
<p>The function <code class="code">regexec</code> accepts the following flags in the
<var class="var">eflags</var> argument:
</p>
<dl class="vtable">
<dt id='index-REG_005fNOTBOL'><span><code class="code">REG_NOTBOL</code><a class="copiable-link" href='#index-REG_005fNOTBOL'> &para;</a></span></dt>
<dd>
<p>Do not regard the beginning of the specified string as the beginning of
a line; more generally, don&rsquo;t make any assumptions about what text might
precede it.
</p>
</dd>
<dt id='index-REG_005fNOTEOL'><span><code class="code">REG_NOTEOL</code><a class="copiable-link" href='#index-REG_005fNOTEOL'> &para;</a></span></dt>
<dd>
<p>Do not regard the end of the specified string as the end of a line; more
generally, don&rsquo;t make any assumptions about what text might follow it.
</p></dd>
</dl>

<p>Here are the possible nonzero values that <code class="code">regexec</code> can return:
</p>
<dl class="vtable">
<dt id='index-REG_005fNOMATCH'><span><code class="code">REG_NOMATCH</code><a class="copiable-link" href='#index-REG_005fNOMATCH'> &para;</a></span></dt>
<dd>
<p>The pattern didn&rsquo;t match the string.  This isn&rsquo;t really an error.
</p>
</dd>
<dt id='index-REG_005fESPACE-1'><span><code class="code">REG_ESPACE</code><a class="copiable-link" href='#index-REG_005fESPACE-1'> &para;</a></span></dt>
<dd>
<p><code class="code">regexec</code> ran out of memory.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Regexp-Subexpressions.html">Match Results with Subexpressions</a>, Previous: <a href="Flags-for-POSIX-Regexps.html">Flags for POSIX Regular Expressions</a>, Up: <a href="Regular-Expressions.html">Regular Expression Matching</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
