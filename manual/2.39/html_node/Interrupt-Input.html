<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Interrupt Input (The GNU C Library)</title>

<meta name="description" content="Interrupt Input (The GNU C Library)">
<meta name="keywords" content="Interrupt Input (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Low_002dLevel-I_002fO.html" rel="up" title="Low-Level I/O">
<link href="IOCTLs.html" rel="next" title="IOCTLs">
<link href="Open-File-Description-Locks-Example.html" rel="prev" title="Open File Description Locks Example">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Interrupt-Input">
<div class="nav-panel">
<p>
Next: <a href="IOCTLs.html" accesskey="n" rel="next">Generic I/O Control operations</a>, Previous: <a href="Open-File-Description-Locks-Example.html" accesskey="p" rel="prev">Open File Description Locks Example</a>, Up: <a href="Low_002dLevel-I_002fO.html" accesskey="u" rel="up">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Interrupt_002dDriven-Input">13.19 Interrupt-Driven Input</h3>

<a class="index-entry-id" id="index-interrupt_002ddriven-input"></a>
<p>If you set the <code class="code">O_ASYNC</code> status flag on a file descriptor
(see <a class="pxref" href="File-Status-Flags.html">File Status Flags</a>), a <code class="code">SIGIO</code> signal is sent whenever
input or output becomes possible on that file descriptor.  The process
or process group to receive the signal can be selected by using the
<code class="code">F_SETOWN</code> command to the <code class="code">fcntl</code> function.  If the file
descriptor is a socket, this also selects the recipient of <code class="code">SIGURG</code>
signals that are delivered when out-of-band data arrives on that socket;
see <a class="ref" href="Out_002dof_002dBand-Data.html">Out-of-Band Data</a>.  (<code class="code">SIGURG</code> is sent in any situation
where <code class="code">select</code> would report the socket as having an &ldquo;exceptional
condition&rdquo;.  See <a class="xref" href="Waiting-for-I_002fO.html">Waiting for Input or Output</a>.)
</p>
<p>If the file descriptor corresponds to a terminal device, then <code class="code">SIGIO</code>
signals are sent to the foreground process group of the terminal.
See <a class="xref" href="Job-Control.html">Job Control</a>.
</p>
<a class="index-entry-id" id="index-fcntl_002eh-7"></a>
<p>The symbols in this section are defined in the header file
<samp class="file">fcntl.h</samp>.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-F_005fGETOWN-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">F_GETOWN</strong><a class="copiable-link" href='#index-F_005fGETOWN-1'> &para;</a></span></dt>
<dd>
<p>This macro is used as the <var class="var">command</var> argument to <code class="code">fcntl</code>, to
specify that it should get information about the process or process
group to which <code class="code">SIGIO</code> signals are sent.  (For a terminal, this is
actually the foreground process group ID, which you can get using
<code class="code">tcgetpgrp</code>; see <a class="ref" href="Terminal-Access-Functions.html">Functions for Controlling Terminal Access</a>.)
</p>
<p>The return value is interpreted as a process ID; if negative, its
absolute value is the process group ID.
</p>
<p>The following <code class="code">errno</code> error condition is defined for this command:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is invalid.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-F_005fSETOWN-1"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">F_SETOWN</strong><a class="copiable-link" href='#index-F_005fSETOWN-1'> &para;</a></span></dt>
<dd>
<p>This macro is used as the <var class="var">command</var> argument to <code class="code">fcntl</code>, to
specify that it should set the process or process group to which
<code class="code">SIGIO</code> signals are sent.  This command requires a third argument
of type <code class="code">pid_t</code> to be passed to <code class="code">fcntl</code>, so that the form of
the call is:
</p>
<div class="example smallexample">
<pre class="example-preformatted">fcntl (<var class="var">filedes</var>, F_SETOWN, <var class="var">pid</var>)
</pre></div>

<p>The <var class="var">pid</var> argument should be a process ID.  You can also pass a
negative number whose absolute value is a process group ID.
</p>
<p>The return value from <code class="code">fcntl</code> with this command is <em class="math">-1</em>
in case of error and some other value if successful.  The following
<code class="code">errno</code> error conditions are defined for this command:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is invalid.
</p>
</dd>
<dt><code class="code">ESRCH</code></dt>
<dd><p>There is no process or process group corresponding to <var class="var">pid</var>.
</p></dd>
</dl>
</dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="IOCTLs.html">Generic I/O Control operations</a>, Previous: <a href="Open-File-Description-Locks-Example.html">Open File Description Locks Example</a>, Up: <a href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
