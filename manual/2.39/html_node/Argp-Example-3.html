<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Argp Example 3 (The GNU C Library)</title>

<meta name="description" content="Argp Example 3 (The GNU C Library)">
<meta name="keywords" content="Argp Example 3 (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Argp-Examples.html" rel="up" title="Argp Examples">
<link href="Argp-Example-4.html" rel="next" title="Argp Example 4">
<link href="Argp-Example-2.html" rel="prev" title="Argp Example 2">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Argp-Example-3">
<div class="nav-panel">
<p>
Next: <a href="Argp-Example-4.html" accesskey="n" rel="next">A Program Using Multiple Combined Argp Parsers</a>, Previous: <a href="Argp-Example-2.html" accesskey="p" rel="prev">A Program Using Argp with Only Default Options</a>, Up: <a href="Argp-Examples.html" accesskey="u" rel="up">Argp Examples</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="A-Program-Using-Argp-with-User-Options">26.3.11.3 A Program Using Argp with User Options</h4>

<p>This program uses the same features as example 2, adding user options
and arguments.
</p>
<p>We now use the first four fields in <code class="code">argp</code> (see <a class="pxref" href="Argp-Parsers.html">Specifying Argp Parsers</a>)
and specify <code class="code">parse_opt</code> as the parser function.  See <a class="xref" href="Argp-Parser-Functions.html">Argp Parser Functions</a>.
</p>
<p>Note that in this example, <code class="code">main</code> uses a structure to communicate
with the <code class="code">parse_opt</code> function, a pointer to which it passes in the
<code class="code">input</code> argument to <code class="code">argp_parse</code>.  See <a class="xref" href="Argp.html">Parsing Program Options with Argp</a>.  It is retrieved
by <code class="code">parse_opt</code> through the <code class="code">input</code> field in its <code class="code">state</code>
argument.  See <a class="xref" href="Argp-Parsing-State.html">Argp Parsing State</a>.  Of course, it&rsquo;s also possible to
use global variables instead, but using a structure like this is
somewhat more flexible and clean.
</p>
<div class="example smallexample">
<pre class="example-preformatted">

/* <span class="r">This program uses the same features as example 2, and uses options and
   arguments.

   We now use the first four fields in ARGP, so here&rsquo;s a description of them:
     OPTIONS  &ndash; A pointer to a vector of struct argp_option (see below)
     PARSER   &ndash; A function to parse a single option, called by argp
     ARGS_DOC &ndash; A string describing how the non-option arguments should look
     DOC      &ndash; A descriptive string about this program; if it contains a
                 vertical tab character (\v), the part after it will be
                 printed *following* the options

   The function PARSER takes the following arguments:
     KEY  &ndash; An integer specifying which option this is (taken
             from the KEY field in each struct argp_option), or
             a special key specifying something else; the only
             special keys we use here are ARGP_KEY_ARG, meaning
             a non-option argument, and ARGP_KEY_END, meaning
             that all arguments have been parsed
     ARG  &ndash; For an option KEY, the string value of its
             argument, or NULL if it has none
     STATE&ndash; A pointer to a struct argp_state, containing
             various useful information about the parsing state; used here
             are the INPUT field, which reflects the INPUT argument to
             argp_parse, and the ARG_NUM field, which is the number of the
             current non-option argument being parsed
   It should return either 0, meaning success, ARGP_ERR_UNKNOWN, meaning the
   given KEY wasn&rsquo;t recognized, or an errno value indicating some other
   error.

   Note that in this example, main uses a structure to communicate with the
   parse_opt function, a pointer to which it passes in the INPUT argument to
   argp_parse.  Of course, it&rsquo;s also possible to use global variables
   instead, but this is somewhat more flexible.

   The OPTIONS field contains a pointer to a vector of struct argp_option&rsquo;s;
   that structure has the following fields (if you assign your option
   structures using array initialization like this example, unspecified
   fields will be defaulted to 0, and need not be specified):
     NAME   &ndash; The name of this option&rsquo;s long option (may be zero)
     KEY    &ndash; The KEY to pass to the PARSER function when parsing this option,
               *and* the name of this option&rsquo;s short option, if it is a
               printable ascii character
     ARG    &ndash; The name of this option&rsquo;s argument, if any
     FLAGS  &ndash; Flags describing this option; some of them are:
                 OPTION_ARG_OPTIONAL &ndash; The argument to this option is optional
                 OPTION_ALIAS        &ndash; This option is an alias for the
                                        previous option
                 OPTION_HIDDEN       &ndash; Don&rsquo;t show this option in &ndash;help output
     DOC    &ndash; A documentation string for this option, shown in &ndash;help output

   An options vector should be terminated by an option with all fields zero.</span> */

#include &lt;stdlib.h&gt;
#include &lt;argp.h&gt;

const char *argp_program_version =
  &quot;argp-ex3 1.0&quot;;
const char *argp_program_bug_address =
  &quot;&lt;bug-gnu-utils@gnu.org&gt;&quot;;

/* <span class="r">Program documentation.</span> */
static char doc[] =
  &quot;Argp example #3 -- a program with options and arguments using argp&quot;;

/* <span class="r">A description of the arguments we accept.</span> */
static char args_doc[] = &quot;ARG1 ARG2&quot;;

/* <span class="r">The options we understand.</span> */
static struct argp_option options[] = {
  {&quot;verbose&quot;,  'v', 0,      0,  &quot;Produce verbose output&quot; },
  {&quot;quiet&quot;,    'q', 0,      0,  &quot;Don't produce any output&quot; },
  {&quot;silent&quot;,   's', 0,      OPTION_ALIAS },
  {&quot;output&quot;,   'o', &quot;FILE&quot;, 0,
   &quot;Output to FILE instead of standard output&quot; },
  { 0 }
};

/* <span class="r">Used by <code class="code">main</code> to communicate with <code class="code">parse_opt</code>.</span> */
struct arguments
{
  char *args[2];                /* <span class="r"><var class="var">arg1</var> &amp; <var class="var">arg2</var></span> */
  int silent, verbose;
  char *output_file;
};

/* <span class="r">Parse a single option.</span> */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  /* <span class="r">Get the <var class="var">input</var> argument from <code class="code">argp_parse</code>, which we
     know is a pointer to our arguments structure.</span> */
  struct arguments *arguments = state-&gt;input;

  switch (key)
    {
    case 'q': case 's':
      arguments-&gt;silent = 1;
      break;
    case 'v':
      arguments-&gt;verbose = 1;
      break;
    case 'o':
      arguments-&gt;output_file = arg;
      break;

    case ARGP_KEY_ARG:
      if (state-&gt;arg_num &gt;= 2)
        /* <span class="r">Too many arguments.</span> */
        argp_usage (state);

      arguments-&gt;args[state-&gt;arg_num] = arg;

      break;

    case ARGP_KEY_END:
      if (state-&gt;arg_num &lt; 2)
        /* <span class="r">Not enough arguments.</span> */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* <span class="r">Our argp parser.</span> */
static struct argp argp = { options, parse_opt, args_doc, doc };

int
main (int argc, char **argv)
{
  struct arguments arguments;

  /* <span class="r">Default values.</span> */
  arguments.silent = 0;
  arguments.verbose = 0;
  arguments.output_file = &quot;-&quot;;

  /* <span class="r">Parse our arguments; every option seen by <code class="code">parse_opt</code> will
     be reflected in <code class="code">arguments</code>.</span> */
  argp_parse (&amp;argp, argc, argv, 0, 0, &amp;arguments);

  printf (&quot;ARG1 = %s\nARG2 = %s\nOUTPUT_FILE = %s\n&quot;
          &quot;VERBOSE = %s\nSILENT = %s\n&quot;,
          arguments.args[0], arguments.args[1],
          arguments.output_file,
          arguments.verbose ? &quot;yes&quot; : &quot;no&quot;,
          arguments.silent ? &quot;yes&quot; : &quot;no&quot;);

  exit (0);
}
</pre></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Argp-Example-4.html">A Program Using Multiple Combined Argp Parsers</a>, Previous: <a href="Argp-Example-2.html">A Program Using Argp with Only Default Options</a>, Up: <a href="Argp-Examples.html">Argp Examples</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
