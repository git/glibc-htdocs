<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Heap Consistency Checking (The GNU C Library)</title>

<meta name="description" content="Heap Consistency Checking (The GNU C Library)">
<meta name="keywords" content="Heap Consistency Checking (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Unconstrained-Allocation.html" rel="up" title="Unconstrained Allocation">
<link href="Statistics-of-Malloc.html" rel="next" title="Statistics of Malloc">
<link href="Malloc-Tunable-Parameters.html" rel="prev" title="Malloc Tunable Parameters">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Heap-Consistency-Checking">
<div class="nav-panel">
<p>
Next: <a href="Statistics-of-Malloc.html" accesskey="n" rel="next">Statistics for Memory Allocation with <code class="code">malloc</code></a>, Previous: <a href="Malloc-Tunable-Parameters.html" accesskey="p" rel="prev">Malloc Tunable Parameters</a>, Up: <a href="Unconstrained-Allocation.html" accesskey="u" rel="up">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Heap-Consistency-Checking-1">3.2.3.8 Heap Consistency Checking</h4>

<a class="index-entry-id" id="index-heap-consistency-checking"></a>
<a class="index-entry-id" id="index-consistency-checking_002c-of-heap"></a>

<p>You can ask <code class="code">malloc</code> to check the consistency of dynamic memory by
using the <code class="code">mcheck</code> function and preloading the malloc debug library
<samp class="file">libc_malloc_debug</samp> using the <var class="var">LD_PRELOAD</var> environment variable.
This function is a GNU extension, declared in <samp class="file">mcheck.h</samp>.
<a class="index-entry-id" id="index-mcheck_002eh"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mcheck"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">mcheck</strong> <code class="def-code-arguments">(void (*<var class="var">abortfn</var>) (enum mcheck_status <var class="var">status</var>))</code><a class="copiable-link" href='#index-mcheck'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:mcheck const:malloc_hooks
| AS-Unsafe corrupt
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>Calling <code class="code">mcheck</code> tells <code class="code">malloc</code> to perform occasional
consistency checks.  These will catch things such as writing
past the end of a block that was allocated with <code class="code">malloc</code>.
</p>
<p>The <var class="var">abortfn</var> argument is the function to call when an inconsistency
is found.  If you supply a null pointer, then <code class="code">mcheck</code> uses a
default function which prints a message and calls <code class="code">abort</code>
(see <a class="pxref" href="Aborting-a-Program.html">Aborting a Program</a>).  The function you supply is called with
one argument, which says what sort of inconsistency was detected; its
type is described below.
</p>
<p>It is too late to begin allocation checking once you have allocated
anything with <code class="code">malloc</code>.  So <code class="code">mcheck</code> does nothing in that
case.  The function returns <code class="code">-1</code> if you call it too late, and
<code class="code">0</code> otherwise (when it is successful).
</p>
<p>The easiest way to arrange to call <code class="code">mcheck</code> early enough is to use
the option &lsquo;<samp class="samp">-lmcheck</samp>&rsquo; when you link your program; then you don&rsquo;t
need to modify your program source at all.  Alternatively you might use
a debugger to insert a call to <code class="code">mcheck</code> whenever the program is
started, for example these gdb commands will automatically call <code class="code">mcheck</code>
whenever the program starts:
</p>
<div class="example smallexample">
<pre class="example-preformatted">(gdb) break main
Breakpoint 1, main (argc=2, argv=0xbffff964) at whatever.c:10
(gdb) command 1
Type commands for when breakpoint 1 is hit, one per line.
End with a line saying just &quot;end&quot;.
&gt;call mcheck(0)
&gt;continue
&gt;end
(gdb) &hellip;
</pre></div>

<p>This will however only work if no initialization function of any object
involved calls any of the <code class="code">malloc</code> functions since <code class="code">mcheck</code>
must be called before the first such function.
</p>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mprobe"><span class="category-def">Function: </span><span><code class="def-type">enum mcheck_status</code> <strong class="def-name">mprobe</strong> <code class="def-code-arguments">(void *<var class="var">pointer</var>)</code><a class="copiable-link" href='#index-mprobe'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Unsafe race:mcheck const:malloc_hooks
| AS-Unsafe corrupt
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>The <code class="code">mprobe</code> function lets you explicitly check for inconsistencies
in a particular allocated block.  You must have already called
<code class="code">mcheck</code> at the beginning of the program, to do its occasional
checks; calling <code class="code">mprobe</code> requests an additional consistency check
to be done at the time of the call.
</p>
<p>The argument <var class="var">pointer</var> must be a pointer returned by <code class="code">malloc</code>
or <code class="code">realloc</code>.  <code class="code">mprobe</code> returns a value that says what
inconsistency, if any, was found.  The values are described below.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-enum-mcheck_005fstatus"><span class="category-def">Data Type: </span><span><strong class="def-name">enum mcheck_status</strong><a class="copiable-link" href='#index-enum-mcheck_005fstatus'> &para;</a></span></dt>
<dd><p>This enumerated type describes what kind of inconsistency was detected
in an allocated block, if any.  Here are the possible values:
</p>
<dl class="table">
<dt><code class="code">MCHECK_DISABLED</code></dt>
<dd><p><code class="code">mcheck</code> was not called before the first allocation.
No consistency checking can be done.
</p></dd>
<dt><code class="code">MCHECK_OK</code></dt>
<dd><p>No inconsistency detected.
</p></dd>
<dt><code class="code">MCHECK_HEAD</code></dt>
<dd><p>The data immediately before the block was modified.
This commonly happens when an array index or pointer
is decremented too far.
</p></dd>
<dt><code class="code">MCHECK_TAIL</code></dt>
<dd><p>The data immediately after the block was modified.
This commonly happens when an array index or pointer
is incremented too far.
</p></dd>
<dt><code class="code">MCHECK_FREE</code></dt>
<dd><p>The block was already freed.
</p></dd>
</dl>
</dd></dl>

<p>Another possibility to check for and guard against bugs in the use of
<code class="code">malloc</code>, <code class="code">realloc</code> and <code class="code">free</code> is to set the environment
variable <code class="code">MALLOC_CHECK_</code>.  When <code class="code">MALLOC_CHECK_</code> is set to a
non-zero value less than 4, a special (less efficient) implementation is
used which is designed to be tolerant against simple errors, such as
double calls of <code class="code">free</code> with the same argument, or overruns of a
single byte (off-by-one bugs).  Not all such errors can be protected
against, however, and memory leaks can result.  Like in the case of
<code class="code">mcheck</code>, one would need to preload the <samp class="file">libc_malloc_debug</samp>
library to enable <code class="code">MALLOC_CHECK_</code> functionality.  Without this
preloaded library, setting <code class="code">MALLOC_CHECK_</code> will have no effect.
</p>
<p>Any detected heap corruption results in immediate termination of the
process.
</p>
<p>There is one problem with <code class="code">MALLOC_CHECK_</code>: in SUID or SGID binaries
it could possibly be exploited since diverging from the normal programs
behavior it now writes something to the standard error descriptor.
Therefore the use of <code class="code">MALLOC_CHECK_</code> is disabled by default for
SUID and SGID binaries.
</p>
<p>So, what&rsquo;s the difference between using <code class="code">MALLOC_CHECK_</code> and linking
with &lsquo;<samp class="samp">-lmcheck</samp>&rsquo;?  <code class="code">MALLOC_CHECK_</code> is orthogonal with respect to
&lsquo;<samp class="samp">-lmcheck</samp>&rsquo;.  &lsquo;<samp class="samp">-lmcheck</samp>&rsquo; has been added for backward
compatibility.  Both <code class="code">MALLOC_CHECK_</code> and &lsquo;<samp class="samp">-lmcheck</samp>&rsquo; should
uncover the same bugs - but using <code class="code">MALLOC_CHECK_</code> you don&rsquo;t need to
recompile your application.
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Statistics-of-Malloc.html">Statistics for Memory Allocation with <code class="code">malloc</code></a>, Previous: <a href="Malloc-Tunable-Parameters.html">Malloc Tunable Parameters</a>, Up: <a href="Unconstrained-Allocation.html">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
