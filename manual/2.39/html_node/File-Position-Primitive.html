<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>File Position Primitive (The GNU C Library)</title>

<meta name="description" content="File Position Primitive (The GNU C Library)">
<meta name="keywords" content="File Position Primitive (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Low_002dLevel-I_002fO.html" rel="up" title="Low-Level I/O">
<link href="Descriptors-and-Streams.html" rel="next" title="Descriptors and Streams">
<link href="I_002fO-Primitives.html" rel="prev" title="I/O Primitives">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="File-Position-Primitive">
<div class="nav-panel">
<p>
Next: <a href="Descriptors-and-Streams.html" accesskey="n" rel="next">Descriptors and Streams</a>, Previous: <a href="I_002fO-Primitives.html" accesskey="p" rel="prev">Input and Output Primitives</a>, Up: <a href="Low_002dLevel-I_002fO.html" accesskey="u" rel="up">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Setting-the-File-Position-of-a-Descriptor">13.3 Setting the File Position of a Descriptor</h3>

<p>Just as you can set the file position of a stream with <code class="code">fseek</code>, you
can set the file position of a descriptor with <code class="code">lseek</code>.  This
specifies the position in the file for the next <code class="code">read</code> or
<code class="code">write</code> operation.  See <a class="xref" href="File-Positioning.html">File Positioning</a>, for more information
on the file position and what it means.
</p>
<p>To read the current file position value from a descriptor, use
<code class="code">lseek (<var class="var">desc</var>, 0, SEEK_CUR)</code>.
</p>
<a class="index-entry-id" id="index-file-positioning-on-a-file-descriptor"></a>
<a class="index-entry-id" id="index-positioning-a-file-descriptor"></a>
<a class="index-entry-id" id="index-seeking-on-a-file-descriptor"></a>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-lseek"><span class="category-def">Function: </span><span><code class="def-type">off_t</code> <strong class="def-name">lseek</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, off_t <var class="var">offset</var>, int <var class="var">whence</var>)</code><a class="copiable-link" href='#index-lseek'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">lseek</code> function is used to change the file position of the
file with descriptor <var class="var">filedes</var>.
</p>
<p>The <var class="var">whence</var> argument specifies how the <var class="var">offset</var> should be
interpreted, in the same way as for the <code class="code">fseek</code> function, and it must
be one of the symbolic constants <code class="code">SEEK_SET</code>, <code class="code">SEEK_CUR</code>, or
<code class="code">SEEK_END</code>.
</p>
<dl class="vtable">
<dt id='index-SEEK_005fSET-1'><span><code class="code">SEEK_SET</code><a class="copiable-link" href='#index-SEEK_005fSET-1'> &para;</a></span></dt>
<dd><p>Specifies that <var class="var">offset</var> is a count of characters from the beginning
of the file.
</p>
</dd>
<dt id='index-SEEK_005fCUR-1'><span><code class="code">SEEK_CUR</code><a class="copiable-link" href='#index-SEEK_005fCUR-1'> &para;</a></span></dt>
<dd><p>Specifies that <var class="var">offset</var> is a count of characters from the current
file position.  This count may be positive or negative.
</p>
</dd>
<dt id='index-SEEK_005fEND-1'><span><code class="code">SEEK_END</code><a class="copiable-link" href='#index-SEEK_005fEND-1'> &para;</a></span></dt>
<dd><p>Specifies that <var class="var">offset</var> is a count of characters from the end of
the file.  A negative count specifies a position within the current
extent of the file; a positive count specifies a position past the
current end.  If you set the position past the current end, and
actually write data, you will extend the file with zeros up to that
position.
</p></dd>
</dl>

<p>The return value from <code class="code">lseek</code> is normally the resulting file
position, measured in bytes from the beginning of the file.
You can use this feature together with <code class="code">SEEK_CUR</code> to read the
current file position.
</p>
<p>If you want to append to the file, setting the file position to the
current end of file with <code class="code">SEEK_END</code> is not sufficient.  Another
process may write more data after you seek but before you write,
extending the file so the position you write onto clobbers their data.
Instead, use the <code class="code">O_APPEND</code> operating mode; see <a class="pxref" href="Operating-Modes.html">I/O Operating Modes</a>.
</p>
<p>You can set the file position past the current end of the file.  This
does not by itself make the file longer; <code class="code">lseek</code> never changes the
file.  But subsequent output at that position will extend the file.
Characters between the previous end of file and the new position are
filled with zeros.  Extending the file in this way can create a
&ldquo;hole&rdquo;: the blocks of zeros are not actually allocated on disk, so the
file takes up less space than it appears to; it is then called a
&ldquo;sparse file&rdquo;.
<a class="index-entry-id" id="index-sparse-files"></a>
<a class="index-entry-id" id="index-holes-in-files"></a>
</p>
<p>If the file position cannot be changed, or the operation is in some way
invalid, <code class="code">lseek</code> returns a value of <em class="math">-1</em>.  The following
<code class="code">errno</code> error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> is not a valid file descriptor.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>The <var class="var">whence</var> argument value is not valid, or the resulting
file offset is not valid.  A file offset is invalid.
</p>
</dd>
<dt><code class="code">ESPIPE</code></dt>
<dd><p>The <var class="var">filedes</var> corresponds to an object that cannot be positioned,
such as a pipe, FIFO or terminal device.  (POSIX.1 specifies this error
only for pipes and FIFOs, but on GNU systems, you always get
<code class="code">ESPIPE</code> if the object is not seekable.)
</p></dd>
</dl>

<p>When the source file is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> the
<code class="code">lseek</code> function is in fact <code class="code">lseek64</code> and the type
<code class="code">off_t</code> has 64 bits which makes it possible to handle files up to
2^63 bytes in length.
</p>
<p>This function is a cancellation point in multi-threaded programs.  This
is a problem if the thread allocates some resources (like memory, file
descriptors, semaphores or whatever) at the time <code class="code">lseek</code> is
called.  If the thread gets canceled these resources stay allocated
until the program ends.  To avoid this calls to <code class="code">lseek</code> should be
protected using cancellation handlers.
</p>
<p>The <code class="code">lseek</code> function is the underlying primitive for the
<code class="code">fseek</code>, <code class="code">fseeko</code>, <code class="code">ftell</code>, <code class="code">ftello</code> and
<code class="code">rewind</code> functions, which operate on streams instead of file
descriptors.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-lseek64"><span class="category-def">Function: </span><span><code class="def-type">off64_t</code> <strong class="def-name">lseek64</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, off64_t <var class="var">offset</var>, int <var class="var">whence</var>)</code><a class="copiable-link" href='#index-lseek64'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to the <code class="code">lseek</code> function.  The difference
is that the <var class="var">offset</var> parameter is of type <code class="code">off64_t</code> instead of
<code class="code">off_t</code> which makes it possible on 32 bit machines to address
files larger than 2^31 bytes and up to 2^63 bytes.  The
file descriptor <code class="code">filedes</code> must be opened using <code class="code">open64</code> since
otherwise the large offsets possible with <code class="code">off64_t</code> will lead to
errors with a descriptor in small file mode.
</p>
<p>When the source file is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32 bits machine this function is actually available under the name
<code class="code">lseek</code> and so transparently replaces the 32 bit interface.
</p></dd></dl>

<p>You can have multiple descriptors for the same file if you open the file
more than once, or if you duplicate a descriptor with <code class="code">dup</code>.
Descriptors that come from separate calls to <code class="code">open</code> have independent
file positions; using <code class="code">lseek</code> on one descriptor has no effect on the
other.  For example,
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">{
  int d1, d2;
  char buf[4];
  d1 = open (&quot;foo&quot;, O_RDONLY);
  d2 = open (&quot;foo&quot;, O_RDONLY);
  lseek (d1, 1024, SEEK_SET);
  read (d2, buf, 4);
}
</pre></div></div>

<p>will read the first four characters of the file <samp class="file">foo</samp>.  (The
error-checking code necessary for a real program has been omitted here
for brevity.)
</p>
<p>By contrast, descriptors made by duplication share a common file
position with the original descriptor that was duplicated.  Anything
which alters the file position of one of the duplicates, including
reading or writing data, affects all of them alike.  Thus, for example,
</p>
<div class="example smallexample">
<pre class="example-preformatted">{
  int d1, d2, d3;
  char buf1[4], buf2[4];
  d1 = open (&quot;foo&quot;, O_RDONLY);
  d2 = dup (d1);
  d3 = dup (d2);
  lseek (d3, 1024, SEEK_SET);
  read (d1, buf1, 4);
  read (d2, buf2, 4);
}
</pre></div>

<p>will read four characters starting with the 1024&rsquo;th character of
<samp class="file">foo</samp>, and then four more characters starting with the 1028&rsquo;th
character.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-off_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">off_t</strong><a class="copiable-link" href='#index-off_005ft'> &para;</a></span></dt>
<dd>
<p>This is a signed integer type used to represent file sizes.  In
the GNU C Library, this type is no narrower than <code class="code">int</code>.
</p>
<p>If the source is compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> this type
is transparently replaced by <code class="code">off64_t</code>.
</p></dd></dl>

<dl class="first-deftp">
<dt class="deftp" id="index-off64_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">off64_t</strong><a class="copiable-link" href='#index-off64_005ft'> &para;</a></span></dt>
<dd>
<p>This type is used similar to <code class="code">off_t</code>.  The difference is that even
on 32 bit machines, where the <code class="code">off_t</code> type would have 32 bits,
<code class="code">off64_t</code> has 64 bits and so is able to address files up to
2^63 bytes in length.
</p>
<p>When compiling with <code class="code">_FILE_OFFSET_BITS == 64</code> this type is
available under the name <code class="code">off_t</code>.
</p></dd></dl>

<p>These aliases for the &lsquo;<samp class="samp">SEEK_&hellip;</samp>&rsquo; constants exist for the sake
of compatibility with older BSD systems.  They are defined in two
different header files: <samp class="file">fcntl.h</samp> and <samp class="file">sys/file.h</samp>.
</p>
<dl class="vtable">
<dt id='index-L_005fSET-1'><span><code class="code">L_SET</code><a class="copiable-link" href='#index-L_005fSET-1'> &para;</a></span></dt>
<dd><p>An alias for <code class="code">SEEK_SET</code>.
</p>
</dd>
<dt id='index-L_005fINCR-1'><span><code class="code">L_INCR</code><a class="copiable-link" href='#index-L_005fINCR-1'> &para;</a></span></dt>
<dd><p>An alias for <code class="code">SEEK_CUR</code>.
</p>
</dd>
<dt id='index-L_005fXTND-1'><span><code class="code">L_XTND</code><a class="copiable-link" href='#index-L_005fXTND-1'> &para;</a></span></dt>
<dd><p>An alias for <code class="code">SEEK_END</code>.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Descriptors-and-Streams.html">Descriptors and Streams</a>, Previous: <a href="I_002fO-Primitives.html">Input and Output Primitives</a>, Up: <a href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
