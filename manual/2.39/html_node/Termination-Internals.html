<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Termination Internals (The GNU C Library)</title>

<meta name="description" content="Termination Internals (The GNU C Library)">
<meta name="keywords" content="Termination Internals (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Program-Termination.html" rel="up" title="Program Termination">
<link href="Aborting-a-Program.html" rel="prev" title="Aborting a Program">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Termination-Internals">
<div class="nav-panel">
<p>
Previous: <a href="Aborting-a-Program.html" accesskey="p" rel="prev">Aborting a Program</a>, Up: <a href="Program-Termination.html" accesskey="u" rel="up">Program Termination</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Termination-Internals-1">26.7.5 Termination Internals</h4>

<p>The <code class="code">_exit</code> function is the primitive used for process termination
by <code class="code">exit</code>.  It is declared in the header file <samp class="file">unistd.h</samp>.
<a class="index-entry-id" id="index-unistd_002eh-16"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005fexit"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">_exit</strong> <code class="def-code-arguments">(int <var class="var">status</var>)</code><a class="copiable-link" href='#index-_005fexit'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">_exit</code> function is the primitive for causing a process to
terminate with status <var class="var">status</var>.  Calling this function does not
execute cleanup functions registered with <code class="code">atexit</code> or
<code class="code">on_exit</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-_005fExit"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">_Exit</strong> <code class="def-code-arguments">(int <var class="var">status</var>)</code><a class="copiable-link" href='#index-_005fExit'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">_Exit</code> function is the ISO&nbsp;C<!-- /@w --> equivalent to <code class="code">_exit</code>.
The ISO&nbsp;C<!-- /@w --> committee members were not sure whether the definitions of
<code class="code">_exit</code> and <code class="code">_Exit</code> were compatible so they have not used the
POSIX name.
</p>
<p>This function was introduced in ISO&nbsp;C99<!-- /@w --> and is declared in
<samp class="file">stdlib.h</samp>.
</p></dd></dl>

<p>When a process terminates for any reason&mdash;either because the program
terminates, or as a result of a signal&mdash;the
following things happen:
</p>
<ul class="itemize mark-bullet">
<li>All open file descriptors in the process are closed.  See <a class="xref" href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a>.
Note that streams are not flushed automatically when the process
terminates; see <a class="ref" href="I_002fO-on-Streams.html">Input/Output on Streams</a>.

</li><li>A process exit status is saved to be reported back to the parent process
via <code class="code">wait</code> or <code class="code">waitpid</code>; see <a class="ref" href="Process-Completion.html">Process Completion</a>.  If the
program exited, this status includes as its low-order 8 bits the program
exit status.


</li><li>Any child processes of the process being terminated are assigned a new
parent process.  (On most systems, including GNU, this is the <code class="code">init</code>
process, with process ID 1.)

</li><li>A <code class="code">SIGCHLD</code> signal is sent to the parent process.

</li><li>If the process is a session leader that has a controlling terminal, then
a <code class="code">SIGHUP</code> signal is sent to each process in the foreground job,
and the controlling terminal is disassociated from that session.
See <a class="xref" href="Job-Control.html">Job Control</a>.

</li><li>If termination of a process causes a process group to become orphaned,
and any member of that process group is stopped, then a <code class="code">SIGHUP</code>
signal and a <code class="code">SIGCONT</code> signal are sent to each process in the
group.  See <a class="xref" href="Job-Control.html">Job Control</a>.
</li></ul>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Aborting-a-Program.html">Aborting a Program</a>, Up: <a href="Program-Termination.html">Program Termination</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
