<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Temporary Files (The GNU C Library)</title>

<meta name="description" content="Temporary Files (The GNU C Library)">
<meta name="keywords" content="Temporary Files (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="File-System-Interface.html" rel="up" title="File System Interface">
<link href="Making-Special-Files.html" rel="prev" title="Making Special Files">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Temporary-Files">
<div class="nav-panel">
<p>
Previous: <a href="Making-Special-Files.html" accesskey="p" rel="prev">Making Special Files</a>, Up: <a href="File-System-Interface.html" accesskey="u" rel="up">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Temporary-Files-1">14.11 Temporary Files</h3>

<p>If you need to use a temporary file in your program, you can use the
<code class="code">tmpfile</code> function to open it.  Or you can use the <code class="code">tmpnam</code>
(better: <code class="code">tmpnam_r</code>) function to provide a name for a temporary
file and then you can open it in the usual way with <code class="code">fopen</code>.
</p>
<p>The <code class="code">tempnam</code> function is like <code class="code">tmpnam</code> but lets you choose
what directory temporary files will go in, and something about what
their file names will look like.  Important for multi-threaded programs
is that <code class="code">tempnam</code> is reentrant, while <code class="code">tmpnam</code> is not since it
returns a pointer to a static buffer.
</p>
<p>These facilities are declared in the header file <samp class="file">stdio.h</samp>.
<a class="index-entry-id" id="index-stdio_002eh-17"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tmpfile"><span class="category-def">Function: </span><span><code class="def-type">FILE *</code> <strong class="def-name">tmpfile</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href='#index-tmpfile'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap lock
| AC-Unsafe mem fd lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function creates a temporary binary file for update mode, as if by
calling <code class="code">fopen</code> with mode <code class="code">&quot;wb+&quot;</code>.  The file is deleted
automatically when it is closed or when the program terminates.  (On
some other ISO&nbsp;C<!-- /@w --> systems the file may fail to be deleted if the program
terminates abnormally).
</p>
<p>This function is reentrant.
</p>
<p>When the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a
32-bit system this function is in fact <code class="code">tmpfile64</code>, i.e., the LFS
interface transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tmpfile64"><span class="category-def">Function: </span><span><code class="def-type">FILE *</code> <strong class="def-name">tmpfile64</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href='#index-tmpfile64'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap lock
| AC-Unsafe mem fd lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">tmpfile</code>, but the stream it returns a
pointer to was opened using <code class="code">tmpfile64</code>.  Therefore this stream can
be used for files larger than 2^31 bytes on 32-bit machines.
</p>
<p>Please note that the return type is still <code class="code">FILE *</code>.  There is no
special <code class="code">FILE</code> type for the LFS interface.
</p>
<p>If the sources are compiled with <code class="code">_FILE_OFFSET_BITS == 64</code> on a 32
bits machine this function is available under the name <code class="code">tmpfile</code>
and so transparently replaces the old interface.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tmpnam"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">tmpnam</strong> <code class="def-code-arguments">(char *<var class="var">result</var>)</code><a class="copiable-link" href='#index-tmpnam'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:tmpnam/!result
| AS-Unsafe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function constructs and returns a valid file name that does not
refer to any existing file.  If the <var class="var">result</var> argument is a null
pointer, the return value is a pointer to an internal static string,
which might be modified by subsequent calls and therefore makes this
function non-reentrant.  Otherwise, the <var class="var">result</var> argument should be
a pointer to an array of at least <code class="code">L_tmpnam</code> characters, and the
result is written into that array.
</p>
<p>It is possible for <code class="code">tmpnam</code> to fail if you call it too many times
without removing previously-created files.  This is because the limited
length of the temporary file names gives room for only a finite number
of different names.  If <code class="code">tmpnam</code> fails it returns a null pointer.
</p>
<p><strong class="strong">Warning:</strong> Between the time the pathname is constructed and the
file is created another process might have created a file with the same
name using <code class="code">tmpnam</code>, leading to a possible security hole.  The
implementation generates names which can hardly be predicted, but when
opening the file you should use the <code class="code">O_EXCL</code> flag.  Using
<code class="code">tmpfile</code> or <code class="code">mkstemp</code> is a safe way to avoid this problem.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tmpnam_005fr"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">tmpnam_r</strong> <code class="def-code-arguments">(char *<var class="var">result</var>)</code><a class="copiable-link" href='#index-tmpnam_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is nearly identical to the <code class="code">tmpnam</code> function, except
that if <var class="var">result</var> is a null pointer it returns a null pointer.
</p>
<p>This guarantees reentrancy because the non-reentrant situation of
<code class="code">tmpnam</code> cannot happen here.
</p>
<p><strong class="strong">Warning</strong>: This function has the same security problems as
<code class="code">tmpnam</code>.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-L_005ftmpnam"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">L_tmpnam</strong><a class="copiable-link" href='#index-L_005ftmpnam'> &para;</a></span></dt>
<dd>
<p>The value of this macro is an integer constant expression that
represents the minimum size of a string large enough to hold a file name
generated by the <code class="code">tmpnam</code> function.
</p></dd></dl>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-TMP_005fMAX"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">TMP_MAX</strong><a class="copiable-link" href='#index-TMP_005fMAX'> &para;</a></span></dt>
<dd>
<p>The macro <code class="code">TMP_MAX</code> is a lower bound for how many temporary names
you can create with <code class="code">tmpnam</code>.  You can rely on being able to call
<code class="code">tmpnam</code> at least this many times before it might fail saying you
have made too many temporary file names.
</p>
<p>With the GNU C Library, you can create a very large number of temporary
file names.  If you actually created the files, you would probably run
out of disk space before you ran out of names.  Some other systems have
a fixed, small limit on the number of temporary files.  The limit is
never less than <code class="code">25</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tempnam"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">tempnam</strong> <code class="def-code-arguments">(const char *<var class="var">dir</var>, const char *<var class="var">prefix</var>)</code><a class="copiable-link" href='#index-tempnam'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function generates a unique temporary file name.  If <var class="var">prefix</var>
is not a null pointer, up to five characters of this string are used as
a prefix for the file name.  The return value is a string newly
allocated with <code class="code">malloc</code>, so you should release its storage with
<code class="code">free</code> when it is no longer needed.
</p>
<p>Because the string is dynamically allocated this function is reentrant.
</p>
<p>The directory prefix for the temporary file name is determined by
testing each of the following in sequence.  The directory must exist and
be writable.
</p>
<ul class="itemize mark-bullet">
<li>The environment variable <code class="code">TMPDIR</code>, if it is defined.  For security
reasons this only happens if the program is not SUID or SGID enabled.

</li><li>The <var class="var">dir</var> argument, if it is not a null pointer.

</li><li>The value of the <code class="code">P_tmpdir</code> macro.

</li><li>The directory <samp class="file">/tmp</samp>.
</li></ul>

<p>This function is defined for SVID compatibility.
</p>
<p><strong class="strong">Warning:</strong> Between the time the pathname is constructed and the
file is created another process might have created a file with the same
name using <code class="code">tempnam</code>, leading to a possible security hole.  The
implementation generates names which can hardly be predicted, but when
opening the file you should use the <code class="code">O_EXCL</code> flag.  Using
<code class="code">tmpfile</code> or <code class="code">mkstemp</code> is a safe way to avoid this problem.
</p></dd></dl>
<a class="index-entry-id" id="index-TMPDIR-environment-variable"></a>

<dl class="first-deftypevr">
<dt class="deftypevr" id="index-P_005ftmpdir"><span class="category-def">SVID Macro: </span><span><code class="def-type">char *</code> <strong class="def-name">P_tmpdir</strong><a class="copiable-link" href='#index-P_005ftmpdir'> &para;</a></span></dt>
<dd>
<p>This macro is the name of the default directory for temporary files.
</p></dd></dl>

<p>Older Unix systems did not have the functions just described.  Instead
they used <code class="code">mktemp</code> and <code class="code">mkstemp</code>.  Both of these functions
work by modifying a file name template string you pass.  The last six
characters of this string must be &lsquo;<samp class="samp">XXXXXX</samp>&rsquo;.  These six &lsquo;<samp class="samp">X</samp>&rsquo;s
are replaced with six characters which make the whole string a unique
file name.  Usually the template string is something like
&lsquo;<samp class="samp">/tmp/<var class="var">prefix</var>XXXXXX</samp>&rsquo;, and each program uses a unique <var class="var">prefix</var>.
</p>
<p><strong class="strong">NB:</strong> Because <code class="code">mktemp</code> and <code class="code">mkstemp</code> modify the
template string, you <em class="emph">must not</em> pass string constants to them.
String constants are normally in read-only storage, so your program
would crash when <code class="code">mktemp</code> or <code class="code">mkstemp</code> tried to modify the
string.  These functions are declared in the header file <samp class="file">stdlib.h</samp>.
<a class="index-entry-id" id="index-stdlib_002eh-10"></a>
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mktemp"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">mktemp</strong> <code class="def-code-arguments">(char *<var class="var">template</var>)</code><a class="copiable-link" href='#index-mktemp'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">mktemp</code> function generates a unique file name by modifying
<var class="var">template</var> as described above.  If successful, it returns
<var class="var">template</var> as modified.  If <code class="code">mktemp</code> cannot find a unique file
name, it makes <var class="var">template</var> an empty string and returns that.  If
<var class="var">template</var> does not end with &lsquo;<samp class="samp">XXXXXX</samp>&rsquo;, <code class="code">mktemp</code> returns a
null pointer.
</p>
<p><strong class="strong">Warning:</strong> Between the time the pathname is constructed and the
file is created another process might have created a file with the same
name using <code class="code">mktemp</code>, leading to a possible security hole.  The
implementation generates names which can hardly be predicted, but when
opening the file you should use the <code class="code">O_EXCL</code> flag.  Using
<code class="code">mkstemp</code> is a safe way to avoid this problem.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mkstemp"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">mkstemp</strong> <code class="def-code-arguments">(char *<var class="var">template</var>)</code><a class="copiable-link" href='#index-mkstemp'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">mkstemp</code> function generates a unique file name just as
<code class="code">mktemp</code> does, but it also opens the file for you with <code class="code">open</code>
(see <a class="pxref" href="Opening-and-Closing-Files.html">Opening and Closing Files</a>).  If successful, it modifies
<var class="var">template</var> in place and returns a file descriptor for that file open
for reading and writing.  If <code class="code">mkstemp</code> cannot create a
uniquely-named file, it returns <code class="code">-1</code>.  If <var class="var">template</var> does not
end with &lsquo;<samp class="samp">XXXXXX</samp>&rsquo;, <code class="code">mkstemp</code> returns <code class="code">-1</code> and does not
modify <var class="var">template</var>.
</p>
<p>The file is opened using mode <code class="code">0600</code>.  If the file is meant to be
used by other users this mode must be changed explicitly.
</p></dd></dl>

<p>Unlike <code class="code">mktemp</code>, <code class="code">mkstemp</code> is actually guaranteed to create a
unique file that cannot possibly clash with any other program trying to
create a temporary file.  This is because it works by calling
<code class="code">open</code> with the <code class="code">O_EXCL</code> flag, which says you want to create a
new file and get an error if the file already exists.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mkdtemp"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">mkdtemp</strong> <code class="def-code-arguments">(char *<var class="var">template</var>)</code><a class="copiable-link" href='#index-mkdtemp'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">mkdtemp</code> function creates a directory with a unique name.  If
it succeeds, it overwrites <var class="var">template</var> with the name of the
directory, and returns <var class="var">template</var>.  As with <code class="code">mktemp</code> and
<code class="code">mkstemp</code>, <var class="var">template</var> should be a string ending with
&lsquo;<samp class="samp">XXXXXX</samp>&rsquo;.
</p>
<p>If <code class="code">mkdtemp</code> cannot create an uniquely named directory, it returns
<code class="code">NULL</code> and sets <code class="code">errno</code> appropriately.  If <var class="var">template</var> does
not end with &lsquo;<samp class="samp">XXXXXX</samp>&rsquo;, <code class="code">mkdtemp</code> returns <code class="code">NULL</code> and does
not modify <var class="var">template</var>.  <code class="code">errno</code> will be set to <code class="code">EINVAL</code> in
this case.
</p>
<p>The directory is created using mode <code class="code">0700</code>.
</p></dd></dl>

<p>The directory created by <code class="code">mkdtemp</code> cannot clash with temporary
files or directories created by other users.  This is because directory
creation always works like <code class="code">open</code> with <code class="code">O_EXCL</code>.
See <a class="xref" href="Creating-Directories.html">Creating Directories</a>.
</p>
<p>The <code class="code">mkdtemp</code> function comes from OpenBSD.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Making-Special-Files.html">Making Special Files</a>, Up: <a href="File-System-Interface.html">File System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
