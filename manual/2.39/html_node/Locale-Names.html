<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Locale Names (The GNU C Library)</title>

<meta name="description" content="Locale Names (The GNU C Library)">
<meta name="keywords" content="Locale Names (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Locales.html" rel="up" title="Locales">
<link href="Locale-Information.html" rel="next" title="Locale Information">
<link href="Standard-Locales.html" rel="prev" title="Standard Locales">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Locale-Names">
<div class="nav-panel">
<p>
Next: <a href="Locale-Information.html" accesskey="n" rel="next">Accessing Locale Information</a>, Previous: <a href="Standard-Locales.html" accesskey="p" rel="prev">Standard Locales</a>, Up: <a href="Locales.html" accesskey="u" rel="up">Locales and Internationalization</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Locale-Names-1">7.6 Locale Names</h3>

<p>The following command prints a list of locales supported by the
system:
</p>
<a class="index-entry-id" id="index-locale-1"></a>
<div class="example smallexample">
<pre class="example-preformatted">  locale -a
</pre></div>

<p><strong class="strong">Portability Note:</strong> With the notable exception of the standard
locale names &lsquo;<samp class="samp">C</samp>&rsquo; and &lsquo;<samp class="samp">POSIX</samp>&rsquo;, locale names are
system-specific.
</p>
<p>Most locale names follow XPG syntax and consist of up to four parts:
</p>
<div class="example smallexample">
<pre class="example-preformatted"><var class="var">language</var>[_<var class="var">territory</var>[.<var class="var">codeset</var>]][@<var class="var">modifier</var>]
</pre></div>

<p>Beside the first part, all of them are allowed to be missing.  If the
full specified locale is not found, less specific ones are looked for.
The various parts will be stripped off, in the following order:
</p>
<ol class="enumerate">
<li> codeset
</li><li> normalized codeset
</li><li> territory
</li><li> modifier
</li></ol>

<p>For example, the locale name &lsquo;<samp class="samp">de_AT.iso885915@euro</samp>&rsquo; denotes a
German-language locale for use in Austria, using the ISO-8859-15
(Latin-9) character set, and with the Euro as the currency symbol.
</p>
<p>In addition to locale names which follow XPG syntax, systems may
provide aliases such as &lsquo;<samp class="samp">german</samp>&rsquo;.  Both categories of names must
not contain the slash character &lsquo;<samp class="samp">/</samp>&rsquo;.
</p>
<p>If the locale name starts with a slash &lsquo;<samp class="samp">/</samp>&rsquo;, it is treated as a
path relative to the configured locale directories; see <code class="code">LOCPATH</code>
below.  The specified path must not contain a component &lsquo;<samp class="samp">..</samp>&rsquo;, or
the name is invalid, and <code class="code">setlocale</code> will fail.
</p>
<p><strong class="strong">Portability Note:</strong> POSIX suggests that if a locale name starts
with a slash &lsquo;<samp class="samp">/</samp>&rsquo;, it is resolved as an absolute path.  However,
the GNU C Library treats it as a relative path under the directories listed
in <code class="code">LOCPATH</code> (or the default locale directory if <code class="code">LOCPATH</code>
is unset).
</p>
<p>Locale names which are longer than an implementation-defined limit are
invalid and cause <code class="code">setlocale</code> to fail.
</p>
<p>As a special case, locale names used with <code class="code">LC_ALL</code> can combine
several locales, reflecting different locale settings for different
categories.  For example, you might want to use a U.S. locale with ISO
A4 paper format, so you set <code class="code">LANG</code> to &lsquo;<samp class="samp">en_US.UTF-8</samp>&rsquo;, and
<code class="code">LC_PAPER</code> to &lsquo;<samp class="samp">de_DE.UTF-8</samp>&rsquo;.  In this case, the
<code class="code">LC_ALL</code>-style combined locale name is
</p>
<div class="example smallexample">
<pre class="example-preformatted">LC_CTYPE=en_US.UTF-8;LC_TIME=en_US.UTF-8;LC_PAPER=de_DE.UTF-8;&hellip;
</pre></div>

<p>followed by other category settings not shown here.
</p>
<a class="index-entry-id" id="index-LOCPATH"></a>
<p>The path used for finding locale data can be set using the
<code class="code">LOCPATH</code> environment variable.  This variable lists the
directories in which to search for locale definitions, separated by a
colon &lsquo;<samp class="samp">:</samp>&rsquo;.
</p>
<p>The default path for finding locale data is system specific.  A typical
value for the <code class="code">LOCPATH</code> default is:
</p>
<div class="example smallexample">
<pre class="example-preformatted">/usr/share/locale
</pre></div>

<p>The value of <code class="code">LOCPATH</code> is ignored by privileged programs for
security reasons, and only the default directory is used.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Locale-Information.html">Accessing Locale Information</a>, Previous: <a href="Standard-Locales.html">Standard Locales</a>, Up: <a href="Locales.html">Locales and Internationalization</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
