<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Merged Signals (The GNU C Library)</title>

<meta name="description" content="Merged Signals (The GNU C Library)">
<meta name="keywords" content="Merged Signals (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Defining-Handlers.html" rel="up" title="Defining Handlers">
<link href="Nonreentrancy.html" rel="next" title="Nonreentrancy">
<link href="Signals-in-Handler.html" rel="prev" title="Signals in Handler">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Merged-Signals">
<div class="nav-panel">
<p>
Next: <a href="Nonreentrancy.html" accesskey="n" rel="next">Signal Handling and Nonreentrant Functions</a>, Previous: <a href="Signals-in-Handler.html" accesskey="p" rel="prev">Signals Arriving While a Handler Runs</a>, Up: <a href="Defining-Handlers.html" accesskey="u" rel="up">Defining Signal Handlers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Signals-Close-Together-Merge-into-One">25.4.5 Signals Close Together Merge into One</h4>
<a class="index-entry-id" id="index-handling-multiple-signals"></a>
<a class="index-entry-id" id="index-successive-signals"></a>
<a class="index-entry-id" id="index-merging-of-signals"></a>

<p>If multiple signals of the same type are delivered to your process
before your signal handler has a chance to be invoked at all, the
handler may only be invoked once, as if only a single signal had
arrived.  In effect, the signals merge into one.  This situation can
arise when the signal is blocked, or in a multiprocessing environment
where the system is busy running some other processes while the signals
are delivered.  This means, for example, that you cannot reliably use a
signal handler to count signals.  The only distinction you can reliably
make is whether at least one signal has arrived since a given time in
the past.
</p>
<p>Here is an example of a handler for <code class="code">SIGCHLD</code> that compensates for
the fact that the number of signals received may not equal the number of
child processes that generate them.  It assumes that the program keeps track
of all the child processes with a chain of structures as follows:
</p>
<div class="example smallexample">
<pre class="example-preformatted">struct process
{
  struct process *next;
  /* <span class="r">The process ID of this child.</span>  */
  int pid;
  /* <span class="r">The descriptor of the pipe or pseudo terminal</span>
     <span class="r">on which output comes from this child.</span>  */
  int input_descriptor;
  /* <span class="r">Nonzero if this process has stopped or terminated.</span>  */
  sig_atomic_t have_status;
  /* <span class="r">The status of this child; 0 if running,</span>
     <span class="r">otherwise a status value from <code class="code">waitpid</code>.</span>  */
  int status;
};

struct process *process_list;
</pre></div>

<p>This example also uses a flag to indicate whether signals have arrived
since some time in the past&mdash;whenever the program last cleared it to
zero.
</p>
<div class="example smallexample">
<pre class="example-preformatted">/* <span class="r">Nonzero means some child&rsquo;s status has changed</span>
   <span class="r">so look at <code class="code">process_list</code> for the details.</span>  */
int process_status_change;
</pre></div>

<p>Here is the handler itself:
</p>
<div class="example smallexample">
<pre class="example-preformatted">void
sigchld_handler (int signo)
{
  int old_errno = errno;

  while (1) {
    register int pid;
    int w;
    struct process *p;

    /* <span class="r">Keep asking for a status until we get a definitive result.</span>  */
    do
      {
        errno = 0;
        pid = waitpid (WAIT_ANY, &amp;w, WNOHANG | WUNTRACED);
      }
    while (pid &lt;= 0 &amp;&amp; errno == EINTR);

    if (pid &lt;= 0) {
      /* <span class="r">A real failure means there are no more</span>
         <span class="r">stopped or terminated child processes, so return.</span>  */
      errno = old_errno;
      return;
    }

    /* <span class="r">Find the process that signaled us, and record its status.</span>  */

    for (p = process_list; p; p = p-&gt;next)
      if (p-&gt;pid == pid) {
        p-&gt;status = w;
        /* <span class="r">Indicate that the <code class="code">status</code> field</span>
           <span class="r">has data to look at.  We do this only after storing it.</span>  */
        p-&gt;have_status = 1;

        /* <span class="r">If process has terminated, stop waiting for its output.</span>  */
        if (WIFSIGNALED (w) || WIFEXITED (w))
          if (p-&gt;input_descriptor)
            FD_CLR (p-&gt;input_descriptor, &amp;input_wait_mask);

        /* <span class="r">The program should check this flag from time to time</span>
           <span class="r">to see if there is any news in <code class="code">process_list</code>.</span>  */
        ++process_status_change;
      }

    /* <span class="r">Loop around to handle all the processes</span>
       <span class="r">that have something to tell us.</span>  */
  }
}
</pre></div>

<p>Here is the proper way to check the flag <code class="code">process_status_change</code>:
</p>
<div class="example smallexample">
<pre class="example-preformatted">if (process_status_change) {
  struct process *p;
  process_status_change = 0;
  for (p = process_list; p; p = p-&gt;next)
    if (p-&gt;have_status) {
      &hellip; <span class="r">Examine <code class="code">p-&gt;status</code></span> &hellip;
    }
}
</pre></div>

<p>It is vital to clear the flag before examining the list; otherwise, if a
signal were delivered just before the clearing of the flag, and after
the appropriate element of the process list had been checked, the status
change would go unnoticed until the next signal arrived to set the flag
again.  You could, of course, avoid this problem by blocking the signal
while scanning the list, but it is much more elegant to guarantee
correctness by doing things in the right order.
</p>
<p>The loop which checks process status avoids examining <code class="code">p-&gt;status</code>
until it sees that status has been validly stored.  This is to make sure
that the status cannot change in the middle of accessing it.  Once
<code class="code">p-&gt;have_status</code> is set, it means that the child process is stopped
or terminated, and in either case, it cannot stop or terminate again
until the program has taken notice.  See <a class="xref" href="Atomic-Usage.html">Atomic Usage Patterns</a>, for more
information about coping with interruptions during accesses of a
variable.
</p>
<p>Here is another way you can test whether the handler has run since the
last time you checked.  This technique uses a counter which is never
changed outside the handler.  Instead of clearing the count, the program
remembers the previous value and sees whether it has changed since the
previous check.  The advantage of this method is that different parts of
the program can check independently, each part checking whether there
has been a signal since that part last checked.
</p>
<div class="example smallexample">
<pre class="example-preformatted">sig_atomic_t process_status_change;

sig_atomic_t last_process_status_change;

&hellip;
{
  sig_atomic_t prev = last_process_status_change;
  last_process_status_change = process_status_change;
  if (last_process_status_change != prev) {
    struct process *p;
    for (p = process_list; p; p = p-&gt;next)
      if (p-&gt;have_status) {
        &hellip; <span class="r">Examine <code class="code">p-&gt;status</code></span> &hellip;
      }
  }
}
</pre></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Nonreentrancy.html">Signal Handling and Nonreentrant Functions</a>, Previous: <a href="Signals-in-Handler.html">Signals Arriving While a Handler Runs</a>, Up: <a href="Defining-Handlers.html">Defining Signal Handlers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
