<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Terminal Access Functions (The GNU C Library)</title>

<meta name="description" content="Terminal Access Functions (The GNU C Library)">
<meta name="keywords" content="Terminal Access Functions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Functions-for-Job-Control.html" rel="up" title="Functions for Job Control">
<link href="Process-Group-Functions.html" rel="prev" title="Process Group Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Terminal-Access-Functions">
<div class="nav-panel">
<p>
Previous: <a href="Process-Group-Functions.html" accesskey="p" rel="prev">Process Group Functions</a>, Up: <a href="Functions-for-Job-Control.html" accesskey="u" rel="up">Functions for Job Control</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Functions-for-Controlling-Terminal-Access">29.6.3 Functions for Controlling Terminal Access</h4>

<p>These are the functions for reading or setting the foreground
process group of a terminal.  You should include the header files
<samp class="file">sys/types.h</samp> and <samp class="file">unistd.h</samp> in your application to use
these functions.
<a class="index-entry-id" id="index-unistd_002eh-21"></a>
<a class="index-entry-id" id="index-sys_002ftypes_002eh-3"></a>
</p>
<p>Although these functions take a file descriptor argument to specify
the terminal device, the foreground job is associated with the terminal
file itself and not a particular open file descriptor.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tcgetpgrp"><span class="category-def">Function: </span><span><code class="def-type">pid_t</code> <strong class="def-name">tcgetpgrp</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>)</code><a class="copiable-link" href='#index-tcgetpgrp'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function returns the process group ID of the foreground process
group associated with the terminal open on descriptor <var class="var">filedes</var>.
</p>
<p>If there is no foreground process group, the return value is a number
greater than <code class="code">1</code> that does not match the process group ID of any
existing process group.  This can happen if all of the processes in the
job that was formerly the foreground job have terminated, and no other
job has yet been moved into the foreground.
</p>
<p>In case of an error, a value of <code class="code">-1</code> is returned.  The
following <code class="code">errno</code> error conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is not a valid file descriptor.
</p>
</dd>
<dt><code class="code">ENOSYS</code></dt>
<dd><p>The system doesn&rsquo;t support job control.
</p>
</dd>
<dt><code class="code">ENOTTY</code></dt>
<dd><p>The terminal file associated with the <var class="var">filedes</var> argument isn&rsquo;t the
controlling terminal of the calling process.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tcsetpgrp"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">tcsetpgrp</strong> <code class="def-code-arguments">(int <var class="var">filedes</var>, pid_t <var class="var">pgid</var>)</code><a class="copiable-link" href='#index-tcsetpgrp'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is used to set a terminal&rsquo;s foreground process group ID.
The argument <var class="var">filedes</var> is a descriptor which specifies the terminal;
<var class="var">pgid</var> specifies the process group.  The calling process must be a
member of the same session as <var class="var">pgid</var> and must have the same
controlling terminal.
</p>
<p>For terminal access purposes, this function is treated as output.  If it
is called from a background process on its controlling terminal,
normally all processes in the process group are sent a <code class="code">SIGTTOU</code>
signal.  The exception is if the calling process itself is ignoring or
blocking <code class="code">SIGTTOU</code> signals, in which case the operation is
performed and no signal is sent.
</p>
<p>If successful, <code class="code">tcsetpgrp</code> returns <code class="code">0</code>.  A return value of
<code class="code">-1</code> indicates an error.  The following <code class="code">errno</code> error
conditions are defined for this function:
</p>
<dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is not a valid file descriptor.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>The <var class="var">pgid</var> argument is not valid.
</p>
</dd>
<dt><code class="code">ENOSYS</code></dt>
<dd><p>The system doesn&rsquo;t support job control.
</p>
</dd>
<dt><code class="code">ENOTTY</code></dt>
<dd><p>The <var class="var">filedes</var> isn&rsquo;t the controlling terminal of the calling process.
</p>
</dd>
<dt><code class="code">EPERM</code></dt>
<dd><p>The <var class="var">pgid</var> isn&rsquo;t a process group in the same session as the calling
process.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-tcgetsid"><span class="category-def">Function: </span><span><code class="def-type">pid_t</code> <strong class="def-name">tcgetsid</strong> <code class="def-code-arguments">(int <var class="var">fildes</var>)</code><a class="copiable-link" href='#index-tcgetsid'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is used to obtain the process group ID of the session
for which the terminal specified by <var class="var">fildes</var> is the controlling terminal.
If the call is successful the group ID is returned.  Otherwise the
return value is <code class="code">(pid_t) -1</code> and the global variable <code class="code">errno</code>
is set to the following value:
</p><dl class="table">
<dt><code class="code">EBADF</code></dt>
<dd><p>The <var class="var">filedes</var> argument is not a valid file descriptor.
</p>
</dd>
<dt><code class="code">ENOTTY</code></dt>
<dd><p>The calling process does not have a controlling terminal, or the file
is not the controlling terminal.
</p></dd>
</dl>
</dd></dl>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Process-Group-Functions.html">Process Group Functions</a>, Up: <a href="Functions-for-Job-Control.html">Functions for Job Control</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
