<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Memory Protection (The GNU C Library)</title>

<meta name="description" content="Memory Protection (The GNU C Library)">
<meta name="keywords" content="Memory Protection (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Memory.html" rel="up" title="Memory">
<link href="Locking-Pages.html" rel="next" title="Locking Pages">
<link href="Resizing-the-Data-Segment.html" rel="prev" title="Resizing the Data Segment">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Memory-Protection">
<div class="nav-panel">
<p>
Next: <a href="Locking-Pages.html" accesskey="n" rel="next">Locking Pages</a>, Previous: <a href="Resizing-the-Data-Segment.html" accesskey="p" rel="prev">Resizing the Data Segment</a>, Up: <a href="Memory.html" accesskey="u" rel="up">Virtual Memory Allocation And Paging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Memory-Protection-1">3.4 Memory Protection</h3>
<a class="index-entry-id" id="index-memory-protection"></a>
<a class="index-entry-id" id="index-page-protection"></a>
<a class="index-entry-id" id="index-protection-flags"></a>

<p>When a page is mapped using <code class="code">mmap</code>, page protection flags can be
specified using the protection flags argument.  See <a class="xref" href="Memory_002dmapped-I_002fO.html">Memory-mapped I/O</a>.
</p>
<p>The following flags are available:
</p>
<dl class="vtable">
<dt id='index-PROT_005fWRITE'><span><code class="code">PROT_WRITE</code><a class="copiable-link" href='#index-PROT_005fWRITE'> &para;</a></span></dt>
<dd>
<p>The memory can be written to.
</p>
</dd>
<dt id='index-PROT_005fREAD'><span><code class="code">PROT_READ</code><a class="copiable-link" href='#index-PROT_005fREAD'> &para;</a></span></dt>
<dd>
<p>The memory can be read.  On some architectures, this flag implies that
the memory can be executed as well (as if <code class="code">PROT_EXEC</code> had been
specified at the same time).
</p>
</dd>
<dt id='index-PROT_005fEXEC'><span><code class="code">PROT_EXEC</code><a class="copiable-link" href='#index-PROT_005fEXEC'> &para;</a></span></dt>
<dd>
<p>The memory can be used to store instructions which can then be executed.
On most architectures, this flag implies that the memory can be read (as
if <code class="code">PROT_READ</code> had been specified).
</p>
</dd>
<dt id='index-PROT_005fNONE'><span><code class="code">PROT_NONE</code><a class="copiable-link" href='#index-PROT_005fNONE'> &para;</a></span></dt>
<dd>
<p>This flag must be specified on its own.
</p>
<p>The memory is reserved, but cannot be read, written, or executed.  If
this flag is specified in a call to <code class="code">mmap</code>, a virtual memory area
will be set aside for future use in the process, and <code class="code">mmap</code> calls
without the <code class="code">MAP_FIXED</code> flag will not use it for subsequent
allocations.  For anonymous mappings, the kernel will not reserve any
physical memory for the allocation at the time the mapping is created.
</p></dd>
</dl>

<p>The operating system may keep track of these flags separately even if
the underlying hardware treats them the same for the purposes of access
checking (as happens with <code class="code">PROT_READ</code> and <code class="code">PROT_EXEC</code> on some
platforms).  On GNU systems, <code class="code">PROT_EXEC</code> always implies
<code class="code">PROT_READ</code>, so that users can view the machine code which is
executing on their system.
</p>
<p>Inappropriate access will cause a segfault (see <a class="pxref" href="Program-Error-Signals.html">Program Error Signals</a>).
</p>
<p>After allocation, protection flags can be changed using the
<code class="code">mprotect</code> function.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-mprotect"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">mprotect</strong> <code class="def-code-arguments">(void *<var class="var">address</var>, size_t <var class="var">length</var>, int <var class="var">protection</var>)</code><a class="copiable-link" href='#index-mprotect'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>A successful call to the <code class="code">mprotect</code> function changes the protection
flags of at least <var class="var">length</var> bytes of memory, starting at
<var class="var">address</var>.
</p>
<p><var class="var">address</var> must be aligned to the page size for the mapping.  The
system page size can be obtained by calling <code class="code">sysconf</code> with the
<code class="code">_SC_PAGESIZE</code> parameter (see <a class="pxref" href="Sysconf-Definition.html">Definition of <code class="code">sysconf</code></a>).  The system
page size is the granularity in which the page protection of anonymous
memory mappings and most file mappings can be changed.  Memory which is
mapped from special files or devices may have larger page granularity
than the system page size and may require larger alignment.
</p>
<p><var class="var">length</var> is the number of bytes whose protection flags must be
changed.  It is automatically rounded up to the next multiple of the
system page size.
</p>
<p><var class="var">protection</var> is a combination of the <code class="code">PROT_*</code> flags described
above.
</p>
<p>The <code class="code">mprotect</code> function returns <em class="math">0</em> on success and <em class="math">-1</em>
on failure.
</p>
<p>The following <code class="code">errno</code> error conditions are defined for this
function:
</p>
<dl class="table">
<dt><code class="code">ENOMEM</code></dt>
<dd><p>The system was not able to allocate resources to fulfill the request.
This can happen if there is not enough physical memory in the system for
the allocation of backing storage.  The error can also occur if the new
protection flags would cause the memory region to be split from its
neighbors, and the process limit for the number of such distinct memory
regions would be exceeded.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p><var class="var">address</var> is not properly aligned to a page boundary for the
mapping, or <var class="var">length</var> (after rounding up to the system page size) is
not a multiple of the applicable page size for the mapping, or the
combination of flags in <var class="var">protection</var> is not valid.
</p>
</dd>
<dt><code class="code">EACCES</code></dt>
<dd><p>The file for a file-based mapping was not opened with open flags which
are compatible with <var class="var">protection</var>.
</p>
</dd>
<dt><code class="code">EPERM</code></dt>
<dd><p>The system security policy does not allow a mapping with the specified
flags.  For example, mappings which are both <code class="code">PROT_EXEC</code> and
<code class="code">PROT_WRITE</code> at the same time might not be allowed.
</p></dd>
</dl>
</dd></dl>

<p>If the <code class="code">mprotect</code> function is used to make a region of memory
inaccessible by specifying the <code class="code">PROT_NONE</code> protection flag and
access is later restored, the memory retains its previous contents.
</p>
<p>On some systems, it may not be possible to specify additional flags
which were not present when the mapping was first created.  For example,
an attempt to make a region of memory executable could fail if the
initial protection flags were &lsquo;<samp class="samp">PROT_READ | PROT_WRITE</samp>&rsquo;.
</p>
<p>In general, the <code class="code">mprotect</code> function can be used to change any
process memory, no matter how it was allocated.  However, portable use
of the function requires that it is only used with memory regions
returned by <code class="code">mmap</code> or <code class="code">mmap64</code>.
</p>
<ul class="mini-toc">
<li><a href="#Memory-Protection-Keys" accesskey="1">Memory Protection Keys</a></li>
</ul>
<div class="subsection-level-extent" id="Memory-Protection-Keys">
<h4 class="subsection">3.4.1 Memory Protection Keys</h4>

<a class="index-entry-id" id="index-memory-protection-key"></a>
<a class="index-entry-id" id="index-protection-key"></a>
<a class="index-entry-id" id="index-MPK"></a>
<p>On some systems, further restrictions can be added to specific pages
using <em class="dfn">memory protection keys</em>.  These restrictions work as follows:
</p>
<ul class="itemize mark-bullet">
<li>All memory pages are associated with a protection key.  The default
protection key does not cause any additional protections to be applied
during memory accesses.  New keys can be allocated with the
<code class="code">pkey_alloc</code> function, and applied to pages using
<code class="code">pkey_mprotect</code>.

</li><li>Each thread has a set of separate access right restriction for each
protection key.  These access rights can be manipulated using the
<code class="code">pkey_set</code> and <code class="code">pkey_get</code> functions.

</li><li>During a memory access, the system obtains the protection key for the
accessed page and uses that to determine the applicable access rights,
as configured for the current thread.  If the access is restricted, a
segmentation fault is the result ((see <a class="pxref" href="Program-Error-Signals.html">Program Error Signals</a>).
These checks happen in addition to the <code class="code">PROT_</code>* protection flags
set by <code class="code">mprotect</code> or <code class="code">pkey_mprotect</code>.
</li></ul>

<p>New threads and subprocesses inherit the access rights of the current
thread.  If a protection key is allocated subsequently, existing threads
(except the current) will use an unspecified system default for the
access rights associated with newly allocated keys.
</p>
<p>Upon entering a signal handler, the system resets the access rights of
the current thread so that pages with the default key can be accessed,
but the access rights for other protection keys are unspecified.
</p>
<p>Applications are expected to allocate a key once using
<code class="code">pkey_alloc</code>, and apply the key to memory regions which need
special protection with <code class="code">pkey_mprotect</code>:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  int key = pkey_alloc (0, PKEY_DISABLE_ACCESS);
  if (key &lt; 0)
    /* Perform error checking, including fallback for lack of support.  */
    ...;

  /* Apply the key to a special memory region used to store critical
     data.  */
  if (pkey_mprotect (region, region_length,
                     PROT_READ | PROT_WRITE, key) &lt; 0)
    ...; /* Perform error checking (generally fatal).  */
</pre></div>

<p>If the key allocation fails due to lack of support for memory protection
keys, the <code class="code">pkey_mprotect</code> call can usually be skipped.  In this
case, the region will not be protected by default.  It is also possible
to call <code class="code">pkey_mprotect</code> with a key value of <em class="math">-1</em>, in which
case it will behave in the same way as <code class="code">mprotect</code>.
</p>
<p>After key allocation assignment to memory pages, <code class="code">pkey_set</code> can be
used to temporarily acquire access to the memory region and relinquish
it again:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  if (key &gt;= 0 &amp;&amp; pkey_set (key, 0) &lt; 0)
    ...; /* Perform error checking (generally fatal).  */
  /* At this point, the current thread has read-write access to the
     memory region.  */
  ...
  /* Revoke access again.  */
  if (key &gt;= 0 &amp;&amp; pkey_set (key, PKEY_DISABLE_ACCESS) &lt; 0)
    ...; /* Perform error checking (generally fatal).  */
</pre></div>

<p>In this example, a negative key value indicates that no key had been
allocated, which means that the system lacks support for memory
protection keys and it is not necessary to change the the access rights
of the current thread (because it always has access).
</p>
<p>Compared to using <code class="code">mprotect</code> to change the page protection flags,
this approach has two advantages: It is thread-safe in the sense that
the access rights are only changed for the current thread, so another
thread which changes its own access rights concurrently to gain access
to the mapping will not suddenly see its access rights revoked.  And
<code class="code">pkey_set</code> typically does not involve a call into the kernel and a
context switch, so it is more efficient.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pkey_005falloc"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">pkey_alloc</strong> <code class="def-code-arguments">(unsigned int <var class="var">flags</var>, unsigned int <var class="var">restrictions</var>)</code><a class="copiable-link" href='#index-pkey_005falloc'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Unsafe corrupt
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Allocate a new protection key.  The <var class="var">flags</var> argument is reserved and
must be zero.  The <var class="var">restrictions</var> argument specifies access rights
which are applied to the current thread (as if with <code class="code">pkey_set</code>
below).  Access rights of other threads are not changed.
</p>
<p>The function returns the new protection key, a non-negative number, or
<em class="math">-1</em> on error.
</p>
<p>The following <code class="code">errno</code> error conditions are defined for this
function:
</p>
<dl class="table">
<dt><code class="code">ENOSYS</code></dt>
<dd><p>The system does not implement memory protection keys.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>The <var class="var">flags</var> argument is not zero.
</p>
<p>The <var class="var">restrictions</var> argument is invalid.
</p>
<p>The system does not implement memory protection keys or runs in a mode
in which memory protection keys are disabled.
</p>
</dd>
<dt><code class="code">ENOSPC</code></dt>
<dd><p>All available protection keys already have been allocated.
</p>
<p>The system does not implement memory protection keys or runs in a mode
in which memory protection keys are disabled.
</p>
</dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pkey_005ffree"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">pkey_free</strong> <code class="def-code-arguments">(int <var class="var">key</var>)</code><a class="copiable-link" href='#index-pkey_005ffree'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Deallocate the protection key, so that it can be reused by
<code class="code">pkey_alloc</code>.
</p>
<p>Calling this function does not change the access rights of the freed
protection key.  The calling thread and other threads may retain access
to it, even if it is subsequently allocated again.  For this reason, it
is not recommended to call the <code class="code">pkey_free</code> function.
</p>
<dl class="table">
<dt><code class="code">ENOSYS</code></dt>
<dd><p>The system does not implement memory protection keys.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>The <var class="var">key</var> argument is not a valid protection key.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pkey_005fmprotect"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">pkey_mprotect</strong> <code class="def-code-arguments">(void *<var class="var">address</var>, size_t <var class="var">length</var>, int <var class="var">protection</var>, int <var class="var">key</var>)</code><a class="copiable-link" href='#index-pkey_005fmprotect'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Similar to <code class="code">mprotect</code>, but also set the memory protection key for
the memory region to <code class="code">key</code>.
</p>
<p>Some systems use memory protection keys to emulate certain combinations
of <var class="var">protection</var> flags.  Under such circumstances, specifying an
explicit protection key may behave as if additional flags have been
specified in <var class="var">protection</var>, even though this does not happen with the
default protection key.  For example, some systems can support
<code class="code">PROT_EXEC</code>-only mappings only with a default protection key, and
memory with a key which was allocated using <code class="code">pkey_alloc</code> will still
be readable if <code class="code">PROT_EXEC</code> is specified without <code class="code">PROT_READ</code>.
</p>
<p>If <var class="var">key</var> is <em class="math">-1</em>, the default protection key is applied to the
mapping, just as if <code class="code">mprotect</code> had been called.
</p>
<p>The <code class="code">pkey_mprotect</code> function returns <em class="math">0</em> on success and
<em class="math">-1</em> on failure.  The same <code class="code">errno</code> error conditions as for
<code class="code">mprotect</code> are defined for this function, with the following
addition:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The <var class="var">key</var> argument is not <em class="math">-1</em> or a valid memory protection
key allocated using <code class="code">pkey_alloc</code>.
</p>
</dd>
<dt><code class="code">ENOSYS</code></dt>
<dd><p>The system does not implement memory protection keys, and <var class="var">key</var> is
not <em class="math">-1</em>.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pkey_005fset"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">pkey_set</strong> <code class="def-code-arguments">(int <var class="var">key</var>, unsigned int <var class="var">rights</var>)</code><a class="copiable-link" href='#index-pkey_005fset'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Change the access rights of the current thread for memory pages with the
protection key <var class="var">key</var> to <var class="var">rights</var>.  If <var class="var">rights</var> is zero, no
additional access restrictions on top of the page protection flags are
applied.  Otherwise, <var class="var">rights</var> is a combination of the following
flags:
</p>
<dl class="vtable">
<dt id='index-PKEY_005fDISABLE_005fWRITE'><span><code class="code">PKEY_DISABLE_WRITE</code><a class="copiable-link" href='#index-PKEY_005fDISABLE_005fWRITE'> &para;</a></span></dt>
<dd>
<p>Subsequent attempts to write to memory with the specified protection
key will fault.
</p>
</dd>
<dt id='index-PKEY_005fDISABLE_005fACCESS'><span><code class="code">PKEY_DISABLE_ACCESS</code><a class="copiable-link" href='#index-PKEY_005fDISABLE_005fACCESS'> &para;</a></span></dt>
<dd>
<p>Subsequent attempts to write to or read from memory with the specified
protection key will fault.
</p></dd>
</dl>

<p>Operations not specified as flags are not restricted.  In particular,
this means that the memory region will remain executable if it was
mapped with the <code class="code">PROT_EXEC</code> protection flag and
<code class="code">PKEY_DISABLE_ACCESS</code> has been specified.
</p>
<p>Calling the <code class="code">pkey_set</code> function with a protection key which was not
allocated by <code class="code">pkey_alloc</code> results in undefined behavior.  This
means that calling this function on systems which do not support memory
protection keys is undefined.
</p>
<p>The <code class="code">pkey_set</code> function returns <em class="math">0</em> on success and <em class="math">-1</em>
on failure.
</p>
<p>The following <code class="code">errno</code> error conditions are defined for this
function:
</p>
<dl class="table">
<dt><code class="code">EINVAL</code></dt>
<dd><p>The system does not support the access rights restrictions expressed in
the <var class="var">rights</var> argument.
</p></dd>
</dl>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-pkey_005fget"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">pkey_get</strong> <code class="def-code-arguments">(int <var class="var">key</var>)</code><a class="copiable-link" href='#index-pkey_005fget'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Return the access rights of the current thread for memory pages with
protection key <var class="var">key</var>.  The return value is zero or a combination of
the <code class="code">PKEY_DISABLE_</code>* flags; see the <code class="code">pkey_set</code> function.
</p>
<p>Calling the <code class="code">pkey_get</code> function with a protection key which was not
allocated by <code class="code">pkey_alloc</code> results in undefined behavior.  This
means that calling this function on systems which do not support memory
protection keys is undefined.
</p></dd></dl>

</div>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Locking-Pages.html">Locking Pages</a>, Previous: <a href="Resizing-the-Data-Segment.html">Resizing the Data Segment</a>, Up: <a href="Memory.html">Virtual Memory Allocation And Paging</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
