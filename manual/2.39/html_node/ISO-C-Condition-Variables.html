<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>ISO C Condition Variables (The GNU C Library)</title>

<meta name="description" content="ISO C Condition Variables (The GNU C Library)">
<meta name="keywords" content="ISO C Condition Variables (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="ISO-C-Threads.html" rel="up" title="ISO C Threads">
<link href="ISO-C-Thread_002dlocal-Storage.html" rel="next" title="ISO C Thread-local Storage">
<link href="ISO-C-Mutexes.html" rel="prev" title="ISO C Mutexes">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="ISO-C-Condition-Variables">
<div class="nav-panel">
<p>
Next: <a href="ISO-C-Thread_002dlocal-Storage.html" accesskey="n" rel="next">Thread-local Storage</a>, Previous: <a href="ISO-C-Mutexes.html" accesskey="p" rel="prev">Mutexes</a>, Up: <a href="ISO-C-Threads.html" accesskey="u" rel="up">ISO C Threads</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Condition-Variables">36.1.5 Condition Variables</h4>
<a class="index-entry-id" id="index-condvar"></a>
<a class="index-entry-id" id="index-condition-variables"></a>

<p>Mutexes are not the only synchronization mechanisms available.  For
some more complex tasks, the GNU C Library also implements <em class="dfn">condition
variables</em>, which allow the programmer to think at a higher level when
solving complex synchronization problems.  They are used to
synchronize threads waiting on a certain condition to happen.
</p>
<p>The fundamental data type for condition variables is the <code class="code">cnd_t</code>:
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-cnd_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">cnd_t</strong><a class="copiable-link" href='#index-cnd_005ft'> &para;</a></span></dt>
<dd>
<p>The <code class="code">cnd_t</code> uniquely identifies a condition variable object.
</p></dd></dl>

<p>The following functions are used for working with condition variables:
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-cnd_005finit"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">cnd_init</strong> <code class="def-code-arguments">(cnd_t *<var class="var">cond</var>)</code><a class="copiable-link" href='#index-cnd_005finit'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">cnd_init</code> initializes a new condition variable, identified by
<var class="var">cond</var>.
</p>
<p>This function may return <code class="code">thrd_success</code>, <code class="code">thrd_nomem</code>, or
<code class="code">thrd_error</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-cnd_005fsignal"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">cnd_signal</strong> <code class="def-code-arguments">(cnd_t *<var class="var">cond</var>)</code><a class="copiable-link" href='#index-cnd_005fsignal'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">cnd_signal</code> unblocks one thread that is currently waiting on the
condition variable pointed to by <var class="var">cond</var>.  If a thread is
successfully unblocked, this function returns <code class="code">thrd_success</code>.  If
no threads are blocked, this function does nothing and returns
<code class="code">thrd_success</code>.  Otherwise, this function returns
<code class="code">thrd_error</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-cnd_005fbroadcast"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">cnd_broadcast</strong> <code class="def-code-arguments">(cnd_t *<var class="var">cond</var>)</code><a class="copiable-link" href='#index-cnd_005fbroadcast'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">cnd_broadcast</code> unblocks all the threads that are currently
waiting on the condition variable pointed to by <var class="var">cond</var>.  This
function returns <code class="code">thrd_success</code> on success.  If no threads are
blocked, this function does nothing and returns
<code class="code">thrd_success</code>. Otherwise, this function returns
<code class="code">thrd_error</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-cnd_005fwait"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">cnd_wait</strong> <code class="def-code-arguments">(cnd_t *<var class="var">cond</var>, mtx_t *<var class="var">mutex</var>)</code><a class="copiable-link" href='#index-cnd_005fwait'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">cnd_wait</code> atomically unlocks the mutex pointed to by <var class="var">mutex</var>
and blocks on the condition variable pointed to by <var class="var">cond</var> until
the thread is signaled by <code class="code">cnd_signal</code> or <code class="code">cnd_broadcast</code>.
The mutex is locked again before the function returns.
</p>
<p>This function returns either <code class="code">thrd_success</code> or <code class="code">thrd_error</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-cnd_005ftimedwait"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">cnd_timedwait</strong> <code class="def-code-arguments">(cnd_t *restrict <var class="var">cond</var>, mtx_t *restrict <var class="var">mutex</var>, const struct timespec *restrict <var class="var">time_point</var>)</code><a class="copiable-link" href='#index-cnd_005ftimedwait'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe lock
| AC-Unsafe lock
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">cnd_timedwait</code> atomically unlocks the mutex pointed to by
<var class="var">mutex</var> and blocks on the condition variable pointed to by
<var class="var">cond</var> until the thread is signaled by <code class="code">cnd_signal</code> or
<code class="code">cnd_broadcast</code>, or until the calendar time pointed to by
<var class="var">time_point</var> has been reached.  The mutex is locked again before
the function returns.
</p>
<p>As for <code class="code">mtx_timedlock</code>, since this function takes an absolute
time, if a duration is required, the calendar time must be calculated
manually.  See <a class="xref" href="Time-Basics.html">Time Basics</a>, and <a class="ref" href="Calendar-Time.html">Calendar Time</a>.
</p>
<p>This function may return <code class="code">thrd_success</code>, <code class="code">thrd_nomem</code>, or
<code class="code">thrd_error</code>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-cnd_005fdestroy"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">cnd_destroy</strong> <code class="def-code-arguments">(cnd_t *<var class="var">cond</var>)</code><a class="copiable-link" href='#index-cnd_005fdestroy'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code class="code">cnd_destroy</code> destroys the condition variable pointed to by
<var class="var">cond</var>.  If there are threads waiting on <var class="var">cond</var>, the behavior
is undefined.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="ISO-C-Thread_002dlocal-Storage.html">Thread-local Storage</a>, Previous: <a href="ISO-C-Mutexes.html">Mutexes</a>, Up: <a href="ISO-C-Threads.html">ISO C Threads</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
