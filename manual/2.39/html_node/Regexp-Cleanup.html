<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Regexp Cleanup (The GNU C Library)</title>

<meta name="description" content="Regexp Cleanup (The GNU C Library)">
<meta name="keywords" content="Regexp Cleanup (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Regular-Expressions.html" rel="up" title="Regular Expressions">
<link href="Subexpression-Complications.html" rel="prev" title="Subexpression Complications">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Regexp-Cleanup">
<div class="nav-panel">
<p>
Previous: <a href="Subexpression-Complications.html" accesskey="p" rel="prev">Complications in Subexpression Matching</a>, Up: <a href="Regular-Expressions.html" accesskey="u" rel="up">Regular Expression Matching</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="POSIX-Regexp-Matching-Cleanup">10.3.6 POSIX Regexp Matching Cleanup</h4>

<p>When you are finished using a compiled regular expression, you can
free the storage it uses by calling <code class="code">regfree</code>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-regfree"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">regfree</strong> <code class="def-code-arguments">(regex_t *<var class="var">compiled</var>)</code><a class="copiable-link" href='#index-regfree'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>Calling <code class="code">regfree</code> frees all the storage that <code class="code">*<var class="var">compiled</var></code>
points to.  This includes various internal fields of the <code class="code">regex_t</code>
structure that aren&rsquo;t documented in this manual.
</p>
<p><code class="code">regfree</code> does not free the object <code class="code">*<var class="var">compiled</var></code> itself.
</p></dd></dl>

<p>You should always free the space in a <code class="code">regex_t</code> structure with
<code class="code">regfree</code> before using the structure to compile another regular
expression.
</p>
<p>When <code class="code">regcomp</code> or <code class="code">regexec</code> reports an error, you can use
the function <code class="code">regerror</code> to turn it into an error message string.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-regerror"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">regerror</strong> <code class="def-code-arguments">(int <var class="var">errcode</var>, const regex_t *restrict <var class="var">compiled</var>, char *restrict <var class="var">buffer</var>, size_t <var class="var">length</var>)</code><a class="copiable-link" href='#index-regerror'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env
| AS-Unsafe corrupt heap lock dlopen
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function produces an error message string for the error code
<var class="var">errcode</var>, and stores the string in <var class="var">length</var> bytes of memory
starting at <var class="var">buffer</var>.  For the <var class="var">compiled</var> argument, supply the
same compiled regular expression structure that <code class="code">regcomp</code> or
<code class="code">regexec</code> was working with when it got the error.  Alternatively,
you can supply <code class="code">NULL</code> for <var class="var">compiled</var>; you will still get a
meaningful error message, but it might not be as detailed.
</p>
<p>If the error message can&rsquo;t fit in <var class="var">length</var> bytes (including a
terminating null character), then <code class="code">regerror</code> truncates it.
The string that <code class="code">regerror</code> stores is always null-terminated
even if it has been truncated.
</p>
<p>The return value of <code class="code">regerror</code> is the minimum length needed to
store the entire error message.  If this is less than <var class="var">length</var>, then
the error message was not truncated, and you can use it.  Otherwise, you
should call <code class="code">regerror</code> again with a larger buffer.
</p>
<p>Here is a function which uses <code class="code">regerror</code>, but always dynamically
allocates a buffer for the error message:
</p>
<div class="example smallexample">
<pre class="example-preformatted">char *get_regerror (int errcode, regex_t *compiled)
{
  size_t length = regerror (errcode, compiled, NULL, 0);
  char *buffer = xmalloc (length);
  (void) regerror (errcode, compiled, buffer, length);
  return buffer;
}
</pre></div>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Subexpression-Complications.html">Complications in Subexpression Matching</a>, Up: <a href="Regular-Expressions.html">Regular Expression Matching</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
