<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Executing a File (The GNU C Library)</title>

<meta name="description" content="Executing a File (The GNU C Library)">
<meta name="keywords" content="Executing a File (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Processes.html" rel="up" title="Processes">
<link href="Process-Completion.html" rel="next" title="Process Completion">
<link href="Querying-a-Process.html" rel="prev" title="Querying a Process">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Executing-a-File">
<div class="nav-panel">
<p>
Next: <a href="Process-Completion.html" accesskey="n" rel="next">Process Completion</a>, Previous: <a href="Querying-a-Process.html" accesskey="p" rel="prev">Querying a Process</a>, Up: <a href="Processes.html" accesskey="u" rel="up">Processes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Executing-a-File-1">27.6 Executing a File</h3>
<a class="index-entry-id" id="index-executing-a-file"></a>
<a class="index-entry-id" id="index-exec-functions"></a>

<p>This section describes the <code class="code">exec</code> family of functions, for executing
a file as a process image.  You can use these functions to make a child
process execute a new program after it has been forked.
</p>
<p>To see the effects of <code class="code">exec</code> from the point of view of the called
program, see <a class="ref" href="Program-Basics.html">The Basic Program/System Interface</a>.
</p>
<a class="index-entry-id" id="index-unistd_002eh-19"></a>
<p>The functions in this family differ in how you specify the arguments,
but otherwise they all do the same thing.  They are declared in the
header file <samp class="file">unistd.h</samp>.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-execv"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">execv</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, char *const <var class="var">argv</var><code class="t">[]</code>)</code><a class="copiable-link" href='#index-execv'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">execv</code> function executes the file named by <var class="var">filename</var> as a
new process image.
</p>
<p>The <var class="var">argv</var> argument is an array of null-terminated strings that is
used to provide a value for the <code class="code">argv</code> argument to the <code class="code">main</code>
function of the program to be executed.  The last element of this array
must be a null pointer.  By convention, the first element of this array
is the file name of the program sans directory names.  See <a class="xref" href="Program-Arguments.html">Program Arguments</a>, for full details on how programs can access these arguments.
</p>
<p>The environment for the new process image is taken from the
<code class="code">environ</code> variable of the current process image; see
<a class="ref" href="Environment-Variables.html">Environment Variables</a>, for information about environments.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-execl"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">execl</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, const char *<var class="var">arg0</var>, &hellip;)</code><a class="copiable-link" href='#index-execl'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is similar to <code class="code">execv</code>, but the <var class="var">argv</var> strings are
specified individually instead of as an array.  A null pointer must be
passed as the last such argument.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-execve"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">execve</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, char *const <var class="var">argv</var><code class="t">[]</code>, char *const <var class="var">env</var><code class="t">[]</code>)</code><a class="copiable-link" href='#index-execve'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is similar to <code class="code">execv</code>, but permits you to specify the environment
for the new program explicitly as the <var class="var">env</var> argument.  This should
be an array of strings in the same format as for the <code class="code">environ</code>
variable; see <a class="ref" href="Environment-Access.html">Environment Access</a>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-fexecve"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">fexecve</strong> <code class="def-code-arguments">(int <var class="var">fd</var>,  char *const <var class="var">argv</var><code class="t">[]</code>, char *const <var class="var">env</var><code class="t">[]</code>)</code><a class="copiable-link" href='#index-fexecve'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is similar to <code class="code">execve</code>, but instead of identifying the program
executable by its pathname, the file descriptor <var class="var">fd</var> is used.  The
descriptor must have been opened with the <code class="code">O_RDONLY</code> flag or (on
Linux) the <code class="code">O_PATH</code> flag.
</p>
<p>On Linux, <code class="code">fexecve</code> can fail with an error of <code class="code">ENOSYS</code> if
<samp class="file">/proc</samp> has not been mounted and the kernel lacks support for the
underlying <code class="code">execveat</code> system call.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-execle"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">execle</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, const char *<var class="var">arg0</var>, &hellip;, char *const <var class="var">env</var><code class="t">[]</code>)</code><a class="copiable-link" href='#index-execle'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This is similar to <code class="code">execl</code>, but permits you to specify the
environment for the new program explicitly.  The environment argument is
passed following the null pointer that marks the last <var class="var">argv</var>
argument, and should be an array of strings in the same format as for
the <code class="code">environ</code> variable.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-execvp"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">execvp</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, char *const <var class="var">argv</var><code class="t">[]</code>)</code><a class="copiable-link" href='#index-execvp'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">execvp</code> function is similar to <code class="code">execv</code>, except that it
searches the directories listed in the <code class="code">PATH</code> environment variable
(see <a class="pxref" href="Standard-Environment.html">Standard Environment Variables</a>) to find the full file name of a
file from <var class="var">filename</var> if <var class="var">filename</var> does not contain a slash.
</p>
<p>This function is useful for executing system utility programs, because
it looks for them in the places that the user has chosen.  Shells use it
to run the commands that users type.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-execlp"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">execlp</strong> <code class="def-code-arguments">(const char *<var class="var">filename</var>, const char *<var class="var">arg0</var>, &hellip;)</code><a class="copiable-link" href='#index-execlp'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is like <code class="code">execl</code>, except that it performs the same
file name searching as the <code class="code">execvp</code> function.
</p></dd></dl>

<p>The size of the argument list and environment list taken together must
not be greater than <code class="code">ARG_MAX</code> bytes.  See <a class="xref" href="General-Limits.html">General Capacity Limits</a>.  On
GNU/Hurd systems, the size (which compares against <code class="code">ARG_MAX</code>)
includes, for each string, the number of characters in the string, plus
the size of a <code class="code">char *</code>, plus one, rounded up to a multiple of the
size of a <code class="code">char *</code>.  Other systems may have somewhat different
rules for counting.
</p>
<p>These functions normally don&rsquo;t return, since execution of a new program
causes the currently executing program to go away completely.  A value
of <code class="code">-1</code> is returned in the event of a failure.  In addition to the
usual file name errors (see <a class="pxref" href="File-Name-Errors.html">File Name Errors</a>), the following
<code class="code">errno</code> error conditions are defined for these functions:
</p>
<dl class="table">
<dt><code class="code">E2BIG</code></dt>
<dd><p>The combined size of the new program&rsquo;s argument list and environment
list is larger than <code class="code">ARG_MAX</code> bytes.  GNU/Hurd systems have no
specific limit on the argument list size, so this error code cannot
result, but you may get <code class="code">ENOMEM</code> instead if the arguments are too
big for available memory.
</p>
</dd>
<dt><code class="code">ENOEXEC</code></dt>
<dd><p>The specified file can&rsquo;t be executed because it isn&rsquo;t in the right format.
</p>
</dd>
<dt><code class="code">ENOMEM</code></dt>
<dd><p>Executing the specified file requires more storage than is available.
</p></dd>
</dl>

<p>If execution of the new file succeeds, it updates the access time field
of the file as if the file had been read.  See <a class="xref" href="File-Times.html">File Times</a>, for more
details about access times of files.
</p>
<p>The point at which the file is closed again is not specified, but
is at some point before the process exits or before another process
image is executed.
</p>
<p>Executing a new process image completely changes the contents of memory,
copying only the argument and environment strings to new locations.  But
many other attributes of the process are unchanged:
</p>
<ul class="itemize mark-bullet">
<li>The process ID and the parent process ID.  See <a class="xref" href="Process-Creation-Concepts.html">Process Creation Concepts</a>.

</li><li>Session and process group membership.  See <a class="xref" href="Concepts-of-Job-Control.html">Concepts of Job Control</a>.

</li><li>Real user ID and group ID, and supplementary group IDs.  See <a class="xref" href="Process-Persona.html">The Persona of a Process</a>.

</li><li>Pending alarms.  See <a class="xref" href="Setting-an-Alarm.html">Setting an Alarm</a>.

</li><li>Current working directory and root directory.  See <a class="xref" href="Working-Directory.html">Working Directory</a>.  On GNU/Hurd systems, the root directory is not copied when
executing a setuid program; instead the system default root directory
is used for the new program.

</li><li>File mode creation mask.  See <a class="xref" href="Setting-Permissions.html">Assigning File Permissions</a>.

</li><li>Process signal mask; see <a class="ref" href="Process-Signal-Mask.html">Process Signal Mask</a>.

</li><li>Pending signals; see <a class="ref" href="Blocking-Signals.html">Blocking Signals</a>.

</li><li>Elapsed processor time associated with the process; see <a class="ref" href="Processor-Time.html">Processor Time Inquiry</a>.
</li></ul>

<p>If the set-user-ID and set-group-ID mode bits of the process image file
are set, this affects the effective user ID and effective group ID
(respectively) of the process.  These concepts are discussed in detail
in <a class="ref" href="Process-Persona.html">The Persona of a Process</a>.
</p>
<p>Signals that are set to be ignored in the existing process image are
also set to be ignored in the new process image.  All other signals are
set to the default action in the new process image.  For more
information about signals, see <a class="ref" href="Signal-Handling.html">Signal Handling</a>.
</p>
<p>File descriptors open in the existing process image remain open in the
new process image, unless they have the <code class="code">FD_CLOEXEC</code>
(close-on-exec) flag set.  The files that remain open inherit all
attributes of the open file descriptors from the existing process image,
including file locks.  File descriptors are discussed in <a class="ref" href="Low_002dLevel-I_002fO.html">Low-Level Input/Output</a>.
</p>
<p>Streams, by contrast, cannot survive through <code class="code">exec</code> functions,
because they are located in the memory of the process itself.  The new
process image has no streams except those it creates afresh.  Each of
the streams in the pre-<code class="code">exec</code> process image has a descriptor inside
it, and these descriptors do survive through <code class="code">exec</code> (provided that
they do not have <code class="code">FD_CLOEXEC</code> set).  The new process image can
reconnect these to new streams using <code class="code">fdopen</code> (see <a class="pxref" href="Descriptors-and-Streams.html">Descriptors and Streams</a>).
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Process-Completion.html">Process Completion</a>, Previous: <a href="Querying-a-Process.html">Querying a Process</a>, Up: <a href="Processes.html">Processes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
