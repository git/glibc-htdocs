<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Charset conversion in gettext (The GNU C Library)</title>

<meta name="description" content="Charset conversion in gettext (The GNU C Library)">
<meta name="keywords" content="Charset conversion in gettext (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Message-catalogs-with-gettext.html" rel="up" title="Message catalogs with gettext">
<link href="GUI-program-problems.html" rel="next" title="GUI program problems">
<link href="Advanced-gettext-functions.html" rel="prev" title="Advanced gettext functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Charset-conversion-in-gettext">
<div class="nav-panel">
<p>
Next: <a href="GUI-program-problems.html" accesskey="n" rel="next">How to use <code class="code">gettext</code> in GUI programs</a>, Previous: <a href="Advanced-gettext-functions.html" accesskey="p" rel="prev">Additional functions for more complicated situations</a>, Up: <a href="Message-catalogs-with-gettext.html" accesskey="u" rel="up">The <code class="code">gettext</code> family of functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="How-to-specify-the-output-character-set-gettext-uses">8.2.1.4 How to specify the output character set <code class="code">gettext</code> uses</h4>

<p><code class="code">gettext</code> not only looks up a translation in a message catalog, it
also converts the translation on the fly to the desired output character
set.  This is useful if the user is working in a different character set
than the translator who created the message catalog, because it avoids
distributing variants of message catalogs which differ only in the
character set.
</p>
<p>The output character set is, by default, the value of <code class="code">nl_langinfo
(CODESET)</code>, which depends on the <code class="code">LC_CTYPE</code> part of the current
locale.  But programs which store strings in a locale independent way
(e.g. UTF-8) can request that <code class="code">gettext</code> and related functions
return the translations in that encoding, by use of the
<code class="code">bind_textdomain_codeset</code> function.
</p>
<p>Note that the <var class="var">msgid</var> argument to <code class="code">gettext</code> is not subject to
character set conversion.  Also, when <code class="code">gettext</code> does not find a
translation for <var class="var">msgid</var>, it returns <var class="var">msgid</var> unchanged &ndash;
independently of the current output character set.  It is therefore
recommended that all <var class="var">msgid</var>s be US-ASCII strings.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-bind_005ftextdomain_005fcodeset"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">bind_textdomain_codeset</strong> <code class="def-code-arguments">(const char *<var class="var">domainname</var>, const char *<var class="var">codeset</var>)</code><a class="copiable-link" href='#index-bind_005ftextdomain_005fcodeset'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">bind_textdomain_codeset</code> function can be used to specify the
output character set for message catalogs for domain <var class="var">domainname</var>.
The <var class="var">codeset</var> argument must be a valid codeset name which can be used
for the <code class="code">iconv_open</code> function, or a null pointer.
</p>
<p>If the <var class="var">codeset</var> parameter is the null pointer,
<code class="code">bind_textdomain_codeset</code> returns the currently selected codeset
for the domain with the name <var class="var">domainname</var>.  It returns <code class="code">NULL</code> if
no codeset has yet been selected.
</p>
<p>The <code class="code">bind_textdomain_codeset</code> function can be used several times.
If used multiple times with the same <var class="var">domainname</var> argument, the
later call overrides the settings made by the earlier one.
</p>
<p>The <code class="code">bind_textdomain_codeset</code> function returns a pointer to a
string containing the name of the selected codeset.  The string is
allocated internally in the function and must not be changed by the
user.  If the system went out of core during the execution of
<code class="code">bind_textdomain_codeset</code>, the return value is <code class="code">NULL</code> and the
global variable <code class="code">errno</code> is set accordingly.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="GUI-program-problems.html">How to use <code class="code">gettext</code> in GUI programs</a>, Previous: <a href="Advanced-gettext-functions.html">Additional functions for more complicated situations</a>, Up: <a href="Message-catalogs-with-gettext.html">The <code class="code">gettext</code> family of functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
