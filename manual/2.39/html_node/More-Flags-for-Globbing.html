<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>More Flags for Globbing (The GNU C Library)</title>

<meta name="description" content="More Flags for Globbing (The GNU C Library)">
<meta name="keywords" content="More Flags for Globbing (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Globbing.html" rel="up" title="Globbing">
<link href="Flags-for-Globbing.html" rel="prev" title="Flags for Globbing">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="More-Flags-for-Globbing">
<div class="nav-panel">
<p>
Previous: <a href="Flags-for-Globbing.html" accesskey="p" rel="prev">Flags for Globbing</a>, Up: <a href="Globbing.html" accesskey="u" rel="up">Globbing</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="More-Flags-for-Globbing-1">10.2.3 More Flags for Globbing</h4>

<p>Beside the flags described in the last section, the GNU implementation of
<code class="code">glob</code> allows a few more flags which are also defined in the
<samp class="file">glob.h</samp> file.  Some of the extensions implement functionality
which is available in modern shell implementations.
</p>
<dl class="vtable">
<dt id='index-GLOB_005fPERIOD'><span><code class="code">GLOB_PERIOD</code><a class="copiable-link" href='#index-GLOB_005fPERIOD'> &para;</a></span></dt>
<dd>
<p>The <code class="code">.</code> character (period) is treated special.  It cannot be
matched by wildcards.  See <a class="xref" href="Wildcard-Matching.html">Wildcard Matching</a>, <code class="code">FNM_PERIOD</code>.
</p>
</dd>
<dt id='index-GLOB_005fMAGCHAR'><span><code class="code">GLOB_MAGCHAR</code><a class="copiable-link" href='#index-GLOB_005fMAGCHAR'> &para;</a></span></dt>
<dd>
<p>The <code class="code">GLOB_MAGCHAR</code> value is not to be given to <code class="code">glob</code> in the
<var class="var">flags</var> parameter.  Instead, <code class="code">glob</code> sets this bit in the
<var class="var">gl_flags</var> element of the <var class="var">glob_t</var> structure provided as the
result if the pattern used for matching contains any wildcard character.
</p>
</dd>
<dt id='index-GLOB_005fALTDIRFUNC'><span><code class="code">GLOB_ALTDIRFUNC</code><a class="copiable-link" href='#index-GLOB_005fALTDIRFUNC'> &para;</a></span></dt>
<dd>
<p>Instead of using the normal functions for accessing the
filesystem the <code class="code">glob</code> implementation uses the user-supplied
functions specified in the structure pointed to by <var class="var">pglob</var>
parameter.  For more information about the functions refer to the
sections about directory handling see <a class="ref" href="Accessing-Directories.html">Accessing Directories</a>, and
<a class="ref" href="Reading-Attributes.html">Reading the Attributes of a File</a>.
</p>
</dd>
<dt id='index-GLOB_005fBRACE'><span><code class="code">GLOB_BRACE</code><a class="copiable-link" href='#index-GLOB_005fBRACE'> &para;</a></span></dt>
<dd>
<p>If this flag is given, the handling of braces in the pattern is changed.
It is now required that braces appear correctly grouped.  I.e., for each
opening brace there must be a closing one.  Braces can be used
recursively.  So it is possible to define one brace expression in
another one.  It is important to note that the range of each brace
expression is completely contained in the outer brace expression (if
there is one).
</p>
<p>The string between the matching braces is separated into single
expressions by splitting at <code class="code">,</code> (comma) characters.  The commas
themselves are discarded.  Please note what we said above about recursive
brace expressions.  The commas used to separate the subexpressions must
be at the same level.  Commas in brace subexpressions are not matched.
They are used during expansion of the brace expression of the deeper
level.  The example below shows this
</p>
<div class="example smallexample">
<pre class="example-preformatted">glob (&quot;{foo/{,bar,biz},baz}&quot;, GLOB_BRACE, NULL, &amp;result)
</pre></div>

<p>is equivalent to the sequence
</p>
<div class="example smallexample">
<pre class="example-preformatted">glob (&quot;foo/&quot;, GLOB_BRACE, NULL, &amp;result)
glob (&quot;foo/bar&quot;, GLOB_BRACE|GLOB_APPEND, NULL, &amp;result)
glob (&quot;foo/biz&quot;, GLOB_BRACE|GLOB_APPEND, NULL, &amp;result)
glob (&quot;baz&quot;, GLOB_BRACE|GLOB_APPEND, NULL, &amp;result)
</pre></div>

<p>if we leave aside error handling.
</p>
</dd>
<dt id='index-GLOB_005fNOMAGIC'><span><code class="code">GLOB_NOMAGIC</code><a class="copiable-link" href='#index-GLOB_005fNOMAGIC'> &para;</a></span></dt>
<dd>
<p>If the pattern contains no wildcard constructs (it is a literal file name),
return it as the sole &ldquo;matching&rdquo; word, even if no file exists by that name.
</p>
</dd>
<dt id='index-GLOB_005fTILDE'><span><code class="code">GLOB_TILDE</code><a class="copiable-link" href='#index-GLOB_005fTILDE'> &para;</a></span></dt>
<dd>
<p>If this flag is used the character <code class="code">~</code> (tilde) is handled specially
if it appears at the beginning of the pattern.  Instead of being taken
verbatim it is used to represent the home directory of a known user.
</p>
<p>If <code class="code">~</code> is the only character in pattern or it is followed by a
<code class="code">/</code> (slash), the home directory of the process owner is
substituted.  Using <code class="code">getlogin</code> and <code class="code">getpwnam</code> the information
is read from the system databases.  As an example take user <code class="code">bart</code>
with his home directory at <samp class="file">/home/bart</samp>.  For him a call like
</p>
<div class="example smallexample">
<pre class="example-preformatted">glob (&quot;~/bin/*&quot;, GLOB_TILDE, NULL, &amp;result)
</pre></div>

<p>would return the contents of the directory <samp class="file">/home/bart/bin</samp>.
Instead of referring to the own home directory it is also possible to
name the home directory of other users.  To do so one has to append the
user name after the tilde character.  So the contents of user
<code class="code">homer</code>&rsquo;s <samp class="file">bin</samp> directory can be retrieved by
</p>
<div class="example smallexample">
<pre class="example-preformatted">glob (&quot;~homer/bin/*&quot;, GLOB_TILDE, NULL, &amp;result)
</pre></div>

<p>If the user name is not valid or the home directory cannot be determined
for some reason the pattern is left untouched and itself used as the
result.  I.e., if in the last example <code class="code">home</code> is not available the
tilde expansion yields to <code class="code">&quot;~homer/bin/*&quot;</code> and <code class="code">glob</code> is not
looking for a directory named <code class="code">~homer</code>.
</p>
<p>This functionality is equivalent to what is available in C-shells if the
<code class="code">nonomatch</code> flag is set.
</p>
</dd>
<dt id='index-GLOB_005fTILDE_005fCHECK'><span><code class="code">GLOB_TILDE_CHECK</code><a class="copiable-link" href='#index-GLOB_005fTILDE_005fCHECK'> &para;</a></span></dt>
<dd>
<p>If this flag is used <code class="code">glob</code> behaves as if <code class="code">GLOB_TILDE</code> is
given.  The only difference is that if the user name is not available or
the home directory cannot be determined for other reasons this leads to
an error.  <code class="code">glob</code> will return <code class="code">GLOB_NOMATCH</code> instead of using
the pattern itself as the name.
</p>
<p>This functionality is equivalent to what is available in C-shells if
the <code class="code">nonomatch</code> flag is not set.
</p>
</dd>
<dt id='index-GLOB_005fONLYDIR'><span><code class="code">GLOB_ONLYDIR</code><a class="copiable-link" href='#index-GLOB_005fONLYDIR'> &para;</a></span></dt>
<dd>
<p>If this flag is used the globbing function takes this as a
<strong class="strong">hint</strong> that the caller is only interested in directories
matching the pattern.  If the information about the type of the file
is easily available non-directories will be rejected but no extra
work will be done to determine the information for each file.  I.e.,
the caller must still be able to filter directories out.
</p>
<p>This functionality is only available with the GNU <code class="code">glob</code>
implementation.  It is mainly used internally to increase the
performance but might be useful for a user as well and therefore is
documented here.
</p></dd>
</dl>

<p>Calling <code class="code">glob</code> will in most cases allocate resources which are used
to represent the result of the function call.  If the same object of
type <code class="code">glob_t</code> is used in multiple call to <code class="code">glob</code> the resources
are freed or reused so that no leaks appear.  But this does not include
the time when all <code class="code">glob</code> calls are done.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-globfree"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">globfree</strong> <code class="def-code-arguments">(glob_t *<var class="var">pglob</var>)</code><a class="copiable-link" href='#index-globfree'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">globfree</code> function frees all resources allocated by previous
calls to <code class="code">glob</code> associated with the object pointed to by
<var class="var">pglob</var>.  This function should be called whenever the currently used
<code class="code">glob_t</code> typed object isn&rsquo;t used anymore.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-globfree64"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">globfree64</strong> <code class="def-code-arguments">(glob64_t *<var class="var">pglob</var>)</code><a class="copiable-link" href='#index-globfree64'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe corrupt lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is equivalent to <code class="code">globfree</code> but it frees records of
type <code class="code">glob64_t</code> which were allocated by <code class="code">glob64</code>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Flags-for-Globbing.html">Flags for Globbing</a>, Up: <a href="Globbing.html">Globbing</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
