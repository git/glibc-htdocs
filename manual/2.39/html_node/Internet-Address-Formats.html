<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Internet Address Formats (The GNU C Library)</title>

<meta name="description" content="Internet Address Formats (The GNU C Library)">
<meta name="keywords" content="Internet Address Formats (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Internet-Namespace.html" rel="up" title="Internet Namespace">
<link href="Host-Addresses.html" rel="next" title="Host Addresses">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Internet-Address-Formats">
<div class="nav-panel">
<p>
Next: <a href="Host-Addresses.html" accesskey="n" rel="next">Host Addresses</a>, Up: <a href="Internet-Namespace.html" accesskey="u" rel="up">The Internet Namespace</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Internet-Socket-Address-Formats">16.6.1 Internet Socket Address Formats</h4>

<p>In the Internet namespace, for both IPv4 (<code class="code">AF_INET</code>) and IPv6
(<code class="code">AF_INET6</code>), a socket address consists of a host address
and a port on that host.  In addition, the protocol you choose serves
effectively as a part of the address because local port numbers are
meaningful only within a particular protocol.
</p>
<p>The data types for representing socket addresses in the Internet namespace
are defined in the header file <samp class="file">netinet/in.h</samp>.
<a class="index-entry-id" id="index-netinet_002fin_002eh"></a>
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-sockaddr_005fin"><span class="category-def">Data Type: </span><span><strong class="def-name">struct sockaddr_in</strong><a class="copiable-link" href='#index-struct-sockaddr_005fin'> &para;</a></span></dt>
<dd>
<p>This is the data type used to represent socket addresses in the
Internet namespace.  It has the following members:
</p>
<dl class="table">
<dt><code class="code">sa_family_t sin_family</code></dt>
<dd><p>This identifies the address family or format of the socket address.
You should store the value <code class="code">AF_INET</code> in this member.  The address
family is stored in host byte order.  See <a class="xref" href="Socket-Addresses.html">Socket Addresses</a>.
</p>
</dd>
<dt><code class="code">struct in_addr sin_addr</code></dt>
<dd><p>This is the IPv4 address.  See <a class="xref" href="Host-Addresses.html">Host Addresses</a>, and <a class="ref" href="Host-Names.html">Host Names</a>, for how to get a value to store here.  The IPv4 address is
stored in network byte order.
</p>
</dd>
<dt><code class="code">unsigned short int sin_port</code></dt>
<dd><p>This is the port number.  See <a class="xref" href="Ports.html">Internet Ports</a>.  The port number is stored in
network byte order.
</p></dd>
</dl>
</dd></dl>

<p>When you call <code class="code">bind</code> or <code class="code">getsockname</code>, you should specify
<code class="code">sizeof (struct sockaddr_in)</code> as the <var class="var">length</var> parameter if
you are using an IPv4 Internet namespace socket address.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-sockaddr_005fin6"><span class="category-def">Data Type: </span><span><strong class="def-name">struct sockaddr_in6</strong><a class="copiable-link" href='#index-struct-sockaddr_005fin6'> &para;</a></span></dt>
<dd><p>This is the data type used to represent socket addresses in the IPv6
namespace.  It has the following members:
</p>
<dl class="table">
<dt><code class="code">sa_family_t sin6_family</code></dt>
<dd><p>This identifies the address family or format of the socket address.
You should store the value of <code class="code">AF_INET6</code> in this member.
See <a class="xref" href="Socket-Addresses.html">Socket Addresses</a>.  The address family is stored in host byte
order.
</p>
</dd>
<dt><code class="code">struct in6_addr sin6_addr</code></dt>
<dd><p>This is the IPv6 address of the host machine.  See <a class="xref" href="Host-Addresses.html">Host Addresses</a>, and <a class="ref" href="Host-Names.html">Host Names</a>, for how to get a value to store
here.  The address is stored in network byte order.
</p>
</dd>
<dt id='index-flow-label'><span><code class="code">uint32_t sin6_flowinfo</code><a class="copiable-link" href='#index-flow-label'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-IPv6-flow-label"></a>
<a class="index-entry-id" id="index-traffic-class"></a>
<a class="index-entry-id" id="index-IPv6-traffic-class"></a>
<p>This combines the IPv6 traffic class and flow label values, as found
in the IPv6 header.  This field is stored in network byte order.  Only
the 28 lower bits (of the number in network byte order) are used; the
remaining bits must be zero.  The lower 20 bits are the flow label, and
bits 20 to 27 are the the traffic class.  Typically, this field is
zero.
</p>
</dd>
<dt id='index-scope-ID'><span><code class="code">uint32_t sin6_scope_id</code><a class="copiable-link" href='#index-scope-ID'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-IPv6-scope-ID"></a>
<p>For link-local addresses, this identifies the interface on which this
address is valid.  The scope ID is stored in host byte order.
Typically, this field is zero.
</p>
</dd>
<dt><code class="code">uint16_t sin6_port</code></dt>
<dd><p>This is the port number.  See <a class="xref" href="Ports.html">Internet Ports</a>.  The port number is stored in
network byte order.
</p>
</dd>
</dl>
</dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Host-Addresses.html">Host Addresses</a>, Up: <a href="Internet-Namespace.html">The Internet Namespace</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
