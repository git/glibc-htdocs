<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>iconv Examples (The GNU C Library)</title>

<meta name="description" content="iconv Examples (The GNU C Library)">
<meta name="keywords" content="iconv Examples (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Generic-Charset-Conversion.html" rel="up" title="Generic Charset Conversion">
<link href="Other-iconv-Implementations.html" rel="next" title="Other iconv Implementations">
<link href="Generic-Conversion-Interface.html" rel="prev" title="Generic Conversion Interface">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="iconv-Examples">
<div class="nav-panel">
<p>
Next: <a href="Other-iconv-Implementations.html" accesskey="n" rel="next">Some Details about other <code class="code">iconv</code> Implementations</a>, Previous: <a href="Generic-Conversion-Interface.html" accesskey="p" rel="prev">Generic Character Set Conversion Interface</a>, Up: <a href="Generic-Charset-Conversion.html" accesskey="u" rel="up">Generic Charset Conversion</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="A-complete-iconv-example">6.5.2 A complete <code class="code">iconv</code> example</h4>

<p>The example below features a solution for a common problem.  Given that
one knows the internal encoding used by the system for <code class="code">wchar_t</code>
strings, one often is in the position to read text from a file and store
it in wide character buffers.  One can do this using <code class="code">mbsrtowcs</code>,
but then we run into the problems discussed above.
</p>
<div class="example smallexample">
<pre class="example-preformatted">int
file2wcs (int fd, const char *charset, wchar_t *outbuf, size_t avail)
{
  char inbuf[BUFSIZ];
  size_t insize = 0;
  char *wrptr = (char *) outbuf;
  int result = 0;
  iconv_t cd;

  cd = iconv_open (&quot;WCHAR_T&quot;, charset);
  if (cd == (iconv_t) -1)
    {
      /* <span class="r">Something went wrong.</span>  */
      if (errno == EINVAL)
        error (0, 0, &quot;conversion from '%s' to wchar_t not available&quot;,
               charset);
      else
        perror (&quot;iconv_open&quot;);

      /* <span class="r">Terminate the output string.</span>  */
      *outbuf = L'\0';

      return -1;
    }

  while (avail &gt; 0)
    {
      size_t nread;
      size_t nconv;
      char *inptr = inbuf;

      /* <span class="r">Read more input.</span>  */
      nread = read (fd, inbuf + insize, sizeof (inbuf) - insize);
      if (nread == 0)
        {
          /* <span class="r">When we come here the file is completely read.</span>
             <span class="r">This still could mean there are some unused</span>
             <span class="r">characters in the <code class="code">inbuf</code>.  Put them back.</span>  */
          if (lseek (fd, -insize, SEEK_CUR) == -1)
            result = -1;

          /* <span class="r">Now write out the byte sequence to get into the</span>
             <span class="r">initial state if this is necessary.</span>  */
          iconv (cd, NULL, NULL, &amp;wrptr, &amp;avail);

          break;
        }
      insize += nread;

      /* <span class="r">Do the conversion.</span>  */
      nconv = iconv (cd, &amp;inptr, &amp;insize, &amp;wrptr, &amp;avail);
      if (nconv == (size_t) -1)
        {
          /* <span class="r">Not everything went right.  It might only be</span>
             <span class="r">an unfinished byte sequence at the end of the</span>
             <span class="r">buffer.  Or it is a real problem.</span>  */
          if (errno == EINVAL)
            /* <span class="r">This is harmless.  Simply move the unused</span>
               <span class="r">bytes to the beginning of the buffer so that</span>
               <span class="r">they can be used in the next round.</span>  */
            memmove (inbuf, inptr, insize);
          else
            {
              /* <span class="r">It is a real problem.  Maybe we ran out of</span>
                 <span class="r">space in the output buffer or we have invalid</span>
                 <span class="r">input.  In any case back the file pointer to</span>
                 <span class="r">the position of the last processed byte.</span>  */
              lseek (fd, -insize, SEEK_CUR);
              result = -1;
              break;
            }
        }
    }

  /* <span class="r">Terminate the output string.</span>  */
  if (avail &gt;= sizeof (wchar_t))
    *((wchar_t *) wrptr) = L'\0';

  if (iconv_close (cd) != 0)
    perror (&quot;iconv_close&quot;);

  return (wchar_t *) wrptr - outbuf;
}
</pre></div>

<a class="index-entry-id" id="index-stateful-4"></a>
<p>This example shows the most important aspects of using the <code class="code">iconv</code>
functions.  It shows how successive calls to <code class="code">iconv</code> can be used to
convert large amounts of text.  The user does not have to care about
stateful encodings as the functions take care of everything.
</p>
<p>An interesting point is the case where <code class="code">iconv</code> returns an error and
<code class="code">errno</code> is set to <code class="code">EINVAL</code>.  This is not really an error in the
transformation.  It can happen whenever the input character set contains
byte sequences of more than one byte for some character and texts are not
processed in one piece.  In this case there is a chance that a multibyte
sequence is cut.  The caller can then simply read the remainder of the
takes and feed the offending bytes together with new character from the
input to <code class="code">iconv</code> and continue the work.  The internal state kept in
the descriptor is <em class="emph">not</em> unspecified after such an event as is the
case with the conversion functions from the ISO&nbsp;C<!-- /@w --> standard.
</p>
<p>The example also shows the problem of using wide character strings with
<code class="code">iconv</code>.  As explained in the description of the <code class="code">iconv</code>
function above, the function always takes a pointer to a <code class="code">char</code>
array and the available space is measured in bytes.  In the example, the
output buffer is a wide character buffer; therefore, we use a local
variable <var class="var">wrptr</var> of type <code class="code">char *</code>, which is used in the
<code class="code">iconv</code> calls.
</p>
<p>This looks rather innocent but can lead to problems on platforms that
have tight restriction on alignment.  Therefore the caller of <code class="code">iconv</code>
has to make sure that the pointers passed are suitable for access of
characters from the appropriate character set.  Since, in the
above case, the input parameter to the function is a <code class="code">wchar_t</code>
pointer, this is the case (unless the user violates alignment when
computing the parameter).  But in other situations, especially when
writing generic functions where one does not know what type of character
set one uses and, therefore, treats text as a sequence of bytes, it might
become tricky.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Other-iconv-Implementations.html">Some Details about other <code class="code">iconv</code> Implementations</a>, Previous: <a href="Generic-Conversion-Interface.html">Generic Character Set Conversion Interface</a>, Up: <a href="Generic-Charset-Conversion.html">Generic Charset Conversion</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
