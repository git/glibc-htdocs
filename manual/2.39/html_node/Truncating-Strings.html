<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Truncating Strings (The GNU C Library)</title>

<meta name="description" content="Truncating Strings (The GNU C Library)">
<meta name="keywords" content="Truncating Strings (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="String-and-Array-Utilities.html" rel="up" title="String and Array Utilities">
<link href="String_002fArray-Comparison.html" rel="next" title="String/Array Comparison">
<link href="Concatenating-Strings.html" rel="prev" title="Concatenating Strings">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Truncating-Strings">
<div class="nav-panel">
<p>
Next: <a href="String_002fArray-Comparison.html" accesskey="n" rel="next">String/Array Comparison</a>, Previous: <a href="Concatenating-Strings.html" accesskey="p" rel="prev">Concatenating Strings</a>, Up: <a href="String-and-Array-Utilities.html" accesskey="u" rel="up">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Truncating-Strings-while-Copying">5.6 Truncating Strings while Copying</h3>
<a class="index-entry-id" id="index-truncating-strings"></a>
<a class="index-entry-id" id="index-string-truncation"></a>

<p>The functions described in this section copy or concatenate the
possibly-truncated contents of a string or array to another, and
similarly for wide strings.  They follow the string-copying functions
in their header conventions.  See <a class="xref" href="Copying-Strings-and-Arrays.html">Copying Strings and Arrays</a>.  The
&lsquo;<samp class="samp">str</samp>&rsquo; functions are declared in the header file <samp class="file">string.h</samp>
and the &lsquo;<samp class="samp">wc</samp>&rsquo; functions are declared in the file <samp class="file">wchar.h</samp>.
</p>
<p>As noted below, these functions are problematic as their callers may
have truncation-related bugs and performance issues.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strncpy"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">strncpy</strong> <code class="def-code-arguments">(char *restrict <var class="var">to</var>, const char *restrict <var class="var">from</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-strncpy'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">strcpy</code> but always copies exactly
<var class="var">size</var> bytes into <var class="var">to</var>.
</p>
<p>If <var class="var">from</var> does not contain a null byte in its first <var class="var">size</var>
bytes, <code class="code">strncpy</code> copies just the first <var class="var">size</var> bytes.  In this
case no null terminator is written into <var class="var">to</var>.
</p>
<p>Otherwise <var class="var">from</var> must be a string with length less than
<var class="var">size</var>.  In this case <code class="code">strncpy</code> copies all of <var class="var">from</var>,
followed by enough null bytes to add up to <var class="var">size</var> bytes in all.
</p>
<p>The behavior of <code class="code">strncpy</code> is undefined if the strings overlap.
</p>
<p>This function was designed for now-rarely-used arrays consisting of
non-null bytes followed by zero or more null bytes.  It needs to set
all <var class="var">size</var> bytes of the destination, even when <var class="var">size</var> is much
greater than the length of <var class="var">from</var>.  As noted below, this function
is generally a poor choice for processing strings.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcsncpy"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wcsncpy</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">wto</var>, const wchar_t *restrict <var class="var">wfrom</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-wcsncpy'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">wcscpy</code> but always copies exactly
<var class="var">size</var> wide characters into <var class="var">wto</var>.
</p>
<p>If <var class="var">wfrom</var> does not contain a null wide character in its first
<var class="var">size</var> wide characters, then <code class="code">wcsncpy</code> copies just the first
<var class="var">size</var> wide characters.  In this case no null terminator is
written into <var class="var">wto</var>.
</p>
<p>Otherwise <var class="var">wfrom</var> must be a wide string with length less than
<var class="var">size</var>.  In this case <code class="code">wcsncpy</code> copies all of <var class="var">wfrom</var>,
followed by enough null wide characters to add up to <var class="var">size</var> wide
characters in all.
</p>
<p>The behavior of <code class="code">wcsncpy</code> is undefined if the strings overlap.
</p>
<p>This function is the wide-character counterpart of <code class="code">strncpy</code> and
suffers from most of the problems that <code class="code">strncpy</code> does.  For
example, as noted below, this function is generally a poor choice for
processing strings.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strndup"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">strndup</strong> <code class="def-code-arguments">(const char *<var class="var">s</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-strndup'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">strdup</code> but always copies at most
<var class="var">size</var> bytes into the newly allocated string.
</p>
<p>If the length of <var class="var">s</var> is more than <var class="var">size</var>, then <code class="code">strndup</code>
copies just the first <var class="var">size</var> bytes and adds a closing null byte.
Otherwise all bytes are copied and the string is terminated.
</p>
<p>This function differs from <code class="code">strncpy</code> in that it always terminates
the destination string.
</p>
<p>As noted below, this function is generally a poor choice for
processing strings.
</p>
<p><code class="code">strndup</code> is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-strndupa"><span class="category-def">Macro: </span><span><code class="def-type">char *</code> <strong class="def-name">strndupa</strong> <code class="def-code-arguments">(const char *<var class="var">s</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-strndupa'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">strndup</code> but like <code class="code">strdupa</code> it
allocates the new string using <code class="code">alloca</code> see <a class="pxref" href="Variable-Size-Automatic.html">Automatic Storage with Variable Size</a>.  The same advantages and limitations of <code class="code">strdupa</code> are
valid for <code class="code">strndupa</code>, too.
</p>
<p>This function is implemented only as a macro, just like <code class="code">strdupa</code>.
Just as <code class="code">strdupa</code> this macro also must not be used inside the
parameter list in a function call.
</p>
<p>As noted below, this function is generally a poor choice for
processing strings.
</p>
<p><code class="code">strndupa</code> is only available if GNU CC is used.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-stpncpy"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">stpncpy</strong> <code class="def-code-arguments">(char *restrict <var class="var">to</var>, const char *restrict <var class="var">from</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-stpncpy'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">stpcpy</code> but copies always exactly
<var class="var">size</var> bytes into <var class="var">to</var>.
</p>
<p>If the length of <var class="var">from</var> is more than <var class="var">size</var>, then <code class="code">stpncpy</code>
copies just the first <var class="var">size</var> bytes and returns a pointer to the
byte directly following the one which was copied last.  Note that in
this case there is no null terminator written into <var class="var">to</var>.
</p>
<p>If the length of <var class="var">from</var> is less than <var class="var">size</var>, then <code class="code">stpncpy</code>
copies all of <var class="var">from</var>, followed by enough null bytes to add up
to <var class="var">size</var> bytes in all.  This behavior is rarely useful, but it
is implemented to be useful in contexts where this behavior of the
<code class="code">strncpy</code> is used.  <code class="code">stpncpy</code> returns a pointer to the
<em class="emph">first</em> written null byte.
</p>
<p>This function is not part of ISO or POSIX but was found useful while
developing the GNU C Library itself.
</p>
<p>Its behavior is undefined if the strings overlap.  The function is
declared in <samp class="file">string.h</samp>.
</p>
<p>As noted below, this function is generally a poor choice for
processing strings.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcpncpy"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wcpncpy</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">wto</var>, const wchar_t *restrict <var class="var">wfrom</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-wcpncpy'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">wcpcpy</code> but copies always exactly
<var class="var">wsize</var> wide characters into <var class="var">wto</var>.
</p>
<p>If the length of <var class="var">wfrom</var> is more than <var class="var">size</var>, then
<code class="code">wcpncpy</code> copies just the first <var class="var">size</var> wide characters and
returns a pointer to the wide character directly following the last
non-null wide character which was copied last.  Note that in this case
there is no null terminator written into <var class="var">wto</var>.
</p>
<p>If the length of <var class="var">wfrom</var> is less than <var class="var">size</var>, then <code class="code">wcpncpy</code>
copies all of <var class="var">wfrom</var>, followed by enough null wide characters to add up
to <var class="var">size</var> wide characters in all.  This behavior is rarely useful, but it
is implemented to be useful in contexts where this behavior of the
<code class="code">wcsncpy</code> is used.  <code class="code">wcpncpy</code> returns a pointer to the
<em class="emph">first</em> written null wide character.
</p>
<p>This function is not part of ISO or POSIX but was found useful while
developing the GNU C Library itself.
</p>
<p>Its behavior is undefined if the strings overlap.
</p>
<p>As noted below, this function is generally a poor choice for
processing strings.
</p>
<p><code class="code">wcpncpy</code> is a GNU extension.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strncat"><span class="category-def">Function: </span><span><code class="def-type">char *</code> <strong class="def-name">strncat</strong> <code class="def-code-arguments">(char *restrict <var class="var">to</var>, const char *restrict <var class="var">from</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-strncat'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is like <code class="code">strcat</code> except that not more than <var class="var">size</var>
bytes from <var class="var">from</var> are appended to the end of <var class="var">to</var>, and
<var class="var">from</var> need not be null-terminated.  A single null byte is also
always appended to <var class="var">to</var>, so the total
allocated size of <var class="var">to</var> must be at least <code class="code"><var class="var">size</var> + 1</code> bytes
longer than its initial length.
</p>
<p>The <code class="code">strncat</code> function could be implemented like this:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">char *
strncat (char *to, const char *from, size_t size)
{
  size_t len = strlen (to);
  memcpy (to + len, from, strnlen (from, size));
  to[len + strnlen (from, size)] = '\0';
  return to;
}
</pre></div></div>

<p>The behavior of <code class="code">strncat</code> is undefined if the strings overlap.
</p>
<p>As a companion to <code class="code">strncpy</code>, <code class="code">strncat</code> was designed for
now-rarely-used arrays consisting of non-null bytes followed by zero
or more null bytes.  However, As noted below, this function is generally a poor
choice for processing strings.  Also, this function has significant
performance issues.  See <a class="xref" href="Concatenating-Strings.html">Concatenating Strings</a>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcsncat"><span class="category-def">Function: </span><span><code class="def-type">wchar_t *</code> <strong class="def-name">wcsncat</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">wto</var>, const wchar_t *restrict <var class="var">wfrom</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-wcsncat'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is like <code class="code">wcscat</code> except that not more than <var class="var">size</var>
wide characters from <var class="var">from</var> are appended to the end of <var class="var">to</var>,
and <var class="var">from</var> need not be null-terminated.  A single null wide
character is also always appended to <var class="var">to</var>, so the total allocated
size of <var class="var">to</var> must be at least <code class="code">wcsnlen (<var class="var">wfrom</var>,
<var class="var">size</var>) + 1</code> wide characters longer than its initial length.
</p>
<p>The <code class="code">wcsncat</code> function could be implemented like this:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">wchar_t *
wcsncat (wchar_t *restrict wto, const wchar_t *restrict wfrom,
         size_t size)
{
  size_t len = wcslen (wto);
  memcpy (wto + len, wfrom, wcsnlen (wfrom, size) * sizeof (wchar_t));
  wto[len + wcsnlen (wfrom, size)] = L'\0';
  return wto;
}
</pre></div></div>

<p>The behavior of <code class="code">wcsncat</code> is undefined if the strings overlap.
</p>
<p>As noted below, this function is generally a poor choice for
processing strings.  Also, this function has significant performance
issues.  See <a class="xref" href="Concatenating-Strings.html">Concatenating Strings</a>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strlcpy"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">strlcpy</strong> <code class="def-code-arguments">(char *restrict <var class="var">to</var>, const char *restrict <var class="var">from</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-strlcpy'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function copies the string <var class="var">from</var> to the destination array
<var class="var">to</var>, limiting the result&rsquo;s size (including the null terminator)
to <var class="var">size</var>.  The caller should ensure that <var class="var">size</var> includes room
for the result&rsquo;s terminating null byte.
</p>
<p>If <var class="var">size</var> is greater than the length of the string <var class="var">from</var>,
this function copies the non-null bytes of the string
<var class="var">from</var> to the destination array <var class="var">to</var>,
and terminates the copy with a null byte.  Like other
string functions such as <code class="code">strcpy</code>, but unlike <code class="code">strncpy</code>, any
remaining bytes in the destination array remain unchanged.
</p>
<p>If <var class="var">size</var> is nonzero and less than or equal to the the length of the string
<var class="var">from</var>, this function copies only the first &lsquo;<samp class="samp"><var class="var">size</var> - 1</samp>&rsquo;
bytes to the destination array <var class="var">to</var>, and writes a terminating null
byte to the last byte of the array.
</p>
<p>This function returns the length of the string <var class="var">from</var>.  This means
that truncation occurs if and only if the returned value is greater
than or equal to <var class="var">size</var>.
</p>
<p>The behavior is undefined if <var class="var">to</var> or <var class="var">from</var> is a null pointer,
or if the destination array&rsquo;s size is less than <var class="var">size</var>, or if the
string <var class="var">from</var> overlaps the first <var class="var">size</var> bytes of the
destination array.
</p>
<p>As noted below, this function is generally a poor choice for
processing strings.  Also, this function has a performance issue,
as its time cost is proportional to the length of <var class="var">from</var>
even when <var class="var">size</var> is small.
</p>
<p>This function is derived from OpenBSD 2.4.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcslcpy"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">wcslcpy</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">to</var>, const wchar_t *restrict <var class="var">from</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-wcslcpy'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is a variant of <code class="code">strlcpy</code> for wide strings.
The  <var class="var">size</var> argument counts the length of the destination buffer in
wide characters (and not bytes).
</p>
<p>This function is derived from BSD.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-strlcat"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">strlcat</strong> <code class="def-code-arguments">(char *restrict <var class="var">to</var>, const char *restrict <var class="var">from</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-strlcat'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function appends the string <var class="var">from</var> to the
string <var class="var">to</var>, limiting the result&rsquo;s total size (including the null
terminator) to <var class="var">size</var>.  The caller should ensure that <var class="var">size</var>
includes room for the result&rsquo;s terminating null byte.
</p>
<p>This function copies as much as possible of the string <var class="var">from</var> into
the array at <var class="var">to</var> of <var class="var">size</var> bytes, starting at the terminating
null byte of the original string <var class="var">to</var>.  In effect, this appends
the string <var class="var">from</var> to the string <var class="var">to</var>.  Although the resulting
string will contain a null terminator, it can be truncated (not all
bytes in <var class="var">from</var> may be copied).
</p>
<p>This function returns the sum of the original length of <var class="var">to</var> and
the length of <var class="var">from</var>.  This means that truncation occurs if and
only if the returned value is greater than or equal to <var class="var">size</var>.
</p>
<p>The behavior is undefined if <var class="var">to</var> or <var class="var">from</var> is a null pointer,
or if the destination array&rsquo;s size is less than <var class="var">size</var>, or if the
destination array does not contain a null byte in its first <var class="var">size</var>
bytes, or if the string <var class="var">from</var> overlaps the first <var class="var">size</var> bytes
of the destination array.
</p>
<p>As noted below, this function is generally a poor choice for
processing strings.  Also, this function has significant performance
issues.  See <a class="xref" href="Concatenating-Strings.html">Concatenating Strings</a>.
</p>
<p>This function is derived from OpenBSD 2.4.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-wcslcat"><span class="category-def">Function: </span><span><code class="def-type">size_t</code> <strong class="def-name">wcslcat</strong> <code class="def-code-arguments">(wchar_t *restrict <var class="var">to</var>, const wchar_t *restrict <var class="var">from</var>, size_t <var class="var">size</var>)</code><a class="copiable-link" href='#index-wcslcat'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is a variant of <code class="code">strlcat</code> for wide strings.
The  <var class="var">size</var> argument counts the length of the destination buffer in
wide characters (and not bytes).
</p>
<p>This function is derived from BSD.
</p></dd></dl>

<p>Because these functions can abruptly truncate strings or wide strings,
they are generally poor choices for processing them.  When copying or
concatening multibyte strings, they can truncate within a multibyte
character so that the result is not a valid multibyte string.  When
combining or concatenating multibyte or wide strings, they may
truncate the output after a combining character, resulting in a
corrupted grapheme.  They can cause bugs even when processing
single-byte strings: for example, when calculating an ASCII-only user
name, a truncated name can identify the wrong user.
</p>
<p>Although some buffer overruns can be prevented by manually replacing
calls to copying functions with calls to truncation functions, there
are often easier and safer automatic techniques, such as fortification
(see <a class="pxref" href="Source-Fortification.html">Fortification of function calls</a>) and AddressSanitizer
(see <a data-manual="gcc" href="https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html#Instrumentation-Options">Program Instrumentation Options</a> in <cite class="cite">Using GCC</cite>).
Because truncation functions can mask
application bugs that would otherwise be caught by the automatic
techniques, these functions should be used only when the application&rsquo;s
underlying logic requires truncation.
</p>
<p><strong class="strong">Note:</strong> GNU programs should not truncate strings or wide
strings to fit arbitrary size limits.  See <a data-manual="standards" href="https://www.gnu.org/prep/standards/html_node/Semantics.html#Semantics">Writing
Robust Programs</a> in <cite class="cite">The GNU Coding Standards</cite>.  Instead of
string-truncation functions, it is usually better to use dynamic
memory allocation (see <a class="pxref" href="Unconstrained-Allocation.html">Unconstrained Allocation</a>) and functions
such as <code class="code">strdup</code> or <code class="code">asprintf</code> to construct strings.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="String_002fArray-Comparison.html">String/Array Comparison</a>, Previous: <a href="Concatenating-Strings.html">Concatenating Strings</a>, Up: <a href="String-and-Array-Utilities.html">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
