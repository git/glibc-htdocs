<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Host Names (The GNU C Library)</title>

<meta name="description" content="Host Names (The GNU C Library)">
<meta name="keywords" content="Host Names (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Host-Addresses.html" rel="up" title="Host Addresses">
<link href="Host-Address-Functions.html" rel="prev" title="Host Address Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Host-Names">
<div class="nav-panel">
<p>
Previous: <a href="Host-Address-Functions.html" accesskey="p" rel="prev">Host Address Functions</a>, Up: <a href="Host-Addresses.html" accesskey="u" rel="up">Host Addresses</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Host-Names-1">16.6.2.4 Host Names</h4>
<a class="index-entry-id" id="index-hosts-database"></a>
<a class="index-entry-id" id="index-converting-host-name-to-address"></a>
<a class="index-entry-id" id="index-converting-host-address-to-name"></a>

<p>Besides the standard numbers-and-dots notation for Internet addresses,
you can also refer to a host by a symbolic name.  The advantage of a
symbolic name is that it is usually easier to remember.  For example,
the machine with Internet address &lsquo;<samp class="samp">158.121.106.19</samp>&rsquo; is also known as
&lsquo;<samp class="samp">alpha.gnu.org</samp>&rsquo;; and other machines in the &lsquo;<samp class="samp">gnu.org</samp>&rsquo;
domain can refer to it simply as &lsquo;<samp class="samp">alpha</samp>&rsquo;.
</p>
<a class="index-entry-id" id="index-_002fetc_002fhosts"></a>
<a class="index-entry-id" id="index-netdb_002eh"></a>
<p>Internally, the system uses a database to keep track of the mapping
between host names and host numbers.  This database is usually either
the file <samp class="file">/etc/hosts</samp> or an equivalent provided by a name server.
The functions and other symbols for accessing this database are declared
in <samp class="file">netdb.h</samp>.  They are BSD features, defined unconditionally if
you include <samp class="file">netdb.h</samp>.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-struct-hostent"><span class="category-def">Data Type: </span><span><strong class="def-name">struct hostent</strong><a class="copiable-link" href='#index-struct-hostent'> &para;</a></span></dt>
<dd>
<p>This data type is used to represent an entry in the hosts database.  It
has the following members:
</p>
<dl class="table">
<dt><code class="code">char *h_name</code></dt>
<dd><p>This is the &ldquo;official&rdquo; name of the host.
</p>
</dd>
<dt><code class="code">char **h_aliases</code></dt>
<dd><p>These are alternative names for the host, represented as a null-terminated
vector of strings.
</p>
</dd>
<dt><code class="code">int h_addrtype</code></dt>
<dd><p>This is the host address type; in practice, its value is always either
<code class="code">AF_INET</code> or <code class="code">AF_INET6</code>, with the latter being used for IPv6
hosts.  In principle other kinds of addresses could be represented in
the database as well as Internet addresses; if this were done, you
might find a value in this field other than <code class="code">AF_INET</code> or
<code class="code">AF_INET6</code>.  See <a class="xref" href="Socket-Addresses.html">Socket Addresses</a>.
</p>
</dd>
<dt><code class="code">int h_length</code></dt>
<dd><p>This is the length, in bytes, of each address.
</p>
</dd>
<dt><code class="code">char **h_addr_list</code></dt>
<dd><p>This is the vector of addresses for the host.  (Recall that the host
might be connected to multiple networks and have different addresses on
each one.)  The vector is terminated by a null pointer.
</p>
</dd>
<dt><code class="code">char *h_addr</code></dt>
<dd><p>This is a synonym for <code class="code">h_addr_list[0]</code>; in other words, it is the
first host address.
</p></dd>
</dl>
</dd></dl>

<p>As far as the host database is concerned, each address is just a block
of memory <code class="code">h_length</code> bytes long.  But in other contexts there is an
implicit assumption that you can convert IPv4 addresses to a
<code class="code">struct in_addr</code> or an <code class="code">uint32_t</code>.  Host addresses in
a <code class="code">struct hostent</code> structure are always given in network byte
order; see <a class="ref" href="Byte-Order.html">Byte Order Conversion</a>.
</p>
<p>You can use <code class="code">gethostbyname</code>, <code class="code">gethostbyname2</code> or
<code class="code">gethostbyaddr</code> to search the hosts database for information about
a particular host.  The information is returned in a
statically-allocated structure; you must copy the information if you
need to save it across calls.  You can also use <code class="code">getaddrinfo</code> and
<code class="code">getnameinfo</code> to obtain this information.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gethostbyname"><span class="category-def">Function: </span><span><code class="def-type">struct hostent *</code> <strong class="def-name">gethostbyname</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>)</code><a class="copiable-link" href='#index-gethostbyname'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:hostbyname env locale
| AS-Unsafe dlopen plugin corrupt heap lock
| AC-Unsafe lock corrupt mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">gethostbyname</code> function returns information about the host
named <var class="var">name</var>.  If the lookup fails, it returns a null pointer.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gethostbyname2"><span class="category-def">Function: </span><span><code class="def-type">struct hostent *</code> <strong class="def-name">gethostbyname2</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>, int <var class="var">af</var>)</code><a class="copiable-link" href='#index-gethostbyname2'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:hostbyname2 env locale
| AS-Unsafe dlopen plugin corrupt heap lock
| AC-Unsafe lock corrupt mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">gethostbyname2</code> function is like <code class="code">gethostbyname</code>, but
allows the caller to specify the desired address family (e.g.
<code class="code">AF_INET</code> or <code class="code">AF_INET6</code>) of the result.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gethostbyaddr"><span class="category-def">Function: </span><span><code class="def-type">struct hostent *</code> <strong class="def-name">gethostbyaddr</strong> <code class="def-code-arguments">(const void *<var class="var">addr</var>, socklen_t <var class="var">length</var>, int <var class="var">format</var>)</code><a class="copiable-link" href='#index-gethostbyaddr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:hostbyaddr env locale
| AS-Unsafe dlopen plugin corrupt heap lock
| AC-Unsafe lock corrupt mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">gethostbyaddr</code> function returns information about the host
with Internet address <var class="var">addr</var>.  The parameter <var class="var">addr</var> is not
really a pointer to char - it can be a pointer to an IPv4 or an IPv6
address.  The <var class="var">length</var> argument is the size (in bytes) of the address
at <var class="var">addr</var>.  <var class="var">format</var> specifies the address format; for an IPv4
Internet address, specify a value of <code class="code">AF_INET</code>; for an IPv6
Internet address, use <code class="code">AF_INET6</code>.
</p>
<p>If the lookup fails, <code class="code">gethostbyaddr</code> returns a null pointer.
</p></dd></dl>

<a class="index-entry-id" id="index-h_005ferrno"></a>
<p>If the name lookup by <code class="code">gethostbyname</code> or <code class="code">gethostbyaddr</code>
fails, you can find out the reason by looking at the value of the
variable <code class="code">h_errno</code>.  (It would be cleaner design for these
functions to set <code class="code">errno</code>, but use of <code class="code">h_errno</code> is compatible
with other systems.)
</p>
<p>Here are the error codes that you may find in <code class="code">h_errno</code>:
</p>
<dl class="vtable">
<dt id='index-HOST_005fNOT_005fFOUND'><span><code class="code">HOST_NOT_FOUND</code><a class="copiable-link" href='#index-HOST_005fNOT_005fFOUND'> &para;</a></span></dt>
<dd>
<p>No such host is known in the database.
</p>
</dd>
<dt id='index-TRY_005fAGAIN'><span><code class="code">TRY_AGAIN</code><a class="copiable-link" href='#index-TRY_005fAGAIN'> &para;</a></span></dt>
<dd>
<p>This condition happens when the name server could not be contacted.  If
you try again later, you may succeed then.
</p>
</dd>
<dt id='index-NO_005fRECOVERY'><span><code class="code">NO_RECOVERY</code><a class="copiable-link" href='#index-NO_005fRECOVERY'> &para;</a></span></dt>
<dd>
<p>A non-recoverable error occurred.
</p>
</dd>
<dt id='index-NO_005fADDRESS'><span><code class="code">NO_ADDRESS</code><a class="copiable-link" href='#index-NO_005fADDRESS'> &para;</a></span></dt>
<dd>
<p>The host database contains an entry for the name, but it doesn&rsquo;t have an
associated Internet address.
</p></dd>
</dl>

<p>The lookup functions above all have one thing in common: they are not
reentrant and therefore unusable in multi-threaded applications.
Therefore provides the GNU C Library a new set of functions which can be
used in this context.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gethostbyname_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">gethostbyname_r</strong> <code class="def-code-arguments">(const char *restrict <var class="var">name</var>, struct hostent *restrict <var class="var">result_buf</var>, char *restrict <var class="var">buf</var>, size_t <var class="var">buflen</var>, struct hostent **restrict <var class="var">result</var>, int *restrict <var class="var">h_errnop</var>)</code><a class="copiable-link" href='#index-gethostbyname_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env locale
| AS-Unsafe dlopen plugin corrupt heap lock
| AC-Unsafe lock corrupt mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">gethostbyname_r</code> function returns information about the host
named <var class="var">name</var>.  The caller must pass a pointer to an object of type
<code class="code">struct hostent</code> in the <var class="var">result_buf</var> parameter.  In addition
the function may need extra buffer space and the caller must pass a
pointer and the size of the buffer in the <var class="var">buf</var> and <var class="var">buflen</var>
parameters.
</p>
<p>A pointer to the buffer, in which the result is stored, is available in
<code class="code">*<var class="var">result</var></code> after the function call successfully returned.  The
buffer passed as the <var class="var">buf</var> parameter can be freed only once the caller
has finished with the result hostent struct, or has copied it including all
the other memory that it points to.  If an error occurs or if no entry is
found, the pointer <code class="code">*<var class="var">result</var></code> is a null pointer.  Success is
signalled by a zero return value.  If the function failed the return value
is an error number.  In addition to the errors defined for
<code class="code">gethostbyname</code> it can also be <code class="code">ERANGE</code>.  In this case the call
should be repeated with a larger buffer.  Additional error information is
not stored in the global variable <code class="code">h_errno</code> but instead in the object
pointed to by <var class="var">h_errnop</var>.
</p>
<p>Here&rsquo;s a small example:
</p><div class="example smallexample">
<pre class="example-preformatted">struct hostent *
gethostname (char *host)
{
  struct hostent *hostbuf, *hp;
  size_t hstbuflen;
  char *tmphstbuf;
  int res;
  int herr;

  hostbuf = malloc (sizeof (struct hostent));
  hstbuflen = 1024;
  tmphstbuf = malloc (hstbuflen);

  while ((res = gethostbyname_r (host, hostbuf, tmphstbuf, hstbuflen,
                                 &amp;hp, &amp;herr)) == ERANGE)
    {
      /* Enlarge the buffer.  */
      tmphstbuf = reallocarray (tmphstbuf, hstbuflen, 2);
      hstbuflen *= 2;
    }

  free (tmphstbuf);
  /*  Check for errors.  */
  if (res || hp == NULL)
    return NULL;
  return hp;
}
</pre></div>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gethostbyname2_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">gethostbyname2_r</strong> <code class="def-code-arguments">(const char *<var class="var">name</var>, int <var class="var">af</var>, struct hostent *restrict <var class="var">result_buf</var>, char *restrict <var class="var">buf</var>, size_t <var class="var">buflen</var>, struct hostent **restrict <var class="var">result</var>, int *restrict <var class="var">h_errnop</var>)</code><a class="copiable-link" href='#index-gethostbyname2_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env locale
| AS-Unsafe dlopen plugin corrupt heap lock
| AC-Unsafe lock corrupt mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">gethostbyname2_r</code> function is like <code class="code">gethostbyname_r</code>, but
allows the caller to specify the desired address family (e.g.
<code class="code">AF_INET</code> or <code class="code">AF_INET6</code>) for the result.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gethostbyaddr_005fr"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">gethostbyaddr_r</strong> <code class="def-code-arguments">(const void *<var class="var">addr</var>, socklen_t <var class="var">length</var>, int <var class="var">format</var>, struct hostent *restrict <var class="var">result_buf</var>, char *restrict <var class="var">buf</var>, size_t <var class="var">buflen</var>, struct hostent **restrict <var class="var">result</var>, int *restrict <var class="var">h_errnop</var>)</code><a class="copiable-link" href='#index-gethostbyaddr_005fr'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe env locale
| AS-Unsafe dlopen plugin corrupt heap lock
| AC-Unsafe lock corrupt mem fd
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">gethostbyaddr_r</code> function returns information about the host
with Internet address <var class="var">addr</var>.  The parameter <var class="var">addr</var> is not
really a pointer to char - it can be a pointer to an IPv4 or an IPv6
address.  The <var class="var">length</var> argument is the size (in bytes) of the address
at <var class="var">addr</var>.  <var class="var">format</var> specifies the address format; for an IPv4
Internet address, specify a value of <code class="code">AF_INET</code>; for an IPv6
Internet address, use <code class="code">AF_INET6</code>.
</p>
<p>Similar to the <code class="code">gethostbyname_r</code> function, the caller must provide
buffers for the result and memory used internally.  In case of success
the function returns zero.  Otherwise the value is an error number where
<code class="code">ERANGE</code> has the special meaning that the caller-provided buffer is
too small.
</p></dd></dl>

<p>You can also scan the entire hosts database one entry at a time using
<code class="code">sethostent</code>, <code class="code">gethostent</code> and <code class="code">endhostent</code>.  Be careful
when using these functions because they are not reentrant.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sethostent"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">sethostent</strong> <code class="def-code-arguments">(int <var class="var">stayopen</var>)</code><a class="copiable-link" href='#index-sethostent'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:hostent env locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function opens the hosts database to begin scanning it.  You can
then call <code class="code">gethostent</code> to read the entries.
</p>
<p>If the <var class="var">stayopen</var> argument is nonzero, this sets a flag so that
subsequent calls to <code class="code">gethostbyname</code> or <code class="code">gethostbyaddr</code> will
not close the database (as they usually would).  This makes for more
efficiency if you call those functions several times, by avoiding
reopening the database for each call.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-gethostent"><span class="category-def">Function: </span><span><code class="def-type">struct hostent *</code> <strong class="def-name">gethostent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href='#index-gethostent'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:hostent race:hostentbuf env locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function returns the next entry in the hosts database.  It
returns a null pointer if there are no more entries.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-endhostent"><span class="category-def">Function: </span><span><code class="def-type">void</code> <strong class="def-name">endhostent</strong> <code class="def-code-arguments">(void)</code><a class="copiable-link" href='#index-endhostent'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Unsafe race:hostent env locale
| AS-Unsafe dlopen plugin heap lock
| AC-Unsafe corrupt lock fd mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function closes the hosts database.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Host-Address-Functions.html">Host Address Functions</a>, Up: <a href="Host-Addresses.html">Host Addresses</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
