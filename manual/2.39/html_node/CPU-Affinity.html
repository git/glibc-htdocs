<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>CPU Affinity (The GNU C Library)</title>

<meta name="description" content="CPU Affinity (The GNU C Library)">
<meta name="keywords" content="CPU Affinity (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Priority.html" rel="up" title="Priority">
<link href="Traditional-Scheduling.html" rel="prev" title="Traditional Scheduling">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="CPU-Affinity">
<div class="nav-panel">
<p>
Previous: <a href="Traditional-Scheduling.html" accesskey="p" rel="prev">Traditional Scheduling</a>, Up: <a href="Priority.html" accesskey="u" rel="up">Process CPU Priority And Scheduling</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Limiting-execution-to-certain-CPUs">23.3.5 Limiting execution to certain CPUs</h4>

<p>On a multi-processor system the operating system usually distributes
the different processes which are runnable on all available CPUs in a
way which allows the system to work most efficiently.  Which processes
and threads run can be to some extend be control with the scheduling
functionality described in the last sections.  But which CPU finally
executes which process or thread is not covered.
</p>
<p>There are a number of reasons why a program might want to have control
over this aspect of the system as well:
</p>
<ul class="itemize mark-bullet">
<li>One thread or process is responsible for absolutely critical work
which under no circumstances must be interrupted or hindered from
making progress by other processes or threads using CPU resources.  In
this case the special process would be confined to a CPU which no
other process or thread is allowed to use.

</li><li>The access to certain resources (RAM, I/O ports) has different costs
from different CPUs.  This is the case in NUMA (Non-Uniform Memory
Architecture) machines.  Preferably memory should be accessed locally
but this requirement is usually not visible to the scheduler.
Therefore forcing a process or thread to the CPUs which have local
access to the most-used memory helps to significantly boost the
performance.

</li><li>In controlled runtimes resource allocation and book-keeping work (for
instance garbage collection) is performance local to processors.  This
can help to reduce locking costs if the resources do not have to be
protected from concurrent accesses from different processors.
</li></ul>

<p>The POSIX standard up to this date is of not much help to solve this
problem.  The Linux kernel provides a set of interfaces to allow
specifying <em class="emph">affinity sets</em> for a process.  The scheduler will
schedule the thread or process on CPUs specified by the affinity
masks.  The interfaces which the GNU C Library define follow to some
extent the Linux kernel interface.
</p>
<dl class="first-deftp">
<dt class="deftp" id="index-cpu_005fset_005ft"><span class="category-def">Data Type: </span><span><strong class="def-name">cpu_set_t</strong><a class="copiable-link" href='#index-cpu_005fset_005ft'> &para;</a></span></dt>
<dd>
<p>This data set is a bitset where each bit represents a CPU.  How the
system&rsquo;s CPUs are mapped to bits in the bitset is system dependent.
The data type has a fixed size; in the unlikely case that the number
of bits are not sufficient to describe the CPUs of the system a
different interface has to be used.
</p>
<p>This type is a GNU extension and is defined in <samp class="file">sched.h</samp>.
</p></dd></dl>

<p>To manipulate the bitset, to set and reset bits, a number of macros are
defined.  Some of the macros take a CPU number as a parameter.  Here
it is important to never exceed the size of the bitset.  The following
macro specifies the number of bits in the <code class="code">cpu_set_t</code> bitset.
</p>
<dl class="first-deftypevr">
<dt class="deftypevr" id="index-CPU_005fSETSIZE"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">CPU_SETSIZE</strong><a class="copiable-link" href='#index-CPU_005fSETSIZE'> &para;</a></span></dt>
<dd>
<p>The value of this macro is the maximum number of CPUs which can be
handled with a <code class="code">cpu_set_t</code> object.
</p></dd></dl>

<p>The type <code class="code">cpu_set_t</code> should be considered opaque; all
manipulation should happen via the next four macros.
</p>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-CPU_005fZERO"><span class="category-def">Macro: </span><span><code class="def-type">void</code> <strong class="def-name">CPU_ZERO</strong> <code class="def-code-arguments">(cpu_set_t *<var class="var">set</var>)</code><a class="copiable-link" href='#index-CPU_005fZERO'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro initializes the CPU set <var class="var">set</var> to be the empty set.
</p>
<p>This macro is a GNU extension and is defined in <samp class="file">sched.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-CPU_005fSET"><span class="category-def">Macro: </span><span><code class="def-type">void</code> <strong class="def-name">CPU_SET</strong> <code class="def-code-arguments">(int <var class="var">cpu</var>, cpu_set_t *<var class="var">set</var>)</code><a class="copiable-link" href='#index-CPU_005fSET'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro adds <var class="var">cpu</var> to the CPU set <var class="var">set</var>.
</p>
<p>The <var class="var">cpu</var> parameter must not have side effects since it is
evaluated more than once.
</p>
<p>This macro is a GNU extension and is defined in <samp class="file">sched.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-CPU_005fCLR"><span class="category-def">Macro: </span><span><code class="def-type">void</code> <strong class="def-name">CPU_CLR</strong> <code class="def-code-arguments">(int <var class="var">cpu</var>, cpu_set_t *<var class="var">set</var>)</code><a class="copiable-link" href='#index-CPU_005fCLR'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro removes <var class="var">cpu</var> from the CPU set <var class="var">set</var>.
</p>
<p>The <var class="var">cpu</var> parameter must not have side effects since it is
evaluated more than once.
</p>
<p>This macro is a GNU extension and is defined in <samp class="file">sched.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-CPU_005fISSET"><span class="category-def">Macro: </span><span><code class="def-type">int</code> <strong class="def-name">CPU_ISSET</strong> <code class="def-code-arguments">(int <var class="var">cpu</var>, const cpu_set_t *<var class="var">set</var>)</code><a class="copiable-link" href='#index-CPU_005fISSET'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This macro returns a nonzero value (true) if <var class="var">cpu</var> is a member
of the CPU set <var class="var">set</var>, and zero (false) otherwise.
</p>
<p>The <var class="var">cpu</var> parameter must not have side effects since it is
evaluated more than once.
</p>
<p>This macro is a GNU extension and is defined in <samp class="file">sched.h</samp>.
</p></dd></dl>


<p>CPU bitsets can be constructed from scratch or the currently installed
affinity mask can be retrieved from the system.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sched_005fgetaffinity"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sched_getaffinity</strong> <code class="def-code-arguments">(pid_t <var class="var">pid</var>, size_t <var class="var">cpusetsize</var>, cpu_set_t *<var class="var">cpuset</var>)</code><a class="copiable-link" href='#index-sched_005fgetaffinity'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function stores the CPU affinity mask for the process or thread
with the ID <var class="var">pid</var> in the <var class="var">cpusetsize</var> bytes long bitmap
pointed to by <var class="var">cpuset</var>.  If successful, the function always
initializes all bits in the <code class="code">cpu_set_t</code> object and returns zero.
</p>
<p>If <var class="var">pid</var> does not correspond to a process or thread on the system
the or the function fails for some other reason, it returns <code class="code">-1</code>
and <code class="code">errno</code> is set to represent the error condition.
</p>
<dl class="table">
<dt><code class="code">ESRCH</code></dt>
<dd><p>No process or thread with the given ID found.
</p>
</dd>
<dt><code class="code">EFAULT</code></dt>
<dd><p>The pointer <var class="var">cpuset</var> does not point to a valid object.
</p></dd>
</dl>

<p>This function is a GNU extension and is declared in <samp class="file">sched.h</samp>.
</p></dd></dl>

<p>Note that it is not portably possible to use this information to
retrieve the information for different POSIX threads.  A separate
interface must be provided for that.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-sched_005fsetaffinity"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">sched_setaffinity</strong> <code class="def-code-arguments">(pid_t <var class="var">pid</var>, size_t <var class="var">cpusetsize</var>, const cpu_set_t *<var class="var">cpuset</var>)</code><a class="copiable-link" href='#index-sched_005fsetaffinity'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>This function installs the <var class="var">cpusetsize</var> bytes long affinity mask
pointed to by <var class="var">cpuset</var> for the process or thread with the ID <var class="var">pid</var>.
If successful the function returns zero and the scheduler will in the future
take the affinity information into account.
</p>
<p>If the function fails it will return <code class="code">-1</code> and <code class="code">errno</code> is set
to the error code:
</p>
<dl class="table">
<dt><code class="code">ESRCH</code></dt>
<dd><p>No process or thread with the given ID found.
</p>
</dd>
<dt><code class="code">EFAULT</code></dt>
<dd><p>The pointer <var class="var">cpuset</var> does not point to a valid object.
</p>
</dd>
<dt><code class="code">EINVAL</code></dt>
<dd><p>The bitset is not valid.  This might mean that the affinity set might
not leave a processor for the process or thread to run on.
</p></dd>
</dl>

<p>This function is a GNU extension and is declared in <samp class="file">sched.h</samp>.
</p></dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-getcpu"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">getcpu</strong> <code class="def-code-arguments">(unsigned int *cpu, unsigned int *node)</code><a class="copiable-link" href='#index-getcpu'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code class="code">getcpu</code> function identifies the processor and node on which
the calling thread or process is currently running and writes them into
the integers pointed to by the <var class="var">cpu</var> and <var class="var">node</var> arguments.  The
processor is a unique nonnegative integer identifying a CPU.  The node
is a unique nonnegative integer identifying a NUMA node.  When either
<var class="var">cpu</var> or <var class="var">node</var> is <code class="code">NULL</code>, nothing is written to the
respective pointer.
</p>
<p>The return value is <code class="code">0</code> on success and <code class="code">-1</code> on failure.  The
following <code class="code">errno</code> error condition is defined for this function:
</p>
<dl class="table">
<dt><code class="code">ENOSYS</code></dt>
<dd><p>The operating system does not support this function.
</p></dd>
</dl>

<p>This function is Linux-specific and is declared in <samp class="file">sched.h</samp>.
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Traditional-Scheduling.html">Traditional Scheduling</a>, Up: <a href="Priority.html">Process CPU Priority And Scheduling</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
