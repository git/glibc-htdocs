<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This is
The GNU C Library Reference Manual, for version
2.39.

Copyright © 1993-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Dynamic Output (The GNU C Library)</title>

<meta name="description" content="Dynamic Output (The GNU C Library)">
<meta name="keywords" content="Dynamic Output (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Formatted-Output.html" rel="up" title="Formatted Output">
<link href="Variable-Arguments-Output.html" rel="next" title="Variable Arguments Output">
<link href="Formatted-Output-Functions.html" rel="prev" title="Formatted Output Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.r {font-family: initial; font-weight: normal; font-style: normal}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Dynamic-Output">
<div class="nav-panel">
<p>
Next: <a href="Variable-Arguments-Output.html" accesskey="n" rel="next">Variable Arguments Output Functions</a>, Previous: <a href="Formatted-Output-Functions.html" accesskey="p" rel="prev">Formatted Output Functions</a>, Up: <a href="Formatted-Output.html" accesskey="u" rel="up">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Dynamically-Allocating-Formatted-Output">12.12.8 Dynamically Allocating Formatted Output</h4>

<p>The functions in this section do formatted output and place the results
in dynamically allocated memory.
</p>
<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-asprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">asprintf</strong> <code class="def-code-arguments">(char **<var class="var">ptr</var>, const char *<var class="var">template</var>, &hellip;)</code><a class="copiable-link" href='#index-asprintf'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe locale
| AS-Unsafe heap
| AC-Unsafe mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">sprintf</code>, except that it dynamically
allocates a string (as with <code class="code">malloc</code>; see <a class="pxref" href="Unconstrained-Allocation.html">Unconstrained Allocation</a>) to hold the output, instead of putting the output in a
buffer you allocate in advance.  The <var class="var">ptr</var> argument should be the
address of a <code class="code">char *</code> object, and a successful call to
<code class="code">asprintf</code> stores a pointer to the newly allocated string at that
location.
</p>
<p>The return value is the number of characters allocated for the buffer, or
less than zero if an error occurred.  Usually this means that the buffer
could not be allocated.
</p>
<p>Here is how to use <code class="code">asprintf</code> to get the same result as the
<code class="code">snprintf</code> example, but more easily:
</p>
<div class="example smallexample">
<pre class="example-preformatted">/* <span class="r">Construct a message describing the value of a variable</span>
   <span class="r">whose name is <var class="var">name</var> and whose value is <var class="var">value</var>.</span> */
char *
make_message (char *name, char *value)
{
  char *result;
  if (asprintf (&amp;result, &quot;value of %s is %s&quot;, name, value) &lt; 0)
    return NULL;
  return result;
}
</pre></div>
</dd></dl>

<dl class="first-deftypefn first-deftypefun-alias-first-deftypefn">
<dt class="deftypefn deftypefun-alias-deftypefn" id="index-obstack_005fprintf"><span class="category-def">Function: </span><span><code class="def-type">int</code> <strong class="def-name">obstack_printf</strong> <code class="def-code-arguments">(struct obstack *<var class="var">obstack</var>, const char *<var class="var">template</var>, &hellip;)</code><a class="copiable-link" href='#index-obstack_005fprintf'> &para;</a></span></dt>
<dd>
<p>Preliminary:
| MT-Safe race:obstack locale
| AS-Unsafe corrupt heap
| AC-Unsafe corrupt mem
| See <a class="xref" href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>This function is similar to <code class="code">asprintf</code>, except that it uses the
obstack <var class="var">obstack</var> to allocate the space.  See <a class="xref" href="Obstacks.html">Obstacks</a>.
</p>
<p>The characters are written onto the end of the current object.
To get at them, you must finish the object with <code class="code">obstack_finish</code>
(see <a class="pxref" href="Growing-Objects.html">Growing Objects</a>).
</p></dd></dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Variable-Arguments-Output.html">Variable Arguments Output Functions</a>, Previous: <a href="Formatted-Output-Functions.html">Formatted Output Functions</a>, Up: <a href="Formatted-Output.html">Formatted Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
