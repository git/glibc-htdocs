<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU C Library.

This is
The GNU C Library Reference Manual, for version
2.23.

Copyright (C) 1993-2016 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Malloc Tunable Parameters (The GNU C Library)</title>

<meta name="description" content="Malloc Tunable Parameters (The GNU C Library)">
<meta name="keywords" content="Malloc Tunable Parameters (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Unconstrained-Allocation.html" rel="up" title="Unconstrained Allocation">
<link href="Heap-Consistency-Checking.html" rel="next" title="Heap Consistency Checking">
<link href="Aligned-Memory-Blocks.html" rel="prev" title="Aligned Memory Blocks">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection" id="Malloc-Tunable-Parameters">
<div class="header">
<p>
Next: <a href="Heap-Consistency-Checking.html" accesskey="n" rel="next">Heap Consistency Checking</a>, Previous: <a href="Aligned-Memory-Blocks.html" accesskey="p" rel="prev">Allocating Aligned Memory Blocks</a>, Up: <a href="Unconstrained-Allocation.html" accesskey="u" rel="up">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Malloc-Tunable-Parameters-1"></span><h4 class="subsubsection">3.2.2.8 Malloc Tunable Parameters</h4>

<p>You can adjust some parameters for dynamic memory allocation with the
<code>mallopt</code> function.  This function is the general SVID/XPG
interface, defined in <samp>malloc.h</samp>.
<span id="index-malloc_002eh"></span>
</p>
<dl class="def">
<dt id="index-mallopt"><span class="category">Function: </span><span><em>int</em> <strong>mallopt</strong> <em>(int <var>param</var>, int <var>value</var>)</em><a href='#index-mallopt' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Unsafe init const:mallopt
| AS-Unsafe init lock
| AC-Unsafe init lock
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>


<p>When calling <code>mallopt</code>, the <var>param</var> argument specifies the
parameter to be set, and <var>value</var> the new value to be set.  Possible
choices for <var>param</var>, as defined in <samp>malloc.h</samp>, are:
</p>
<dl compact="compact">
<dt><span><code>M_MMAP_MAX</code></span></dt>
<dd><p>The maximum number of chunks to allocate with <code>mmap</code>.  Setting this
to zero disables all use of <code>mmap</code>.
</p></dd>
<dt><span><code>M_MMAP_THRESHOLD</code></span></dt>
<dd><p>All chunks larger than this value are allocated outside the normal
heap, using the <code>mmap</code> system call.  This way it is guaranteed
that the memory for these chunks can be returned to the system on
<code>free</code>.  Note that requests smaller than this threshold might still
be allocated via <code>mmap</code>.
</p></dd>
<dt><span><code>M_PERTURB</code></span></dt>
<dd><p>If non-zero, memory blocks are filled with values depending on some
low order bits of this parameter when they are allocated (except when
allocated by <code>calloc</code>) and freed.  This can be used to debug the
use of uninitialized or freed heap memory.  Note that this option does not
guarantee that the freed block will have any specific values.  It only
guarantees that the content the block had before it was freed will be
overwritten.
</p></dd>
<dt><span><code>M_TOP_PAD</code></span></dt>
<dd><p>This parameter determines the amount of extra memory to obtain from the
system when a call to <code>sbrk</code> is required.  It also specifies the
number of bytes to retain when shrinking the heap by calling <code>sbrk</code>
with a negative argument.  This provides the necessary hysteresis in
heap size such that excessive amounts of system calls can be avoided.
</p></dd>
<dt><span><code>M_TRIM_THRESHOLD</code></span></dt>
<dd><p>This is the minimum size (in bytes) of the top-most, releasable chunk
that will cause <code>sbrk</code> to be called with a negative argument in
order to return memory to the system.
</p></dd>
</dl>

</dd></dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Heap-Consistency-Checking.html">Heap Consistency Checking</a>, Previous: <a href="Aligned-Memory-Blocks.html">Allocating Aligned Memory Blocks</a>, Up: <a href="Unconstrained-Allocation.html">Unconstrained Allocation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
