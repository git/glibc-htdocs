<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU C Library.

This is
The GNU C Library Reference Manual, for version
2.23.

Copyright (C) 1993-2016 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>FP Bit Twiddling (The GNU C Library)</title>

<meta name="description" content="FP Bit Twiddling (The GNU C Library)">
<meta name="keywords" content="FP Bit Twiddling (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Arithmetic-Functions.html" rel="up" title="Arithmetic Functions">
<link href="FP-Comparison-Functions.html" rel="next" title="FP Comparison Functions">
<link href="Remainder-Functions.html" rel="prev" title="Remainder Functions">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="subsection" id="FP-Bit-Twiddling">
<div class="header">
<p>
Next: <a href="FP-Comparison-Functions.html" accesskey="n" rel="next">Floating-Point Comparison Functions</a>, Previous: <a href="Remainder-Functions.html" accesskey="p" rel="prev">Remainder Functions</a>, Up: <a href="Arithmetic-Functions.html" accesskey="u" rel="up">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Setting-and-modifying-single-bits-of-FP-values"></span><h4 class="subsection">20.8.5 Setting and modifying single bits of FP values</h4>
<span id="index-FP-arithmetic"></span>

<p>There are some operations that are too complicated or expensive to
perform by hand on floating-point numbers.  ISO&nbsp;C99<!-- /@w --> defines
functions to do these operations, which mostly involve changing single
bits.
</p>
<dl class="def">
<dt id="index-copysign"><span class="category">Function: </span><span><em>double</em> <strong>copysign</strong> <em>(double <var>x</var>, double <var>y</var>)</em><a href='#index-copysign' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-copysignf"><span class="category">Function: </span><span><em>float</em> <strong>copysignf</strong> <em>(float <var>x</var>, float <var>y</var>)</em><a href='#index-copysignf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-copysignl"><span class="category">Function: </span><span><em>long double</em> <strong>copysignl</strong> <em>(long double <var>x</var>, long double <var>y</var>)</em><a href='#index-copysignl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions return <var>x</var> but with the sign of <var>y</var>.  They work
even if <var>x</var> or <var>y</var> are NaN or zero.  Both of these can carry a
sign (although not all implementations support it) and this is one of
the few operations that can tell the difference.
</p>
<p><code>copysign</code> never raises an exception.
</p>
<p>This function is defined in IEC&nbsp;559<!-- /@w --> (and the appendix with
recommended functions in IEEE&nbsp;754<!-- /@w -->/IEEE&nbsp;854<!-- /@w -->).
</p></dd></dl>

<dl class="def">
<dt id="index-signbit"><span class="category">Function: </span><span><em>int</em> <strong>signbit</strong> <em>(<em>float-type</em> <var>x</var>)</em><a href='#index-signbit' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p><code>signbit</code> is a generic macro which can work on all floating-point
types.  It returns a nonzero value if the value of <var>x</var> has its sign
bit set.
</p>
<p>This is not the same as <code>x &lt; 0.0</code>, because IEEE&nbsp;754<!-- /@w --> floating
point allows zero to be signed.  The comparison <code>-0.0 &lt; 0.0</code> is
false, but <code>signbit (-0.0)</code> will return a nonzero value.
</p></dd></dl>

<dl class="def">
<dt id="index-nextafter"><span class="category">Function: </span><span><em>double</em> <strong>nextafter</strong> <em>(double <var>x</var>, double <var>y</var>)</em><a href='#index-nextafter' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-nextafterf"><span class="category">Function: </span><span><em>float</em> <strong>nextafterf</strong> <em>(float <var>x</var>, float <var>y</var>)</em><a href='#index-nextafterf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-nextafterl"><span class="category">Function: </span><span><em>long double</em> <strong>nextafterl</strong> <em>(long double <var>x</var>, long double <var>y</var>)</em><a href='#index-nextafterl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code>nextafter</code> function returns the next representable neighbor of
<var>x</var> in the direction towards <var>y</var>.  The size of the step between
<var>x</var> and the result depends on the type of the result.  If
<em class='math'><var>x</var> = <var>y</var></em> the function simply returns <var>y</var>.  If either
value is <code>NaN</code>, <code>NaN</code> is returned.  Otherwise
a value corresponding to the value of the least significant bit in the
mantissa is added or subtracted, depending on the direction.
<code>nextafter</code> will signal overflow or underflow if the result goes
outside of the range of normalized numbers.
</p>
<p>This function is defined in IEC&nbsp;559<!-- /@w --> (and the appendix with
recommended functions in IEEE&nbsp;754<!-- /@w -->/IEEE&nbsp;854<!-- /@w -->).
</p></dd></dl>

<dl class="def">
<dt id="index-nexttoward"><span class="category">Function: </span><span><em>double</em> <strong>nexttoward</strong> <em>(double <var>x</var>, long double <var>y</var>)</em><a href='#index-nexttoward' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-nexttowardf"><span class="category">Function: </span><span><em>float</em> <strong>nexttowardf</strong> <em>(float <var>x</var>, long double <var>y</var>)</em><a href='#index-nexttowardf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-nexttowardl"><span class="category">Function: </span><span><em>long double</em> <strong>nexttowardl</strong> <em>(long double <var>x</var>, long double <var>y</var>)</em><a href='#index-nexttowardl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions are identical to the corresponding versions of
<code>nextafter</code> except that their second argument is a <code>long
double</code>.
</p></dd></dl>

<span id="index-NaN-1"></span>
<dl class="def">
<dt id="index-nan"><span class="category">Function: </span><span><em>double</em> <strong>nan</strong> <em>(const char *<var>tagp</var>)</em><a href='#index-nan' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-nanf"><span class="category">Function: </span><span><em>float</em> <strong>nanf</strong> <em>(const char *<var>tagp</var>)</em><a href='#index-nanf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-nanl"><span class="category">Function: </span><span><em>long double</em> <strong>nanl</strong> <em>(const char *<var>tagp</var>)</em><a href='#index-nanl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe locale
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code>nan</code> function returns a representation of NaN, provided that
NaN is supported by the target platform.
<code>nan (&quot;<var>n-char-sequence</var>&quot;)</code> is equivalent to
<code>strtod (&quot;NAN(<var>n-char-sequence</var>)&quot;)</code>.
</p>
<p>The argument <var>tagp</var> is used in an unspecified manner.  On IEEE&nbsp;754<!-- /@w --> systems, there are many representations of NaN, and <var>tagp</var>
selects one.  On other systems it may do nothing.
</p></dd></dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="FP-Comparison-Functions.html">Floating-Point Comparison Functions</a>, Previous: <a href="Remainder-Functions.html">Remainder Functions</a>, Up: <a href="Arithmetic-Functions.html">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
