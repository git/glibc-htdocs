<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU C Library.

This is
The GNU C Library Reference Manual, for version
2.24.

Copyright (C) 1993-2016 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Rounding Functions (The GNU C Library)</title>

<meta name="description" content="Rounding Functions (The GNU C Library)">
<meta name="keywords" content="Rounding Functions (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Arithmetic-Functions.html" rel="up" title="Arithmetic Functions">
<link href="Remainder-Functions.html" rel="next" title="Remainder Functions">
<link href="Normalization-Functions.html" rel="prev" title="Normalization Functions">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="subsection" id="Rounding-Functions">
<div class="header">
<p>
Next: <a href="Remainder-Functions.html" accesskey="n" rel="next">Remainder Functions</a>, Previous: <a href="Normalization-Functions.html" accesskey="p" rel="prev">Normalization Functions</a>, Up: <a href="Arithmetic-Functions.html" accesskey="u" rel="up">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Rounding-Functions-1"></span><h4 class="subsection">20.8.3 Rounding Functions</h4>
<span id="index-converting-floats-to-integers"></span>

<span id="index-math_002eh-4"></span>
<p>The functions listed here perform operations such as rounding and
truncation of floating-point values.  Some of these functions convert
floating point numbers to integer values.  They are all declared in
<samp>math.h</samp>.
</p>
<p>You can also convert floating-point numbers to integers simply by
casting them to <code>int</code>.  This discards the fractional part,
effectively rounding towards zero.  However, this only works if the
result can actually be represented as an <code>int</code>&mdash;for very large
numbers, this is impossible.  The functions listed here return the
result as a <code>double</code> instead to get around this problem.
</p>
<dl class="def">
<dt id="index-ceil"><span class="category">Function: </span><span><em>double</em> <strong>ceil</strong> <em>(double <var>x</var>)</em><a href='#index-ceil' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-ceilf"><span class="category">Function: </span><span><em>float</em> <strong>ceilf</strong> <em>(float <var>x</var>)</em><a href='#index-ceilf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-ceill"><span class="category">Function: </span><span><em>long double</em> <strong>ceill</strong> <em>(long double <var>x</var>)</em><a href='#index-ceill' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions round <var>x</var> upwards to the nearest integer,
returning that value as a <code>double</code>.  Thus, <code>ceil (1.5)</code>
is <code>2.0</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-floor"><span class="category">Function: </span><span><em>double</em> <strong>floor</strong> <em>(double <var>x</var>)</em><a href='#index-floor' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-floorf"><span class="category">Function: </span><span><em>float</em> <strong>floorf</strong> <em>(float <var>x</var>)</em><a href='#index-floorf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-floorl"><span class="category">Function: </span><span><em>long double</em> <strong>floorl</strong> <em>(long double <var>x</var>)</em><a href='#index-floorl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions round <var>x</var> downwards to the nearest
integer, returning that value as a <code>double</code>.  Thus, <code>floor
(1.5)</code> is <code>1.0</code> and <code>floor (-1.5)</code> is <code>-2.0</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-trunc"><span class="category">Function: </span><span><em>double</em> <strong>trunc</strong> <em>(double <var>x</var>)</em><a href='#index-trunc' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-truncf"><span class="category">Function: </span><span><em>float</em> <strong>truncf</strong> <em>(float <var>x</var>)</em><a href='#index-truncf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-truncl"><span class="category">Function: </span><span><em>long double</em> <strong>truncl</strong> <em>(long double <var>x</var>)</em><a href='#index-truncl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>The <code>trunc</code> functions round <var>x</var> towards zero to the nearest
integer (returned in floating-point format).  Thus, <code>trunc (1.5)</code>
is <code>1.0</code> and <code>trunc (-1.5)</code> is <code>-1.0</code>.
</p></dd></dl>

<dl class="def">
<dt id="index-rint"><span class="category">Function: </span><span><em>double</em> <strong>rint</strong> <em>(double <var>x</var>)</em><a href='#index-rint' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-rintf"><span class="category">Function: </span><span><em>float</em> <strong>rintf</strong> <em>(float <var>x</var>)</em><a href='#index-rintf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-rintl"><span class="category">Function: </span><span><em>long double</em> <strong>rintl</strong> <em>(long double <var>x</var>)</em><a href='#index-rintl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions round <var>x</var> to an integer value according to the
current rounding mode.  See <a href="Floating-Point-Parameters.html">Floating Point Parameters</a>, for
information about the various rounding modes.  The default
rounding mode is to round to the nearest integer; some machines
support other modes, but round-to-nearest is always used unless
you explicitly select another.
</p>
<p>If <var>x</var> was not initially an integer, these functions raise the
inexact exception.
</p></dd></dl>

<dl class="def">
<dt id="index-nearbyint"><span class="category">Function: </span><span><em>double</em> <strong>nearbyint</strong> <em>(double <var>x</var>)</em><a href='#index-nearbyint' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-nearbyintf"><span class="category">Function: </span><span><em>float</em> <strong>nearbyintf</strong> <em>(float <var>x</var>)</em><a href='#index-nearbyintf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-nearbyintl"><span class="category">Function: </span><span><em>long double</em> <strong>nearbyintl</strong> <em>(long double <var>x</var>)</em><a href='#index-nearbyintl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions return the same value as the <code>rint</code> functions, but
do not raise the inexact exception if <var>x</var> is not an integer.
</p></dd></dl>

<dl class="def">
<dt id="index-round"><span class="category">Function: </span><span><em>double</em> <strong>round</strong> <em>(double <var>x</var>)</em><a href='#index-round' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-roundf"><span class="category">Function: </span><span><em>float</em> <strong>roundf</strong> <em>(float <var>x</var>)</em><a href='#index-roundf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-roundl"><span class="category">Function: </span><span><em>long double</em> <strong>roundl</strong> <em>(long double <var>x</var>)</em><a href='#index-roundl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions are similar to <code>rint</code>, but they round halfway
cases away from zero instead of to the nearest integer (or other
current rounding mode).
</p></dd></dl>

<dl class="def">
<dt id="index-lrint"><span class="category">Function: </span><span><em>long int</em> <strong>lrint</strong> <em>(double <var>x</var>)</em><a href='#index-lrint' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-lrintf"><span class="category">Function: </span><span><em>long int</em> <strong>lrintf</strong> <em>(float <var>x</var>)</em><a href='#index-lrintf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-lrintl"><span class="category">Function: </span><span><em>long int</em> <strong>lrintl</strong> <em>(long double <var>x</var>)</em><a href='#index-lrintl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions are just like <code>rint</code>, but they return a
<code>long int</code> instead of a floating-point number.
</p></dd></dl>

<dl class="def">
<dt id="index-llrint"><span class="category">Function: </span><span><em>long long int</em> <strong>llrint</strong> <em>(double <var>x</var>)</em><a href='#index-llrint' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-llrintf"><span class="category">Function: </span><span><em>long long int</em> <strong>llrintf</strong> <em>(float <var>x</var>)</em><a href='#index-llrintf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-llrintl"><span class="category">Function: </span><span><em>long long int</em> <strong>llrintl</strong> <em>(long double <var>x</var>)</em><a href='#index-llrintl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions are just like <code>rint</code>, but they return a
<code>long long int</code> instead of a floating-point number.
</p></dd></dl>

<dl class="def">
<dt id="index-lround"><span class="category">Function: </span><span><em>long int</em> <strong>lround</strong> <em>(double <var>x</var>)</em><a href='#index-lround' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-lroundf"><span class="category">Function: </span><span><em>long int</em> <strong>lroundf</strong> <em>(float <var>x</var>)</em><a href='#index-lroundf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-lroundl"><span class="category">Function: </span><span><em>long int</em> <strong>lroundl</strong> <em>(long double <var>x</var>)</em><a href='#index-lroundl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions are just like <code>round</code>, but they return a
<code>long int</code> instead of a floating-point number.
</p></dd></dl>

<dl class="def">
<dt id="index-llround"><span class="category">Function: </span><span><em>long long int</em> <strong>llround</strong> <em>(double <var>x</var>)</em><a href='#index-llround' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-llroundf"><span class="category">Function: </span><span><em>long long int</em> <strong>llroundf</strong> <em>(float <var>x</var>)</em><a href='#index-llroundf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-llroundl"><span class="category">Function: </span><span><em>long long int</em> <strong>llroundl</strong> <em>(long double <var>x</var>)</em><a href='#index-llroundl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions are just like <code>round</code>, but they return a
<code>long long int</code> instead of a floating-point number.
</p></dd></dl>


<dl class="def">
<dt id="index-modf"><span class="category">Function: </span><span><em>double</em> <strong>modf</strong> <em>(double <var>value</var>, double *<var>integer-part</var>)</em><a href='#index-modf' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-modff"><span class="category">Function: </span><span><em>float</em> <strong>modff</strong> <em>(float <var>value</var>, float *<var>integer-part</var>)</em><a href='#index-modff' class='copiable-anchor'> &para;</a></span></dt>
<dt id="index-modfl"><span class="category">Function: </span><span><em>long double</em> <strong>modfl</strong> <em>(long double <var>value</var>, long double *<var>integer-part</var>)</em><a href='#index-modfl' class='copiable-anchor'> &para;</a></span></dt>
<dd><p>Preliminary:
| MT-Safe 
| AS-Safe 
| AC-Safe 
| See <a href="POSIX-Safety-Concepts.html">POSIX Safety Concepts</a>.
</p>

<p>These functions break the argument <var>value</var> into an integer part and a
fractional part (between <code>-1</code> and <code>1</code>, exclusive).  Their sum
equals <var>value</var>.  Each of the parts has the same sign as <var>value</var>,
and the integer part is always rounded toward zero.
</p>
<p><code>modf</code> stores the integer part in <code>*<var>integer-part</var></code>, and
returns the fractional part.  For example, <code>modf (2.5, &amp;intpart)</code>
returns <code>0.5</code> and stores <code>2.0</code> into <code>intpart</code>.
</p></dd></dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Remainder-Functions.html">Remainder Functions</a>, Previous: <a href="Normalization-Functions.html">Normalization Functions</a>, Up: <a href="Arithmetic-Functions.html">Arithmetic Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
