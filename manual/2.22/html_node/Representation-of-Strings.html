<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU C Library.

This is
The GNU C Library Reference Manual, for version
2.22.

Copyright (C) 1993-2015 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version
1.3 or any later version published by the Free
Software Foundation; with the Invariant Sections being "Free Software
Needs Free Documentation" and "GNU Lesser General Public License",
the Front-Cover texts being "A GNU Manual", and with the Back-Cover
Texts as in (a) below.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

(a) The FSF's Back-Cover Text is: "You have the freedom to
copy and modify this GNU manual.  Buying copies from the FSF
supports it in developing GNU and promoting software freedom." -->
<title>Representation of Strings (The GNU C Library)</title>

<meta name="description" content="Representation of Strings (The GNU C Library)">
<meta name="keywords" content="Representation of Strings (The GNU C Library)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="String-and-Array-Utilities.html" rel="up" title="String and Array Utilities">
<link href="String_002fArray-Conventions.html" rel="next" title="String/Array Conventions">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="section" id="Representation-of-Strings">
<div class="header">
<p>
Next: <a href="String_002fArray-Conventions.html" accesskey="n" rel="next">String and Array Conventions</a>, Up: <a href="String-and-Array-Utilities.html" accesskey="u" rel="up">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Representation-of-Strings-1"></span><h3 class="section">5.1 Representation of Strings</h3>
<span id="index-string_002c-representation-of"></span>

<p>This section is a quick summary of string concepts for beginning C
programmers.  It describes how character strings are represented in C
and some common pitfalls.  If you are already familiar with this
material, you can skip this section.
</p>
<span id="index-string"></span>
<span id="index-multibyte-character-string"></span>
<p>A <em>string</em> is an array of <code>char</code> objects.  But string-valued
variables are usually declared to be pointers of type <code>char *</code>.
Such variables do not include space for the text of a string; that has
to be stored somewhere else&mdash;in an array variable, a string constant,
or dynamically allocated memory (see <a href="Memory-Allocation.html">Allocating Storage For Program Data</a>).  It&rsquo;s up to
you to store the address of the chosen memory space into the pointer
variable.  Alternatively you can store a <em>null pointer</em> in the
pointer variable.  The null pointer does not point anywhere, so
attempting to reference the string it points to gets an error.
</p>
<span id="index-wide-character-string"></span>
<p>&ldquo;string&rdquo; normally refers to multibyte character strings as opposed to
wide character strings.  Wide character strings are arrays of type
<code>wchar_t</code> and as for multibyte character strings usually pointers
of type <code>wchar_t *</code> are used.
</p>
<span id="index-null-character"></span>
<span id="index-null-wide-character"></span>
<p>By convention, a <em>null character</em>, <code>'\0'</code>, marks the end of a
multibyte character string and the <em>null wide character</em>,
<code>L'\0'</code>, marks the end of a wide character string.  For example, in
testing to see whether the <code>char *</code> variable <var>p</var> points to a
null character marking the end of a string, you can write
<code>!*<var>p</var></code> or <code>*<var>p</var> == '\0'</code>.
</p>
<p>A null character is quite different conceptually from a null pointer,
although both are represented by the integer <code>0</code>.
</p>
<span id="index-string-literal"></span>
<p><em>String literals</em> appear in C program source as strings of
characters between double-quote characters (&lsquo;<samp>&quot;</samp>&rsquo;) where the initial
double-quote character is immediately preceded by a capital &lsquo;<samp>L</samp>&rsquo;
(ell) character (as in <code>L&quot;foo&quot;</code>).  In ISO&nbsp;C<!-- /@w -->, string literals
can also be formed by <em>string concatenation</em>: <code>&quot;a&quot; &quot;b&quot;</code> is the
same as <code>&quot;ab&quot;</code>.  For wide character strings one can either use
<code>L&quot;a&quot; L&quot;b&quot;</code> or <code>L&quot;a&quot; &quot;b&quot;</code>.  Modification of string literals is
not allowed by the GNU C compiler, because literals are placed in
read-only storage.
</p>
<p>Character arrays that are declared <code>const</code> cannot be modified
either.  It&rsquo;s generally good style to declare non-modifiable string
pointers to be of type <code>const char *</code>, since this often allows the
C compiler to detect accidental modifications as well as providing some
amount of documentation about what your program intends to do with the
string.
</p>
<p>The amount of memory allocated for the character array may extend past
the null character that normally marks the end of the string.  In this
document, the term <em>allocated size</em> is always used to refer to the
total amount of memory allocated for the string, while the term
<em>length</em> refers to the number of characters up to (but not
including) the terminating null character.
<span id="index-length-of-string"></span>
<span id="index-allocation-size-of-string"></span>
<span id="index-size-of-string"></span>
<span id="index-string-length"></span>
<span id="index-string-allocation"></span>
</p>
<p>A notorious source of program bugs is trying to put more characters in a
string than fit in its allocated size.  When writing code that extends
strings or moves characters into a pre-allocated array, you should be
very careful to keep track of the length of the text and make explicit
checks for overflowing the array.  Many of the library functions
<em>do not</em> do this for you!  Remember also that you need to allocate
an extra byte to hold the null character that marks the end of the
string.
</p>
<span id="index-single_002dbyte-string"></span>
<span id="index-multibyte-string"></span>
<p>Originally strings were sequences of bytes where each byte represents a
single character.  This is still true today if the strings are encoded
using a single-byte character encoding.  Things are different if the
strings are encoded using a multibyte encoding (for more information on
encodings see <a href="Extended-Char-Intro.html">Introduction to Extended Characters</a>).  There is no difference in
the programming interface for these two kind of strings; the programmer
has to be aware of this and interpret the byte sequences accordingly.
</p>
<p>But since there is no separate interface taking care of these
differences the byte-based string functions are sometimes hard to use.
Since the count parameters of these functions specify bytes a call to
<code>strncpy</code> could cut a multibyte character in the middle and put an
incomplete (and therefore unusable) byte sequence in the target buffer.
</p>
<span id="index-wide-character-string-1"></span>
<p>To avoid these problems later versions of the ISO&nbsp;C<!-- /@w --> standard
introduce a second set of functions which are operating on <em>wide
characters</em> (see <a href="Extended-Char-Intro.html">Introduction to Extended Characters</a>).  These functions don&rsquo;t have
the problems the single-byte versions have since every wide character is
a legal, interpretable value.  This does not mean that cutting wide
character strings at arbitrary points is without problems.  It normally
is for alphabet-based languages (except for non-normalized text) but
languages based on syllables still have the problem that more than one
wide character is necessary to complete a logical unit.  This is a
higher level problem which the C&nbsp;library<!-- /@w --> functions are not designed
to solve.  But it is at least good that no invalid byte sequences can be
created.  Also, the higher level functions can also much easier operate
on wide character than on multibyte characters so that a general advise
is to use wide characters internally whenever text is more than simply
copied.
</p>
<p>The remaining of this chapter will discuss the functions for handling
wide character strings in parallel with the discussion of the multibyte
character strings since there is almost always an exact equivalent
available.
</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="String_002fArray-Conventions.html">String and Array Conventions</a>, Up: <a href="String-and-Array-Utilities.html">String and Array Utilities</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
